
/*
	Print book (pb)
*/

SET NAMES utf8;

drop table if exists pb_categories;
create table pb_categories (
	id int(11) not null auto_increment,
	folder varchar(50) not null,
	parent_id int(11),
	
	/* name */
	/* description */
	image varchar(255),
	
	status int(11) default 1,
	position int(11) default 1,
	created_at datetime,
	updated_at datetime,
	primary key (id)
) DEFAULT CHARSET=utf8;
alter table pb_categories add index pb_categories_position_idx (position);
alter table pb_categories add index pb_categories_status_idx (status);

drop table if exists pb_sizes;
SET character_set_client = utf8;
create table pb_sizes (
	id int(11) not null auto_increment,
	
	type int(11) default 1, /* 1-printbook; 2-photobook; 3-polibook */
	
	name varchar(255),
	price_base numeric(10,2) not null default 0,
	price_per_page numeric(10,2) not null default 0,
	price_personal_page numeric(10,2) default 0,
	price_personal_cover numeric(10,2) default 0,
	start_page int(11) default 12,
	stop_page int(11) default 96,
	page_block int(11) default 2,
	
	discount1 numeric(10,2) default 0, /* ����� 3..4 */
	discount2 numeric(10,2) default 0, /* ����� 5..10 */
	discount3 numeric(10,2) default 0, /* ����� 10.. */
	
	/* ����������� �������� ��� ��������, ����� ������������ ����� ��� ��������� ������� */
	page_min_width int(11),
	page_min_height int(11),
	cover_min_width int(11),
	cover_min_height int(11),
	
	image varchar(255),
	
	status int(11) default 1,
	position int(11) default 1,
	created_at datetime,
	updated_at datetime,
	primary key (id)
) DEFAULT CHARSET=utf8;
alter table pb_sizes add index pb_sizes_position_idx (position);
alter table pb_sizes add index pb_sizes_status_idx (status);

insert into pb_sizes (id,position,name,price_base,price_per_page,start_page,page_block,created_at) values (1,1,'20x30 см',70,4,12,4,now());
insert into pb_sizes (id,position,name,price_base,price_per_page,start_page,page_block,created_at) values (2,2,'30x30 см',150,5,16,2,now());
insert into pb_sizes (id,position,name,price_base,price_per_page,start_page,page_block,created_at) values (3,3,'30x20 см',83,4,16,4,now());
insert into pb_sizes (id,position,name,price_base,price_per_page,start_page,page_block,created_at) values (4,4,'20x20 см',69,4,12,4,now());
insert into pb_sizes (id,position,name,price_base,price_per_page,start_page,page_block,created_at) values (5,5,'20x15 см',52,3,12,4,now());
insert into pb_sizes (id,position,name,price_base,price_per_page,start_page,page_block,created_at) values (6,6,'15x20 см',52,3,12,4,now());

drop table if exists pb_covers;
create table pb_covers (
	id int(11) not null auto_increment,
	pb_size_id int(11) not null,
	category_id int(11) not null,
	
	name varchar(255),
	description varchar(255),
	price numeric(10,2) not null default 0,
	is_individual int(11) default 0, /* �������������� ������� */
	has_supercover int(11) default 0, /* �������� ������������������� */
	check_size int(11) default 1, /* ��������� ������ ������������ �����������? */
	supercover_price numeric(10,2) not null default 0,
	image varchar(255),
	allow_min_pages int(11),
	
	status int(11) default 1,
	position int(11) default 1,
	created_at datetime,
	updated_at datetime,
	primary key (id)
) DEFAULT CHARSET=utf8;
alter table pb_covers add index pb_covers_position_idx (position);
alter table pb_covers add index pb_covers_status_idx (status);
alter table pb_covers add index pb_covers_pb_size_id_idx (pb_size_id);

drop table if exists pb_supercovers;
create table pb_supercovers (
	id int(11) not null auto_increment,
	pb_cover_id int(11) not null,
	
	name varchar(255),
	description varchar(255),
	price numeric(10,2) not null default 0,
	image varchar(255),
	
	status int(11) default 1,
	position int(11) default 1,
	created_at datetime,
	updated_at datetime,
	primary key (id)
) DEFAULT CHARSET=utf8;
alter table pb_supercovers add index pb_supercovers_position_idx (position);
alter table pb_supercovers add index pb_supercovers_status_idx (status);
alter table pb_supercovers add index pb_supercovers_pb_cover_id_idx (pb_cover_id);

drop table if exists pb_papers;
create table pb_papers (
	id int(11) not null auto_increment,
	pb_size_id int(11) not null,
	
	name varchar(255),
	price numeric(10,2) not null default 0,
	
	status int(11) default 1,
	position int(11) default 1,
	created_at datetime,
	updated_at datetime,
	primary key (id)
) DEFAULT CHARSET=utf8;
alter table pb_papers add index pb_papers_position_idx (position);
alter table pb_papers add index pb_papers_status_idx (status);
alter table pb_papers add index pb_papers_pb_size_id_idx (pb_size_id);

drop table if exists pb_lamination;
create table pb_lamination (
	id int(11) not null auto_increment,
	pb_size_id int(11) not null,
	
	name varchar(255),
	price numeric(10,2) not null default 0,
	
	status int(11) default 1,
	position int(11) default 1,
	created_at datetime,
	updated_at datetime,
	primary key (id)
) DEFAULT CHARSET=utf8;
alter table pb_lamination add index pb_lamination_position_idx (position);
alter table pb_lamination add index pb_lamination_status_idx (status);
alter table pb_lamination add index pb_lamination_pb_size_id_idx (pb_size_id);

drop table if exists pb_personal_cover;
create table pb_personal_cover (
	id int(11) not null auto_increment,
	pb_size_id int(11) not null,
	
	name varchar(255),
	price numeric(10,2) not null default 0,
	
	status int(11) default 1,
	position int(11) default 1,
	created_at datetime,
	updated_at datetime,
	primary key (id)
) DEFAULT CHARSET=utf8;
alter table pb_personal_cover add index pb_personal_cover_position_idx (position);
alter table pb_personal_cover add index pb_personal_cover_status_idx (status);
alter table pb_personal_cover add index pb_personal_cover_pb_size_id_idx (pb_size_id);

/* ����: ������������, ... */
drop table if exists pb_boxes;
create table pb_boxes (
	id int(11) not null auto_increment,
	pb_size_id int(11) not null,
	category_id int(11) not null,
	
	name varchar(255),
	description varchar(255),
	price numeric(10,2) not null default 0,
	image varchar(255),
	
	status int(11) default 1,
	position int(11) default 1,
	created_at datetime,
	updated_at datetime,
	primary key (id)
) DEFAULT CHARSET=utf8;
alter table pb_boxes add index pb_boxes_position_idx (position);
alter table pb_boxes add index pb_boxes_status_idx (status);
alter table pb_boxes add index pb_boxes_pb_size_id_idx (pb_size_id);

/* ����: ������������, ... */
drop table if exists pb_design_works;
create table pb_design_works (
	id int(11) not null auto_increment,
	pb_size_id int(11) not null,
	category_id int(11) not null,
	
	name varchar(255),
	url varchar(255),
	base_price numeric(10,2) not null default 0,
	price numeric(10,2) not null default 0,
	image varchar(255),
	
	status int(11) default 1,
	position int(11) default 1,
	created_at datetime,
	updated_at datetime,
	primary key (id)
) DEFAULT CHARSET=utf8;
alter table pb_design_works add index pb_design_works_position_idx (position);
alter table pb_design_works add index pb_design_works_status_idx (status);
alter table pb_design_works add index pb_design_works_pb_size_id_idx (pb_size_id);

drop table if exists pb_paper_base;
create table pb_paper_base (
	id int(11) not null auto_increment,
	pb_size_id int(11) not null,
	
	name varchar(255),
	price numeric(10,2) not null default 0,
	
	status int(11) default 1,
	position int(11) default 1,
	created_at datetime,
	updated_at datetime,
	primary key (id)
) DEFAULT CHARSET=utf8;
alter table pb_paper_base add index pb_paper_base_position_idx (position);
alter table pb_paper_base add index pb_paper_base_status_idx (status);
alter table pb_paper_base add index pb_paper_base_pb_size_id_idx (pb_size_id);

drop table if exists pb_forzaces;
create table pb_forzaces (
	id int(11) not null auto_increment,
	pb_size_id int(11) not null,
	category_id int(11) not null,
	
	name varchar(255),
	description varchar(255),
	price numeric(10,2) not null default 0,
	image varchar(255),
	allow_min_pages int(11),
	
	status int(11) default 1,
	position int(11) default 1,
	created_at datetime,
	updated_at datetime,
	primary key (id)
) DEFAULT CHARSET=utf8;
alter table pb_forzaces add index pb_forzaces_position_idx (position);
alter table pb_forzaces add index pb_forzaces_status_idx (status);
alter table pb_forzaces add index pb_forzaces_pb_size_id_idx (pb_size_id);

/********************** order system *************************/
drop table if exists pb_settings;
create table pb_settings (
	id int(11) not null auto_increment,
	
	/* order_mail_subject */
	/* order_mail_body */
	
	/* order_mail_printed_subject */
	/* order_mail_printed_body */
	
	admin_emails varchar(255),
	/* admin_order_mail_subject */
	/* admin_order_mail_body */
	
	dont_use_discount int(11) default 0, /* 0-������������ ������� ������: 1-�� ������������ ������ */
	
	primary key (id)
) DEFAULT CHARSET=utf8;
insert into pb_settings (id) values(1);

/* The ways of delivery */
drop table if exists pb_deliveries;
create table pb_deliveries (
	id int(11) not null auto_increment,
	/* name */
	price decimal(6,2),
	order_prefix varchar(20),
	status int(11) default 1, /* 0-deleted; 1-active */
	position int(11) default 1,
	created_at datetime,
	updated_at datetime,
	primary key (id)
) DEFAULT CHARSET=utf8;

/* The way of payment */
drop table if exists pb_paymentways;
create table pb_paymentways (
	id int(11) not null auto_increment,
	/* name */
	/* description */
	status int(11) default 1, /* 0-deleted; 1-active */
	code int(11) default 1,
	discount int(11) default 0,
	position int(11) default 1,
	created_at datetime,
	updated_at datetime,
	primary key (id)
) DEFAULT CHARSET=utf8;

/* ����� ����� ��������� �������� � ������ */
drop table if exists pb_deliveries_paymentways;
create table pb_deliveries_paymentways (
	id int(11) not null auto_increment,
	delivery_id int(11) not null,
	paymentway_id int(11) not null,
	created_at datetime,
	updated_at datetime,
	primary key (id)
) DEFAULT CHARSET=utf8;

drop table if exists pb_orders;
create table pb_orders (
	id int(11) not null auto_increment,
	client_id int(11) not null,
	order_number varchar(50) unique not null,
	
	pages int(11) default 0,
	quantity int(11) default 0,
	personal_pages int(11) default 0, 
	selected_personal_pages varchar(255),
	forzaces int(11) default 1, /* 1-����(�������������), 2-���(���������������) */ 
	
	size_id int(11) default 0, 
	price_base numeric(10,2) default 0,
	price_per_page numeric(10,2) default 0,
	price_personal_page numeric(10,2) default 0,
	price_personal_cover numeric(10,2) default 0,
	
	cover_id int(11) default 0, 
	cover_price numeric(10,2) default 0,
	
	forzac_id int(11) default 0, 
	forzac_price numeric(10,2) default 0,
	
	supercover_id int(11) default 0, 
	supercover_price numeric(10,2) default 0,
	
	paper_id int(11) default 0,
	paper_price numeric(10,2) default 0,
	
	lamination_id int(11) default 0,
	lamination_price numeric(10,2) default 0,
	
	paper_base_id int(11) default 0,
	paper_base_price numeric(10,2) default 0,
	
	personal_cover_id int(11) default 0,
	personal_cover_price numeric(10,2) default 0,
	
	box_id int(11) default 0,
	box_price numeric(10,2) default 0,
	
	design_work_id int(11) default 0,
	design_work_base_price numeric(10,2) default 0,
	design_work_price numeric(10,2) default 0,
	
	paymentway_id int(11) default 0,
	delivery_id int(11) default 0,
	delivery_price numeric(10,2) default 0,
	
	address varchar(255), /* ����� ��������, ���� ��� ���������� �� ������ � ���������� */
	comments text,
	
	discount decimal(10,2) default 0.0,
	payed decimal(10,2) default 0.0,
	tax int(11) default 0,
	status int(11) default 0, /* 0 - ���������, �������������; 1 - ������; 2 - �������� �� �����������; 3-� ��������; 4 - ��������� */
	created_at datetime,
	updated_at datetime,
	primary key (id)
) DEFAULT CHARSET=utf8;
alter table pb_orders add index pb_orders_client_id_idx (client_id);
alter table pb_orders add index pb_orders_status_idx (status);
alter table pb_orders add index pb_orders_order_number_idx (order_number);
alter table pb_orders add index pb_orders_created_at_idx (created_at);

drop table if exists pb_order_images;
create table pb_order_images (
	id int(11) not null auto_increment,
	order_id int(11) not null,
	page int(11),
	personal_page int(11) default 0,
	type int(11) default 1, /* 1-page; 2-cover; 3-forzats1; 4-forzats2, 5-supercover */
	comments text,
	price decimal(6,2),
	thumbnail varchar(255), /* 240x180 */
	small varchar(255), /* fitted in 800x800 */
	small_w int(11),
	small_h int(11),
	original_filename varchar(255),
	filename varchar(255),
	filename_w int(11),
	filename_h int(11),
	created_at datetime,
	updated_at datetime,
	primary key (id)
) DEFAULT CHARSET=utf8;
alter table pb_order_images add index pb_order_images_order_id_idx (order_id);
alter table pb_order_images add index pb_order_images_page_idx (page);

drop table if exists pb_order_page_comments;
create table pb_order_page_comments (
	id int(11) not null auto_increment,
	order_id int(11) not null,
	page int(11), /* 1...n - pages; -2-cover; -5-supercover; -3-forzac1; -4-forzac2 */
	comments text,
	created_at datetime,
	updated_at datetime,
	primary key (id)
) DEFAULT CHARSET=utf8;
alter table pb_order_page_comments add index pb_order_page_comments_order_id_idx (order_id);
alter table pb_order_page_comments add index pb_order_page_comments_page_idx (page);

/********************** order system *************************/

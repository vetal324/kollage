
drop table if exists clients;
create table clients (
    id int(11) not null auto_increment,
    parent_id int(11),
    type_id int(11) default 1,
    street_id int(11),
    allow_fair int(11) default 0,
    professional int(11) default 0,
    firstname varchar(100),
    lastname varchar(100),
    lastname2 varchar(100),
    description text,
    email varchar(100) not null unique,
    password varchar(100),
    city varchar(50),
    zip varchar(10),
    address varchar(255),
    office varchar(255),
    phone varchar(20),
    bdate datetime,
    status int(11) default 1, /* 1-needs authorization; 2-active; 0-inactive; 3-order process is denied */
    send_news int(11) default 0,
    purse decimal(6,2) default 0.00,
    photo_discount int(11),
    position int(11) default 1,
    activation_key varchar(255),
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;
alter table clients add index clients_email_idx (email);
alter table clients add index clients_parent_id_idx (parent_id);
alter table clients add index clients_type_id_idx (type_id);
alter table clients add index clients_street_id_idx (street_id);
alter table clients add index clients_status_idx (status);
alter table clients add index clients_send_news_idx (send_news);

insert into clients (id,parent_id,email) values (1,null,'1, level1: 200');
insert into clients (id,parent_id,email) values (2,1,'2-level2: 100');
insert into clients (id,parent_id,email) values (3,2,'3-level3: 50');
insert into clients (id,parent_id,email) values (4,2,'4-level3: 200');
insert into clients (id,parent_id,email) values (5,4,'5-level4: 120');
insert into clients (id,parent_id,email) values (6,4,'6-level4: 100');
insert into clients (id,parent_id,email) values (7,6,'7-level5: 200');
insert into clients (id,parent_id,email) values (8,7,'8-level6: 100');
insert into clients (id,parent_id,email) values (9,8,'9-level7: 50');
insert into clients (id,parent_id,email) values (10,7,'10-level6: 40');
insert into clients (id,parent_id,email) values (11,10,'11-level7: 50');
insert into clients (id,parent_id,email) values (12,11,'12-level8: 100');
insert into clients (id,parent_id,email) values (13,12,'13-level9: 200');
insert into clients (id,parent_id,email) values (14,13,'14-level10: 300');
insert into clients (id,parent_id,email) values (15,14,'15-level11: 100');
insert into clients (id,parent_id,email) values (16,15,'16-level12: 60');
insert into clients (id,parent_id,email) values (17,11,'17-level8: 70');
insert into clients (id,parent_id,email) values (18,6,'18-level5: 300');
insert into clients (id,parent_id,email) values (19,18,'19-level6: 100');
insert into clients (id,parent_id,email) values (20,1,'20-level2: 500');
insert into clients (id,parent_id,email) values (21,20,'21-level3: 400');
insert into clients (id,parent_id,email) values (22,21,'22-level4: 300');
insert into clients (id,parent_id,email) values (23,22,'23-level5: 200');
insert into clients (id,parent_id,email) values (24,23,'24-level6: 100');
insert into clients (id,parent_id,email) values (25,23,'25-level6: 250');
insert into clients (id,parent_id,email) values (26,22,'26-level5: 50');
insert into clients (id,parent_id,email) values (27,21,'27-level4: 150');
insert into clients (id,parent_id,email) values (28,27,'28-level5: 200');
insert into clients (id,parent_id,email) values (29,27,'29-level5: 150');
insert into clients (id,parent_id,email) values (30,29,'30-level6: 150');
insert into clients (id,parent_id,email) values (31,20,'31-level3: 200');
insert into clients (id,parent_id,email) values (32,31,'32-level4: 100');
insert into clients (id,parent_id,email) values (33,32,'33-level5: 200');

drop table if exists sop_mlm;
create table sop_mlm (
    id int(11) not null auto_increment,
    parent_id int(11),
    client_id int(11) not null,
    sum decimal(10,2) default 0, /* ����� ������� �� ���� � ����, ������� ����������� ������ */
    own_orders_sum decimal(10,2) default 0, /* ����� ����������� ������� */
    branches int(11) default 0, /* ���������� ����� � ���� */
    percent int(11) default 0, /* ���������� ������ */
    level int(11) default 1,
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;
alter table sop_mlm add index sop_mlm_parent_id_idx (parent_id);
alter table sop_mlm add index sop_mlm_client_id_idx (client_id);

drop table if exists sop_orders;
create table sop_orders (
    id int(11) not null auto_increment,
    client_id int(11) not null,
    sum decimal(5,2) default 0.0,
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;
alter table sop_orders add index sop_orders_client_id_idx (client_id);

insert into sop_orders (id,client_id,sum,created_at) values (1,1,200,'2009-05-01 10:00');
insert into sop_orders (id,client_id,sum,created_at) values (2,2,100,'2009-05-01 10:00');
insert into sop_orders (id,client_id,sum,created_at) values (3,3,50,'2009-05-01 10:00');
insert into sop_orders (id,client_id,sum,created_at) values (4,4,200,'2009-05-01 10:00');
insert into sop_orders (id,client_id,sum,created_at) values (5,5,120,'2009-05-01 10:00');
insert into sop_orders (id,client_id,sum,created_at) values (6,6,100,'2009-05-01 10:00');
insert into sop_orders (id,client_id,sum,created_at) values (7,7,200,'2009-05-01 10:00');
insert into sop_orders (id,client_id,sum,created_at) values (8,8,100,'2009-05-01 10:00');
insert into sop_orders (id,client_id,sum,created_at) values (9,9,50,'2009-05-01 10:00');
insert into sop_orders (id,client_id,sum,created_at) values (10,10,40,'2009-05-01 10:00');
insert into sop_orders (id,client_id,sum,created_at) values (11,11,50,'2009-05-01 10:00');
insert into sop_orders (id,client_id,sum,created_at) values (12,12,100,'2009-05-01 10:00');
insert into sop_orders (id,client_id,sum,created_at) values (13,13,200,'2009-05-01 10:00');
insert into sop_orders (id,client_id,sum,created_at) values (14,14,300,'2009-05-01 10:00');
insert into sop_orders (id,client_id,sum,created_at) values (15,15,100,'2009-05-01 10:00');
insert into sop_orders (id,client_id,sum,created_at) values (16,16,60,'2009-05-01 10:00');
insert into sop_orders (id,client_id,sum,created_at) values (17,17,70,'2009-05-01 10:00');
insert into sop_orders (id,client_id,sum,created_at) values (18,18,300,'2009-05-01 10:00');
insert into sop_orders (id,client_id,sum,created_at) values (19,19,100,'2009-05-01 10:00');
insert into sop_orders (id,client_id,sum,created_at) values (20,20,500,'2009-05-01 10:00');
insert into sop_orders (id,client_id,sum,created_at) values (21,21,400,'2009-05-01 10:00');
insert into sop_orders (id,client_id,sum,created_at) values (22,22,300,'2009-05-01 10:00');
insert into sop_orders (id,client_id,sum,created_at) values (23,23,200,'2009-05-01 10:00');
insert into sop_orders (id,client_id,sum,created_at) values (24,24,100,'2009-05-01 10:00');
insert into sop_orders (id,client_id,sum,created_at) values (25,25,250,'2009-05-01 10:00');
insert into sop_orders (id,client_id,sum,created_at) values (26,26,50,'2009-05-01 10:00');
insert into sop_orders (id,client_id,sum,created_at) values (27,27,150,'2009-05-01 10:00');
insert into sop_orders (id,client_id,sum,created_at) values (28,28,200,'2009-05-01 10:00');
insert into sop_orders (id,client_id,sum,created_at) values (29,29,150,'2009-05-01 10:00');
insert into sop_orders (id,client_id,sum,created_at) values (30,30,150,'2009-05-01 10:00');
insert into sop_orders (id,client_id,sum,created_at) values (31,31,200,'2009-05-01 10:00');
insert into sop_orders (id,client_id,sum,created_at) values (32,32,100,'2009-05-01 10:00');
insert into sop_orders (id,client_id,sum,created_at) values (33,33,200,'2009-05-01 10:00');

/* ================================================ */
delimiter $$
/* search root nodes */
drop procedure if exists sop_mlm $$
create procedure sop_mlm()
begin
    declare c1_id int;
    declare node_sum decimal(10,2);
    declare c1 cursor for select id from clients where parent_id is null or parent_id=0 order by id;
    declare exit handler for not found begin end;
    
    delete from sop_mlm where year(created_at)=year(now()) and month(created_at)=month(now());
    
    open c1;
    loop
        fetch c1 into c1_id;
        set node_sum = 0;
        call sop_mlm_calc_tree( c1_id, 1, 1, node_sum );
        select c1_id,node_sum;
    end loop;
    close c1;
    
end $$
delimiter ;

delimiter $$
/* process a tree of the nodes from local root to lowest node */
drop procedure if exists sop_mlm_calc_tree $$
create procedure sop_mlm_calc_tree( in node_id int, in node_level int, in node_global_level int, inout node_sum decimal(10,2) )
begin
    declare c1_id, c1_parent_id, mlm_id int;
    declare node_sum2 decimal(10,2);
    declare c1 cursor for select id,parent_id from clients where parent_id=node_id order by id;
    declare exit handler for not found begin
        if mlm_id > 0 then
            update sop_mlm set own_orders_sum=(select sum(sum) from sop_orders where client_id=node_id group by sum) where id=mlm_id;
            update sop_mlm set sum=own_orders_sum+node_sum where id=mlm_id;
        end if;
    
    /*
        if c1_id is null then
            ��������� ���� � �����
        end if;
    */
    end;
    
    if node_level = 1 then
        insert into sop_mlm (client_id,created_at,branches) values (node_id,now(),(select count(*) from clients where parent_id=node_id));
        set mlm_id = (select last_insert_id());
    end if;
    
    open c1;
    loop
        fetch c1 into c1_id,c1_parent_id;
        set node_sum2 = 0;
        call sop_mlm_calc_subtree( c1_id, 1, node_level+1, node_sum2 );
        if node_level < 10 then /* ��������� ������ �� ������ ������� ���� */
            set node_sum = node_sum + (select sum(sum) from sop_orders where client_id=c1_id group by sum);
            select node_id,c1_id,node_sum;
            call sop_mlm_calc_tree( c1_id, node_level+1, node_level+1, node_sum );
        end if;
    end loop;
    close c1;
    
end $$
delimiter ;

delimiter $$
/* process a tree of the nodes from local root to lowest node */
drop procedure if exists sop_mlm_calc_subtree $$
create procedure sop_mlm_calc_subtree( in node_id int, in node_level int, in node_global_level int, inout node_sum decimal(10,2) )
begin
    declare c1_id, c1_parent_id, mlm_id int;
    declare node_sum2 decimal(10,2);
    declare c1 cursor for select id,parent_id from clients where parent_id=node_id order by id;
    declare exit handler for not found begin
        if mlm_id > 0 then
            update sop_mlm set own_orders_sum=(select sum(sum) from sop_orders where client_id=node_id group by sum) where id=mlm_id;
            update sop_mlm set sum=own_orders_sum+node_sum,parent_id=(select parent_id from clients where id=node_id) where id=mlm_id;
        end if;
    /*
        if c1_id is null then
            ��������� ���� � �����
        end if;
    */
    end;
    
    if node_level = 1 then
        insert into sop_mlm (client_id,created_at,branches,level) values (node_id,now(),(select count(*) from clients where parent_id=node_id),node_global_level);
        set mlm_id = (select last_insert_id());
    end if;
    
    open c1;
    loop
        fetch c1 into c1_id,c1_parent_id;
        if node_level < 10 then /* ��������� ������ �� ������ ������� ���� */
            set node_sum = node_sum + (select sum(sum) from sop_orders where client_id=c1_id group by sum);
            call sop_mlm_calc_subtree( c1_id, node_level+1, node_level+1, node_sum );
        end if;
    end loop;
    close c1;
    
end $$
delimiter ;

delimiter $$
drop procedure if exists sop_mlm_total_earns $$
create procedure sop_mlm_total_earns()
begin
    declare c1_sum,c2_sum,c3_sum,c4_sum,c5_sum int;
    declare c1 cursor for select sum(sum)*0.03 from sop_mlm where own_orders_sum>=100 and own_orders_sum<200;
    declare c2 cursor for select sum(sum)*0.05 from sop_mlm where own_orders_sum>=200 and own_orders_sum<300;
    declare c3 cursor for select sum(sum)*0.08 from sop_mlm where own_orders_sum>=300 and own_orders_sum<400;
    declare c4 cursor for select sum(sum)*0.12 from sop_mlm where own_orders_sum>=400;
    declare c5 cursor for select sum(sum) from sop_mlm where parent_id is null;
    declare exit handler for not found begin end;
    
    open c1; fetch c1 into c1_sum; close c1;
    open c2; fetch c2 into c2_sum; close c2;
    open c3; fetch c3 into c3_sum; close c3;
    open c4; fetch c4 into c4_sum; close c4;
    open c5; fetch c5 into c5_sum; close c5;
    
    select c1_sum as '3%',c2_sum as '5%',c3_sum as '8%',c4_sum as '12%',(c1_sum+c2_sum+c3_sum+c4_sum) as 'total %',c5_sum as 'total',(c1_sum+c2_sum+c3_sum+c4_sum)/c5_sum*100 as '%';
    
end $$
delimiter ;

delimiter $$
drop procedure if exists sop_mlm_total_earns2 $$
create procedure sop_mlm_total_earns2()
begin
    declare c1_sum,c2_sum,c3_sum,c4_sum,c5_sum int;
    declare c1 cursor for select sum(sum)*0.03 from sop_mlm where own_orders_sum>=100 and own_orders_sum<200;
    declare c2 cursor for select sum(sum)*0.05 from sop_mlm where own_orders_sum>=200 and own_orders_sum<300;
    declare c3 cursor for select sum(sum)*0.07 from sop_mlm where own_orders_sum>=300 and own_orders_sum<400;
    declare c4 cursor for select sum(sum)*0.09 from sop_mlm where own_orders_sum>=400;
    declare c5 cursor for select sum(sum) from sop_mlm where parent_id is null;
    declare exit handler for not found begin end;
    
    open c1; fetch c1 into c1_sum; close c1;
    open c2; fetch c2 into c2_sum; close c2;
    open c3; fetch c3 into c3_sum; close c3;
    open c4; fetch c4 into c4_sum; close c4;
    open c5; fetch c5 into c5_sum; close c5;
    
    select c1_sum as '3%',c2_sum as '5%',c3_sum as '7%',c4_sum as '9%',(c1_sum+c2_sum+c3_sum+c4_sum) as 'total %',c5_sum as 'total',(c1_sum+c2_sum+c3_sum+c4_sum)/c5_sum*100 as '%';
    
end $$
delimiter ;

/* ================================================ */


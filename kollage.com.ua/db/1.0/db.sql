
/* ================================== Common =============================== */
drop table if exists dictionary;
create table dictionary (
    id int(11) not null auto_increment,
    folder varchar(50),
    parent_id int(11),
    name varchar(100),
    value MEDIUMTEXT,
    lang varchar(10),
    primary key (id)
) DEFAULT CHARSET=utf8;
alter table dictionary add index dictionary_folder_idx (folder);
alter table dictionary add index dictionary_parent_id_idx (parent_id);
alter table dictionary add index dictionary_name_idx (name);
alter table dictionary add index dictionary_lang_idx (lang);

drop table if exists settings;
create table settings (
    id int(11) not null auto_increment,
    tax int(11) default 20,
    use_tax_in_sop int(11) default 0,
    use_tax_in_shop int(11) default 0,
    use_tax_in_fair int(11) default 0,
    /* activation_mail_subject */
    /* activation_mail_body */
    /* register_mail_subject */
    /* register_mail_body */
    /* register_mail2admin_subject */
    /* register_mail2admin_body */
    /* password_reminder_mail_subject */
    /* password_reminder_mail_body */
    /* contacts_at_top */
    from_name varchar(100),
    from_email varchar(100),
    service_file_price varchar(100),
    service_file_price_opt varchar(100),
    service_file_price_opt2 varchar(100),
    primary key (id)
) DEFAULT CHARSET=utf8;
insert into settings (id) values (1);

drop table if exists currencies;
create table currencies (
    id int(11) not null auto_increment,
    name varchar(50),
    symbol varchar(10),
    exchange_rate float,
    code int(11), /* http://www.iso.org/iso/support/faqs/faqs_widely_used_standards/widely_used_standards_other/currency_codes/currency_codes_list-1.htm */
    position int(11) default 1,
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;

drop table if exists cron;
create table cron (
    id int(11) not null auto_increment,
    last_start int(11) default 0, /* Unix timestamp in seconds, GMT */
    period int(11), /* in seconds */
    status int(11) default 1, /* 1/0 */
    script varchar(100),
    primary key (id)
) DEFAULT CHARSET=utf8;
insert into cron (script,period) values ('clear_tmp_orders.php',3600);

drop table if exists users;
create table users (
    id int(11) not null auto_increment,
    name varchar(255),
    email varchar(255),
    login varchar(255),
    pwd varchar(255),
    position int(11),
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;
-- login/pwd = 1d23
insert into users (name,login,pwd) values ('Administrator','admin','97d8d6e248de2b2945c79dadf0e78aa0');

drop table if exists roles;
create table roles (
    id int(11) not null auto_increment,
    name varchar(255),
    code varchar(255),
    
    /* description */
    
    position int(11),
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;
insert into roles (name,code) values ('articles','articles');
insert into roles (name,code) values ('sop','sop');
insert into roles (name,code) values ('shop_orders','shop_orders');
insert into roles (name,code) values ('shop_products','shop_products');
insert into roles (name,code) values ('clients','clients');
insert into roles (name,code) values ('banners','banners');
insert into roles (name,code) values ('settings','settings');
insert into roles (name,code) values ('currencies','currencies');
insert into roles (name,code) values ('streets','streets');
insert into roles (name,code) values ('users','users');
insert into roles (name,code) values ('fair','fair');

drop table if exists user_roles;
create table user_roles (
    id int(11) not null auto_increment,
    user_id int(11),
    role_id int(11),
    primary key (id)
) DEFAULT CHARSET=utf8;

drop table if exists unique_visitors;
create table unique_visitors (
    id int(11) not null auto_increment,
    ipaddress varchar(20),
    hostname varchar(255),
    hits int(11) default 1,
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;

drop table if exists categories;
create table categories (
    id int(11) not null auto_increment,
    category_id int(11),
    folder varchar(50),
    /* name */
    /* text */
    design int(11) default 1,
    position int(11) default 1,
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;
insert into categories (folder) values ('services');
insert into categories (folder) values ('news');
insert into categories (folder) values ('photos');
insert into categories (folder) values ('shop');
insert into categories (folder) values ('fair');
alter table categories add index categories_folder_idx (folder);
alter table categories add index categories_category_id_idx (category_id);

drop table if exists articles;
create table articles (
    id int(11) not null auto_increment,
    category_id int(11),
    parent_id int(11),
    process_id int(11),
    folder varchar(50),
    /* header */
    /* ingress */
    /* text */
    publish_date datetime,
    image varchar(255),
    design_type int(11) default 1,
    status int(11) default 1,
    show_in_menu int(11) default 1,
    /* images */
    position int(11) default 1,
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;
alter table articles add index articles_folder_idx (folder);
alter table articles add index articles_category_id_idx (category_id);
insert into articles (folder) values('contact');
insert into articles (folder) values('service_anounces_right');

/*
    folder = {article|work}
    url - ��� �������� ������ �� �����������
    filename - ������������ �����������
    small, thumbnail - ������������ �� filename, ���� �� �����������
*/
drop table if exists images;
create table images (
    id int(11) not null auto_increment,
    folder varchar(50),
    tag varchar(50),
    parent_id int(11),
    name varchar(255),
    author varchar(255),
    url varchar(255),
    description text,
    filename varchar(255),
    small varchar(255),
    tiny varchar(255),
    thumbnail varchar(255),
    video_flv varchar(255),
    video_mov varchar(255),
    type varchar(10) default 'unknown',
    publish_date datetime,
    position int(11) default 1,
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;
alter table images add index images_folder_idx (folder);
alter table images add index images_parent_id_idx (parent_id);

drop table if exists clients;
create table clients (
    id int(11) not null auto_increment,
    parent_id int(11),
    type_id int(11) default 1,
    street_id int(11),
    allow_fair int(11) default 0,
    professional int(11) default 0,
    firstname varchar(100),
    lastname varchar(100),
    lastname2 varchar(100),
    description text,
    email varchar(100) not null unique,
    password varchar(100),
    city varchar(50),
    zip varchar(10),
    address varchar(255),
    office varchar(255),
    phone varchar(20),
    bdate datetime,
    status int(11) default 1, /* 1-needs authorization; 2-active; 0-inactive; 3-order process is denied */
    send_news int(11) default 0,
    purse decimal(6,2) default 0.00,
    photo_discount int(11) default 0,
    printbook_discount int(11) default 0,
    position int(11) default 1,
    activation_key varchar(255),
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;
alter table clients add index clients_email_idx (email);
alter table clients add index clients_type_id_idx (type_id);
alter table clients add index clients_street_id_idx (street_id);
alter table clients add index clients_status_idx (status);
alter table clients add index clients_send_news_idx (send_news);
alter table clients add index clients_parent_id_idx (parent_id);

drop table if exists purse_operations;
create table purse_operations (
    id int(11) not null auto_increment,
    client_id int(11),
    amount decimal(6,2) default 0.00,
    description varchar(255),
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;
alter table purse_operations add index purse_operations_client_id_idx (client_id);

drop table if exists client_types;
create table client_types (
    id int(11) not null auto_increment,
    /* name: �������, ����������, ������� */
    position int(11) default 1,
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;
insert into client_types (id) values (1); /*�������*/
insert into client_types (id) values (2); /*�������*/
insert into client_types (id) values (3); /*������� �������*/
insert into client_types (id) values (4); /*��������� �����*/

drop table if exists order_numbers;
create table order_numbers (
    id int(11) not null auto_increment,
    order_number int(11),
    primary key (id)
) DEFAULT CHARSET=utf8;
insert into order_numbers (order_number) values(0);
insert into order_numbers (order_number) values(0);
insert into order_numbers (order_number) values(0);

drop table if exists banners;
create table banners (
    id int(11) not null auto_increment,
    category_id int(11),
    folder varchar(50),
    /* name */
    /* description */
    url varchar(255),
    image varchar(255),
    image_width int(11),
    image_height int(11),
    image_type int(11),
    position int(11) default 1,
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;

drop table if exists regions;
create table regions (
    id int(11) not null auto_increment,
    folder varchar(50),
    /* name */
    /* description */
    image varchar(255),
    image_width int(11),
    image_height int(11),
    position int(11) default 1,
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;

drop table if exists streets;
create table streets (
    id int(11) not null auto_increment,
    region_id int(11),
    /* name */
    /* description */
    square varchar(10),
    image varchar(255),
    image_width int(11),
    image_height int(11),
    position int(11) default 1,
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;
alter table streets add index streets_region_id_idx (region_id);

drop table if exists pos_operations;
create table pos_operations (
    id int(11) not null auto_increment,
    order_number varchar(50) not null,
    transaction_number varchar(100) unique not null,
    bill_number varchar(20) not null,
    payed decimal(5,2) default 0.0,
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;

drop table if exists cards_operations;
create table cards_operations (
    id int(11) not null auto_increment,
    order_number varchar(50) not null,
    xid varchar(50), /* ������������� ���������� */
    rrn varchar(20), /* ������������� ���������� � ������������� ����� */
    payed decimal(5,2) default 0.0,
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;

/* /Common */

/* =================================== System of ordering the photos =================================== */
drop table if exists sop_settings;
create table sop_settings (
    id int(11) not null auto_increment,
    
    /* order_mail_subject */
    /* order_mail_body */
    
    /* order_mail_printed_subject */
    /* order_mail_printed_body */
    
    admin_emails varchar(255),
    /* admin_order_mail_subject */
    /* admin_order_mail_body */
    
    dont_use_discount int(11) default 0, /* 0-������������ ������� ������: 1-�� ������������ ������ */
    
    primary key (id)
) DEFAULT CHARSET=utf8;
insert into sop_settings (id) values(1);

drop table if exists sop_discounts;
create table sop_discounts (
    id int(11) not null auto_increment,
    client_type_id int(11) not null,
    /* name */
    discount float default 0,
    position int(11) default 1,
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;

/* The ways of delivery */
drop table if exists sop_deliveries;
create table sop_deliveries (
    id int(11) not null auto_increment,
    /* name */
    price decimal(6,2),
    order_prefix varchar(20),
    status int(11) default 1, /* 0-deleted; 1-active */
    position int(11) default 1,
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;

/* The way of payment */
drop table if exists sop_paymentways;
create table sop_paymentways (
    id int(11) not null auto_increment,
    /* name */
    /* description */
    status int(11) default 1, /* 0-deleted; 1-active */
    code int(11) default 1,
    discount int(11) default 0,
    position int(11) default 1,
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;

/* ����� ����� ��������� �������� � ������ */
drop table if exists sop_deliveries_paymentways;
create table sop_deliveries_paymentways (
    id int(11) not null auto_increment,
    delivery_id int(11) not null,
    paymentway_id int(11) not null,
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;

drop table if exists sop_points;
create table sop_points (
    id int(11) not null auto_increment,
    /* name */
    /* address */
    order_prefix varchar(20),
    phone varchar(20),
    email varchar(255),
    status int(11) default 1, /* 0-deleted; 1-active */
    position int(11) default 1,
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;

drop table if exists sop_order_photos_sizes;
create table sop_order_photos_sizes (
    id int(11) not null auto_increment,
    name varchar(50),
    price decimal(6,2),
    weight decimal(6,2),
    
    /* description */
    
    status int(11) default 1, /* 0-deleted; 1-active */
    position int(11) default 1,
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;

drop table if exists sop_orders;
create table sop_orders (
    id int(11) not null auto_increment,
    client_id int(11) not null,
    order_number varchar(50) unique not null,
    point_id int(11) default 0,
    paymentway_id int(11) default 0,
    delivery_id int(11) default 0,
    address varchar(255), /* ����� ��������, ���� ��� ���������� �� ������ � ���������� */
    comments text,
    discount decimal(5,2) default 0.0,
    payed decimal(5,2) default 0.0,
    tax int(11) default 0,
    status int(11) default 0, /* 0 - ���������, �������������; 1 - ������; 2 - �������� �� �����������; 3-� ��������; 4 - ��������� */
    photo_paper int(11) default 1, /* 1-���������;2-������� */
    border int(11) default 0, /* 0-����� ���; 1-����� ���� */
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;
alter table sop_orders add index sop_orders_client_id_idx (client_id);
alter table sop_orders add index sop_orders_status_idx (status);
alter table sop_orders add index sop_orders_delivery_id_idx (delivery_id);
alter table sop_orders add index sop_orders_paymentway_id_idx (paymentway_id);
alter table sop_orders add index sop_orders_point_id_idx (point_id);
alter table sop_orders add index sop_orders_order_number_idx (order_number);
alter table sop_orders add index sop_orders_created_at_idx (created_at);

drop table if exists sop_order_photos;
create table sop_order_photos (
    id int(11) not null auto_increment,
    order_id int(11) not null,
    price decimal(6,2),
    size_id int(11) not null,
    quantity int(11) default 1,
    thumbnail varchar(255), /* 120x90 */
    small varchar(255), /* fitted in 800x600 */
    small_w int(11),
    small_h int(11),
    filename varchar(255),
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;
alter table sop_order_photos add index sop_order_photos_order_id_idx (order_id);
alter table sop_order_photos add index sop_order_photos_size_id_idx (size_id);
/* /Sysytem of ordering the photos */

/* =================================== Shop ===================================== */
drop table if exists shop_settings;
create table shop_settings (
    id int(11) not null auto_increment,
    /* order_mail_subject */
    /* order_mail_body */
    
    admin_emails varchar(255),
    /* admin_order_mail_subject */
    /* admin_order_mail_body */
    
    file_price varchar(100),
    file_price_opt varchar(100),
    file_price_opt2 varchar(100),
    
    /* ������� %, ������� */ 
    markup_940 decimal(5,2), /* >=$940 */
    markup_500 decimal(5,2), /* >=$500 */
    markup_100 decimal(5,2), /* >=$100 */
    markup_10 decimal(5,2), /* >=$10 */
    markup_1 decimal(5,2), /* >=$1 */
    markup_0 decimal(5,2), /* >=$0 */
    
    /* ������� %, ��� */ 
    markup_opt_940 decimal(5,2), /* ������� >=$940 */
    markup_opt_500 decimal(5,2), /* ������� >=$500 */
    markup_opt_100 decimal(5,2), /* ������� >=$100 */
    markup_opt_10 decimal(5,2), /* ������� >=$10 */
    markup_opt_1 decimal(5,2), /* ������� >=$1 */
    markup_opt_0 decimal(5,2), /* ������� >=$0 */
    
    /* ������� %, ������� ��� */ 
    markup_opt2_940 decimal(5,2), /* ������� >=$940 */
    markup_opt2_500 decimal(5,2), /* ������� >=$500 */
    markup_opt2_100 decimal(5,2), /* ������� >=$100 */
    markup_opt2_10 decimal(5,2), /* ������� >=$10 */
    markup_opt2_1 decimal(5,2), /* ������� >=$1 */
    markup_opt2_0 decimal(5,2) /* ������� >=$0 */
    
    primary key (id)
) DEFAULT CHARSET=utf8;
insert into shop_settings (id) values(1);

drop table if exists shop_product_categories;
create table shop_product_categories (
    id int(11) not null auto_increment,
    parent_id int(11),
    link_to_category_id int(11),
    folder varchar(50),
    /* name */
    /* description */
    position int(11) default 1,
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;
alter table shop_product_categories add index shop_product_categories_parent_id_idx (parent_id);

drop table if exists shop_features;
create table shop_features (
    id int(11) not null auto_increment,
    category_id int(11),
    folder varchar(50),
    /* name */
    /* description */
    match_analog int(11) default 0, /* 1-������������ � ������ �������, 0-��� */
    show_in_short_description int(11) default 0,
    status int(11) default 1, /* 1-active, 0-inactive */
    position int(11) default 1,
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;
alter table shop_features add index shop_features_category_id_idx (category_id);

drop table if exists shop_feature_values;
create table shop_feature_values (
    id int(11) not null auto_increment,
    feature_id int(11),
    folder varchar(50),
    /* name */
    /* description */
    status int(11) default 1, /* 1-active, 0-inactive */
    position int(11) default 1,
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;
alter table shop_feature_values add index shop_feature_values_feature_id_idx (feature_id);

drop table if exists shop_product_features;
create table shop_product_features (
    id int(11) not null auto_increment,
    product_id int(11),
    feature_value_id int(11),
    position int(11) default 1,
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;
alter table shop_product_features add index shop_product_features_product_id_idx (product_id);
alter table shop_product_features add index shop_product_features_feature_value_id_idx (feature_value_id);

drop table if exists shop_products;
create table shop_products (
    id int(11) not null auto_increment,
    category_id int(11),
    brand_id int(11),
    /* name */
    /* ingress */
    /* description */
    number varchar(50),
    price decimal(6,2) default 0.0,
    price_opt decimal(6,2) default 0.0,
    price_opt2 decimal(6,2) default 0.0,
    nice_price decimal(6,2) default 0.0,
    nice_price_start_at datetime,
    nice_price_end_at datetime,
    status int(11) default 1, /* 1-active, 0-inactive */
    /* images */
    /* features */
    show_in_shop_news int(11) default 0,
    show_in_best int(11) default 0,
    design_type int(11) default 1,
    position int(11) default 1,
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;
alter table shop_products add index shop_products_category_id_idx (category_id);
alter table shop_products add index shop_products_brand_id_idx (brand_id);
alter table shop_products add index shop_products_show_in_shop_news_idx (show_in_shop_news);
alter table shop_products add index shop_products_show_in_best_idx (show_in_best);
alter table shop_products add index shop_products_status_idx (status);
alter table shop_products add index shop_products_number_idx (number);
alter table shop_products add index shop_products_price_idx (price);
alter table shop_products add index shop_products_created_at_idx (created_at);
alter table shop_products add index shop_products_updated_at_idx (updated_at);

/* ������������� ������� */
drop table if exists shop_brands;
create table shop_brands (
    id int(11) not null auto_increment,
    /* name */
    /* description */
    image varchar(255),
    status int(11) default 1, /* 0-deleted; 1-active */
    position int(11) default 1,
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;
alter table shop_brands add index shop_brands_position_idx (position);

/* ������� ������ �� ������ ������� */
drop table if exists shop_product_availability;
create table shop_product_availability (
    id int(11) not null auto_increment,
    product_id int(11),
    point_id int(11),
    quantity int(11) default 0,
    position int(11) default 1,
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;
alter table shop_product_availability add index shop_product_availability_product_id_idx (product_id);
alter table shop_product_availability add index shop_product_availability_point_id_idx (point_id);
alter table shop_product_availability add index shop_product_availability_quantity_idx (quantity);

/* ������ */
drop table if exists shop_points;
create table shop_points (
    id int(11) not null auto_increment,
    group_id int(11) default 1, /* 1 - Sumy; 2 - external */
    /* name */
    /* address */
    order_prefix varchar(20),
    phone varchar(20),
    email varchar(255),
    status int(11) default 1, /* 0-deleted; 1-active */
    position int(11) default 1,
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;
alter table shop_points add index shop_points_group_id_idx (group_id);
alter table shop_points add index shop_points_status_idx (status);

/* ������������ ����� ������� ������� ������� � ����������� ������ */
drop table if exists shop_point_codes;
create table shop_point_codes (
    id int(11) not null auto_increment,
    point_id int(11) not null,
    number_external varchar(255),
    number_internal varchar(255),
    primary key (id)
) DEFAULT CHARSET=utf8;
alter table shop_point_codes add index shop_point_codes_point_id_idx (point_id);

drop table if exists shop_point_settings;
create table shop_point_settings (
    id int(11) not null auto_increment,
    point_id int(11) not null,
    number_column int(11) default 8,
    price_column int(11) default 6,
    name_column int(11) default 2,
    quantity_column int(11) default 4,
    primary key (id)
) DEFAULT CHARSET=utf8;

/* The ways of delivery */
drop table if exists shop_deliveries;
create table shop_deliveries (
    id int(11) not null auto_increment,
    /* name */
    price decimal(6,2),
    order_prefix varchar(20),
    status int(11) default 1, /* 0-deleted; 1-active */
    position int(11) default 1,
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;

/* The way of payment */
drop table if exists shop_paymentways;
create table shop_paymentways (
    id int(11) not null auto_increment,
    /* name */
    /* description */
    code int(11) default 1,
    status int(11) default 1, /* 0-deleted; 1-active */
    position int(11) default 1,
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;

drop table if exists shop_discounts;
create table shop_discounts (
    id int(11) not null auto_increment,
    client_type_id int(11) not null,
    /* name */
    discount float default 0,
    position int(11) default 1,
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;
alter table shop_discounts add index shop_discounts_client_type_id_idx (client_type_id);

drop table if exists shop_orders;
create table shop_orders (
    id int(11) not null auto_increment,
    client_id int(11) not null,
    order_number varchar(50) unique not null,
    paymentway_id int(11) default 0,
    delivery_id int(11) default 0,
    address varchar(255), /* ����� ��������, ���� ��� ���������� �� ������ � ���������� */
    comments text,
    discount decimal(6,2) default 0.0,
    payed decimal(6,2) default 0.0,
    tax int(11) default 0,
    status int(11) default 0, /* 0 - ���������, �������������; 1 - ������; 2 - ��������; 3 - ��������� */
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;
alter table shop_orders add index shop_orders_client_id_idx (client_id);
alter table shop_orders add index shop_orders_order_number_idx (order_number);
alter table shop_orders add index shop_orders_paymentway_id_idx (paymentway_id);
alter table shop_orders add index shop_orders_delivery_id_idx (delivery_id);
alter table shop_orders add index shop_orders_status_idx (status);

drop table if exists shop_order_products;
create table shop_order_products (
    id int(11) not null auto_increment,
    order_id int(11) not null,
    product_id int(11) not null,
    price decimal(6,2) default 0.0,
    quantity int(11) default 1,
    pexists int(11) default 1,
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;
alter table shop_order_products add index shop_order_products_order_id_idx (order_id);
alter table shop_order_products add index shop_order_product_id_idx (product_id);

/* /Shop */

/* FAIR */
drop table if exists fair_settings;
create table fair_settings (
    id int(11) not null auto_increment,
    
    /* order_mail_subject */
    /* order_mail_body */
    
    admin_emails varchar(255),
    /* admin_order_mail_subject */
    /* admin_order_mail_body */
    
    primary key (id)
) DEFAULT CHARSET=utf8;
insert into fair_settings (id) values(1);

drop table if exists fair_categories;
create table fair_categories (
    id int(11) not null auto_increment,
    parent_id int(11),
    category int(11) default 1, /* type of the category: 1-representative; 2-conceptual */
    folder varchar(50),
    
    /* name */
    /* description */
    
    position int(11) default 1,
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;
alter table fair_categories add index fair_categories_parent_id_idx (parent_id);

drop table if exists fair_objects;
create table fair_objects (
    id int(11) not null auto_increment,
    type int(11) default 1, /* 1-photo; 2-template */
    category_id1 int(11),
    category_id2 int(11),
    client_id int(11),
    country_id int(11),
    views int(11) default 0,
    buyings int(11) default 0,
    rating_view int(11) default 0,
    rating_buy int(11) default 0,
    with_processing int(11) default 0, /* 0-without processing; 1-with processing */
    license int(11) default 0, /* 0-no license, 1-allow */
    license_price decimal(6,2) default 0.0,
    keywords varchar(255),
    
    /* title */
    /* description */
    
    filename varchar(255),
    filename_w int(11),
    filename_h int(11),
    
    thumbnail varchar(255), /* fit in 110x110px */
    thumbnail_w int(11),
    thumbnail_h int(11),
    
    preview varchar(255), /* fit in 400x400px */
    preview_w int(11),
    preview_h int(11),
    
    xs varchar(255), /* 0-500px */
    xs_w int(11),
    xs_h int(11),
    xs_price decimal(6,2) default 0.0,
    
    s varchar(255), /* 500-1000px */
    s_w int(11),
    s_h int(11),
    s_price decimal(6,2) default 0.0,
    
    m varchar(255), /* 1000-2000px */
    m_w int(11),
    m_h int(11),
    m_price decimal(6,2) default 0.0,
    
    l varchar(255), /* 2000-3000px */
    l_w int(11),
    l_h int(11),
    l_price decimal(6,2) default 0.0,
    
    xl varchar(255), /* 3000-4000px */
    xl_w int(11),
    xl_h int(11),
    xl_price decimal(6,2) default 0.0,
    
    xxl varchar(255), /* original */
    xxl_w int(11),
    xxl_h int(11),
    xxl_price decimal(6,2) default 0.0,
    
    was_used_software int(11) default 0, /* 0-not used; 1-used */
    software varchar(255),
    photo_make varchar(255),
    photo_model varchar(255),
    photo_resolution int(11), /* dpi */
    photo_iso int(11),
    photo_datetime_original datetime,
    photo_datetime_digitized datetime,
    
    status int(11) default 0, /* 0-wait for accept; 1-accepted */
    position int(11) default 1,
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;
alter table fair_objects add index fair_objects_category_id1_idx (category_id1);
alter table fair_objects add index fair_objects_category_id2_idx (category_id2);
alter table fair_objects add index fair_objects_client_id_idx (client_id);
alter table fair_objects add index fair_objects_rating_view_idx (rating_view);
alter table fair_objects add index fair_objects_rating_buy_idx (rating_buy);
alter table fair_objects add index fair_objects_rating_buy_idx (rating_buy);
alter table fair_objects add index fair_objects_was_used_software_idx (was_used_software);

/* The way of payment */
drop table if exists fair_paymentways;
create table fair_paymentways (
    id int(11) not null auto_increment,
    /* name */
    /* description */
    status int(11) default 1, /* 0-deleted; 1-active */
    code int(11) default 1,
    position int(11) default 1,
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;

drop table if exists fair_orders;
create table fair_orders (
    id int(11) not null auto_increment,
    client_id int(11) not null,
    order_number varchar(50) unique not null,
    paymentway_id int(11) default 0,
    discount decimal(5,2) default 0.0,
    payed decimal(5,2) default 0.0,
    tax int(11) default 0,
    status int(11) default 0, /* 0 - ���������, �������������; 1 - ������; 2 - �������� �� �����������; 3-� ��������; 4 - ��������� */
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;
alter table fair_orders add index fair_orders_client_id_idx (client_id);
alter table fair_orders add index fair_orders_status_idx (status);
alter table fair_orders add index fair_orders_paymentway_id_idx (paymentway_id);
alter table fair_orders add index fair_orders_order_number_idx (order_number);

drop table if exists fair_order_objects;
create table fair_order_objects (
    id int(11) not null auto_increment,
    order_id int(11) not null,
    fair_object_id varchar(255) not null,
    price decimal(6,2) default 0.0,
    object_size int(11) not null, /* 1-xs; 2-s; 3-m; 4-l; 5-xl; 6-xxl; 7-full licence */
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;
alter table fair_order_objects add index fair_order_objects_order_id_idx (order_id);
alter table fair_order_objects add index fair_order_objects_fair_object_id_idx (fair_object_id);
alter table fair_order_objects add index fair_order_objects_fair_object_size_idx (object_size);

drop table if exists fair_documents;
create table fair_documents (
    id int(11) not null auto_increment,
    client_id int(11),
    
    name varchar(255),
    filename varchar(255),
    /* description */
    
    status int(11) default 0,
    position int(11) default 1,
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;
alter table fair_documents add index fair_documents_client_id_idx (client_id);
alter table fair_documents add index fair_documents_position_idx (position);

drop table if exists fair_object_documents;
create table fair_object_documents (
    id int(11) not null auto_increment,
    fair_object_id int(11) not null,
    fair_document_id int(11) not null,
    status int(11) default 0,
    position int(11) default 1,
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;
alter table fair_object_documents add index fair_object_documents_fair_object_id_idx (fair_object_id);
alter table fair_object_documents add index fair_object_documents_fair_document_id_idx (fair_document_id);

/* /FAIR */

drop table if exists posterminal_operations;
create table posterminal_operations (
    id int(11) not null auto_increment,
    point_id int(11),
    type int(11) default 0, /* 1-sop; 2-shop; 3-fair */
    client_id int(11),
    amount decimal(6,2) default 0.00,
    description varchar(255),
    transaction_id varchar(255),
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;
alter table posterminal_operations add index posterminal_operations_client_id_idx (client_id);
alter table posterminal_operations add index posterminal_operations_type_idx (type);
insert into posterminal_operations (client_id,amount,description,created_at) values (0,0,'first record to calculate',now());

drop table if exists userfiles;
create table userfiles (
    id int(11) not null auto_increment,
    client_id int(11),
    
    name varchar(255),
    filename varchar(255),
    description text,
    
    status int(11) default 0,
    position int(11) default 1,
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;
alter table userfiles add index userfiles_client_id_idx (client_id);
alter table userfiles add index userfiles_position_idx (position);


/*
    Photo book (pb)
*/

SET NAMES utf8;

/* Тип фотокниги: выпускана, классическая, ... */
drop table if exists pb2_types;
create table pb2_types (
    id int(11) not null auto_increment,
    
    name varchar(255),
    image varchar(255),
    personal_covers int(11) default 1, /* возможна персонализация обложки? */
    personal_pages int(11) default 1, /* возможна персонализация страниц? */
    box int(11) default 1, /* возможен бокс для фотокниги? */
    
    status int(11) default 1,
    position int(11) default 1,
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;
alter table pb2_types add index pb2_types_position_idx (position);
alter table pb2_types add index pb2_types_status_idx (status);

/* Размеры */
drop table if exists pb2_sizes;
SET character_set_client = utf8;
create table pb2_sizes (
    id int(11) not null auto_increment,
    
    name varchar(255),
    price_base numeric(10,2) not null default 0,
    price_per_page numeric(10,2) not null default 0,
    price_personal_page numeric(10,2) default 0,
    price_personal_cover numeric(10,2) default 0,
    price_design_work numeric(10,2) default 0, /* работа дизайнера за страницу */
    start_page int(11) default 12, /* стартовое количество разворотов */
    page_block int(11) default 2, /* страниц в развороте */
    
    discount1 numeric(10,2) default 0, /* тираж 3..4 */
    discount2 numeric(10,2) default 0, /* тираж 5..10 */
    discount3 numeric(10,2) default 0, /* тираж 10.. */
    
    status int(11) default 1,
    position int(11) default 1,
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;
alter table pb2_sizes add index pb2_sizes_position_idx (position);
alter table pb2_sizes add index pb2_sizes_status_idx (status);

insert into pb2_sizes (id,position,name,price_base,price_per_page,start_page,page_block,created_at) values (1,1,'20x30 см',70,4,12,4,now());
insert into pb2_sizes (id,position,name,price_base,price_per_page,start_page,page_block,created_at) values (2,2,'30x30 см',150,5,16,2,now());
insert into pb2_sizes (id,position,name,price_base,price_per_page,start_page,page_block,created_at) values (3,3,'30x20 см',83,4,16,4,now());
insert into pb2_sizes (id,position,name,price_base,price_per_page,start_page,page_block,created_at) values (4,4,'20x20 см',69,4,12,4,now());
insert into pb2_sizes (id,position,name,price_base,price_per_page,start_page,page_block,created_at) values (5,5,'20x15 см',52,3,12,4,now());
insert into pb2_sizes (id,position,name,price_base,price_per_page,start_page,page_block,created_at) values (6,6,'15x20 см',52,3,12,4,now());

/* фотобумага */
drop table if exists pb2_papers;
create table pb2_papers (
    id int(11) not null auto_increment,
    pb2_size_id int(11) not null,
    
    name varchar(255),
    price numeric(10,2) not null default 0,
    
    status int(11) default 1,
    position int(11) default 1,
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;
alter table pb2_papers add index pb2_papers_position_idx (position);
alter table pb2_papers add index pb2_papers_status_idx (status);
alter table pb2_papers add index pb2_papers_pb2_size_id_idx (pb2_size_id);

/* Технология сборки: пластиковая, картонная */
drop table if exists pb2_make_tech;
create table pb2_make_tech (
    id int(11) not null auto_increment,
    pb2_size_id int(11) not null,
    
    name varchar(255),
    price numeric(10,2) not null default 0,
    
    status int(11) default 1,
    position int(11) default 1,
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;
alter table pb2_make_tech add index pb2_make_tech_position_idx (position);
alter table pb2_make_tech add index pb2_make_tech_status_idx (status);
alter table pb2_make_tech add index pb2_make_tech_pb2_size_id_idx (pb2_size_id);

/* Материал обложки: кожзам, ... */
drop table if exists pb2_covers;
create table pb2_covers (
    id int(11) not null auto_increment,
    pb2_size_id int(11) not null,
    
    name varchar(255),
    price numeric(10,2) not null default 0,
    image varchar(255),
    
    status int(11) default 1,
    position int(11) default 1,
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;
alter table pb2_covers add index pb2_covers_position_idx (position);
alter table pb2_covers add index pb2_covers_status_idx (status);
alter table pb2_covers add index pb2_covers_pb2_size_id_idx (pb2_size_id);

/* Материал форзаца: картон, ... */
drop table if exists pb2_flyleafs;
create table pb2_flyleafs (
    id int(11) not null auto_increment,
    pb2_size_id int(11) not null,
    
    name varchar(255),
    price numeric(10,2) not null default 0,
    image varchar(255),
    
    status int(11) default 1,
    position int(11) default 1,
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;
alter table pb2_flyleafs add index pb2_flyleafs_position_idx (position);
alter table pb2_flyleafs add index pb2_flyleafs_status_idx (status);
alter table pb2_flyleafs add index pb2_flyleafs_pb2_size_id_idx (pb2_size_id);

/* Бокс: классический, ... */
drop table if exists pb2_boxes;
create table pb2_boxes (
    id int(11) not null auto_increment,
    pb2_size_id int(11) not null,
    
    name varchar(255),
    price numeric(10,2) not null default 0,
    image varchar(255),
    
    status int(11) default 1,
    position int(11) default 1,
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;
alter table pb2_boxes add index pb2_boxes_position_idx (position);
alter table pb2_boxes add index pb2_boxes_status_idx (status);
alter table pb2_boxes add index pb2_boxes_pb2_size_id_idx (pb2_size_id);

/* Акцессуары: шильд, ... */
drop table if exists pb2_accessories;
create table pb2_accessories (
    id int(11) not null auto_increment,
    pb2_size_id int(11) not null,
    
    name varchar(255),
    price numeric(10,2) not null default 0,
    image varchar(255),
    
    status int(11) default 1,
    position int(11) default 1,
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;
alter table pb2_accessories add index pb2_accessories_position_idx (position);
alter table pb2_accessories add index pb2_accessories_status_idx (status);
alter table pb2_accessories add index pb2_accessories_pb2_size_id_idx (pb2_size_id);

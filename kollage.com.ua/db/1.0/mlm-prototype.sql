
drop table if exists clients;
create table clients (
    id int(11) not null auto_increment,
    parent_id int(11),
    type_id int(11) default 1,
    street_id int(11),
    allow_fair int(11) default 0,
    professional int(11) default 0,
    firstname varchar(100),
    lastname varchar(100),
    lastname2 varchar(100),
    description text,
    email varchar(100) not null unique,
    password varchar(100),
    city varchar(50),
    zip varchar(10),
    address varchar(255),
    office varchar(255),
    phone varchar(20),
    bdate datetime,
    status int(11) default 1, /* 1-needs authorization; 2-active; 0-inactive; 3-order process is denied */
    send_news int(11) default 0,
    purse decimal(6,2) default 0.00,
    photo_discount int(11),
    position int(11) default 1,
    activation_key varchar(255),
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;
alter table clients add index clients_email_idx (email);
alter table clients add index clients_parent_id_idx (parent_id);
alter table clients add index clients_type_id_idx (type_id);
alter table clients add index clients_street_id_idx (street_id);
alter table clients add index clients_status_idx (status);
alter table clients add index clients_send_news_idx (send_news);

insert into clients (id,parent_id,email) values (1,null,'1, level1: 200');
insert into clients (id,parent_id,email) values (2,1,'2-level2: 100');
insert into clients (id,parent_id,email) values (3,2,'3-level3: 50');
insert into clients (id,parent_id,email) values (4,2,'4-level3: 200');
insert into clients (id,parent_id,email) values (5,4,'5-level4: 120');
insert into clients (id,parent_id,email) values (6,4,'6-level4: 100');
insert into clients (id,parent_id,email) values (7,6,'7-level5: 200');
insert into clients (id,parent_id,email) values (8,7,'8-level6: 100');
insert into clients (id,parent_id,email) values (9,8,'9-level7: 50');
insert into clients (id,parent_id,email) values (10,7,'10-level6: 40');
insert into clients (id,parent_id,email) values (11,10,'11-level7: 50');
insert into clients (id,parent_id,email) values (12,11,'12-level8: 100');
insert into clients (id,parent_id,email) values (13,12,'13-level9: 200');
insert into clients (id,parent_id,email) values (14,13,'14-level10: 300');
insert into clients (id,parent_id,email) values (15,14,'15-level11: 100');
insert into clients (id,parent_id,email) values (16,15,'16-level12: 60');
insert into clients (id,parent_id,email) values (17,11,'17-level8: 70');
insert into clients (id,parent_id,email) values (18,6,'18-level5: 300');
insert into clients (id,parent_id,email) values (19,18,'19-level6: 100');
insert into clients (id,parent_id,email) values (20,1,'20-level2: 500');
insert into clients (id,parent_id,email) values (21,20,'21-level3: 400');
insert into clients (id,parent_id,email) values (22,21,'22-level4: 300');
insert into clients (id,parent_id,email) values (23,22,'23-level5: 200');
insert into clients (id,parent_id,email) values (24,23,'24-level6: 100');
insert into clients (id,parent_id,email) values (25,23,'25-level6: 250');
insert into clients (id,parent_id,email) values (26,22,'26-level5: 50');
insert into clients (id,parent_id,email) values (27,21,'27-level4: 150');
insert into clients (id,parent_id,email) values (28,27,'28-level5: 200');
insert into clients (id,parent_id,email) values (29,27,'29-level5: 150');
insert into clients (id,parent_id,email) values (30,29,'30-level6: 150');
insert into clients (id,parent_id,email) values (31,20,'31-level3: 200');
insert into clients (id,parent_id,email) values (32,31,'32-level4: 100');
insert into clients (id,parent_id,email) values (33,32,'33-level5: 200');

drop table if exists sop_mlm;
create table sop_mlm (
    id int(11) not null auto_increment,
    parent_id int(11),
    client_id int(11) not null,
    sum decimal(10,2) default 0, /* ����� ������� �� ���� � ����, ������� ����������� ������ */
    own_orders_sum decimal(10,2) default 0, /* ����� ����������� ������� */
    branches int(11) default 0, /* ���������� ����� � ���� */
    percent int(11) default 0, /* ���������� ������ */
    level int(11) default 1,
    own_orders_marked int(11) default 0, /* 1 - ���� ����� ����������� ������� ��� ����������� � �������� */
    earn decimal(10,2) default 0, /* ������������ ����� */
    children int(11) default 0,
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;
alter table sop_mlm add index sop_mlm_parent_id_idx (parent_id);
alter table sop_mlm add index sop_mlm_client_id_idx (client_id);
alter table sop_mlm add index sop_mlm_percent_idx (percent);
alter table sop_mlm add index sop_mlm_level_idx (level);
alter table sop_mlm add index sop_mlm_own_orders_marked_idx (own_orders_marked);
alter table sop_mlm add index sop_mlm_created_at_idx (created_at);

drop table if exists sop_orders;
create table sop_orders (
    id int(11) not null auto_increment,
    client_id int(11) not null,
    sum decimal(5,2) default 0.0,
    created_at datetime,
    updated_at datetime,
    primary key (id)
) DEFAULT CHARSET=utf8;
alter table sop_orders add index sop_orders_client_id_idx (client_id);
alter table sop_orders add index sop_orders_created_at_idx (created_at);

insert into sop_orders (client_id,sum,created_at) values (1,200,'2009-05-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (2,100,'2009-05-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (3,50,'2009-05-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (4,200,'2009-05-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (5,120,'2009-05-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (6,100,'2009-05-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (7,200,'2009-05-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (8,100,'2009-05-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (9,50,'2009-05-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (10,40,'2009-05-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (11,50,'2009-05-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (12,100,'2009-05-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (13,200,'2009-05-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (14,300,'2009-05-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (15,100,'2009-05-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (16,60,'2009-05-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (17,70,'2009-05-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (18,300,'2009-05-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (19,100,'2009-05-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (20,500,'2009-05-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (21,400,'2009-05-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (22,300,'2009-05-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (23,200,'2009-05-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (24,100,'2009-05-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (25,250,'2009-05-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (26,50,'2009-05-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (27,150,'2009-05-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (28,200,'2009-05-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (29,150,'2009-05-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (30,150,'2009-05-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (31,200,'2009-05-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (32,100,'2009-05-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (33,200,'2009-05-01 10:00');

insert into sop_orders (client_id,sum,created_at) values (1,200,'2009-07-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (2,100,'2009-07-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (3,50,'2009-07-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (4,200,'2009-07-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (5,120,'2009-07-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (6,100,'2009-07-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (7,200,'2009-07-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (8,100,'2009-07-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (9,50,'2009-07-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (10,40,'2009-07-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (11,50,'2009-07-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (12,100,'2009-07-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (13,200,'2009-07-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (14,300,'2009-07-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (15,100,'2009-07-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (16,60,'2009-07-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (17,70,'2009-07-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (18,300,'2009-07-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (19,100,'2009-07-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (20,500,'2009-07-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (21,400,'2009-07-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (22,300,'2009-07-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (23,200,'2009-07-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (24,100,'2009-07-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (25,250,'2009-07-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (26,50,'2009-07-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (27,150,'2009-07-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (28,200,'2009-07-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (29,150,'2009-07-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (30,150,'2009-07-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (31,200,'2009-07-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (32,100,'2009-07-01 10:00');
insert into sop_orders (client_id,sum,created_at) values (33,200,'2009-07-01 10:00');

/* ================================================ */
delimiter $$
/* search root nodes */
drop procedure if exists sop_mlm $$
create procedure sop_mlm()
begin
    declare c1_id int;
    declare node_sum decimal(10,2);
    declare c1 cursor for select id from clients where parent_id is null or parent_id=0 order by id;
    declare exit handler for not found begin end;
    
    delete from sop_mlm where year(created_at)=year(now()) and month(created_at)=month(now());
    
    open c1;
    loop
        fetch c1 into c1_id;
        set node_sum = 0;
        call sop_mlm_calc_tree( c1_id, 1, 1, node_sum );
        /*select c1_id,node_sum;*/
    end loop;
    close c1;
    
end $$
delimiter ;

delimiter $$
/* process a tree of the nodes from local root to lowest node */
drop procedure if exists sop_mlm_calc_tree $$
create procedure sop_mlm_calc_tree( in node_id int, in node_level int, in node_global_level int, inout node_sum decimal(10,2) )
begin
    declare c1_id, c1_parent_id, mlm_id int;
    declare node_sum2, node_sum3 decimal(10,2);
    declare c1 cursor for select id,parent_id from clients where parent_id=node_id order by id;
    declare c2 cursor for select sum from sop_mlm where id=mlm_id and year(created_at)=year(now()) and month(created_at)=month(now());
    declare exit handler for not found begin
        if mlm_id > 0 then
            update sop_mlm set own_orders_sum=(select sum(sum) from sop_orders where client_id=node_id and year(created_at)=year(now()) and month(created_at)=month(now()) group by sum) where id=mlm_id;
            update sop_mlm set sum=own_orders_sum+node_sum where id=mlm_id;
            open c2; fetch c2 into node_sum3; close c2;
            if node_sum3 >= 100 and node_sum3 < 200 then
                update sop_mlm set percent=3 where id=mlm_id;
            elseif node_sum3 >= 200 and node_sum3 < 300 then
                update sop_mlm set percent=5 where id=mlm_id;
            elseif node_sum3 >= 300 and node_sum3 < 400 then
                update sop_mlm set percent=7 where id=mlm_id;
            elseif node_sum3 >= 400 then
                update sop_mlm set percent=9 where id=mlm_id;
            end if;
        end if;
    
    /*
        if c1_id is null then
            ��������� ���� � �����
        end if;
    */
    end;
    
    if node_level = 1 then
        insert into sop_mlm (client_id,created_at,branches) values (node_id,now(),(select count(*) from clients where parent_id=node_id));
        set mlm_id = (select last_insert_id());
    end if;
    
    open c1;
    loop
        fetch c1 into c1_id,c1_parent_id;
        set node_sum2 = 0;
        call sop_mlm_calc_subtree( c1_id, 1, node_level+1, node_sum2 );
        set node_sum = node_sum + (select sum(sum) from sop_orders where client_id=c1_id and year(created_at)=year(now()) and month(created_at)=month(now()) group by sum);
        /*select node_id,c1_id,node_sum;*/
        call sop_mlm_calc_tree( c1_id, node_level+1, node_level+1, node_sum );
    end loop;
    close c1;
    
end $$
delimiter ;

delimiter $$
/* process a tree of the nodes from local root to lowest node */
drop procedure if exists sop_mlm_calc_subtree $$
create procedure sop_mlm_calc_subtree( in node_id int, in node_level int, in node_global_level int, inout node_sum decimal(10,2) )
begin
    declare c1_id, c1_parent_id, mlm_id int;
    declare node_sum2, node_sum3 decimal(10,2);
    declare c1 cursor for select id,parent_id from clients where parent_id=node_id order by id;
    declare c2 cursor for select sum from sop_mlm where id=mlm_id and year(created_at)=year(now()) and month(created_at)=month(now());
    declare exit handler for not found begin
        if mlm_id > 0 then
            update sop_mlm set own_orders_sum=(select sum(sum) from sop_orders where client_id=node_id and year(created_at)=year(now()) and month(created_at)=month(now()) group by sum) where id=mlm_id;
            update sop_mlm set sum=own_orders_sum+node_sum,parent_id=(select parent_id from clients where id=node_id) where id=mlm_id;
            open c2; fetch c2 into node_sum3; close c2;
            if node_sum3 >= 100 and node_sum3 < 200 then
                update sop_mlm set percent=3 where id=mlm_id;
            elseif node_sum3 >= 200 and node_sum3 < 300 then
                update sop_mlm set percent=5 where id=mlm_id;
            elseif node_sum3 >= 300 and node_sum3 < 400 then
                update sop_mlm set percent=7 where id=mlm_id;
            elseif node_sum3 >= 400 then
                update sop_mlm set percent=9 where id=mlm_id;
            end if;
        end if;
    /*
        if c1_id is null then
            ��������� ���� � �����
        end if;
    */
    end;
    
    if node_level = 1 then
        insert into sop_mlm (client_id,created_at,branches,level) values (node_id,now(),(select count(*) from clients where parent_id=node_id),node_global_level);
        set mlm_id = (select last_insert_id());
    end if;
    
    open c1;
    loop
        fetch c1 into c1_id,c1_parent_id;
        set node_sum = node_sum + (select sum(sum) from sop_orders where client_id=c1_id and year(created_at)=year(now()) and month(created_at)=month(now()) group by sum);
        call sop_mlm_calc_subtree( c1_id, node_level+1, node_level+1, node_sum );
    end loop;
    close c1;
    
end $$
delimiter ;

delimiter $$
drop procedure if exists sop_mlm_earns $$
create procedure sop_mlm_earns()
begin
    declare c1_id int;
    declare c1 cursor for select client_id from sop_mlm where (parent_id is null or parent_id=0) and year(created_at)=year(now()) and month(created_at)=month(now()) order by id;
    declare exit handler for not found begin end;
    
    update sop_mlm set own_orders_marked=0, earn=0 where year(created_at)=year(now()) and month(created_at)=month(now());
    
    open c1;
    loop
        fetch c1 into c1_id;
        call sop_mlm_leafs( c1_id );
    end loop;
    close c1;
    
end $$
delimiter ;

delimiter $$
drop procedure if exists sop_mlm_leafs $$
create procedure sop_mlm_leafs( in node_id int )
begin
    declare c1_id, c1_parent_id int;
    declare c1 cursor for select client_id,parent_id from sop_mlm where parent_id=node_id and year(created_at)=year(now()) and month(created_at)=month(now()) order by id;
    declare exit handler for not found begin
        if c1_id is null then
            call sop_mlm_calc_earns( node_id );
        end if;
    end;
    
    open c1;
    loop
        fetch c1 into c1_id,c1_parent_id;
        call sop_mlm_leafs( c1_id );
    end loop;
    close c1;
    
end $$
delimiter ;

delimiter $$
drop procedure if exists sop_mlm_calc_earns $$
create procedure sop_mlm_calc_earns( in node_id int )
begin
    declare c1_id, c1_parent_id, c1_percent, c1_children int;
    declare sum, c1_sum, c1_own_orders_sum, c1_own_orders_marked, rest decimal(10,2);
    declare c1 cursor for select id,parent_id,sop_mlm.sum,own_orders_sum,own_orders_marked,percent from sop_mlm where client_id=node_id and year(created_at)=year(now()) and month(created_at)=month(now()) order by id;
    declare exit handler for not found begin
    end;
    
    set rest = 0;
    set c1_children = 0;
    
    loop
        open c1;
        fetch c1 into c1_id, c1_parent_id, c1_sum, c1_own_orders_sum, c1_own_orders_marked, c1_percent;
        close c1;
        
        update sop_mlm set children=children+c1_children where id=c1_id;
        
        if c1_own_orders_marked = 1 then
            set rest = rest + 0;
        else
            set rest = rest + c1_own_orders_sum;
            set c1_children = c1_children + 1;
        end if;
        
        if c1_percent > 0 then
            set sum = rest*c1_percent/100;
            set rest = rest - sum;
            update sop_mlm set earn=earn+sum where id=c1_id;
        end if;
        update sop_mlm set own_orders_marked=1 where id=c1_id;
        set node_id = c1_parent_id;
    end loop;
    
end $$
delimiter ;

/* ����������� ������, �.�. ���� ����� ������ ������ 25% �� ����� �������, �� ��������������� ���������� ���������� ������ � 2% */
delimiter $$
drop procedure if exists sop_mlm_optimize $$
create procedure sop_mlm_optimize()
begin
    declare c1_id int;
    declare tree_sum, earn_sum decimal(10,2);
    declare c1 cursor for select id,sum from sop_mlm where (parent_id is null or parent_id=0) and year(created_at)=year(now()) and month(created_at)=month(now());
    declare c2 cursor for select sum(earn) from sop_mlm where year(created_at)=year(now()) and month(created_at)=month(now());
    declare exit handler for not found begin end;
    
    open c1;
    loop
        fetch c1 into c1_id, tree_sum;
        open c2; fetch c2 into earn_sum; close c2;
        if (earn_sum/tree_sum*100) > 25 then
            call sop_mlm_optimize_tree( c1_id, tree_sum );
        end if;
    end loop;
    close c1;
    
end $$
delimiter ;

delimiter $$
drop procedure if exists sop_mlm_optimize_tree $$
create procedure sop_mlm_optimize_tree( in node_id int, in tree_sum decimal(10,2) )
begin
    declare c2_id int;
    declare earn_sum decimal(10,2);
    declare c1 cursor for select sum(earn) from sop_mlm where year(created_at)=year(now()) and month(created_at)=month(now());
    declare c2 cursor for select id from sop_mlm where percent not in (0,2) and year(created_at)=year(now()) and month(created_at)=month(now()) order by earn desc,id;
    declare exit handler for not found begin end;
    
    loop1: loop
        open c2;
        loop2: loop
            fetch c2 into c2_id;
            update sop_mlm set percent=2 where id=c2_id;
            open c1; fetch c1 into earn_sum; close c1;
            
            if (earn_sum/tree_sum*100) <= 25 then
                select 'LEAVE FROM LOOP';
                leave loop1;
            end if;
            call sop_mlm_earns();
        end loop loop2;
        close c2;
    end loop loop1;
    
end $$
delimiter ;

/* ================================================ */
/* ============== CALL ORDER ====================== 
call sop_mlm();
call sop_mlm_earns();
call sop_mlm_optimize();
  ============== CALL ORDER ====================== */

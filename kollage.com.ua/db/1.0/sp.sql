/* set max_sp_recursion_depth=10000; */

delimiter $$
drop procedure if exists shop_product_categories $$
create procedure shop_product_categories( in root_id int, inout ids text )
begin
    declare c1_id, c1_parent_id, child int;
    declare delim varchar(10);
    declare c1 cursor for select id,parent_id from shop_product_categories where parent_id=root_id;
    declare exit handler for not found begin end;
    
    if length(ids) > 0 then
        set delim = ',';
    else
        set delim = '';
    end if;
    
    set ids = concat( ids, delim, root_id );
    
    open c1;
    loop
        fetch c1 into c1_id,c1_parent_id;
        call shop_product_categories( c1_id, ids );
    end loop;
    close c1;
    
end $$
delimiter ;

delimiter $$
drop procedure if exists shop_product_categories2 $$
create procedure shop_product_categories2( in root_id int )
begin
    declare c1_id, c1_parent_id, child int;
    declare delim varchar(10);
    declare c1 cursor for select id,parent_id from shop_product_categories where parent_id=root_id;
    declare exit handler for not found begin end;
    
    create temporary table if not exists ids_table ( id SMALLINT ) ENGINE=MEMORY;
    
    insert into ids_table (id) values (root_id);
    
    open c1;
    loop
        fetch c1 into c1_id,c1_parent_id;
        call shop_product_categories2( c1_id );
    end loop;
    close c1;
    
end $$
delimiter ;

delimiter $$
drop function if exists shop_product_categories_line $$
create function shop_product_categories_line( start_id int )  returns text DETERMINISTIC
begin
    declare c1_parent_id int;
    declare ids text;
    declare delim varchar(10);
    
    set delim = '';
    set ids = '';
    set ids = concat( ids, delim, start_id );
    set delim = ',';
    set c1_parent_id = (select parent_id from shop_product_categories where id=start_id );
    while c1_parent_id != 0 or c1_parent_id is not null do
        set ids = concat( ids, delim, c1_parent_id );
        set c1_parent_id = (select parent_id from shop_product_categories where id=c1_parent_id );
    end while;
    
    return ids;
end $$
delimiter ;

delimiter $$
drop function if exists shop_product_root_category $$
create function shop_product_root_category( category_id int unsigned )  returns int unsigned DETERMINISTIC
begin
    declare p_id int unsigned;
    declare p_id2 int unsigned;
    
    set p_id = ( select parent_id from shop_product_categories where id=category_id );
    set p_id2 = 0;
    while p_id != 0 do
        set p_id2 = p_id;
        set p_id = ( select parent_id from shop_product_categories where id=p_id );
    end while;
    
    if p_id = 0 or p_id is null then
        if p_id2 = 0 or p_id2 is null then
            set p_id = category_id;
        else
            set p_id = p_id2;
        end if;
    end if;
    
    return p_id;
    
end $$
delimiter ;

delimiter $$
drop function if exists shop_product_page_in_category $$
create function shop_product_page_in_category( i_category_id int unsigned, i_product_id int unsigned, i_rows_per_page int unsigned )  returns int unsigned DETERMINISTIC
begin
    declare rows_in_category, position, c1_id, page int unsigned;
    /*declare c1 cursor for select id from shop_products where category_id=i_category_id order by price;*/
    declare c1 cursor for select distinct p.id as id from shop_products p left join shop_product_availability pa on pa.product_id=p.id where p.status=1 and category_id=i_category_id order by price;
    declare exit handler for not found begin return 1; end;
    
    /*set rows_in_category = ( select count(id) from shop_products where category_id=i_category_id );*/
    /*set rows_in_category = ( select count(p.id) from shop_products p left join shop_product_availability pa on pa.product_id=p.id where pa.quantity>0 and p.status=1 and p.category_id=i_category_id );*/
    set page = 1;
    set position = 0;

    if (select id from shop_products where id=i_product_id) then
        open c1;
        loop
            fetch c1 into c1_id;
            set position = position + 1;
            if c1_id = i_product_id then
                set page = ( select ceil(position / i_rows_per_page) );
                return page;
            end if;
        end loop;
        close c1;
    end if;
    
    return page;
    
end $$
delimiter ;

delimiter $$
drop function if exists shop_product_page_in_category2 $$
create function shop_product_page_in_category2( i_category_id int unsigned, i_product_id int unsigned, i_rows_per_page int unsigned )  returns int unsigned DETERMINISTIC
begin
    declare rows_in_category, position, c1_id, page int unsigned;
    declare c2 cursor for select id from shop_products where category_id=i_category_id order by brand_id,price;
    declare c1 cursor for select distinct p.id from shop_products p left join shop_product_availability pa on pa.product_id=p.id where pa.quantity>0 and p.status=1 and category_id in (select id from ids_table) order by price;
    declare exit handler for not found begin end;
    
    call shop_product_categories2(i_category_id);
    
    set rows_in_category = ( select count(id) from shop_products where category_id in (select id from ids_table) );
    set page = 1;
    set position = 0;

    if (select id from shop_products where id=i_product_id) then
        open c1;
        loop
            fetch c1 into c1_id;
            set position = position + 1;
            if c1_id = i_product_id then
                set page = ( select ceil(position / i_rows_per_page) );
                drop temporary table if exists ids_table;
                return page;
            end if;
        end loop;
        close c1;
    end if;
    
    drop temporary table if exists ids_table;
    return page;
    
end $$
delimiter ;

delimiter $$
drop function if exists fair_categories_line $$
create function fair_categories_line( start_id int )  returns text DETERMINISTIC
begin
    declare c1_parent_id int;
    declare ids text;
    declare delim varchar(10);
    
    set delim = '';
    set ids = '';
    set ids = concat( ids, delim, start_id );
    set delim = ',';
    set c1_parent_id = (select parent_id from fair_categories where id=start_id );
    while c1_parent_id != 0 or c1_parent_id is not null do
        set ids = concat( ids, delim, c1_parent_id );
        set c1_parent_id = (select parent_id from fair_categories where id=c1_parent_id );
    end while;
    
    return ids;
end $$
delimiter ;

delimiter $$
drop function if exists fair_objects_categories_line $$
create function fair_objects_categories_line( start_id int )  returns text DETERMINISTIC
begin
    declare c1_parent_id int;
    declare ids text;
    declare delim varchar(10);
    
    set delim = '';
    set ids = '';
    set ids = concat( ids, delim, start_id );
    set delim = ',';
    set c1_parent_id = (select parent_id from fair_categories where id=start_id );
    while c1_parent_id != 0 or c1_parent_id is not null do
        set ids = concat( ids, delim, c1_parent_id );
        set c1_parent_id = (select parent_id from fair_categories where id=c1_parent_id );
    end while;
    
    return ids;
end $$
delimiter ;

delimiter $$
drop procedure if exists fair_categories $$
create procedure fair_categories( in root_id int, inout ids text )
begin
    declare c1_id, c1_parent_id, child int;
    declare delim varchar(10);
    declare c1 cursor for select id,parent_id from fair_categories where parent_id=root_id;
    declare exit handler for not found begin end;
    
    if length(ids) > 0 then
        set delim = ',';
    else
        set delim = '';
    end if;
    
    set ids = concat( ids, delim, root_id );
    
    open c1;
    loop
        fetch c1 into c1_id,c1_parent_id;
        call fair_categories( c1_id, ids );
    end loop;
    close c1;
    
end $$
delimiter ;

delimiter $$
drop function if exists fair_object_page_in_category $$
create function fair_object_page_in_category( i_category_id int unsigned, i_object_id int unsigned, i_rows_per_page int unsigned )  returns int unsigned DETERMINISTIC
begin
    declare rows_in_category, position, c1_id, page int unsigned;
    declare c1 cursor for select id from fair_objects where category_id1=i_category_id order by brand_id,price desc;
    declare exit handler for not found begin end;
    
    set rows_in_category = ( select count(id) from fair_objects where category_id1=i_category_id );
    set page = 1;
    set position = 0;

    if (select id from fair_objects where id=i_object_id) then
        open c1;
        loop
            fetch c1 into c1_id;
            set position = position + 1;
            if c1_id = i_object_id then
                set page = ( select ceil(position / i_rows_per_page) );
                return page;
            end if;
        end loop;
        close c1;
    end if;
    
    return page;
    
end $$
delimiter ;

<?php
/*
	1. unique_visitor.php
*/
require('lib/util.php');
load_configuration();
initialize_session( 'admin' );
require('lib/init.php');

	
	$host = $_SERVER['HTTP_HOST'];
	$ipaddress = $_SERVER['REMOTE_ADDR'];
	
	$ret = CheckUniqueVisitor( $host, $ipaddress );
	switch( $ret )
	{
		case 1:
			$result = "Visitor's stat was added";
			break;
		case 2:
			$result = "Visitor's stat was changed";
			break;
		default:
			$result = "No action";
			break;
	}

	//header("Expires: 0");
	//header("Cache-Control: private");
	//header("Pragma: no-cache");

	header('Content-type: text/xml');
	$xmlHeader = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>";
	echo $xmlHeader;

	echo "<result code='$ret'>" . xmlencode($result) . "</result>";
	
	function CheckUniqueVisitor( $host, $ipaddress )
	{
		global $conn;
		$retval = 0;
		
		if( $ipaddress )
		{
			$uv = new MysqlTable( 'unique_visitors' );
			if( !$uv->find_first( "ipaddress='$ipaddress'" ) )
			{
				$uv->save( Array("host"=>$host, "ipaddress"=>$ipaddress) );
				$uv->save( Array( "id"=>$uv->get_last_insert_id() ) );
				$retval = 1;
			}
			else
			{
				$id = $uv->data[0]["id"];
				$hits = $uv->data[0]["hits"] + 1;
				
				if( !$uv->find_first( "week(updated_at)=week(now()) and ipaddress='$ipaddress'" ) )
				{
					$uv->save( Array("id"=>$id, "hits"=>$hits) );
					$retval = 2;
				}
			}
		}

		return( $retval );
	}
?>
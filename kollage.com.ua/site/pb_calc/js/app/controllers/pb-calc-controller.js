(function () {
    "use strict";

    angular
        .module('pbCalcApp')
        .controller('pbCalcCtrl', pbCalcCtrl);

    /*================================================================================================================*/
    /*============================================== PB_CALC Controller ==============================================*/
    /*================================================================================================================*/

    pbCalcCtrl.$inject = ['$scope', 'pbCalcService'];
    function pbCalcCtrl($scope, pbCalcService) {

        var vm = this;

        //Call to initializing method
        getPbCalcData();

        //Public API
        vm.chosePbType      = chosePbType;
        vm.chosePbFormat    = chosePbFormat;
        vm.choseCurrency    = choseCurrency;
        vm.chosePbCoverType = chosePbCoverType;
        vm.chosePbCover     = chosePbCover;

        vm.itemPrice   = itemPrice;

        //For detailed view
        vm._getBasePrice                = _getBasePrice;
        vm._getPaperPrice               = _getPaperPrice;
        vm._getPaperBasePrice           = _getPaperBasePrice;
        vm._getLaminationPrice          = _getLaminationPrice;
        vm._getPhotobookForzacePrice    = _getPhotobookForzacePrice;
        vm._getCoverPrice               = _getCoverPrice;
        vm._getSuperCoverPrice          = _getSuperCoverPrice;

        vm.designPrice = designPrice;
        vm.boxPrice    = boxPrice;
        vm.totalPrice  = totalPrice;

        vm.serverUrl = getServerUrl();

        vm.detailedView = true;

        function getPbCalcData() {
            var serviceUrl = getServerUrl() + "/?calculator.pb_calculator_json";
            pbCalcService.getPbCalcData(serviceUrl)
                .then(function (response) {
                    initializePbCalc(response);
                });
        }

        /*============================================== Initialization ==============================================*/
        function initializePbCalc(response) {
            vm.pbTypes = response.data.pbTypes;
            vm.currencies = response.data.currencies;

            console.log('vm.pbTypes: ');
            console.log(vm.pbTypes);
            console.log('vm.currencies: ');
            console.log(vm.currencies);

            vm.pageCount = 0;
            vm.pbItemCount = 0;
            vm.pageCounts = [];
            vm.itemCounts = [];

            choseDefaultCurrency();
            choseDefaultPbType();
        }


        /*============================================== Currencies ==================================================*/
        function choseDefaultCurrency() {
            if (vm.currencies && Object.keys(vm.currencies).length > 0) {
                var currencyId = Object.keys(vm.currencies)[0];
                choseCurrency(currencyId);
            }
        }
        function choseCurrency(currencyId) {
            vm.currencyId = currencyId;
            vm.currency = vm.currencies[currencyId];
            var currencyRate = vm.currency ? vm.currency.rate : 1;
            vm.currencyCoef = 1 / currencyRate;
        }

        /*============================================= PB Types =====================================================*/
        function choseDefaultPbType() {
            if (vm.pbTypes && Object.keys(vm.pbTypes).length > 0) {
                var pbTypeId = Object.keys(vm.pbTypes)[0];
                chosePbType(pbTypeId);
            }
        }
        function chosePbType(pbTypeId) {
            vm.pbTypeId = pbTypeId;
            vm.pbType = vm.pbTypes[pbTypeId];
            vm.pbFormats = vm.pbType.formats;
            choseDefaultPbFormat();
        }

        /*============================================= PB Formats ===================================================*/
        function choseDefaultPbFormat() {
            if (vm.pbFormats && Object.keys(vm.pbFormats).length > 0) {
                var pbFormatId = Object.keys(vm.pbFormats)[0];
                chosePbFormat(pbFormatId);
            }
        }
        function chosePbFormat(pbFormatId) {

            vm.pbFormatId = pbFormatId;
            vm.pbFormat = vm.pbFormats[pbFormatId];

            vm.pbCoverTypes     = vm.pbFormat.cover_types;
            vm.pbPapers         = vm.pbFormat.papers;
            vm.pbPaperBases     = vm.pbFormat.paper_bases;
            vm.pbLaminations    = vm.pbFormat.laminations;

            fillPages();

            choseDefaultPbCoverType();
            choseDefaultPbPaper();
            choseDefaultPbPaperBase();
            choseDefaultPbLamination();
        }

        /*============================================= PB Papers ====================================================*/
        function fillPages() {
            console.log('fillPages invoked.');
            vm.pageCounts = [];
            var pbFormat = vm.pbFormat;
            if (pbFormat && pbFormat.start_page > 0) {
                var max_page = pbFormat.stop_page > 0 ? pbFormat.stop_page: 26;
                var page_block = pbFormat.page_block > 0 ? pbFormat.page_block : 2;
                for (var i = pbFormat.start_page; i < max_page; i+= page_block) {
                    vm.pageCounts.push(i);
                }
            }
            if (vm.pageCounts.indexOf(vm.pageCount) == -1) {
                vm.pageCount = vm.pageCounts.length > 0 ? vm.pageCounts[0] : 1;
            }
        }

        /*============================================= PB Papers ====================================================*/
        function choseDefaultPbPaper() {
            if (vm.pbPapers && Object.keys(vm.pbPapers).length > 0) {
                var pbPaperId = Object.keys(vm.pbPapers)[0];
                chosePbPaper(pbPaperId);
            }
        }
        function chosePbPaper(pbPaperId) {
            vm.pbPaperId = pbPaperId;
            vm.pbPaper = vm.pbPapers[pbPaperId];
        }


        /*======================================== PB Paper Bases ====================================================*/
        function choseDefaultPbPaperBase() {
            if (vm.pbPaperBases && Object.keys(vm.pbPaperBases).length > 0) {
                var pbPaperBaseId = Object.keys(vm.pbPaperBases)[0];
                chosePbPaperBase(pbPaperBaseId);
            }
        }
        function chosePbPaperBase(pbPaperBaseId) {
            vm.pbPaperBaseId = pbPaperBaseId;
            vm.pbPaperBase = vm.pbPaperBases[pbPaperBaseId];
        }


        /*======================================== PB Laminations ====================================================*/
        function choseDefaultPbLamination() {
            if (vm.pbLaminations && Object.keys(vm.pbLaminations).length > 0) {
                var pbLaminationId = Object.keys(vm.pbLaminations)[0];
                chosePbLamination(pbLaminationId);
            }
        }
        function chosePbLamination(pbLaminationId) {
            vm.pbLaminationId = pbLaminationId;
            vm.pbLamination = vm.pbLaminations[pbLaminationId];
        }

        /*============================================ PB Cover Types  ===============================================*/
        function choseDefaultPbCoverType() {
            if (vm.pbCoverTypes && Object.keys(vm.pbCoverTypes).length > 0) {
                var pbCoverTypeId = Object.keys(vm.pbCoverTypes)[0];
                chosePbCoverType(pbCoverTypeId);
            }
        }
        function chosePbCoverType(pbCoverTypeId) {
            vm.pbCoverTypeId = pbCoverTypeId;
            vm.pbCoverType = vm.pbCoverTypes[pbCoverTypeId];
            vm.pbCovers = vm.pbCoverType.covers;
            choseDefaultPbCover();
        }

        /*============================================ PB Covers Types  ==============================================*/
        function choseDefaultPbCover() {
            if (vm.pbCovers && Object.keys(vm.pbCovers).length > 0) {
                var pbCoverId = Object.keys(vm.pbCovers)[0];
                chosePbCover(pbCoverId);
            }
        }
        function chosePbCover(pbCoverId) {
            vm.pbCoverId = pbCoverId;
            vm.pbCover = vm.pbCovers[pbCoverId];
        }


        /*========================================== Price Calculation ===============================================*/
        function itemPrice() {

            var itemPrice = 0;

            itemPrice += _getBasePrice();
            itemPrice += _getPaperPrice();
            itemPrice += _getPaperBasePrice();
            itemPrice += _getLaminationPrice();
            itemPrice += _getPhotobookForzacePrice();
            itemPrice += _getCoverPrice();
            itemPrice += _getSuperCoverPrice();


            console.log('itemPrice: ' + itemPrice);
            return itemPrice;
        }

        function designPrice() {
            var designPrice = 0;

            console.log('designPrice: ' + designPrice);
            return designPrice;
        }

        function boxPrice() {

            var boxPrice = 0;

            console.log('boxPrice: ' + boxPrice);
            return boxPrice;
        }

        function totalPrice() {

            var totalPrice = 0;

            console.log('totalPrice: ' + totalPrice);
            return totalPrice;
        }

        /*============================================= Private Methods ==============================================*/
        function _getBasePrice() {
            var priceBase = vm.pbFormat ? vm.pbFormat.price_base : 0;
            return priceBase * vm.currencyCoef;
        }

        function _getPaperPrice() {
            var pbPaper = vm.pbPaper;
            if (pbPaper) {
                return pbPaper.price * vm.pageCount * vm.currencyCoef;
            }
            return 0;
        }

        function _getPaperBasePrice() {
            var pbPaperBase = vm.pbPaperBase;
            if (pbPaperBase) {
                return pbPaperBase.price * vm.pageCount * vm.currencyCoef;
            }
            return 0;
        }

        function _getLaminationPrice() {
            var pbLamination = vm.pbLamination;
            if (pbLamination) {
                return pbLamination.price * vm.pageCount * vm.currencyCoef;
            }
            return 0;
        }

        function _getPhotobookForzacePrice() {

            return 0;
        }

        function _getCoverPrice() {
            var pbCover = vm.pbCover;
            if (pbCover) {
                return pbCover.price * vm.currencyCoef;
            }
            return 0;
        }

        function _getSuperCoverPrice() {

            return 0;
        }

        /*============================================== Helper Methods ==============================================*/
        function getServerUrl() {
            var currentLocation = window.location.href;
            console.log('currentLocation: ' + currentLocation);
            return currentLocation.includes('dev.kollage') ?
                'http://dev.kollage.com.ua' :
                'http://kollage.com.ua';
        }
    }

})();
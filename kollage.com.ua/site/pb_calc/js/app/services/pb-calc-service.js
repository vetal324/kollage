(function () {
    "use strict";

    angular
        .module('pbCalcApp')
        .service('pbCalcService', pbCalcService);

    /*============================================== pbCalc Controller ==============================================*/
    pbCalcService.$inject = ['$http'];
    function pbCalcService($http) {

        this.getPbCalcData = function (serviceUrl) {
            console.log('pbCalcService -> getPbCalcData invoked, serviceUrl :');
            console.log(serviceUrl);
            return $http({
                method: "get",
                url: serviceUrl
            }).then(
                function successCallback (response) {
                    return response;
                },
                function errorCallback( response ) {
                    return response;
                }
            );
        };

    }

})();
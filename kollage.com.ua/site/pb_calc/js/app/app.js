(function () {

    angular
        .module('pbCalcApp', []);

    angular.element(document).ready(function () {
        var app = $('#page-wrapper')[0];
        angular.bootstrap(app, ['pbCalcApp']);
    });

})();
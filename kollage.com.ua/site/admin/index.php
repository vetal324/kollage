<?php

header("Expires: 0");
header("Cache-Control: private");
header("Pragma: no-cache");

require('../lib/util.php');
load_configuration();
initialize_session( 'admin' );

$_ENV['controllers_path'] .= '/admin';
$_ENV['views_path'] .= '/admin';

require('../lib/init.php');

$auth = load_controller( 'auth' );
$auth->check();

list($controller, $method, $parameters) = parse_html_query();
if( !$controller )
{
    $controller = 'welcome'; $method = 'show';
}

if( $controller && $method )
{
    $document = load_controller( $controller );
    if( is_object($document) )
    {
        //run_action( $document, $method, $parameters );
        run_action( $document, $method );
    }
}

?>
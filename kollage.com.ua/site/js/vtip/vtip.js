/**
Vertigo Tip by www.vertigo-project.com
Requires jQuery
*/

var vtip = this.vtip = function() {    
	this.xOffset = 10; // x distance from mouse
	this.yOffset = 10; // y distance from mouse
	
	var $ = jQuery;
	
	$(".vtip").off('mouseenter mouseleave mousemove').hover(    
		function(e) {
			this.t = this.title;
			this.title = ''; 
			$('body').append( '<div id="vtip" class="tip-wrap"><div class="tip-top"></div><div class="tip_middle"><div class="tip-content">'+this.t+'</div></div><div class="tip-bottom"></div></div>' );
			this.yOffset=($("#vtip").height()+3)*-1;
			this.xOffset=-33;
			this.top = (e.pageY + this.yOffset);
			this.left = (e.pageX + this.xOffset);
			$('div#vtip').css("top", this.top+"px").css("left", this.left+"px").fadeIn(350);
			
		},
		function() {
			this.title = this.t;
			$("div#vtip").fadeOut(350).remove();
		}
	).mousemove(
		function(e) {
			this.top = (e.pageY + this.yOffset);
			this.left = (e.pageX + this.xOffset);
			$("div#vtip").css("top", this.top+"px").css("left", this.left+"px");
		}
	);
	
};

jQuery(document).ready(function($){vtip();}) 
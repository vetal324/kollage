/* Demo Note:  This demo uses a FileProgress class that handles the UI for displaying the file name and percent complete.
The FileProgress class is not part of SWFUpload.
*/


/* **********************
   Event Handlers
   These are my custom event handlers to make my
   web application behave the way I went when SWFUpload
   completes different tasks.  These aren't part of the SWFUpload
   package.  They are part of my application.  Without these none
   of the actions SWFUpload makes will show up in my application.
   ********************** */
   
var upload_timer;
var start_upload_time;
var start_upload_flag = true;

function getFileExt( name )
{
	var retval = "";
	
	var ext = name.match( /^.*\.([^.]+)$/i )[1];
	if( ext ) retval = ext.toLowerCase();
	
	return( retval );
}

function fileQueued(file) {
	try {
		SWFUpload.queue.size += file.size;
		if( getFileExt(file.name) == "jpg" )
		{
			SWFUpload.queue.timeToApply += 15;
		}
		else if( getFileExt(file.name) == "zip" || getFileExt(file.name) == "rar" )
		{
			SWFUpload.queue.timeToApply += ( ( file.size * 60 ) / 7260000 ).round();
		}
		var progress = new FileProgress(file, this.customSettings.progressTarget);
		progress.setStatus("ожидание загрузки...");
		progress.toggleCancel(true, this);

	} catch (ex) {
		this.debug(ex);
	}

}

function fileQueueError(file, errorCode, message) {
	try {
		if (errorCode === SWFUpload.QUEUE_ERROR.QUEUE_LIMIT_EXCEEDED) {
			alert("You have attempted to queue too many files.\n" + (message === 0 ? "You have reached the upload limit." : "You may select " + (message > 1 ? "up to " + message + " files." : "one file.")));
			return;
		}

		var progress = new FileProgress(file, this.customSettings.progressTarget);
		progress.setError();
		progress.toggleCancel(false);

		switch (errorCode) {
		case SWFUpload.QUEUE_ERROR.FILE_EXCEEDS_SIZE_LIMIT:
			progress.setStatus("File is too big.");
			this.debug("Error Code: File too big, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
			break;
		case SWFUpload.QUEUE_ERROR.ZERO_BYTE_FILE:
			progress.setStatus("Cannot upload Zero Byte files.");
			this.debug("Error Code: Zero byte file, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
			break;
		case SWFUpload.QUEUE_ERROR.INVALID_FILETYPE:
			progress.setStatus("Invalid File Type.");
			this.debug("Error Code: Invalid File Type, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
			break;
		default:
			if (file !== null) {
				progress.setStatus("Unhandled Error");
			}
			this.debug("Error Code: " + errorCode + ", File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
			break;
		}
	} catch (ex) {
		this.debug(ex);
	}
}

function UpdateUploadTimer()
{
	var now = (new Date()).getTime();
	$('tdTimeElapsedQueue').innerHTML = SWFUpload.speed.formatTime( (now - start_upload_time) / 1000 );
	upload_timer = setTimeout( UpdateUploadTimer, 700 );
}

function uploadStart(file) {
	try {
	
		if( start_upload_flag )
		{
			start_upload_time = (new Date()).getTime();
			start_upload_flag = false;
			upload_timer = setTimeout( UpdateUploadTimer, 700 );
		}
		
		var progress = new FileProgress(file, this.customSettings.progressTarget);
		progress.setStatus("загружаю...");
		progress.toggleCancel(true, this);
		$("flash_cancel_upload_button").style.visibility = 'visible';
		$("flash_cancel_upload_button").style.display = 'block';
		
		updateSpeed.call(this, file);
	}
	catch (ex) {}
	
	return true;
}

function uploadProgress(file, bytesLoaded, bytesTotal) {
	try {
		var percent = Math.ceil((bytesLoaded / bytesTotal) * 100);

		var progress = new FileProgress(file, this.customSettings.progressTarget);
		progress.setProgress( percent );
		progress.setStatus( "загрузка "+ (bytesTotal/1024/1024).toFixed(3)  +"MB ("+ percent +"%)" );
		
		updateSpeed.call(this, file);
	} catch (ex) {
		this.debug(ex);
	}
}

function uploadSuccess(file, serverData) {
	try {
		var progress = new FileProgress(file, this.customSettings.progressTarget);
		progress.setComplete();
		progress.setStatus("загрузка завершена, идет обработка...");
		progress.toggleCancel(false);
		
		updateSpeed.call(this, file);

	} catch (ex) {
		this.debug(ex);
	}
}

function uploadError(file, errorCode, message) {
	try {
		var progress = new FileProgress(file, this.customSettings.progressTarget);
		progress.setError();
		progress.toggleCancel(false);

		switch (errorCode) {
		case SWFUpload.UPLOAD_ERROR.HTTP_ERROR:
			progress.setStatus("Upload Error: " + message);
			this.debug("Error Code: HTTP Error, File name: " + file.name + ", Message: " + message);
			break;
		case SWFUpload.UPLOAD_ERROR.UPLOAD_FAILED:
			progress.setStatus("Upload Failed.");
			this.debug("Error Code: Upload Failed, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
			break;
		case SWFUpload.UPLOAD_ERROR.IO_ERROR:
			progress.setStatus("Server (IO) Error");
			this.debug("Error Code: IO Error, File name: " + file.name + ", Message: " + message);
			break;
		case SWFUpload.UPLOAD_ERROR.SECURITY_ERROR:
			progress.setStatus("Security Error");
			this.debug("Error Code: Security Error, File name: " + file.name + ", Message: " + message);
			break;
		case SWFUpload.UPLOAD_ERROR.UPLOAD_LIMIT_EXCEEDED:
			progress.setStatus("Upload limit exceeded.");
			this.debug("Error Code: Upload Limit Exceeded, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
			break;
		case SWFUpload.UPLOAD_ERROR.FILE_VALIDATION_FAILED:
			progress.setStatus("Failed Validation.  Upload skipped.");
			this.debug("Error Code: File Validation Failed, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
			break;
		case SWFUpload.UPLOAD_ERROR.FILE_CANCELLED:
			progress.setStatus("Cancelled");
			progress.setCancelled();
			break;
		case SWFUpload.UPLOAD_ERROR.UPLOAD_STOPPED:
			progress.setStatus("Stopped");
			break;
		default:
			progress.setStatus("Unhandled Error: " + errorCode);
			this.debug("Error Code: " + errorCode + ", File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
			break;
		}
	} catch (ex) {
		this.debug(ex);
	}
}

// This event comes from the Queue Plugin
function queueComplete(numFilesUploaded) {
	LoadPhotosForm( load_photo_start_msg, page );
	UpdateOrderParameters();
	$("flash_cancel_upload_button").style.display = 'none';
	if( upload_timer ) clearTimeout( upload_timer );
	this.customSettings.tdAverageSpeed.innerHTML = '-';
	this.customSettings.tdTimeElapsedQueue.innerHTML = '-';
	this.customSettings.tdTimeRemainingQueue.innerHTML = '-';
	ClosePhotosFlashForm();
}

// This event comes from the Speed Plugin
function updateSpeed(file)
{
	var now = (new Date()).getTime();
	
	this.customSettings.tdAverageSpeed.innerHTML = SWFUpload.speed.formatBPS(file.averageSpeed); //средняя скорость
	
	//this.customSettings.tdTimeRemaining.innerHTML = SWFUpload.speed.formatTime(file.timeRemaining); //осталось, сек
	//this.customSettings.tdTimeElapsed.innerHTML = SWFUpload.speed.formatTime(file.timeElapsed); //время загрузки, сек
	
	//this.customSettings.tdTimeElapsedQueue.innerHTML = SWFUpload.speed.formatTime( (now - start_upload_time) / 1000 ); //прошло, сек    
	this.customSettings.tdTimeRemainingQueue.innerHTML = SWFUpload.speed.formatTime(file.timeRemainingQueue + SWFUpload.queue.timeToApply); //время загрузки очереди, сек
}

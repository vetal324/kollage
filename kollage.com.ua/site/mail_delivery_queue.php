<?php

require('lib/util.php');
load_configuration();
initialize_session( 'frontend' );

require('lib/init.php');

$base = load_controller( 'base' );
$base->mail_delivery_queue();

?>
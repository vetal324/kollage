<?php

$config = array(
	'db_host'               => 'localhost',
	'db_user'               => 'kollage',
	'db_password'           => 'secret',
	'db_name'               => 'kollage_v2',

	'tmp_path'              => 'tmp',
	'app_path'              => 'app',
	'controllers_path'      => 'controllers',
	'models_path'           => 'models',
	'views_path'            => 'views',
	'data_path'             => 'data',
	'mqueue_path'           => 'mqueue',
	'sop_path'              => 'sop',
	'fair_path'             => 'fair',
	'fair_documents'        => 'fair_documents',
	'userfiles_path'        => 'userfiles',
	'printbook_path'        => 'printbook',
	'photobook_path'        => 'photobook',

	'designer_orders'       => 'printbook/designer',
	
	'insit' => Array(
		'path' => 'printbook/import/insit',
		'notify_to' => 'gnatiuk_an@insit.com.ua'
	),
	
	'session_lifetime'      => 48
);

?>
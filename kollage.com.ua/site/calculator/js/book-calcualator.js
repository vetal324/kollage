var TYPE_BOOK_ID = 1;
var TYPE_BROCHURE_ID = 2;

var FORMAT_CUSTOM_ID = 9;

var BookCalculator = function(config) {

    this.sourceUrl = config.sourceUrl;

    this.templates = config.templates;

    this.container = $("#book-calculator");
    this.priceArea = $("#book-price-area");

    this.typeId = config.typeId;

    this.paperPriceDivider = 2;
    if (this.typeId === TYPE_BROCHURE_ID) {
        //brochures
        this.paperPriceDivider = 4;
    }

    this.bookTypesContainer = this.container.find("#book-types");

    this.bookFormatsContainer = this.container.find("#book-formats");


    this.coverTypesContainer = this.container.find("#cover-types");

    this.coverPaperTypesSection = this.container.find("#cover-paper-type-section");
    this.coverPaperTypesContainer = this.container.find("#cover-paper-types");

    this.coverLaminationsSection = this.container.find("#cover-lamination-section");
    this.coverLaminationsContainer = this.container.find("#cover-lamination-types");


    this.paperTypesSection = this.container.find("#paper-type-section");
    this.paperTypesContainer = this.container.find("#paper-types");

    this.printsContainer = this.container.find("#prints");

    this.printSection = this.container.find("#print-section");
    this.printBwSection = this.container.find("#print-bw-section");
    this.printColorSection = this.container.find("#print-color-section");

    this.bwPrintTypesContainer = this.container.find("#bw-print-types");
    this.colorPrintTypesContainer = this.container.find("#color-print-types");

    this.colorPageCountSection = this.container.find("#color-page-count-section");

    this.extraOptionsSection = this.container.find("#extra-options-section");
    this.extraOptionsContainer = this.container.find("#extra-options");

    this.printCountField = this.container.find("#print-count");
    this.pageCountField = this.container.find("#page-count");
    this.colorPageCountField = this.container.find("#color-page-count");

    this.priceContainer = $("#price-container");

    //bookTypes, loaded from server
    this.bookTypes = {};

    //UI Model
    this.minPrintCount = 0;
    this.maxPrintCount = 0;
    this.minPageCount = 0;
    this.maxPageCount = 0;

    this.printCount = 0;

    this.pageCount = 0;
    this.colorPageCount = 0;

    this.bookTypeId = 0;
    this.bookType = {};

    this.bookFormatId = 0;
    this.bookFormats = {};
    this.bookFormat = {};

    this.coverTypeId = 0;
    this.coverTypes = {};
    this.coverType = {};

    this.coverPaperTypeId = 0;
    this.coverPaperTypes = {};
    this.coverPaperType = {};

    this.coverLaminationId = 0;
    this.coverLaminations = {};
    this.coverLamination = {};
    this.coverLaminationEnabled = true;

    this.paperTypeId = 0;
    this.paperTypes = {};
    this.paperType = {};

    this.printTypes = {};
    this.printColor = 'bw';

    this.bwPrintTypeId = 0;
    this.bwPrintTypes = {};
    this.bwPrintType = {};

    this.colorPrintTypeId = 0;
    this.colorPrintTypes = {};
    this.colorPrintType = {};


};

$.extend(true, BookCalculator, {

    prototype: {

        /*============================================================================================================*/
        /*========================================== INITIALIZATION ==================================================*/
        /*============================================================================================================*/

        init: function () {
            var scope = this;
            scope.initializeTemplates();
            this.loadData().done(function (response) {
                if (response.bookTypes) {
                    scope.initializeCalculator(response.bookTypes);
                } else {
                    console.log('Failed to initialize BookCalculator.');
                }
            });
        },

        loadData: function () {
            var scope = this;
            return $.ajax({
                url: scope.sourceUrl,
                type: 'GET',
                dataType: "json",
                contentType:"application/json; charset=UTF-8",
                cache: false,
                data: {}
            });
        },

        initializeTemplates: function () {
            this.bookTypeTemplate = Handlebars.compile($("#" + this.templates.bookTypeTemplateId).html());
            this.bookFormatTemplate = Handlebars.compile($("#" + this.templates.bookFormatTemplateId).html());
            this.bookCoverTypeTemplate = Handlebars.compile($("#" + this.templates.bookCoverTypeTemplateId).html());
            this.bookCoverPaperTypeTemplate = Handlebars.compile($("#" + this.templates.bookCoverPaperTypeTemplateId).html());
            this.bookCoverLaminationsTemplate = Handlebars.compile($("#" + this.templates.bookCoverLaminationsTemplateId).html());

            this.bookPaperTypeTemplate = Handlebars.compile($("#" + this.templates.bookPaperTypeTemplateId).html());

            this.bookPrintsTemplate = Handlebars.compile($("#" + this.templates.bookPrintsTemplateId).html());

            this.bookBwPrintTypesTemplate = Handlebars.compile($("#" + this.templates.bookBwPrintTypesTemplateId).html());
            this.bookColorPrintTypesTemplate = Handlebars.compile($("#" + this.templates.bookColorPrintTypesTemplateId).html());

            this.bookExtraOptionsTemplate = Handlebars.compile($("#" + this.templates.extraOptionsTemplateId).html());

            this.priceTemplate = Handlebars.compile($("#" + this.templates.priceTemplateId).html());
        },

        initializeCalculator: function (bookTypes) {

            this.bookTypes = bookTypes;

            console.log('bookTypes:');
            console.log(bookTypes);

            this.render();

            this.bindListeners();
        },


        /*============================================================================================================*/
        /*=============================================== RENDERING ==================================================*/
        /*============================================================================================================*/

        render: function () {

            this.renderBookTypes();
        },

        /*============================================ Book types ====================================================*/

        renderBookTypes: function () {
            var bookTypes = this.bookTypes;
            this.bookTypesContainer.html("");
            this.bookFormatsContainer.html("");
            if (bookTypes) {
                for (var key in bookTypes) {
                    var bookType = bookTypes[key];
                    var bookTypeHtml = this.bookTypeTemplate(bookType);
                    this.bookTypesContainer.append(bookTypeHtml);
                }
                this.selectDefaultBookType();
            }
        },

        selectDefaultBookType: function() {
            if (this.bookTypes  && Object.keys(this.bookTypes).length > 0) {
                var bookTypeId = Object.keys(this.bookTypes)[0];
                this.selectBookType(bookTypeId);
            }
        },

        selectBookType: function (bookTypeId) {
            this.bookTypeId = bookTypeId;
            this.bookType = this.bookTypes[this.bookTypeId];
            $("#book-types .product-item").removeClass('active');
            $("#book-type-" + bookTypeId).addClass('active');
            this.renderBookFormats();
            this.renderExtraOptions();
        },

        /*===================================== Extra options ========================================================*/
        renderExtraOptions: function () {
            this.extraOptionsContainer.html("");
            this.extraOptions = this.bookType.options;
            if (this.extraOptions && Object.keys(this.extraOptions).length > 0) {
                this.extraOptionsSection.show();
                var extraOptionsHtml = this.bookExtraOptionsTemplate(this.bookType);
                this.extraOptionsContainer.html(extraOptionsHtml);
            } else {
                this.extraOptionsSection.hide();
            }
        },

        /*=========================================== Book formats ===================================================*/
        renderBookFormats: function () {
            this.bookFormatsContainer.html("");
            this.bookFormats = this.bookType.formats;
            if (this.bookFormats && Object.keys(this.bookFormats).length > 0) {
                for (var key in this.bookFormats) {
                    var format = this.bookFormats[key];
                    var bookFormatHtml = this.bookFormatTemplate(format);
                    this.bookFormatsContainer.append(bookFormatHtml);
                }
                this.selectDefaultBookFormat();
            }
        },

        selectDefaultBookFormat: function () {
            if (this.bookFormats) {
                var bookFormatId = Object.keys(this.bookFormats)[0];
                this.selectBookFormat(bookFormatId);
            }
        },

        selectBookFormat: function (bookFormatId) {
            this.bookFormatId = bookFormatId;
            this.bookFormat = this.bookFormats[this.bookFormatId];
            $("#book-formats .product-item").removeClass('active');
            $("#book-format-" + bookFormatId).addClass('active');

            if (bookFormatId == FORMAT_CUSTOM_ID) {
                //Custom format
                alert("Выберите ближайший стандартный формат (в сторону увеличения), или свяжитесь с нами: (0542) 61-20-23");
                return;
            }

            this.renderCoverTypes();
            this.renderPaperTypes();
            this.calculatePrice(this);
        },

        /*============================================================================================================*/
        /*============================================== BOOK COVER ==================================================*/
        /*============================================================================================================*/

        /*=========================================== Cover types ====================================================*/
        renderCoverTypes: function () {
            this.coverTypesContainer.html("");
            this.coverTypes = this.bookFormat.cover_types;
            if (this.coverTypes && Object.keys(this.coverTypes).length > 0) {
                for (var key in this.coverTypes) {
                    var coverType = this.coverTypes[key];
                    var coverTypeHtml = this.bookCoverTypeTemplate(coverType);
                    this.coverTypesContainer.append(coverTypeHtml);
                }
                this.selectDefaultCoverType();
            }
        },

        selectDefaultCoverType: function () {
            if (this.coverTypes) {
                var coverTypeId = Object.keys(this.coverTypes)[0];
                this.selectCoverType(coverTypeId);
            }
        },

        selectCoverType: function (coverTypeId) {
            this.coverTypeId = coverTypeId;
            this.coverType = this.coverTypes[this.coverTypeId];
            $("#cover-types .product-item").removeClass('active');
            $("#cover-type-" + coverTypeId).addClass('active');
            this.setPageCountAttrs();
            this.renderCoverPaperTypes();
            this.renderCoverLaminations();
        },


        setPageCountAttrs: function () {

            //Print Count
            this.minPrintCount = this.bookType.min_print_count;
            this.maxPrintCount = this.bookType.max_print_count;
            this.printCountField.attr('min', this.minPrintCount);
            this.printCountField.attr('max', this.maxPrintCount);
            this.printCountField.val(this.minPrintCount);

            //Page Count
            this.minPageCount = Math.max(this.coverType.min_page_count, this.bookType.min_page_count);
            this.maxPageCount = Math.min(this.coverType.max_page_count, this.bookType.max_page_count);
            this.pageCountStep = this.bookFormat.page_count_step;



            var minPageCount = this.minPageCount;
            if (minPageCount % this.pageCountStep != 0) {
                do {
                    minPageCount++;
                } while (minPageCount % this.pageCountStep != 0);
                this.minPageCount = minPageCount;
            }
            var maxPageCount = this.maxPageCount;
            if (maxPageCount % this.pageCountStep != 0) {
                do {
                    maxPageCount++;
                } while (maxPageCount % this.pageCountStep != 0);
                this.maxPageCount = maxPageCount;
            }

            this.pageCountField.attr("step", this.pageCountStep);
            this.colorPageCountField.attr("step", this.pageCountStep);

            this.pageCountField.attr('min', this.minPageCount);
            this.colorPageCountField.attr('min', this.minPageCount);

            this.pageCountField.attr('max', this.maxPageCount);
            this.colorPageCountField.attr('max', this.maxPageCount);

            this.pageCountField.val(this.minPageCount);
            this.pageCount = this.minPageCount;
            this.printCount = this.minPrintCount;

            /*if (this.pageCount < this.minPageCount) {
                this.pageCountField.val(this.minPageCount);
                this.pageCount = this.minPageCount;
            } else if (this.pageCount > this.maxPageCount) {
                this.pageCountField.val(this.maxPageCount);
                this.pageCount = this.maxPageCount;
            }*/
        },

        /*======================================== Cover paper types =================================================*/
        renderCoverPaperTypes: function () {
            this.coverPaperTypesContainer.html("");
            this.coverPaperTypes = this.coverType.paper_types;
            if (this.coverPaperTypes && Object.keys(this.coverPaperTypes).length > 0) {
                this.coverPaperTypesSection.show();
                var coverPaperTypeHtml = this.bookCoverPaperTypeTemplate(this.coverType);
                this.coverPaperTypesContainer.html(coverPaperTypeHtml);
                this.selectDefaultCoverPaperType();
            } else {
                this.coverPaperTypesSection.hide();
            }
        },

        selectDefaultCoverPaperType: function () {
            if (this.coverPaperTypes && Object.keys(this.coverPaperTypes).length > 0) {
                this.coverPaperTypeId = Object.keys(this.coverPaperTypes)[0];
                this.coverPaperType = this.coverPaperTypes[this.coverPaperTypeId];
                this.coverLaminationEnabled = this.coverPaperType.is_laminated;
            }
        },

        /*======================================== Cover laminations =================================================*/
        renderCoverLaminations: function () {
            this.coverLaminationsContainer.html("");
            this.coverLaminations = this.coverType.laminations;
            if (this.coverLaminationEnabled && this.coverLaminations && Object.keys(this.coverLaminations).length > 0) {
                this.coverLaminationsSection.show();
                var coverLaminationHtml = this.bookCoverLaminationsTemplate(this.coverType);
                this.coverLaminationsContainer.html(coverLaminationHtml);
                this.selectDefaultLamination();
            } else {
                this.coverLamination = {};
                this.coverLaminationId = 0;
                this.coverLaminationsSection.hide();
            }
        },

        selectDefaultLamination: function () {
            if (this.coverLaminationEnabled && this.coverLaminations && Object.keys(this.coverLaminations).length > 0) {
                this.coverLaminationId = Object.keys(this.coverLaminations)[0];
                this.coverLamination = this.coverLaminations[this.coverLaminationId];
            }
        },

        /*============================================================================================================*/
        /*======================================== BOOK INNER BLOCK ==================================================*/
        /*============================================================================================================*/
        renderPaperTypes: function () {
            this.paperTypes = this.bookFormat.paper_types;
            this.paperTypesContainer.html("");
            if (this.paperTypes && Object.keys(this.paperTypes).length > 0) {
                this.paperTypesSection.show();
                var paperTypesHtml = this.bookPaperTypeTemplate(this.bookFormat);
                this.paperTypesContainer.html(paperTypesHtml);
                this.selectDefaultPaperType();
            } else {
                this.paperTypesSection.hide();
            }
        },

        selectDefaultPaperType: function () {
            if (this.paperTypes && Object.keys(this.paperTypes).length > 0) {
                var paperTypeId = Object.keys(this.paperTypes)[0];
                this.selectPaperType(paperTypeId);
            }
        },

        selectPaperType: function (paperTypeId) {
            console.log('selectPaperType invoked.');
            if (this.paperTypes && Object.keys(this.paperTypes).length > 0) {
                this.paperTypeId = paperTypeId;
                this.paperType = this.paperTypes[paperTypeId];
                this.renderPrints();
            }
        },

        renderPrints: function () {
            console.log('renderPrints invoked.');
            this.printTypes = this.paperType.print_types;
            var printsHtml = this.bookPrintsTemplate(this.printTypes);
            this.printsContainer.html(printsHtml);
            this.renderPrintTypes();
        },

        renderPrintTypes: function (printColor) {
            console.log('renderPrintTypes invoked.');

            if (this.printTypes && Object.keys(this.printTypes).length > 0) {

                this.bwPrintTypes = this.printTypes.bw;
                this.colorPrintTypes = this.printTypes.color;

                if (!printColor) {
                    if (this.bwPrintTypes && Object.keys(this.bwPrintTypes).length > 0) {
                        printColor = 'bw';
                    } else if (this.colorPrintTypes && Object.keys(this.colorPrintTypes).length > 0) {
                        printColor = 'color';
                    }
                }
                this.printColor = printColor;

                if (this.printColor == 'combined') {
                    this.renderBwPrintTypes();
                    this.renderColorPrintTypes();
                } else if (this.printColor == 'bw') {
                    this.renderBwPrintTypes();
                    this.printColorSection.css('display', 'none');
                } else if (this.printColor == 'color') {
                    this.renderColorPrintTypes();
                    this.printBwSection.css('display', 'none');
                }

            } else {
                this.printSection.css('display', 'none');
                this.printBwSection.css('display', 'none');
                this.printColorSection.css('display', 'none');
            }
        },

        renderBwPrintTypes: function () {
            this.bwPrintTypesContainer.html("");
            if (this.bwPrintTypes && Object.keys(this.bwPrintTypes).length > 0) {
                this.printBwSection.css('display', 'inline-block');
                var bwPrintTypesHtml = this.bookBwPrintTypesTemplate(this.printTypes);
                this.bwPrintTypesContainer.html(bwPrintTypesHtml);
                this.selectDefaultBwPrintType();
                if (Object.keys(this.bwPrintTypes).length > 1) {
                    this.printBwSection.css('display', 'inline-block');
                } else {
                    this.printBwSection.css('display', 'none');
                }
            } else {
                this.printBwSection.css('display', 'none');
            }
        },

        selectDefaultBwPrintType: function () {
            if (this.bwPrintTypes && Object.keys(this.bwPrintTypes).length > 0) {
                var bwPrintTypeId = Object.keys(this.bwPrintTypes)[0];
                this.selectBwPrintType(bwPrintTypeId);
            }
        },

        selectBwPrintType: function (bwPrintTypeId) {
            this.bwPrintTypeId = bwPrintTypeId;
            if (this.bwPrintTypes && Object.keys(this.bwPrintTypes).length > 0) {
                this.bwPrintType = this.bwPrintTypes[this.bwPrintTypeId];
                if (this.bwPrintType) {
                    var minPrintCount = this.bwPrintType.min_print_count;
                    //if (minPrintCount > this.minPrintCount) {
                        this.minPrintCount = minPrintCount;
                        this.printCountField.attr('min', this.minPrintCount);
                        if (this.minPrintCount > this.printCount) {
                            this.printCountField.val(this.minPrintCount);
                            this.printCount = this.minPrintCount;
                        }
                    //}
                }
            }
        },

        renderColorPrintTypes: function () {
            this.colorPrintTypesContainer.html("");
            if (this.colorPrintTypes && Object.keys(this.colorPrintTypes).length > 0) {
                this.printColorSection.css('display', 'inline-block');
                var colorPrintTypesHtml = this.bookColorPrintTypesTemplate(this.printTypes);
                this.colorPrintTypesContainer.html(colorPrintTypesHtml);
                this.selectDefaultColorPrintType();
                if (Object.keys(this.colorPrintTypes).length > 1) {
                    this.printColorSection.css('display', 'inline-block');
                } else {
                    this.printColorSection.css('display', 'none');
                }
            } else {
                this.printColorSection.css('display', 'none');
            }
        },

        selectDefaultColorPrintType: function () {
            if (this.colorPrintTypes && Object.keys(this.colorPrintTypes).length > 0) {
                var colorPrintTypeId = Object.keys(this.colorPrintTypes)[0];
                this.selectColorPrintType(colorPrintTypeId);
            }
        },

        selectColorPrintType: function (colorPrintTypeId) {
            this.colorPrintTypeId = colorPrintTypeId;
            if (this.colorPrintTypes && Object.keys(this.colorPrintTypes).length > 0) {
                this.colorPrintType = this.colorPrintTypes[this.colorPrintTypeId];
                if (this.colorPrintType) {
                    var minPrintCount = this.colorPrintType.min_print_count;
                    //if (minPrintCount > this.minPrintCount) {
                        this.minPrintCount = minPrintCount;
                        this.printCountField.attr('min', this.minPrintCount);
                        if (this.minPrintCount > this.printCount) {
                            this.printCountField.val(this.minPrintCount);
                            this.printCount = this.minPrintCount;
                        }
                    //}
                }
            }
        },

        /*============================================================================================================*/
        /*======================================= BINDING LISTENERS ==================================================*/
        /*============================================================================================================*/

        bindListeners: function () {
            this.container.on('click', '#book-types .product-item', {scope: this}, this.bookTypeClickHandler);
            this.container.on('click', '#book-formats .product-item', {scope: this}, this.bookFormatClickHandler);
            this.container.on('click', '#cover-types .product-item', {scope: this}, this.bookCoverTypeClickHandler);
            this.container.on('change', '#book-cover-paper-type-select', {scope: this}, this.bookCoverPaperTypeChangeHandler);
            this.container.on('change', '#cover-lamination-section', {scope: this}, this.bookCoverLaminationChangeHandler);
            this.container.on('change', '#paper-types-select', {scope: this}, this.paperTypeChangeHandler);
            this.container.on('click', '#prints input[type="radio"]', {scope: this}, this.printsToggleHandler);
            this.container.on('change', '#bw-print-types-select', {scope: this}, this.bwPrintTypeChangeHandler);
            this.container.on('change', '#color-print-types-select', {scope: this}, this.colorPrintTypeChangeHandler);


            this.container.on('change', '#page-count', {scope: this}, this.pageCountChangeHandler);
            this.container.on('change', '#color-page-count', {scope: this}, this.colorPageCountChangeHandler);

            this.container.on('change', '#print-count', {scope: this}, this.printCountChangeHandler);

            this.container.on('change', '.extra-option', {scope: this}, this.extraOptionChangeHandler);

            this.priceArea.on('click', '.close-btn', {scope: this}, this.closeBtnClickHandler);
        },


        /*============================================================================================================*/
        /*========================================== EVENT HANDLERS ==================================================*/
        /*============================================================================================================*/
        bookTypeClickHandler: function (event) {
            var scope = event.data.scope;
            var bookTypeId = $(this).data("id");

            scope.resetBookFormats(scope);
            scope.resetCoverTypes(scope);
            scope.resetCoverPaperTypes(scope);
            scope.resetCoverLaminations(scope);
            scope.resetPaperTypes(scope);
            scope.resetPrintTypes(scope);

            scope.selectBookType(bookTypeId);
            scope.calculatePrice(scope);
        },

        bookFormatClickHandler: function (event) {
            var scope = event.data.scope;

            scope.resetCoverTypes(scope);
            scope.resetCoverPaperTypes(scope);
            scope.resetCoverLaminations(scope);
            scope.resetPaperTypes(scope);
            scope.resetPrintTypes(scope);

            var bookFormatId = $(this).data("id");
            scope.selectBookFormat(bookFormatId);
            scope.calculatePrice(scope);
        },

        bookCoverTypeClickHandler: function (event) {
            var scope = event.data.scope;

            scope.resetCoverPaperTypes(scope);
            scope.resetCoverLaminations(scope);

            var coverTypeId = $(this).data("id");
            scope.selectCoverType(coverTypeId);
            scope.calculatePrice(scope);
        },

        bookCoverPaperTypeChangeHandler: function (event) {
            var scope = event.data.scope;
            scope.coverPaperTypeId = $(this).find("option:selected").data("id");
            scope.coverPaperType = scope.coverPaperTypes[scope.coverPaperTypeId];
            scope.coverLaminationEnabled = scope.coverPaperType.is_laminated;
            if (!scope.coverLaminationEnabled) {
                scope.coverLaminationsSection.hide();
            } else {
                scope.coverLaminationsSection.show();
            }
            scope.calculatePrice(scope);
        },

        bookCoverLaminationChangeHandler: function (event) {
            var scope = event.data.scope;
            scope.coverLaminationId = $(this).find("option:selected").data("id");
            scope.coverLamination = scope.coverLaminations[scope.coverLaminationId];
            scope.calculatePrice(scope);
        },

        paperTypeChangeHandler: function (event) {
            console.log('paperTypeChangeHandler invoked.');
            var scope = event.data.scope;
            scope.resetPrintTypes(scope);
            var paperTypeId = $(this).find("option:selected").data("id");
            scope.selectPaperType(paperTypeId);
            scope.renderPrints();
            scope.calculatePrice(scope);
        },

        bwPrintTypeChangeHandler: function (event) {
            console.log('bwPrintTypeChangeHandler invoked.');
            var scope = event.data.scope;
            var bwPrintTypeId = $(this).find("option:selected").data("id");
            scope.selectBwPrintType(bwPrintTypeId);
            scope.calculatePrice(scope);
        },

        colorPrintTypeChangeHandler: function (event) {
            console.log('colorPrintTypeChangeHandler invoked.');
            var scope = event.data.scope;
            var colorPrintTypeId = $(this).find("option:selected").data("id");
            scope.selectColorPrintType(colorPrintTypeId);
            scope.calculatePrice(scope);
        },

        printsToggleHandler: function (event) {
            console.log('printsToggleHandler invoked.');
            var scope = event.data.scope;
            var value = $(this).val();
            if (value == 'combined') {
                scope.printColor = 'combined';
                scope.colorPageCountSection.css('display', 'inline-block');
                scope.renderPrintTypes('combined');
            } else if (value == 'bw') {
                scope.printColor = 'bw';
                scope.colorPageCountSection.css('display', 'none');
                scope.renderPrintTypes('bw');
            } else if (value == 'color') {
                scope.printColor = 'color';
                scope.colorPageCountSection.css('display', 'none');
                scope.renderPrintTypes('color');
            }
            scope.calculatePrice(scope);
        },

        pageCountChangeHandler: function (event) {
            var scope = event.data.scope;
            var pageCount = $(this).val();

            if (pageCount % scope.pageCountStep != 0) {
                do {
                    pageCount++;
                } while (pageCount % scope.pageCountStep != 0);
            }
            if (pageCount > scope.maxPageCount) {
                pageCount = scope.maxPageCount;
            } else if (pageCount < scope.minPageCount) {
                pageCount = scope.minPageCount;
            }
            $(this).val(pageCount);
            scope.pageCount = parseInt(pageCount);
            scope.calculatePrice(scope);
        },

        colorPageCountChangeHandler: function (event) {
            var scope = event.data.scope;
            var colorPageCount = $(this).val();
            if (colorPageCount % scope.pageCountStep != 0) {
                do {
                    colorPageCount++;
                } while (colorPageCount % scope.pageCountStep != 0);
            }
            if (colorPageCount > scope.pageCount) {
                colorPageCount = scope.pageCount;
            } else if (colorPageCount < scope.pageCountStep) {
                colorPageCount = scope.pageCountStep;
            }
            scope.colorPageCount = colorPageCount;
            $(this).val(colorPageCount);
            scope.colorPageCount = parseInt(colorPageCount);
            scope.calculatePrice(scope);
        },

        extraOptionChangeHandler: function (event) {
            var scope = event.data.scope;
            scope.calculatePrice(scope);
        },

        closeBtnClickHandler: function (event) {
            // var mainPageLink = 'http://kollage.com.ua/services/article/24/57';
            window.history.back();
        },

        printCountChangeHandler: function (event) {
            var scope = event.data.scope;
            var printCount = $(this).val();
            if (printCount > scope.maxPrintCount) {
                printCount = scope.maxPrintCount;
            } else if (printCount < scope.minPrintCount) {
                printCount = scope.minPrintCount;
            }
            $(this).val(printCount);
            scope.printCount = printCount;
            $(".print-count-value").html(scope.printCount);
            scope.calculatePrice(scope);
        },

        /*============================================================================================================*/
        /*============================================= HELPERS ======================================================*/
        /*============================================================================================================*/
        resetBookFormats: function (scope) {
            scope.bookFormatId = 0;
            scope.bookFormats = {};
            scope.bookFormat = {};
        },

        resetCoverTypes: function (scope) {
            scope.coverTypeId = 0;
            scope.coverTypes = {};
            scope.coverType = {};
        },

        resetCoverPaperTypes: function (scope) {
            scope.coverPaperTypeId = 0;
            scope.coverPaperTypes = {};
            scope.coverPaperType = {};
        },

        resetCoverLaminations: function (scope) {
            scope.coverLaminationId = 0;
            scope.coverLaminations = {};
            scope.coverLamination = {};
        },

        resetPaperTypes: function (scope) {
            scope.paperTypeId = 0;
            scope.paperTypes = {};
            scope.paperType = {};
        },

        resetPrintTypes: function (scope) {
            scope.printTypes = {};
            scope.printColor = 'bw';
            scope.bwPrintTypeId = 0;
            scope.bwPrintTypes = {};
            scope.bwPrintType = {};
            scope.colorPrintTypeId = 0;
            scope.colorPrintTypes = {};
            scope.colorPrintType = {};
            scope.resetPrintTypesUI(scope);
        },

        resetPrintTypesUI: function (scope) {
            scope.colorPageCountSection.css('display', 'none');
            scope.printBwSection.css('display', 'none');
            scope.printColorSection.css('display', 'none');
        },

        /*============================================================================================================*/
        /*============================================= CALCULATION ==================================================*/
        /*============================================================================================================*/
        calculatePrice: function (scope) {

            console.log('calculatePrice invoked.');

            var priceInfo = {};

            //Set Page Count
            priceInfo = this.setPageCount(priceInfo);

            //Set Common Info
            priceInfo = this.setProductInfo(priceInfo);

            //Cover Price
            var coverBasePrice = this.calculateCoverBasePrice();
            var coverPaperTypePrice = this.calculateCoverPaperTypePrice();
            var coverLaminationPrice = this.calculateCoverLaminationPrice();
            var coverPrice = coverBasePrice + coverPaperTypePrice + coverLaminationPrice;
            priceInfo.book_cover_base_price = this.formatPrice(coverBasePrice);
            priceInfo.book_cover_paper_price = this.formatPrice(coverPaperTypePrice);
            priceInfo.book_cover_lam_price = this.formatPrice(coverLaminationPrice);
            priceInfo.book_cover_price = parseFloat(Math.round(coverPrice * 100) / 100).toFixed(2);

            //Inner Block Price
            var paper_price = this.calculatePaperPrice();
            var bw_print_price = this.calculateBwPrintPrice();
            var color_print_price = this.calculateColorPrintPrice();
            priceInfo.paper_price = this.formatPrice(paper_price);
            priceInfo.bw_print_price = this.formatPrice(bw_print_price);
            priceInfo.color_print_price = this.formatPrice(color_print_price);
            var innerPrice = (paper_price / scope.paperPriceDivider) * priceInfo.page_count
                            + bw_print_price * priceInfo.bw_page_count
                            + color_print_price * priceInfo.color_page_count;
            priceInfo.inner_price = this.formatPrice(innerPrice);

            //Discount
            var discount = 0;
            if (this.bookType) {
                var threshold1 = this.bookType.threshold1;
                var threshold2 = this.bookType.threshold2;
                var threshold3 = this.bookType.threshold3;
                var threshold4 = this.bookType.threshold4;
                var printCount = this.printCount;
                if (printCount >= threshold4) {
                    discount = this.bookType.discount4;
                } else if (printCount >= threshold3) {
                    discount = this.bookType.discount3;
                } else if (printCount >= threshold2) {
                    discount = this.bookType.discount2;
                } else if (printCount >= threshold1) {
                    discount = this.bookType.discount1;
                }
                priceInfo.discount = discount;
            }

            //Item Price
            var itemPrice = innerPrice + coverPrice;
            if (discount > 0) {
                itemPrice = ((innerPrice + coverPrice) * (100 - discount)) / 100;
            }
            priceInfo.item_price = this.formatPrice(itemPrice, 2);

            //Print Count
            priceInfo.print_count = this.printCount;

            //Extra Options
            var extraOptionsObj = this.calculateExtraOptions(priceInfo);
            var extraOptions = extraOptionsObj.extra_options;
            var extraOptionsPrice = extraOptionsObj.extra_options_price;
            priceInfo.extra_options = extraOptions;
            priceInfo.extra_options_price = this.formatPrice(extraOptionsPrice, 2);

            //Total Price
            var totalPrice = itemPrice * priceInfo.print_count + extraOptionsPrice;
            priceInfo.total_price = this.formatPrice(totalPrice, 2);

            var priceInfoHtml = this.priceTemplate(priceInfo);
            this.priceContainer.html(priceInfoHtml);

        },

        calculateCoverBasePrice: function () {
            if (this.coverType) {
                var book_cover_base_price = this.coverType.price;
                return book_cover_base_price > 0 ? book_cover_base_price : 0;
            }
            return 0;
        },

        calculateCoverPaperTypePrice: function () {
            if (this.coverPaperType) {
                var book_cover_paper_price = this.coverPaperType.price;
                return book_cover_paper_price > 0 ? book_cover_paper_price : 0;
            }
            return 0;
        },

        calculateCoverLaminationPrice: function () {
            if (this.coverLaminationEnabled && this.coverLamination) {
                var book_cover_lam_price = this.coverLamination.price;
                return book_cover_lam_price > 0 ? book_cover_lam_price : 0;
            }
            return 0;
        },

        calculatePaperPrice: function () {
            if (this.paperType) {
                return this.paperType.price > 0 ? this.paperType.price : 0;
            }
            return 0;
        },


        calculateBwPrintPrice: function () {
            if (this.bwPrintType) {
                return this.bwPrintType.price > 0 ? this.bwPrintType.price : 0;
            }
            return 0;
        },

        calculateColorPrintPrice: function () {
            if (this.colorPrintType) {
                return this.colorPrintType.price > 0 ? this.colorPrintType.price : 0;
            }
            return 0;
        },

        calculateExtraOptions: function (priceInfo) {
            var extraOptionsObj = {};
            var extraOptionsPrice = 0;
            var extraOptions = [];
            if (this.bookType && this.bookType.options && Object.keys(this.bookType.options).length > 0) {
                var checkedOptions = $(".extra-option:checked");
                for (var i = 0; i < checkedOptions.length; i++) {
                    var checkedOption = $(checkedOptions[i]);
                    var extraOptionId = checkedOption.data("id");
                    var option = this.bookType.options[extraOptionId];
                    var extraOptionPrice = this.calculateExtraOptionPrice(option, priceInfo);
                    var formattedPrice = this.formatPrice(extraOptionPrice);
                    var extraOption = {
                        id: extraOptionId,
                        name: option.name,
                        price: extraOptionPrice,
                        price_formatted: formattedPrice
                    };
                    extraOptionsPrice += extraOptionPrice;
                    extraOptions.push(extraOption);
                }
            }

            extraOptionsObj.extra_options = extraOptions;
            extraOptionsObj.extra_options_price = extraOptionsPrice;

            console.log('calculateExtraOptions, extraOptionsObj:');
            console.log(extraOptionsObj);

            return extraOptionsObj;
        },

        calculateExtraOptionPrice: function (option, priceInfo) {

            var discount = parseFloat(priceInfo.discount);

            var itemsCount = priceInfo.print_count;
            var pageCount = priceInfo.page_count;

            var pricePage = parseFloat(option.price_page);
            var priceItem = parseFloat(option.price_item);
            var priceOrder = parseFloat(option.price_order);

            return priceOrder + priceItem * itemsCount + pricePage * pageCount;
        },

        setProductInfo: function (priceInfo) {
            console.log('setProductInfo.')
            if (this.bookType) {
                priceInfo.book_type = this.bookType.name;
            } else {
                priceInfo.book_type = '-';
            }

            if (this.bookFormat) {
                priceInfo.book_format = this.bookFormat.name;
            } else {
                priceInfo.book_format = '-';
            }

            if (this.coverType) {
                priceInfo.book_cover = this.coverType.name;
            } else {
                priceInfo.book_cover = '-';
            }

            if (this.coverLaminationEnabled && this.coverLamination) {
                priceInfo.lamination = this.coverLamination.name;
            }
            if (this.coverPaperType) {
                priceInfo.cover_paper = this.coverPaperType.name;
            }

            if (this.paperType) {
                priceInfo.paper = this.paperType.name;
            }

            if (this.printColor == 'bw') {
                if (this.bwPrintType) {
                    priceInfo.print_type = this.bwPrintType.name;
                }
            } else if (this.printColor == 'color') {
                if (this.colorPrintType) {
                    priceInfo.print_type = this.colorPrintType.name;
                }
            } else if (this.printColor == 'combined') {
                if (this.bwPrintType) {
                    priceInfo.print_type = this.bwPrintType.name;
                }
                if (this.colorPrintType) {
                    priceInfo.print_type += " / ";
                    priceInfo.print_type += this.colorPrintType.name;
                }
            }

            return priceInfo;
        },

        setPageCount: function (priceInfo) {
            if (this.pageCount) {
                priceInfo.page_count = this.pageCount;
            } else {
                priceInfo.page_count = 0;
            }
            if (this.printColor == 'bw') {
                priceInfo.bw_page_count = priceInfo.page_count;
                priceInfo.color_page_count = 0;
            } else if (this.printColor == 'color') {
                priceInfo.color_print = true;
                priceInfo.color_page_count = priceInfo.page_count;
                priceInfo.bw_page_count = 0;
            } else if (this.printColor == 'combined') {
                priceInfo.color_print = true;
                if (this.colorPageCount) {
                    priceInfo.color_page_count = this.colorPageCount;
                } else {
                    priceInfo.color_page_count = 0;
                }
                priceInfo.bw_page_count = priceInfo.page_count - priceInfo.color_page_count;
            }
            return priceInfo;
        },

        formatPrice: function (price, symbols) {
            if (!symbols) {
                symbols = 3;
            }
            return parseFloat(Math.round(price * 1000) / 1000).toFixed(symbols);
        }


    }

});
window.BookCalculator = BookCalculator;
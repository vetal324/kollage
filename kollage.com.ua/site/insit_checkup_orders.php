<?php

require('lib/util.php');
load_configuration();
initialize_session( 'frontend' );

require('lib/init.php');

$base = load_controller( 'base' );
$base->insit_checkup_orders();
$base->insit_checkup_ready_orders();

?>
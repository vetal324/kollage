<?php

require('lib/util.php');
load_configuration();
initialize_session( 'frontend' );

require('lib/init.php');

$base = load_controller( 'base' );
$base->clear_tmp_orders();

?>
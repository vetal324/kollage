<?php
/*****************************************************
 Class v.1.0, 2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

class Settings extends Model
{
    var $tax, $use_tax_in_sop, $use_tax_in_shop, $use_tax_in_fair;
    var $activation_mail_subject, $activation_mail_body, $register_mail_subject, $register_mail_body,
        $password_reminder_mail_subject, $password_reminder_mail_body, $register_mail2admin_subject,
        $register_mail2admin_body, $confirm_prof_subject, $confirm_prof_body, $prof_confirmed_subject, $prof_confirmed_body, $contacts_at_top, $confirm_ads_subject, $confirm_ads_body, $ads_confirmed_subject, $ads_confirmed_body;
    var $from_name, $from_email;
    var $service_file_price, $service_file_price_opt, $service_file_price_opt2;
    
    var $tablename = 'settings';

    function Settings( $id=1 )
    {
        parent::Model( $id );
    }
    
    function _Load( &$row )
    {
        $this->id = intval( $row['id'] );
        $this->tax = intval( $row['tax'] );
        $this->use_tax_in_sop = intval( $row['use_tax_in_sop'] );
        $this->use_tax_in_shop = intval( $row['use_tax_in_shop'] );
        $this->use_tax_in_fair = intval( $row['use_tax_in_fair'] );
        $this->from_name = $row['from_name'];
        $this->from_email = $row['from_email'];
        $this->service_file_price = $row['service_file_price'];
        $this->service_file_price_opt = $row['service_file_price_opt'];
        $this->service_file_price_opt2 = $row['service_file_price_opt2'];
        
        $t = new MysqlTable('dictionary');
        if( $t->find_all( "parent_id={$this->id} and folder='{$this->tablename}' and lang='". $_SESSION['lang'] ."'" ) )
        {
            foreach( $t->data as $row2 )
            {
                switch( $row2['name'] )
                {
                    case 'activation_mail_subject';
                        $this->activation_mail_subject = $row2['value'];
                        break;
                    case 'activation_mail_body';
                        $this->activation_mail_body = $row2['value'];
                        break;
                    case 'register_mail_subject';
                        $this->register_mail_subject = $row2['value'];
                        break;
                    case 'register_mail_body';
                        $this->register_mail_body = $row2['value'];
                        break;
                    case 'password_reminder_mail_subject';
                        $this->password_reminder_mail_subject = $row2['value'];
                        break;
                    case 'password_reminder_mail_body';
                        $this->password_reminder_mail_body = $row2['value'];
                        break;
                    case 'register_mail2admin_subject';
                        $this->register_mail2admin_subject = $row2['value'];
                        break;
                    case 'register_mail2admin_body';
                        $this->register_mail2admin_body = $row2['value'];
                        break;
                    case 'confirm_prof_subject';
                        $this->confirm_prof_subject = $row2['value'];
                        break;
                    case 'confirm_prof_body';
                        $this->confirm_prof_body = $row2['value'];
                        break;
                    case 'prof_confirmed_subject';
                        $this->prof_confirmed_subject = $row2['value'];
                        break;
                    case 'prof_confirmed_body';
                        $this->prof_confirmed_body = $row2['value'];
                        break;
                    case 'contacts_at_top';
                        $this->contacts_at_top = $row2['value'];
                        break;
                    case 'confirm_ads_subject';
                        $this->confirm_ads_subject = $row2['value'];
                        break;
                    case 'confirm_ads_body';
                        $this->confirm_ads_body = $row2['value'];
                        break;
                    case 'ads_confirmed_subject';
                        $this->ads_confirmed_subject = $row2['value'];
                        break;
                    case 'ads_confirmed_body';
                        $this->ads_confirmed_body = $row2['value'];
                        break;
                }
            }
        }
        
        $this->loaded = true;
    }
    
    function Save( $data=null )
    {   
        $this->CheckupData();
        
        $t = new MysqlTable( $this->tablename );
        if( is_array($data) ) $t->save( $data );
        else
        {
            $data = Array();
            $data['id'] = $this->id;
            $data['tax'] = $this->tax;
            $data['use_tax_in_sop'] = $this->use_tax_in_sop;
            $data['use_tax_in_shop'] = $this->use_tax_in_shop;
            $data['use_tax_in_fair'] = $this->use_tax_in_fair;
            $data['activation_mail_subject'] = $this->activation_mail_subject;
            $data['activation_mail_body'] = $this->activation_mail_body;
            $data['register_mail_subject'] = $this->register_mail_subject;
            $data['register_mail_body'] = $this->register_mail_body;
            $data['password_reminder_mail_subject'] = $this->password_reminder_mail_subject;
            $data['password_reminder_mail_body'] = $this->password_reminder_mail_body;
            $data['from_name'] = $this->from_name;
            $data['from_email'] = $this->from_email;
            $data['service_file_price'] = $this->service_file_price;
            $data['service_file_price_opt'] = $this->service_file_price_opt;
            $data['service_file_price_opt2'] = $this->service_file_price_opt2;
            $data['register_mail2admin_subject'] = $this->register_mail2admin_subject;
            $data['register_mail2admin_body'] = $this->register_mail2admin_body;
            $data['confirm_prof_subject'] = $this->confirm_prof_subject;
            $data['confirm_prof_body'] = $this->confirm_prof_body;
            $data['prof_confirmed_subject'] = $this->prof_confirmed_subject;
            $data['prof_confirmed_body'] = $this->prof_confirmed_body;
            $data['contacts_at_top'] = $this->contacts_at_top;
            $data['confirm_ads_subject'] = $this->confirm_ads_subject;
            $data['confirm_ads_body'] = $this->confirm_ads_body;
            $data['ads_confirmed_subject'] = $this->ads_confirmed_subject;
            $data['ads_confirmed_body'] = $this->ads_confirmed_body;
            
            $t->save( $data );
        }

        $this->Load( $t->get_last_insert_id() );
        
        return( $this->id );
    }
    
    function DeleteServiceFilePrice()
    {
        if( $this->IsLoaded() )
        {
            $attachment = new Attachment();
            $attachment->filename = $this->service_file_price;
            $attachment->rm();
            
            $this->service_file_price = '';
            $this->Save();
        }
    }
    
    function DeleteServiceFilePriceOpt()
    {
        if( $this->IsLoaded() )
        {
            $attachment = new Attachment();
            $attachment->filename = $this->service_file_price_opt;
            $attachment->rm();
            
            $this->service_file_price_opt = '';
            $this->Save();
        }
    }
    
    function DeleteServiceFilePriceOpt2()
    {
        if( $this->IsLoaded() )
        {
            $attachment = new Attachment();
            $attachment->filename = $this->service_file_price_opt2;
            $attachment->rm();
            
            $this->service_file_price_opt2 = '';
            $this->Save();
        }
    }
    
    function Xml()
    {
        $retval = "<settings>";
        $retval .= "<id>{$this->id}</id>";
        $retval .= "<tax>{$this->tax}</tax>";
        $retval .= "<use_tax_in_sop>{$this->use_tax_in_sop}</use_tax_in_sop>";
        $retval .= "<use_tax_in_shop>{$this->use_tax_in_shop}</use_tax_in_shop>";
        $retval .= "<use_tax_in_fair>{$this->use_tax_in_fair}</use_tax_in_fair>";
        $retval .= "<activation_mail_subject><![CDATA[{$this->activation_mail_subject}]]></activation_mail_subject>";
        $retval .= "<activation_mail_body><![CDATA[{$this->activation_mail_body}]]></activation_mail_body>";
        $retval .= "<register_mail_subject><![CDATA[{$this->register_mail_subject}]]></register_mail_subject>";
        $retval .= "<register_mail_body><![CDATA[{$this->register_mail_body}]]></register_mail_body>";
        $retval .= "<password_reminder_mail_subject><![CDATA[{$this->password_reminder_mail_subject}]]></password_reminder_mail_subject>";
        $retval .= "<password_reminder_mail_body><![CDATA[{$this->password_reminder_mail_body}]]></password_reminder_mail_body>";
        $retval .= "<from_name><![CDATA[{$this->from_name}]]></from_name>";
        $retval .= "<from_email><![CDATA[{$this->from_email}]]></from_email>";
        $retval .= "<service_file_price><![CDATA[{$this->service_file_price}]]></service_file_price>";
        $retval .= "<service_file_price_opt><![CDATA[{$this->service_file_price_opt}]]></service_file_price_opt>";
        $retval .= "<service_file_price_opt2><![CDATA[{$this->service_file_price_opt2}]]></service_file_price_opt2>";
        $retval .= "<register_mail2admin_subject><![CDATA[{$this->register_mail2admin_subject}]]></register_mail2admin_subject>";
        $retval .= "<register_mail2admin_body><![CDATA[{$this->register_mail2admin_body}]]></register_mail2admin_body>";
        $retval .= "<confirm_prof_subject><![CDATA[{$this->confirm_prof_subject}]]></confirm_prof_subject>";
        $retval .= "<confirm_prof_body><![CDATA[{$this->confirm_prof_body}]]></confirm_prof_body>";
        $retval .= "<prof_confirmed_subject><![CDATA[{$this->prof_confirmed_subject}]]></prof_confirmed_subject>";
        $retval .= "<prof_confirmed_body><![CDATA[{$this->prof_confirmed_body}]]></prof_confirmed_body>";
        $retval .= "<contacts_at_top><![CDATA[{$this->contacts_at_top}]]></contacts_at_top>";
        $retval .= "<confirm_ads_subject><![CDATA[{$this->confirm_ads_subject}]]></confirm_ads_subject>";
        $retval .= "<confirm_ads_body><![CDATA[{$this->confirm_ads_body}]]></confirm_ads_body>";
        $retval .= "<ads_confirmed_subject><![CDATA[{$this->ads_confirmed_subject}]]></ads_confirmed_subject>";
        $retval .= "<ads_confirmed_body><![CDATA[{$this->ads_confirmed_body}]]></ads_confirmed_body>";
        $retval .= "</settings>";
        
        return( $retval );
    }
}

?>
<?php
/*****************************************************
 Class v.1.0, 2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

class SOPOrderPhoto extends Model
{
    var $order_id, $quantity, $size, $size_id, $price, $thumbnail, $small, $small_w, $small_h, $filename;
    
    var $tablename = 'sop_order_photos';

    function SOPOrderPhoto( $id=0 )
    {
        $this->quantity = 1;
        
        $t = new MysqlTable('sop_order_photos_sizes');
        if( $row = $t->find_first( "status=1", "position,id" ) )
        {
            $this->size_id = $row['id'];
        }
        
        parent::Model( $id );
    }
    
    function _Load( &$row )
    {
        $this->id = intval( $row['id'] );
        $this->order_id = intval( $row['order_id'] );
        $this->quantity = intval( $row['quantity'] );
        $this->size_id = intval( $row['size_id'] );
        $this->price = floatval( $row['price'] );
        $this->thumbnail = $row['thumbnail'];
        $this->small = $row['small'];
        $this->small_w = $row['small_w'];
        $this->small_h = $row['small_h'];
        $this->filename = $row['filename'];
        
        $md = new MysqlDateTime();
        $md->Parse( $row['created_at'] );
        $this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
        
        $this->updated_at = $row['updated_at'];
        
        $this->size = new SOPOrderPhotoSize( $this->size_id );
        
        $this->loaded = true;
    }
    
    function Save( $data=null )
    {   
        $this->CheckupData();
        
        $t = new MysqlTable( $this->tablename );
        if( is_array($data) ) $t->save( $data );
        else
        {
            $data = Array();
            $data['id'] = $this->id;
            $data['order_id'] = $this->order_id;
            $data['quantity'] = $this->quantity;
            $data['size_id'] = $this->size_id;
            $data['price'] = $this->price;
            $data['thumbnail'] = $this->thumbnail;
            $data['small'] = $this->small;
            $data['small_w'] = $this->small_w;
            $data['small_h'] = $this->small_h;
            $data['filename'] = $this->filename;
            
            $t->save( $data );
        }
            
        $this->Load( $t->get_last_insert_id() );
        
        return( $this->id );
    }
    
    function Delete()
    {
        if( $this->IsLoaded() )
        {
            $this->DeleteFiles();
            parent::Delete();
        }
    }
    
    function DeleteFiles()
    {
        if( $this->IsLoaded() )
        {
            $order = new SOPOrder( $this->order_id );
            if( $order->IsLoaded() )
            {
                $dir = $order->dir ."/". $order->order_number ."/";
                if( file_exists( $dir . $this->filename ) ) unlink( $dir . $this->filename );
                if( file_exists( $dir . $this->thumbnail ) ) unlink( $dir . $this->thumbnail );
                if( file_exists( $dir . $this->small ) ) unlink( $dir . $this->small );
            }
        }
    }
    
    function Xml()
    {
        $retval = "<photo>";
        $retval .= "<id>{$this->id}</id>";
        $retval .= "<order_id>{$this->order_id}</order_id>";
        $retval .= "<size_id>{$this->size_id}</size_id>";
        $retval .= "<price>{$this->price}</price>";
        $retval .= "<quantity>{$this->quantity}</quantity>";
        $retval .= "<thumbnail>{$this->thumbnail}</thumbnail>";
        $retval .= "<small width='{$this->small_w}' height='{$this->small_h}'>{$this->small}</small>";
        $retval .= "<filename>{$this->filename}</filename>";
        $retval .= "<created_at>{$this->created_at}</created_at>";
        $retval .= "<updated_at>{$this->updated_at}</updated_at>";
        
        $retval .= $this->size->Xml();
        
        $retval .= "</photo>";
        
        return( $retval );
    }
}

?>
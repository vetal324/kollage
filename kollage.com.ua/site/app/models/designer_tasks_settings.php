<?php

class DesignerTasksSettings extends Model
{
	var $operator_emails, $order_mail_body;
	
	var $tablename = 'designer_tasks_settings';

	function DesignerTasksSettings( $id=1 )
	{
		parent::Model( $id );
	}
	
	function _Load( &$row )
	{
		$this->id = intval( $row['id'] );
		$this->operator_emails = $row['operator_emails'];
		$this->designer_emails = $row['designer_emails'];
		$this->loaded = true;
	}
	
	function Save( $data=null )
	{   
		$this->CheckupData();
		
		$t = new MysqlTable( $this->tablename );
		if( is_array($data) ) $t->save( $data );
		else
		{
			$data = Array();
			$data['id'] = $this->id;
			$data['operator_emails'] = $this->operator_emails;
			$data['designer_emails'] = $this->designer_emails;
			
			$t->save( $data );
		}

		$this->Load( $t->get_last_insert_id() );
		
		return( $this->id );
	}
	
	function Xml()
	{
		$retval = "<settings>";
		$retval .= "<id>{$this->id}</id>";
		$retval .= "<operator_emails><![CDATA[{$this->operator_emails}]]></operator_emails>";
		$retval .= "<designer_emails><![CDATA[{$this->designer_emails}]]></designer_emails>";
		$retval .= "</settings>";
		
		return( $retval );
	}
}

?>
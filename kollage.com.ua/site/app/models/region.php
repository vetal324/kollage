<?php
/*****************************************************
 Class v.1.0, 2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

class Region extends Model
{
    var $folder, $name, $description;
    var $image, $image_width, $image_height;
    
    var $tablename = 'regions';

    function Region( $id=0 )
    {
        parent::Model( $id );
    }
    
    function _Load( &$row )
    {
        $this->id = intval( $row['id'] );
        $this->folder = $row['folder'];
        $this->image = $row['image'];
        $this->image_width = $row['image_width'];
        $this->image_height = $row['image_height'];
        
        $this->position = intval( $row['position'] );
        
        $t = new MysqlTable('dictionary');
        if( $t->find_all( "parent_id={$this->id} and folder='{$this->tablename}' and lang='". $_SESSION['lang'] ."'" ) )
        {
            foreach( $t->data as $row2 )
            {
                switch( $row2['name'] )
                {
                    case 'name';
                        $this->name = $row2['value'];
                        break;
                    case 'description';
                        $this->description = $row2['value'];
                        break;
                }
            }
        }
        
        $md = new MysqlDateTime();
        $md->Parse( $row['created_at'] );
        $this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
        
        $this->updated_at = $row['updated_at'];
        
        $this->loaded = true;
    }
    
    function Save( $data=null )
    {   
        $this->CheckupData();
        
        $t = new MysqlTable( $this->tablename );
        if( is_array($data) ) $t->save( $data );
        else
        {
            $data = Array();
            $data['id'] = $this->id;
            $data['folder'] = $this->folder;
            $data['name'] = $this->name;
            $data['description'] = $this->description;
            $data['position'] = $this->position;
            $data['image'] = $this->image;
            $data['image_width'] = $this->image_width;
            $data['image_height'] = $this->image_height;
            
            $t->save( $data );
        }
            
        $this->Load( $t->get_last_insert_id() );
        
        return( $this->id );
    }
    
    function DeleteImage()
    {
        if( $this->IsLoaded() )
        {
            $attachment = new Attachment();
            $attachment->filename = $this->image;
            $attachment->rm();
            
            $this->image = '';
            $this->Save();
        }
    }
    
    function Delete()
    {
        if( $this->IsLoaded() )
        {
            $this->DeleteImage();
            $t = new MysqlTable( $this->tablename );
            $t->del( $this->id );
        }
    }
    
    function Xml()
    {
        $retval = "<region>";
            $retval .= "<id>{$this->id}</id>";
            $retval .= "<folder>{$this->folder}</folder>";
            $retval .= "<name><![CDATA[{$this->name}]]></name>";
            $retval .= "<description><![CDATA[{$this->description}]]></description>";
            $retval .= "<image width='{$this->image_width}' height='{$this->image_height}'>{$this->image}</image>";
            $retval .= "<created_at>{$this->created_at}</created_at>";
            $retval .= "<updated_at>{$this->updated_at}</updated_at>";
        $retval .= "</region>";
        
        return( $retval );
    }
}

?>
<?php

class SOPOrderPhotoSize extends Model
{
    var $name, $price, $status, $description, $weight;
    var $price1; //Розница
    var $price2; //Оптовик
    var $price3; //Крупный оптовик
    var $price4; //Рекламный агент
    var $price5; //Профессионал
    
    var $tablename = 'sop_order_photos_sizes';

    function SOPOrderPhotoSize( $id=0 )
    {
        $this->status = 1;
        parent::Model( $id );
    }
    
    function _Load( &$row )
    {
        $this->id = intval( $row['id'] );
        $this->name = $row['name'];

        $this->price = sprintf( "%01.2f", floatval( $row['price'] ) );

        $this->price1 = sprintf( "%01.2f", floatval( $row['price1'] ) );
        $this->price2 = sprintf( "%01.2f", floatval( $row['price2'] ) );
        $this->price3 = sprintf( "%01.2f", floatval( $row['price3'] ) );
        $this->price4 = sprintf( "%01.2f", floatval( $row['price4'] ) );
        $this->price5 = sprintf( "%01.2f", floatval( $row['price5'] ) );

        $this->weight = sprintf( "%01.1f", floatval( $row['weight'] ) );
        
        $this->status = intval( $row['status'] );
        $this->position = intval( $row['position'] );
        
        $t = new MysqlTable('dictionary');
        if( $t->find_all( "parent_id={$this->id} and folder='{$this->tablename}' and lang='". $_SESSION['lang'] ."'" ) )
        {
            foreach( $t->data as $row2 )
            {
                switch( $row2['name'] )
                {
                    case 'description';
                        $this->description = $row2['value'];
                        break;
                }
            }
        }
        
        $md = new MysqlDateTime();
        $md->Parse( $row['created_at'] );
        $this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
        
        $this->updated_at = $row['updated_at'];
        
        $this->loaded = true;
    }
    
    function Save( $data=null )
    {   
        $this->CheckupData();
        
        $t = new MysqlTable( $this->tablename );
        if( is_array($data) ) $t->save( $data );
        else
        {
            $data = Array();
            $data['id'] = $this->id;
            $data['name'] = $this->name;
            $data['price'] = $this->price;
            $data['price1'] = $this->price1;
            $data['price2'] = $this->price2;
            $data['price3'] = $this->price3;
            $data['price4'] = $this->price4;
            $data['price5'] = $this->price5;
            $data['weight'] = $this->weight;
            $data['status'] = $this->status;
            $data['description'] = $this->description;
            $data['position'] = $this->position;
            
            $t->save( $data );
        }
            
        $this->Load( $t->get_last_insert_id() );
        
        return( $this->id );
    }
    
    function Delete()
    {
        if( $this->IsLoaded() )
        {
            $this->status = 0;
            $this->Save();
        }
    }

    function GetPrice( $client_type=0 ) {
        $retval = $this->price1;

        switch( $client_type ) {
            case ClientType::RETAIL:
                $retval = $this->price1;
                break;
            case ClientType::WHOLESALE:
                $retval = $this->price2;
                break;
            case ClientType::BIG_WHOLESALE:
                $retval = $this->price3;
                break;
            case ClientType::ADMAN:
                $retval = $this->price4;
                break;
            case ClientType::PROFESSIONAL:
                $retval = $this->price5;
                break;
        }

        return( $retval );
    }
    
    function Xml()
    {
        $retval = "<size>";
        $retval .= "<id>{$this->id}</id>";
        $retval .= "<name><![CDATA[{$this->name}]]></name>";
        $retval .= "<price><![CDATA[{$this->price}]]></price>";
        $retval .= "<price1><![CDATA[{$this->price1}]]></price1>";
        $retval .= "<price2><![CDATA[{$this->price2}]]></price2>";
        $retval .= "<price3><![CDATA[{$this->price3}]]></price3>";
        $retval .= "<price4><![CDATA[{$this->price4}]]></price4>";
        $retval .= "<price5><![CDATA[{$this->price5}]]></price5>";
        $retval .= "<weight><![CDATA[{$this->weight}]]></weight>";
        $retval .= "<description><![CDATA[{$this->description}]]></description>";
        $retval .= "<status>{$this->status}</status>";
        $retval .= "<position>{$this->position}</position>";
        $retval .= "<created_at>{$this->created_at}</created_at>";
        $retval .= "<updated_at>{$this->updated_at}</updated_at>";
        $retval .= "</size>";
        
        return( $retval );
    }
}

?>
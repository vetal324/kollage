<?php
/*****************************************************
Class v.1.0, 2017
Copyright Vitaliy Koshelenko vetal324@gmail.com
 ******************************************************/

/**
 * Class BookCoverLamination
 *
 * (paper type for cover)
 *
 */
class BookCoverLamination extends Model {

    var $name, $description, $price;
    var $cover_id;

    var $tablename = 'book_cover_lamination';

    function __construct( $id=0 ) {
        $this->status = 1;
        parent::Model( $id );
    }

    function _Load( &$row ) {
        $this->id = intval( $row['id'] );
        $this->cover_id = intval( $row['cover_id'] );

        $this->position = intval( $row['position'] );

        $this->name = trim( $row['name'] );
        $this->description = trim( $row['description'] );

        $this->price = floatval($row['price']);

        $md = new MysqlDateTime();
        $md->Parse( $row['created_at'] );
        $this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
        $this->updated_at = $row['updated_at'];

        $this->loaded = true;
    }

    function Save( $data=null ) {
        $this->CheckupData();

        $t = new MysqlTable( $this->tablename );
        if(is_array($data)) {
            $t->save( $data );
        } else {
            $data = Array();
            $data['id'] = $this->id;
            $data['cover_id'] = $this->cover_id;
            $data['name'] = $this->name;
            $data['description'] = $this->description;
            $data['price'] = $this->price;

            $t->save( $data );
        }

        $this->Load( $t->get_last_insert_id() );

        return( $this->id );
    }

    function Json() {
        $lamination = new StdClass();

        $lamination->id     = $this->id;

        $lamination->name   = $this->name;
        $lamination->description   = $this->description;

        $lamination->price   = $this->price;

        return $lamination;
    }


    function Xml() {
        $retval = "<book_cover_lamination>";
        $retval .= "<id>{$this->id}</id>";
        $retval .= "<cover_id>{$this->cover_id}</cover_id>";
        $retval .= "<name>{$this->name}</name>";
        $retval .= "<description><![CDATA[{$this->description}]]></description>";
        $retval .= "<price>{$this->price}</price>";
        $retval .= "</book_cover_lamination>";

        return( $retval );
    }
}

?>
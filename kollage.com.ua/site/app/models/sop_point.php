<?php
/*****************************************************
 Class v.1.0, 2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

class SOPPoint extends Model
{
    var $name, $address, $phone, $email, $status, $order_prefix;
    
    var $tablename = 'sop_points';

    function SOPPoint( $id=0 )
    {
        $this->status = 1;
        parent::Model( $id );
    }
    
    function _Load( &$row )
    {
        $this->id = intval( $row['id'] );
        $this->phone = $row['phone'];
        $this->email = $row['email'];
        $this->order_prefix = $row['order_prefix'];
        
        $this->status = intval( $row['status'] );
        $this->position = intval( $row['position'] );
        
        $t = new MysqlTable('dictionary');
        if( $t->find_all( "parent_id={$this->id} and folder='{$this->tablename}' and lang='". $_SESSION['lang'] ."'" ) )
        {
            foreach( $t->data as $row2 )
            {
                switch( $row2['name'] )
                {
                    case 'name';
                        $this->name = $row2['value'];
                        break;
                    case 'address';
                        $this->address = $row2['value'];
                        break;
                }
            }
        }
        
        $md = new MysqlDateTime();
        $md->Parse( $row['created_at'] );
        $this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
        
        $this->updated_at = $row['updated_at'];
        
        $this->loaded = true;
    }
    
    function Save( $data=null )
    {   
        $this->CheckupData();
        
        $t = new MysqlTable( $this->tablename );
        if( is_array($data) ) $t->save( $data );
        else
        {
            $data = Array();
            $data['id'] = $this->id;
            $data['name'] = $this->name;
            $data['address'] = $this->address;
            $data['order_prefix'] = $this->order_prefix;
            $data['phone'] = $this->phone;
            $data['email'] = $this->email;
            $data['status'] = $this->status;
            $data['position'] = $this->position;
            
            $t->save( $data );
        }
            
        $this->Load( $t->get_last_insert_id() );
        
        return( $this->id );
    }
    
    function Delete()
    {
        if( $this->IsLoaded() )
        {
            $this->status = 0;
            $this->Save();
        }
    }
    
    function Xml()
    {
        $retval = "<point>";
        $retval .= "<id>{$this->id}</id>";
        $retval .= "<name><![CDATA[{$this->name}]]></name>";
        $retval .= "<address><![CDATA[{$this->address}]]></address>";
        $retval .= "<phone>{$this->phone}</phone>";
        $retval .= "<order_prefix>{$this->order_prefix}</order_prefix>";
        $retval .= "<email>{$this->email}</email>";
        $retval .= "<status>{$this->status}</status>";
        $retval .= "<position>{$this->position}</position>";
        $retval .= "<created_at>{$this->created_at}</created_at>";
        $retval .= "<updated_at>{$this->updated_at}</updated_at>";
        $retval .= "</point>";
        
        return( $retval );
    }
}

?>
<?php
/*****************************************************
 Class v.1.0, 2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

class ShopSettings extends Model
{
	var $order_mail_subject, $order_mail_body, $file_price, $file_price_opt, $file_price_opt2, $admin_emails, $admin_order_mail_subject, $admin_order_mail_body;
	var $page_title, $page_keywords;
	
	var $tablename = 'shop_settings';

	function ShopSettings( $id=1 )
	{
		parent::Model( $id );
	}
	
	function _Load( &$row )
	{
		$this->id = intval( $row['id'] );
		$this->file_price = $row['file_price'];
		$this->file_price_opt = $row['file_price_opt'];
		$this->file_price_opt2 = $row['file_price_opt2'];
		$this->admin_emails = $row['admin_emails'];
		
		$this->page_title = $row['page_title'];
		$this->page_keywords = $row['page_keywords'];
		
		$t = new MysqlTable('dictionary');
		if( $t->find_all( "parent_id={$this->id} and folder='{$this->tablename}' and lang='". $_SESSION['lang'] ."'" ) )
		{
			foreach( $t->data as $row2 )
			{
				switch( $row2['name'] )
				{
					case 'order_mail_subject';
						$this->order_mail_subject = $row2['value'];
						break;
					case 'order_mail_body';
						$this->order_mail_body = $row2['value'];
						break;
					case 'admin_order_mail_subject';
						$this->admin_order_mail_subject = $row2['value'];
						break;
					case 'admin_order_mail_body';
						$this->admin_order_mail_body = $row2['value'];
						break;
				}
			}
		}
		
		$this->loaded = true;
	}
	
	function Save( $data=null )
	{   
		$this->CheckupData();
		
		$t = new MysqlTable( $this->tablename );
		if( is_array($data) ) $t->save( $data );
		else
		{
			$data = Array();
			$data['id'] = $this->id;
			$data['order_mail_subject'] = $this->order_mail_subject;
			$data['order_mail_body'] = $this->order_mail_body;
			$data['admin_emails'] = $this->admin_emails;
			$data['admin_order_mail_subject'] = $this->admin_order_mail_subject;
			$data['admin_order_mail_body'] = $this->admin_order_mail_body;
			$data['file_price'] = $this->file_price;
			$data['file_price_opt'] = $this->file_price_opt;
			$data['file_price_opt2'] = $this->file_price_opt2;
			$data['page_title'] = $this->page_title;
			$data['page_keywords'] = $this->page_keywords;
			
			$t->save( $data );
		}

		$this->Load( $t->get_last_insert_id() );
		
		return( $this->id );
	}
	
	function DeleteFilePrice()
	{
		if( $this->IsLoaded() )
		{
			$attachment = new Attachment();
			$attachment->filename = $this->file_price;
			$attachment->rm();
			
			$this->file_price = '';
			$this->Save();
		}
	}
	
	function DeleteFilePriceOpt()
	{
		if( $this->IsLoaded() )
		{
			$attachment = new Attachment();
			$attachment->filename = $this->file_price_opt;
			$attachment->rm();
			
			$this->file_price_opt = '';
			$this->Save();
		}
	}
	
	function DeleteFilePriceOpt2()
	{
		if( $this->IsLoaded() )
		{
			$attachment = new Attachment();
			$attachment->filename = $this->file_price_opt2;
			$attachment->rm();
			
			$this->file_price_opt2 = '';
			$this->Save();
		}
	}
	
	function Xml()
	{
		$retval = "<settings>";
		$retval .= "<id>{$this->id}</id>";
		$retval .= "<order_mail_subject><![CDATA[{$this->order_mail_subject}]]></order_mail_subject>";
		$retval .= "<order_mail_body><![CDATA[{$this->order_mail_body}]]></order_mail_body>";
		$retval .= "<admin_emails><![CDATA[{$this->admin_emails}]]></admin_emails>";
		$retval .= "<admin_order_mail_subject><![CDATA[{$this->admin_order_mail_subject}]]></admin_order_mail_subject>";
		$retval .= "<admin_order_mail_body><![CDATA[{$this->admin_order_mail_body}]]></admin_order_mail_body>";
		$retval .= "<file_price><![CDATA[{$this->file_price}]]></file_price>";
		$retval .= "<file_price_opt><![CDATA[{$this->file_price_opt}]]></file_price_opt>";
		$retval .= "<file_price_opt2><![CDATA[{$this->file_price_opt2}]]></file_price_opt2>";
		$retval .= "<page_title><![CDATA[{$this->page_title}]]></page_title>";
		$retval .= "<page_keywords><![CDATA[{$this->page_keywords}]]></page_keywords>";
		$retval .= "</settings>";
		
		return( $retval );
	}
}

?>
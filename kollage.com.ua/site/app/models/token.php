<?php

class Token extends Model
{
    var $token, $client_id, $google_cloud_token, $os;
    
    var $tablename = 'tokens';

    function Token( $id=0 )
    {
        parent::Model( $id );
    }

    function LoadByToken( $token ) {
      $this->loaded = false;
      
      $t = new MysqlTable( $this->tablename );
      if( $t->find_first( "token='$token'" ) )
      {
         $this->_Load( $t->data[0] );
      }
    }
    
    function _Load( &$row )
    {
        $this->id = intval( $row['id'] );
        $this->token = $row['token'];
        $this->google_cloud_token = $row['google_cloud_token'];
        $this->os = $row['os'];
        $this->client_id = intval( $row['client_id'] );
        
        $this->position = intval( $row['position'] );
        
        $md = new MysqlDateTime();
        $md->Parse( $row['created_at'] );
        $this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
        
        $this->updated_at = $row['updated_at'];
        
        $this->loaded = true;
    }
    
    function Save( $data=null )
    {   
        $this->CheckupData();
        
        $t = new MysqlTable( $this->tablename );
        if( is_array($data) ) $t->save( $data );
        else
        {
            $data = Array();
            $data['id'] = $this->id;
            $data['token'] = $this->token;
            $data['google_cloud_token'] = $this->google_cloud_token;
            $data['os'] = $this->os;
            $data['client_id'] = $this->client_id;
            $data['position'] = $this->position;
            
            $t->save( $data );
        }
            
        $this->Load( $t->get_last_insert_id() );
        
        return( $this->id );
    }
    
    function Xml()
    {
        $retval = "<country>";
        $retval .= "<id>{$this->id}</id>";
        $retval .= "<token><![CDATA[{$this->token}]]></token>";
        $retval .= "<google_cloud_token><![CDATA[{$this->google_cloud_token}]]></google_cloud_token>";
        $retval .= "<os><![CDATA[{$this->os}]]></os>";
        $retval .= "<client_id><![CDATA[{$this->client_id}]]></client_id>";
        $retval .= "<position>{$this->position}</position>";
        $retval .= "<created_at>{$this->created_at}</created_at>";
        $retval .= "<updated_at>{$this->updated_at}</updated_at>";
        $retval .= "</country>";
        
        return( $retval );
    }
}

?>
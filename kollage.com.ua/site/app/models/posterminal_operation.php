<?php
/*****************************************************
 Class v.1.0, 2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

class PosterminalOperation extends Model
{
    var $client_id, $amount, $description, $transaction_id, $type, $point_id;
    
    var $tablename = 'posterminal_operations';

    function PosterminalOperation( $id=0 )
    {
        parent::Model( $id );
    }
    
    function _Load( &$row )
    {
        $this->id = intval( $row['id'] );
        $this->type = intval( $row['type'] );
        $this->client_id = intval( $row['client_id'] );
        $this->amount = sprintf( "%01.2f", floatval( $row['amount'] ) );
        $this->description = $row['description'];
        $this->transaction_id = $row['transaction_id'];
        $this->point_id = $row['point_id'];
        
        $md = new MysqlDateTime();
        $md->Parse( $row['created_at'] );
        $this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
        
        $this->updated_at = $row['updated_at'];
        
        $this->loaded = true;
    }
    
    function Save( $data=null )
    {   
        $result = false;
        $this->CheckupData();
        
        $t = new MysqlTable( $this->tablename );
        if( is_array($data) ) $result = $t->save( $data );
        else
        {
            $data = Array();
            $data['id'] = $this->id;
            $data['type'] = $this->type;
            $data['client_id'] = $this->client_id;
            $data['amount'] = $this->amount;
            $data['description'] = $this->description;
            $data['transaction_id'] = $this->transaction_id;
            $data['point_id'] = $this->point_id;
            
            $result = $t->save( $data );
        }
            
        $this->Load( $t->get_last_insert_id() );
        
        return( $this->id );
    }
    
    function Xml()
    {
        $retval = "<posterminal_operation>";
        $retval .= "<id>{$this->id}</id>";
        $retval .= "<type>{$this->type}</type>";
        $retval .= "<client_id>{$this->client_id}</client_id>";
        $retval .= "<description><![CDATA[{$this->description}]]></description>";
        $retval .= "<amount>{$this->amount}</amount>";
        $retval .= "<transaction_id>{$this->transaction_id}</transaction_id>";
        $retval .= "<point_id>{$this->point_id}</point_id>";
        $retval .= "<created_at>{$this->created_at}</created_at>";
        $retval .= "<updated_at>{$this->updated_at}</updated_at>";
        $retval .= "</posterminal_operation>";
        
        return( $retval );
    }
}

?>
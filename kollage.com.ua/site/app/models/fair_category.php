<?php
/*****************************************************
 Class v.1.0, 2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

class FairCategory extends Model
{
    var $name, $description, $parent_id, $folder, $category;
    
    var $tablename = 'fair_categories';

    function FairCategory( $id=0 )
    {
        parent::Model( $id );
    }
    
    function _Load( &$row )
    {
        $this->id = intval( $row['id'] );
        $this->parent_id = intval( $row['parent_id'] );
        $this->category = intval( $row['category'] );
        $this->folder = $row['folder'];
        
        $this->position = intval( $row['position'] );
        
        $t = new MysqlTable('dictionary');
        if( $t->find_all( "parent_id={$this->id} and folder='{$this->tablename}' and lang='". $_SESSION['lang'] ."'" ) )
        {
            foreach( $t->data as $row2 )
            {
                switch( $row2['name'] )
                {
                    case 'name';
                        $this->name = $row2['value'];
                        break;
                    case 'description';
                        $this->description = $row2['value'];
                        break;
                }
            }
        }
        
        $md = new MysqlDateTime();
        $md->Parse( $row['created_at'] );
        $this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
        
        $this->updated_at = $row['updated_at'];
        
        $this->loaded = true;
    }
    
    function Save( $data=null )
    {   
        $this->CheckupData();
        
        $t = new MysqlTable( $this->tablename );
        if( is_array($data) ) $t->save( $data );
        else
        {
            $data = Array();
            $data['id'] = $this->id;
            $data['parent_id'] = $this->parent_id;
            $data['category'] = $this->category;
            $data['folder'] = $this->folder;
            $data['name'] = $this->name;
            $data['description'] = $this->description;
            $data['position'] = $this->position;
            $data['lang'] = $_SESSION['lang'];
            
            $t->save( $data );
        }
            
        $this->Load( $t->get_last_insert_id() );
        
        return( $this->id );
    }
    
    function Delete()
    {
        if( $this->IsLoaded() )
        {
            $t = new MysqlTable( $this->tablename );
            $t->del( $this->id );
            $ta = new MysqlTable( 'fair_photos' );
            if( $this->category == 2 )
            {
                $ta->find_all( "category_id2={$this->id}" );
                foreach( $ta->data as $row )
                {
                    $o = new FairPhoto( $row['id'] );
                    $o->category_id2 = 0;
                    $o->Save();
                }
            }
            else
            {
                $ta->find_all( "category_id1={$this->id}" );
                foreach( $ta->data as $row )
                {
                    $o = new FairPhoto( $row['id'] );
                    $o->category_id1 = 0;
                    $o->Save();
                }
            }
        }
    }
    
    function Xml()
    {
        $retval = "<category>";
        $retval .= "<id>{$this->id}</id>";
        $retval .= "<parent_id>{$this->parent_id}</parent_id>";
        $retval .= "<category>{$this->category}</category>";
        $retval .= "<folder><![CDATA[{$this->folder}]]></folder>";
        $retval .= "<name><![CDATA[{$this->name}]]></name>";
        $retval .= "<description><![CDATA[{$this->description}]]></description>";
        $retval .= "<position>{$this->position}</position>";
        $retval .= "<created_at>{$this->created_at}</created_at>";
        $retval .= "<updated_at>{$this->updated_at}</updated_at>";
        $retval .= "</category>";
        
        return( $retval );
    }
}

?>
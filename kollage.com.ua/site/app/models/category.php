<?php
/*****************************************************
 Class v.1.0, 2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

class Category extends Model
{
	var $name, $text, $design, $category_id, $folder;
	var $images = Array();
	var $page_title, $page_keywords;
	
	var $folder_for_child = 'category';
	
	var $tablename = 'categories';

	function Category( $id=0 )
	{
		parent::Model( $id );
	}
	
	function _Load( &$row )
	{
		$this->id = intval( $row['id'] );
		$this->category_id = intval( $row['category_id'] );
		$this->folder = $row['folder'];
		$this->design = intval( $row['design'] );
        
		$this->page_title = $row['page_title'];
		$this->page_keywords = $row['page_keywords'];
		
		$this->position = intval( $row['position'] );
		
		$t = new MysqlTable('dictionary');
		if( $t->find_all( "parent_id={$this->id} and folder='{$this->tablename}' and lang='". $_SESSION['lang'] ."'" ) )
		{
			foreach( $t->data as $row2 )
			{
				switch( $row2['name'] )
				{
					case 'name';
						$this->name = $row2['value'];
						break;
					case 'text';
						$this->text = $row2['value'];
						break;
				}
			}
		}
		
		$md = new MysqlDateTime();
		$md->Parse( $row['created_at'] );
		$this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
		
		$this->updated_at = $row['updated_at'];
		
		$this->loaded = true;
		
		$this->LoadImages();
	}
	
	function LoadImages()
	{
		if( $this->IsLoaded() )
		{
			$t = new MysqlTable('images');
			if( $t->find_all( "folder='{$this->folder_for_child}' and parent_id={$this->id}", "position,id" ) )
			{
				unset( $this->images ); $this->images = Array();
				foreach( $t->data as $row )
				{
					array_push( $this->images, new Image($row['id'], true) );
				}
			}
		}
	}
	
	function DeleteImage()
	{
		if( $this->IsLoaded() )
		{
			$attachment = new Attachment();
			$attachment->filename = $this->image;
			$attachment->rm();
			
			$this->image = '';
			$this->Save();
		}
	}
	
	function Save( $data=null )
	{   
		$this->CheckupData();
		
		$t = new MysqlTable( $this->tablename );
		if( is_array($data) ) $t->save( $data );
		else
		{
			$data = Array();
			$data['id'] = $this->id;
			$data['category_id'] = $this->category_id;
			$data['folder'] = $this->folder;
			$data['name'] = $this->name;
			$data['text'] = $this->text;
			$data['design'] = $this->design;
			$data['page_title'] = $this->page_title;
			$data['page_keywords'] = $this->page_keywords;
			$data['position'] = $this->position;
			$data['lang'] = $_SESSION['lang'];
			
			$t->save( $data );
		}
			
		$this->Load( $t->get_last_insert_id() );
		
		return( $this->id );
	}
	
	function Delete()
	{
		if( $this->IsLoaded() )
		{
			$t = new MysqlTable( $this->tablename );
			$t->del( $this->id );
			$ta = new MysqlTable( 'articles' );
			$ta->find_all( "category_id={$this->id}" );
			foreach( $ta->data as $row )
			{
				$o = new Article( $row['id'] );
				$o->Delete();
			}
		}
	}
	
	function TransformText()
	{
		global $log;
		$retval = $this->text;
		
		if( $retval!="" )
		{
			while( preg_match( "/\{img([^;]+);*([lst]*);*([lrc]*);*([0-9]*);*([0-9]*)\}/", $retval, $regs ) )
			{
				//$log->Write("found image ". json_encode($regs));
				$t = new MysqlTable('images');
				$row = $t->find_first("parent_id={$this->id} and tag='{$regs[1]}'");
				$i = new Image( $row['id'] );
				if( $i->IsLoaded() )
				{
					//$log->Write("found image is loaded");
					$src = "";
					if( strtolower($regs[2])=="l" && $i->filename!="" ) $src = $i->filename;
					if( strtolower($regs[2])=="s" && $i->small!="" ) $src = $i->small;
					if( strtolower($regs[2])=="t" && $i->tiny!="" ) $src = $i->tiny;
					
					$style = "";
					$align = "";
					if( strtolower($regs[3])=="l" ) { $align = "align='left'"; $style.=""; }
					if( strtolower($regs[3])=="r" ) { $align = "align='right'"; $style.=""; }
					if( strtolower($regs[3])=="c" ) { $align = "align='center'"; $style.=""; }
					
					$width = "";
					if( intval($regs[4])>0 ) $width = ' width="'. intval($regs[4]) .'"';
					
					$height = "";
					if( intval($regs[5])>0 ) $height = ' height="'. intval($regs[5]) .'"';
					
					if( $src!="" )
					{
						if( $regs[2]!="l" && $i->filename!="" ) {
							$retval = preg_replace( "/\{img". $regs[1] .";*". $regs[2] .";*". $regs[3] .";*([0-9]*);*([0-9]*)\}/", "<a class='highslide' href='". SITEPREFIX ."{$_ENV['data_path']}/{$i->filename}' onclick='return hs.expand(this, { slideshowGroup: 1, captionText: \"". htmlspecialchars($i->description) ."\"} )'><img src='". SITEPREFIX ."{$_ENV['data_path']}/$src' border='0' {$align} {$width} {$height} style='{$style}'/></a>", $retval );
						}
						else {
							$retval = preg_replace( "/\{img". $regs[1] .";*". $regs[2] .";*". $regs[3] .";*([0-9]*);*([0-9]*)\}/", "<img src='". SITEPREFIX ."{$_ENV['data_path']}/$src' border='0' {$align} {$width} {$height} style='{$style}'/>", $retval );
						}
					}
					else
					{
						$retval = preg_replace( "/\{img". $regs[1] .";*([lst]*);*([lrc]*);*([0-9]*);*([0-9]*)\}/", "", $retval );
					}
				}
				else
				{
					$retval = preg_replace( "/\{img". $regs[1] .";*([lst]*);*([lrc]*);*([0-9]*);*([0-9]*)\}/", "", $retval );
				}
			}
				
			// galleries
			while( preg_match( "/\{gallery;*([lst]*);*([0-9]*);*([0-9]*)\}/", $retval, $regs ) )
			{
				$log->Write("found gallery");
				$type = strtolower($regs[1]);
				$t = new MysqlTable('images');
				if( $t->find_all("parent_id={$this->id} and folder='{$this->folder_for_child}'") ) {
					
					$w = 0; $width = ""; $width_css = "";
					if( intval($regs[2])>0 ) {
						$w = intval($regs[2]);
						$width = ' width="'. $w .'"';
						$width_css = ';width:'. $w .'px';
					}
					
					$h = 0; $height = ""; $height_css = "";
					if( intval($regs[3])>0 ) {
						$h = intval($regs[3]);
						$height = ' height="'. $h .'"';
						$height_css = ';height:'. $h .'px';
					}
					
					$gallery_id = "gallery_". md5( time() . rand(1,99) );
					
					$gallery_begin = '<div id="'. $gallery_id .'" class="protoshow" style="'. $width_css . $height_css .'"><ul class="show">';
					$gallery_content = "";
					$gallery_end = '</ul></div><script type="text/javascript">Event.observe(window, "load",function(){$("'. $gallery_id .'").protoShow({interval:5000,pauseOnHover:true,transitionType:"fade",captions:false,navigation:true,controls:false});});</script>';
							
					foreach( $t->data as $row ) {
						$i = new Image( $row['id'] );
						if( $i->IsLoaded() )
						{
							$src = "";
							if( $type=="l" && $i->filename!="" ) $src = $i->filename;
							if( $type=="s" && $i->small!="" ) $src = $i->small;
							if( $type=="t" && $i->tiny!="" ) $src = $i->tiny;
							
							if( $src!="" ) {
								$gallery_content .= '<li class="slide" data-slide-interval=""><img src="'. SITEPREFIX . $_ENV['data_path'] .'/'. $src .'" alt="" border="0"'. $width . $height .'/></li>';
							}
						}
					}
					
					if( $gallery_content ) {
						$retval = str_replace( $regs[0], $gallery_begin . $gallery_content . $gallery_end, $retval ); 
					}
				}
				else {
					preg_replace( "/\{gallery;*([lst]*);*([0-9]*);*([0-9]*)\}/", "", $retval );
				}
			}
			// galleries
		}
		
		return( $retval );
	}
	
	function Xml( $transform=false )
	{
		global $log;

		$add = "";
		if( $transform )
		{
			$add .= "<text_transformed><![CDATA[". $this->TransformText() ."]]></text_transformed>";
		}
		
		$retval = "<category>";
		$retval .= "<id>{$this->id}</id>";
		$retval .= "<category_id>{$this->category_id}</category_id>";
		$retval .= "<folder><![CDATA[{$this->folder}]]></folder>";
		$retval .= "<folder_for_child><![CDATA[{$this->folder_for_child}]]></folder_for_child>";
		$retval .= "<name><![CDATA[{$this->name}]]></name>";
		$retval .= "<text><![CDATA[{$this->text}]]></text>";
		$retval .= "<design>{$this->design}</design>";
		$retval .= "<page_title>{$this->page_title}</page_title>";
		$retval .= "<page_keywords>{$this->page_keywords}</page_keywords>";
		$retval .= "<position>{$this->position}</position>";
		$retval .= "<created_at>{$this->created_at}</created_at>";
		$retval .= "<updated_at>{$this->updated_at}</updated_at>";
		
		$retval .= "<images>";
		foreach( $this->images as $img )
		{
			$retval .= $img->Xml();
		}
		$retval .= "</images>";
		
		$retval .= $add;
		
		$retval .= "</category>";
		
		return( $retval );
	}
}

?>
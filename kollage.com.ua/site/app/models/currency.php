<?php
/*****************************************************
 Class v.1.0, 2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

class Currency extends Model
{
    var $name, $symbol, $exchange_rate, $code;
    
    var $tablename = 'currencies';

    function Currency( $id=0 )
    {
        parent::Model( $id );
    }
    
    function LoadByCode( $code )
    {
        $this->loaded = false;
        
        if( $code!='' )
        {
            $t = new MysqlTable( $this->tablename);
            if( $t->find_first( "code='$code'", "created_at desc" ) )
            {
                $this->_Load( $t->data[0] );
            }
        }
    }
    
    function _Load( &$row )
    {
        $this->id = intval( $row['id'] );
        $this->name = $row['name'];
        $this->symbol = $row['symbol'];
        $this->exchange_rate = floatval( $row['exchange_rate'] );
        $this->code = $row['code'];
        
        $this->position = intval( $row['position'] );
        
        $md = new MysqlDateTime();
        $md->Parse( $row['created_at'] );
        $this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
        
        $this->updated_at = $row['updated_at'];
        
        $this->loaded = true;
    }
    
    function Save( $data=null )
    {   
        $this->CheckupData();
        
        $t = new MysqlTable( $this->tablename );
        if( is_array($data) ) $t->save( $data );
        else
        {
            $data = Array();
            $data['id'] = $this->id;
            $data['name'] = $this->name;
            $data['symbol'] = $this->symbol;
            $data['exchange_rate'] = $this->exchange_rate;
            $data['code'] = $this->code;
            $data['position'] = $this->position;
            
            $t->save( $data );
        }
            
        $this->Load( $t->get_last_insert_id() );
        
        return( $this->id );
    }
    
    function Xml()
    {
        $retval = "<currency>";
        $retval .= "<id>{$this->id}</id>";
        $retval .= "<name><![CDATA[{$this->name}]]></name>";
        $retval .= "<symbol><![CDATA[{$this->symbol}]]></symbol>";
        $retval .= "<exchange_rate>{$this->exchange_rate}</exchange_rate>";
        $retval .= "<code>{$this->code}</code>";
        $retval .= "<position>{$this->position}</position>";
        $retval .= "<created_at>{$this->created_at}</created_at>";
        $retval .= "<updated_at>{$this->updated_at}</updated_at>";
        $retval .= "</currency>";
        
        return( $retval );
    }
}

?>
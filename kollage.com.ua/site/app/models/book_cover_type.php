<?php
/*****************************************************
Class v.1.0, 2017
Copyright Vitaliy Koshelenko vetal324@gmail.com
 ******************************************************/

/**
 * Class BookCoverType
 *
 * Print item cover type (hard/slim/etc.)
 */
class BookCoverType extends Model {

    var $name, $description, $price;
    var $min_page_count, $max_page_count;
    var $format_id;
    var $image;

    var $tablename = 'book_cover_types';

    function __construct( $id=0 ) {
        $this->status = 1;
        parent::Model( $id );
    }

    function _Load( &$row ) {
        $this->id = intval( $row['id'] );
        $this->format_id = intval( $row['format_id'] );

        $this->position = intval( $row['position'] );

        $this->name = trim( $row['name'] );
        $this->description = trim( $row['description'] );
        $this->image = $row['image'];

        $this->min_page_count = intval( $row['min_page_count'] );
        $this->max_page_count = intval( $row['max_page_count'] );

        $this->price = floatval($row['price']);

        $md = new MysqlDateTime();
        $md->Parse( $row['created_at'] );
        $this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
        $this->updated_at = $row['updated_at'];

        $this->loaded = true;
    }


    function Save( $data=null ) {
        $this->CheckupData();

        $t = new MysqlTable( $this->tablename );
        if(is_array($data)) {
            $t->save( $data );
        } else {
            $data = Array();
            $data['id'] = $this->id;
            $data['format_id'] = $this->format_id;
            $data['name'] = $this->name;
            $data['description'] = $this->description;
            $data['image'] = $this->image;
            $data['price'] = $this->price;

            $data['min_page_count'] = $this->min_page_count;
            $data['max_page_count'] = $this->max_page_count;

            $t->save( $data );
        }

        $this->Load( $t->get_last_insert_id() );

        return( $this->id );
    }

    function Json() {

        $coverType = new StdClass();

        $coverType->id     = $this->id;

        $coverType->name   = $this->name;
        $coverType->description   = $this->description;

        $coverType->price   = $this->price;

        $coverType->min_page_count   = $this->min_page_count;
        $coverType->max_page_count   = $this->max_page_count;

        $coverType->image   = $this->image;

        $coverType->paper_types = array();
        $coverType->laminations = array();

        $paperTypesTable = new MysqlTable('book_cover_paper_types');
        $paperTypesTable->find_all('cover_id='.$this->id, 'position,id');
        $paperTypes = $paperTypesTable->data;
        foreach ($paperTypes as $paperTypesRow) {
            $paperTypeId = $paperTypesRow['id'];
            $paperType = new BookCoverPaperType($paperTypeId);
            $coverType->paper_types[$paperTypeId] = $paperType->Json();
        }

        $laminationTable = new MysqlTable('book_cover_lamination');
        $laminationTable->find_all('cover_id='.$this->id, 'position,id');
        $laminations = $laminationTable->data;
        foreach ($laminations as $laminationRow) {
            $laminationId = $laminationRow['id'];
            $lamination = new BookCoverLamination($laminationId);
            $coverType->laminations[$laminationId] = $lamination->Json();
        }

        return $coverType;
    }

    function Xml($full=true) {
        $retval = "<book_cover_type>";
        $retval .= "<id>{$this->id}</id>";
        $retval .= "<format_id>{$this->format_id}</format_id>";
        $retval .= "<name>{$this->name}</name>";
        $retval .= "<description><![CDATA[{$this->description}]]></description>";
        $retval .= "<image>{$this->image}</image>";
        $retval .= "<price>{$this->price}</price>";
        $retval .= "<min_page_count>{$this->min_page_count}</min_page_count>";
        $retval .= "<max_page_count>{$this->max_page_count}</max_page_count>";

        $retval .= "<book_cover_paper_types>";
        if ($full) {
            $paperTypeTable = new MysqlTable('book_cover_paper_types');
            $paperTypeTable->find_all('cover_id='.$this->id, 'position,id');
            $paperTypes = $paperTypeTable->data;
            foreach ($paperTypes as $paperTypesRow) {
                $paperType = new BookCoverPaperType($paperTypesRow['id']);
                $paperTypeXml = $paperType->Xml();
                $retval .= $paperTypeXml;
            }
        }
        $retval .= "</book_cover_paper_types>";

        $retval .= "<book_cover_laminations>";
        if ($full) {
            $laminationTable = new MysqlTable('book_cover_lamination');
            $laminationTable->find_all('cover_id='.$this->id, 'position,id');
            $laminationTypes = $laminationTable->data;
            foreach ($laminationTypes as $laminationTypesRow) {
                $laminationType = new BookCoverLamination($laminationTypesRow['id']);
                $laminationTypeXml = $laminationType->Xml();
                $retval .= $laminationTypeXml;
            }
        }
        $retval .= "</book_cover_laminations>";

        $retval .= "</book_cover_type>";

        return( $retval );
    }
}

?>
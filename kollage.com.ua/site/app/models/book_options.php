<?php
/*****************************************************
Class v.1.0, 2017
Copyright Vitaliy Koshelenko vetal324@gmail.com
 ******************************************************/

/**
 * Class BookOption
 *
 * Book options (forzaz,vichitka,tisnenie,isbn)
 */
class BookOption extends Model {

    var $name, $description;
    var $type_id;
    var $price_page, $price_item, $price_order;

    var $tablename = 'book_options';

    function __construct( $id=0 ) {
        $this->status = 1;
        parent::Model( $id );
    }

    function _Load( &$row ) {
        $this->id = intval( $row['id'] );
        $this->type_id = intval( $row['type_id'] );

        $this->position = intval( $row['position'] );

        $this->name = trim( $row['name'] );
        $this->description = trim( $row['description'] );

        $this->price_page = floatval($row['price_page']);
        $this->price_item = floatval($row['price_item']);
        $this->price_order = floatval($row['price_order']);

        $md = new MysqlDateTime();
        $md->Parse( $row['created_at'] );
        $this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
        $this->updated_at = $row['updated_at'];

        $this->loaded = true;
    }

    function Save( $data=null ) {
        $this->CheckupData();

        $t = new MysqlTable( $this->tablename );
        if(is_array($data)) {
            $t->save( $data );
        } else {
            $data = Array();
            $data['id'] = $this->id;
            $data['type_id'] = $this->type_id;
            $data['name'] = $this->name;
            $data['description'] = $this->description;

            $data['price_page'] = $this->price_page;
            $data['price_item'] = $this->price_item;
            $data['price_order'] = $this->price_order;

            $t->save( $data );
        }

        $this->Load( $t->get_last_insert_id() );

        return( $this->id );
    }


    function Json() {
        $bookOption = new StdClass();

        $bookOption->id     = $this->id;

        $bookOption->name   = $this->name;
        $bookOption->description   = $this->description;

        $bookOption->price_page   = number_format((float)$this->price_page, 2, '.', '');
        $bookOption->price_item   = number_format((float)$this->price_item, 2, '.', '');
        $bookOption->price_order   = number_format((float)$this->price_order, 2, '.', '');

        return $bookOption;
    }

    function Xml() {
        $retval = "<book_option>";
        $retval .= "<id>{$this->id}</id>";
        $retval .= "<type_id>{$this->type_id}</type_id>";
        $retval .= "<name>{$this->name}</name>";
        $retval .= "<description><![CDATA[{$this->description}]]></description>";
        $retval .= "<price_page>{$this->price_page}</price_page>";
        $retval .= "<price_item>{$this->price_item}</price_item>";
        $retval .= "<price_order>{$this->price_order}</price_order>";
        $retval .= "</book_option>";

        return( $retval );
    }

}

?>
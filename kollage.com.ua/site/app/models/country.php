<?php

class Country extends Model
{
    var $name, $phone_code;
    
    var $tablename = 'countries';

    function Country( $id=0 )
    {
        parent::Model( $id );
    }

    /**
     * Load country by full phone number
     * @param string $phone +380502356789
     */
    function LoadByPhoneNumber( $phone )
    {
        $this->loaded = false;
        
        if( $phone )
        {
            $phone = preg_replace( '@[\s\\\/=&\(\)\[\]\{\}%\^\+\-\._]+@', '', $phone );
            $t = new MysqlTable( $this->tablename );
            if( $t->find_first( "instr('$phone', phone_code)=1" ) )
            {
                $this->_Load( $t->data[0] );
            }
        }
    }
    
    function _Load( &$row )
    {
        $this->id = intval( $row['id'] );
        $this->name = $row['name'];
        $this->phone_code = $row['phone_code'];
        
        $this->position = intval( $row['position'] );
        
        $md = new MysqlDateTime();
        $md->Parse( $row['created_at'] );
        $this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
        
        $this->updated_at = $row['updated_at'];
        
        $this->loaded = true;
    }
    
    function Save( $data=null )
    {   
        $this->CheckupData();
        
        $t = new MysqlTable( $this->tablename );
        if( is_array($data) ) $t->save( $data );
        else
        {
            $data = Array();
            $data['id'] = $this->id;
            $data['name'] = $this->name;
            $data['phone_code'] = $this->phone_code;
            $data['position'] = $this->position;
            
            $t->save( $data );
        }
            
        $this->Load( $t->get_last_insert_id() );
        
        return( $this->id );
    }
    
    function Xml()
    {
        $retval = "<country>";
        $retval .= "<id>{$this->id}</id>";
        $retval .= "<name><![CDATA[{$this->name}]]></name>";
        $retval .= "<phone_code><![CDATA[{$this->phone_code}]]></phone_code>";
        $retval .= "<position>{$this->position}</position>";
        $retval .= "<created_at>{$this->created_at}</created_at>";
        $retval .= "<updated_at>{$this->updated_at}</updated_at>";
        $retval .= "</country>";
        
        return( $retval );
    }
}

?>
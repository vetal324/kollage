<?php
/*****************************************************
Class v.1.0, 2017
Copyright Vitaliy Koshelenko vetal324@gmail.com
 ******************************************************/

/**
 * Class BookPrintType
 *
 * Print item inner pages print type (color/gray/rizograf/etc.)
 */
class BookPrintType extends Model {

    var $name, $description, $price;
    var $paper_id;
    var $is_color;
    var $min_print_count;

    var $tablename = 'book_print_types';

    function __construct( $id=0 ) {
        $this->status = 1;
        parent::Model( $id );
    }

    function _Load( &$row ) {

        $this->id = intval( $row['id'] );
        $this->paper_id = intval( $row['paper_id'] );

        $this->position = intval( $row['position'] );

        $this->name = trim( $row['name'] );
        $this->description = trim( $row['description'] );

        $this->price = floatval($row['price']);
        $this->is_color = intval($row['is_color']);

        $this->min_print_count = intval( $row['min_print_count'] );

        $md = new MysqlDateTime();
        $md->Parse( $row['created_at'] );
        $this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
        $this->updated_at = $row['updated_at'];

        $this->loaded = true;
    }

    function Save( $data=null ) {
        $this->CheckupData();

        $t = new MysqlTable( $this->tablename );
        if(is_array($data)) {
            $t->save( $data );
        } else {
            $data = Array();
            $data['id'] = $this->id;
            $data['paper_id'] = $this->paper_id;
            $data['name'] = $this->name;
            $data['description'] = $this->description;
            $data['price'] = $this->price;
            $data['is_color'] = $this->is_color;
            $data['min_print_count'] = $this->min_print_count;

            $t->save( $data );
        }

        $this->Load( $t->get_last_insert_id() );

        return( $this->id );
    }

    function Json() {
        $printType = new StdClass();

        $printType->id     = $this->id;

        $printType->name   = $this->name;
        $printType->description   = $this->description;

        $printType->price   = $this->price;

        $printType->is_color   = $this->is_color == 1 ? true : false;

        $printType->min_print_count = $this->min_print_count;

        return $printType;
    }


    function Xml($full=true) {
        $retval = "<book_print_type>";
        $retval .= "<id>{$this->id}</id>";
        $retval .= "<paper_id>{$this->paper_id}</paper_id>";
        $retval .= "<name>{$this->name}</name>";
        $retval .= "<description><![CDATA[{$this->description}]]></description>";
        $retval .= "<is_color>{$this->is_color}</is_color>";
        $retval .= "<price>{$this->price}</price>";
        $retval .= "<min_print_count>{$this->min_print_count}</min_print_count>";

        $retval .= "</book_print_type>";

        return( $retval );
    }

}

?>
<?php
/*****************************************************
Class v.1.0, 2017
Copyright Vitaliy Koshelenko vetal324@gmail.com
 ******************************************************/

/**
 * Class BookCoverPaperType
 *
 * (paper type for cover)
 *
 */
class BookCoverPaperType extends Model {

    var $name, $description, $density, $price;
    var $cover_id;
    var $is_laminated;

    var $tablename = 'book_cover_paper_types';

    function __construct( $id=0 ) {
        $this->status = 1;
        parent::Model( $id );
    }

    function _Load( &$row ) {
        $this->id = intval( $row['id'] );
        $this->cover_id = intval( $row['cover_id'] );

        $this->position = intval( $row['position'] );

        $this->name = trim( $row['name'] );
        $this->description = trim( $row['description'] );

        $this->density = intval( $row['density'] );
        $this->price = floatval($row['price']);

        $this->is_laminated = intval($row['is_laminated']);

        $md = new MysqlDateTime();
        $md->Parse( $row['created_at'] );
        $this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
        $this->updated_at = $row['updated_at'];

        $this->loaded = true;
    }


    function Save( $data=null ) {
        $this->CheckupData();

        $t = new MysqlTable( $this->tablename );
        if(is_array($data)) {
            $t->save( $data );
        } else {
            $data = Array();
            $data['id'] = $this->id;
            $data['cover_id'] = $this->cover_id;
            $data['name'] = $this->name;
            $data['description'] = $this->description;
            $data['price'] = $this->price;
            $data['density'] = $this->density;
            $data['is_laminated'] = $this->is_laminated;

            $t->save( $data );
        }

        $this->Load( $t->get_last_insert_id() );

        return( $this->id );
    }

    function Json() {
        $paperType = new StdClass();

        $paperType->id     = $this->id;

        $paperType->name   = $this->name;
        $paperType->description   = $this->description;

        $paperType->price   = $this->price;

        $paperType->density   = $this->density;
        $paperType->is_laminated   = $this->is_laminated == 1 ? true : false;

        return $paperType;
    }


    function Xml() {
        $retval = "<book_cover_paper_type>";
        $retval .= "<id>{$this->id}</id>";
        $retval .= "<cover_id>{$this->cover_id}</cover_id>";
        $retval .= "<name>{$this->name}</name>";
        $retval .= "<description><![CDATA[{$this->description}]]></description>";
        $retval .= "<price>{$this->price}</price>";
        $retval .= "<density>{$this->density}</density>";
        $retval .= "<is_laminated>{$this->is_laminated}</is_laminated>";
        $retval .= "</book_cover_paper_type>";

        return( $retval );
    }
}

?>
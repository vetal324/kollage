<?php
/*****************************************************
 Class v.1.0, 2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

class SOPDelivery extends Model
{
    var $name, $price, $status, $order_prefix;
    
    var $tablename = 'sop_deliveries';

    function SOPDelivery( $id=0 )
    {
        $this->price = 0.0;
        $this->status = 1;
        parent::Model( $id );
    }
    
    function _Load( &$row )
    {
        $this->id = intval( $row['id'] );
        $this->price = floatval( $row['price'] );
        $this->status = intval( $row['status'] );
        $this->position = intval( $row['position'] );
        $this->order_prefix = $row['order_prefix'];
        
        $t = new MysqlTable('dictionary');
        if( $t->find_all( "parent_id={$this->id} and folder='{$this->tablename}' and lang='". $_SESSION['lang'] ."'" ) )
        {
            foreach( $t->data as $row2 )
            {
                switch( $row2['name'] )
                {
                    case 'name';
                        $this->name = $row2['value'];
                        break;
                }
            }
        }
        
        $md = new MysqlDateTime();
        $md->Parse( $row['created_at'] );
        $this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
        
        $this->updated_at = $row['updated_at'];
        
        $this->loaded = true;
    }
    
    function Save( $data=null )
    {   
        $this->CheckupData();
        
        $t = new MysqlTable( $this->tablename );
        if( is_array($data) ) $t->save( $data );
        else
        {
            $data = Array();
            $data['id'] = $this->id;
            $data['name'] = $this->name;
            $data['order_prefix'] = $this->order_prefix;
            $data['price'] = $this->price;
            $data['status'] = $this->status;
            $data['position'] = $this->position;
            $data['lang'] = $_SESSION['lang'];
            
            $t->save( $data );
        }
            
        $this->Load( $t->get_last_insert_id() );
        
        return( $this->id );
    }
    
    function Delete()
    {
        if( $this->IsLoaded() )
        {
            $this->status = 0;
            $this->Save();
        }
    }
    
    function GetXmlPaymentways()
    {
        global $db;
        $retval = '<paymentways>';
        
        $rs = $db->query("select id from sop_paymentways where status=1 order by position,id");
        while( $row = $rs->fetch_row() )
        {
            $o = new SopPaymentway( $row[0] );
            $selected = ( $db->getone("select delivery_id from sop_deliveries_paymentways where delivery_id={$this->id} and paymentway_id={$o->id}") )? 'yes' : 'no';
            $retval .= "<paymentway selected='{$selected}'>";
            $retval .= "<id><![CDATA[{$o->id}]]></id>";
            $retval .= "<name><![CDATA[{$o->name}]]></name>";
            $retval .= "</paymentway>";
        }
        $retval .= "</paymentways>";
        
        return( $retval );
    }
    
    function Xml()
    {
        $retval = "<delivery>";
        $retval .= "<id>{$this->id}</id>";
        $retval .= "<name><![CDATA[{$this->name}]]></name>";
        $retval .= "<price>". sprintf( "%01.2f", $this->price ) ."</price>";
        $retval .= "<status>{$this->status}</status>";
        $retval .= "<order_prefix>{$this->order_prefix}</order_prefix>";
        $retval .= "<position>{$this->position}</position>";
        $retval .= "<created_at>{$this->created_at}</created_at>";
        $retval .= "<updated_at>{$this->updated_at}</updated_at>";
        $retval .= $this->GetXmlPaymentways();
        $retval .= "</delivery>";
        
        return( $retval );
    }
}

?>
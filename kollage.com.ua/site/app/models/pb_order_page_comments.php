<?php
/*****************************************************
 Class v.1.0, 2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

class PBOrderPageComment extends Model
{
	var $order_id, $page, $comments;
	
	var $tablename = 'pb_order_page_comments';

	function __construct( $id=0 )
	{
		parent::Model( $id );
	}
	
	function LoadByOrderAndPage( $order_id, $page ) {
		$this->loaded = false;
		
		if( $order_id > 0 && $page!=0 && $page!=null )
		{
			$t = new MysqlTable( $this->tablename );
			if( $t->find_first( "order_id={$order_id} and page={$page}" ) )
			{
				$this->_Load( $t->data[0] );
			}
		}
	}
	
	function _Load( &$row )
	{
		$this->id = intval( $row['id'] );
		$this->order_id = intval( $row['order_id'] );
		$this->page = intval( $row['page'] );
		$this->comments = $row['comments'];
		
		$md = new MysqlDateTime();
		$md->Parse( $row['created_at'] );
		$this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
		
		$this->updated_at = $row['updated_at'];
		
		$this->loaded = true;
	}
	
	function Save( $data=null )
	{   
		$this->CheckupData();
		
		$t = new MysqlTable( $this->tablename );
		if( is_array($data) ) $t->save( $data );
		else
		{
			$data = Array();
			$data['id'] = $this->id;
			$data['order_id'] = $this->order_id;
			$data['page'] = $this->page;
			$data['comments'] = $this->comments;
			
			$t->save( $data );
		}
			
		$this->Load( $t->get_last_insert_id() );
		
		return( $this->id );
	}
	
	function Xml()
	{
		$retval = "<pb_order_page_comment>";
		$retval .= "<id>{$this->id}</id>";
		$retval .= "<order_id>{$this->order_id}</order_id>";
		$retval .= "<page>{$this->page}</page>";
		$retval .= "<comments><![CDATA[{$this->comments}]]></comments>";
		$retval .= "<created_at>{$this->created_at}</created_at>";
		$retval .= "<updated_at>{$this->updated_at}</updated_at>";
		$retval .= "</pb_order_page_comment>";
		
		return( $retval );
	}
}

?>
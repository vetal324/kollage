<?php
/*****************************************************
 Class v.1.0, 2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

class PBOrderImage extends Model
{
	var $order_id, $type, $page, $personal_page, $comments, $price, $thumbnail, $small, $small_w, $small_h, $original_filename, $filename, $filename_w, $filename_h;
	
	var $tablename = 'pb_order_images';

	function __construct( $id=0 )
	{
		parent::Model( $id );
	}
	
	function _Load( &$row )
	{
		$this->id = intval( $row['id'] );
		$this->order_id = intval( $row['order_id'] );
		$this->page = intval( $row['page'] );
		$this->personal_page = intval( $row['personal_page'] );
		$this->type = intval( $row['type'] );
		$this->comments = $row['comments'];
		$this->price = floatval( $row['price'] );
		$this->thumbnail = $row['thumbnail'];
		$this->small = $row['small'];
		$this->small_w = intval( $row['small_w'] );
		$this->small_h = intval( $row['small_h'] );
		$this->original_filename = $row['original_filename'];
		$this->filename = $row['filename'];
		$this->filename_w = intval( $row['filename_w'] );
		$this->filename_h = intval( $row['filename_h'] );
		
		$md = new MysqlDateTime();
		$md->Parse( $row['created_at'] );
		$this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
		
		$this->updated_at = $row['updated_at'];
		
		$this->loaded = true;
	}
	
	function Save( $data=null )
	{   
		$this->CheckupData();
		
		$t = new MysqlTable( $this->tablename );
		if( is_array($data) ) $t->save( $data );
		else
		{
			$data = Array();
			$data['id'] = $this->id;
			$data['order_id'] = $this->order_id;
			$data['page'] = $this->page;
			$data['personal_page'] = $this->personal_page;
			$data['type'] = $this->type;
			$data['comments'] = $this->comments;
			$data['price'] = $this->price;
			$data['thumbnail'] = $this->thumbnail;
			$data['small'] = $this->small;
			$data['small_w'] = $this->small_w;
			$data['small_h'] = $this->small_h;
			$data['original_filename'] = $this->original_filename;
			$data['filename'] = $this->filename;
			$data['filename_w'] = $this->filename_w;
			$data['filename_h'] = $this->filename_h;
			
			$t->save( $data );
		}
			
		$this->Load( $t->get_last_insert_id() );
		
		return( $this->id );
	}
	
	function Delete()
	{
		if( $this->IsLoaded() )
		{
			$this->DeleteFiles();
			parent::Delete();
		}
	}
	
	function DeleteFiles()
	{
		if( $this->IsLoaded() )
		{
			$order = new SOPOrder( $this->order_id );
			if( $order->IsLoaded() )
			{
				$dir = $order->dir ."/". $order->order_number ."/";
				if( file_exists( $dir . $this->filename ) ) unlink( $dir . $this->filename );
				if( file_exists( $dir . $this->thumbnail ) ) unlink( $dir . $this->thumbnail );
				if( file_exists( $dir . $this->small ) ) unlink( $dir . $this->small );
			}
		}
	}
	
	public static function ParseName( $name ) {
		$retval = Array(
			'type' => PBOrder::PB_IMAGE_TYPE_PAGE,
			'page' => 0,
			'personal_page' => 1
		);
		
		if( preg_match( "/^(cover)_?([\d]*)\.jpg$/i", $name, $arr ) ) {
			$retval['type'] = PBOrder::PB_IMAGE_TYPE_COVER;
			$retval['personal_page'] = $arr[2];
		}
		else if( preg_match( "/^(f1)_?([\d]*)\.jpg$/i", $name, $arr ) ) {
			$retval['type'] = PBOrder::PB_IMAGE_TYPE_FLYLEAF1;
			$retval['personal_page'] = $arr[2];
		}
		else if( preg_match( "/^(f2)_?([\d]*)\.jpg$/i", $name, $arr ) ) {
			$retval['type'] = PBOrder::PB_IMAGE_TYPE_FLYLEAF2;
			$retval['personal_page'] = $arr[2];
		}
		else if( preg_match( "/^([\d]*)_?([\d]*)\.jpg$/i", $name, $arr ) ) {
			$retval['type'] = PBOrder::PB_IMAGE_TYPE_PAGE;
			$retval['page'] = $arr[1];
			$retval['personal_page'] = $arr[2];
		}
		
		return( $retval );
	}
	
	public static function ParseType( $name ) {
		$parse_result = self::ParseName( $name );
		
		$retval = intval( $parse_result['type'] );
		
		return( $retval );
	}
	
	public static function ParsePage( $name ) {
		$parse_result = self::ParseName( $name );
		
		$retval = intval( $parse_result['page'] );
		
		return( $retval );
	}
	
	public static function ParsePersonalPage( $name ) {
		$parse_result = self::ParseName( $name );
		
		$retval = intval( $parse_result['personal_page'] );
		
		return( $retval );
	}
	
	function Xml()
	{
		$retval = "<image>";
		$retval .= "<id>{$this->id}</id>";
		$retval .= "<order_id>{$this->order_id}</order_id>";
		$retval .= "<page>{$this->page}</page>";
		$retval .= "<personal_page>{$this->personal_page}</personal_page>";
		$retval .= "<type>{$this->type}</type>";
		$retval .= "<comments><![CDATA[{$this->comments}]]></comments>";
		$retval .= "<price>{$this->price}</price>";
		$retval .= "<quantity>{$this->quantity}</quantity>";
		$retval .= "<thumbnail>{$this->thumbnail}</thumbnail>";
		$retval .= "<small width='{$this->small_w}' height='{$this->small_h}'>{$this->thumbnail}</small>";
		$retval .= "<original_filename><![CDATA[{$this->original_filename}]]></original_filename>";
		$retval .= "<filename>{$this->filename}</filename>";
		$retval .= "<filename_w>{$this->filename_w}</filename_w>";
		$retval .= "<filename_h>{$this->filename_h}</filename_h>";
		$retval .= "<created_at>{$this->created_at}</created_at>";
		$retval .= "<updated_at>{$this->updated_at}</updated_at>";
		$retval .= "</image>";
		
		return( $retval );
	}
}

?>
<?php
/*****************************************************
 Class v.1.0, 2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

class FairOrderObject extends Model
{
    var $order_id, $fair_object_id, $price;
    var $object;
    var $full_load;
    
    var $tablename = 'fair_order_objects';

    function FairOrderObject( $id=0, $full_load=false )
    {
        $this->full_load = $full_load;
        parent::Model( $id );
    }
    
    function _Load( &$row )
    {
        $this->id = intval( $row['id'] );
        $this->order_id = intval( $row['order_id'] );
        $this->fair_object_id = intval( $row['fair_object_id'] );
        $this->price = floatval( $row['price'] );
        
        if( $this->full_load ) $this->object = new FairObject( $this->product_id, true );
        else $this->object = new FairObject( $this->product_id, false );
        
        $this->loaded = true;
    }
    
    function Save( $data=null )
    {   
        $this->CheckupData();
        
        $t = new MysqlTable( $this->tablename );
        if( is_array($data) ) $t->save( $data );
        else
        {
            $data = Array();
            $data['id'] = $this->id;
            $data['order_id'] = $this->order_id;
            $data['fair_object_id'] = $this->fair_object_id;
            $data['price'] = $this->price;
            $data['lang'] = $_SESSION['lang'];
            
            $t->save( $data );
        }
            
        $this->Load( $t->get_last_insert_id() );
        
        return( $this->id );
    }
    
    function Xml()
    {
        $retval = "<fair_order_object>";
        $retval .= "<id>{$this->id}</id>";
        $retval .= "<order_id>{$this->order_id}</order_id>";
        $retval .= "<fair_object_id>{$this->fair_object_id}</fair_object_id>";
        $retval .= "<price>{$this->price}</price>";
        $retval .= "<created_at>{$this->created_at}</created_at>";
        $retval .= "<updated_at>{$this->updated_at}</updated_at>";
        
        if( $this->object ) $retval .= $this->object->Xml();
        
        $retval .= "</fair_order_object>";
        
        return( $retval );
    }
}

?>
<?php
/*****************************************************
Class v.1.0, 2017
Copyright Vitaliy Koshelenko vetal324@gmail.com
 ******************************************************/

/**
 * Class BookPageType
 *
 * Print item inner pages page type
 */
class BookPaperType extends Model {

    var $name, $description, $price;
    var $density;
    var $format_id;

    var $tablename = 'book_paper_types';

    function __construct( $id=0 ) {
        $this->status = 1;
        parent::Model( $id );
    }

    function _Load( &$row ) {

        $this->id = intval( $row['id'] );
        $this->format_id = intval( $row['format_id'] );

        $this->position = intval( $row['position'] );

        $this->name = trim( $row['name'] );
        $this->description = trim( $row['description'] );

        $this->price = floatval($row['price']);
        $this->density = intval($row['density']);

        $md = new MysqlDateTime();
        $md->Parse( $row['created_at'] );
        $this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
        $this->updated_at = $row['updated_at'];

        $this->loaded = true;
    }

    function Save( $data=null ) {
        $this->CheckupData();

        $t = new MysqlTable( $this->tablename );
        if(is_array($data)) {
            $t->save( $data );
        } else {
            $data = Array();
            $data['id'] = $this->id;
            $data['format_id'] = $this->format_id;
            $data['name'] = $this->name;
            $data['description'] = $this->description;
            $data['price'] = $this->price;
            $data['density'] = $this->density;

            $t->save( $data );
        }

        $this->Load( $t->get_last_insert_id() );

        return( $this->id );
    }

    function Json() {
        $paperType = new StdClass();

        $paperType->id     = $this->id;

        $paperType->name   = $this->name;
        $paperType->description   = $this->description;

        $paperType->price   = $this->price;

        $paperType->density   = $this->density;

        $paperType->print_types = new StdClass();
        $paperType->print_types->is_combined = false;
        $paperType->print_types->color = array();
        $paperType->print_types->bw = array();

        $printTypesTable = new MysqlTable('book_print_types');
        $printTypesTable->find_all('paper_id='.$this->id, 'position,id');
        $printTypes = $printTypesTable->data;
        foreach ($printTypes as $printTypesRow) {
            $printTypeId = $printTypesRow['id'];
            $printType = new BookPrintType($printTypeId);
            $printTypeJson = $printType->Json();
            if ($printTypeJson->is_color) {
                $paperType->print_types->color[$printTypeId] = $printTypeJson;
            } else {
                $paperType->print_types->bw[$printTypeId] = $printTypeJson;
            }
        }

        if (sizeof($paperType->print_types->color)>0 && sizeof($paperType->print_types->bw)>0) {
            $paperType->print_types->is_combined = true;
        }

        return $paperType;
    }


    function Xml($full=true) {
        $retval = "<book_paper_type>";
        $retval .= "<id>{$this->id}</id>";
        $retval .= "<format_id>{$this->format_id}</format_id>";
        $retval .= "<name>{$this->name}</name>";
        $retval .= "<description><![CDATA[{$this->description}]]></description>";
        $retval .= "<price>{$this->price}</price>";
        $retval .= "<density>{$this->density}</density>";


        $retval .= "<book_print_types>";
        if ($full) {
            $printTypesTable = new MysqlTable('book_print_types');
            $printTypesTable->find_all('paper_id='.$this->id, 'position,id');
            $printTypes = $printTypesTable->data;
            foreach ($printTypes as $printTypesRow) {
                $printType = new BookPrintType($printTypesRow['id']);
                $printTypeXml = $printType->Xml();
                $retval .= $printTypeXml;
            }
        }
        $retval .= "</book_print_types>";

        $retval .= "</book_paper_type>";

        return( $retval );
    }


}


?>
<?php
/*****************************************************
 Class v.1.0, 2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

class ShopOrder extends Model
{
	var $client_id, $order_number, $paymentway_id, $delivery_id, $address, $comments, $discount, $payed, $tax, $status;
	var $products = Array();
	var $client, $paymentway, $delivery;
	var $full_load;
	var $amount;
	var $currency;
	
	var $name, $phone, $email; //��������� ��� ������
	
	var $tablename = 'shop_orders';

	function ShopOrder( $id=0, $full_load=false )
	{
		$this->full_load = $full_load;
		$this->status = 0;
		$this->amount = 0;
		parent::Model( $id );
	}
	
	function LoadByNumber( $number )
	{
		$this->loaded = false;
		
		$t = new MysqlTable( $this->tablename );
		if( $t->find_first( "order_number='$number'" ) )
		{
			$this->_Load( $t->data[0] );
		}
	}
	
	function _Load( &$row )
	{
		$this->id = intval( $row['id'] );
		
		$this->client_id = intval( $row['client_id'] );
		$this->client = new Client( $this->client_id );
		
		$this->order_number = $row['order_number'];
		
		$this->paymentway_id = intval( $row['paymentway_id'] );
		$this->paymentway = new ShopPaymentway( $this->paymentway_id );
		
		$this->delivery_id = intval( $row['delivery_id'] );
		$this->delivery = new ShopDelivery( $this->delivery_id );
		
		$this->address = $row['address'];
		$this->comments = $row['comments'];
		
		$this->name = $row['name'];
		$this->phone = $row['phone'];
		$this->email = $row['email'];
		
		$this->discount = $row['discount'];
		$this->payed = $row['payed'];
		$this->tax = $row['tax'];
		
		$this->status = intval( $row['status'] );
		
		$md = new MysqlDateTime();
		$md->Parse( $row['created_at'] );
		$this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
		
		$this->updated_at = $row['updated_at'];
		
		$this->loaded = true;
		
		$this->LoadProducts();
		$this->LoadCurrency();
	}
	
	function LoadCurrency( $currency_code=null )
	{
		global $db;
		
		if( $this->IsLoaded() )
		{
			if( !$currency_code )
			{
				$current = new Currency( $_SESSION['currency_id'] );
				$code = $current->code;
			}
			else
			{
				$code = $currency_code;
			}
			$md = new MysqlDateTime( $this->created_at );
			$created_at = $md->GetMysqlValue();
			$id = $db->getone( "select id from currencies where created_at<='". $created_at ."' and code={$code} order by created_at desc limit 1" );
			$this->currency = new Currency( $id );
		}
		else
		{
			$this->currency = null;
		}
	}
	
	function LoadProducts()
	{
		$this->amount = 0;
		$this->products = Array();
		if( $this->IsLoaded() )
		{
			$t = new MysqlTable('shop_order_products');
			$t->find_all( "order_id={$this->id}" );
			foreach( $t->data as $row )
			{
				$p = new ShopOrderProduct($row['id'],$this->full_load);
				array_push( $this->products, $p );
				$this->amount += ($p->price * $p->quantity);
			}
			$this->amount += $this->delivery->price;
		}
	}
	
	function Save( $data=null )
	{   
		$this->CheckupData();
		
		$t = new MysqlTable( $this->tablename );
		if( is_array($data) ) $t->save( $data );
		else
		{
			$data = Array();
			$data['id'] = $this->id;
			$data['client_id'] = $this->client_id;
			$data['order_number'] = $this->order_number;
			$data['paymentway_id'] = $this->paymentway_id;
			$data['delivery_id'] = $this->delivery_id;
			$data['address'] = $this->address;
			$data['name'] = $this->name;
			$data['phone'] = $this->phone;
			$data['email'] = $this->email;
			$data['comments'] = $this->comments;
			$data['discount'] = $this->discount;
			$data['payed'] = $this->payed;
			$data['tax'] = $this->tax;
			$data['status'] = $this->status;
			$data['lang'] = $_SESSION['lang'];
			
			$t->save( $data );
		}
		
		//global $log;
		//$log->Write( "Model ShopOrder->Save. {$t->debug_messages}" );
			
		$this->Load( $t->get_last_insert_id() );
		
		return( $this->id );
	}
	
	function Delete()
	{
		if( $this->IsLoaded() )
		{
			$this->status = 99;
			$this->Save();
		}
	}
	
	function Xml()
	{
		$retval = "<order>";
		$retval .= "<id>{$this->id}</id>";
		$retval .= "<client_id>{$this->client_id}</client_id>";
		$retval .= "<order_number><![CDATA[{$this->order_number}]]></order_number>";
		$retval .= "<paymentway_id>{$this->paymentway_id}</paymentway_id>";
		$retval .= "<delivery_id>{$this->delivery_id}</delivery_id>";
		$retval .= "<address><![CDATA[{$this->address}]]></address>";
		$retval .= "<name><![CDATA[{$this->name}]]></name>";
		$retval .= "<phone><![CDATA[{$this->phone}]]></phone>";
		$retval .= "<email><![CDATA[{$this->email}]]></email>";
		$retval .= "<comments><![CDATA[{$this->comments}]]></comments>";
		$retval .= "<discount>{$this->discount}</discount>";
		$retval .= "<amount>{$this->amount}</amount>";
		$retval .= "<payed>{$this->payed}</payed>";
		$retval .= "<tax>{$this->tax}</tax>";
		$retval .= "<status>{$this->status}</status>";
		$retval .= "<created_at>{$this->created_at}</created_at>";
		$retval .= "<updated_at>{$this->updated_at}</updated_at>";
		
		if($this->client) $retval .= $this->client->Xml();
		if($this->paymentway) $retval .= $this->paymentway->Xml();
		if($this->delivery) $retval .= $this->delivery->Xml();
		
		$retval .= "<products>";
		foreach( $this->products as $o )
		{
			$retval .= $o->Xml();
		}
		$retval .= "</products>";
		
		if( $this->currency ) $retval .= $this->currency->Xml();
		
		$retval .= "</order>";
		
		return( $retval );
	}
}

?>
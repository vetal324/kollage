<?php
/*****************************************************
 Class v.1.0, 2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

class OrderNumber extends Model
{
	var $order_number;
	
	var $tablename = 'order_numbers';

	function OrderNumber( $id=1 ) /* 1-sop; 2-shop; 3-fair; 4-printbook; 5-photobook */
	{
		parent::Model( $id );
	}
	
	function _Load( &$row )
	{
		$this->id = intval( $row['id'] );
		$this->order_number = intval( $row['order_number'] );
		
		$this->loaded = true;
	}
	
	function Save( $data=null )
	{   
		$this->CheckupData();
		
		$t = new MysqlTable( $this->tablename );
		if( is_array($data) ) $t->save( $data );
		else
		{
			$data = Array();
			$data['id'] = $this->id;
			$data['order_number'] = $this->order_number;
			
			$t->save( $data );
		}
			
		$this->Load( $t->get_last_insert_id() );
		
		return( $this->id );
	}
	
	function GetNext()
	{
		$retval = 0;
		
		if( $this->IsLoaded() )
		{
			$retval = sprintf( "%010d", ++$this->order_number );
			$this->Save();
		}
		
		return( $retval );
	}
	
	function Get()
	{
		$retval = 0;
		
		if( $this->IsLoaded() )
		{
			$retval = sprintf( "%010d", $this->order_number );
		}
		
		return( $retval );
	}

}

?>
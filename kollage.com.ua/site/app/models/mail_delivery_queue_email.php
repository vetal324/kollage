<?php

class MailDeliveryQueueEmail extends Model
{
    var $email, $queue_id, $status;
    
    var $tablename = 'mail_delivery_queue_emails';

    function MailDeliveryQueueEmail( $id=0 )
    {
        parent::Model( $id );
    }
    
    function _Load( &$row )
    {
        $this->id = intval( $row['id'] );
        $this->queue_id = intval( $row['queue_id'] );
        $this->status = intval( $row['status'] );

        $this->email = $row['email'];
        
        $md = new MysqlDateTime();
        $md->Parse( $row['created_at'] );
        $this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
        
        $md->Parse( $row['updated_at'] );
        $this->updated_at = $md->GetFrontEndValue('d.m.y.hh.mm');
        
        $this->loaded = true;
    }
    
    function Save( $data=null )
    {   
        $this->CheckupData();
        
        $t = new MysqlTable( $this->tablename );
        if( is_array($data) ) $t->save( $data );
        else
        {
            $data = Array();
            $data['id'] = $this->id;
            $data['email'] = $this->email;
            $data['queue_id'] = $this->queue_id;
            $data['status'] = $this->status;
            $data['lang'] = $_SESSION['lang'];
            
            $t->save( $data );
        }
            
        $this->Load( $t->get_last_insert_id() );
        
        return( $this->id );
    }
    
    function Xml()
    {
        $retval = "<queue_email>";
        $retval .= "<id>{$this->id}</id>";
        $retval .= "<name><![CDATA[{$this->name}]]></name>";
        $retval .= "<position>{$this->position}</position>";
        $retval .= "<created_at>{$this->created_at}</created_at>";
        $retval .= "<updated_at>{$this->updated_at}</updated_at>";
        $retval .= "</queue_email>";
        
        return( $retval );
    }
}

?>
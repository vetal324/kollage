<?php
/*****************************************************
 Class v.1.0, 2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

class FairSettings extends Model
{
    var $order_mail_subject, $order_mail_body, $admin_emails, $admin_order_mail_subject, $admin_order_mail_body;
    
    var $tablename = 'fair_settings';

    function FairSettings( $id=1 )
    {
        parent::Model( $id );
    }
    
    function _Load( &$row )
    {
        $this->id = intval( $row['id'] );
        $this->admin_emails = $row['admin_emails'];
        
        $t = new MysqlTable('dictionary');
        if( $t->find_all( "parent_id={$this->id} and folder='{$this->tablename}' and lang='". $_SESSION['lang'] ."'" ) )
        {
            foreach( $t->data as $row2 )
            {
                switch( $row2['name'] )
                {
                    case 'order_mail_subject';
                        $this->order_mail_subject = $row2['value'];
                        break;
                    case 'order_mail_body';
                        $this->order_mail_body = $row2['value'];
                        break;
                    case 'admin_order_mail_subject';
                        $this->admin_order_mail_subject = $row2['value'];
                        break;
                    case 'admin_order_mail_body';
                        $this->admin_order_mail_body = $row2['value'];
                        break;
                }
            }
        }
        
        $this->loaded = true;
    }
    
    function Save( $data=null )
    {   
        $this->CheckupData();
        
        $t = new MysqlTable( $this->tablename );
        if( is_array($data) ) $t->save( $data );
        else
        {
            $data = Array();
            $data['id'] = $this->id;
            $data['order_mail_subject'] = $this->order_mail_subject;
            $data['order_mail_body'] = $this->order_mail_body;
            $data['admin_emails'] = $this->admin_emails;
            $data['admin_order_mail_subject'] = $this->admin_order_mail_subject;
            $data['admin_order_mail_body'] = $this->admin_order_mail_body;
            
            $t->save( $data );
        }

        $this->Load( $t->get_last_insert_id() );
        
        return( $this->id );
    }
    
    function Xml()
    {
        $retval = "<settings>";
        $retval .= "<id>{$this->id}</id>";
        $retval .= "<order_mail_subject><![CDATA[{$this->order_mail_subject}]]></order_mail_subject>";
        $retval .= "<order_mail_body><![CDATA[{$this->order_mail_body}]]></order_mail_body>";
        $retval .= "<admin_emails><![CDATA[{$this->admin_emails}]]></admin_emails>";
        $retval .= "<admin_order_mail_subject><![CDATA[{$this->admin_order_mail_subject}]]></admin_order_mail_subject>";
        $retval .= "<admin_order_mail_body><![CDATA[{$this->admin_order_mail_body}]]></admin_order_mail_body>";
        $retval .= "</settings>";
        
        return( $retval );
    }
}

?>
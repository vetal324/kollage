<?php
/*****************************************************
 Class v.1.0, 2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

class Article extends Model
{
	var $folder;
	var $category_id, $header, $ingress, $text, $publish_date, $design_type, $status, $show_in_menu, $client_types;
	var $image, $image_width, $image_height;
	var $images;
	var $page_title, $page_keywords;
	var $full_load;
	var $relative_url;
	
	var $folder_for_child = 'article';
	
	var $tablename =  "articles";

	function Article( $id, $full_load=true )
	{
		$this->full_load = $full_load;
		$this->position = 0;
		$this->images = Array();
		$this->status = 1;
		$this->show_in_menu = 1;
		$this->client_types = '';
		$this->design_type = 1;
		$this->publish_date = date( "d.m.Y" );
		$this->Load($id);
	}
	
	/*
		$id_folder content value of ID or FOLDER
	*/
	function Load( $id_folder )
	{
		if( is_numeric($id_folder) )
		{
			$this->LoadByID( intval($id_folder) );
		}
		else
		{
			$this->LoadByFolder( $id_folder );
		}
	}
	
	function LoadByID( $id )
	{
		$this->loaded = false;
		
		if( $id>0 )
		{
			$t = new MysqlTable( $this->tablename);
			if( $t->find_first( "id=$id" ) )
			{
				$this->_Load( $t->data[0] );
			}
		}
	}
	
	function LoadByFolder( $folder )
	{
		$this->loaded = false;
		
		if( $folder!='' )
		{
			$t = new MysqlTable( $this->tablename);
			if( $t->find_first( "folder='$folder'" ) )
			{
				$this->_Load( $t->data[0] );
			}
		}
	}
	
	function _Load( &$row )
	{
		$this->id = intval( $row['id'] );
		$this->category_id = intval( $row['category_id'] );
		$this->folder = $row['folder'];
		
		$md = new MysqlDateTime();
		
		$md->Parse($row['publish_date']);
		$this->publish_date = $md->GetValue( 'd.m.y' );
		
		$this->image = $row['image'];
		if( $aimage = new AImage( SITEROOT . '/data/'. $this->image ) )
		{
			$this->image_width = $aimage->GetWidth();
			$this->image_height = $aimage->GetHeight();
		}
		
		$this->design_type = intval( $row['design_type'] );
		$this->status = intval( $row['status'] );
		$this->show_in_menu = intval( $row['show_in_menu'] );
		$this->client_types = $row['client_types'];
		
		$t = new MysqlTable('dictionary');
		if( $t->find_all( "parent_id={$this->id} and folder='{$this->tablename}' and lang='". $_SESSION['lang'] ."'" ) )
		{
			foreach( $t->data as $row2 )
			{
				switch( $row2['name'] )
				{
					case 'header';
						$this->header = $row2['value'];
						break;
					case 'ingress';
						$this->ingress = $row2['value'];
						break;
					case 'text';
						$this->text = $row2['value'];
						break;
				}
			}
		}
		
		$this->position = intval( $row['position'] );
        
		$this->page_title = $row['page_title'];
		$this->page_keywords = $row['page_keywords'];
		
		$md->Parse( $row['created_at'] );
		$this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
		
		$this->updated_at = $row['updated_at'];
		
		$this->loaded = true;
		
		#Load gallery
		$this->LoadImages();

		if ($this->full_load) {
			$category = new Category($this->category_id);
			$translite_header = url_cyrillic_translite( $this->header );
			$translite_category = url_cyrillic_translite( $category->name );
			$this->relative_url = "/{$translite_category}/{$translite_header}/sa{$this->id}";
		}
	}
	
	function LoadImages()
	{
		if( $this->IsLoaded() )
		{
			$t = new MysqlTable('images');
			if( $t->find_all( "folder='{$this->folder_for_child}' and parent_id={$this->id}", "position,id" ) )
			{
				unset( $this->images ); $this->images = Array();
				foreach( $t->data as $row )
				{
					array_push( $this->images, new Image($row['id'],$this->full_load) );
				}
			}
		}
	}

	function hasClientAccess($client) {
		if (!$this->client_types) return true;
		if ($client && $client->isLoaded()) {
			if ($this->client_types) {
				$types = explode(';', $this->client_types);
				return in_array($client->type_id, $types);
			}
		}
		return false;
	}
	
	function DeleteImage()
	{
		if( $this->IsLoaded() )
		{
			$attachment = new Attachment();
			$attachment->filename = $this->image;
			$attachment->rm();
			
			$this->image = '';
			$this->Save();
		}
	}
	
	function Save( $data=null )
	{   
		$this->CheckupData();
		
		$t = new MysqlTable( $this->tablename );
		if( is_array($data) ) $t->save( $data );
		else
		{
			$data = Array();
			$data['id'] = $this->id;
			$data['category_id'] = $this->category_id;
			$data['folder'] = $this->folder;
			$data['position'] = $this->position;
			$data['header'] = $this->header;
			$data['ingress'] = $this->ingress;
			$data['text'] = $this->text;
			$data['publish_date'] = $this->publish_date;
			$data['image'] = $this->image;
			$data['page_title'] = $this->page_title;
			$data['page_keywords'] = $this->page_keywords;
			$data['design_type'] = $this->design_type;
			$data['status'] = $this->status;
			$data['show_in_menu'] = $this->show_in_menu;
			$data['client_types'] = $this->client_types;
			$data['lang'] = $_SESSION['lang'];
			
			$t->save( $data );
		}
			
		$this->Load( $t->get_last_insert_id() );
		
		return( $this->id );
	}
	
	function Delete()
	{
		if( $this->IsLoaded() )
		{
			$t = new MysqlTable( $this->tablename );
			$t->del( $this->id );
			
			#Remove image from the disk
			$attachment = new Attachment();
			$attachment->filename = $this->image;
			$attachment->rm();
			
			#Remove images stored into gallery from the disk
			foreach( $this->images as $image )
			{
				$image->Delete();
			}
		}
	}
	
	function TransformText()
	{
		$retval = $this->text;
		
		if( $retval != "" )
		{
			// images
			while( preg_match( "/\{img([^;]+);*([lst]*);*([lrc]*);*([0-9]*);*([0-9]*)\}/", $retval, $regs ) )
			{
				$t = new MysqlTable('images');
				$row = $t->find_first("parent_id={$this->id} and tag='{$regs[1]}'");
				$i = new Image( $row['id'] );
				if( $i->IsLoaded() )
				{
					$src = "";
					if( strtolower($regs[2])=="l" && $i->filename!="" ) $src = $i->filename;
					if( strtolower($regs[2])=="s" && $i->small!="" ) $src = $i->small;
					if( strtolower($regs[2])=="t" && $i->tiny!="" ) $src = $i->tiny;
					
					$style = "";
					$align = "";
					if( strtolower($regs[3])=="l" ) { $align = "align='left'"; $style.=""; }
					if( strtolower($regs[3])=="r" ) { $align = "align='right'"; $style.=""; }
					if( strtolower($regs[3])=="c" ) { $align = "align='center'"; $style.=""; }
					
					$width = "";
					if( intval($regs[4])>0 ) $width = ' width="'. intval($regs[4]) .'"';
					
					$height = "";
					if( intval($regs[5])>0 ) $height = ' height="'. intval($regs[5]) .'"';
					
					if( $src!="" )
					{
						if( $regs[2]!="l" && $i->filename!="" ) {
							$i->description = str_replace( "'", '&#39;', $i->description );
							$i->description = str_replace( '"', '&quot;', $i->description );
							$i->description = str_replace( "\n", "", $i->description );
							$i->description = str_replace( "\r", "", $i->description );
							$retval = preg_replace( "/\{img". $regs[1] .";*". $regs[2] .";*". $regs[3] .";*([0-9]*);*([0-9]*)\}/", "<a class='highslide' href='". SITEPREFIX ."{$_ENV['data_path']}/{$i->filename}' onclick=\"return hs.expand(this, { slideshowGroup: 1, captionText: '{$i->description}'} )\"><img src='". SITEPREFIX ."{$_ENV['data_path']}/$src' border='0' {$align} {$width} {$height} style='{$style}'/></a>", $retval );
						}
						else {
							$retval = preg_replace( "/\{img". $regs[1] .";*". $regs[2] .";*". $regs[3] .";*([0-9]*);*([0-9]*)\}/", "<img src='". SITEPREFIX ."{$_ENV['data_path']}/$src' border='0' {$align} {$width} {$height} style='{$style}'/>", $retval );
						}
					}
					else
					{
						$retval = preg_replace( "/\{img". $regs[1] .";*([lst]*);*([lrc]*);*([0-9]*);*([0-9]*)\}/", "", $retval );
					}
				}
				else
				{
					$retval = preg_replace( "/\{img". $regs[1] .";*([lst]*);*([lrc]*);*([0-9]*);*([0-9]*)\}/", "", $retval );
				}
			}
			// images
			
			// galleries
			while( preg_match( "/\{gallery;*([lst]*);*([0-9]*);*([0-9]*)\}/", $retval, $regs ) )
			{
				$type = strtolower($regs[1]);
				$t = new MysqlTable('images');
				if( $t->find_all("parent_id={$this->id} and folder='{$this->folder_for_child}'") ) {
					
					$w = 0; $width = ""; $width_css = "";
					if( intval($regs[2])>0 ) {
						$w = intval($regs[2]);
						$width = ' width="'. $w .'"';
						$width_css = ';width:'. $w .'px';
					}
					
					$h = 0; $height = ""; $height_css = "";
					if( intval($regs[3])>0 ) {
						$h = intval($regs[3]);
						$height = ' height="'. $h .'"';
						$height_css = ';height:'. $h .'px';
					}
					
					$gallery_id = "gallery_". md5( time() . rand(1,99) );
					
					$gallery_begin = '<div id="'. $gallery_id .'" class="protoshow" style="'. $width_css . $height_css .'"><ul class="show">';
					$gallery_content = "";
					$gallery_end = '</ul></div><script type="text/javascript">Event.observe(window, "load",function(){$("'. $gallery_id .'").protoShow({interval:5000,pauseOnHover:true,transitionType:"fade",captions:false,navigation:true,controls:false});});</script>';
							
					foreach( $t->data as $row ) {
						$i = new Image( $row['id'] );
						if( $i->IsLoaded() )
						{
							$src = "";
							if( $type=="l" && $i->filename!="" ) $src = $i->filename;
							if( $type=="s" && $i->small!="" ) $src = $i->small;
							if( $type=="t" && $i->tiny!="" ) $src = $i->tiny;
							
							if( $src!="" ) {
								$gallery_content .= '<li class="slide" data-slide-interval=""><img src="'. SITEPREFIX . $_ENV['data_path'] .'/'. $src .'" alt="" border="0"'. $width . $height .'/></li>';
							}
						}
					}
					
					if( $gallery_content ) {
						$retval = str_replace( $regs[0], $gallery_begin . $gallery_content . $gallery_end, $retval ); 
					}
				}
			}
			// galleries
		}
		
		return( $retval );
	}
	
	function Xml( $transform=false )
	{
		$add = "";
		if( $transform )
		{
			$add .= "<text_transformed><![CDATA[". $this->TransformText() ."]]></text_transformed>";
		}
	
		$retval = "<article>";
		$retval .= "<id>{$this->id}</id>";
		$retval .= "<category_id>{$this->category_id}</category_id>";
		$retval .= "<folder><![CDATA[{$this->folder}]]></folder>";
		$retval .= "<folder_for_child><![CDATA[{$this->folder_for_child}]]></folder_for_child>";
		$retval .= "<header><![CDATA[{$this->header}]]></header>";
		$retval .= "<ingress><![CDATA[{$this->ingress}]]></ingress>";
		$retval .= "<text><![CDATA[{$this->text}]]></text>";
		$retval .= "<publish_date>{$this->publish_date}</publish_date>";
		$retval .= "<image width='{$this->image_width}' height='{$this->image_height}'>{$this->image}</image>";
		$retval .= "<design_type>{$this->design_type}</design_type>";
		$retval .= "<status>{$this->status}</status>";
		$retval .= "<show_in_menu>{$this->show_in_menu}</show_in_menu>";
		$retval .= "<position>{$this->position}</position>";
		$retval .= "<client_types><![CDATA[{$this->client_types}]]></client_types>";
		$retval .= "<page_title><![CDATA[{$this->page_title}]]></page_title>";
		$retval .= "<page_keywords><![CDATA[{$this->page_keywords}]]></page_keywords>";
		$retval .= "<relative_url><![CDATA[{$this->relative_url}]]></relative_url>";
		$retval .= "<created_at>{$this->created_at}</created_at>";
		$retval .= "<updated_at>{$this->updated_at}</updated_at>";
		
		$retval .= "<images>";
		foreach( $this->images as $img )
		{
			$retval .= $img->Xml();
		}
		$retval .= "</images>";
		
		$retval .= $add;
		
		$retval .= "</article>";
		
		return( $retval );
	}
}

?>
<?php
/*****************************************************
 Class v.1.0, 2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

class PBOrder extends Model
{
	const PB_IMAGE_TYPE_PAGE 				= 1;
	const PB_IMAGE_TYPE_COVER 			= 2;
	const PB_IMAGE_TYPE_FLYLEAF1 		= 3;
	const PB_IMAGE_TYPE_FLYLEAF2 		= 4;
	const PB_IMAGE_TYPE_SUPERCOVER 	= 5;
	
	var $dictionary;

	var $client_id, $order_number, $address, $comments, $discount, $tax, $status, $payed;
	var $paymentway_id, $delivery_id, $delivery_price;
	
	// objects
	var $paymentway, $delivery;
	var $size, $cover, $supercover, $paper, $lamination, $paper_base, $personal_cover, $box, $design_work, $forzac;
	
	var $pages, $quantity, $personal_pages, $selected_personal_pages, $size_id, $cover_id, $supercover_id, $paper_id, $lamination_id, $paper_base_id, $personal_cover_id, $box_id, $design_work_id, $forzaces, $forzac_id;
	var $images, $images_count;
	var $price_base, $price_per_page, $price_personal_page, $price_personal_cover, $cover_price, $supercover_price, $paper_price, $lamination_price, $paper_base_price, $personal_cover_price, $box_price, $design_work_base_price, $design_work_price, $forzac_price;
	var $price_amount, $discount_price, $total_price;
	var $dir;
	var $client;
	var $db_discount = 0;
	var $log;
	
	var $calculated_price_one, $calculated_price_design_work, $calculated_price_box;
	
	var $full_load = false;
	
	/* status
		0 - ���������, �������������
		1 - ������
		2 - �������� �� �����������
		3 - ���������
	*/
	
	var $tablename = 'pb_orders';

	function __construct( $id=0, $full_load=false )
	{
		global $log;
		$this->log = $log;
		
		$this->full_load = $full_load;
		
		$this->dir = SITEROOT .'/'. $_ENV['printbook_path'] .'/orders/';
		
		$this->status = 0;
		$this->tax = 0;
		$this->images_count = 0;
		$this->discount = 0;
		$this->price_amount = 0;
		$this->total_amount = 0;
		$this->payed = 0;
		
		$this->images = Array();
		
		$this->define_dictionary();
		
		parent::Model( $id );
	}
	
	function LoadByNumber( $number, $full_load=false )
	{
		$this->loaded = false;
		$this->full_load = $full_load;
		
		$t = new MysqlTable( $this->tablename );
		if( $t->find_first( "order_number='$number'" ) )
		{
			$this->_Load( $t->data[0] );
		}
	}
	
	function _Load( &$row )
	{
		global $db;
		
		$this->id = intval( $row['id'] );
		$this->client_id = intval( $row['client_id'] );
		$this->order_number = $row['order_number'];
		$this->address = $row['address'];
		$this->comments = $row['comments'];
		
		$this->pages = intval( $row['pages'] );
		$this->forzaces = intval( $row['forzaces'] );
		$this->quantity = intval( $row['quantity'] );
		$this->personal_pages = intval( $row['personal_pages'] );
		$this->selected_personal_pages = $row['selected_personal_pages'];
		
		$this->size_id = intval( $row['size_id'] );
		$this->cover_id = intval( $row['cover_id'] );
		$this->supercover_id = intval( $row['supercover_id'] );
		$this->paper_id = intval( $row['paper_id'] );
		$this->lamination_id = intval( $row['lamination_id'] );
		$this->paper_base_id = intval( $row['paper_base_id'] );
		$this->personal_cover_id = intval( $row['personal_cover_id'] );
		$this->box_id = intval( $row['box_id'] );
		$this->design_work_id = intval( $row['design_work_id'] );
        $this->forzac_id = intval( $row['forzac_id'] );
		
		$this->price_base = floatval( $row['price_base'] );
		$this->price_per_page = floatval( $row['price_per_page'] );
		$this->price_personal_page = floatval( $row['price_personal_page'] );
		$this->price_personal_cover = floatval( $row['price_personal_cover'] ); //isn't used
		$this->cover_price = floatval( $row['cover_price'] );
		$this->supercover_price = floatval( $row['supercover_price'] );
		$this->paper_price = floatval( $row['paper_price'] );
		$this->lamination_price = floatval( $row['lamination_price'] );
		$this->paper_base_price = floatval( $row['paper_base_price'] );
		$this->personal_cover_price = floatval( $row['personal_cover_price'] );
		$this->box_price = floatval( $row['box_price'] );
		$this->design_work_base_price = floatval( $row['design_work_base_price'] );
		$this->design_work_price = floatval( $row['design_work_price'] );
        $this->forzac_price = floatval( $row['forzac_price'] );
		
		$this->paymentway_id = intval( $row['paymentway_id'] );
		$this->delivery_id = intval( $row['delivery_id'] );
		$this->delivery_price = floatval( $row['delivery_price'] );
		
		$this->tax = intval( $row['tax'] );
		$this->payed = floatval( $row['payed'] );
		$this->db_discount = floatval( $row['discount'] );
		
		$this->status = intval( $row['status'] );
		$this->position = intval( $row['position'] );
		
		$md = new MysqlDateTime();
		$md->Parse( $row['created_at'] );
		$this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
		
		$md->Parse( $row['updated_at'] );
		$this->updated_at = $md->GetFrontEndValue('d.m.y.hh.mm');
		
		if( $this->full_load ) {
			$this->paymentway = new PBPaymentway( $this->paymentway_id );
			$this->delivery = new PBDelivery( $this->delivery_id );
			$this->size = new PBSize( $this->size_id );
			$this->cover = new PBCover( $this->cover_id );
			$this->supercover = new PBSuperCover( $this->supercover_id );
			$this->paper = new PBPaper( $this->paper_id );
			$this->lamination = new PBLamination( $this->lamination_id );
			$this->paper_base = new PBPaperBase( $this->paper_base_id );
			$this->personal_cover = new PBPersonalCover( $this->personal_cover_id );
			$this->box = new PBBox( $this->box_id );
			$this->design_work = new PBDesignWork( $this->design_work_id );
            $this->forzac = new PBForzace( $this->forzac_id );
		}
		
		$this->loaded = true;
		
		$this->CalculatePrices();
	}
	
	function define_dictionary()
	{
		$dpath = sprintf('%s/%s/%s.xml', SITEROOT, $_ENV['app_path'] .'/'. $_ENV['views_path'], $_SESSION['lang'] );
		$this->dictionary = new Dictionary( $dpath, 'utf-8' );
	}
	
	function getForzacesTitle() {
		$retval = "";
		
		if( $this->forzaces == 1 ) $retval = $this->dictionary->get('/locale/photobook/calc/forzaces_yes');
		else if( $this->forzaces == 2 ) $retval = $this->dictionary->get('/locale/photobook/calc/forzaces_no');
		
		return( $retval );
	}
	
	function getPersonalPagesTitle() {
		$retval = "";
		
		if( $this->personal_pages == 0 ) $retval = $this->dictionary->get('/locale/common/no');
		else if( $this->personal_pages == 1 ) $retval = $this->dictionary->get('/locale/common/yes');
		
		return( $retval );
	}
	
	function getPersonalCoverTitle() {
		$retval = "";
		
		if( $this->personal_cover_id == 0 ) $retval = $this->dictionary->get('/locale/common/no');
		else if( $this->personal_cover_id == 1 ) $retval = $this->dictionary->get('/locale/common/yes');
		
		return( $retval );
	}
	
	function CalculatePrices()
	{
		global $db, $log;
		
		if( $this->IsLoaded() )
		{
			//Discount in percent
			$client = new Client( $this->client_id );
			$this->client = $client;
			$this->discount = 0;
			
			$this->price_amount = 0;
			$this->images_count = intval($db->getone( "select count(id) from pb_order_images where order_id={$this->id}" ));
			
			if( $client->IsLoaded() && $client->printbook_discount > 0 ) {
				$this->discount = $client->printbook_discount;
			}
			else { 
				$this->discount += $this->GetDiscountPercent();
			}
			
			if( $this->paymentway && $this->paymentway->IsLoaded() && $this->paymentway->discount > 0 )
			{
				$this->discount = $this->paymentway->discount;
			}
			
			//���� ���������� � ���������� ������ ���� ������, �� ������� ��� ������
			
			$pbs = new PBSettings();
			if( $pbs->IsLoaded() && $pbs->dont_use_discount == 1 )
			{
				$this->discount = 0;
			}
			
			//var $calculated_price_one, $calculated_price_design_work, $calculated_price_box;
			$this->calculated_price_one = $this->price_base;
			$this->calculated_price_one += $this->price_per_page * $this->pages;
			$this->calculated_price_one += $this->paper_price * $this->pages;
			$this->calculated_price_one += $this->lamination_price * $this->pages;
			$this->calculated_price_one += $this->paper_base_price * $this->pages;
			$this->calculated_price_one += $this->cover_price;
			$this->calculated_price_one += $this->supercover_price;
            $this->calculated_price_one += $this->forzac_price;
			$this->calculated_price_one += $this->personal_pages * $this->price_personal_page - $this->personal_pages * $this->price_per_page;
			if( $this->personal_cover_id > 0 ) {
				$this->calculated_price_one += $this->personal_cover_price;
			}
			
			$this->calculated_price_design_work = $this->design_work_base_price + $this->design_work_price * $this->pages;
			$this->calculated_price_box = $this->box_price;
			$this->calculated_price_one += $this->calculated_price_box;
			
			$this->discount_price = $this->GetDiscountPrice( $this->calculated_price_one );
			$this->calculated_price_one -= $this->discount_price;
			$this->calculated_price_one = floor( $this->calculated_price_one );
			
			$this->price_amount = $this->calculated_price_one * $this->quantity + $this->calculated_price_design_work;
			
			$this->total_price = $this->price_amount + $this->delivery_price;
		}
	}
	
	function LoadImages( $page=0, $rows_per_page=0 )
	{
		if( $this->IsLoaded() )
		{
			$limit = "";
			if( $page>0 && $rows_per_page>0 ) $limit = ($page-1)*$rows_per_page .','. $rows_per_page;
			
			$this->images = Array();
			$t = new MysqlTable('pb_order_images');
			if( $t->find_all( "order_id={$this->id}", "id", $limit ) )
			{
				$this->images = Array();
				foreach( $t->data as $row )
				{
					$p = new PBOrderImage($row['id']);
					array_push( $this->images, $p );
				}
			}
		}
	}
	
	function GenerateOrderNumber( $client_id=0 )
	{
		$on = new OrderNumber(1);
		$retval = $on->GetNext();
		
		return( $retval );
	}
	
	// SITEROOT/sop/{order_number}/
	function CreateFolders()
	{
		$retval = true;
		
		if( $this->client_id>0 && $this->order_number )
		{
			$dir = $this->dir . $this->order_number;
			if( $retval && !is_dir( $dir ) ) {
				$retval = mkdir( $dir, 0770 );
				chmod( $dir, 0770 );
			}
		}
		
		return( $retval );
	}
	
	function DeleteFolder()
	{
		$retval = true;
		
		if( $retval = $this->IsLoaded() )
		{
			$dir = $this->dir . $this->order_number;
			if( preg_match("/printbook\/orders\/.+$/i", $dir) )
			{
				rmdir_rf( $dir );
			}
			$md = new MysqlDateTime( $this->created_at );
			$dir = sprintf( "%s%s/%s", $this->dir, $md->GetFrontEndValue('y.m.d'), $this->order_number );
			if( preg_match("/printbook\/orders\/.+$/i", $dir) )
			{
				rmdir_rf( $dir );
			}
		}
		
		return( $retval );
	}
	
	function Save( $data=null )
	{   
		$this->CheckupData();
		
		//insert record, but generate order number before this
		if( !$this->id )
		{
			$this->order_number = $this->GenerateOrderNumber( $this->client_id );
			$this->log->Write( "pb_order->Save() Generate order number N=". $this->order_number );
			$this->CreateFolders();
		}
		
		$t = new MysqlTable( $this->tablename, true );
		if( is_array($data) ) $t->save( $data );
		else
		{
			$data = Array();
			$data['id'] = $this->id;
			$data['client_id'] = $this->client_id;
			$data['order_number'] = $this->order_number;
			
			$data['pages'] = $this->pages;
			$data['quantity'] = $this->quantity;
			$data['forzaces'] = $this->forzaces;
			
			$data['paymentway_id'] = $this->paymentway_id;
			$data['delivery_id'] = $this->delivery_id;
			$data['delivery_price'] = $this->delivery_price;
			$data['size_id'] = $this->size_id;
			$data['cover_id'] = $this->cover_id;
			$data['supercover_id'] = $this->supercover_id;
			$data['paper_id'] = $this->paper_id;
			$data['lamination_id'] = $this->lamination_id;
			$data['paper_base_id'] = $this->paper_base_id;
			$data['personal_cover_id'] = $this->personal_cover_id;
			$data['box_id'] = $this->box_id;
			$data['design_work_id'] = $this->design_work_id;
            $data['forzac_id'] = $this->forzac_id;
			
			$data['price_base'] = $this->price_base;
			$data['price_per_page'] = $this->price_per_page;
			$data['price_personal_page'] = $this->price_personal_page;
			$data['price_personal_cover'] = $this->price_personal_cover;
			$data['cover_price'] = $this->cover_price;
			$data['supercover_price'] = $this->supercover_price;
			$data['paper_price'] = $this->paper_price;
			$data['lamination_price'] = $this->lamination_price;
			$data['paper_base_price'] = $this->paper_base_price;
			$data['personal_cover_price'] = $this->personal_cover_price;
			$data['box_price'] = $this->box_price;
			$data['design_work_base_price'] = $this->design_work_base_price;
			$data['design_work_price'] = $this->design_work_price;
            $data['forzac_price'] = $this->forzac_price;
			
			$data['personal_pages'] = $this->personal_pages;
			$data['selected_personal_pages'] = $this->selected_personal_pages;
			
			$data['address'] = $this->address;
			$data['comments'] = $this->comments;
			$data['discount'] = $this->discount;
			$data['tax'] = $this->tax;
			$data['status'] = $this->status;
			$data['payed'] = $this->payed;
			
			$t->save( $data );
			
		}
		//$this->log->Write( "pb_order->Save() Data=". $t->debug_messages );
			
		$this->Load( $t->get_last_insert_id() );
		
		return( $this->id );
	}
	
	function UploadImageForm( $posted_file )
	{
		global $db;
		$retval = false;
		
		if( $retval = $this->IsLoaded() )
		{
			$prefix = $_ENV['printbook_path'] ."/orders/". $this->order_number ."/";
			$dir = $this->dir ."/". $this->order_number ."/";
			$attachment = new Attachment( $prefix );
			$filetype = new AFileType( $_FILES[$posted_file]['tmp_name'] );
			if( $retval=($filetype->GetType()==FILE_JPG) && $retval = $attachment->upload($posted_file) )
			{
				$this->SaveUploadedImage( $attachment->filename, $attachment->name );
			}
			else
			{
				$this->log->Write( "pb_order->UploadImageForm() order_id={$this->id} filetype=". $filetype->GetType() ." prefix={$prefix}" );
			}
		}
		else
		{
			$this->log->Write( "pb_order->UploadImageForm() Order object isn't loaded." );
		}
		
		return( $retval );
	}
	
	function SaveUploadedImage( $real_name, $file_name ) {
		global $db;
		$dir = $this->dir ."/". $this->order_number ."/";
		if( file_exists($dir.$real_name) ) {
			$op = new PBOrderImage();
			$op->order_id = $this->id;
			$op->filename = $real_name;
			$op->original_filename = $file_name;
			$op->type = PBOrderImage::ParseType( $file_name );
			$op->page = PBOrderImage::ParsePage( $file_name );
			$op->personal_page = PBOrderImage::ParsePersonalPage( $file_name );
			
			//make thumbnail
			$io = new AImage( $dir . $real_name );
			$w = $io->GetWidth(); $h = $io->GetHeight();
			$op->filename_w = $w;
			$op->filename_h = $h;
			if( $io->CopyResize( $dir . 'thumbnail-'. $real_name, 240, 180 ) )
			{
				$op->thumbnail = 'thumbnail-'. $real_name;
			}
			
			if( $w > $h ) { $width = 800; $height = 0; }
			else { $width = 0; $height = 800; }
			if( $io->CopyResize( $dir . 'small-'. $real_name, $width, $height ) )
			{
				$op->small = 'small-'. $real_name;
				$io->SetFilename( $dir . $op->small );
				$op->small_w = $io->GetWidth();
				$op->small_h = $io->GetHeight();
			}
			
			$this->CalculatePrices();
			$op->Save();
			$this->CalculatePrices();
		}
	}
	
	function UpdateUploadedImage( $data ) {
		global $db;
		$dir = $this->dir ."/". $this->order_number ."/";
		$id = $data['id'];
		$real_name = $data['filename'];
		$file_name = $data['original_filename'];
		if( $id > 0 && file_exists($dir.$real_name) ) {
			$op = new PBOrderImage( $id );
			if( $op->IsLoaded() ) { 
				$data['type'] = PBOrderImage::ParseType( $file_name );
				$data['page'] = PBOrderImage::ParsePage( $file_name );
				$data['personal_page'] = PBOrderImage::ParsePersonalPage( $file_name );
				
				//make thumbnail
				$io = new AImage( $dir . $real_name );
				$w = $io->GetWidth(); $h = $io->GetHeight();
				$data['filename_w'] = $w;
				$data['filename_h'] = $h;
				if( $io->CopyResize( $dir . 'thumbnail-'. $real_name, 240, 180 ) )
				{
					$data['thumbnail'] = 'thumbnail-'. $real_name;
				}
				
				if( $w > $h ) { $width = 800; $height = 0; }
				else { $width = 0; $height = 800; }
				if( $io->CopyResize( $dir . 'small-'. $real_name, $width, $height ) )
				{
					$data['small'] = 'small-'. $real_name;
					$io->SetFilename( $dir . $op->small );
					$data['small_w'] = $io->GetWidth();
					$data['small_h'] = $io->GetHeight();
				}
				
				$this->CalculatePrices();
				$op->Save( $data );
				$this->CalculatePrices();
			}
		}
	}

	function DeleteAllImages() {
		if( $this->IsLoaded() )
		{
			foreach( $this->images as $obj )
			{
				$obj->Delete();
			}
		}
	}
	
	function Delete()
	{
		if( $this->IsLoaded() )
		{
			$this->DeleteAllImages();
			$this->DeleteFolder();
			$this->status = 99;
			$this->Save();
		}
	}
	
	function GetDiscountPercent() {
		$retval = 0;
		
		if( $this->IsLoaded() && $this->size && $this->size->IsLoaded() ) {
				if( $this->size->discount1 > 0 && $this->quantity >= 3 && $this->quantity <= 4 ) $retval = $this->size->discount1;
				else if( $this->size->discount2 > 0 && $this->quantity >= 5 && $this->quantity <= 9 ) $retval = $this->size->discount2;
				else if( $this->size->discount3 > 0 && $this->quantity >= 10 ) $retval = $this->size->discount3;
		}
		
		return( $retval );
	}
	
	function GetDiscountPrice( $price_one ) {
		return( $price_one * $this->GetDiscountPercent() / 100 );
	}
	
	function HasSupercover() {
		$retval = false;
		
		if( $this->supercover_id > 0 ) $reetval = true;
		
		return( $retval );
	}
	
	function HasForzaces() {
		$retval = false;
		
		if( $this->forzaces == 1 ) $reetval = true;
		
		return( $retval );
	}
	
	function Xml()
	{
		$retval = "<order>";
		$retval .= "<id>{$this->id}</id>";
		$retval .= "<client_id>{$this->client_id}</client_id>";
		$retval .= "<order_number><![CDATA[{$this->order_number}]]></order_number>";
		$retval .= "<address><![CDATA[{$this->address}]]></address>";
		$retval .= "<comments><![CDATA[{$this->comments}]]></comments>";
		
		$retval .= "<paymentway_id>{$this->paymentway_id}</paymentway_id>";
		$retval .= "<delivery_id>{$this->delivery_id}</delivery_id>";
		$retval .= "<delivery_price>{$this->delivery_price}</delivery_price>";
		
		$retval .= "<size_id>{$this->size_id}</size_id>";
		$retval .= "<cover_id>{$this->cover_id}</cover_id>";
		$retval .= "<supercover_id>{$this->supercover_id}</supercover_id>";
		$retval .= "<paper_id>{$this->paper_id}</paper_id>";
		$retval .= "<lamination_id>{$this->lamination_id}</lamination_id>";
		$retval .= "<paper_base_id>{$this->paper_base_id}</paper_base_id>";
		$retval .= "<personal_cover_id>{$this->personal_cover_id}</personal_cover_id>";
		$retval .= "<personal_pages>{$this->personal_pages}</personal_pages>";
		$retval .= "<selected_personal_pages>{$this->selected_personal_pages}</selected_personal_pages>";
		$retval .= "<box_id>{$this->box_id}</box_id>";
		$retval .= "<design_work_id>{$this->design_work_id}</design_work_id>";
        $retval .= "<forzac_id>{$this->forzac_id}</forzac_id>";
		
		$retval .= "<price_base>{$this->price_base}</price_base>";
		$retval .= "<price_per_page>{$this->price_per_page}</price_per_page>";
		$retval .= "<price_personal_page>{$this->price_personal_page}</price_personal_page>";
		$retval .= "<price_personal_cover>{$this->price_personal_cover}</price_personal_cover>";
		$retval .= "<cover_price>{$this->cover_price}</cover_price>";
		$retval .= "<supercover_price>{$this->supercover_price}</supercover_price>";
		$retval .= "<paper_price>{$this->paper_price}</paper_price>";
		$retval .= "<lamination_price>{$this->lamination_price}</lamination_price>";
		$retval .= "<paper_base_price>{$this->paper_base_price}</paper_base_price>";
		$retval .= "<personal_cover_price>{$this->personal_cover_price}</personal_cover_price>";
		$retval .= "<box_price>{$this->box_price}</box_price>";
		$retval .= "<design_work_base_price>{$this->design_work_base_price}</design_work_base_price>";
		$retval .= "<design_work_price>{$this->design_work_price}</design_work_price>";
        $retval .= "<forzac_price>{$this->forzac_price}</forzac_price>";
		
		$retval .= "<tax>{$this->tax}</tax>";
		$retval .= "<payed>". sprintf( "%01.2f", $this->payed ) ."</payed>";
		$retval .= "<status>{$this->status}</status>";
		$retval .= "<pages>{$this->pages}</pages>";
		$retval .= "<quantity>{$this->quantity}</quantity>";
		$retval .= "<forzaces>{$this->forzaces}</forzaces>";
		$retval .= "<images_count>{$this->images_count}</images_count>";
		$retval .= "<calculated_price_one>". sprintf( "%01.2f", $this->calculated_price_one ) ."</calculated_price_one>";
		$retval .= "<calculated_price_design_work>". sprintf( "%01.2f", $this->calculated_price_design_work ) ."</calculated_price_design_work>";
		$retval .= "<price_amount>". sprintf( "%01.2f", $this->price_amount ) ."</price_amount>";
		$retval .= "<discount_price>". sprintf( "%01.2f", $this->discount_price ) ."</discount_price>";
		$retval .= "<total_price>". sprintf( "%01.2f", $this->total_price ) ."</total_price>";
		$retval .= "<discount>{$this->discount}</discount>";
		$retval .= "<position>{$this->position}</position>";
		$retval .= "<created_at>{$this->created_at}</created_at>";
		$retval .= "<updated_at>{$this->updated_at}</updated_at>";
		
		if( $this->full_load ) {
			$retval .= $this->paymentway->Xml();
			$retval .= $this->delivery->Xml();
			$retval .= $this->size->Xml();
			$retval .= $this->cover->Xml();
			$retval .= $this->supercover->Xml();
			$retval .= $this->paper->Xml();
			$retval .= $this->lamination->Xml();
			$retval .= $this->paper_base->Xml();
			$retval .= $this->personal_cover->Xml();
			$retval .= $this->box->Xml();
			$retval .= $this->design_work->Xml();
            $retval .= $this->forzac->Xml();
		}
		
		$retval .= "<images>";
		foreach( $this->images as $p )
		{
			$retval .= $p->Xml();
		}
		$retval .= "</images>";

		if( $this->client ) $retval .= $this->client->Xml();
		
		$retval .= "</order>";
		
		return( $retval );
	}
}

?>
<?php
/*****************************************************
 Class v.1.0, 2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

class SOPDiscount extends Model
{
    var $name, $client_type_id, $discount;
    var $client_type, $client_types;
    
    var $tablename = 'sop_discounts';

    function SOPDiscount( $id=0 )
    {
        $this->client_types = Array();
        parent::Model( $id );
    }
    
    function _Load( &$row )
    {
        $this->id = intval( $row['id'] );
        $this->client_type_id = intval( $row['client_type_id'] );
        $this->discount = floatval( $row['discount'] );
        
        $this->position = intval( $row['position'] );
        
        $t = new MysqlTable('dictionary');
        if( $t->find_all( "parent_id={$this->id} and folder='{$this->tablename}' and lang='". $_SESSION['lang'] ."'" ) )
        {
            foreach( $t->data as $row2 )
            {
                switch( $row2['name'] )
                {
                    case 'name';
                        $this->name = $row2['value'];
                        break;
                }
            }
        }
        
        $md = new MysqlDateTime();
        $md->Parse( $row['created_at'] );
        $this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
        
        $this->updated_at = $row['updated_at'];
        
        $this->loaded = true;
        
        $this->client_type = new ClientType( $this->client_type_id );
    }
    
    function Save( $data=null )
    {   
        $this->CheckupData();
        
        $t = new MysqlTable( $this->tablename );
        if( is_array($data) ) $t->save( $data );
        else
        {
            $data = Array();
            $data['id'] = $this->id;
            $data['client_type_id'] = $this->client_type_id;
            $data['name'] = $this->name;
            $data['discount'] = $this->discount;
            $data['position'] = $this->position;
            $data['lang'] = $_SESSION['lang'];
            
            $t->save( $data );
        }
            
        $this->Load( $t->get_last_insert_id() );
        
        return( $this->id );
    }
    
    function LoadClientTypes()
    {
        $t = new MysqlTable('client_types');
        if( $t->find_all( "","position,id" ) )
        {
            $this->client_types = Array();
            foreach( $t->data as $row )
            {
                array_push( $this->client_types, new ClientType($row['id']) );
            }
        }
    }
    
    function Xml()
    {
        $this->LoadClientTypes();
        
        $retval = "<discount>";
        $retval .= "<id>{$this->id}</id>";
        $retval .= "<client_type_id>{$this->client_type_id}</client_type_id>";
        $retval .= "<name><![CDATA[{$this->name}]]></name>";
        $retval .= "<discount>{$this->discount}</discount>";
        $retval .= "<position>{$this->position}</position>";
        $retval .= "<created_at>{$this->created_at}</created_at>";
        $retval .= "<updated_at>{$this->updated_at}</updated_at>";
        
        $retval .= "<client_type>";
        if( $this->IsLoaded() && $this->client_type->IsLoaded() ) $retval .= $this->client_type->Xml();
        $retval .= "</client_type>";
        
        $retval .= "<client_types>";
        foreach( $this->client_types as $o )
        {
            $retval .= $o->Xml();
        }
        $retval .= "</client_types>";
        
        $retval .= "</discount>";
        
        return( $retval );
    }
}

?>
<?php
/*****************************************************
 Class v.1.0, 2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

class User extends Model
{
    var $name, $email, $login, $pwd;
    
    var $folder_for_child = 'user';
    
    var $tablename = 'users';

    function User( $id=0 )
    {
        parent::Model( $id );
    }
    
    /*
        $id_folder content value of ID or FOLDER
    */
    function Load( $id_folder )
    {
        if( is_numeric($id_folder) )
        {
            $this->LoadByID( intval($id_folder) );
        }
    }
    
    function LoadByID( $id )
    {
        $this->loaded = false;
        
        if( $id>0 )
        {
            $t = new MysqlTable( $this->tablename );
            if( $t->find_first( "id=$id" ) )
            {
                $this->_Load( $t->data[0] );
            }
        }
    }
    
    function _Load( &$row )
    {
        $this->id = intval( $row['id'] );
        
        $md = new MysqlDateTime();
        
        $this->name = $row['name'];
        $this->email = $row['email'];
        $this->login = $row['login'];
        $this->pwd = $row['pwd'];
        
        $this->position = intval( $row['position'] );
        
        $md->Parse( $row['created_at'] );
        $this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
        
        $this->updated_at = $row['updated_at'];
        
        $this->loaded = true;
    }
    
    function Save( $data=null )
    {   
        $this->CheckupData();
        
        $t = new MysqlTable( $this->tablename );
        if( is_array($data) ) $t->save( $data );
        else
        {
            $data = Array();
            $data['id'] = $this->id;
            $data['name'] = $this->name;
            $data['email'] = $this->email;
            $data['login'] = $this->login;
            $data['pwd'] = $this->pwd;
            $data['position'] = $this->position;
            
            $t->save( $data );
        }
            
        $this->Load( $t->get_last_insert_id() );
        
        return( $this->id );
    }
    
    function GetRolesXml()
    {
        global $db;
        $retval = '';
        
        $t = new MysqlTable('roles');
        $t->find_all('','position,id');
        foreach( $t->data as $row )
        {
            $o = new Role( $row['id'] );
            $selected = ( $db->getone("select role_id from user_roles where role_id={$o->id} and user_id={$this->id}") )? 'yes' : 'no';
            $retval .= "<role selected='{$selected}'>";
            $retval .= "<id><![CDATA[{$o->id}]]></id>";
            $retval .= "<name><![CDATA[{$o->name}]]></name>";
            $retval .= "<code><![CDATA[{$o->code}]]></code>";
            $retval .= "</role>";
        }
        
        return( $retval );
    }
    
    function Xml()
    {
        $retval = "<user>";
        $retval .= "<id>{$this->id}</id>";
        $retval .= "<name><![CDATA[{$this->name}]]></name>";
        $retval .= "<email><![CDATA[{$this->email}]]></email>";
        $retval .= "<login><![CDATA[{$this->login}]]></login>";
        $retval .= "<pwd><![CDATA[{$this->pwd}]]></pwd>";
        $retval .= "<position>{$this->position}</position>";
        $retval .= "<created_at>{$this->created_at}</created_at>";
        $retval .= "<updated_at>{$this->updated_at}</updated_at>";
        
        $retval .= "<roles>";
        $retval .= $this->GetRolesXml();
        $retval .= "</roles>";
        
        $retval .= "</user>";
        
        return( $retval );
    }

}

?>
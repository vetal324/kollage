<?php

class Client extends Model
{
	var $folder;
	var $type_id, $parent_id, $country_id, $professional, $firstname, $lastname, $lastname2, $description, $email, $password, $city, $zip, $street_id, $address, $phone, $bdate, $send_news, $purse, $allow_fair, $activation_key, $allow_userfile, $reset_password_key;
	var $type, $types, $tokens, $street, $country;
	var $status; /* 1-needs authorization; 2-active; 0-inactive; 3-order process is denied */
	var $ipaddress, $network_name;
	var $order_limit = 0;
	var $phone2; //city phone
	var $phone3; //maybe wrong phone
	
	var $photo_discount = 0;
	var $printbook_discount = 0;
	
	var $folder_for_child = 'client';
	
	var $tablename = 'clients';
	
	function Client( $id=0 )
	{
		$this->street = new Street();
		$this->country = new Country();
		$this->id = $id;
		$this->position = 0;
		$this->professional = 0;
		$this->status = 1;
		$this->country_id = 0;
		$this->send_news = 1;
		$this->allow_fair = 0;
		$this->allow_userfile = 0;
		$this->photo_discount = 0;
		$this->printbook_discount = 0;
		$this->types = Array();
		$this->tokens = Array();
		
		$this->ipaddress = $_SERVER['REMOTE_ADDR'];
		$this->network_name = '';
		
		$this->Load($id);
		$this->LoadTypes();
	}
	
	/*
		$id_folder content value of ID or FOLDER
	*/
	function Load( $id_folder )
	{
		if( is_numeric($id_folder) )
		{
			$this->LoadByID( intval($id_folder) );
		}
	}
	
	function LoadByID( $id )
	{
		$this->loaded = false;
		
		if( $id>0 )
		{
			$t = new MysqlTable( $this->tablename );
			if( $t->find_first( "id=$id" ) )
			{
				$this->_Load( $t->data[0] );
			}
		}
	}
	
	function LoadByEmail( $email )
	{
		$this->loaded = false;
		
		$t = new MysqlTable( $this->tablename );
		if( $t->find_first( "email='$email' AND status=2" ) )
		{
			$this->_Load( $t->data[0] );
		}
	}
	
	function LoadByPhone( $country_id, $phone )
	{
		global $log;

		$this->loaded = false;
		
		$t = new MysqlTable( $this->tablename, true );
		if( $t->find_first( "country_id='$country_id' AND phone='$phone'" ) )
		{
			$this->_Load( $t->data[0] );
		}
	}
	
	function LoadByActivationKey( $key )
	{
		$this->loaded = false;
		
		$t = new MysqlTable( $this->tablename );
      $key = preg_replace( '@[\s\\\/=&\(\)\[\]\{\}%\^\+\-\._]+@', '', $key );
		if( $t->find_first( "activation_key='$key'" ) )
		{
			$this->_Load( $t->data[0] );
		}
	}
	
	function LoadByResetPasswordKey( $key )
	{
		$this->loaded = false;
		
		$t = new MysqlTable( $this->tablename );
        $key = preg_replace( '@[\s\\\/=&\(\)\[\]\{\}%\^\+\-\._]+@', '', $key );
		if( $t->find_first( "reset_password_key='$key'" ) )
		{
			$this->_Load( $t->data[0] );
		}
	}
	
	function LoadByEmailAndPassword( $email, $password )
	{
		$this->loaded = false;
		
		$t = new MysqlTable( $this->tablename );
		if( $t->find_first( "email='$email' AND password='$password' AND status=2" ) )
		{
			$this->_Load( $t->data[0] );
		}
	}
	
	function LoadByPhoneAndPassword( $country_id, $phone, $password )
	{
		$this->loaded = false;
		
		$t = new MysqlTable( $this->tablename, true );
		if( $t->find_first( "country_id='$country_id' AND phone='$phone' AND password='$password' AND status=2" ) )
		{
			$this->_Load( $t->data[0] );
		}
	}
	
	function _Load( &$row )
	{
		global $db;
		
		$this->id = intval( $row['id'] );
		
		$md = new MysqlDateTime();
		
		$this->type_id = intval( $row['type_id'] );
		$this->parent_id = intval( $row['parent_id'] );
		$this->street_id = intval( $row['street_id'] );
		$this->country_id = intval( $row['country_id'] );
		$this->firstname = $row['firstname'];
		$this->lastname = $row['lastname'];
		$this->lastname2 = $row['lastname2'];
		$this->description = $row['description'];
		$this->email = $row['email'];
		$this->password = $row['password'];
		$this->city = $row['city'];
		$this->zip = $row['zip'];
		$this->address = $row['address'];
		$this->phone = $row['phone'];
		$this->phone2 = $row['phone2'];
		$this->phone3 = $row['phone3'];
		$this->status = intval( $row['status'] );
		$this->send_news = intval( $row['send_news'] );
		$this->professional = intval( $row['professional'] );
		$this->allow_fair = intval( $row['allow_fair'] );
		$this->allow_userfile = intval( $row['allow_userfile'] );
		$this->photo_discount = floatval( $row['photo_discount'] );
		$this->printbook_discount = floatval( $row['printbook_discount'] );
		$this->activation_key = $row['activation_key'];
		$this->reset_password_key = $row['reset_password_key'];
		
		$this->purse = sprintf( "%01.2f", floatval( $row['purse'] ) );
		
		$this->position = intval( $row['position'] );
		
		$md->Parse( $row['bdate'] );
		$this->bdate = $md->GetFrontEndValue('d.m.y');
		
		$md->Parse( $row['created_at'] );
		$this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
		
		$this->updated_at = $row['updated_at'];
		
		$this->order_limit = ( intval($db->getone("select count(*) from sop_orders where client_id={$this->id} and status>=3 and status<=10")) > 0 )? 0 : 1;
		
		$this->street = new Street( $this->street_id );
		$this->country = new Country( $this->country_id );

		$this->LoadTokens();
		
		$this->loaded = true;
		
		$this->LoadType();
	}

	function isType($type) {
		if ($this->isLoaded() && $this->type_id === $type) return true;
		return false;
	}

	function isRegular() {
		return $this->isType(ClientType::RETAIL);
	}

	function isOpt() {
		return $this->isType(ClientType::WHOLESALE);
	}

	function isLargeOpt() {
		return $this->isType(ClientType::BIG_WHOLESALE);
	}

	function isProf() {
		return $this->isType(ClientType::PROFESSIONAL);
	}

	function isAds() {
		return $this->isType(ClientType::ADMAN);
	}
	
	function Save( $data=null )
	{   
		global $log;

		$this->CheckupData();
		
		$t = new MysqlTable( $this->tablename, true );
		if( is_array($data) ) $t->save( $data );
		else
		{
			$data = Array();
			$data['id'] = $this->id;
			$data['type_id'] = $this->type_id;
			$data['parent_id'] = $this->parent_id;
			$data['street_id'] = $this->street_id;
			$data['country_id'] = $this->country_id;
			$data['firstname'] = $this->firstname;
			$data['lastname'] = $this->lastname;
			$data['lastname2'] = $this->lastname2;
			$data['description'] = $this->description;
			$data['email'] = $this->email;
			$data['password'] = $this->password;
			$data['city'] = $this->city;
			$data['zip'] = $this->zip;
			$data['address'] = $this->address;
			$data['phone'] = ($this->phone)? $this->phone : null;
			$data['phone2'] = ($this->phone2)? $this->phone2 : null;
			$data['phone3'] = ($this->phone3)? $this->phone3 : null;
			$data['bdate'] = $this->bdate;
			$data['status'] = $this->status;
			$data['send_news'] = $this->send_news;
			$data['professional'] = $this->professional;
			$data['purse'] = $this->purse;
			$data['allow_fair'] = $this->allow_fair;
			$data['allow_userfile'] = $this->allow_userfile;
			$data['photo_discount'] = $this->photo_discount;
			$data['printbook_discount'] = $this->printbook_discount;
			$data['position'] = $this->position;
			$data['activation_key'] = $this->activation_key;
			$data['reset_password_key'] = $this->reset_password_key;
			
			$t->save( $data );
		}

		//$log->Write( 'Client model. Error. '. $t->debug_messages );
			
		$this->Load( $t->get_last_insert_id() );
		
		return( $this->id );
	}
	
	function Delete()
	{
		if( $this->IsLoaded() )
		{
			$this->status = 99;
			$this->email = 'deleted('. date("Y-m-d H:i") .') '. $this->email;
			$this->phone3 .= ' '. $this->phone;
			$this->phone = null;
			$this->Save();
		}
	}
	
	function Xml()
	{
		$retval = "<client>";
		$retval .= "<id>{$this->id}</id>";
		$retval .= "<type_id>{$this->type_id}</type_id>";
		$retval .= "<parent_id>{$this->parent_id}</parent_id>";
		$retval .= "<street_id>{$this->street_id}</street_id>";
		$retval .= "<region_id>{$this->street->region_id}</region_id>";
		$retval .= "<type><![CDATA[{$this->type}]]></type>";
		$retval .= "<folder_for_child><![CDATA[{$this->folder_for_child}]]></folder_for_child>";
		$retval .= "<firstname><![CDATA[{$this->firstname}]]></firstname>";
		$retval .= "<lastname><![CDATA[{$this->lastname}]]></lastname>";
		$retval .= "<lastname2><![CDATA[{$this->lastname2}]]></lastname2>";
		$retval .= "<description><![CDATA[{$this->description}]]></description>";
		$retval .= "<email><![CDATA[{$this->email}]]></email>";
		$retval .= "<password><![CDATA[{$this->password}]]></password>";
		$retval .= "<city><![CDATA[{$this->city}]]></city>";
		$retval .= "<zip><![CDATA[{$this->zip}]]></zip>";
		$retval .= "<address><![CDATA[{$this->address}]]></address>";
		$retval .= "<phone><![CDATA[{$this->phone}]]></phone>";
		$retval .= "<phone2><![CDATA[{$this->phone2}]]></phone2>";
		$retval .= "<phone3><![CDATA[{$this->phone3}]]></phone3>";
		$retval .= "<bdate>{$this->bdate}</bdate>";
		$retval .= "<status>{$this->status}</status>";
		$retval .= "<send_news>{$this->send_news}</send_news>";
		$retval .= "<allow_fair>{$this->allow_fair}</allow_fair>";
		$retval .= "<allow_userfile>{$this->allow_userfile}</allow_userfile>";
		$retval .= "<professional>{$this->professional}</professional>";
		$retval .= "<purse>{$this->purse}</purse>";
		$retval .= "<photo_discount>{$this->photo_discount}</photo_discount>";
		$retval .= "<printbook_discount>{$this->printbook_discount}</printbook_discount>";
		$retval .= "<order_limit>{$this->order_limit}</order_limit>";
		$retval .= "<position>{$this->position}</position>";
		$retval .= "<created_at>{$this->created_at}</created_at>";
		$retval .= "<updated_at>{$this->updated_at}</updated_at>";
		$retval .= "<ipaddress>{$this->ipaddress}</ipaddress>";
		$retval .= "<network_name>{$this->network_name}</network_name>";
		
		$retval .= "<types>";
		foreach( $this->types as $o )
		{
			$retval .= $o->Xml();
		}
		$retval .= "</types>";
		
		$retval .= $this->street->Xml();
		$retval .= $this->country->Xml();
		
		$retval .= "</client>";
		
		return( $retval );
	}
	
	function LoadType()
	{
		if( $this->IsLoaded() )
		{
			$t = new MysqlTable('client_types');
			if( $row = $t->find_first( "id={$this->type_id}" ) )
			{
				$o = new ClientType( $row['id'] );
				$this->type = $o->name;
			}
		}
	}
	
	function LoadTokens()
	{
		$t = new MysqlTable('tokens');
		if( $t->find_all( "client_id={$this->id}" ) )
		{
			unset( $this->tokens ); $this->tokens = Array();
			foreach( $t->data as $row )
			{
				array_push( $this->tokens, new Token($row['id']) );
			}
		}
	}
	
	function LoadTypes()
	{
		$t = new MysqlTable('client_types');
		if( $t->find_all( "","position,id" ) )
		{
			unset( $this->types ); $this->types = Array();
			foreach( $t->data as $row )
			{
				array_push( $this->types, new ClientType($row['id']) );
			}
		}
	}
	
	function RecalculatePurse()
	{
		global $db;

		if( $this->IsLoaded() )
		{
			$sql = "update clients set purse=(select sum(amount) from purse_operations where client_id={$this->id}) where id={$this->id}";
			$db->query( $sql );
		}
	}
}

?>
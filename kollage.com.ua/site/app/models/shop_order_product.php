<?php
/*****************************************************
 Class v.1.0, 2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

class ShopOrderProduct extends Model
{
	var $order_id, $product_id, $product_name, $price, $quantity, $pexists;
	var $product;
	var $full_load;
	
	var $tablename = 'shop_order_products';

	function ShopOrderProduct( $id=0, $full_load=false )
	{
		$this->full_load = $full_load;
		parent::Model( $id );
	}
	
	function _Load( &$row )
	{
		$this->id = intval( $row['id'] );
		$this->order_id = intval( $row['order_id'] );
		$this->product_id = intval( $row['product_id'] );
		$this->product_name = trim( $row['product_name'] );
		$this->price = floatval( $row['price'] );
		$this->quantity = intval( $row['quantity'] );
		$this->pexists = intval( $row['pexists'] );
		
		if( $this->full_load ) $this->product = new ShopProduct( $this->product_id, true );
		else $this->product = new ShopProduct( $this->product_id, false );
		
		$this->loaded = true;
	}
	
	function Save( $data=null )
	{   
		$this->CheckupData();
		
		$t = new MysqlTable( $this->tablename );
		if( is_array($data) ) $t->save( $data );
		else
		{
			$data = Array();
			$data['id'] = $this->id;
			$data['order_id'] = $this->order_id;
			$data['product_id'] = $this->product_id;
			$data['product_name'] = $this->product_name;
			$data['price'] = $this->price;
			$data['quantity'] = $this->quantity;
			$data['pexists'] = $this->pexists;
			$data['lang'] = $_SESSION['lang'];
			
			$t->save( $data );
		}
			
		$this->Load( $t->get_last_insert_id() );
		
		return( $this->id );
	}
	
	function Xml()
	{
		$retval = "<order_product>";
		$retval .= "<id>{$this->id}</id>";
		$retval .= "<order_id>{$this->order_id}</order_id>";
		$retval .= "<product_id>{$this->product_id}</product_id>";
		$retval .= "<product_name><![CDATA[{$this->product_name}]]></product_name>";
		$retval .= "<price>{$this->price}</price>";
		$retval .= "<quantity>{$this->quantity}</quantity>";
		$retval .= "<pexists>{$this->pexists}</pexists>";
		$retval .= "<created_at>{$this->created_at}</created_at>";
		$retval .= "<updated_at>{$this->updated_at}</updated_at>";
		
		if( $this->product ) $retval .= $this->product->Xml();
		
		$retval .= "</order_product>";
		
		return( $retval );
	}
}

?>
<?php
/*****************************************************
 Class v.1.0, 2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

class SOPOrder extends Model
{
	var $client_id, $order_number, $point_id, $paymentway_id, $delivery_id, $address, $comments, $discount, $tax, $status, $photo_paper, $border, $payed, $platform;
	var $point, $paymentway, $delivery;
	var $photos, $photos_price_amount, $photos_count, $discount_price, $total_price;
   var $liqpay_payment_id, $liqpay_status;
	var $dir;
	var $client;
	var $delivery0 = 300; //set delivery=0 if total photo price is 300uah
	var $db_discount = 0;
	var $log;
	
	/* status
		0 - временный, редактируемый
		1 - принят
		2 - спечатан на лаборатории
		3 - доставлен
	*/
	
	var $tablename = 'sop_orders';

	function SOPOrder( $id=0 )
	{
		global $log;
		$this->log = $log;
		
		$this->dir = SITEROOT .'/'. $_ENV['sop_path'] .'/';
		
		$this->status = 0;
		$this->photo_paper = 1;
		$this->border = 1;
		$this->tax = 0;
		$this->photos_price_amount = 0;
		$this->photos_count = 0;
		$this->discount = 0;
		$this->payed = 0;
		
		$t = new MysqlTable('sop_deliveries');
		if( $row = $t->find_first("status=1","position,id") )
		{
			$this->delivery_id = $row['id'];
			$this->delivery = new SOPDelivery( $this->delivery_id );
		}
		
		$this->photos = Array();
		
		parent::Model( $id );
	}
	
	function LoadByNumber( $number )
	{
		$this->loaded = false;
		
		$t = new MysqlTable( $this->tablename );
		if( $t->find_first( "order_number='$number'" ) )
		{
			$this->_Load( $t->data[0] );
		}
	}
	
	function _Load( &$row )
	{
		global $db;
		
		$this->id = intval( $row['id'] );
		$this->client_id = intval( $row['client_id'] );
		$this->order_number = $row['order_number'];
		$this->address = $row['address'];
		$this->comments = $row['comments'];
		$this->point_id = intval( $row['point_id'] );
		$this->paymentway_id = intval( $row['paymentway_id'] );
		$this->delivery_id = intval( $row['delivery_id'] );
		$this->tax = intval( $row['tax'] );
		$this->photo_paper = intval( $row['photo_paper'] );
		$this->border = intval( $row['border'] );
		$this->payed = floatval( $row['payed'] );
		$this->db_discount = floatval( $row['discount'] );
      $this->platform = $row['platform'];
      $this->liqpay_payment_id = $row['liqpay_payment_id'];
		$this->liqpay_status = $row['liqpay_status'];
		
		$this->status = intval( $row['status'] );
		$this->position = intval( $row['position'] );
		
		$md = new MysqlDateTime();
		$md->Parse( $row['created_at'] );
		$this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
		
		$md->Parse( $row['updated_at'] );
		$this->updated_at = $md->GetFrontEndValue('d.m.y.hh.mm');
		
		$this->point = new SOPPoint( $this->point_id );
		$this->paymentway = new SOPPaymentway( $this->paymentway_id );
		$this->delivery = new SOPDelivery( $this->delivery_id );
		
		$this->loaded = true;
		
		$this->CalculatePrices();
	}
	
	function CalculatePrices()
	{
		global $db, $log;
		
		if( $this->IsLoaded() )
		{
			//Discount in percent
			$client = new Client( $this->client_id );
			$this->client = $client;
			
			$this->discount = 0;
			$this->photos_price_amount = $db->getone( "select sum(quantity*price) as s from sop_order_photos where order_id={$this->id}" );
			$this->photos_count = intval($db->getone( "select sum(quantity) from sop_order_photos where order_id={$this->id}" ));
			
			if( $this->paymentway->IsLoaded() && $this->paymentway->discount > 0 )
			{
				$this->discount = $this->paymentway->discount;
			}
			
			if( $this->photos_price_amount >= 250 ) $this->discount = 5.0;
			if( $this->photos_price_amount >= 500 ) $this->discount = 10.0;
			if( $this->photos_price_amount >= 1000 ) $this->discount = 20.0;
			
			//если клиент не розничный, то отменяем все скидки ранее насчитанные
			if( $client->IsLoaded() && $client->type_id>1 ) {
				$this->discount = 0;
			}
			
			if( $client->IsLoaded() && $client->photo_discount > 0 )
			{
				$this->discount = $client->photo_discount;
			}

			/* временная скидка при заказе с моб. приложения */
			/*if( $this->platform == 'android' ) {
				$this->discount = 16.5;
			}*/
			
			//Если выставлено в настройках отмена всех скидок, то отменям все скидки
			$sops = new SopSettings();
			if( $sops->IsLoaded() && $sops->dont_use_discount == 1 )
			{
				$this->discount = 0;
			}

			if( $this->status > 0 ) {
				$this->discount = $this->db_discount;
			}
			
			$this->discount_price = $this->photos_price_amount * $this->discount/100;
			$this->total_price = $this->photos_price_amount - $this->discount_price + $this->GetDeliveryPrice();
		}
	}
	
	function LoadPhotos( $page=0, $rows_per_page=0 )
	{
		if( $this->IsLoaded() )
		{
			$limit = "";
			if( $page>0 && $rows_per_page>0 ) $limit = ($page-1)*$rows_per_page .','. $rows_per_page;
			
			$this->photos = Array();
			$t = new MysqlTable('sop_order_photos');
			if( $t->find_all( "order_id={$this->id}", "id", $limit ) )
			{
				$this->photos = Array();
				foreach( $t->data as $row )
				{
					$p = new SOPOrderPhoto($row['id']);
					array_push( $this->photos, $p );
				}
			}
		}
	}
	
	function GenerateOrderNumber( $client_id=0 )
	{
		$on = new OrderNumber(1);
		$retval = $on->GetNext();
		
		return( $retval );
	}
	
	// SITEROOT/sop/{order_number}/
	function CreateFolders()
	{
		$retval = true;
		
		if( $this->order_number )
		{
			$dir = $this->dir . $this->order_number;
			if( $retval && !is_dir( $dir ) ) $retval = mkdir( $dir, 0770 );
		}
		
		return( $retval );
	}
	
	function DeleteFolder()
	{
		$retval = true;
		
		if( $retval = $this->IsLoaded() )
		{
			$dir = $this->dir . $this->order_number;
			if( preg_match("/sop\/.+$/i", $dir) )
			{
				rmdir_rf( $dir );
			}
			$md = new MysqlDateTime( $this->created_at );
			$dir = sprintf( "%s%s/%s", $this->dir, $md->GetFrontEndValue('y.m.d'), $this->order_number );
			if( preg_match("/sop\/.+$/i", $dir) )
			{
				rmdir_rf( $dir );
			}
		}
		
		return( $retval );
	}
	
	function Save( $data=null )
	{   
		$this->CheckupData();
		
		//insert record, but generate order number before this
		if( !$this->id )
		{
			$this->order_number = $this->GenerateOrderNumber( $this->client_id );
			$this->CreateFolders();
		}
		
		$t = new MysqlTable( $this->tablename );
		if( is_array($data) ) $t->save( $data );
		else
		{
			$data = Array();
			$data['id'] = $this->id;
			$data['client_id'] = $this->client_id;
			$data['order_number'] = $this->order_number;
			$data['point_id'] = $this->point_id;
			$data['paymentway_id'] = $this->paymentway_id;
			$data['delivery_id'] = $this->delivery_id;
			$data['address'] = $this->address;
			$data['comments'] = $this->comments;
			$data['discount'] = $this->discount;
			$data['tax'] = $this->tax;
			$data['photo_paper'] = $this->photo_paper;
			$data['border'] = $this->border;
			$data['status'] = $this->status;
			$data['payed'] = $this->payed;
         	$data['platform'] = $this->platform;
         	$data['liqpay_payment_id'] = $this->liqpay_payment_id;
			$data['liqpay_status'] = $this->liqpay_status;
			
			$t->save( $data );
		}
			
		$this->Load( $t->get_last_insert_id() );
		
		return( $this->id );
	}
	
	function UploadPhoto( $posted_file )
	{
		$retval = false;
		
		if( $retval = $this->IsLoaded() )
		{
			$prefix = $_ENV['sop_path'] ."/". $this->order_number ."/";
			$dir = $this->dir ."/". $this->order_number ."/";
			$attachment = new Attachment( $prefix );

			$retval = $attachment->upload($posted_file);
			if( $retval )
			{
				$op = new SOPOrderPhoto();
				$op->order_id = $this->id;
				$op->filename = $attachment->filename;
				
				//make thumbnail
				$io = new AImage( $dir . $attachment->filename );
				$w = $io->GetWidth(); $h = $io->GetHeight();
				if( $io->CopyResize( $dir . 'thumbnail-'. $attachment->filename, 120, 90 ) )
				{
					$op->thumbnail = 'thumbnail-'. $attachment->filename;
				}
				
				if( $w > $h ) { $width = 600; $height = 0; }
				else { $width = 0; $height = 600; }
				if( $io->CopyResize( $dir . 'small-'. $attachment->filename, $width, $height ) )
				{
					$op->small = 'small-'. $attachment->filename;
					$io->SetFilename( $dir . $op->small );
					$op->small_w = $io->GetWidth();
					$op->small_h = $io->GetHeight();
				}
				
				$retval = $op->Save();
				$this->CalculatePrices();

				$this->log->Write( "sop_order->UploadPhoto order_id={$this->id} number={$this->order_number} {$_FILES[$posted_file]['tmp_name']} size={$_FILES[$posted_file]['size']} uploaded to {$attachment->filename}" );
			}
			else
			{
				$this->log->Write( "sop_order->UploadPhoto() order_id={$this->id} {$_FILES[$posted_file]['tmp_name']} size={$_FILES[$posted_file]['size']} prefix={$prefix}" );
			}
		}
		
		return( $retval );
	}
	
	function UploadZip( $posted_file )
	{
		$retval = false;
		$count_photos = 0;
		
		if( $this->IsLoaded() )
		{
			$prefix = $_ENV['sop_path'] ."/". $this->order_number ."/";
			$dir = $this->dir ."/". $this->order_number ."/";
			$filetype = new AFileType( $_FILES[$posted_file]['tmp_name'] );
			if( $filetype->GetType()==FILE_ZIP )
			{
				$tmp_dir_name = 'zip-'.time();
				if( mkdir( $dir.$tmp_dir_name, 0700 ) )
				{
					if( $fp = zip_open( $_FILES[$posted_file]['tmp_name'] ) )
					{
						while( $entry = zip_read($fp) )
						{
							zip_entry_open( $fp, $entry );
							$entry_name = zip_entry_name( $entry );
							if( substr( $entry_name, -1) == '/' ) { /* dir */ }
							else
							{
								//$w = fopen( $dir.'/'.$tmp_dir_name.'/'.$entry_name, "w" );
								$entry_name = basename( $entry_name );
								$w = fopen( $dir.'/'.$tmp_dir_name.'/'.$entry_name, "w" );
								fwrite( $w, zip_entry_read( $entry, zip_entry_filesize($entry) ), zip_entry_filesize($entry) );
								fclose( $w );
								
								//process unpacked image
								$filetype->SetFile( $dir.$tmp_dir_name.'/'.$entry_name );
								if( $filetype->GetType() == FILE_JPG )
								{
									$op = new SOPOrderPhoto();
									$op->order_id = $this->id;
									$op->filename = mt_rand(1,9) .'-'. time() .'-'. $posted_file .'.'. get_file_ext( $entry_name );
									copy( $dir.$tmp_dir_name.'/'.$entry_name, $dir.$op->filename );
									
									//make thumbnail
									$io = new AImage( $dir . $op->filename );
									$w = $io->GetWidth(); $h = $io->GetHeight();
									if( $io->CopyResize( $dir . 'thumbnail-'. $op->filename, 120, 90 ) )
									{
										$op->thumbnail = 'thumbnail-'. $op->filename;
									}
									
									if( $w > $h ) { $width = 600; $height = 0; }
									else { $width = 0; $height = 600; }
									if( $io->CopyResize( $dir . 'small-'. $op->filename, $width, $height ) )
									{
										$op->small = 'small-'. $op->filename;
										$io->SetFilename( $dir . $op->small );
										$op->small_w = $io->GetWidth();
										$op->small_h = $io->GetHeight();
									}
									
									$op->Save();
									
									$count_photos++;
									$retval = true;
								}
								else
								{
									$this->log->Write( "sop_order->UploadZip() It's not JPEG file order_id={$this->id} file=". $dir.$tmp_dir_name.'/'.$entry_name );
								}
							}
							zip_entry_close( $entry );
						}
						zip_close( $fp );
						chdir( $dir );
						rmdir_rf( $dir.$tmp_dir_name );
					}
				}
				$this->CalculatePrices();
			}
			else
			{
				$this->log->Write( "sop_order->UploadZip() order_id={$this->id} It's not ZIP file" );
			}
		}
		
		return( $retval );
	}
	
	function UploadRar( $posted_file )
	{
		$retval = false;
		$count_photos = 0;
		
		if( $this->IsLoaded() )
		{
			$prefix = $_ENV['sop_path'] ."/". $this->order_number ."/";
			$dir = $this->dir ."/". $this->order_number ."/";
			$filetype = new AFileType( $_FILES[$posted_file]['tmp_name'] );
			if( $filetype->GetType()==FILE_RAR )
			{
				$tmp_dir_name = 'rar-'.time();
				if( mkdir( $dir.$tmp_dir_name, 0700 ) )
				{
					if( $fp = rar_open( $_FILES[$posted_file]['tmp_name'] ) )
					{
						$list = rar_list( $fp );
						for( $i=0; $i<count($list); $i++ )
						{
							$entry_name = $list[$i]->getName();
							$entry_name = basename( $entry_name );
							$ext = strtolower( GetFileExtension( $entry_name ) );
							if( $ext == "jpg" )
							{
								$list[$i]->extract( false, $dir.'/'.$tmp_dir_name.'/'. $entry_name );
								
								//process unpacked image
								$filetype->SetFile( $dir.$tmp_dir_name.'/'.$entry_name );
								if( $filetype->GetType() == FILE_JPG )
								{
									$op = new SOPOrderPhoto();
									$op->order_id = $this->id;
									$op->filename = mt_rand(1,9) .'-'. time() .'-'. $posted_file .'.'. get_file_ext( $entry_name );
									copy( $dir.$tmp_dir_name.'/'.$entry_name, $dir.$op->filename );
									
									//make thumbnail
									$io = new AImage( $dir . $op->filename );
									$w = $io->GetWidth(); $h = $io->GetHeight();
									if( $io->CopyResize( $dir . 'thumbnail-'. $op->filename, 120, 90 ) )
									{
										$op->thumbnail = 'thumbnail-'. $op->filename;
									}
									
									if( $w > $h ) { $width = 600; $height = 0; }
									else { $width = 0; $height = 600; }
									if( $io->CopyResize( $dir . 'small-'. $op->filename, $width, $height ) )
									{
										$op->small = 'small-'. $op->filename;
										$io->SetFilename( $dir . $op->small );
										$op->small_w = $io->GetWidth();
										$op->small_h = $io->GetHeight();
									}
									
									$op->Save();
									
									$count_photos++;
									$retval = true;
								}
								else
								{
									$this->log->Write( "sop_order->UploadRar() It's not JPEG file order_id={$this->id} file=". $dir.$tmp_dir_name.'/'.$entry_name );
								}
							}
						}
						rar_close( $fp );
						chdir( $dir );
						rmdir_rf( $dir.$tmp_dir_name );
					}
				}
				$this->CalculatePrices();
			}
			else
			{
				$this->log->Write( "sop_order->UploadRar() order_id={$this->id} It's not RAR file" );
			}
		}
		
		return( $retval );
	}
	
	function Delete()
	{
		if( $this->IsLoaded() )
		{
			foreach( $photos as $obj )
			{
				$obj->Delete();
			}
			$this->DeleteFolder();
			$this->status = 99;
			$this->Save();
		}
	}
	
	function GetDeliveryPrice()
	{
		global $db;
		
		$retval = 0;
		
		if( $this->IsLoaded() && $this->delivery->IsLoaded() )
		{
			if( $this->photos_price_amount < $this->delivery0 )
			{
				$retval = $this->delivery->price;
				if( $this->delivery->id == 5 ) //Post http://ukrposhta.ua/ua/kalkulyator-forma-rozraxunku
				{
					$p = 0;
					$weight = floatval( $db->getone( "select sum(p.quantity*s.weight) from sop_order_photos p left join sop_order_photos_sizes s on s.id=p.size_id where p.order_id={$this->id}" ) );
					if( $weight >= 20 ) //бандероль
					{
						if( $weight <= 100 ) $p = 13.45;
						else if( $weight>100 && $weight<=250 ) $p = 15.25;
						else if( $weight>250 && $weight<=500 ) $p = 17.65;
						else $p = 22.90;
					}
					else if( $weight > 0 && $weight < 20 )//письмо
					{
						$p = 13.45;
					}
					$retval = $p;
					$retval += $this->photos_price_amount * 0.02; //+2% от объявленной стоимости
				}
				if( $this->paymentway->code == 7 ) //наложный платеж
				{
					$p = $this->photos_price_amount + $retval;
					if( $p < 108 ) $np = 5.0;
					else if( $p>=108 && $p<500 ) $np = $p * 0.048;
					else if( $p>=500 && $p<1000 ) $np = $p * 0.024;
					else $np = $p * 0.0144;
					
					$retval += $np;
				}
			}
		}
		
		return( $retval );
	}
	
	function GetPaperName()
	{
		$retval = '';
		
		if( $this->IsLoaded() )
		{
			switch( $this->photo_paper )
			{
				case 2:
					$retval = 'matt';
					break;
				case 1:
				default:
					$retval = 'glossy';
					break;
			}
		}
		
		return( $retval );
	}
	
	function GetPaperPrefix()
	{
		$retval = '';
		
		if( $this->IsLoaded() )
		{
			switch( $this->photo_paper )
			{
				case 2:
					$retval = 'M';
					break;
				case 1:
				default:
					$retval = 'G';
					break;
			}
		}
		
		return( $retval );
	}
	
	function Xml()
	{
		$retval = "<order>";
		$retval .= "<id>{$this->id}</id>";
		$retval .= "<client_id>{$this->client_id}</client_id>";
		$retval .= "<order_number><![CDATA[{$this->order_number}]]></order_number>";
		$retval .= "<address><![CDATA[{$this->address}]]></address>";
		$retval .= "<comments><![CDATA[{$this->comments}]]></comments>";
		$retval .= "<point_id>{$this->point_id}</point_id>";
		$retval .= "<point_name><![CDATA[{$this->point->name}]]></point_name>";
		$retval .= "<point_address><![CDATA[{$this->point->address}]]></point_address>";
		$retval .= "<paymentway_id>{$this->paymentway_id}</paymentway_id>";
		$retval .= "<paymentway_name>{$this->paymentway->name}</paymentway_name>";
		$retval .= "<delivery_id>{$this->delivery_id}</delivery_id>";
		$retval .= "<delivery>". sprintf( "%01.2f", $this->GetDeliveryPrice() ) ."</delivery>";
		$retval .= "<delivery_name>{$this->delivery->name}</delivery_name>";
		$retval .= "<tax>{$this->tax}</tax>";
		$retval .= "<payed>". sprintf( "%01.2f", $this->payed ) ."</payed>";
		$retval .= "<photo_paper>{$this->photo_paper}</photo_paper>";
		$retval .= "<border>{$this->border}</border>";
		$retval .= "<status>{$this->status}</status>";
		$retval .= "<photos_count>{$this->photos_count}</photos_count>";
		$retval .= "<photos_price_amount>". sprintf( "%01.2f", $this->photos_price_amount ) ."</photos_price_amount>";
		$retval .= "<discount_price>". sprintf( "%01.2f", $this->discount_price ) ."</discount_price>";
		$retval .= "<total_price>". sprintf( "%01.2f", $this->total_price ) ."</total_price>";
		$retval .= "<discount>{$this->discount}</discount>";
		$retval .= "<platform>{$this->platform}</platform>";
		$retval .= "<position>{$this->position}</position>";
		$retval .= "<created_at>{$this->created_at}</created_at>";
		$retval .= "<updated_at>{$this->updated_at}</updated_at>";
		
		$retval .= "<photos>";
		foreach( $this->photos as $p )
		{
			$retval .= $p->Xml();
		}
		$retval .= "</photos>";

		if( $this->client ) $retval .= $this->client->Xml();
		
		$retval .= "</order>";
		
		return( $retval );
	}
}

?>
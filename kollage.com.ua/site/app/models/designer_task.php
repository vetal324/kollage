<?php

class DesignerTask extends Model
{
    var $email, $queue_id, $status;
    
    var $tablename = 'designer_tasks';

    function DesignerTask( $id=0 )
    {
        parent::Model( $id );
    }

	function LoadByFolder( $folder, $created )
	{
		$this->loaded = false;
		
		if( $folder!='' )
		{
			$t = new MysqlTable( $this->tablename);
            $where = "folder='$folder'";
            if ($created) $where .= " AND YEAR(folder_created)=YEAR('$created') AND MONTH(folder_created)=MONTH('$created') AND DAY(folder_created)=DAY('$created') AND HOUR(folder_created)=HOUR('$created')";
			if( $t->find_first( $where ) ) {
				$this->_Load( $t->data[0] );
			}
		}
	}
    
    function _Load( &$row )
    {
        $this->id = intval( $row['id'] );
        $this->status = intval( $row['status'] );

        $this->folder = $row['folder'];
        $this->readme = $row['readme'];
        
        $md = new MysqlDateTime();
        $md->Parse( $row['created_at'] );
        $this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
        
        $md->Parse( $row['updated_at'] );
        $this->updated_at = $md->GetFrontEndValue('d.m.y.hh.mm');

        $md->Parse( $row['folder_created'] );
        $this->folder_created = $md->GetFrontEndValue('d.m.y.hh.mm');

        $md->Parse( $row['email_sent_to_designer'] );
        $this->email_sent_to_designer = $md->GetFrontEndValue('d.m.y.hh.mm');

        $md->Parse( $row['email_sent_to_operator'] );
        $this->email_sent_to_operator = $md->GetFrontEndValue('d.m.y.hh.mm');
        
        $this->loaded = true;
    }
    
    function Save( $data=null )
    {   
        $this->CheckupData();
        
        $t = new MysqlTable($this->tablename);
        if( is_array($data) ) $t->save( $data );
        else
        {
            $data = Array();
            $data['id'] = $this->id;
            $data['folder'] = $this->folder;
            $data['folder_created'] = $this->folder_created;
            $data['readme'] = $this->readme;
            $data['status'] = $this->status;
            $data['email_sent_to_designer'] = $this->email_sent_to_designer;
            $data['email_sent_to_operator'] = $this->email_sent_to_operator;
            
            $t->save( $data );
        }
            
        $this->Load( $t->get_last_insert_id() );
        
        return( $this->id );
    }
    
    function Xml()
    {
        $retval = "<queue_email>";
        $retval .= "<id>{$this->id}</id>";
        $retval .= "<status>{$this->status}</status>";
        $retval .= "<folder><![CDATA[{$this->folder}]]></folder>";
        $retval .= "<readme><![CDATA[{$this->readme}]]></readme>";
        $retval .= "<created_at>{$this->created_at}</created_at>";
        $retval .= "<updated_at>{$this->updated_at}</updated_at>";
        $retval .= "</queue_email>";
        
        return( $retval );
    }
}

?>
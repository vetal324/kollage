<?php
/*****************************************************
 Class v.1.0, 2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

class ShopBrand extends Model
{
    var $name, $description, $status;
    var $image, $image_width, $image_height;
    
    var $tablename = 'shop_brands';

    function ShopBrand( $id=0 )
    {
        $this->status = 1;
        parent::Model( $id );
    }
    
    function _Load( &$row )
    {
        $this->id = intval( $row['id'] );
        $this->category_id = intval( $row['category_id'] );
        
        $this->image = $row['image'];
        if( $aimage = new AImage( SITEROOT . '/data/'. $this->image ) )
        {
            $this->image_width = $aimage->GetWidth();
            $this->image_height = $aimage->GetHeight();
        }
        
        $this->status = intval( $row['status'] );
        $this->position = intval( $row['position'] );
        
        $t = new MysqlTable('dictionary');
        if( $t->find_all( "parent_id={$this->id} and folder='{$this->tablename}' and lang='". $_SESSION['lang'] ."'" ) )
        {
            foreach( $t->data as $row2 )
            {
                switch( $row2['name'] )
                {
                    case 'name';
                        $this->name = $row2['value'];
                        break;
                    case 'description';
                        $this->description = $row2['value'];
                        break;
                }
            }
        }
        
        $md = new MysqlDateTime();
        $md->Parse( $row['created_at'] );
        $this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
        
        $this->updated_at = $row['updated_at'];
        
        $this->loaded = true;
    }
    
    function Save( $data=null )
    {   
        $this->CheckupData();
        
        $t = new MysqlTable( $this->tablename );
        if( is_array($data) ) $t->save( $data );
        else
        {
            $data = Array();
            $data['id'] = $this->id;
            $data['category_id'] = $this->category_id;
            $data['name'] = $this->name;
            $data['description'] = $this->description;
            $data['status'] = $this->status;
            $data['position'] = $this->position;
            $data['lang'] = $_SESSION['lang'];
            
            $t->save( $data );
        }
            
        $this->Load( $t->get_last_insert_id() );
        
        return( $this->id );
    }
    
    function Delete()
    {
        if( $this->IsLoaded() )
        {
            $this->status = 0;
            $this->Save();
        }
    }
    
    function Xml()
    {
        $retval = "<brand>";
        $retval .= "<id>{$this->id}</id>";
        $retval .= "<name><![CDATA[{$this->name}]]></name>";
        $retval .= "<description><![CDATA[{$this->description}]]></description>";
        $retval .= "<image width='{$this->image_width}' height='{$this->image_height}'>{$this->image}</image>";
        $retval .= "<status>{$this->status}</status>";
        $retval .= "<position>{$this->position}</position>";
        $retval .= "<created_at>{$this->created_at}</created_at>";
        $retval .= "<updated_at>{$this->updated_at}</updated_at>";
        $retval .= "</brand>";
        
        return( $retval );
    }
}

?>
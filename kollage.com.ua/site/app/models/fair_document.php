<?php
/*****************************************************
 Class v.1.0, 2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

class FairDocument extends Model
{
    var $client_id, $name, $description, $filename;
    
    var $tablename = 'fair_documents';

    function FairDocument( $id=0 )
    {
        parent::Model( $id );
    }
    
    function _Load( &$row )
    {
        $this->id = intval( $row['id'] );
        $this->client_id = intval( $row['client_id'] );
        $this->name = $row['name'];
        $this->filename = $row['filename'];
        
        $this->position = intval( $row['position'] );
        
        $t = new MysqlTable('dictionary');
        if( $t->find_all( "parent_id={$this->id} and folder='{$this->tablename}' and lang='". $_SESSION['lang'] ."'" ) )
        {
            foreach( $t->data as $row2 )
            {
                switch( $row2['name'] )
                {
                    case 'description';
                        $this->description = $row2['value'];
                        break;
                }
            }
        }
        
        $md = new MysqlDateTime();
        $md->Parse( $row['created_at'] );
        $this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
        
        $this->updated_at = $row['updated_at'];
        
        $this->loaded = true;
    }
    
    function Save( $data=null )
    {   
        $this->CheckupData();
        
        $t = new MysqlTable( $this->tablename );
        if( is_array($data) ) $t->save( $data );
        else
        {
            $data = Array();
            $data['id'] = $this->id;
            $data['client_id'] = $this->client_id;
            $data['name'] = $this->name;
            $data['filename'] = $this->filename;
            $data['description'] = $this->description;
            $data['position'] = $this->position;
            $data['lang'] = $_SESSION['lang'];
            
            $t->save( $data );
        }
            
        $this->Load( $t->get_last_insert_id() );
        
        return( $this->id );
    }
    
    function Delete()
    {
        global $db;

        if( $this->IsLoaded() )
        {
            $db->query( "delete from fair_object_documents where fair_document_id={$this->id}" );
            $this->DeleteFile();
            parent::Delete();
        }
    }
    
    function DeleteFile()
    {
        if( $this->IsLoaded() )
        {
            $attachment = new Attachment( $_ENV['fair_documents'] );
            $attachment->filename = $this->filename;
            $attachment->rm();
            
            $this->filename = '';
            $t = new MysqlTable( $this->tablename );
            $data = Array( 'id'=>$this->id, 'filename'=>$this->filename );
            $t->save( $data );
        }
    }
    
    function Xml()
    {
        if( $this->filename ) $filename = $this->filename;
        
        $retval = "<fair_document>";
        $retval .= "<id>{$this->id}</id>";
        $retval .= "<client_id>{$this->client_id}</client_id>";
        $retval .= "<name>{$this->name}</name>";
        $retval .= "<filename>{$filename}</filename>";
        $retval .= "<description><![CDATA[{$this->description}]]></description>";
        $retval .= "<position>{$this->position}</position>";
        $retval .= "<created_at>{$this->created_at}</created_at>";
        $retval .= "<updated_at>{$this->updated_at}</updated_at>";
        $retval .= "</fair_document>";
        
        return( $retval );
    }
}

?>
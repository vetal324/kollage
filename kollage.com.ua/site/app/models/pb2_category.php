<?php
/*****************************************************
 Class v.1.0, 2011
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

class PB2Category extends Model
{
	var $folder, $parent_id, $name, $description, $image;
	
	var $tablename = 'pb2_categories';

	function __construct( $id=0 )
	{
		$this->status = 1;
		parent::Model( $id );
	}
	
	function _Load( &$row )
	{
		$this->id = intval( $row['id'] );
		
		$this->image = $row['image'];
		$this->status = intval( $row['status'] );
		$this->position = intval( $row['position'] );
		$this->parent_id = intval( $row['parent_id'] );
		
		$this->folder = trim( $row['folder'] );
		
		$t = new MysqlTable('dictionary');
		if( $t->find_all( "parent_id={$this->id} and folder='{$this->tablename}' and lang='". $_SESSION['lang'] ."'" ) )
		{
			foreach( $t->data as $row2 )
			{
				switch( $row2['name'] )
				{
					case 'name';
						$this->name = $row2['value'];
						break;
					case 'description';
						$this->description = $row2['value'];
						break;
				}
			}
		}
		
		$md = new MysqlDateTime();
		$md->Parse( $row['created_at'] );
		$this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
		
		$this->updated_at = $row['updated_at'];
		
		$this->loaded = true;
	}
	
	function Save( $data=null )
	{   
		$this->CheckupData();
		
		$t = new MysqlTable( $this->tablename );
		if( is_array($data) ) $t->save( $data );
		else
		{
			$data = Array();
			$data['id'] = $this->id;
			$data['image'] = $this->image;
			$data['status'] = $this->status;
			$data['parent_id'] = $this->parent_id;
			$data['position'] = $this->position;
			$data['folder'] = $this->folder;
			$data['name'] = $this->name;
			$data['description'] = $this->description;
			$data['lang'] = $_SESSION['lang'];
			
			$t->save( $data );
		}
			
		$this->Load( $t->get_last_insert_id() );
		
		return( $this->id );
	}
	
	function Delete()
	{
		if( $this->IsLoaded() )
		{
			$this->status = 0;
			$this->Save();
		}
	}
	
	function DeleteImage()
	{
		if( $this->IsLoaded() )
		{
			$attachment = new Attachment();
			$attachment->filename = $this->image;
			$attachment->rm();
			$attachment->filename = 'thumbnail-'. $attachment->filename;
			$attachment->rm();
			
			$this->image = '';
			$this->Save();
		}
	}
	
	function Xml()
	{
		$retval = "<pb_category>";
		$retval .= "<id>{$this->id}</id>";
		$retval .= "<parent_id>{$this->parent_id}</parent_id>";
		$retval .= "<folder><![CDATA[{$this->folder}]]></folder>";
		$retval .= "<image><![CDATA[{$this->image}]]></image>";
		$retval .= "<name><![CDATA[{$this->name}]]></name>";
		$retval .= "<description><![CDATA[{$this->description}]]></description>";
		$retval .= "<status>{$this->status}</status>";
		$retval .= "<position>{$this->position}</position>";
		$retval .= "<created_at>{$this->created_at}</created_at>";
		$retval .= "<updated_at>{$this->updated_at}</updated_at>";
		$retval .= "</pb_category>";
		
		return( $retval );
	}
}

?>
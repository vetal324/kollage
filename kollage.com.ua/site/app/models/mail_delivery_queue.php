<?php

class MailDeliveryQueue extends Model
{
    var $name, $subject, $from_name, $from_email, $attachment_file, $attachment_name, $body, $status;

    var $total = 0, $sent = 0;
    
    var $tablename = 'mail_delivery_queues';

    function MailDeliveryQueue( $id=0 )
    {
        parent::Model( $id );
    }
    
    function _Load( &$row )
    {
        $this->id = intval( $row['id'] );

        $this->name = $row['name'];
        $this->subject = $row['subject'];
        $this->from_name = $row['from_name'];
        $this->from_email = $row['from_email'];
        $this->attachment_file = $row['attachment_file'];
        $this->attachment_name = $row['attachment_name'];
        $this->body = $row['body'];
        $this->status = intval( $row['status'] );
        
        $md = new MysqlDateTime();
        $md->Parse( $row['created_at'] );
        $this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
        
        $md->Parse( $row['updated_at'] );
        $this->updated_at = $md->GetFrontEndValue('d.m.y.hh.mm');

        $this->loaded = true;

        $this->LoadQueueEmailsInfo();
	}

    function LoadQueueEmailsInfo() {
    	global $db;
    	if( $this->loaded ) {
    		$t = 'mail_delivery_queue_emails';
    		$this->total = $db->getone( "select count(*) from {$t} where queue_id={$this->id}" );
    		$this->sent = $db->getone( "select count(*) from {$t} where queue_id={$this->id} and status=2" );
    	}
    }
    
    function Save( $data=null )
    {   
        $this->CheckupData();
        
        $t = new MysqlTable( $this->tablename );
        if( is_array($data) ) $t->save( $data );
        else
        {
            $data = Array();
            $data['id'] = $this->id;
            $data['name'] = $this->name;
            $data['subject'] = $this->subject;
            $data['from_name'] = $this->from_name;
            $data['from_email'] = $this->from_email;
            $data['attachment_file'] = $this->attachment_file;
            $data['attachment_name'] = $this->attachment_name;
            $data['body'] = $this->body;
            $data['status'] = $this->status;
            $data['lang'] = $_SESSION['lang'];
            
            $t->save( $data );
        }
            
        $this->Load( $t->get_last_insert_id() );
        
        return( $this->id );
    }
    
    function Xml()
    {
        $retval = "<queue>";
        $retval .= "<id>{$this->id}</id>";
        $retval .= "<name><![CDATA[{$this->name}]]></name>";
        $retval .= "<subject><![CDATA[{$this->subject}]]></subject>";
        $retval .= "<from_name><![CDATA[{$this->from_name}]]></from_name>";
        $retval .= "<from_email><![CDATA[{$this->from_email}]]></from_email>";
        $retval .= "<attachment_file><![CDATA[{$this->attachment_file}]]></attachment_file>";
        $retval .= "<attachment_name><![CDATA[{$this->attachment_name}]]></attachment_name>";
        $retval .= "<body><![CDATA[{$this->body}]]></body>";
        $retval .= "<status>{$this->status}</status>";
        $retval .= "<total>{$this->total}</total>";
        $retval .= "<sent>{$this->sent}</sent>";
        $retval .= "<created_at>{$this->created_at}</created_at>";
        $retval .= "<updated_at>{$this->updated_at}</updated_at>";
        $retval .= "</queue>";
        
        return( $retval );
    }
}

?>
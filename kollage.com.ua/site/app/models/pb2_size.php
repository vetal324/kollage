<?php
/*****************************************************
 Class v.1.0, 2011
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

class PB2Size extends Model
{
	var $type, $name, $price_base, $price_per_page, $start_page, $stop_page, $page_block, $status, $price_personal_page, $price_personal_cover, $discount1, $discount2, $discount3;
	var $price_design_work;
	var $image;
	
	var $page_min_width, $page_min_height, $cover_min_width, $cover_min_height;
	
	var $tablename = 'pb2_sizes';

	function __construct( $id=0 )
	{
		$this->status = 1;
		parent::Model( $id );
	}
	
	function _Load( &$row )
	{
		$this->id = intval( $row['id'] );
		
		$this->status = intval( $row['status'] );
		$this->position = intval( $row['position'] );
		
		$this->type = intval( $row['type'] );
		
		$this->name = trim( $row['name'] );
		$this->image = $row['image'];
		
		$this->price_base = floatval( $row['price_base'] );
		$this->price_per_page = floatval( $row['price_per_page'] );
		$this->price_personal_page = floatval( $row['price_personal_page'] );
		$this->price_personal_cover = floatval( $row['price_personal_cover'] );
		$this->price_design_work = floatval( $row['price_design_work'] );
		$this->start_page = intval( $row['start_page'] );
		$this->stop_page = intval( $row['stop_page'] );
		$this->page_block = intval( $row['page_block'] );
		$this->discount1 = intval( $row['discount1'] );
		$this->discount2 = intval( $row['discount2'] );
		$this->discount3 = intval( $row['discount3'] );
		
		$this->page_min_width = intval( $row['page_min_width'] );
		$this->page_min_height = intval( $row['page_min_height'] );
		$this->cover_min_width = intval( $row['cover_min_width'] );
		$this->cover_min_height = intval( $row['cover_min_height'] );
		
		$md = new MysqlDateTime();
		$md->Parse( $row['created_at'] );
		$this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
		
		$this->updated_at = $row['updated_at'];
		
		$this->loaded = true;
	}
	
	function Save( $data=null )
	{   
		$this->CheckupData();
		
		$t = new MysqlTable( $this->tablename );
		if( is_array($data) ) $t->save( $data );
		else
		{
			//$price_personal_page, $discount1, $discount2, $discount3;
			$data = Array();
			$data['id'] = $this->id;
			$data['type'] = $this->type;
			$data['name'] = $this->name;
			$data['image'] = $this->image;
			$data['price_base'] = $this->price_base;
			$data['status'] = $this->status;
			$data['price_per_page'] = $this->price_per_page;
			$data['start_page'] = $this->start_page;
			$data['stop_page'] = $this->stop_page;
			$data['position'] = $this->position;
			$data['page_block'] = $this->page_block;
			$data['price_personal_page'] = $this->price_personal_page;
			$data['price_personal_cover'] = $this->price_personal_cover;
			$data['price_design_work'] = $this->price_design_work;
			$data['discount1'] = $this->discount1;
			$data['discount2'] = $this->discount2;
			$data['discount3'] = $this->discount3;
			$data['page_min_width'] = $this->page_min_width;
			$data['page_min_height'] = $this->page_min_height;
			$data['cover_min_width'] = $this->cover_min_width;
			$data['cover_min_height'] = $this->cover_min_height;
			$data['lang'] = $_SESSION['lang'];
			
			$t->save( $data );
		}
			
		$this->Load( $t->get_last_insert_id() );
		
		return( $this->id );
	}
	
	function Delete()
	{
		if( $this->IsLoaded() )
		{
			$this->status = 0;
			$this->Save();
		}
	}
	
	function DeleteImage()
	{
		if( $this->IsLoaded() )
		{
			$attachment = new Attachment();
			$attachment->filename = $this->image;
			$attachment->rm();
			$attachment->filename = 'thumbnail-'. $attachment->filename;
			$attachment->rm();
			
			$this->image = '';
			$this->Save();
		}
	}

    function Json()
    {

        global $db;

        $pbSize = new StdClass();

        $pbSize->id = $this->id;
        $pbSize->name = $this->name;
        $pbSize->image = $this->image;
        $pbSize->price_base = $this->price_base;
        $pbSize->price_per_page = $this->price_per_page;
        $pbSize->price_personal_page = $this->price_personal_page;
        $pbSize->price_personal_cover = $this->price_personal_cover;
        $pbSize->price_design_work = $this->price_design_work;
        $pbSize->start_page = $this->start_page;
        $pbSize->stop_page = $this->stop_page;
        $pbSize->page_block = $this->page_block;
        $pbSize->discount1 = $this->discount1;
        $pbSize->discount2 = $this->discount2;

        //Papers
        $pbSize->papers = array();
        $pbPapersTable = new MysqlTable('pb2_papers');
        $pbPapersTable->find_all('pb_size_id='.$this->id.' and status=1', 'position,id');
        $pbPapers = $pbPapersTable->data;
        foreach($pbPapers as $pbPapersRow) {
            $pbPapersId = $pbPapersRow['id'];
            $pbPaper = new PB2Paper($pbPapersId);
            $pbSize->papers[$pbPapersId] = $pbPaper->Json();
        }

        //Lamination
        $pbSize->laminations = array();
        $pbLaminationTable = new MysqlTable('pb2_lamination');
        $pbLaminationTable->find_all('pb_size_id='.$this->id.' and status=1', 'position,id');
        $pbLaminations = $pbLaminationTable->data;
        foreach($pbLaminations as $pbLaminationRow) {
            $pbLaminationId = $pbLaminationRow['id'];
            $pbLamination = new PB2Lamination($pbLaminationId);
            $pbSize->laminations[$pbLaminationId] = $pbLamination->Json();
        }

        //Paper bases
        $pbSize->paper_bases = array();
        $pbPaperBaseTable = new MysqlTable('pb2_paper_base');
        $pbPaperBaseTable->find_all('pb_size_id='.$this->id.' and status=1', 'position,id');
        $pbPaperBases = $pbPaperBaseTable->data;
        foreach($pbPaperBases as $pbPaperBaseRow) {
            $pbPaperBaseId = $pbPaperBaseRow['id'];
            $pbPaperBase = new PB2PaperBase($pbPaperBaseId);
            $pbSize->paper_bases[$pbPaperBaseId] = $pbPaperBase->Json();
        }

        //Covers/cover types
        $pbSize->cover_types = array();
        $sql = "select * from pb2_categories where folder='cover' and status=1 and parent_id=".$this->id." order by position";
        $rs = $db->query( $sql );
        while( $row = $db->getrow($rs) ) {
            $category_id = $row[0];
            $category = new PB2Category($category_id);
            $pbSize->cover_types[$category_id]->id = $category_id;
            $pbSize->cover_types[$category_id]->name = $category->name;
            $pbSize->cover_types[$category_id]->description = $category->description;
            $pbSize->cover_types[$category_id]->image = $category->image;
            $pbSize->cover_types[$category_id]->covers = array();
            $innerSql = "select id from pb2_covers where status=1 and category_id={$category_id} and pb_size_id={$this->id} order by position,id";
            $innerRs = $db->query($innerSql);
            while ($innerRow = $db->getrow($innerRs)) {
                $cover_id = $innerRow[0];
                $cover = new PB2Cover($cover_id);
                $pbSize->cover_types[$category_id]->covers[$cover_id] = $cover->Json();
            }
        }


        return $pbSize;
    }


    function Xml()
	{
		$retval = "<pb_size>";
		$retval .= "<id>{$this->id}</id>";
		$retval .= "<type>{$this->type}</type>";
		$retval .= "<name><![CDATA[{$this->name}]]></name>";
		$retval .= "<image><![CDATA[{$this->image}]]></image>";
		$retval .= "<status>{$this->status}</status>";
		$retval .= "<position>{$this->position}</position>";
		$retval .= "<price_base>{$this->price_base}</price_base>";
		$retval .= "<price_per_page>{$this->price_per_page}</price_per_page>";
		$retval .= "<start_page>{$this->start_page}</start_page>";
		$retval .= "<stop_page>{$this->stop_page}</stop_page>";
		$retval .= "<page_block>{$this->page_block}</page_block>";
		$retval .= "<price_personal_page>{$this->price_personal_page}</price_personal_page>";
		$retval .= "<price_personal_cover>{$this->price_personal_cover}</price_personal_cover>";
		$retval .= "<price_design_work>{$this->price_design_work}</price_design_work>";
		$retval .= "<discount1>{$this->discount1}</discount1>";
		$retval .= "<discount2>{$this->discount2}</discount2>";
		$retval .= "<discount3>{$this->discount3}</discount3>";
		$retval .= "<page_min_width>{$this->page_min_width}</page_min_width>";
		$retval .= "<page_min_height>{$this->page_min_height}</page_min_height>";
		$retval .= "<cover_min_width>{$this->cover_min_width}</cover_min_width>";
		$retval .= "<cover_min_height>{$this->cover_min_height}</cover_min_height>";
		$retval .= "<created_at>{$this->created_at}</created_at>";
		$retval .= "<updated_at>{$this->updated_at}</updated_at>";
		$retval .= "</pb_size>";
		
		return( $retval );
	}
}

?>
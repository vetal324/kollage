<?php
/*****************************************************
 Class v.1.0, 2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

class FairOrder extends Model
{
    var $client_id, $order_number, $paymentway_id, $discount, $payed, $tax, $status;
    var $objects = Array();
    var $client, $paymentway;
    var $full_load;
    var $amount;
    
    var $tablename = 'fair_orders';

    function FairOrder( $id=0, $full_load=false )
    {
        $this->full_load = $full_load;
        $this->status = 0;
        $this->amount = 0;
        parent::Model( $id );
    }
    
    function LoadByNumber( $number )
    {
        $this->loaded = false;
        
        $t = new MysqlTable( $this->tablename );
        if( $t->find_first( "order_number='$number'" ) )
        {
            $this->_Load( $t->data[0] );
        }
    }
    
    function _Load( &$row )
    {
        $this->id = intval( $row['id'] );
        
        $this->client_id = intval( $row['client_id'] );
        $this->client = new Client( $this->client_id );
        
        $this->order_number = $row['order_number'];
        
        $this->paymentway_id = intval( $row['paymentway_id'] );
        $this->paymentway = new FairPaymentway( $this->paymentway_id );
        
        $this->discount = $row['discount'];
        $this->payed = $row['payed'];
        $this->tax = $row['tax'];
        
        $this->status = intval( $row['status'] );
        
        $md = new MysqlDateTime();
        $md->Parse( $row['created_at'] );
        $this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
        
        $this->updated_at = $row['updated_at'];
        
        $this->loaded = true;
        
        $this->LoadObjects();
    }
    
    function LoadObjects()
    {
        $this->amount = 0;
        $this->objects = Array();
        if( $this->IsLoaded() )
        {
            $t = new MysqlTable('fair_order_objects');
            $t->find_all( "order_id={$this->id}" );
            foreach( $t->data as $row )
            {
                $p = new FairOrderObject($row['id'],$this->full_load);
                array_push( $this->objects, $p );
                $this->amount += ($p->price * $p->quantity);
            }
            $this->amount += $this->delivery->price;
        }
    }
    
    function Save( $data=null )
    {   
        $this->CheckupData();
        
        $t = new MysqlTable( $this->tablename );
        if( is_array($data) ) $t->save( $data );
        else
        {
            $data = Array();
            $data['id'] = $this->id;
            $data['client_id'] = $this->client_id;
            $data['order_number'] = $this->order_number;
            $data['paymentway_id'] = $this->paymentway_id;
            $data['discount'] = $this->discount;
            $data['payed'] = $this->payed;
            $data['tax'] = $this->tax;
            $data['status'] = $this->status;
            $data['lang'] = $_SESSION['lang'];
            
            $t->save( $data );
        }
            
        $this->Load( $t->get_last_insert_id() );
        
        return( $this->id );
    }
    
    function Delete()
    {
        if( $this->IsLoaded() )
        {
            $this->status = 99;
            $this->Save();
        }
    }
    
    function Xml()
    {
        $retval = "<order>";
        $retval .= "<id>{$this->id}</id>";
        $retval .= "<client_id>{$this->client_id}</client_id>";
        $retval .= "<order_number><![CDATA[{$this->order_number}]]></order_number>";
        $retval .= "<paymentway_id>{$this->paymentway_id}</paymentway_id>";
        $retval .= "<discount>{$this->discount}</discount>";
        $retval .= "<amount>{$this->amount}</amount>";
        $retval .= "<payed>{$this->payed}</payed>";
        $retval .= "<tax>{$this->tax}</tax>";
        $retval .= "<status>{$this->status}</status>";
        $retval .= "<created_at>{$this->created_at}</created_at>";
        $retval .= "<updated_at>{$this->updated_at}</updated_at>";
        
        if($this->client) $retval .= $this->client->Xml();
        if($this->paymentway) $retval .= $this->paymentway->Xml();
        
        $retval .= "<objects>";
        foreach( $this->objects as $o )
        {
            $retval .= $o->Xml();
        }
        $retval .= "</objects>";
        
        $retval .= "</order>";
        
        return( $retval );
    }
}

?>
<?php

class ShopComparision extends Model
{
	var $client_key, $product_id;
	
	var $tablename = 'shop_comparision';
	
	static $cookie_key_name = 'uk';
	
	function __construct( $id=0 ) {
		parent::Model( $id );
	}
	
	function LoadByKey( $key ) {
		$this->loaded = false;
		
		$key = trim( $key );
		if( $key != '' )
		{
			$t = new MysqlTable( $this->tablename);
			if( $t->find_first( "client_key='$key'" ) )
			{
				$this->_Load( $t->data[0] );
			}
		}
	}
	
	function LoadByKeyAndProductId( $key, $product_id ) {
		$this->loaded = false;
		
		$key = trim( $key );
		if( $key != '' )
		{
			$t = new MysqlTable( $this->tablename);
			if( $t->find_first( "client_key='$key' AND product_id={$product_id}" ) )
			{
				$this->_Load( $t->data[0] );
			}
		}
	}
	
	function _Load( &$row )
	{
		$this->id = intval( $row['id'] );
		
		$this->client_key = trim( $row['client_key'] );
		$this->product_id = intval( $row['product_id'] );
		
		$md = new MysqlDateTime();
		$md->Parse( $row['created_at'] );
		$this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
		
		$md->Parse( $row['updated_at'] );
		$this->updated_at = $md->GetFrontEndValue('d.m.y.hh.mm');
		
		$this->loaded = true;
	}
	
	function Save( $data=null )
	{   
		$this->CheckupData();
		
		$t = new MysqlTable( $this->tablename );
		if( is_array($data) ) $t->save( $data );
		else
		{
			$data = Array();
			$data['id'] = $this->id;
			$data['client_key'] = $this->client_key;
			$data['product_id'] = $this->product_id;
			
			$data['lang'] = $_SESSION['lang'];
			
			$t->save( $data );
		}
			
		$this->Load( $t->get_last_insert_id() );
		
		return( $this->id );
	}
	
	function Json() {
		$r = new StdClass();
		
		$r->id = $this->id;
		$r->client_key = $this->client_key;
		$r->product_id = $this->product_id;
		$r->created_at = $this->created_at;
		$r->updated_at = $this->updated_at;
		
		return( json_encode($r) );
	}
	
	function Xml()
	{
		$retval = "<shop_comparision>";
		$retval .= "<id>{$this->id}</id>";
		$retval .= "<client_key><![CDATA[{$this->client_key}]]></client_key>";
		$retval .= "<product_id>{$this->product_id}</product_id>";
		$retval .= "<created_at>{$this->created_at}</created_at>";
		$retval .= "<updated_at>{$this->updated_at}</updated_at>";
		$retval .= "</shop_comparision>";
		
		return( $retval );
	}
}

?>
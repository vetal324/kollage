<?php
/*****************************************************
 Class v.1.0, 2011
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

class PBBox extends Model
{
	var $name, $description, $price, $pb_size_id, $category_id;
	var $image;
	
	var $tablename = 'pb_boxes';

	function __construct( $id=0 )
	{
		$this->status = 1;
		parent::Model( $id );
	}
	
	function _Load( &$row )
	{
		$this->id = intval( $row['id'] );
		
		$this->status = intval( $row['status'] );
		$this->position = intval( $row['position'] );
		
		$this->name = trim( $row['name'] );
		$this->description = trim( $row['description'] );
		$this->image = $row['image'];
		$this->price = floatval( $row['price'] );
		$this->pb_size_id = intval( $row['pb_size_id'] );
		$this->category_id = intval( $row['category_id'] );
		
		$md = new MysqlDateTime();
		$md->Parse( $row['created_at'] );
		$this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
		
		$this->updated_at = $row['updated_at'];
		
		$this->loaded = true;
	}
	
	function Save( $data=null )
	{   
		$this->CheckupData();
		
		$t = new MysqlTable( $this->tablename );
		if( is_array($data) ) $t->save( $data );
		else
		{
			$data = Array();
			$data['id'] = $this->id;
			$data['status'] = $this->status;
			$data['position'] = $this->position;
			$data['name'] = $this->name;
			$data['description'] = $this->description;
			$data['image'] = $this->image;
			$data['pb_size_id'] = $this->pb_size_id;
			$data['category_id'] = $this->category_id;
			$data['price'] = $this->price;
			$data['lang'] = $_SESSION['lang'];
			
			$t->save( $data );
		}
			
		$this->Load( $t->get_last_insert_id() );
		
		return( $this->id );
	}
	
	function Delete()
	{
		if( $this->IsLoaded() )
		{
			$this->status = 0;
			$this->Save();
		}
	}
	
	function DeleteImage()
	{
		if( $this->IsLoaded() )
		{
			$attachment = new Attachment();
			$attachment->filename = $this->image;
			$attachment->rm();
			$attachment->filename = 'thumbnail-'. $attachment->filename;
			$attachment->rm();
			
			$this->image = '';
			$this->Save();
		}
	}
	
	function Xml()
	{
		$retval = "<pb_box>";
		$retval .= "<id>{$this->id}</id>";
		$retval .= "<name><![CDATA[{$this->name}]]></name>";
		$retval .= "<description><![CDATA[{$this->description}]]></description>";
		$retval .= "<image><![CDATA[{$this->image}]]></image>";
		$retval .= "<pb_size_id>{$this->pb_size_id}</pb_size_id>";
		$retval .= "<category_id>{$this->category_id}</category_id>";
		$retval .= "<price>{$this->price}</price>";
		$retval .= "<status>{$this->status}</status>";
		$retval .= "<position>{$this->position}</position>";
		$retval .= "<created_at>{$this->created_at}</created_at>";
		$retval .= "<updated_at>{$this->updated_at}</updated_at>";
		$retval .= "</pb_box>";
		
		return( $retval );
	}
}

?>
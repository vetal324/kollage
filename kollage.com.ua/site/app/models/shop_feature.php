<?php
/*****************************************************
 Class v.1.0, 2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

class ShopFeature extends Model
{
	var $name, $description, $status, $category_id, $match_analog, $show_in_short_description, $show_in_filter, $is_numeric;
	var $values = Array();
	
	var $tablename = 'shop_features';
	
	var $full_load;

	function ShopFeature( $id=0, $full_load=true )
	{
		$this->full_load = $full_load;
		$this->status = 1;
		parent::Model( $id );
	}
	
	function _Load( &$row )
	{
		$this->id = intval( $row['id'] );
		$this->category_id = intval( $row['category_id'] );
		
		$this->show_in_short_description = intval( $row['show_in_short_description'] );
		$this->match_analog = intval( $row['match_analog'] );
		$this->show_in_filter = intval( $row['show_in_filter'] );
		$this->is_numeric = intval( $row['is_numeric'] );
		$this->status = intval( $row['status'] );
		$this->position = intval( $row['position'] );
		
		$t = new MysqlTable('dictionary');
		if( $t->find_all( "parent_id={$this->id} and folder='{$this->tablename}' and lang='". $_SESSION['lang'] ."'" ) )
		{
			foreach( $t->data as $row2 )
			{
				switch( $row2['name'] )
				{
					case 'name';
						$this->name = $row2['value'];
						break;
					case 'description';
						$this->description = $row2['value'];
						break;
				}
			}
		}
		
		$md = new MysqlDateTime();
		$md->Parse( $row['created_at'] );
		$this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
		
		$this->updated_at = $row['updated_at'];
		
		$this->loaded = true;
		
		if( $this->full_load ) {
			$this->LoadValues();
		}
	}
	
	function LoadValues()
	{
		if( $this->IsLoaded() )
		{
			$this->values = Array();
			$t = new MysqlTable('shop_feature_values');
			$t->find_all( "feature_id={$this->id} and status=1", "position,id" );
			foreach( $t->data as $row )
			{
				array_push( $this->values, new ShopFeatureValue($row['id']) );
			}
		}
	}
	
	function Save( $data=null )
	{   
		$this->CheckupData();
		
		$t = new MysqlTable( $this->tablename );
		if( is_array($data) ) $t->save( $data );
		else
		{
			$data = Array();
			$data['id'] = $this->id;
			$data['category_id'] = $this->category_id;
			$data['name'] = $this->name;
			$data['description'] = $this->description;
			$data['status'] = $this->status;
			$data['match_analog'] = $this->match_analog;
			$data['show_in_short_description'] = $this->show_in_short_description;
			$data['show_in_filter'] = $this->show_in_filter;
			$data['is_numeric'] = $this->is_numeric;
			$data['position'] = $this->position;
			$data['lang'] = $_SESSION['lang'];
			
			$t->save( $data );
		}
			
		$this->Load( $t->get_last_insert_id() );
		
		return( $this->id );
	}
	
	function Delete()
	{
		if( $this->IsLoaded() )
		{
			$this->status = 0;
			foreach( $this->values as $o )
			{
				$o->Delete();
			}
			$this->Save();
		}
	}
	
	function Xml()
	{
		$retval = "<feature>";
		$retval .= "<id>{$this->id}</id>";
		$retval .= "<category_id>{$this->category_id}</category_id>";
		$retval .= "<name><![CDATA[{$this->name}]]></name>";
		$retval .= "<description><![CDATA[{$this->description}]]></description>";
		$retval .= "<match_analog>{$this->match_analog}</match_analog>";
		$retval .= "<show_in_filter>{$this->show_in_filter}</show_in_filter>";
		$retval .= "<is_numeric>{$this->is_numeric}</is_numeric>";
		$retval .= "<show_in_short_description>{$this->show_in_short_description}</show_in_short_description>";
		$retval .= "<status>{$this->status}</status>";
		$retval .= "<position>{$this->position}</position>";
		$retval .= "<created_at>{$this->created_at}</created_at>";
		$retval .= "<updated_at>{$this->updated_at}</updated_at>";
		
		$retval .= "<values>";
		foreach( $this->values as $o )
		{
			$retval .= $o->Xml();
		}
		$retval .= "</values>";
		
		$retval .= "</feature>";
		
		return( $retval );
	}
}

?>
<?php

include_once( 'trash.php' );

class FairTrash extends Trash
{
    function Xml()
    {
        $xml = "";
        
        $xml .= "<fair_trash>";
        if( count( array_keys($this->items) ) > 0 )
        {
            foreach( $this->items as $k=>$v )
            {
                $p = new FairObject( $k, false );
                if( $p->IsLoaded() )
                {
                    $xml .= "<item>";
                    $xml .= $p->Xml();
                    $xml .= "</item>";
                }
            }
        }
        $xml .= "</fair_trash>";
        
        return( $xml );
    }
}

?>
<?php

class FairObject extends Model
{
    var $type, $category_id1, $category_id2, $client_id, $country_id, $views, $rating_view, $rating_buy, $license, $keywords, $title;
    var $description, $filename, $filename_w, $filename_h, $thumbnail, $thumbnail_w, $thumbnail_h, $preview, $preview_w, $preview_h, $xs, $xs_w, $xs_h;
    var $s, $s_w, $s_h, $m, $m_w, $m_h, $l, $l_w, $l_h, $xl, $xl_w, $xl_h, $xxl, $xxl_w, $xxl_h;
    var $license_price, $xs_price, $s_price, $m_price, $l_price, $xl_price, $xxl_price;
    var $status, $with_processing, $buyings;
    var $was_used_software, $software, $photo_make, $photo_model, $photo_resolution, $photo_iso, $photo_datetime_original, $photo_datetime_digitized;
    var $owner;
    var $sales;
    var $categories, $page_in_category;
    var $documents;
    
    var $tablename = 'fair_objects';
    
    var $dir;
    var $resolution_limit = 4000000; /* 4Mpixels*/

    function FairObject( $id=0 )
    {
        $this->dir = SITEROOT .'/'. $_ENV['fair_path'] .'/';
        
        $this->type = 1;
        $this->views = 0;
        $this->rating_view = 0;
        $this->rating_buy = 0;
        $this->license = 0;
        $this->status = 0;
        $this->sales = 0;
        $this->xs_price = 5;
        $this->s_price = 10;
        $this->m_price = 20;
        $this->l_price = 40;
        $this->xl_price = 50;
        $this->xxl_price = 60;
        $this->with_processing = 0;
        $this->buyings = 0;
        
        $this->documents = Array();
        
        $this->categories = Array();
        $this->page_in_category = 1;
        
        parent::Model( $id );
    }
    
    function _Load( &$row )
    {
        global $db;
        
        $this->id = intval( $row['id'] );
        $this->type = intval( $row['type'] );
        $this->category_id1 = intval( $row['category_id1'] );
        $this->category_id2 = intval( $row['category_id2'] );
        $this->client_id = intval( $row['client_id'] );
        $this->country_id = intval( $row['country_id'] );
        $this->views = intval( $row['views'] );
        $this->rating_view = intval( $row['rating_view'] );
        $this->rating_buy = intval( $row['rating_buy'] );
        $this->with_processing = intval( $row['with_processing'] );
        $this->buyings = intval( $row['buyings'] );
        $this->license = intval( $row['license'] );
        $this->license_price = floatval( $row['license_price'] );
        $this->keywords = $row['keywords'];
        $this->filename = $row['filename'];
        $this->filename_w = $row['filename_w'];
        $this->filename_h = $row['filename_h'];
        $this->thumbnail = $row['thumbnail'];
        $this->thumbnail_w = $row['thumbnail_w'];
        $this->thumbnail_h = $row['thumbnail_h'];
        $this->preview = $row['preview'];
        $this->preview_w = $row['preview_w'];
        $this->preview_h = $row['preview_h'];
        $this->xs = $row['xs'];
        $this->xs_w = $row['xs_w'];
        $this->xs_h = $row['xs_h'];
        $this->xs_price = floatval( $row['xs_price'] );
        $this->s = $row['s'];
        $this->s_w = $row['s_w'];
        $this->s_h = $row['s_h'];
        $this->s_price = floatval( $row['s_price'] );
        $this->m = $row['m'];
        $this->m_w = $row['m_w'];
        $this->m_h = $row['m_h'];
        $this->m_price = floatval( $row['m_price'] );
        $this->l = $row['l'];
        $this->l_w = $row['l_w'];
        $this->l_h = $row['l_h'];
        $this->l_price = floatval( $row['l_price'] );
        $this->xl = $row['xl'];
        $this->xl_w = $row['xl_w'];
        $this->xl_h = $row['xl_h'];
        $this->xl_price = floatval( $row['xl_price'] );
        $this->xxl = $row['xxl'];
        $this->xxl_w = $row['xxl_w'];
        $this->xxl_h = $row['xxl_h'];
        $this->xxl_price = floatval( $row['xxl_price'] );
        
        //Exif information
        $this->was_used_software = intval( $row['was_used_software'] );
        $this->software = $row['software'];
        $this->photo_make = $row['photo_make'];
        $this->photo_model = $row['photo_model'];
        $this->photo_resolution = $row['photo_resolution'];
        $this->photo_iso = $row['photo_iso'];
        $this->photo_datetime_original = $row['photo_datetime_original'];
        $this->photo_datetime_digitized = $row['photo_datetime_digitized'];
        
        $this->status = intval( $row['status'] );
        $this->position = intval( $row['position'] );
        
        $categories_ids = $db->getone( "select fair_objects_categories_line({$this->category_id1})" );
        $ids_a = split( ',', $categories_ids );
        $ids_a = array_reverse( $ids_a );
        foreach( $ids_a as $c_id )
        {
            array_push( $this->categories, new FairCategory($c_id) );
        }
        
        $t = new MysqlTable('dictionary');
        if( $t->find_all( "parent_id={$this->id} and folder='{$this->tablename}' and lang='". $_SESSION['lang'] ."'", "id", "", "name" ) )
        {
            foreach( $t->data as $row2 )
            {
                switch( $row2['name'] )
                {
                    case 'title';
                        $this->title = $row2['value'];
                        break;
                    case 'description';
                        $this->description = $row2['value'];
                        break;
                }
            }
        }
        
        $md = new MysqlDateTime();
        $md->Parse( $row['created_at'] );
        $this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
        
        $md->Parse( $row['photo_datetime_original'] );
        $this->photo_datetime_original = $md->GetFrontEndValue('d.m.y.hh.mm');
        
        $md->Parse( $row['photo_datetime_digitized'] );
        $this->photo_datetime_digitized = $md->GetFrontEndValue('d.m.y.hh.mm');
        
        $this->updated_at = $row['updated_at'];
        
        $this->owner = new Client( $this->client_id );
        
        $this->sales = intval( $db->getone("select count(id) from fair_order_objects fo left join fair_orders o on fo.order_id=o.id where o.status=4 and fo.fair_object_id={$this->id}") );
        
        $this->loaded = true;
        
        $this->LoadDocuments();
    }
    
    function LoadDocuments()
    {
        if( $this->IsLoaded() )
        {
            $t = new MysqlTable('fair_object_documents');
            if( $t->find_all( "fair_object_id={$this->id}", "position,id" ) )
            {
                unset( $this->documents ); $this->documents = Array();
                foreach( $t->data as $row )
                {
                    array_push( $this->documents, new FairDocument($row['fair_document_id']) );
                }
            }
        }
    }
    
    function Save( $data=null )
    {   
        $this->CheckupData();
        
        $t = new MysqlTable( $this->tablename );
        if( is_array($data) ) $t->save( $data );
        else
        {
            $data = Array();
            $data['id'] = $this->id;
            $data['type'] = $this->type;
            $data['category_id1'] = $this->category_id1;
            $data['category_id2'] = $this->category_id2;
            $data['client_id'] = $this->client_id;
            $data['country_id'] = $this->country_id;
            $data['views'] = $this->views;
            $data['rating_view'] = $this->rating_view;
            $data['rating_buy'] = $this->rating_buy;
            $data['with_processing'] = $this->with_processing;
            $data['buyings'] = $this->buyings;
            $data['license'] = $this->license;
            $data['keywords'] = $this->keywords;
            $data['title'] = $this->title;
            $data['description'] = $this->description;
            $data['filename'] = $this->filename;
            $data['filename_w'] = $this->filename_w;
            $data['filename_h'] = $this->filename_h;
            $data['thumbnail'] = $this->thumbnail;
            $data['thumbnail_w'] = $this->thumbnail_w;
            $data['thumbnail_h'] = $this->thumbnail_h;
            $data['preview'] = $this->preview;
            $data['preview_w'] = $this->preview_w;
            $data['preview_h'] = $this->preview_h;
            $data['xs'] = $this->xs;
            $data['xs_w'] = $this->xs_w;
            $data['xs_h'] = $this->xs_h;
            $data['xs_price'] = $this->xs_price;
            $data['s'] = $this->s;
            $data['s_w'] = $this->s_w;
            $data['s_h'] = $this->s_h;
            $data['s_price'] = $this->s_price;
            $data['m'] = $this->m;
            $data['m_w'] = $this->m_w;
            $data['m_h'] = $this->m_h;
            $data['m_price'] = $this->m_price;
            $data['l'] = $this->l;
            $data['l_w'] = $this->l_w;
            $data['l_h'] = $this->l_h;
            $data['l_price'] = $this->l_price;
            $data['xl'] = $this->xl;
            $data['xl_w'] = $this->xl_w;
            $data['xl_h'] = $this->xl_h;
            $data['xl_price'] = $this->xl_price;
            $data['xxl'] = $this->xxl;
            $data['xxl_w'] = $this->xxl_w;
            $data['xxl_h'] = $this->xxl_h;
            $data['xxl_price'] = $this->xxl_price;
            
            $data['was_used_software'] = $this->was_used_software;
            $data['software'] = $this->software;
            $data['photo_make'] = $this->photo_make;
            $data['photo_model'] = $this->photo_model;
            $data['photo_resolution'] = $this->photo_resolution;
            $data['photo_iso'] = $this->photo_iso;
            $data['photo_datetime_original'] = $this->photo_datetime_original;
            $data['photo_datetime_digitized'] = $this->photo_datetime_digitized;
            
            $data['status'] = $this->status;
            $data['position'] = $this->position;
            $data['lang'] = $_SESSION['lang'];
            
            $t->save( $data );
        }
            
        $this->Load( $t->get_last_insert_id() );
        
        return( $this->id );
    }
    
    function Delete()
    {
        if( $this->IsLoaded() )
        {
            #Remove images from the disk
            $attachment = new Attachment();
            $attachment->filename = $this->filename;
            $attachment->rm();
            
            $attachment->filename = $this->thumbnail;
            $attachment->rm();
            
            $attachment->filename = $this->preview;
            $attachment->rm();
            
            $attachment->filename = $this->xs;
            $attachment->rm();
            
            $attachment->filename = $this->s;
            $attachment->rm();
            
            $attachment->filename = $this->m;
            $attachment->rm();
            
            $attachment->filename = $this->l;
            $attachment->rm();
            
            $attachment->filename = $this->xl;
            $attachment->rm();
            
            $attachment->filename = $this->xxl;
            $attachment->rm();
            
            //Continue keep data in db and thumbnail in store
            $this->filename = '';
            $this->xs = '';
            $this->s = '';
            $this->m = '';
            $this->l = '';
            $this->xl = '';
            $this->xxl = '';
            $this->status = 99;
            $this->Save();
        }
    }
    
    function upload( $posted_file='file' )
    {
        $retval = 0;
        $xs = false; $s = false; $m = false; $l = false; $xl = false; $xxl = false;
        $xs_last = false; $s_last = false; $m_last = false; $l_last = false; $xl_last = false; $xxl_last = false;
        
        if( $this->IsLoaded() )
        {
            //delete old
        }
        
        $this->status = 0; //wait for allow
        
        $dir = $this->dir;
        $prefix = $_ENV['fair_path'];
        $attachment = new Attachment( $prefix );
        $filetype = new AFileType( $_FILES[$posted_file]['tmp_name'] );
        if( $filetype->GetType()==FILE_JPG && $attachment->upload($posted_file) )
        {
            $io = new AImage( $dir . $attachment->filename );
            $w = $io->GetWidth(); $h = $io->GetHeight();
            $this->filename = $attachment->filename;
            $this->filename_w = $w;
            $this->filename_h = $h;
            
            if( $w>=4000 || $h>=4000 ) { $xxl = true; $xxl_last = true; }
            if( $w>=3000 || $h>=3000 )
            {
                $xl = true;
                if( !$xxl_last ) $xl_last = true;
            }
            if( $w>=2000 || $h>=2000 )
            {
                $l = true;
                if( !$xl_last ) $l_last = true;
            }
            if( $w>=1000 || $h>=1000 )
            {
                $m = true;
                if( !$l_last && !$xl_last ) $m_last = true;
            }
            if( $w>=500 || $h>=500 )
            {
                $s = true;
                if( !$m_last && !$l_last && !$xl_last ) $s_last = true;
            }
            $xs = true;
            
            if( ($w*$h) >= $this->resolution_limit )
            {
                if( $w > $h ) { $width = 110; $height = 0; }
                else { $width = 0; $height = 110; }
                $nn = 'thumbnail-'. $this->filename;
                $nn = md5($nn) .'.'. GetFileExtension($nn);
                if( ($size = $io->CopyResize( $dir . $nn, $width, $height )) )
                {
                    $this->thumbnail = $nn;
                    $this->thumbnail_w = $size[0];
                    $this->thumbnail_h = $size[1];
                }
                
                if( $w > $h ) { $width = 400; $height = 0; }
                else { $width = 0; $height = 400; }
                $nn = 'preview-'. $this->filename;
                $nn = md5($nn) .'.'. GetFileExtension($nn);
                if( ($size = $io->CopyResize( $dir . $nn, $width, $height )) )
                {
                    $this->preview = $nn;
                    $this->preview_w = $size[0];
                    $this->preview_h = $size[1];
                    $pimg = new AImage( $dir . $this->preview );
                    $pimg->Watermark( "kollage", $dir . $this->preview );
                }
                
                if( $w > $h ) { $width = mt_rand(424,498); $height = 0; }
                else { $width = 0; $height = mt_rand(312,390); }
                $nn = 'xs-'. $this->filename;
                $nn = md5($nn) .'.'. GetFileExtension($nn);
                if( ($size = $io->CopyResize( $dir . $nn, $width, $height )) )
                {
                    $this->xs = $nn;
                    $this->xs_w = $size[0];
                    $this->xs_h = $size[1];
                }
                
                if( $s )
                {
                    if( $s_last )
                    {
                        $this->s = $this->filename;
                        $this->s_w = $w;
                        $this->s_h = $h;
                    }
                    else
                    {
                        if( $w > $h ) { $width = mt_rand(820,992); $height = 0; }
                        else { $width = 0; $height = mt_rand(808,992); }
                        $nn = 's-'. $this->filename;
                        $nn = md5($nn) .'.'. GetFileExtension($nn);
                        if( ($size = $io->CopyResize( $dir . $nn, $width, $height )) )
                        {
                            $this->s = $nn;
                            $this->s_w = $size[0];
                            $this->s_h = $size[1];
                        }
                    }
                }
                
                if( $m )
                {
                    if( $m_last )
                    {
                        $this->m = $this->filename;
                        $this->m_w = $w;
                        $this->m_h = $h;
                    }
                    else
                    {
                        if( $w > $h ) { $width = mt_rand(1680,1994); $height = 0; }
                        else { $width = 0; $height = mt_rand(1622,1892); }
                        $nn = 'm-'. $this->filename;
                        $nn = md5($nn) .'.'. GetFileExtension($nn);
                        if( ($size = $io->CopyResize( $dir . $nn, $width, $height )) )
                        {
                            $this->m = $nn;
                            $this->m_w = $size[0];
                            $this->m_h = $size[1];
                        }
                    }
                }
                
                if( $l )
                {
                    if( $l_last )
                    {
                        $this->l = $this->filename;
                        $this->l_w = $w;
                        $this->l_h = $h;
                    }
                    else
                    {
                        if( $w > $h ) { $width = mt_rand(2560,2982); $height = 0; }
                        else { $width = 0; $height = mt_rand(2560,2982); }
                        $nn = 'l-'. $this->filename;
                        $nn = md5($nn) .'.'. GetFileExtension($nn);
                        if( ($size = $io->CopyResize( $dir . $nn, $width, $height )) )
                        {
                            $this->l = $nn;
                            $this->l_w = $size[0];
                            $this->l_h = $size[1];
                        }
                    }
                }
                
                if( $xl )
                {
                    if( $xl_last )
                    {
                        $this->xl = $this->filename;
                        $this->xl_w = $w;
                        $this->xl_h = $h;
                    }
                    else
                    {
                        if( $w > $h ) { $width = mt_rand(3560,3982); $height = 0; }
                        else { $width = 0; $height = mt_rand(3560,3982); }
                        $nn = 'xl-'. $this->filename;
                        $nn = md5($nn) .'.'. GetFileExtension($nn);
                        if( ($size = $io->CopyResize( $dir . $nn, $width, $height )) )
                        {
                            $this->xl = $nn;
                            $this->xl_w = $size[0];
                            $this->xl_h = $size[1];
                        }
                    }
                }
                
                if( $xxl )
                {
                    $this->xxl = $this->filename;
                    $this->xxl_w = $w;
                    $this->xxl_h = $h;
                }
                
                $this->SetExifInformation();
                
                $this->Save();
                $retval = 1;
            }
            else
            {
                $retval = -1;
            }
        }
        
        return( $retval );
    }
    
    function SetExifInformation()
    {
        $path = SITEROOT .'/'. $_ENV['fair_path'] .'/';
        if( file_exists($path . $this->filename) )
        {
            $exif = new AExif();
            $exif->SetFilename( $path . $this->filename );
            if( $exif->error == "" )
            {
                $this->software = $exif->parsed["software"];
                $this->photo_make = $exif->parsed["make"];
                $this->photo_model = $exif->parsed["model"];
                $this->photo_resolution = $exif->parsed["resolution_x"];
                $this->photo_iso = $exif->parsed["iso"];
                $this->photo_datetime_original = $exif->parsed["datetime_original"];
                $this->photo_datetime_digitized = $exif->parsed["datetime_digitized"];
                if( $this->software != "" ) $this->was_used_software = 1;
                else $this->was_used_software = 0;
            }
        }
    }
    
    function IncreaseViews()
    {
        global $db;
        
        $sql = "update fair_objects set views=views+1 where id=". $this->id;
        $db->getone( $sql );
    }
    
    function Xml()
    {
        $retval = "<object>";
        $retval .= "<id>{$this->id}</id>";
        $retval .= "<type>{$this->type}</type>";
        $retval .= "<category_id1>{$this->category_id1}</category_id1>";
        $retval .= "<category_id2>{$this->category_id2}</category_id2>";
        $retval .= "<client_id><![CDATA[{$this->client_id}]]></client_id>";
        $retval .= "<country_id><![CDATA[{$this->country_id}]]></country_id>";
        $retval .= "<views><![CDATA[{$this->views}]]></views>";
        $retval .= "<sales><![CDATA[{$this->sales}]]></sales>";
        $retval .= "<rating_view><![CDATA[{$this->rating_view}]]></rating_view>";
        $retval .= "<rating_buy><![CDATA[{$this->rating_buy}]]></rating_buy>";
        $retval .= "<with_processing><![CDATA[{$this->with_processing}]]></with_processing>";
        $retval .= "<buyings><![CDATA[{$this->buyings}]]></buyings>";
        $retval .= "<license><![CDATA[{$this->license}]]></license>";
        $retval .= "<license_price>". sprintf( "%01.2f", floatval( $this->license_price ) ) ."</license_price>";
        $retval .= "<keywords><![CDATA[{$this->keywords}]]></keywords>";
        $retval .= "<title><![CDATA[{$this->title}]]></title>";
        $retval .= "<description><![CDATA[{$this->description}]]></description>";
        $retval .= "<filename>{$this->filename}</filename>";
        $retval .= "<filename_w>{$this->filename_w}</filename_w>";
        $retval .= "<filename_h>{$this->filename_h}</filename_h>";
        $retval .= "<thumbnail>{$this->thumbnail}</thumbnail>";
        $retval .= "<thumbnail_w>{$this->thumbnail_w}</thumbnail_w>";
        $retval .= "<thumbnail_h>{$this->thumbnail_h}</thumbnail_h>";
        $retval .= "<preview>{$this->preview}</preview>";
        $retval .= "<preview_w>{$this->preview_w}</preview_w>";
        $retval .= "<preview_h>{$this->preview_h}</preview_h>";
        $retval .= "<xs>{$this->xs}</xs>";
        $retval .= "<xs_w>{$this->xs_w}</xs_w>";
        $retval .= "<xs_h>{$this->xs_h}</xs_h>";
        $retval .= "<xs_price>". sprintf( "%01.2f", floatval( $this->xs_price ) ) ."</xs_price>";
        $retval .= "<s>{$this->s}</s>";
        $retval .= "<s_w>{$this->s_w}</s_w>";
        $retval .= "<s_h>{$this->s_h}</s_h>";
        $retval .= "<s_price>". sprintf( "%01.2f", floatval( $this->s_price ) ) ."</s_price>";
        $retval .= "<m>{$this->m}</m>";
        $retval .= "<m_w>{$this->m_w}</m_w>";
        $retval .= "<m_h>{$this->m_h}</m_h>";
        $retval .= "<m_price>". sprintf( "%01.2f", floatval( $this->m_price ) ) ."</m_price>";
        $retval .= "<l>{$this->l}</l>";
        $retval .= "<l_w>{$this->l_w}</l_w>";
        $retval .= "<l_h>{$this->l_h}</l_h>";
        $retval .= "<l_price>". sprintf( "%01.2f", floatval( $this->l_price ) ) ."</l_price>";
        $retval .= "<xl>{$this->xl}</xl>";
        $retval .= "<xl_w>{$this->xl_w}</xl_w>";
        $retval .= "<xl_h>{$this->xl_h}</xl_h>";
        $retval .= "<xl_price>". sprintf( "%01.2f", floatval( $this->xl_price ) ) ."</xl_price>";
        $retval .= "<xxl>{$this->xxl}</xxl>";
        $retval .= "<xxl_w>{$this->xxl_w}</xxl_w>";
        $retval .= "<xxl_h>{$this->xxl_h}</xxl_h>";
        $retval .= "<xxl_price>". sprintf( "%01.2f", floatval( $this->xxl_price ) ) ."</xxl_price>";
        $retval .= "<status>{$this->status}</status>";
        $retval .= "<position>{$this->position}</position>";
        $retval .= "<created_at>{$this->created_at}</created_at>";
        $retval .= "<updated_at>{$this->updated_at}</updated_at>";
        $retval .= "<page_in_category>{$this->page_in_category}</page_in_category>";
        
        $retval .= "<was_used_software>{$this->was_used_software}</was_used_software>";
        $retval .= "<software>{$this->software}</software>";
        $retval .= "<photo_make>{$this->photo_make}</photo_make>";
        $retval .= "<photo_model>{$this->photo_model}</photo_model>";
        $retval .= "<photo_resolution>{$this->photo_resolution}</photo_resolution>";
        $retval .= "<photo_iso>{$this->photo_iso}</photo_iso>";
        $retval .= "<photo_datetime_original>{$this->photo_datetime_original}</photo_datetime_original>";
        $retval .= "<photo_datetime_digitized>{$this->photo_datetime_digitized}</photo_datetime_digitized>";
        
        $retval .= $this->owner->Xml();
        
        $retval .= "<categories>";
        foreach( $this->categories as $c )
        {
            $retval .= $c->Xml();
        }
        $retval .= "</categories>";
        
        $retval .= "<documents>";
        foreach( $this->documents as $doc )
        {
            $retval .= $doc->Xml();
        }
        $retval .= "</documents>";
        
        $retval .= "</object>";
        
        return( $retval );
    }
}

?>
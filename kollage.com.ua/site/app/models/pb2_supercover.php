<?php
/*****************************************************
 Class v.1.0, 2011
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

class PB2SuperCover extends Model
{
    var $name, $description, $pb_cover_id, $price, $status;
		var $image;
    
    var $tablename = 'pb2_supercovers';

    function __construct( $id=0 )
    {
        $this->status = 1;
        parent::Model( $id );
    }
    
    function _Load( &$row )
    {
        $this->id = intval( $row['id'] );
        
        $this->status = intval( $row['status'] );
        $this->position = intval( $row['position'] );
        
        $this->name = trim( $row['name'] );
        $this->description = trim( $row['description'] );
        $this->image = $row['image'];
        
        $this->price = floatval( $row['price'] );
        $this->pb_cover_id = intval( $row['pb_cover_id'] );
        
        $md = new MysqlDateTime();
        $md->Parse( $row['created_at'] );
        $this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
        
        $this->updated_at = $row['updated_at'];
        
        $this->loaded = true;
    }
    
    function Save( $data=null )
    {   
        $this->CheckupData();
        
        $t = new MysqlTable( $this->tablename );
        if( is_array($data) ) $t->save( $data );
        else
        {
            $data = Array();
            $data['id'] = $this->id;
            $data['name'] = $this->name;
            $data['description'] = $this->description;
            $data['image'] = $this->image;
            $data['pb_cover_id'] = $this->pb_cover_id;
            $data['status'] = $this->status;
            $data['price'] = $this->price;
            $data['position'] = $this->position;
            $data['lang'] = $_SESSION['lang'];
            
            $t->save( $data );
        }
            
        $this->Load( $t->get_last_insert_id() );
        
        return( $this->id );
    }
    
    function Delete()
    {
        if( $this->IsLoaded() )
        {
            $this->status = 0;
            $this->Save();
        }
    }

    function Json()
    {
        $pbSuperCover = new StdClass();

        $pbSuperCover->id = $this->id;
        $pbSuperCover->name = $this->name;
        $pbSuperCover->description = $this->description;
        $pbSuperCover->image = $this->image;
        $pbSuperCover->price = $this->price;

        return $pbSuperCover;
    }


    function Xml()
    {
        $retval = "<pb_supercover>";
        $retval .= "<id>{$this->id}</id>";
        $retval .= "<name><![CDATA[{$this->name}]]></name>";
        $retval .= "<description><![CDATA[{$this->description}]]></description>";
        $retval .= "<image><![CDATA[{$this->image}]]></image>";
        $retval .= "<status>{$this->status}</status>";
        $retval .= "<position>{$this->position}</position>";
        $retval .= "<pb_cover_id>{$this->pb_cover_id}</pb_cover_id>";
        $retval .= "<price>{$this->price}</price>";
        $retval .= "<created_at>{$this->created_at}</created_at>";
        $retval .= "<updated_at>{$this->updated_at}</updated_at>";
        $retval .= "</pb_supercover>";
        
        return( $retval );
    }
}

?>
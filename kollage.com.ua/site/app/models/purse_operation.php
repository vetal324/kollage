<?php
/*****************************************************
 Class v.1.0, 2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

class PurseOperation extends Model
{
    var $client_id, $amount, $description;
    
    var $tablename = 'purse_operations';

    function PurseOperation( $id=0 )
    {
        parent::Model( $id );
    }
    
    function _Load( &$row )
    {
        $this->id = intval( $row['id'] );
        $this->client_id = intval( $row['client_id'] );
        $this->amount = sprintf( "%01.2f", floatval( $row['amount'] ) );
        $this->description = $row['description'];
        
        $md = new MysqlDateTime();
        $md->Parse( $row['created_at'] );
        $this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
        
        $this->updated_at = $row['updated_at'];
        
        $this->loaded = true;
    }
    
    function Save( $data=null )
    {   
        $result = false;
        $this->CheckupData();
        
        $t = new MysqlTable( $this->tablename );
        if( is_array($data) ) $result = $t->save( $data );
        else
        {
            $data = Array();
            $data['id'] = $this->id;
            $data['client_id'] = $this->client_id;
            $data['amount'] = $this->amount;
            $data['description'] = $this->description;
            
            $result = $t->save( $data );
        }
            
        $this->Load( $t->get_last_insert_id() );
        
        if( $result )
        {
            $c = new Client( $this->client_id );
            $c->RecalculatePurse();
        }
        
        return( $this->id );
    }
    
    function Xml()
    {
        $retval = "<purse_operation>";
        $retval .= "<id>{$this->id}</id>";
        $retval .= "<client_id>{$this->client_id}</client_id>";
        $retval .= "<description><![CDATA[{$this->description}]]></description>";
        $retval .= "<amount>{$this->amount}</amount>";
        $retval .= "<created_at>{$this->created_at}</created_at>";
        $retval .= "<updated_at>{$this->updated_at}</updated_at>";
        $retval .= "</purse_operation>";
        
        return( $retval );
    }
}

?>
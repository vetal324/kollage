<?php

class Trash
{
    var $items;
    
    function Trash()
    {
        $this->items = Array();
    }
    
    function add( $id, $quantity )
    {
        if( $this->items[$id] )
        {
            $this->items[$id] += $quantity;
        }
        else $this->items[$id] = $quantity;
    }
    
    function update( $id, $quantity )
    {
        $retval = false;
        
        if( $this->items[$id] )
        {
            $this->items[$id] = $quantity;
        }
        
        return( $retval );
    }
    
    function remove( $id )
    {
        $retval = false;
        
        if( $this->items[$id] )
        {
            $bak = Array();
            foreach( $this->items as $k=>$v)
            {
                $bak[$k] = $v;
            }
            $this->items = Array();
            foreach( $bak as $k=>$v )
            {
                if( $k!= $id )
                {
                    $this->items[$k] = $v;
                }
            }
        }
        
        return( $retval );
    }
    
    function Xml()
    {
    }
}

?>
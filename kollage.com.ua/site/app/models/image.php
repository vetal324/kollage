<?php
/*****************************************************
 Class v.1.0, 2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

class Image extends Model
{
    var $folder, $tag, $parent_id, $name, $author, $description, $video_flv, $video_mov, $publish_date;
    var $filename, $small, $tiny, $thumbnail, $type;
    var $width = 0, $height = 0;
    var $full_load;
    
    var $path = '/data/';
    
    var $tablename = 'images';

    function Image( $id=0, $full_load=true )
    {
        $this->full_load = $full_load;
        $this->position = 0;
        $this->Load($id);
    }
    
    function Load( $id )
    {
        $this->loaded = false;
        
        if( $id>0 )
        {
            $t = new MysqlTable( $this->tablename );
            if( $row = $t->find_first( "id=$id" ) )
            {
                $this->id = intval( $row['id'] );
                $this->folder = $row['folder'];
                $this->tag = $row['tag'];
                $this->parent_id = intval( $row['parent_id'] );
                $this->name = $row['name'];
                $this->author = $row['author'];
                $this->description = $row['description'];
                $this->video_flv = $row['video_flv'];
                $this->video_mov = $row['video_mov'];
                $this->filename = $row['filename'];
                $this->small = $row['small'];
                $this->tiny = $row['tiny'];
                $this->thumbnail = $row['thumbnail'];
                $this->type = $row['type'];
                $this->position = intval( $row['position'] );
                $this->created_at = $row['created_at'];
                $this->updated_at = $row['updated_at'];
                
                $this->loaded = true;
                if( $this->full_load ) $this->GetSize();
            }
        }
    }
    
    function GetSize()
    {
        if( $this->IsLoaded() )
        {
            $this->width = 0;
            $this->height = 0;
            
            $io = new AImage( SITEROOT .'/data/'. $this->filename );
            if( !$io->error )
            {
                $this->width = $io->GetWidth();
                $this->height = $io->GetHeight();
            }
        }
    }
    
    function Save( $data=null )
    {
        $this->CheckupData();
        
        $t = new MysqlTable( $this->tablename );
        if( is_array($data) ) $t->save( $data );
        else
        {
            $data = Array();
            if( $this->id ) $data['id'] = $this->id;
            if( $this->folder ) $data['folder'] = $this->folder;
            if( $this->tag ) $data['tag'] = $this->tag;
            if( $this->parent_id ) $data['parent_id'] = $this->parent_id;
            if( $this->name ) $data['name'] = $this->name;
            if( $this->author ) $data['author'] = $this->author;
            if( $this->description ) $data['description'] = $this->description;
            if( $this->video_flv ) $data['video_flv'] = $this->video_flv;
            if( $this->video_mov ) $data['video_mov'] = $this->video_mov;
            if( $this->filename ) $data['filename'] = $this->filename;
            if( $this->small ) $data['small'] = $this->small;
            if( $this->tiny ) $data['tiny'] = $this->tiny;
            if( $this->thumbnail ) $data['thumbnail'] = $this->thumbnail;
            if( $this->type ) $data['type'] = $this->type;
            if( $this->position ) $data['position'] = $this->position;
            
            $t->save( $data );
        }
        $this->Load( $t->get_last_insert_id() );
        
        return( $this->id );
    }
    
    function Delete()
    {
        if( $this->IsLoaded() )
        {
            $this->DeleteVideoFLV();
            $this->DeleteVideoMOV();
            $this->DeleteImage();
            $this->DeleteSmall();
            $this->DeleteTiny();
            $this->DeleteThumbnail();
            
            $t = new MysqlTable( $this->tablename );
            $t->del( $this->id );
        }
    }
    
    function DeleteImage()
    {
        if( $this->IsLoaded() )
        {
            $attachment = new Attachment();
            $attachment->filename = $this->filename;
            $attachment->rm();
            
            $this->filename = '';
            $t = new MysqlTable( $this->tablename );
            $data = Array( 'id'=>$this->id, 'filename'=>$this->filename );
            $t->save( $data );
            
            $this->Load( $t->get_last_insert_id() );
        }
    }
    
    function DeleteSmall()
    {
        if( $this->IsLoaded() )
        {
            $attachment = new Attachment();
            $attachment->filename = $this->small;
            $attachment->rm();
            
            $this->small = '';
            $t = new MysqlTable( $this->tablename );
            $data = Array( 'id'=>$this->id, 'small'=>$this->small );
            $t->save( $data );
            
            $this->Load( $t->get_last_insert_id() );
        }
    }
    
    function DeleteTiny()
    {
        if( $this->IsLoaded() )
        {
            $attachment = new Attachment();
            $attachment->filename = $this->tiny;
            $attachment->rm();
            
            $this->tiny = '';
            $t = new MysqlTable( $this->tablename );
            $data = Array( 'id'=>$this->id, 'tiny'=>$this->tiny );
            $t->save( $data );
            
            $this->Load( $t->get_last_insert_id() );
        }
    }
    
    function DeleteThumbnail()
    {
        if( $this->IsLoaded() )
        {
            $attachment = new Attachment();
            $attachment->filename = $this->thumbnail;
            $attachment->rm();
            
            $this->thumbnail = '';
            $t = new MysqlTable( $this->tablename );
            $data = Array( 'id'=>$this->id, 'thumbnail'=>$this->thumbnail );
            $t->save( $data );
            
            $this->Load( $t->get_last_insert_id() );
        }
    }
    
    function DeleteVideoFLV()
    {
        if( $this->IsLoaded() )
        {
            $attachment = new Attachment();
            $attachment->filename = $this->video_flv;
            $attachment->rm();
            
            $this->video_flv = '';
            $t = new MysqlTable( $this->tablename );
            $data = Array( 'id'=>$this->id, 'video_flv'=>$this->video_flv );
            $t->save( $data );
            
            $this->Load( $t->get_last_insert_id() );
        }
    }
    
    function DeleteVideoMOV()
    {
        if( $this->IsLoaded() )
        {
            $attachment = new Attachment();
            $attachment->filename = $this->video_mov;
            $attachment->rm();
            
            $this->video_mov = '';
            $t = new MysqlTable( $this->tablename );
            $data = Array( 'id'=>$this->id, 'video_mov'=>$this->video_mov );
            $t->save( $data );
            
            $this->Load( $t->get_last_insert_id() );
        }
    }
    
    function Xml()
    {
        $retval = '';
        
        $filename = ( $this->filename )? $this->path. $this->filename : "";
        $small = ( $this->small )? $this->path. $this->small : "";
        $tiny = ( $this->tiny )? $this->path. $this->tiny : "";
        $thumbnail = ( $this->thumbnail )? $this->path. $this->thumbnail : "";
        $video_flv = ( $this->video_flv )? $this->path. $this->video_flv : "";
        $video_mov = ( $this->video_mov )? $this->path. $this->video_mov : "";
        
        $retval .= "<image>";
        $retval .= "<id>{$this->id}</id>";
        $retval .= "<parent_id>{$this->parent_id}</parent_id>";
        $retval .= "<folder>{$this->folder}</folder>";
        $retval .= "<tag>{$this->tag}</tag>";
        $retval .= "<type>{$this->type}</type>";
        $retval .= "<position>{$this->position}</position>";
        $retval .= "<publish_date>{$this->publish_date}</publish_date>";
        $retval .= "<name><![CDATA[{$this->name}]]></name>";
        $retval .= "<author><![CDATA[{$this->author}]]></author>";
        $retval .= "<description><![CDATA[{$this->description}]]></description>";
        $retval .= "<filename>{$filename}</filename>";
        $retval .= "<small>{$small}</small>";
        $retval .= "<tiny>{$tiny}</tiny>";
        $retval .= "<thumbnail>{$thumbnail}</thumbnail>";
        $retval .= "<video_flv>{$video_flv}</video_flv>";
        $retval .= "<video_mov>{$video_mov}</video_mov>";
        $retval .= "</image>";
        
        return( $retval );
    }
}

?>
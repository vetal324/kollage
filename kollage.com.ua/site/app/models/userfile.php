<?php

class UserFile extends Model
{
    var $client_id, $name, $filename, $description, $status;
    var $client = null;
    
    var $folder_for_child = 'userfile';
    
    var $tablename = 'userfiles';
    
    var $dir = "";

    function UserFile( $id=0 )
    {
        $this->dir = SITEROOT .'/'. $_ENV['userfiles_path'] .'/';
        $this->client = new Client();
        
        parent::Model( $id );
    }
    
    function _Load( &$row )
    {
        $this->id = intval( $row['id'] );
        
        $md = new MysqlDateTime();
        
        $this->client_id = $row['client_id'];
        $this->name = $row['name'];
        $this->filename = $row['filename'];
        $this->description = $row['description'];
        
        $this->position = intval( $row['position'] );
        $this->status = intval( $row['status'] );
        
        $md->Parse( $row['created_at'] );
        $this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
        
        $this->updated_at = $row['updated_at'];
        
        $this->loaded = true;
    }
    
    function LoadClient()
    {
        if( $this->IsLoaded() )
        {
            $this->client = new Client( $this->client_id );
        }
    }
    
    function Save( $data=null )
    {   
        $this->CheckupData();
        
        $t = new MysqlTable( $this->tablename, true );
        if( is_array($data) ) $t->save( $data );
        else
        {
            $data = Array();
            $data['id'] = $this->id;
            $data['client_id'] = $this->client_id;
            $data['name'] = $this->name;
            $data['filename'] = $this->filename;
            $data['description'] = $this->description;
            $data['status'] = $this->status;
            $data['position'] = $this->position;
            
            $t->save( $data );

            global $log;
            $log->Write( "Model ShopOrder->Save. {$t->debug_messages}" );
        }
            
        $this->Load( $t->get_last_insert_id() );
        
        return( $this->id );
    }
    
    function Delete()
    {
        if( $this->IsLoaded() )
        {
            $file = $this->dir . $this->filename;
            unlink( $file );
            parent::Delete();
        }
    }
    
    function Xml()
    {
        $retval = "<userfile>";
        $retval .= "<id>{$this->id}</id>";
        $retval .= "<client_id>{$this->client_id}</client_id>";
        $retval .= "<name><![CDATA[{$this->name}]]></name>";
        $retval .= "<filename><![CDATA[{$this->filename}]]></filename>";
        $retval .= "<description><![CDATA[{$this->description}]]></description>";
        $retval .= "<status>{$this->status}</status>";
        $retval .= "<position>{$this->position}</position>";
        $retval .= "<created_at>{$this->created_at}</created_at>";
        $retval .= "<updated_at>{$this->updated_at}</updated_at>";
        
        $retval .= $this->client->Xml();
        
        $retval .= "</userfile>";
        
        return( $retval );
    }

}

?>
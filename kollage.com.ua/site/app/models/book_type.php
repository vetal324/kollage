<?php
/*****************************************************
Class v.1.0, 2017
Copyright Vitaliy Koshelenko vetal324@gmail.com
 ******************************************************/

/**
 * Class BookType
 *
 * Print item type (book, brochures, notebooks, etc.)
 */
class BookType extends Model {

    var $name, $description;
    var $min_page_count, $max_page_count, $min_print_count, $max_print_count;
    var $discount1, $discount2, $discount3, $discount4;
    var $threshold1, $threshold2, $threshold3, $threshold4;
    var $image;

    var $tablename = 'book_types';

    function __construct( $id=0 ) {
        $this->status = 1;
        parent::Model( $id );
    }


    function _Load( &$row ) {
        $this->id = intval( $row['id'] );

        $this->position = intval( $row['position'] );

        $this->name = trim( $row['name'] );
        $this->description = trim( $row['description'] );
        $this->image = $row['image'];

        $this->min_page_count = intval( $row['min_page_count'] );
        $this->max_page_count = intval( $row['max_page_count'] );
        $this->min_print_count = intval( $row['min_print_count'] );
        $this->max_print_count = intval( $row['max_print_count'] );

        $this->threshold1 = intval($row['threshold1']);
        $this->threshold2 = intval($row['threshold2']);
        $this->threshold3 = intval($row['threshold3']);
        $this->threshold4 = intval($row['threshold4']);

        $this->discount1 = intval( $row['discount1'] );
        $this->discount2 = intval( $row['discount2'] );
        $this->discount3 = intval( $row['discount3'] );
        $this->discount4 = intval( $row['discount4'] );

        $md = new MysqlDateTime();
        $md->Parse( $row['created_at'] );
        $this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
        $this->updated_at = $row['updated_at'];

        $this->loaded = true;
    }

    function Save( $data=null ) {
        $this->CheckupData();

        $t = new MysqlTable( $this->tablename );

        if(is_array($data)) {
            $t->save( $data );
        } else {
            $data = Array();
            $data['id'] = $this->id;
            $data['name'] = $this->name;
            $data['image'] = $this->image;
            $data['description'] = $this->description;

            $data['discount1'] = $this->discount1;
            $data['discount2'] = $this->discount2;
            $data['discount3'] = $this->discount3;
            $data['discount4'] = $this->discount4;

            $data['threshold1'] = $this->threshold1;
            $data['threshold2'] = $this->threshold2;
            $data['threshold3'] = $this->threshold3;
            $data['threshold4'] = $this->threshold4;

            $data['min_page_count'] = $this->min_page_count;
            $data['max_page_count'] = $this->max_page_count;
            $data['min_print_count'] = $this->min_print_count;
            $data['max_print_count'] = $this->max_print_count;

            $t->save( $data );
        }

        $this->Load( $t->get_last_insert_id() );

        return( $this->id );
    }

    function Json() {

        $bookType = new StdClass();

        $bookType->id = $this->id;
        $bookType->name = $this->name;
        
        $bookType->description = $this->description;
        
        $bookType->min_page_count = $this->min_page_count;
        $bookType->max_page_count = $this->max_page_count;
        $bookType->min_print_count = $this->min_print_count;
        $bookType->max_print_count = $this->max_print_count;
        
        $bookType->discount1 = $this->discount1;
        $bookType->discount2 = $this->discount2;
        $bookType->discount3 = $this->discount3;
        $bookType->discount4 = $this->discount4;
        $bookType->threshold1 = $this->threshold1;
        $bookType->threshold2 = $this->threshold2;
        $bookType->threshold3 = $this->threshold3;
        $bookType->threshold4 = $this->threshold4;

        $bookType->image = $this->image;

        $bookType->options = array();
        $bookType->formats = array();

        $bookOptionsTable = new MysqlTable('book_options');
        $bookOptionsTable->find_all('type_id='.$this->id, 'position,id');
        $bookOptions = $bookOptionsTable->data;
        foreach($bookOptions as $bookOptionRow) {
            $bookOptionId = $bookOptionRow['id'];
            $bookOption = new BookOption($bookOptionId);
            $bookType->options[$bookOptionId] = $bookOption->Json();
        }

        $bookFormatsTable = new MysqlTable('book_formats');
        $bookFormatsTable->find_all('type_id='.$this->id, 'position,id');
        $bookFormat = $bookFormatsTable->data;
        foreach($bookFormat as $bookFormatRow) {
            $bookFormatId = $bookFormatRow['id'];
            $bookFormat = new BookFormat($bookFormatId);
            $bookType->formats[$bookFormatId] = $bookFormat->Json();
        }


        return $bookType;
    }


    function Xml($full=true) {
        $retval = "<book_type>";

        $retval .= "<id>{$this->id}</id>";
        $retval .= "<name>{$this->name}</name>";
        $retval .= "<image>{$this->image}</image>";
        $retval .= "<description><![CDATA[{$this->description}]]></description>";
        $retval .= "<min_page_count>{$this->min_page_count}</min_page_count>";
        $retval .= "<max_page_count>{$this->max_page_count}</max_page_count>";
        $retval .= "<min_print_count>{$this->min_print_count}</min_print_count>";
        $retval .= "<max_print_count>{$this->max_print_count}</max_print_count>";

        $retval .= "<discount1>{$this->discount1}</discount1>";
        $retval .= "<discount2>{$this->discount2}</discount2>";
        $retval .= "<discount3>{$this->discount3}</discount3>";
        $retval .= "<discount4>{$this->discount4}</discount4>";

        $retval .= "<threshold1>{$this->threshold1}</threshold1>";
        $retval .= "<threshold2>{$this->threshold2}</threshold2>";
        $retval .= "<threshold3>{$this->threshold3}</threshold3>";
        $retval .= "<threshold4>{$this->threshold4}</threshold4>";

        $retval .= "<book_options>";
        if ($full) {
            $bookOptionsTable = new MysqlTable('book_options');
            $bookOptionsTable->find_all('type_id='.$this->id, 'position,id');
            $bookOptions = $bookOptionsTable->data;
            foreach($bookOptions as $bookOptionRow) {
                $bookOption = new BookOption($bookOptionRow['id']);
                $bookOptionXml = $bookOption->Xml();
                $retval .= $bookOptionXml;
            }
        }
        $retval .= "</book_options>";

        $retval .= "<book_formats>";
        if ($full) {
            $bookFormatsTable = new MysqlTable('book_formats');
            $bookFormatsTable->find_all('type_id='.$this->id, 'position,id');
            $bookFormat = $bookFormatsTable->data;
            foreach($bookFormat as $bookFormatRow) {
                $bookFormat = new BookFormat($bookFormatRow['id']);
                $bookFormatXml = $bookFormat->Xml();
                $retval .= $bookFormatXml;
            }
        }
        $retval .= "</book_formats>";

        $retval .= "</book_type>";

        return( $retval );
    }

}

?>
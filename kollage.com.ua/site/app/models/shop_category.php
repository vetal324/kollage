<?php
/*****************************************************
 Class v.1.0, 2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

class ShopCategory extends Model
{
	var $name, $description, $parent_id, $folder, $link_to_category_id, $show_in_menu;
	var $page_for_product = 1;
	var $page_title, $page_keywords;
	
	var $tablename = 'shop_product_categories';

	function ShopCategory( $id=0 )
	{
		parent::Model( $id );
	}
	
	function _Load( &$row )
	{
		$this->id = intval( $row['id'] );
		$this->parent_id = intval( $row['parent_id'] );
		$this->link_to_category_id = intval( $row['link_to_category_id'] );
		$this->show_in_menu = intval( $row['show_in_menu'] );
		$this->folder = $row['folder'];
        
		$this->page_title = $row['page_title'];
		$this->page_keywords = $row['page_keywords'];
		
		$this->position = intval( $row['position'] );
		
		$t = new MysqlTable('dictionary');
		if( $t->find_all( "parent_id={$this->id} and folder='{$this->tablename}' and lang='". $_SESSION['lang'] ."'" ) )
		{
			foreach( $t->data as $row2 )
			{
				switch( $row2['name'] )
				{
					case 'name';
						$this->name = $row2['value'];
						break;
					case 'description';
						$this->description = $row2['value'];
						break;
				}
			}
		}
		
		$md = new MysqlDateTime();
		$md->Parse( $row['created_at'] );
		$this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
		
		$this->updated_at = $row['updated_at'];
		
		$this->loaded = true;
	}
	
	function Save( $data=null )
	{   
		$this->CheckupData();
		
		$t = new MysqlTable( $this->tablename );
		if( is_array($data) ) $t->save( $data );
		else
		{
			$data = Array();
			$data['id'] = $this->id;
			$data['parent_id'] = $this->parent_id;
			$data['folder'] = $this->folder;
			$data['link_to_category_id'] = $this->link_to_category_id;
			$data['show_in_menu'] = $this->show_in_menu;
			$data['name'] = $this->name;
			$data['description'] = $this->description;
			$data['page_title'] = $this->page_title;
			$data['page_keywords'] = $this->page_keywords;
			$data['position'] = $this->position;
			$data['lang'] = $_SESSION['lang'];
			
			$t->save( $data );
		}
			
		$this->Load( $t->get_last_insert_id() );
		
		return( $this->id );
	}
	
	function Delete()
	{
		if( $this->IsLoaded() )
		{
			$t = new MysqlTable( $this->tablename );
			$t->del( $this->id );
			$ta = new MysqlTable( 'shop_products' );
			$ta->find_all( "parent_id={$this->id}" );
			foreach( $ta->data as $row )
			{
				$o = new ShopProduct( $row['id'] );
				$o->Delete();
			}
		}
	}
	
	function Xml()
	{
		$retval = "<category>";
		$retval .= "<id>{$this->id}</id>";
		$retval .= "<parent_id>{$this->parent_id}</parent_id>";
		$retval .= "<link_to_category_id>{$this->link_to_category_id}</link_to_category_id>";
		$retval .= "<show_in_menu>{$this->show_in_menu}</show_in_menu>";
		$retval .= "<folder><![CDATA[{$this->folder}]]></folder>";
		$retval .= "<name><![CDATA[{$this->name}]]></name>";
		$retval .= "<description><![CDATA[{$this->description}]]></description>";
		$retval .= "<page_for_product>{$this->page_for_product}</page_for_product>";
		$retval .= "<page_title>{$this->page_title}</page_title>";
		$retval .= "<page_keywords>{$this->page_keywords}</page_keywords>";
		$retval .= "<position>{$this->position}</position>";
		$retval .= "<created_at>{$this->created_at}</created_at>";
		$retval .= "<updated_at>{$this->updated_at}</updated_at>";
		$retval .= "</category>";
		
		return( $retval );
	}
}

?>
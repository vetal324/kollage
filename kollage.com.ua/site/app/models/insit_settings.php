<?php
/*****************************************************
 Class v.1.0, 2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

class InsitSettings extends Model
{
	var $order_mail_subject, $order_mail_body, $admin_emails, $admin_order_mail_subject, $admin_order_mail_body;
	var $order_mail_printed_subject, $order_mail_printed_body;
	var $admin_order_mail_ready_subject, $admin_order_mail_ready_body;
	var $dont_use_discount;
	
	var $tablename = 'insit_settings';

	function __construct( $id=1 )
	{
		parent::Model( $id );
	}
	
	function _Load( &$row )
	{
		$this->id = intval( $row['id'] );
		$this->admin_emails = $row['admin_emails'];
		$this->dont_use_discount = $row['dont_use_discount'];
		
		$t = new MysqlTable('dictionary');
		if( $t->find_all( "parent_id={$this->id} and folder='{$this->tablename}' and lang='". $_SESSION['lang'] ."'" ) )
		{
			foreach( $t->data as $row2 )
			{
				switch( $row2['name'] )
				{
					case 'order_mail_subject';
						$this->order_mail_subject = $row2['value'];
						break;
					case 'order_mail_body';
						$this->order_mail_body = $row2['value'];
						break;
					case 'admin_order_mail_subject';
						$this->admin_order_mail_subject = $row2['value'];
						break;
					case 'admin_order_mail_body';
						$this->admin_order_mail_body = $row2['value'];
						break;
					case 'order_mail_printed_subject';
						$this->order_mail_printed_subject = $row2['value'];
						break;
					case 'order_mail_printed_body';
						$this->order_mail_printed_body = $row2['value'];
						break;
					case 'admin_order_mail_ready_subject';
						$this->admin_order_mail_ready_subject = $row2['value'];
						break;
					case 'admin_order_mail_ready_body';
						$this->admin_order_mail_ready_body = $row2['value'];
						break;
				}
			}
		}
		
		$this->loaded = true;
	}
	
	function Save( $data=null )
	{   
		$this->CheckupData();
		
		$t = new MysqlTable( $this->tablename );
		if( is_array($data) ) $t->save( $data );
		else
		{
			$data = Array();
			$data['id'] = $this->id;
			$data['order_mail_subject'] = $this->order_mail_subject;
			$data['order_mail_body'] = $this->order_mail_body;
			$data['admin_emails'] = $this->admin_emails;
			$data['admin_order_mail_subject'] = $this->admin_order_mail_subject;
			$data['admin_order_mail_body'] = $this->admin_order_mail_body;
			$data['order_mail_printed_subject'] = $this->order_mail_printed_subject;
			$data['order_mail_printed_body'] = $this->order_mail_printed_body;
			$data['admin_order_mail_ready_subject'] = $this->admin_order_mail_ready_subject;
			$data['admin_order_mail_ready_body'] = $this->admin_order_mail_ready_body;
			$data['dont_use_discount'] = $this->dont_use_discount;
			
			$t->save( $data );
		}

		$this->Load( $t->get_last_insert_id() );
		
		return( $this->id );
	}
	
	function Xml()
	{
		$retval = "<settings>";
		$retval .= "<id>{$this->id}</id>";
		$retval .= "<dont_use_discount>{$this->dont_use_discount}</dont_use_discount>";
		$retval .= "<order_mail_subject><![CDATA[{$this->order_mail_subject}]]></order_mail_subject>";
		$retval .= "<order_mail_body><![CDATA[{$this->order_mail_body}]]></order_mail_body>";
		$retval .= "<admin_emails><![CDATA[{$this->admin_emails}]]></admin_emails>";
		$retval .= "<admin_order_mail_subject><![CDATA[{$this->admin_order_mail_subject}]]></admin_order_mail_subject>";
		$retval .= "<admin_order_mail_body><![CDATA[{$this->admin_order_mail_body}]]></admin_order_mail_body>";
		$retval .= "<order_mail_printed_subject><![CDATA[{$this->order_mail_printed_subject}]]></order_mail_printed_subject>";
		$retval .= "<order_mail_printed_body><![CDATA[{$this->order_mail_printed_body}]]></order_mail_printed_body>";
		$retval .= "<admin_order_mail_ready_subject><![CDATA[{$this->admin_order_mail_ready_subject}]]></admin_order_mail_ready_subject>";
		$retval .= "<admin_order_mail_ready_body><![CDATA[{$this->admin_order_mail_ready_body}]]></admin_order_mail_ready_body>";
		$retval .= "</settings>";
		
		return( $retval );
	}
}

?>
<?php

class NewsArticle extends Model
{
	var $folder;
	var $header, $text, $publish_date, $status;
	var $images;
	var $page_title, $page_keywords;
	var $full_load;
	
	var $folder_for_child = 'news_article';
	
	var $tablename =  "news_articles";

	function NewsArticle( $id, $full_load=true )
	{
		$this->full_load = $full_load;
		$this->position = 0;
		$this->images = Array();
		$this->status = 1;
		$this->publish_date = date( "d.m.Y" );
		$this->Load($id);
	}
	
	function Load( $id )
	{
		$this->LoadByID( $id );
	}
	
	function LoadByID( $id )
	{
		$this->loaded = false;
		
		if( $id>0 )
		{
			$t = new MysqlTable( $this->tablename);
			if( $t->find_first( "id=$id" ) )
			{
				$this->_Load( $t->data[0] );
			}
		}
	}
	
	function _Load( &$row )
	{
		$this->id = intval( $row['id'] );
		$this->folder = $row['folder'];
		
		$md = new MysqlDateTime();
		
		$md->Parse($row['publish_date']);
		$this->publish_date = $md->GetValue( 'd.m.y' );
		
		$this->status = intval( $row['status'] );
		
		$t = new MysqlTable('dictionary');
		if( $t->find_all( "parent_id={$this->id} and folder='{$this->tablename}' and lang='". $_SESSION['lang'] ."'" ) )
		{
			foreach( $t->data as $row2 )
			{
				switch( $row2['name'] )
				{
					case 'header';
						$this->header = $row2['value'];
						break;
					case 'text';
						$this->text = $row2['value'];
						break;
				}
			}
		}
		
		$this->position = intval( $row['position'] );
        
		$this->page_title = $row['page_title'];
		$this->page_keywords = $row['page_keywords'];
		
		$md->Parse( $row['created_at'] );
		$this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
		
		$this->updated_at = $row['updated_at'];
		
		$this->loaded = true;
		
		#Load gallery
		$this->LoadImages();
	}
	
	function LoadImages()
	{
		if( $this->IsLoaded() )
		{
			$t = new MysqlTable('images');
			if( $t->find_all( "folder='{$this->folder_for_child}' and parent_id={$this->id}", "position,id" ) )
			{
				unset( $this->images ); $this->images = Array();
				foreach( $t->data as $row )
				{
					array_push( $this->images, new Image($row['id'],$this->full_load) );
				}
			}
		}
	}
	
	function Save( $data=null )
	{   
		$this->CheckupData();
		
		$t = new MysqlTable( $this->tablename );
		if( is_array($data) ) $t->save( $data );
		else
		{
			$data = Array();
			$data['id'] = $this->id;
			$data['folder'] = $this->folder;
			$data['position'] = $this->position;
			$data['header'] = $this->header;
			$data['text'] = $this->text;
			$data['publish_date'] = $this->publish_date;
			$data['page_title'] = $this->page_title;
			$data['page_keywords'] = $this->page_keywords;
			$data['status'] = $this->status;
			$data['lang'] = $_SESSION['lang'];
			
			$t->save( $data );
		}
			
		$this->Load( $t->get_last_insert_id() );
		
		return( $this->id );
	}
	
	function Delete()
	{
		if( $this->IsLoaded() )
		{
			$t = new MysqlTable( $this->tablename );
			$t->del( $this->id );
			
			#Remove images stored into gallery from the disk
			foreach( $this->images as $image )
			{
				$image->Delete();
			}
		}
	}
	
	function Xml( $transform=false )
	{
		$retval = "<article>";
		$retval .= "<id>{$this->id}</id>";
		$retval .= "<folder><![CDATA[{$this->folder}]]></folder>";
		$retval .= "<folder_for_child><![CDATA[{$this->folder_for_child}]]></folder_for_child>";
		$retval .= "<header><![CDATA[{$this->header}]]></header>";
		$retval .= "<text><![CDATA[{$this->text}]]></text>";
		$retval .= "<publish_date>{$this->publish_date}</publish_date>";
		$retval .= "<status>{$this->status}</status>";
		$retval .= "<position>{$this->position}</position>";
		$retval .= "<page_title><![CDATA[{$this->page_title}]]></page_title>";
		$retval .= "<page_keywords><![CDATA[{$this->page_keywords}]]></page_keywords>";
		$retval .= "<created_at>{$this->created_at}</created_at>";
		$retval .= "<updated_at>{$this->updated_at}</updated_at>";
		
		$retval .= "<images>";
		foreach( $this->images as $img )
		{
			$retval .= $img->Xml();
		}
		$retval .= "</images>";
		
		$retval .= "</article>";
		
		return( $retval );
	}
}

?>
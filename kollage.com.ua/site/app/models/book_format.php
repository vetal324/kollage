<?php
/*****************************************************
Class v.1.0, 2017
Copyright Vitaliy Koshelenko vetal324@gmail.com
 ******************************************************/

/**
 * Class BookFormat
 *
 * Print item format (A3/A4, etc.)
 */
class BookFormat extends Model {

    var $name, $description, $width, $height;
    var $type_id;
    var $image;
    var $page_count_step;

    var $tablename = 'book_formats';

    function __construct( $id=0 ) {
        $this->status = 1;
        parent::Model( $id );
    }

    function _Load( &$row ) {
        $this->id = intval( $row['id'] );
        $this->type_id = intval( $row['type_id'] );

        $this->position = intval( $row['position'] );

        $this->name = trim( $row['name'] );
        $this->description = trim( $row['description'] );
        $this->image = $row['image'];

        $this->width = intval( $row['width'] );
        $this->height = intval( $row['height'] );

        $this->page_count_step = intval( $row['page_count_step'] );

        $md = new MysqlDateTime();
        $md->Parse( $row['created_at'] );
        $this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
        $this->updated_at = $row['updated_at'];

        $this->loaded = true;
    }


    function Save( $data=null ) {
        $this->CheckupData();

        $t = new MysqlTable( $this->tablename );
        if(is_array($data)) {
            $t->save( $data );
        } else {
            $data = Array();
            $data['id'] = $this->id;
            $data['type_id'] = $this->type_id;
            $data['name'] = $this->name;
            $data['image'] = $this->image;
            $data['description'] = $this->description;
            $data['width'] = $this->width;
            $data['height'] = $this->height;
            $data['page_count_step'] = $this->page_count_step;

            $t->save( $data );
        }

        $this->Load( $t->get_last_insert_id() );

        return( $this->id );
    }

    function Json() {

        $bookFormat = new StdClass();

        $bookFormat->id     = $this->id;

        $bookFormat->name   = $this->name;
        $bookFormat->description   = $this->description;

        $bookFormat->width   = $this->width;
        $bookFormat->height   = $this->height;

        $bookFormat->page_count_step   = $this->page_count_step;

        $bookFormat->cover_types = array();
        $bookFormat->paper_types = array();

        $paperTypesTable = new MysqlTable('book_paper_types');
        $paperTypesTable->find_all('format_id='.$this->id, 'position,id');
        $paperTypes = $paperTypesTable->data;
        foreach ($paperTypes as $paperTypeRow) {
            $paperTypeId = $paperTypeRow['id'];
            $paperType = new BookPaperType($paperTypeId);
            $bookFormat->paper_types[$paperTypeId] = $paperType->Json();
        }

        $coverTypesTable = new MysqlTable('book_cover_types');
        $coverTypesTable->find_all('format_id='.$this->id, 'position,id');
        $coverTypes = $coverTypesTable->data;
        foreach ($coverTypes as $coverTypeRow) {
            $coverTypeId = $coverTypeRow['id'];
            $coverType = new BookCoverType($coverTypeId);
            $bookFormat->cover_types[$coverTypeId] = $coverType->Json();
        }

        $bookFormat->image =  $this->image;

        return $bookFormat;
    }


    function Xml($full=true) {
        $retval = "<book_format>";

        $retval .= "<id>{$this->id}</id>";
        $retval .= "<type_id>{$this->type_id}</type_id>";
        $retval .= "<name>{$this->name}</name>";
        $retval .= "<image>{$this->image}</image>";
        $retval .= "<description><![CDATA[{$this->description}]]></description>";
        $retval .= "<width>{$this->width}</width>";
        $retval .= "<height>{$this->height}</height>";
        $retval .= "<page_count_step>{$this->page_count_step}</page_count_step>";


        $retval .= "<cover_types>";
        if ($full) {
            $coverTypeTable = new MysqlTable('book_cover_types');
            $coverTypeTable->find_all('format_id='.$this->id, 'position,id');
            $coverTypes = $coverTypeTable->data;
            foreach ($coverTypes as $coverTypesRow) {
                $coverType = new BookCoverType($coverTypesRow['id']);
                $coverTypeXml = $coverType->Xml();
                $retval .= $coverTypeXml;
            }
        }
        $retval .= "</cover_types>";

        $retval .= "<paper_types>";
        if ($full) {
            $paperTypeTable = new MysqlTable('book_paper_types');
            $paperTypeTable->find_all('format_id='.$this->id, 'position,id');
            $paperTypes = $paperTypeTable->data;
            foreach ($paperTypes as $paperTypesRow) {
                $paperType = new BookPaperType($paperTypesRow['id']);
                $paperTypeXml = $paperType->Xml();
                $retval .= $paperTypeXml;
            }
        }
        $retval .= "</paper_types>";

        $retval .= "</book_format>";

        return( $retval );
    }
}

?>

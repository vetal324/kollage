<?php
/*****************************************************
 Class v.1.0, 2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

class ShopAvailability extends Model
{
    var $product_id, $point_id, $quantity;
    
    var $tablename = 'shop_product_availability';

    function ShopAvailability( $id=0 )
    {
        $this->quantity = 0;
        parent::Model( $id );
    }
    
    function _Load( &$row )
    {
        $this->id = intval( $row['id'] );
        
        $this->product_id = intval( $row['product_id'] );
        $this->point_id = intval( $row['point_id'] );
        $this->quantity = intval( $row['quantity'] );
        
        $this->position = intval( $row['position'] );
        
        $md = new MysqlDateTime();
        $md->Parse( $row['created_at'] );
        $this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
        
        $this->updated_at = $row['updated_at'];
        
        $this->loaded = true;
    }
    
    function Save( $data=null )
    {   
        $this->CheckupData();
        
        $t = new MysqlTable( $this->tablename );
        if( is_array($data) ) $t->save( $data );
        else
        {
            $data = Array();
            $data['id'] = $this->id;
            $data['product_id'] = $this->product_id;
            $data['point_id'] = $this->point_id;
            $data['quantity'] = $this->quantity;
            $data['position'] = $this->position;
            
            $t->save( $data );
        }
            
        $this->Load( $t->get_last_insert_id() );
        
        return( $this->id );
    }
    
    function Xml()
    {
        $retval = "<availability>";
        $retval .= "<id>{$this->id}</id>";
        $retval .= "<product_id>{$this->product_id}</product_id>";
        $retval .= "<point_id>{$this->point_id}</point_id>";
        $retval .= "<quantity>{$this->quantity}</quantity>";
        $retval .= "<position>{$this->position}</position>";
        $retval .= "<created_at>{$this->created_at}</created_at>";
        $retval .= "<updated_at>{$this->updated_at}</updated_at>";
        $retval .= "</availability>";
        
        return( $retval );
    }
}

?>
<?php

class Insit_Order extends Model
{
	var $order_number, $customer_id, $first_name, $last_name, $product_details, $street, $city, $zip, $phone, $email, $comment, $page_count, $copies, $total_price, $status;
	
	var $tablename = 'insit_orders';
	
	// mapping <file field name>, <type>, <model property name>
	//1-int, 2-string, 3-float
	var $file_fields = Array(
		Array( 'CUSTOMER_ID', 1, 'customer_id' ),
		Array( 'FIRST_NAME', 2, 'first_name' ),
		Array( 'LAST_NAME', 2, 'last_name' ),
		Array( 'PRODUCT_DETAILS', 2, 'product_details' ),
		Array( 'STREET', 2, 'street' ),
		Array( 'CITY', 2, 'city' ),
		Array( 'ZIP', 2, 'zip' ),
		Array( 'PHONE', 2, 'phone' ),
		Array( 'EMAIL', 2, 'email' ),
		Array( 'COMMENT', 2, 'comment' ),
		Array( 'PAGE_COUNT', 1, 'page_count' ),
		Array( 'COPIES', 1, 'copies' ),
		Array( 'TOTAL_PRICE', 3, 'total_price' )
	);

	function Insit_Order( $id=0 )
	{
		parent::Model( $id );
	}
	
	function LoadByNumber( $order_number ) {
		$this->loaded = false;
		
		if( $order_number )
		{
			$t = new MysqlTable( $this->tablename );
			if( $t->find_first( "order_number='{$order_number}'" ) )
			{
				$this->_Load( $t->data[0] );
			}
		}
	}
	
	function _Load( &$row )
	{
		$this->id = intval( $row['id'] );
		
		$this->order_number = $row['order_number'];
		$this->customer_id = intval( $row['customer_id'] );
		$this->first_name = $row['first_name'];
		$this->last_name = $row['last_name'];
		$this->product_details = $row['product_details'];
		$this->street = $row['street'];
		$this->city = $row['city'];
		$this->zip = $row['zip'];
		$this->phone = $row['phone'];
		$this->email = $row['email'];
		$this->comment = $row['comment'];
		$this->page_count = intval( $row['page_count'] );
		$this->copies = intval( $row['copies'] );
		$this->total_price = floatval( $row['total_price'] );
		
		$this->status= intval( $row['status'] );
		
		$md = new MysqlDateTime();
		$md->Parse( $row['created_at'] );
		$this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
		
		$md->Parse( $row['updated_at'] );
		$this->updated_at = $md->GetFrontEndValue('d.m.y.hh.mm');
		
		$this->loaded = true;
	}
	
	function Save( $data=null )
	{   
		$this->CheckupData();
		
		$t = new MysqlTable( $this->tablename );
		if( is_array($data) ) $t->save( $data );
		else
		{
			$data = Array();
			$data['id'] = $this->id;
			$data['order_number'] = $this->order_number;
			$data['customer_id'] = $this->customer_id;
			$data['first_name'] = $this->first_name;
			$data['last_name'] = $this->last_name;
			$data['product_details'] = $this->product_details;
			$data['street'] = $this->street;
			$data['city'] = $this->city;
			$data['zip'] = $this->zip;
			$data['phone'] = $this->phone;
			$data['email'] = $this->email;
			$data['comment'] = $this->comment;
			$data['page_count'] = $this->page_count;
			$data['copies'] = $this->copies;
			$data['total_price'] = $this->total_price;
			$data['status'] = $this->status;
			
			$t->save( $data );
		}
			
		$this->Load( $t->get_last_insert_id() );
		
		return( $this->id );
	}
	
	function Delete()
	{
		if( $this->IsLoaded() )
		{
			$t = new MysqlTable( $this->tablename );
			$t->del( $this->id );
		}
	}
	
	function ParseFromFile( $rows ) {
		global $log;
		
		if( $rows && is_array($rows) ) {
			foreach( $rows as $row ) {
				for( $j=0; $j<count($this->file_fields); $j++ ) {
					$f = $this->file_fields[$j];
					if( stristr( $row, $f[0] ) ) {
						$parse = explode( '=', $row );
						if( count($parse) >= 2 ) {
							$value = trim($parse[1]);
							if( $f[1] == 1 ) $value = intval( $value );
							else if( $f[1] == 3 ) $value = floatval( $value );
							$this->{$f[2]} = $value;
						}
					}
				}
			}
		}
	}
	
	function Xml()
	{
		$retval = "<order>";
			$retval .= "<id>{$this->id}</id>";
			$retval .= "<order_number>{$this->order_number}</order_number>";
			$retval .= "<customer_id>{$this->customer_id}</customer_id>";
			$retval .= "<first_name><![CDATA[{$this->first_name}]]></first_name>";
			$retval .= "<last_name><![CDATA[{$this->last_name}]]></last_name>";
			$retval .= "<product_details><![CDATA[{$this->product_details}]]></product_details>";
			$retval .= "<street><![CDATA[{$this->street}]]></street>";
			$retval .= "<city><![CDATA[{$this->city}]]></city>";
			$retval .= "<zip><![CDATA[{$this->zip}]]></zip>";
			$retval .= "<phone><![CDATA[{$this->phone}]]></phone>";
			$retval .= "<email><![CDATA[{$this->email}]]></email>";
			$retval .= "<comment><![CDATA[{$this->comment}]]></comment>";
			$retval .= "<page_count>{$this->page_count}</page_count>";
			$retval .= "<copies>{$this->copies}</copies>";
			$retval .= "<total_price>{$this->total_price}</total_price>";
			$retval .= "<status>{$this->status}</status>";
			$retval .= "<created_at>{$this->created_at}</created_at>";
			$retval .= "<updated_at>{$this->updated_at}</updated_at>";
		$retval .= "</order>";
		
		return( $retval );
	}
}

?>
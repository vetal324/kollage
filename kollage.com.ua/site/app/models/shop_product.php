<?php
/*****************************************************
 Class v.1.0, 2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

class ShopProduct extends Model
{
	var $category_id, $brand_id, $name, $ingress, $description, $number, $price, $price_opt, $price_opt2, $nice_price, $nice_price_start_at, $nice_price_end_at, $status, $design_type, $show_in_shop_news, $show_in_best, $is_new, $is_action;
	var $images, $features, $feature_values, $individual_features;
	var $folder_for_child = 'product';
	var $categories, $page_in_category;
	var $quantity_local, $quantity_external; //������� �� ���������� ������ � �������
	var $full_load;
	
	var $tablename = 'shop_products';
	
	var $debug = false;
	var $log;

	function ShopProduct( $id=0, $full_load=true )
	{
		global $log;
		$this->log = $log;
	
		$this->show_in_shop_news = 0;
		$this->show_in_best = 0;
		$this->is_new = 0;
		$this->is_action = 0;
		$this->status = 1;
		$this->price = 0.0;
		$this->quantity_local = 0;
		$this->quantity_external = 0;
		$this->images = Array();
		$this->features = Array();
		$this->feature_values = Array();
		$this->individual_features = Array();
		$this->categories = Array();
		$this->full_load = $full_load;
		$this->page_in_category = 1;
		parent::Model( $id );
	}
	
	function _Load( &$row )
	{
		global $db;
		
		$this->id = intval( $row['id'] );
		
		$this->category_id = intval( $row['category_id'] );

		if( $this->debug ) $d_time = microtime(true);
		$this->brand_id = intval( $row['brand_id'] );
		$this->number = $row['number'];
		$this->price = floatval( $row['price'] );
		$this->price_opt = floatval( $row['price_opt'] );
		$this->price_opt2 = floatval( $row['price_opt2'] );
		$this->nice_price = floatval( $row['nice_price'] );
		$this->status = intval( $row['status'] );
		$this->design_type = intval( $row['design_type'] );
		$this->show_in_shop_news = intval( $row['show_in_shop_news'] );
		$this->show_in_best = intval( $row['show_in_best'] );
		$this->is_new = intval( $row['is_new'] );
		$this->is_action = intval( $row['is_action'] );
		
		$this->position = intval( $row['position'] );
		
		$t = new MysqlTable('dictionary');
		if( $t->find_all( "parent_id={$this->id} and folder='{$this->tablename}' and lang='". $_SESSION['lang'] ."'" ) )
		{
			foreach( $t->data as $row2 )
			{
				switch( $row2['name'] )
				{
					case 'name';
						$this->name = $row2['value'];
						break;
					case 'ingress';
						$this->ingress = $row2['value'];
						break;
					case 'description';
						$this->description = $row2['value'];
						break;
				}
			}
		}
		
		$md = new MysqlDateTime();
		$md->Parse( $row['created_at'] );
		$this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
		
		$md->Parse($row['nice_price_start_at']);
		$this->nice_price_start_at = $md->GetValue( 'd.m.y' );
		
		$md->Parse($row['nice_price_end_at']);
		$this->nice_price_end_at = $md->GetValue( 'd.m.y' );
		
		$this->updated_at = $row['updated_at'];
		
		//if( $this->debug ) $d_time = microtime(true);
		$this->quantity_local = intval( $db->getone( "SELECT sum(pa.quantity) FROM shop_product_availability pa LEFT JOIN shop_points sp ON sp.id=pa.point_id WHERE pa.product_id={$this->id} AND sp.group_id=1 AND sp.status=1" ) );
		$this->quantity_external = intval( $db->getone( "SELECT sum(pa.quantity) FROM shop_product_availability pa LEFT JOIN shop_points sp ON sp.id=pa.point_id WHERE pa.product_id={$this->id} AND sp.group_id=2 AND sp.status=1" ) );
		//if( $this->debug ) $this->log->Write( "ShopProduct._Load: load quanties (". (microtime(true) - $d_time) ." sec)" );
		
		$this->loaded = true;
		
		#Load gallery
		//if( $this->debug ) $d_time = microtime(true);
		$this->LoadImages();
		//if( $this->debug ) $this->log->Write( "ShopProduct._Load: load images (". (microtime(true) - $d_time) ." sec)" );
		
		if( $this->full_load )
		{
			//$this->LoadFeatures();
			$this->LoadFeatures( true );
		}
	}
	
	function LoadImages()
	{
		if( $this->IsLoaded() )
		{
			$t = new MysqlTable('images');
			if( $t->find_all( "folder='{$this->folder_for_child}' and parent_id={$this->id}", "position,id" ) )
			{
				unset( $this->images ); $this->images = Array();
				foreach( $t->data as $row )
				{
					array_push( $this->images, new Image($row['id'],$this->full_load) );
					if( !$this->full_load ) break;
				}
			}
		}
	}
	
	function LoadFeatures( $load_all=false )
	{
		global $db;

		if( $this->IsLoaded() )
		{
			if( $load_all ) $rs = $db->query( "select distinct f.id,v.id,p.product_id,f.position,f.match_analog from shop_features f left join shop_feature_values v on f.id=v.feature_id left join shop_product_features p on v.id=p.feature_value_id where p.product_id={$this->id} order by f.position,f.match_analog,f.id" );
			else $rs = $db->query( "select distinct f.id,v.id,p.product_id,f.position,f.match_analog from shop_features f left join shop_feature_values v on f.id=v.feature_id left join shop_product_features p on v.id=p.feature_value_id where p.product_id={$this->id} and (f.match_analog=0 or f.match_analog is null) and f.status=1 and v.status=1 order by f.position,f.match_analog,f.id" );
			if( $rs )
			{
				$this->features = Array();
				$this->feature_values = Array();
				while( $row = $rs->fetch_row() )
				{
					array_push( $this->features, new ShopFeature($row[0],false) );
					array_push( $this->feature_values, new ShopFeatureValue($row[1]) );
				}
			}
			//load individual features
			if( $load_all ) $rs = $db->query( "select id, individual_name, individual_value from shop_product_features where product_id={$this->id} and feature_value_id is null order by position,id" );
			else {
				$rs = $db->query( "select id, individual_name, individual_value from shop_product_features where product_id={$this->id} and status=1 and feature_value_id is null order by position,id" );
			}
			if( $rs )
			{
				$this->individual_features = Array();
				while( $row = $rs->fetch_row() )
				{
					$f = new StdClass();
					$f->id = $row[0];
					$f->individual_name = $row[1];
					$f->individual_value = $row[2];
					array_push( $this->individual_features, $f );
				}
			}
		}
	}
	
	function Save( $data=null )
	{   
		$this->CheckupData();
		
		$t = new MysqlTable( $this->tablename );
		if( is_array($data) ) $t->save( $data );
		else
		{
			$data = Array();
			$data['id'] = $this->id;
			$data['category_id'] = $this->category_id;
			$data['brand_id'] = $this->brand_id;
			$data['name'] = $this->name;
			$data['ingress'] = $this->ingress;
			$data['description'] = $this->description;
			$data['number'] = $this->number;
			$data['price'] = $this->price;
			$data['price_opt'] = $this->price_opt;
			$data['price_opt2'] = $this->price_opt2;
			$data['nice_price'] = $this->nice_price;
			$data['nice_price_start_at'] = $this->nice_price_start_at;
			$data['nice_price_end_at'] = $this->nice_price_end_at;
			$data['status'] = $this->status;
			$data['show_in_shop_news'] = $this->show_in_shop_news;
			$data['show_in_best'] = $this->show_in_best;
			$data['is_new'] = $this->is_new;
			$data['is_action'] = $this->is_action;
			$data['design_type'] = $this->design_type;
			$data['position'] = $this->position;
			$data['lang'] = $_SESSION['lang'];

			$t->save( $data );
		}

		$this->Load( $t->get_last_insert_id() );
		
		return( $this->id );
	}
	
	function Delete()
	{
		global $db;
		
		if( $this->IsLoaded() )
		{
			$sql = "select o.id from shop_orders o left join shop_order_products p on p.order_id=o.id where o.status in (0,1,2,3) and p.product_id={$this->fields['id']} limit 1";
			$id = intval( $db->getone($sql) );
			if( $id > 0 )
			{
				$this->status = 99;
				$this->Save();
			}
			else
			{
				parent::Delete();
			}
		}
	}
	
	function NicePriceActive()
	{
		global $db;
		$retval = false;
		
		if( $this->IsLoaded() )
		{
			$np = $db->getone( "select (now()>=nice_price_start_at and now()<nice_price_end_at) from {$this->tablename} where id={$this->id}" );
			if( $np=="1" ) $retval = true;
		}
		
		return( $retval );
	}
	
	function GetPrice()
	{
		$retval = 0;
		
		if( $this->IsLoaded() )
		{
			if( $this->NicePriceActive() )
			{
				$retval = $this->nice_price;
			}
			else
			{
				$retval = $this->price;
			}
		}
		
		return( $retval );
	}
	
	function GetPriceOpt()
	{
		$retval = 0;
		
		if( $this->IsLoaded() )
		{
			if( $this->NicePriceActive() )
			{
				$retval = $this->nice_price;
			}
			else
			{
				$retval = $this->price_opt;
			}
		}
		
		return( $retval );
	}
	
	function GetPriceOpt2()
	{
		$retval = 0;
		
		if( $this->IsLoaded() )
		{
			if( $this->NicePriceActive() )
			{
				$retval = $this->nice_price;
			}
			else
			{
				$retval = $this->price_opt2;
			}
		}
		
		return( $retval );
	}
	
	function GetPriceByClient( $client ) {
		$retval = 0;
		
		if( $this->IsLoaded() ) {
			if( $client && $client->type_id == 2 ) $retval = $this->GetPriceOpt();
			else if( $client && $client->type_id == 3 ) $retval = $this->GetPriceOpt2();
			else $retval = $this->GetPrice();
		}
		
		return( $retval );
	}
	
	function Xml()
	{
		$retval = "<product>";
		$retval .= "<id>{$this->id}</id>";
		$retval .= "<folder_for_child><![CDATA[{$this->folder_for_child}]]></folder_for_child>";
		$retval .= "<category_id>{$this->category_id}</category_id>";
		$retval .= "<brand_id>{$this->brand_id}</brand_id>";
		$retval .= "<name><![CDATA[{$this->name}]]></name>";
		$retval .= "<ingress><![CDATA[{$this->ingress}]]></ingress>";
		$retval .= "<description><![CDATA[{$this->description}]]></description>";
		$retval .= "<number><![CDATA[{$this->number}]]></number>";
		$retval .= "<price>". sprintf( "%01.2f", floatval( $this->price ) ) ."</price>";
		$retval .= "<price_opt>". sprintf( "%01.2f", floatval( $this->price_opt ) ) ."</price_opt>";
		$retval .= "<price_opt2>". sprintf( "%01.2f", floatval( $this->price_opt2 ) ) ."</price_opt2>";
		$retval .= "<nice_price>". sprintf( "%01.2f", floatval( $this->nice_price ) ) ."</nice_price>";
		$retval .= "<nice_price_start_at>{$this->nice_price_start_at}</nice_price_start_at>";
		$retval .= "<nice_price_end_at>{$this->nice_price_end_at}</nice_price_end_at>";
		$retval .= "<nice_price_active>". $this->NicePriceActive() ."</nice_price_active>";
		$retval .= "<status>{$this->status}</status>";
		$retval .= "<quantity_local>{$this->quantity_local}</quantity_local>";
		$retval .= "<quantity_external>{$this->quantity_external}</quantity_external>";
		$retval .= "<design_type>{$this->design_type}</design_type>";
		$retval .= "<show_in_shop_news>{$this->show_in_shop_news}</show_in_shop_news>";
		$retval .= "<show_in_best>{$this->show_in_best}</show_in_best>";
		$retval .= "<is_new>{$this->is_new}</is_new>";
		$retval .= "<is_action>{$this->is_action}</is_action>";
		$retval .= "<position>{$this->position}</position>";
		$retval .= "<created_at>{$this->created_at}</created_at>";
		$retval .= "<updated_at>{$this->updated_at}</updated_at>";
		$retval .= "<page_in_category>{$this->page_in_category}</page_in_category>";
		
		$retval .= "<images>";
		foreach( $this->images as $img )
		{
			$retval .= $img->Xml();
		}
		$retval .= "</images>";
  
		$retval .= "<features>";
		for( $i=0; $i<count($this->features); $i++ )
		{
			$retval .= "<feature id='{$this->features[$i]->id}' match_analog='{$this->features[$i]->match_analog}' show_in_short_description='{$this->features[$i]->show_in_short_description}' show_in_filter='{$this->features[$i]->show_in_filter}' value_id='{$this->feature_values[$i]->id}'>";
				$retval .= "<name><![CDATA[{$this->features[$i]->name}]]></name>";
				$retval .= "<value_name><![CDATA[{$this->feature_values[$i]->name}]]></value_name>";
			$retval .= "</feature>";
		}
		$retval .= "</features>";
  
		$retval .= "<individual_features>";
		for( $i=0; $i<count($this->individual_features); $i++ )
		{
			$retval .= "<feature id='{$this->individual_features[$i]->id}'>";
				$retval .= "<individual_name><![CDATA[{$this->individual_features[$i]->individual_name}]]></individual_name>";
				$retval .= "<individual_value><![CDATA[{$this->individual_features[$i]->individual_value}]]></individual_value>";
			$retval .= "</feature>";
		}
		$retval .= "</individual_features>";
		
		$retval .= "<categories>";
		foreach( $this->categories as $c )
		{
			$retval .= $c->Xml();
		}
		$retval .= "</categories>";
		
		$retval .= "</product>";
		
		return( $retval );
	}
}

?>
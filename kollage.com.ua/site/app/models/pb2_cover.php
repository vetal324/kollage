<?php
/*****************************************************
 Class v.1.0, 2011
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

class PB2Cover extends Model
{
    var $name, $description, $pb_size_id, $category_id, $price, $status, $is_individual = 0, $has_supercover,
        $check_size = 1, $supercover_price, $allow_min_pages;
    var $image;
    var $supercovers;
    
    var $tablename = 'pb2_covers';

    function __construct( $id=0 )
    {
        $this->status = 1;
        $this->supercovers = Array();
        parent::Model( $id );
    }
    
    function _Load( &$row )
    {
        $this->id = intval( $row['id'] );
        
        $this->status = intval( $row['status'] );
        $this->position = intval( $row['position'] );
        
        $this->name = trim( $row['name'] );
        $this->description = trim( $row['description'] );
        $this->image = $row['image'];
        
        $this->price = floatval( $row['price'] );
        $this->supercover_price = floatval( $row['supercover_price'] );
        $this->pb_size_id = intval( $row['pb_size_id'] );
        $this->category_id = intval( $row['category_id'] );
        $this->is_individual = intval( $row['is_individual'] );
        $this->has_supercover = intval( $row['has_supercover'] );
        $this->check_size = intval( $row['check_size'] );
        $this->allow_min_pages = intval( $row['allow_min_pages'] );
        
        $md = new MysqlDateTime();
        $md->Parse( $row['created_at'] );
        $this->created_at = $md->GetFrontEndValue('d.m.y.hh.mm');
        
        $this->updated_at = $row['updated_at'];
        
        $this->loaded = true;
        
        $this->LoadSupercovers();
    }
    
    function LoadSupercovers() {
			if( $this->IsLoaded() )
			{
				$t = new MysqlTable('pb2_supercovers');
				if( $t->find_all( "pb_cover_id={$this->id}", "position,id" ) )
				{
					unset( $this->supercovers ); $this->supercovers = Array();
					foreach( $t->data as $row )
					{
						array_push( $this->supercovers, new PB2SuperCover($row['id']) );
					}
				}
			}
		}
    
    function Save( $data=null )
    {   
        $this->CheckupData();
        
        $t = new MysqlTable( $this->tablename );
        if( is_array($data) ) $t->save( $data );
        else
        {
            $data = Array();
            $data['id'] = $this->id;
            $data['name'] = $this->name;
            $data['description'] = $this->description;
            $data['image'] = $this->image;
            $data['pb_size_id'] = $this->pb_size_id;
            $data['category_id'] = $this->category_id;
            $data['status'] = $this->status;
            $data['price'] = $this->price;
            $data['is_individual'] = $this->is_individual;
            $data['has_supercover'] = $this->has_supercover;
            $data['check_size'] = $this->check_size;
            $data['allow_min_pages'] = $this->allow_min_pages;
            $data['supercover_price'] = $this->supercover_price;
            $data['position'] = $this->position;
            $data['lang'] = $_SESSION['lang'];
            
            $t->save( $data );
        }
            
        $this->Load( $t->get_last_insert_id() );
        
        return( $this->id );
    }
    
    function Delete()
    {
        if( $this->IsLoaded() )
        {
            $this->status = 0;
            $this->Save();
        }
    }
	
		function DeleteImage()
		{
			if( $this->IsLoaded() )
			{
				$attachment = new Attachment();
				$attachment->filename = $this->image;
				$attachment->rm();
				$attachment->filename = 'thumbnail-'. $attachment->filename;
				$attachment->rm();
				
				$this->image = '';
				$this->Save();
			}
		}

    function Json()
    {
        $pbCover = new StdClass();

        $pbCover->id = $this->id;
        $pbCover->name = $this->name;
        $pbCover->image = $this->image;
        $pbCover->price = $this->price;
        $pbCover->supercover_price = $this->supercover_price;
        $pbCover->is_individual = $this->is_individual;
        $pbCover->has_supercover = $this->has_supercover;
        $pbCover->check_size = $this->check_size;
        $pbCover->allow_min_pages = $this->allow_min_pages;

        $pbCover->supercovers = array();
        foreach($this->supercovers as $sc) {
            $pbCover->supercovers[$sc->id] = $sc->Json();
        }

        return $pbCover;
    }


    function Xml()
    {
        $retval = "<pb_cover>";
        $retval .= "<id>{$this->id}</id>";
        $retval .= "<name><![CDATA[{$this->name}]]></name>";
        $retval .= "<description><![CDATA[{$this->description}]]></description>";
        $retval .= "<image><![CDATA[{$this->image}]]></image>";
        $retval .= "<status>{$this->status}</status>";
        $retval .= "<position>{$this->position}</position>";
        $retval .= "<category_id>{$this->category_id}</category_id>";
        $retval .= "<pb_size_id>{$this->pb_size_id}</pb_size_id>";
        $retval .= "<is_individual>{$this->is_individual}</is_individual>";
        $retval .= "<has_supercover>{$this->has_supercover}</has_supercover>";
        $retval .= "<check_size>{$this->check_size}</check_size>";
        $retval .= "<allow_min_pages>{$this->allow_min_pages}</allow_min_pages>";
        $retval .= "<price>{$this->price}</price>";
        $retval .= "<supercover_price>{$this->supercover_price}</supercover_price>";
        $retval .= "<created_at>{$this->created_at}</created_at>";
        $retval .= "<updated_at>{$this->updated_at}</updated_at>";
        
				$retval .= "<supercovers>";
				foreach( $this->supercovers as $sc )
				{
					$retval .= $sc->Xml();
				}
				$retval .= "</supercovers>";
		
        $retval .= "</pb_cover>";
        
        return( $retval );
    }
}

?>
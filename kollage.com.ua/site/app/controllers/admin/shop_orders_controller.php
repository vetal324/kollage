<?php
/*****************************************************
 Class v.1.0, 02.2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

include_once( 'base_controller.php' );

class shop_orders_controller extends base_controller
{
	var $folder = '';
	
	function shop_orders_controller()
	{
		parent::base_controller();
		
		$this->menuleft->add_item( new WEMenuItem( "s1", $this->dictionary->get('/locale/shop_orders/menuleft/s1'), "?".$this->controller_name.".index&s=1" ) );
		$this->menuleft->add_item( new WEMenuItem( "s2", $this->dictionary->get('/locale/shop_orders/menuleft/s2'), "?".$this->controller_name.".index&s=2" ) );
		$this->menuleft->add_item( new WEMenuItem( "s3", $this->dictionary->get('/locale/shop_orders/menuleft/s3'), "?".$this->controller_name.".index&s=3" ) );
		$this->menuleft->add_item( new WEMenuItem( "s99", $this->dictionary->get('/locale/shop_orders/menuleft/s99'), "?".$this->controller_name.".index&s=99" ) );
	}
	
	function index( $parameters='' )
	{
		$status = intval( $_REQUEST['s'] );
		$this->bigtabs->select_child( 'shop_orders' );
		$this->menuleft->select_child( 's'. $status );
		
		$this->method_name = 'list';
		$xml = "<data>";
		$xml .= "<list>";
		
		$t = new MysqlTable( 'shop_orders' );
		parent::define_pages( $t->get_count('status='.$status) );
		$t->find_all( 'status='.$status, 'id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$order = new ShopOrder( $a['id'] );
			$xml .= $order->Xml();
		}
		
		$xml .= "</list>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'list', $xml );
	}
	
	function edit( $save_result="" )
	{
		$order = new ShopOrder( $_REQUEST['id'], true );
		$this->bigtabs->select_child( 'shop_orders' );
		$this->menuleft->select_child( 's'. $order->status );
		
		$this->method_name = 'edit';
		$xml = "<data>";
		
		$xml .= "<edit>";
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= $order->Xml();
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'edit', $xml );
	}
	
	function save()
	{
		$this->edit( 'false' );
	}
	
	function order_del()
	{
		$order = new ShopOrder( $_REQUEST['id'] );
		$_REQUEST['s'] = $order->status;
		$order->Delete();
		$this->index();

	}
	
	function del_product()
	{
		$id = intval( $_REQUEST['id'] );
		$o = new ShopOrderProduct( $id );
		if( $o->IsLoaded() )
		{
			$_REQUEST['id'] = $o->order_id;
			$o->Delete();
		}
		$this->edit();
	}
	
	function process()
	{
		$status = intval( $_REQUEST['status'] ); if( $status == 0 ) $status = 2;
		$order = new ShopOrder( $_REQUEST['id'] );
		$_REQUEST['s'] = $order->status;
		$order->status = $status;
		$order->Save();
		$this->index();

	}
	
	function set_exists()
	{
		$id = intval( $_REQUEST['id'] );
		$v = intval( $_REQUEST['v'] );
		$o = new ShopOrderProduct( $id );
		if( $o->IsLoaded() )
		{
			$o->pexists = $v;
			$o->Save();
		}
		
		$_REQUEST['id'] = $o->order_id;
		$this->edit();
	}
	
	function orders_del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		$o = new ShopOrder( $ids[0] );
		$_REQUEST['s'] = $o->status;
		foreach( $ids as $id )
		{
			$o = new ShopOrder( $id );
			$o->Delete();
		}
		
		$this->index();
	}
	
	function del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new ShopOrder( $id );
			$o->Delete();
		}
		$this->index( 'scroll_to_list' );
	}
	
	function order_view()
	{
		$this->edit();
	}
	
	function order_send()
	{
		$o = new ShopOrder( $_REQUEST['id'], true );
		$subject = $_REQUEST['subject'];
		$body = $_REQUEST['body'];
		if( $o->IsLoaded() )
		{
			//Send email to admin and client
			$settings = new Settings();
			$shop_settings = new ShopSettings();
			$mail = new AMail( 'windows-1251' );
				$mail->subject = $subject;
				
				$mail->from_name = $settings->from_name;
				$mail->from_email = $settings->from_email;
				$mail->to_name = $o->name;
				$mail->to_email = $o->email;
			$mail->Send( $body );
		}        
		$this->edit();
	}
}

?>
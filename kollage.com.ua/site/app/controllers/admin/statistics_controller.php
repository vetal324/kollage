<?php
/*****************************************************
 Class v.1.0, 02.2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

include_once( 'base_controller.php' );

class statistics_controller extends base_controller
{
    function statistics_controller()
    {
        parent::base_controller();
        
        $this->menuleft->add_item( new WEMenuItem( "unique_visitors", $this->dictionary->get('/locale/statistics/menuleft/unique_visitors'), "?".$this->controller_name.".unique_visitors" ) );
    }

    function index()
    {
        $this->unique_visitors();
    }

    function unique_visitors()
    {
        global $db;
        $date_sql = "";
        
        $this->bigtabs->select_child( 'statistics' );
        $this->menuleft->select_child( 'unique_visitors' );
        
        $this->method_name = 'unique_visitors';
        
        $uv = $db->getone( "select sum(hits) as c from unique_visitors $date_sql" );
        $from_date = $db->getone( "select date_format(created_at,'%d.%m.%Y') as created_at2 from unique_visitors $date_sql order by created_at limit 1" );
        $to_date = $db->getone( "select date_format(created_at,'%d.%m.%Y') as created_at2 from unique_visitors $date_sql order by created_at desc limit 1" );
        
        $xml = "<data>";
        $xml .= "<uv>$uv</uv>";
        $xml .= "<from_date>$from_date</from_date>";
        $xml .= "<to_date>$to_date</to_date>";
        $xml .= "</data>";
        $this->display( 'unique_visitors', $xml );
    }
}

?>
<?php
/*****************************************************
 Class v.1.0, 02.2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

include_once( 'base_controller.php' );

class photos_controller extends base_controller
{
	var $folder = 'photo';
	
	function photos_controller()
	{
		parent::base_controller();
		
		$this->menuleft->add_item( new WEMenuItem( "orders_list1", $this->dictionary->get('/locale/sop/menuleft/orders_list1'), "?".$this->controller_name.".orders_list1" ) );
		$this->menuleft->add_item( new WEMenuItem( "orders_list2", $this->dictionary->get('/locale/sop/menuleft/orders_list2'), "?".$this->controller_name.".orders_list2" ) );
		$this->menuleft->add_item( new WEMenuItem( "orders_list3", $this->dictionary->get('/locale/sop/menuleft/orders_list3'), "?".$this->controller_name.".orders_list3" ) );
		$this->menuleft->add_item( new WEMenuItem( "orders_list4", $this->dictionary->get('/locale/sop/menuleft/orders_list4'), "?".$this->controller_name.".orders_list4" ) );
		$this->menuleft->add_item( new WEMenuSeparator() );
		$this->menuleft->add_item( new WEMenuItem( "report_by_points", $this->dictionary->get('/locale/sop/menuleft/report_by_points'), "?".$this->controller_name.".report_by_points" ) );
		$this->menuleft->add_item( new WEMenuItem( "report_by_dates", $this->dictionary->get('/locale/sop/menuleft/report_by_dates'), "?".$this->controller_name.".report_by_dates" ) );
		$this->menuleft->add_item( new WEMenuSeparator() );
		$this->menuleft->add_item( new WEMenuItem( "articles_list", $this->dictionary->get('/locale/sop/menuleft/articles_list'), "?photo_articles.index" ) );
		$this->menuleft->add_item( new WEMenuSeparator() );
		$this->menuleft->add_item( new WEMenuItem( "userfiles", $this->dictionary->get('/locale/clients/menuleft/userfiles'), "?photos.list_userfiles_clients" ) );
		$this->menuleft->add_item( new WEMenuSeparator() );
		$this->menuleft->add_item( new WEMenuItem( "settings", $this->dictionary->get('/locale/sop/menuleft/settings'), "?".$this->controller_name.".settings_edit" ) );
		$this->menuleft->add_item( new WEMenuItem( "settings2", $this->dictionary->get('/locale/sop/menuleft/settings2'), "?".$this->controller_name.".settings_edit2" ) );
		$this->menuleft->add_item( new WEMenuItem( "points_list", $this->dictionary->get('/locale/sop/menuleft/points_list'), "?".$this->controller_name.".points_list" ) );
		$this->menuleft->add_item( new WEMenuItem( "deliveries_list", $this->dictionary->get('/locale/sop/menuleft/deliveries_list'), "?".$this->controller_name.".deliveries_list" ) );
		$this->menuleft->add_item( new WEMenuItem( "paymentways_list", $this->dictionary->get('/locale/sop/menuleft/paymentways_list'), "?".$this->controller_name.".paymentways_list" ) );
		//$this->menuleft->add_item( new WEMenuItem( "discounts_list", $this->dictionary->get('/locale/sop/menuleft/discounts_list'), "?".$this->controller_name.".discounts_list" ) );
		$this->menuleft->add_item( new WEMenuItem( "sizes_list", $this->dictionary->get('/locale/sop/menuleft/sizes_list'), "?".$this->controller_name.".sizes_list" ) );
	}
	
	function make_elements()
	{
		$retval = "";
		
		$retval .= $this->bigtabs->xml();
		$retval .= $this->smalltabs->xml();
		$retval .= $this->menuleft->xml();
		
		$retval .= "<left_panel show='yes'></left_panel>";
		
		return( $retval );
	}
	
	function index( $parameters='' )
	{
		$this->orders_list1( $parameters );
	}
	
	function order_del()
	{
		$o = new SOPOrder( $_REQUEST['id'] );
		$status = $o->status;
		$o->DeleteFolder();
		$o->status = 99;
		$o->Save();

		switch( $status )
		{
			case 2:
				$this->orders_list2();
				break;
			case 3:
				$this->orders_list3();
				break;
			case 4:
				$this->orders_list4();
				break;
			case 1:
			default:
				$this->orders_list1();
				break;
		}
	}
	
	function orders_del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new SOPOrder( $id );
			if( !$status ) $status = $o->status;
			$o->DeleteFolder();
			$o->status = 99;
			$o->Save();
		}
		
		switch( $status )
		{
			case 2:
				$this->orders_list2();
				break;
			case 3:
				$this->orders_list3();
				break;
			case 4:
				$this->orders_list4();
				break;
			case 1:
			default:
				$this->orders_list1();
				break;
		}
	}
	
	function orders_change_status_marked()
	{
		$status = $_REQUEST['status']; if( !$status ) $status = 3;
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new SOPOrder( $id );
			$o->status = $status;
			$o->Save();
		}
		
		switch( $status-1 )
		{
			case 2:
				$this->orders_list2();
				break;
			case 3:
				$this->orders_list3();
				break;
			case 4:
				$this->orders_list4();
				break;
			case 1:
			default:
				$this->orders_list1();
				break;
		}
	}
	
	function orders_change_status_all()
	{
		global $db;

		$status = $_REQUEST['status']; if( !$status ) $status = 3;
		$sql = "update sop_orders set status={$status} where status=". ($status-1);
		$db->query( $sql );
		
		switch( $status-1 )
		{
			case 2:
				$this->orders_list2();
				break;
			case 3:
				$this->orders_list3();
				break;
			case 4:
				$this->orders_list4();
				break;
			case 1:
			default:
				$this->orders_list1();
				break;
		}
	}
	
	// <Sizes of photos>
	function sizes_list( $parameters='' )
	{
		$this->bigtabs->select_child( 'photos' );
		$this->menuleft->select_child( 'sizes_list' );
		
		$this->method_name = 'sizes_list';
		$xml = "<data>";
		$xml .= "<sizes>";
		
		$t = new MysqlTable( 'sop_order_photos_sizes' );
		parent::define_pages( $t->get_count('status=1') );
		$t->find_all( 'status=1', 'position,id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$o = new SOPOrderPhotoSize( $a['id'] );
			
			if( $o->position == 0 )
			{
				$tp = new MysqlTable('sop_order_photos_sizes');
				$tp->fix_positions( '', "status=1" );
				$o->Load( $a['id'] );
			}
			
			$xml .= $o->Xml();
		}
		
		$xml .= "<last_position>{$o->position}</last_position>";
		
		$xml .= "</sizes>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'sizes_list', $xml );
	}
	
	function size_edit( $save_result="" )
	{
		$last_position = intval( $_REQUEST['last_position'] );
		
		$this->bigtabs->select_child( 'photos' );
		$this->menuleft->select_child( 'sizes_list' );
		
		$this->method_name = 'size_edit';
		$xml = "<data>";
		
		$xml .= "<size_edit>";
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= "<last_position>$last_position</last_position>";
		$o = new SOPOrderPhotoSize( $_REQUEST['id'] );
		$xml .= $o->Xml();
		$xml .= "</size_edit>";
		
		$xml .= "</data>";
		
		$this->display( 'size_edit', $xml );
	}
	
	function size_save()
	{
		$erx = "";
		
		$obj = new SOPOrderPhotoSize( $_REQUEST['size']['id'] );
		$last_position = intval( $_REQUEST['last_position'] );
		if( $last_position > 0 ) $_REQUEST['size']['position'] = $last_position + 1;
		
		if( $obj->Save( $_REQUEST['size'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->size_edit( 'true' );
		}
		else
		{
			$this->size_edit( 'false' );
		}
	}
	
	function size_del()
	{
		$o = new SOPOrderPhotoSize( $_REQUEST['id'] );
		$o->Delete();
		$this->sizes_list( 'scroll_to_list' );
	}
	
	function sizes_del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new SOPOrderPhotoSize( $id );
			$o->Delete();
		}
		$this->sizes_list( 'scroll_to_list' );
	}
	
	function size_move_up()
	{
		$t = new MysqlTable('sop_order_photos_sizes');
		$t->move_position_up( $_REQUEST['id'] );
		$this->sizes_list( 'scroll_to_list' );
	}
	
	function size_move_down()
	{
		$t = new MysqlTable('sop_order_photos_sizes');
		$t->move_position_down( $_REQUEST['id'] );
		$this->sizes_list( 'scroll_to_list' );
	}
	
	function size_swap_position()
	{
		$src = new SOPOrderPhotoSize( $_REQUEST['src'] );
		$dst = new SOPOrderPhotoSize( $_REQUEST['dst'] );
		$this->SwapPosition( $src, $dst, "sop_order_photos_sizes", "status=1" );
		$this->sizes_list( 'scroll_to_list' );
	}
	// </Sizes of photos>

	// <Discounts>
	function discounts_list( $parameters='' )
	{
		$this->bigtabs->select_child( 'photos' );
		$this->menuleft->select_child( 'discounts_list' );
		
		$this->method_name = 'discounts_list';
		$xml = "<data>";
		$xml .= "<list>";
		
		$t = new MysqlTable( 'sop_discounts' );
		parent::define_pages( $t->get_count() );
		$t->find_all( '', 'position,id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$o = new SOPDiscount( $a['id'] );
			
			if( $o->position == 0 )
			{
				$tp = new MysqlTable('sop_discounts');
				$tp->fix_positions();
				$o->Load( $a['id'] );
			}
			
			$xml .= $o->Xml();
		}
		
		$xml .= "<last_position>{$o->position}</last_position>";
		
		$xml .= "</list>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'discounts_list', $xml );
	}
	
	function discount_edit( $save_result="" )
	{
		$last_position = intval( $_REQUEST['last_position'] );
		
		$this->bigtabs->select_child( 'photos' );
		$this->menuleft->select_child( 'discounts_list' );
		
		$this->method_name = 'discount_edit';
		$xml = "<data>";
		
		$xml .= "<edit>";
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= "<last_position>$last_position</last_position>";
		$o = new SOPDiscount( $_REQUEST['id'] );
		$xml .= $o->Xml();
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'discount_edit', $xml );
	}
	
	function discount_save()
	{
		$erx = "";
		
		$obj = new SOPDiscount( $_REQUEST['discount']['id'] );
		$last_position = intval( $_REQUEST['last_position'] );
		if( $last_position > 0 ) $_REQUEST['discount']['position'] = $last_position + 1;
		
		if( $obj->Save( $_REQUEST['discount'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->discount_edit( 'true' );
		}
		else
		{
			$this->discount_edit( 'false' );
		}
	}
	
	function discount_del()
	{
		$o = new SOPDiscount( $_REQUEST['id'] );
		$o->Delete();
		$this->discounts_list( 'scroll_to_list' );
	}
	
	function discounts_del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new SOPDiscount( $id );
			$o->Delete();
		}
		$this->discounts_list( 'scroll_to_list' );
	}
	
	function discount_move_up()
	{
		$t = new MysqlTable('sop_discounts');
		$t->move_position_up( $_REQUEST['id'] );
		$this->discounts_list( 'scroll_to_list' );
	}
	
	function discount_move_down()
	{
		$t = new MysqlTable('sop_discounts');
		$t->move_position_down( $_REQUEST['id'] );
		$this->discounts_list( 'scroll_to_list' );
	}
	
	function discount_swap_position()
	{
		$src = new SOPDiscount( $_REQUEST['src'] );
		$dst = new SOPDiscount( $_REQUEST['dst'] );
		$this->SwapPosition( $src, $dst, "sop_discounts", "status=1" );
		$this->discounts_list( 'scroll_to_list' );
	}
	// </Discounts>
	
	// <Paymentways>
	function paymentways_list( $parameters='' )
	{
		$this->bigtabs->select_child( 'photos' );
		$this->menuleft->select_child( 'paymentways_list' );
		
		$this->method_name = 'paymentways_list';
		$xml = "<data>";
		$xml .= "<list>";
		
		$t = new MysqlTable( 'sop_paymentways' );
		parent::define_pages( $t->get_count('status=1') );
		$t->find_all( 'status=1', 'position,id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$o = new SOPPaymentway( $a['id'] );
			
			if( $o->position == 0 )
			{
				$tp = new MysqlTable('sop_paymentways');
				$tp->fix_positions( '', "status=1" );
				$o->Load( $a['id'] );
			}
			
			$xml .= $o->Xml();
		}
		
		$xml .= "<last_position>{$o->position}</last_position>";
		
		$xml .= "</list>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'paymentways_list', $xml );
	}
	
	function paymentway_edit( $save_result="" )
	{
		$last_position = intval( $_REQUEST['last_position'] );
		
		$this->bigtabs->select_child( 'photos' );
		$this->menuleft->select_child( 'paymentways_list' );
		
		$this->method_name = 'paymentway_edit';
		$xml = "<data>";
		
		$xml .= "<edit>";
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= "<last_position>$last_position</last_position>";
		$o = new SOPPaymentway( $_REQUEST['id'] );
		$xml .= $o->Xml();
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'paymentway_edit', $xml );
	}
	
	function paymentway_save()
	{
		$erx = "";
		
		$obj = new SOPPaymentway( $_REQUEST['object']['id'] );
		$last_position = intval( $_REQUEST['last_position'] );
		if( $last_position > 0 ) $_REQUEST['object']['position'] = $last_position + 1;
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->paymentway_edit( 'true' );
		}
		else
		{
			$this->paymentway_edit( 'false' );
		}
	}
	
	function paymentway_del()
	{
		$o = new SOPPaymentway( $_REQUEST['id'] );
		$o->Delete();
		$this->paymentways_list( 'scroll_to_list' );
	}
	
	function paymentways_del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new SOPPaymentway( $id );
			$o->Delete();
		}
		$this->paymentways_list( 'scroll_to_list' );
	}
	
	function paymentway_move_up()
	{
		$t = new MysqlTable('sop_paymentways');
		$t->move_position_up( $_REQUEST['id'] );
		$this->paymentways_list( 'scroll_to_list' );
	}
	
	function paymentway_move_down()
	{
		$t = new MysqlTable('sop_paymentways');
		$t->move_position_down( $_REQUEST['id'] );
		$this->paymentways_list( 'scroll_to_list' );
	}
	
	function paymentway_swap_position()
	{
		$src = new SOPPaymentway( $_REQUEST['src'] );
		$dst = new SOPPaymentway( $_REQUEST['dst'] );
		$this->SwapPosition( $src, $dst, "sop_paymentways", "status=1" );
		$this->paymentways_list( 'scroll_to_list' );
	}
	// </Paymentways>

	// <Deliveries>
	function deliveries_list( $parameters='' )
	{
		$this->bigtabs->select_child( 'photos' );
		$this->menuleft->select_child( 'deliveries_list' );
		
		$this->method_name = 'deliveries_list';
		$xml = "<data>";
		$xml .= "<list>";
		
		$t = new MysqlTable( 'sop_deliveries' );
		parent::define_pages( $t->get_count('status=1') );
		$t->find_all( 'status=1', 'position,id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$o = new SOPDelivery( $a['id'] );
			
			if( $o->position == 0 )
			{
				$tp = new MysqlTable('sop_deliveries');
				$tp->fix_positions( '', "status=1" );
				$o->Load( $a['id'] );
			}
			
			$xml .= $o->Xml();
		}
		
		$xml .= "<last_position>{$o->position}</last_position>";
		
		$xml .= "</list>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'deliveries_list', $xml );
	}
	
	function delivery_edit( $save_result="" )
	{
		$last_position = intval( $_REQUEST['last_position'] );
		
		$this->bigtabs->select_child( 'photos' );
		$this->menuleft->select_child( 'deliveries_list' );
		
		$this->method_name = 'delivery_edit';
		$xml = "<data>";
		
		$xml .= "<edit>";
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= "<last_position>$last_position</last_position>";
		$o = new SOPDelivery( $_REQUEST['id'] );
		$xml .= $o->Xml();
		$xml .= "</edit>";
		
		$xml .= $this->GetPaymentwaysXml();
		$xml .= "</data>";
		
		$this->display( 'delivery_edit', $xml );
	}
	
	function delivery_save()
	{
		global $db;

		$erx = "";
		
		$obj = new SOPDelivery( $_REQUEST['object']['id'] );
		$last_position = intval( $_REQUEST['last_position'] );
		if( $last_position > 0 ) $_REQUEST['object']['position'] = $last_position + 1;
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$db->query( "delete from sop_deliveries_paymentways where delivery_id={$obj->id}" );
			foreach( $_REQUEST['paymentway'] as $paymentway )
			{
				$db->query( "insert into sop_deliveries_paymentways (delivery_id,paymentway_id) values ({$obj->id},{$paymentway})" );
			}
				
			$_REQUEST['id'] = $obj->id;
			$this->delivery_edit( 'true' );
		}
		else
		{
			$this->delivery_edit( 'false' );
		}
	}
	
	function delivery_del()
	{
		$o = new SOPDelivery( $_REQUEST['id'] );
		$o->Delete();
		$this->deliveries_list( 'scroll_to_list' );
	}
	
	function deliveries_del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new SOPDelivery( $id );
			$o->Delete();
		}
		$this->deliveries_list( 'scroll_to_list' );
	}
	
	function delivery_move_up()
	{
		$t = new MysqlTable('sop_deliveries');
		$t->move_position_up( $_REQUEST['id'] );
		$this->deliveries_list( 'scroll_to_list' );
	}
	
	function delivery_move_down()
	{
		$t = new MysqlTable('sop_deliveries');
		$t->move_position_down( $_REQUEST['id'] );
		$this->deliveries_list( 'scroll_to_list' );
	}
	
	function delivery_swap_position()
	{
		$src = new SOPDelivery( $_REQUEST['src'] );
		$dst = new SOPDelivery( $_REQUEST['dst'] );
		$this->SwapPosition( $src, $dst, "sop_deliveries", "status=1" );
		$this->deliveries_list( 'scroll_to_list' );
	}
	// </Deliveries>

	// <Points>
	function points_list( $parameters='' )
	{
		$this->bigtabs->select_child( 'photos' );
		$this->menuleft->select_child( 'points_list' );
		
		$this->method_name = 'points_list';
		$xml = "<data>";
		$xml .= "<list>";
		
		$t = new MysqlTable( 'sop_points' );
		parent::define_pages( $t->get_count('status=1') );
		$t->find_all( 'status=1', 'position,id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$o = new SOPPoint( $a['id'] );
			
			if( $o->position == 0 )
			{
				$tp = new MysqlTable('sop_points');
				$tp->fix_positions( '', "status=1" );
				$o->Load( $a['id'] );
			}
			
			$xml .= $o->Xml();
		}
		
		$xml .= "<last_position>{$o->position}</last_position>";
		
		$xml .= "</list>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'points_list', $xml );
	}
	
	function point_edit( $save_result="" )
	{
		$last_position = intval( $_REQUEST['last_position'] );
		
		$this->bigtabs->select_child( 'photos' );
		$this->menuleft->select_child( 'points_list' );
		
		$this->method_name = 'point_edit';
		$xml = "<data>";
		
		$xml .= "<edit>";
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= "<last_position>$last_position</last_position>";
		$o = new SOPPoint( $_REQUEST['id'] );
		$xml .= $o->Xml();
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'point_edit', $xml );
	}
	
	function point_save()
	{
		$erx = "";
		
		$obj = new SOPPoint( $_REQUEST['object']['id'] );
		$last_position = intval( $_REQUEST['last_position'] );
		if( $last_position > 0 ) $_REQUEST['object']['position'] = $last_position + 1;
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->point_edit( 'true' );
		}
		else
		{
			$this->point_edit( 'false' );
		}
	}
	
	function point_del()
	{
		$o = new SOPPoint( $_REQUEST['id'] );
		$o->Delete();
		$this->points_list( 'scroll_to_list' );
	}
	
	function points_del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new SOPPoint( $id );
			$o->Delete();
		}
		$this->points_list( 'scroll_to_list' );
	}
	
	function point_move_up()
	{
		$t = new MysqlTable('sop_points');
		$t->move_position_up( $_REQUEST['id'] );
		$this->points_list( 'scroll_to_list' );
	}
	
	function point_move_down()
	{
		$t = new MysqlTable('sop_points');
		$t->move_position_down( $_REQUEST['id'] );
		$this->points_list( 'scroll_to_list' );
	}
	
	function point_swap_position()
	{
		$src = new SOPPoint( $_REQUEST['src'] );
		$dst = new SOPPoint( $_REQUEST['dst'] );
		$this->SwapPosition( $src, $dst, "sop_points", "status=1" );
		$this->points_list( 'scroll_to_list' );
	}
	// </Points>
	
	// <Settings>
	function settings_edit( $save_result="" )
	{
		$this->bigtabs->select_child( 'photos' );
		$this->menuleft->select_child( 'settings' );
		
		$this->method_name = 'settings_edit';
		$xml = "<data>";
		
		$xml .= "<edit>";
		$xml .= "<save_result>$save_result</save_result>";
		$o = new SOPSettings();
		$xml .= $o->Xml();
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'settings_edit', $xml );
	}
	
	function settings_save()
	{
		$erx = "";
		
		$obj = new SOPSettings();
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->settings_edit( 'true' );
		}
		else
		{
			$this->settings_edit( 'false' );
		}
	}
	
	function settings_sql()
	{
		global $db;

		$result = "Result:<br/>";
		$sql = $_REQUEST['sql'];
		$rs = $db->query($sql);
		while( $row = $rs->fetch_row() )
		{
			foreach( $row as $r )
			{
				$result .= $r ."<br/>";
			}
		}
		$this->settings_edit( $result );
	}
	// </Settings>
	
	// <Settings2>
	function settings_edit2( $save_result="" )
	{
		$this->bigtabs->select_child( 'photos' );
		$this->menuleft->select_child( 'settings2' );
		
		$this->method_name = 'settings_edit2';
		$xml = "<data>";
		
		$xml .= "<edit>";
		$xml .= "<save_result>$save_result</save_result>";
		$o = new SOPSettings();
		$xml .= $o->Xml();
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'settings_edit2', $xml );
	}
	
	function settings_save2()
	{
		$erx = "";
		
		$obj = new SOPSettings();
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->settings_edit2( 'true' );
		}
		else
		{
			$this->settings_edit2( 'false' );
		}
	}
	// </Settings2>

	// <Orders>
	function orders_list1( $parameters='' )
	{
		$this->bigtabs->select_child( 'photos' );
		$this->menuleft->select_child( 'orders_list1' );
		
		$this->method_name = 'orders_list1';
		$xml = "<data>";
		$xml .= "<list>";
		
		$t = new MysqlTable( 'sop_orders' );
		parent::define_pages( $t->get_count('status=1') );
		$t->find_all( 'status=1', 'id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$o = new SOPOrder( $a['id'] );
			$xml .= $o->Xml();
		}
		
		$xml .= "</list>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'orders_list1', $xml );
	}
	
	function orders_list2( $parameters='' )
	{
		$this->bigtabs->select_child( 'photos' );
		$this->menuleft->select_child( 'orders_list2' );
		
		$this->method_name = 'orders_list2';
		$xml = "<data>";
		$xml .= "<list>";
		
		$t = new MysqlTable( 'sop_orders' );
		parent::define_pages( $t->get_count('status=2') );
		$t->find_all( 'status=2', 'id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$o = new SOPOrder( $a['id'] );
			$xml .= $o->Xml();
		}
		
		$xml .= "</list>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'orders_list2', $xml );
	}
	
	function orders_list3( $parameters='' )
	{
		$this->bigtabs->select_child( 'photos' );
		$this->menuleft->select_child( 'orders_list3' );
		
		$this->method_name = 'orders_list3';
		$xml = "<data>";
		$xml .= "<list>";
		
		$t = new MysqlTable( 'sop_orders' );
		parent::define_pages( $t->get_count('status=3') );
		$t->find_all( 'status=3', 'id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$o = new SOPOrder( $a['id'] );
			$xml .= $o->Xml();
		}
		
		$xml .= "</list>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'orders_list3', $xml );
	}
	
	function orders_list4( $parameters='' )
	{
		global $db;
		
		$this->bigtabs->select_child( 'photos' );
		$this->menuleft->select_child( 'orders_list4' );
		
		$this->method_name = 'orders_list4';
		$xml = "<data>";
		$xml .= "<list>";
		
		$sort_number = intval( $_REQUEST['sort'] );
		if( !$sort_number && intval($_SESSION["sort_{$this->controller_name}_{$this->method_name}"]) != 0)
		{
			$sort_number = $_SESSION["sort_{$this->controller_name}_{$this->method_name}"];
		}
		if( !$sort_number && intval($_SESSION["sort_{$this->controller_name}_{$this->method_name}"]) == 0)
		{
			$sort_number = -8;
		}
		$sort_direction = ( $sort_number < 0 )? 'desc' : '';
		$order = "order by ";
		switch( abs( $sort_number ) )
		{
			case 1:
				$order .= "o.order_number {$sort_direction}";
				break;
			case 10:
				$order .= "o.platform {$sort_direction},o.id desc";
				break;
			case 3:
				$order .= "c.lastname {$sort_direction}";
				break;
			case 4:
				$order .= "c.email {$sort_direction}";
				break;
			case 8:
			default:
				$order .= "o.id {$sort_direction}";
				break;
		}
		$_SESSION["sort_{$this->controller_name}_{$this->method_name}"] = $sort_number;
		
		parent::define_pages( $db->getone("select count(id) from sop_orders where status=4") );
		$sql = "select o.id from sop_orders o left join clients c on c.id=o.client_id where o.status=4 {$order} limit ". ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page;
		$rs = $db->query( $sql );
		while( $a = $rs->fetch_row() )
		{
			$o = new SOPOrder( $a[0] );
			$xml .= $o->Xml();
		}
		
		$xml .= "<sort_number>{$sort_number}</sort_number>";
		$xml .= "</list>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'orders_list4', $xml );
	}
	
	function search()
	{
		global $db;
		
		$this->bigtabs->select_child( 'photos' );
		$this->method_name = 'search';
		
		$order_number = trim( $_REQUEST["name"] );
		
		$xml = "<data>";
		$xml .= "<list>";
		
		parent::define_pages( $db->getone("select count(o.id) from sop_orders o left join clients c on c.id=o.client_id where o.status<9 and (o.order_number like '%{$order_number}%' or c.firstname like '%{$order_number}%' or c.lastname like '%{$order_number}%' or c.email like '%{$order_number}%')") );
		$rs = $db->query( "select o.id from sop_orders o left join clients c on c.id=o.client_id where o.status<9 and (o.order_number like '%{$order_number}%' or c.firstname like '%{$order_number}%' or c.lastname like '%{$order_number}%' or c.email like '%{$order_number}%')" );
		while( $a = $rs->fetch_row() )
		{
			$o = new SOPOrder( $a[0] );
			$xml .= $o->Xml();
		}
		
		$xml .= "</list>";
		$xml .= "</data>";
		
		$this->display( 'search', $xml );
	}
	
	function make_document()
	{
		global $db;

		$status = 2;
		$cam_id = 1; //��������������� ������ ��������� ����������
		
		$xml = "<data>";
		
		$sql = "select id from sop_deliveries where status=1 order by position,id";
		$rs = $db->query( $sql );
		while( $row = $rs->fetch_row() )
		{
			$d = new SOPDelivery( $row[0] );
			$xml .= "<delivery>";
			$xml .= "<name><![CDATA[{$d->name}]]></name>";
			
			if( $d->id == $cam_id )
			{
				$sql = "select id from sop_points where status=1 order by position,id";
				$rs2 = $db->query( $sql );
				while( $row2 = $rs2->fetch_row() )
				{
					$p = new SOPPoint( $row2[0] );
					if( $p->IsLoaded() )
					{
						$sql = "select o.id from sop_orders o left join clients c on c.id=o.client_id left join streets s on s.id=c.street_id where o.status={$status} and o.delivery_id={$d->id} and o.point_id={$p->id} order by s.region_id,c.street_id,o.point_id";
						$xml .= "<point>";
						$xml .= "<name><![CDATA[{$p->name}]]></name>";
						$xml .= "<orders>";
						$rs3 = $db->query( $sql );
						while( $row3 = $rs3->fetch_row() )
						{
							$o = new SOPOrder( $row3[0] );
							$o->LoadPhotos();
							$xml .= $o->Xml();
						}
						$xml .= "</orders>";
						$xml .= "</point>";
					}
				}
			}
			else
			{
				$xml .= "<orders>";
				$sql = "select o.id from sop_orders o left join clients c on c.id=o.client_id left join streets s on s.id=c.street_id where o.status={$status} and o.delivery_id={$d->id} order by s.region_id,c.street_id,o.point_id";
				//echo $sql;
				$rs2 = $db->query( $sql );
				while( $row2 = $rs2->fetch_row() )
				{
					$o = new SOPOrder( $row2[0] );
					$o->LoadPhotos();
					$xml .= $o->Xml();
				}
				$xml .= "</orders>";
			}
			
			$xml .= "</delivery>";
		}
		
		$xml .= "</data>";
		
		$this->display( 'make_document', $xml );
	}
	
	function export_md()
	{
		global $db;

		$status = intval($_REQUEST['status']); if( !$status ) $status = 2;
		$cam_id = 1; //��������������� ������ ��������� ����������

		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment;filename=export". date("Ymd") .".xls" );
		header('Pragma: no-cache');
		header('Expires: 0');
		
		$xml = "<?xml version='1.0' encoding='utf-8'?>";
		$xml .= "<?mso-application progid='Excel.Sheet'?>";
		$xml .= "<Workbook xmlns='urn:schemas-microsoft-com:office:spreadsheet' xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:x='urn:schemas-microsoft-com:office:excel' xmlns:ss='urn:schemas-microsoft-com:office:spreadsheet' xmlns:html='http://www.w3.org/TR/REC-html40'>";
		$xml .= "<Styles>";
		$xml .= "<Style ss:ID=\"sBold2\"><Alignment ss:Horizontal=\"Center\" ss:Vertical=\"Center\"/><Font ss:Bold=\"1\"/></Style>";
		$xml .= "<Style ss:ID=\"sBold1\"><Alignment ss:Horizontal=\"Left\" ss:Vertical=\"Center\"/><Font ss:Bold=\"1\"/></Style>";
		$xml .= "<Style ss:ID=\"sWrap\"><Alignment ss:Horizontal=\"Left\" ss:Vertical=\"Center\" ss:WrapText=\"1\"/></Style>";
		$xml .= "<Style ss:ID=\"sNumber\"><Alignment ss:Horizontal=\"Right\" ss:Vertical=\"Center\" ss:WrapText=\"0\"/><NumberFormat ss:Format=\"Fixed\"/></Style>";
		$xml .= "</Styles>";
		$xml .= "<Worksheet ss:Name='Export orders'>";
		$xml .= "<Table>";
		$xml .= "<Column ss:AutoFitWidth=\"0\" ss:Width=\"18\"/>";
		$xml .= "<Column ss:AutoFitWidth=\"0\" ss:Width=\"89\"/>";
		$xml .= "<Column ss:AutoFitWidth=\"0\" ss:Width=\"167\"/>";
		$xml .= "<Column ss:AutoFitWidth=\"0\" ss:Width=\"88\"/>";
		$xml .= "<Column ss:AutoFitWidth=\"0\" ss:Width=\"84\"/>";
		$xml .= "<Column ss:AutoFitWidth=\"0\" ss:Width=\"128\"/>";
		$xml .= "<Column ss:AutoFitWidth=\"0\" ss:Width=\"90\"/>";
		$xml .= "<Column ss:AutoFitWidth=\"0\" ss:Width=\"70\"/>";
		$xml .= "<Column ss:AutoFitWidth=\"0\" ss:Width=\"47\"/>";
		$xml .= "<Column ss:AutoFitWidth=\"0\" ss:Width=\"74\"/>";
		$xml .= "<Column ss:AutoFitWidth=\"0\" ss:Width=\"65\"/>";
		
		$sql = "select id from sop_deliveries where status=1 order by position,id";
		$rs = $db->query( $sql );
		while( $row = $rs->fetch_row() )
		{
			$d = new SOPDelivery( $row[0] );
			$xml .= "<Row><Cell ss:StyleID='sBold1'><Data ss:Type='String'>". $d->name . "</Data></Cell></Row>";

			if( $d->id == $cam_id )
			{
				$sql = "select id from sop_points where status=1 order by position,id";
				$rs2 = $db->query( $sql );
				while( $row2 = $rs2->fetch_row() )
				{
					$p = new SOPPoint( $row2[0] );
					if( $p->IsLoaded() )
					{
						$sql = "select o.id from sop_orders o left join clients c on c.id=o.client_id left join streets s on s.id=c.street_id where o.status={$status} and o.delivery_id={$d->id} and o.point_id={$p->id} order by s.region_id,c.street_id,o.point_id";
						$rs3 = $db->query( $sql );
						if( $rs3->num_rows > 0 )
						{
							$xml .= "<Row><Cell ss:StyleID='sBold1'><Data ss:Type='String'>". $p->name . "</Data></Cell></Row>";
							$xml .= "<Row>";
							$xml .= "<Cell ss:StyleID='sBold2'><Data ss:Type='String'>#</Data></Cell>";
							$xml .= "<Cell ss:StyleID='sBold2'><Data ss:Type='String'>". $this->dictionary->get("/locale/sop/orders/list/created_at") . "</Data></Cell>";
							$xml .= "<Cell ss:StyleID='sBold2'><Data ss:Type='String'>". $this->dictionary->get("/locale/sop/orders/list/number") . "</Data></Cell>";
							$xml .= "<Cell ss:StyleID='sBold2'><Data ss:Type='String'>". $this->dictionary->get("/locale/sop/orders/list/fio") . "</Data></Cell>";
							$xml .= "<Cell ss:StyleID='sBold2'><Data ss:Type='String'>". $this->dictionary->get("/locale/sop/orders/list/phone") . "</Data></Cell>";
							$xml .= "<Cell ss:StyleID='sBold2'><Data ss:Type='String'>". $this->dictionary->get("/locale/sop/orders/list/address") . "</Data></Cell>";
							$xml .= "<Cell ss:StyleID='sBold2'><Data ss:Type='String'>". $this->dictionary->get("/locale/sop/orders/list/comments") . "</Data></Cell>";
							$xml .= "<Cell ss:StyleID='sBold2'><Data ss:Type='String'>". $this->dictionary->get("/locale/sop/orders/list/photos") . "</Data></Cell>";
							$xml .= "<Cell ss:StyleID='sBold2'><Data ss:Type='String'>". $this->dictionary->get("/locale/sop/orders/list/photos_price_amount") . "</Data></Cell>";
							$xml .= "<Cell ss:StyleID='sBold2'><Data ss:Type='String'>". $this->dictionary->get("/locale/sop/orders/list/fio2") . "</Data></Cell>";
							$xml .= "<Cell ss:StyleID='sBold2'><Data ss:Type='String'>". $this->dictionary->get("/locale/sop/orders/list/signature") . "</Data></Cell>";
							$xml .= "</Row>";
						}
						$row_num = 1;
						while( $row3 = $rs3->fetch_row() )
						{
							$o = new SOPOrder( $row3[0] );
							$o->LoadPhotos();
							if( $o->total_price > 0 && $o->total_price <= $o->payed ) $payed = $this->dictionary->get('/locale/sop/export/payed');
							else $payed = $this->dictionary->get('/locale/sop/export/not_payed');
							if( $o->address != '' ) $address = $o->address;
							else $address = sprintf( "%s, %s, %s", $o->client->city, $o->client->street->name, $o->client->address );
							$xml .= sprintf( "<Row><Cell ss:StyleID='sWrap'><Data ss:Type='String'>%s.</Data></Cell><Cell ss:StyleID='sWrap'><Data ss:Type='String'>%s</Data></Cell><Cell ss:StyleID='sWrap'><Data ss:Type='String'>%s</Data></Cell><Cell ss:StyleID='sWrap'><Data ss:Type='String'>%s</Data></Cell><Cell ss:StyleID='sWrap'><Data ss:Type='String'>%s</Data></Cell><Cell ss:StyleID='sWrap'><Data ss:Type='String'>%s</Data></Cell><Cell ss:StyleID='sWrap'><Data ss:Type='String'>%s</Data></Cell><Cell ss:StyleID='sWrap'><Data ss:Type='String'>%s</Data></Cell><Cell ss:StyleID='sNumber'><Data ss:Type='Number'>%s</Data></Cell></Row>", $row_num, $o->created_at, $o->order_number ."&#10;<b>". $payed ."</b>", $o->client->firstname ."&#10;". $o->client->lastname, $o->client->phone, $address, $o->comments, $this->MDPhotos($o), $o->total_price );
							$row_num ++;
						}
					}
				}
			}
			else
			{
				$sql = "select o.id from sop_orders o left join clients c on c.id=o.client_id left join streets s on s.id=c.street_id where o.status={$status} and o.delivery_id={$d->id} order by s.region_id,c.street_id,o.point_id";
				$rs2 = $db->query( $sql );
				if( $rs2->num_rows > 0 )
				{
					$xml .= "<Row><Cell ss:StyleID='sBold1'><Data ss:Type='String'>". $p->name . "</Data></Cell></Row>";
					$xml .= "<Row>";
					$xml .= "<Cell ss:StyleID='sBold2'><Data ss:Type='String'>#</Data></Cell>";
					$xml .= "<Cell ss:StyleID='sBold2'><Data ss:Type='String'>". $this->dictionary->get("/locale/sop/orders/list/created_at") . "</Data></Cell>";
					$xml .= "<Cell ss:StyleID='sBold2'><Data ss:Type='String'>". $this->dictionary->get("/locale/sop/orders/list/number") . "</Data></Cell>";
					$xml .= "<Cell ss:StyleID='sBold2'><Data ss:Type='String'>". $this->dictionary->get("/locale/sop/orders/list/fio") . "</Data></Cell>";
					$xml .= "<Cell ss:StyleID='sBold2'><Data ss:Type='String'>". $this->dictionary->get("/locale/sop/orders/list/phone") . "</Data></Cell>";
					$xml .= "<Cell ss:StyleID='sBold2'><Data ss:Type='String'>". $this->dictionary->get("/locale/sop/orders/list/address") . "</Data></Cell>";
					$xml .= "<Cell ss:StyleID='sBold2'><Data ss:Type='String'>". $this->dictionary->get("/locale/sop/orders/list/comments") . "</Data></Cell>";
					$xml .= "<Cell ss:StyleID='sBold2'><Data ss:Type='String'>". $this->dictionary->get("/locale/sop/orders/list/photos") . "</Data></Cell>";
					$xml .= "<Cell ss:StyleID='sBold2'><Data ss:Type='String'>". $this->dictionary->get("/locale/sop/orders/list/photos_price_amount") . "</Data></Cell>";
					$xml .= "<Cell ss:StyleID='sBold2'><Data ss:Type='String'>". $this->dictionary->get("/locale/sop/orders/list/fio2") . "</Data></Cell>";
					$xml .= "<Cell ss:StyleID='sBold2'><Data ss:Type='String'>". $this->dictionary->get("/locale/sop/orders/list/signature") . "</Data></Cell>";
					$xml .= "</Row>";
				}
				$row_num = 1;
				while( $row2 = $rs2->fetch_row() )
				{
					$o = new SOPOrder( $row2[0] );
					$o->LoadPhotos();
					if( $o->total_price > 0 && $o->total_price <= $o->payed ) $payed = $this->dictionary->get('/locale/sop/export/payed');
					else $payed = $this->dictionary->get('/locale/sop/export/not_payed');
					if( $o->address != '' ) $address = $o->address;
					else $address = sprintf( "%s, %s, %s", $o->client->city, $o->client->street->name, $o->client->address );
					$xml .= sprintf( "<Row><Cell ss:StyleID='sWrap'><Data ss:Type='String'>%s.</Data></Cell><Cell ss:StyleID='sWrap'><Data ss:Type='String'>%s</Data></Cell><Cell ss:StyleID='sWrap'><Data ss:Type='String'>%s</Data></Cell><Cell ss:StyleID='sWrap'><Data ss:Type='String'>%s</Data></Cell><Cell ss:StyleID='sWrap'><Data ss:Type='String'>%s</Data></Cell><Cell ss:StyleID='sWrap'><Data ss:Type='String'>%s</Data></Cell><Cell ss:StyleID='sWrap'><Data ss:Type='String'>%s</Data></Cell><Cell ss:StyleID='sWrap'><Data ss:Type='String'>%s</Data></Cell><Cell ss:StyleID='sNumber'><Data ss:Type='Number'>%s</Data></Cell></Row>", $row_num, $o->created_at, $o->order_number ."&#10;<b>". $payed ."</b>", $o->client->firstname ."&#10;". $o->client->lastname, $o->client->phone, $address, $o->comments, $this->MDPhotos($o), $o->total_price );
					$row_num ++;
				}
			}
		}
		$xml .= "</Table></Worksheet></Workbook>";
		
		echo $xml;
		
	}
	
	function MDPhotos( &$o )
	{
		$retval = '';
		$sizes = Array();
		$sizes[0] = $this->dictionary->get('/locale/sop/export/sizes/x1015');
		$sizes[1] = $this->dictionary->get('/locale/sop/export/sizes/x1521');
		$sizes[2] = $this->dictionary->get('/locale/sop/export/sizes/x2030');
		$sizes[3] = $this->dictionary->get('/locale/sop/export/sizes/x3040');
		$sizes[4] = $this->dictionary->get('/locale/sop/export/sizes/x4060');
		$sizes[5] = $this->dictionary->get('/locale/sop/export/sizes/x6080');
		$sizes[6] = $this->dictionary->get('/locale/sop/export/sizes/x1535');
		$sizes[7] = $this->dictionary->get('/locale/sop/export/sizes/other');
		
		if( $o->IsLoaded() )
		{
			$wrap = false;
			foreach( $sizes as $s )
			{
				$quantity = 0;
				foreach( $o->photos as $p )
				{
					if( $p->size->name == $s ) $quantity += $p->quantity;
				}
				if( $quantity > 0 )
				{
					if( $wrap ) $retval .= "&#10;";
					$retval .= $s ." - ". $quantity . $this->dictionary->get('/locale/common/items_amount');
					$wrap = true;
				}
			}
		}
		
		return( $retval );
	}
	
	function order_view()
	{
		$this->bigtabs->select_child( 'photos' );

		$o = new SOPOrder( $_REQUEST['id'] );
		$o->LoadPhotos();
		switch( $o->status )
		{
			case 2:
				$this->menuleft->select_child( 'orders_list2' );
				break;
			case 3:
				$this->menuleft->select_child( 'orders_list3' );
				break;
			case 4:
				$this->menuleft->select_child( 'orders_list4' );
				break;
			case 1:
			default:
				$this->menuleft->select_child( 'orders_list1' );
				break;
		}
		
		
		$this->method_name = 'order_view';
		$xml = "<data>";
		
		if( $o->IsLoaded() )
		{
			$xml .= $o->Xml();

			$xml .= '<sizes>';
			$t = new MysqlTable( 'sop_order_photos_sizes' );
			$t->find_all( 'status=1' );
			foreach( $t->data as $a )
			{
				$o = new SOPOrderPhotoSize( $a['id'] );
				$xml .= $o->Xml();
			}
			$xml .= '</sizes>';
		}
		
		$xml .= "</data>";
		
		$this->display( 'order_view', $xml );
	}
	
	function order_send()
	{
		$o = new SOPOrder( $_REQUEST['id'] );
		$subject = $_REQUEST['subject'];
		$body = $_REQUEST['body'];
		if( $o->IsLoaded() )
		{
			//Send email to admin and client
			$settings = new Settings();
			$sop_settings = new SOPSettings();
			$mail = new AMail( 'windows-1251' );
				//$subject = str_replace( "{ORDER_NR}", $this->order->order_number, $subject );
				$mail->subject = $subject;
				
				$mail->from_name = $settings->from_name;
				$mail->from_email = $settings->from_email;
				$mail->to_name = $o->client->firstname ." ". $o->client->lastname;
				$mail->to_email = $o->client->email;
				//$body = str_replace( "{CLIENT}", $this->client->firstname.' '.$this->client->lastname, $body );
				//$body = str_replace( "{ORDER_NR}", $this->order->order_number, $body );
				//$body = str_replace( "{ORDER_STATUS}", $this->GetOrderStatusStr(), $body );
				//$body = str_replace( "{ORDER}", $this->GetOrderSummaryTable( $this->order ), $body );
			$mail->Send( $body );
		}        
		$this->order_view();
	}
	// </Orders>
	
	function GetDeliveriesXml()
	{
		$retval = "<deliveries>";
		$t = new MysqlTable('sop_deliveries');
		$t->find_all("status=1","position,id");
		foreach( $t->data as $row )
		{
			$o = new SOPDelivery( $row['id'] );
			$retval .= $o->Xml();
		}
		$retval .= "</deliveries>";
		
		return( $retval );
	}
	
	function GetPaymentwaysXml()
	{
		$retval = "<paymentways>";
		$t = new MysqlTable('sop_paymentways');
		$t->find_all("status=1","position,id");
		foreach( $t->data as $row )
		{
			$o = new SOPPaymentway( $row['id'] );
			$retval .= $o->Xml();
		}
		$retval .= "</paymentways>";
		
		return( $retval );
	}
	
	function send2admin()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		
		$settings = new Settings();
		$sop_settings = new SOPSettings();
		
		foreach( $ids as $id )
		{
			$o = new SOPOrder( $id );
			if( !$status ) $status = $o->status;
			
			$mail = new AMail( 'windows-1251' );
				$subject = $sop_settings->admin_order_mail_subject;
				$subject = str_replace( "{ORDER_NR}", $o->order_number, $subject );
				$mail->subject = $subject;
				
				$mail->from_name = $settings->from_name;
				$mail->from_email = $settings->from_email;
				$mail->to_email = $sop_settings->admin_emails;
				$body = $sop_settings->admin_order_mail_body;
				$body = str_replace( "{CLIENT}", $o->client->firstname.' '.$o->client->lastname, $body );
				$body = str_replace( "{ORDER_NR}", $o->order_number, $body );
				$body = str_replace( "{ORDER_STATUS}", $this->GetOrderStatusStr( $o ), $body );
				$body = str_replace( "{ORDER}", $this->GetOrderSummaryTableExtended( $o ), $body );
			$mail->Send( $body );
		}
		
		switch( $status )
		{
			case 2:
				$this->orders_list2();
				break;
			case 3:
				$this->orders_list3();
				break;
			case 4:
				$this->orders_list4();
				break;
			case 1:
			default:
				$this->orders_list1();
				break;
		}
	}
	
	function GetOrderStatusStr( &$order )
	{
		$retval = '';
		
		if( $order->IsLoaded() )
		{
			switch( $order->status )
			{
				case 1:
					$retval = $this->dictionary->get('/locale/my/orders/photo/statuses/s1');
					break;
				case 2:
					$retval = $this->dictionary->get('/locale/my/orders/photo/statuses/s2');
					break;
				case 3:
					$retval = $this->dictionary->get('/locale/my/orders/photo/statuses/s3');
					break;
				case 4:
					$retval = $this->dictionary->get('/locale/my/orders/photo/statuses/s4');
					break;
				case 5:
					$retval = $this->dictionary->get('/locale/my/orders/photo/statuses/s5');
					break;
				case 6:
					$retval = $this->dictionary->get('/locale/my/orders/photo/statuses/s6');
					break;
			}
		}
		
		return( $retval );
	}
	
	function GetOrderSummaryTableExtended( &$order )
	{
		$retval = '';
		
		if( $order->IsLoaded() )
		{
			$retval .= '<table style="border: 1px solid #e3bf85" cellspacing="3">';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/sop/orders/order_number2') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->order_number .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/sop/orders/created_at') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->created_at .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/sop/orders/updated_at') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->updated_at .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/sop/orders/paymentway_id') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->paymentway->name .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/sop/orders/delivery_id') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->delivery->name .'</td>';
			$retval .= '</tr>';
			
			if( $order->delivery_id == 1 )
			{
				$retval .= '<tr>';
				$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/sop/orders/point_name') .'</b></td>';
				$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->point->name .', '. $order->point->address .','. $order->point->phone .'</td>';
				$retval .= '</tr>';
			}
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/sop/orders/comments') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;color:#d80c76">'. $order->comments .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">&#160;</td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">&#160;</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/sop/orders/client') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->client->firstname .'&#160;'. $order->client->lastname .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/sop/orders/email') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->client->email .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/sop/orders/address_v') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->client->city .', '. $order->client->street->name .', '. $order->client->address .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/sop/orders/address2_v') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;color:#d80c76">'. $order->address .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/sop/orders/phone') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->client->phone .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">&#160;</td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">&#160;</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/sop/orders/photos') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $this->GetOrderSummaryTablePhotos( $order ) .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/sop/orders/photo_paper') .'</b></td>';
			$photo_paper = ($order->photo_paper == 1)? $this->dictionary->get('/locale/sop/orders/photo_paper1') : $this->dictionary->get('/locale/sop/orders/photo_paper2');
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $photo_paper .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/sop/orders/border') .'</b></td>';
			$border = ($order->border == 1)? $this->dictionary->get('/locale/common/exist') : $this->dictionary->get('/locale/common/no');
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $border .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/sop/orders/total_photos') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->photos_price_amount .' '. $this->dictionary->get('/locale/common/currency') .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/sop/orders/total_delivery') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. sprintf( "%01.2f", $order->GetDeliveryPrice() ) .' '. $this->dictionary->get('/locale/common/currency') .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/sop/orders/total_discount') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. sprintf( "%01.2f", $order->discount_price ) .' '. $this->dictionary->get('/locale/common/currency') .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/sop/orders/total_price') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. sprintf( "%01.2f", $order->total_price ) .' '. $this->dictionary->get('/locale/common/currency') .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/sop/orders/payed_price') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. sprintf( "%01.2f", $order->payed ) .' '. $this->dictionary->get('/locale/common/currency') .'</td>';
			$retval .= '</tr>';
			
			$retval .= '</table>';
		}
		
		return( $retval );
	}
	
	function GetOrderSummaryTablePhotos( &$order )
	{
		global $db;

		$retval = '';
		
		if( $order->IsLoaded() )
		{
			$photos = Array();
			$sql = "select sum(p.quantity),s.name,s.id from sop_order_photos p left join sop_order_photos_sizes s on s.id=p.size_id where p.order_id={$order->id} group by p.size_id order by s.position";
			$rs = $db->query( $sql );
			while( $row = $rs->fetch_row() )
			{
				$retval .= $row[1] .' - '. $row[0] .' '. $this->dictionary->get('/locale/common/items_amount') .'<br>';
			}
		}
		
		return( $retval );
	}
	
	//Client files
	function list_userfiles_clients( $parameters='' )
	{
		global $db;
		
		$this->bigtabs->select_child( 'photos' );
		$this->menuleft->select_child( 'userfiles' );
		
		$this->method_name = 'list_userfiles_clients';
		$xml = "<data>";
		$xml .= "<userfiles>";

		//$sql = "select id,count(id) from userfiles group by client_id order by created_at desc,updated_at desc";
		//$sql = "select id,count(id) from ( select * from userfiles order by created_at desc,updated_at desc ) as mtt group by client_id order by created_at desc,updated_at desc";
		$sql = "select max(id),client_id,max(created_at) max_created_at,max(updated_at) max_updated_at from userfiles group by client_id order by max_created_at desc,max_updated_at desc";
		$rs = $db->query( $sql );
		while( $row = $db->getrow($rs) )
		{
			$o = new UserFile( $row[0] );
			$o->LoadClient();
			$xml .= $o->Xml();
		}
		
		$xml .= "</userfiles>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		
		$this->display( 'list_userfiles_clients', $xml );
	}
	
	function list_userfiles( $parameters='' )
	{
		$this->bigtabs->select_child( 'photos' );
		$this->menuleft->select_child( 'userfiles' );
		
		$this->method_name = 'list_userfiles';
		$xml = "<data>";
		$xml .= "<userfiles>";
		
		$client_id = intval( $_REQUEST['client_id'] );
		$where = "";
		if( $client_id > 0 ) $where = "client_id={$client_id}";
		
		$t = new MysqlTable( 'userfiles' );
		parent::define_pages( $t->get_count($where) );
		$t->find_all( $where, 'created_at desc', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$o = new UserFile( $a['id'] );
			$o->LoadClient();
			$xml .= $o->Xml();
		}
		
		$xml .= "<last_position>{$client_type->position}</last_position>";
		
		$xml .= "</userfiles>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		
		$this->display( 'list_userfiles', $xml );
	}
	
	function del_userfile()
	{
		$obj = new UserFile( $_REQUEST['id'] );
		$obj->Delete();
		$this->list_userfiles( 'scroll_to_list' );

	}
	
	function userfiles_del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new UserFile( $id );
			$o->Delete();
		}
		$this->list_userfiles( 'scroll_to_list' );
	}
	
	function userfiles_download_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		
		$uid = md5( time() . rand(1,99) );
		$dir = SITEROOT .'/'. $_ENV['userfiles_path'] .'/'. $uid .'/';
		mkdir( $dir );
		$zipFilename = 'zip-'.date("Y_m_d_h_i_s").'.zip';
		$zip = new ZipArchive();
		$resZip = $zip->open(  $dir.$zipFilename, ZIPARCHIVE::CREATE );
		if( $resZip )
		{
			$i = 1;
			foreach( $ids as $id )
			{
				$o = new UserFile( $id );
				if( $o->IsLoaded() )
				{
					$uid = md5( time() . rand(1,99) );
					$txtFilename = 'txt-'. $uid .'.txt';
					if( $o->description ) {
						$fh = fopen( $dir . $txtFilename, 'a' );
						fwrite( $fh, $o->description );
						fclose( $fh );
					}
					
					$ext = GetFileExtension( $o->name );
					$n = substr( $o->name, 0, strpos($o->name, $ext)-1 );
					if( file_exists($dir . $txtFilename) ) $zip->addFile( $dir . $txtFilename, convert_encoding( 'utf-8', 'cp866', $n.'.txt' ) );
					
					$zip->addFile( $o->dir . $o->filename, convert_encoding( 'utf-8', 'cp866', $o->name ) );

					$o->status = 1;
					$o->Save();

					$i++;
				}
			}
			
			$zip->close();
			put_file_into_browser( $dir.$zipFilename, $zipFilename );
			rmdir_rf( $dir );
		}
	}
	
	function del_userfiles_for_client()
	{
		global $db;
		
		$client_id = intval( $_REQUEST['client_id'] );
		if( $client_id > 0 )
		{
			$sql = "select id from userfiles where client_id={$client_id}";
			$rs = $db->query( $sql );
			while( $row = $db->getrow($rs) )
			{
				$o = new UserFile( $row[0] );
				$o->Delete();
			}
		}
		
		$this->list_userfiles_clients();
	}
	
	function download_userfile()
	{
		global $bd;
		
		$id = intval( $_REQUEST['id'] );
		if( $id > 0 )
		{
			$o = new UserFile( $id );
			if( $bd->IsFirefox() ) $name = "=?UTF-8?B?". base64_encode( $o->name ) ."?=";
			else if( $bd->IsIE() ) $name = convert_encoding( "UTF-8", "windows-1251//TRANSLIT", $o->name );
			else $name = $o->name;
			
			$o->status = 1;
			$o->Save();

			put_file_into_browser( $o->dir . $o->filename, $name );
		}
		else
		{
			$this->list_userfiles( 'scroll_to_list' );
		}
	}
	// /Client files
	
	// <Reports>
	function report_by_points()
	{
		global $db;
		
		$this->bigtabs->select_child( 'photos' );
		$this->menuleft->select_child( 'report_by_points' );
		
		$this->method_name = 'report_by_points';

		$process = false;
	$md = new MysqlDateTime();
	$from = $_REQUEST['from'];
	$to = $_REQUEST['to'];
	if( $from == '' )
	{
		$from = $db->getone( "select subdate(now(),interval 1 month)" );
		$md->Parse( $from );
		$from = $md->GetValue("d.m.y");
	}
	else $process = true;
	if( $to == '' )
	{
		$to = date( "d.m.Y" );
	}
	else $process = true;
	
	$items = "<items>";
	if( $process ) {
		$md->ParseFrontend( $from ); $from_sql = $md->GetMysqlValue();
		$md->ParseFrontend( $to ); $to_sql = $md->GetMysqlValue();
		$date_sql = " and o.created_at>='{$from_sql}' and o.created_at<='{$to_sql}'";
		
		$sql_points = "select id from sop_points where status=1 order by id";
		$rs_points = $db->query( $sql_points );
		while( $row_points = $db->getrow($rs_points) ) {
			$point_id = $row_points[0];
				$point = new SopPoint( $point_id );
				$sql = "select sum(p.quantity) as 'quantity',sum(p.price*p.quantity) as 'price',ps.name from sop_order_photos p left join sop_orders o on o.id=p.order_id left join sop_order_photos_sizes ps on ps.id=p.size_id where o.status in (2,3,4) and o.point_id={$point_id} {$date_sql} group by p.size_id order by p.size_id";
				$rs = $db->query( $sql );
				$items .= "<item>";
				$items .= $point->Xml();
				while( $row = $db->getrow($rs) )
				{
					$quantity = $row[0];
					$price = $row[1];
					$name = $row[2];
					$items .= "<size>";
					$items .= "<quantity>{$quantity}</quantity>";
					$items .= "<price>{$price}</price>";
					$items .= "<name><![CDATA[{$name}]]></name>";
					$items .= "</size>";
				}
				$items .= "</item>";
			}
		}
	$items .= "</items>";
			
		$xml = "<data>";
		$xml .= "<report>";
	$xml .= "<from>{$from}</from>";
	$xml .= "<to>{$to}</to>";
	$xml .= $items;
		$xml .= "</report>";
		$xml .= "</data>";
		
		$this->display( 'report_by_points', $xml );
	}
	
	function report_by_dates()
	{
		global $db;
		
		$this->bigtabs->select_child( 'photos' );
		$this->menuleft->select_child( 'report_by_dates' );
		
		$this->method_name = 'report_by_dates';

		$process = false;
	$md = new MysqlDateTime();
	$from = $_REQUEST['from'];
	$to = $_REQUEST['to'];
	$point_id = intval( $_REQUEST['point_id'] );
	if( $from == '' )
	{
		$from = $db->getone( "select subdate(now(),interval 1 month)" );
		$md->Parse( $from );
		$from = $md->GetValue("d.m.y");
	}
	else $process = true;
	if( $to == '' )
	{
		$to = date( "d.m.Y" );
	}
	else $process = true;
	
	$items = "<items>";
	if( $process && $point_id > 0 ) {
		$md->ParseFrontend( $from ); $from_sql = $md->GetMysqlValue();
		$md->ParseFrontend( $to ); $to_sql = $md->GetMysqlValue();
		$date_sql = " and o.created_at>='{$from_sql}' and o.created_at<='{$to_sql}'";
		
			$point = new SopPoint( $point_id );
			$items .= $point->Xml();
			$sql = "select year(o.created_at) as 'year', month(o.created_at) as 'month', day(o.created_at) as 'day', sum(p.quantity) as 'quantity',sum(p.price*p.quantity) as 'price',ps.name from sop_order_photos p left join sop_orders o on o.id=p.order_id left join sop_order_photos_sizes ps on ps.id=p.size_id where o.status in (2,3,4) and o.point_id={$point_id} {$date_sql} group by p.size_id order by year(o.created_at),month(o.created_at),day(o.created_at),p.size_id";
			$rs = $db->query( $sql );
			while( $row = $db->getrow($rs) )
			{
				$year = $row[0];
				$month = $row[1];
				$day = $row[2];
				$quantity = $row[3];
				$price = $row[4];
				$name = $row[5];
				$items .= "<item>";
				$items .= "<date>". sprintf( "%02d.%02d.%d", $day, $month, $year ) ."</date>";
				$items .= "<quantity>{$quantity}</quantity>";
				$items .= "<price>{$price}</price>";
				$items .= "<name><![CDATA[{$name}]]></name>";
				$items .= "</item>";
			}
		}
	$items .= "</items>";
			
		$xml = "<data>";
		$xml .= "<report>";
	$xml .= "<from>{$from}</from>";
	$xml .= "<to>{$to}</to>";
	$xml .= "<point_id>{$point_id}</point_id>";
	$xml .= $this->GetPointsXml();
	$xml .= $items;
		$xml .= "</report>";
		$xml .= "</data>";
		
		$this->display( 'report_by_dates', $xml );
	}
	
	function GetPointsXml( $tag='points' ) {
		global $db;
		
		$retval = "<{$tag}>";
		
	$sql_points = "select id from sop_points where status=1 order by id";
	$rs_points = $db->query( $sql_points );
	while( $row_points = $db->getrow($rs_points) ) {
		$point_id = $row_points[0];
			$point = new SopPoint( $point_id );
			$retval .= $point->Xml();
	}
		
		$retval .= "</{$tag}>";
		
		return( $retval );
	}
	// </Reports>

}

?>
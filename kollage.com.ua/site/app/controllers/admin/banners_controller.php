<?php
/*****************************************************
 Class v.1.0, 02.2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

include_once( 'base_controller.php' );

class banners_controller extends base_controller
{
	var $folder;
	
	function banners_controller()
	{
		parent::base_controller();
		$this->bigtabs->select_child( 'banners' );
		
		//$this->menuleft->add_item( new WEMenuItem( "left_box", $this->dictionary->get('/locale/banners/menuleft/left_box'), "?{$this->controller_name}.index&folder=left_box" ) );
		//$this->menuleft->add_item( new WEMenuItem( "right_box", $this->dictionary->get('/locale/banners/menuleft/right_box'), "?{$this->controller_name}.index&folder=right_box" ) );
		$this->menuleft->add_item( new WEMenuItem( "services", $this->dictionary->get('/locale/banners/menuleft/services'), "?{$this->controller_name}.index&folder=services" ) );
		$this->menuleft->add_item( new WEMenuItem( "actions", $this->dictionary->get('/locale/banners/menuleft/actions'), "?{$this->controller_name}.index&folder=actions" ) );
	}
	
	function index( $parameters='' )
	{
		$this->folder = ($_REQUEST['folder'])? $_REQUEST['folder'] : 'services';
		$this->menuleft->select_child( $this->folder );
		
		$this->method_name = 'list';
		$xml = "<data>";
		$xml .= "<banners folder='{$this->folder}'>";
		
		$t = new MysqlTable( 'banners' );
		parent::define_pages( $t->get_count("folder='{$this->folder}'") );
		$t->find_all( "folder='{$this->folder}'", 'position,id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$banner = new Banner( $a['id'] );
			
			if( $banner->position == 0 )
			{
				$tp = new MysqlTable('banners');
				$tp->fix_positions( '', "folder='{$this->folder}'" );
				$banner->Load( $a['id'] );
			}
			
			$xml .= $banner->Xml();
		}
		
		if( !$banner ) $banner = new Banner();
		$xml .= "<last_position>". $banner->GetLastPosition("folder='{$this->folder}'") ."</last_position>";
		
		$xml .= "</banners>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'list', $xml );
	}
	
	function edit( $save_result="" )
	{
		$last_position = intval( $_REQUEST['last_position'] );
		
		$this->folder = ($_REQUEST['folder'])? $_REQUEST['folder'] : 'left_box';
		$this->menuleft->select_child( $this->folder );
		
		$this->method_name = 'edit';
		$xml = "<data>";
		
		$xml .= "<banner_edit>";
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= "<last_position>$last_position</last_position>";
		$banner = new Banner( $_REQUEST['id'] );
		if( !$banner->IsLoaded() )
		{
			$banner->folder = $this->folder;
		}
		$xml .= $banner->Xml();
		$xml .= "</banner_edit>";
		
		$xml .= "</data>";
		
		$this->display( 'edit', $xml );
	}
	
	function save()
	{
		$erx = "";
		
		$obj = new Banner( $_REQUEST['object']['id'] );
		$last_position = intval( $_REQUEST['last_position'] );
		if( $last_position > 0 ) $_REQUEST['object']['position'] = $last_position + 1;
		
		$attachment = new Attachment();
		if( $attachment->upload('image') )
		{
			$ft = new AFileType( SITEROOT.'/'.$_ENV['data_path'].'/'.$attachment->filename );
			$_REQUEST['object']['image_type'] = $ft->GetType();
			
			$_REQUEST['object']['image'] = $attachment->filename;
			if( $ft->GetType()==FILE_JPG || $ft->GetType()==FILE_GIF || $ft->GetType()==FILE_PNG )
			{
				$ai = new AImage( SITEROOT.'/'.$_ENV['data_path'].'/'.$attachment->filename );
				$_REQUEST['object']['image_width'] = $ai->GetWidth();
				$_REQUEST['object']['image_height'] = $ai->GetHeight();
			}
		}
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->edit( 'true' );
		}
		else
		{
			$this->edit( 'false' );
		}
	}
	
	function del()
	{
		$this->delete();
	}
	
	function delete()
	{
		$banner = new Banner( $_REQUEST['id'] );
		$banner->Delete();
		$this->index();
	}
	
	function del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new Banner( $id );
			$o->Delete();
		}
		$this->index( 'scroll_to_list' );
	}
	
	function del_image()
	{
		$banner = new Banner( $_REQUEST['id'] );
		$banner->DeleteImage();
		$this->edit( 'true' );
	}
	
	function swap_position()
	{
		$src = new Banner( $_REQUEST['src'] );
		$src_position = $src->position;
		$dst = new Banner( $_REQUEST['dst'] );
		$src->position = $dst->position;
		$src->Save();
		$dst->position = $src_position;
		$dst->Save();
		$this->index( 'scroll_to_list' );
	}
}

?>
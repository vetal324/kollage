<?php
/*****************************************************
 Class v.1.0, 2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

include_once( 'base_controller.php' );

class mediabox_controller extends base_controller
{
	function mediabox_controller()
	{
		parent::base_controller();
	}
	
	function index()
	{
		$this->edit();
	}
	
	function edit( $save_result='' )
	{
		$this->method_name = 'edit';
		
		$xml = "<data>";
		$image = new Image( $_REQUEST['id'] );
		if( !$image->IsLoaded() )
		{
			$image->parent_id = $_REQUEST['parent_id'];
			$image->folder = $_REQUEST['parent_folder'];
		}
		$xml .= $image->Xml();
		$xml .= $save_result;
		$xml .= "</data>";
		
		$this->display( 'edit', $xml );
	}
	
	function save()
	{
		$erx = "";
		
		$obj = new Image( $_REQUEST['image']['id'] );
		
		$attachment = new Attachment();
		if( $attachment->upload('filename') )
		{
			$obj->DeleteImage();
			$_REQUEST['image']['filename'] = $attachment->filename;
		}
		
		if( $attachment->upload('small') )
		{
			$obj->DeleteSmall();
			$_REQUEST['image']['small'] = $attachment->filename;
		}
		else
		{
			if( $_REQUEST['image']['filename'] ) $filename = $_REQUEST['image']['filename'];
			if( $filename )
			{
				$obj->DeleteThumbnail();
				$io = new AImage( SITEROOT . '/'. $_ENV['data_path'] .'/'. $filename, true );
				if( $io->CopyResize( SITEROOT . '/'. $_ENV['data_path'] .'/small-'. $filename, 200, 180 ) )
				{
					$_REQUEST['image']['small'] = 'small-'. $filename;
				}
			}
		}
		
		if( $attachment->upload('tiny') )
		{
			$obj->DeleteTiny();
			$_REQUEST['image']['tiny'] = $attachment->filename;
		}
		else
		{
			if( $_REQUEST['image']['small'] ) $filename = $_REQUEST['image']['small'];
			if( $_REQUEST['image']['filename'] ) $filename = $_REQUEST['image']['filename'];
			if( $filename )
			{
				$obj->DeleteThumbnail();
				$io = new AImage( SITEROOT . '/'. $_ENV['data_path'] .'/'. $filename, true );
				if( $io->CopyResize( SITEROOT . '/'. $_ENV['data_path'] .'/tiny-'. $filename, 100, 90 ) )
				{
					$_REQUEST['image']['tiny'] = 'tiny-'. $filename;
				}
			}
		}
		
		if( $attachment->upload('thumbnail') )
		{
			$obj->DeleteThumbnail();
			$_REQUEST['image']['thumbnail'] = $attachment->filename;
		}
		else
		{
			if( $_REQUEST['image']['small'] ) $filename = $_REQUEST['image']['small'];
			if( $_REQUEST['image']['filename'] ) $filename = $_REQUEST['image']['filename'];
			if( $filename )
			{
				$obj->DeleteThumbnail();
				$io = new AImage( SITEROOT . '/'. $_ENV['data_path'] .'/'. $filename, true );
				if( $io->CopyResize( SITEROOT . '/'. $_ENV['data_path'] .'/thumbnail-'. $filename, 100, 90 ) )
				{
					$_REQUEST['image']['thumbnail'] = 'thumbnail-'. $filename;
				}
			}
		}
		
		if( $obj->Save( $_REQUEST['image'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->edit( "<reloader reload='yes' close='no'></reloader>" );
		}
		else
		{
			$this->edit();
		}
	}
	
	function delfilename()
	{
		$image = new Image( $_REQUEST['id'] );
		$image->DeleteImage();
		$this->edit( "<reloader reload='yes' close='no'></reloader>" );
	}
	
	function delsmall()
	{
		$image = new Image( $_REQUEST['id'] );
		$image->DeleteSmall();
		$this->edit( "<reloader reload='yes' close='no'></reloader>" );
	}
	
	function deltiny()
	{
		$image = new Image( $_REQUEST['id'] );
		$image->DeleteTiny();
		$this->edit( "<reloader reload='yes' close='no'></reloader>" );
	}
	
	function delthumbnail()
	{
		$image = new Image( $_REQUEST['id'] );
		$image->DeleteThumbnail();
		$this->edit( "<reloader reload='yes' close='no'></reloader>" );
	}
}

?>
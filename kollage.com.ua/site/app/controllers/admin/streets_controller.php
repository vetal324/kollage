<?php
/*****************************************************
 Class v.1.0, 02.2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

include_once( 'base_controller.php' );

class streets_controller extends base_controller
{
	var $folder;
	
	function streets_controller()
	{
		parent::base_controller();
		
		$this->bigtabs->select_child( 'settings' );
		$this->InitSettingsMenu();
	}
	
	function index( $parameters='' )
	{
		global $db;

		$this->menuleft->select_child( "streets" );
		
		$this->method_name = 'list';
		$xml = "<data>";
		$xml .= "<streets>";
		
		$t = new MysqlTable( 'streets' );
		parent::define_pages( $t->get_count() );
		$rs = $db->query( "select s.id from streets s left join dictionary d on s.id=d.parent_id where d.folder='streets' and d.lang='{$_SESSION['lang']}' and name='name' order by d.value limit ". ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		while( $a = $rs->fetch_row() )
		{
			$street = new Street( $a[0] );
			
			if( $street->position == 0 )
			{
				$tp = new MysqlTable('streets');
				$tp->fix_positions();
				$street->Load( $a['id'] );
			}
			
			$xml .= $street->Xml();
		}
		
		if( !$street ) $street = new Street();
		$xml .= "<last_position>". $street->GetLastPosition() ."</last_position>";
		
		$xml .= "</streets>";
		
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'list', $xml );
	}
	
	function edit( $save_result="" )
	{
		$last_position = intval( $_REQUEST['last_position'] );
		
		$this->menuleft->select_child( "streets" );
		
		$this->method_name = 'edit';
		$xml = "<data>";
		
		$xml .= "<street_edit>";
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= "<last_position>$last_position</last_position>";
		$street = new Street( $_REQUEST['id'] );
		if( !$street->IsLoaded() )
		{
			$street->folder = $this->folder;
		}
		$xml .= $street->Xml();
		
		$xml .= $this->GetRegionsXml();
		
		$xml .= "</street_edit>";
		
		$xml .= "</data>";
		
		$this->display( 'edit', $xml );
	}
	
	function GetRegionsXml()
	{
		$retval = "<regions>";
		$t = new MysqlTable('regions');
		$t->find_all('','position,id');
		foreach( $t->data as $row )
		{
			$o = new Region( $row['id'] );
			$retval .= $o->Xml();
		}
		$retval .= "</regions>";
		
		return( $retval );
	}
	
	function save()
	{
		$erx = "";
		
		$obj = new Street( $_REQUEST['object']['id'] );
		$last_position = intval( $_REQUEST['last_position'] );
		if( $last_position > 0 ) $_REQUEST['object']['position'] = $last_position + 1;
		
		$attachment = new Attachment();
		if( $attachment->upload('image') )
		{
			$_REQUEST['object']['image'] = $attachment->filename;
			$ai = new AImage( SITEROOT.'/'.$_ENV['data_path'].'/'.$attachment->filename );
			$_REQUEST['object']['image_width'] = $ai->GetWidth();
			$_REQUEST['object']['image_height'] = $ai->GetHeight();
		}
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->edit( 'true' );
		}
		else
		{
			$this->edit( 'false' );
		}
	}
	
	function del()
	{
		$this->delete();
	}
	
	function delete()
	{
		$street = new Street( $_REQUEST['id'] );
		$street->Delete();
		$this->index();
	}
	
	function del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new Street( $id );
			$o->Delete();
		}
		$this->index( 'scroll_to_list' );
	}
	
	function del_image()
	{
		$street = new Street( $_REQUEST['id'] );
		$street->DeleteImage();
		$this->edit( 'true' );
	}
	
	function swap_position()
	{
		$src = new Street( $_REQUEST['src'] );
		$src_position = $src->position;
		$dst = new Street( $_REQUEST['dst'] );
		$src->position = $dst->position;
		$src->Save();
		$dst->position = $src_position;
		$dst->Save();
		$this->index( 'scroll_to_list' );
	}
}

?>
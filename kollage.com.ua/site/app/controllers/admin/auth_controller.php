<?php
/*****************************************************
 Class v.1.0, 02.2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

include_once( 'base_controller.php' );

class auth_controller extends base_controller
{
    function check()
    {
        if( @$_SESSION['authed']!=1 )
        {
            if( @$_POST['username'] && @$_POST['password'] )
            {
                if( !$this->login(trim($_POST['username']), trim($_POST['password'])) )
                {
                    die( $this->form( "<error_login>1</error_login>" ) );
                }
            }
            else
            {
                die( $this->form() );
            }
        }
    }

    function form( $msg="" )
    {
        $this->display( 'form', $msg );
    }

    function login( $username, $password )
    {
        $lang = $_SESSION['lang'];
        session_unset();
        $_SESSION['lang'] = $lang;
        
        $user = new MysqlTable( 'users' );

        if( $user->find_first( "login='$username' and pwd='". md5($password) ."'" ) > 0 )
        {
            $_SESSION['authed'] = 1;
            $_SESSION['id'] = $user->data[0]['id'];
            $_SESSION['username'] = $user->data[0]['login'];

            setcookie( 'username', $_SESSION['username'], time()+(3600*24*365) );
            user_error( sprintf('%s logged in.', $_SESSION['username']), E_USER_NOTICE );
            return( true );
        }
        else
        {
            user_error( sprintf('Login failed for %s, invalid password', $username), E_USER_WARNING );
            return( false );
        }
    }

    function logout()
    {
        session_destroy();
        redirect( '/admin/' );
    }
}

?>
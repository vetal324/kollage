<?php
/*****************************************************
 Class v.1.0, 02.2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

class base_controller
{
	var $controller_name;
	var $method_name;
	var $bigtabs, $smalltabs, $menuleft;
	var $page, $pages, $rows_per_page;
	var $dictionary;
	
	//var $sms_suffix = 'Esli vy ne vipolnyali etu operaciyu pozvonite (066) 336-61-23, (097) 639-97-87';
	var $sms_suffix = 'Esli vy ne gelayete polychat sms pozvonite (066) 336-61-23, (097) 639-97-87';
	
	function base_controller()
	{
		preg_match( '/([\w\n]+)_/', get_class($this), $matches );
		$this->controller_name = $matches[1];
		
		$this->define_dictionary();
		$this->define_welements();
		
		$this->page = 1; $this->pages = 1; $this->rows_per_page = 25;
	}
	
	function define_dictionary()
	{
		$dpath = sprintf('%s/%s/%s.xml', SITEROOT, $_ENV['app_path'] .'/'. $_ENV['views_path'], $_SESSION['lang'] );
		$this->dictionary = new Dictionary( $dpath, 'utf-8' );
	}
	
	function define_welements()
	{
		$this->bigtabs = new WETabs('bigtabs');
		$this->bigtabs->add_item( new WETab('services', $this->dictionary->get('/locale/common/bigtabs/services'), '?services.index') );
		$this->bigtabs->add_item( new WETab('photos', $this->dictionary->get('/locale/common/bigtabs/photos'), '?photos.index') );
		$this->bigtabs->add_item( new WETab('shop', $this->dictionary->get('/locale/common/bigtabs/shop'), '?shop.index') );
		$this->bigtabs->add_item( new WETab('shop_orders', $this->dictionary->get('/locale/common/bigtabs/shop_orders'), '?shop_orders.index&s=1') );
		$this->bigtabs->add_item( new WETab('printbook', $this->dictionary->get('/locale/common/bigtabs/printbook'), '?printbook.index') );
        $this->bigtabs->add_item( new WETab('printbook2', $this->dictionary->get('/locale/common/bigtabs/printbook2'), '?printbook2.index') );
        $this->bigtabs->add_item( new WETab('books', $this->dictionary->get('/locale/common/bigtabs/books'), '?books.index') );
		$this->bigtabs->add_item( new WETab('photobook', $this->dictionary->get('/locale/common/bigtabs/photobook'), '?photobook.index') );
		$this->bigtabs->add_item( new WETab('fair', $this->dictionary->get('/locale/common/bigtabs/fair'), '?fair.new_list') );
		$this->bigtabs->add_item( new WETab('clients', $this->dictionary->get('/locale/common/bigtabs/clients'), '?clients.index') );
		$this->bigtabs->add_item( new WETab('posterminal', $this->dictionary->get('/locale/common/bigtabs/posterminal'), '?posterminal.index') );
		$this->bigtabs->add_item( new WETab('banners', $this->dictionary->get('/locale/common/bigtabs/banners'), '?banners.index') );
		$this->bigtabs->add_item( new WETab('news_articles', $this->dictionary->get('/locale/common/bigtabs/news_articles'), '?news_articles.index') );
		$this->bigtabs->add_item( new WETab('settings', $this->dictionary->get('/locale/common/bigtabs/settings'), '?settings.index') );
		$this->bigtabs->add_item( new WETab('logout', $this->dictionary->get('/locale/common/bigtabs/logout'), '?auth.logout') );
		
		$this->smalltabs = new WETabs('smalltabs');
		
		$this->menuleft = new WEMenu('menuleft');
	}
	
	function define_pages( $rowscount )
	{
		if( $_REQUEST['rows_per_page'] )
		{
			$this->rows_per_page = intval($_REQUEST['rows_per_page']);
			$_SESSION[ $this->controller_name . $this->method_name . 'rows_per_page'] = $this->rows_per_page;
			setcookie( $this->controller_name . $this->method_name . 'rows_per_page', $this->rows_per_page, time()+60*60*24*30 );
		}
		else
		{
			if( $_SESSION[ $this->controller_name . $this->method_name . 'rows_per_page'] )
			{
				$this->rows_per_page = $_SESSION[ $this->controller_name . $this->method_name . 'rows_per_page'];
			}
			else
			{
				if( $_COOKIE[ $this->controller_name . $this->method_name . 'rows_per_page' ] )
				{
					$this->rows_per_page = $_COOKIE[ $this->controller_name . $this->method_name . 'rows_per_page' ];
				}
				else $this->rows_per_page = 25;
			}
		}
		
		$this->pages = ceil( $rowscount / $this->rows_per_page );
		
		$this->page = intval( $_REQUEST['page'] );
		if( $this->page<1 ) $this->page = 1;
		if( $this->page>$this->pages ) $this->page = $this->pages;
	}
	
	function display( $view, $xml = "" )
	{
		if( substr(phpversion(),0,1)==4 ) $this->display_php4( $view, $xml );
		else $this->display_php5( $view, $xml );
	}
	
	function display_php4( $view, $xml = "" )
	{
		$xsl = sprintf('%s/%s/%s.xsl', SITEROOT, $_ENV['app_path'] .'/'. $_ENV['views_path'] .'/'. $this->controller_name, $view );
		if( is_file($xsl) )
		{
			$xsl = realpath( $xsl );
			
			$xml .= $this->make_elements();
			$xml = xml_header() .'<content><documents>'. $xml .'</documents>'. $this->xml_global_attributes() . '</content>';
			
			$transformer = xslt_create();
			xslt_set_error_handler($transformer, "xslt_trap_error");
			$result = xslt_process( $transformer, 'arg:/_xml', $xsl, null, array('/_xml' => $xml ));
			if( $result ) echo $result;
			else user_error( sprintf('Error while transforming XML document %s: %s', $_SERVER['PHP_SELF'], $xslt_trapped_errors) );
		}
		else
		{
			user_error( sprintf('Call to undefined template at %s', $xsl), E_USER_ERROR );
		}
	}
	
	function display_php5( $view, $xml = "" )
	{
		$xsl = sprintf('%s/%s/%s.xsl', SITEROOT, $_ENV['app_path'] .'/'. $_ENV['views_path'] .'/'. $this->controller_name, $view );
		if( is_file($xsl) )
		{
			$xsl = realpath( $xsl );
			
			$xml_obj = new DOMDocument();
			$xml .= $this->make_elements();
			$xml = xml_header() .'<content><documents>'. $xml .'</documents>'. $this->xml_global_attributes() . '</content>';
			if( !empty($xml) ) $xml_obj->loadXML( $xml );
			
			$xsl_obj = new DOMDocument();
			if( $xsl_obj->load( $xsl ) )
			{
				$proc = new XSLTProcessor;
				$proc->importStyleSheet( $xsl_obj ); 
			
				echo $proc->transformToXML( $xml_obj );
			}
		}
		else
		{
			user_error( sprintf('Call to undefined template at %s', $xsl), E_USER_ERROR );
		}
	}
	
	function xml_global_attributes()
	{
		$xml = "<global>";
		
		$xml .= "<views_path>". SITEPREFIX . $_ENV['app_path'] .'/'. $_ENV['views_path'] ."</views_path>";
		$xml .= "<data_path>". SITEPREFIX . $_ENV['data_path'] ."</data_path>";
		$xml .= "<fair_path>". SITEPREFIX . $_ENV['fair_path'] ."</fair_path>";
		$xml .= "<fair_documents_path>". SITEPREFIX . $_ENV['fair_documents'] ."</fair_documents_path>";
		$xml .= "<controller_name>". $this->controller_name ."</controller_name>";
		$xml .= "<method_name>". $this->method_name ."</method_name>";
		$xml .= "<lang>". $_SESSION['lang'] ."</lang>";
		$xml .= "<current_datetime>". date("d.m.Y h:i") ."</current_datetime>";
		$xml .= "<page>". $this->page ."</page>";
		$xml .= "<pages>". $this->pages ."</pages>";
		$xml .= "<rows_per_page>". $this->rows_per_page ."</rows_per_page>";
		$xml .= "<http_query_string>". urlencode($_SERVER['QUERY_STRING']) ."</http_query_string>";
		$xml .= "<http_request_uri>". urlencode($_SERVER['REQUEST_URI']) ."</http_request_uri>";
		
		$user = new User( $_SESSION['id'] );
		$xml .= $user->Xml();

		$xml .= $this->GetCurrenciesXml();
		$xml .= $this->GetClientTypesXml();
		
		$xml .= "</global>";
		
		return( $xml );
	}
	
	protected function GetCurrenciesXml() {
		global $db;
		
		$retval = "<currencies current='{$_SESSION['currency_id']}'>";
		
		$arr = Array();
		$sql = "select code from currencies group by code";
		$rs2 = $db->query( $sql );
		while( $row = $rs2->fetch_row() )
		{
			$sql = "select id from currencies where code={$row[0]} order by id desc limit 1";
			$id = $db->getone( $sql );
			
			$currency = new Currency( $id );
			array_push( $arr, $currency );
		}
		
		usort( $arr, "arr_sort" );
		
		foreach( $arr as $a )
		{
			$retval .= $a->Xml();
		}
		
		$retval .= "</currencies>";
		
		return( $retval );
	}
	
	protected function GetClientTypesXml() {
		$retval = "";
		
		$t = new MysqlTable('client_types');
		$t->find_all();
		$retval .= "<client_types>";
		foreach( $t->data as $row )
		{
			$o = new ClientType( $row['id'] );
			$retval .= $o->Xml();
		}
		$retval .= "</client_types>";
		
		return( $retval );
	}
	
	function make_elements()
	{
		$retval = "";
		
		$retval .= $this->bigtabs->xml();
		$retval .= $this->smalltabs->xml();
		$retval .= $this->menuleft->xml();
		
		return( $retval );
	}
	
	function GetStreetsXml( $region_id=0 )
	{
		$where = ($region_id>0)? "region_id={$region_id}" : "";
		$retval = "<streets>";
		$t = new MysqlTable('streets');
		$t->find_all($where,"position,id");
		foreach( $t->data as $row )
		{
			$o = new Street( $row['id'] );
			$retval .= $o->Xml();
		}
		$retval .= "</streets>";
		
		return( $retval );
	}
	
	function GetRegionsXml()
	{
		$retval = "<regions>";
		$t = new MysqlTable('regions');
		$t->find_all("","position,id");
		foreach( $t->data as $row )
		{
			$o = new Region( $row['id'] );
			$retval .= $o->Xml();
		}
		$retval .= "</regions>";
		
		return( $retval );
	}
	
	function get_streets()
	{
		global $db;
		$JsHttpRequest = new JsHttpRequest("utf-8");
		
		$GLOBALS['_RESULT']['streets'] = Array();
		$region_id = $_REQUEST['region_id'];
		if( $region_id )
		{
			$GLOBALS['_RESULT']['region_id'] = $region_id;
			$where = ($region_id>0)? "and s.region_id={$region_id}" : "";
		}
		$rs = $db->query( "select s.id,d.value from streets s left join dictionary d on s.id=d.parent_id where d.folder='streets' and d.lang='{$_SESSION['lang']}' {$where} and name='name' order by d.value" );
		while( $row=$rs->fetch_row() )
		{
			$o = new Street( $row[0] );
			array_push( $GLOBALS['_RESULT']['streets'], Array( $o->id, $o->name ) );
		}
	}
	
	function delete_products_by_brand()
	{
		global $db;

		$brand_id = intval( $_REQUEST['id'] );
		$sql = "select id from shop_products where brand_id={$brand_id}";
		$rs = $db->query( $sql );
		while( $row = $rs->fetch_row() )
		{
			$o = new ShopProduct( $row[0], false );
			$o->Delete();
		}
	}
	
	function SwapPosition( &$src, &$dst, $table, $where="1" )
	{
		global $db;

		$t = new MysqlTable($table);
		$t->fix_positions( '', $where );

		$src_position = $src->position;
		$dst_position = $dst->position;
		
		if( $dst_position > $src_position )
		{
			$sql = "select id,position from {$table} where {$where} and position>{$src_position} and position<={$dst_position} order by position,id";
			$rs = $db->query( $sql );
			while( $row = $rs->fetch_row() )
			{
				$sql = "update {$table} set position={$row[1]}-1 where id={$row[0]}";
				$db->query( $sql );
			}
		}
		else
		{
			$sql = "select id,position from {$table} where {$where} and position<{$src_position} and position>={$dst_position} order by position,id";
			$rs = $db->query( $sql );
			while( $row = $rs->fetch_row() )
			{
				$sql = "update {$table} set position={$row[1]}+1 where id={$row[0]}";
				$db->query( $sql );
			}
		}
		$src->position = $dst_position;
		$src->Save();
	}
	
	function InitSettingsMenu() {
		$this->menuleft->add_item( new WEMenuItem( "settings", $this->dictionary->get('/locale/settings/menuleft/settings'), "?settings.edit" ) );
		$this->menuleft->add_item( new WEMenuItem( "users", $this->dictionary->get('/locale/settings/menuleft/users'), "?users.index" ) );
		$this->menuleft->add_item( new WEMenuItem( "homepage", $this->dictionary->get('/locale/settings/menuleft/homepage'), "?homepage.edit" ) );
		$this->menuleft->add_item( new WEMenuItem( "contacts", $this->dictionary->get('/locale/settings/menuleft/contacts'), "?settings.contacts_edit" ) );
		$this->menuleft->add_item( new WEMenuItem( "regions", $this->dictionary->get('/locale/settings/menuleft/regions'), "?regions.index" ) );
		$this->menuleft->add_item( new WEMenuItem( "streets", $this->dictionary->get('/locale/settings/menuleft/streets'), "?streets.index" ) );
		$this->menuleft->add_item( new WEMenuItem( "currencies", $this->dictionary->get('/locale/settings/menuleft/currencies'), "?currencies.index" ) );
	}
	
	/**
	 * [SendSMS description]
	 * @param [type] $code    country phone code, from countries table
	 * @param [type] $number  number without country code, 9-10 digits
	 * @param string $message [description]
	 */
	protected function SendSMS( $code, $number, $message='' ) {
		global $log;
		$retval = null;
		$username = 'info@kollage.com.ua';
		$password = 'gtxfnmajnj';
		
		$number = $this->FormatPhone( $code, $number );
		$log->Write( "admin base.SendSMS ({$code}) ({$number}) msg:{$message} sending is in process" );
		$number = $code . $number;
		if( $number && strlen($number)>=11 ) {
			$src = '<?xml version="1.0" encoding="UTF-8"?>';
			$src .= '<SMS>';
			
			$src .= '<operations>';
			$src .= '<operation>SEND</operation>';
			$src .= '</operations>';
			
			$src .= '<authentification>';
			$src .= '<username>'. $username .'</username>';
			$src .= '<password>'. $password .'</password>';
			$src .= '</authentification>';
			
			$src .= '<message>';
			$src .= '<sender>Kollage</sender>';
			$src .= '<text>'. $message .'</text>';
			$src .= '</message>';
			
			$src .= '<numbers>';
			$src .= '<number messageID="msg11">'. $number .'</number>';
			$src .= '</numbers>';
			
			$src .= '</SMS>';
			
			$curl = curl_init();
			$curlOptions = array(   
				CURLOPT_URL=>'https://atompark.com/members/sms/xml.php',
				CURLOPT_SSL_VERIFYPEER=>false,
				CURLOPT_FOLLOWLOCATION=>false,
				CURLOPT_POST=>true,
				CURLOPT_HEADER=>false,
				CURLOPT_RETURNTRANSFER=>true,
				CURLOPT_CONNECTTIMEOUT=>15,
				CURLOPT_TIMEOUT=>100,
				CURLOPT_POSTFIELDS=>array('XML'=>$src)
			);  
			//$log->Write( json_encode($curlOptions) );
			curl_setopt_array( $curl, $curlOptions ); 
			$result = curl_exec( $curl );
			//$log->Write( json_encode($result) );
			if( $result === false ) {
				$log->Write( "admin base.SendSMS ({$number}) msg:{$message} Https request failed" );
			}
			else {
				$log->Write( "admin base.SendSMS ({$number}) msg:{$message} sending successfull" );
			}
			
			curl_close( $curl );
		}
		else {
			$log->Write( "admin base.SendSMS ({$number}) msg:{$message} not sent, number is wrong" );
		}
		
		return( $retval );
	}

	protected function PushNotification( $token, $title, $body, $category='message' ) {
		global $log;
		$api_access_key = 'AIzaSyA7-i7-SINwo7BI0REPIrcyK13aJFHD5Ec'; //Google Cloud Messages

		$log->Write( "admin::base.PushNotification ({$token}) title:{$title} body:{$body} category:{$category}, starting" );

		$data = array(
    		'category' => $category
		);

		$notification = array(
	    	'title' => $title,
	    	'body'  => $body,
	    	'sound' => 'default'
		);

		$fields = array(
	    	//'registration_ids'  => $registrationIds, //1 to 1000
	    	'to'                => $token,
	    	'priority'          => 'normal',
	    	'collapse_key'      => $category,
	    	'content_available' => true,
	    	'data'              => $data,
	    	'notification'      => $notification
		);

		$headers = array
		(
		   'Authorization: key='. $api_access_key,
		   'Content-Type: application/json'
		);

		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_URL, 'https://gcm-http.googleapis.com/gcm/send' );
		curl_setopt( $ch, CURLOPT_POST, true );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode($fields) );
		$result = curl_exec($ch);
		$httpcode = intval( curl_getinfo($ch, CURLINFO_HTTP_CODE) );
		curl_close($ch);

		$log->Write( "admin::base.PushNotification ({$token}) title:{$title} body:{$body} category:{$category}, result". $result );

		$retval = false;
		if( $httpcode==200 ) {
			try {
			   $obj = json_decode( $result );
			   $retval = $obj->success;
			   $log->Write( "admin::base.PushNotification ({$token}) title:{$title} body:{$body} category:{$category}, success" );
			}
			catch( Exception $e ) {
				$log->Write( "admin::base.PushNotification ({$token}) title:{$title} body:{$body} category:{$category}, error: {$e->message}" );
			}
		}

		return( $retval );
	}
		
	function FormatPhone( $code, $phone ) {
		global $log;

		if( $phone ) {
			$phone = trim( $phone );
			//$log->Write("FormatPhone1 '{$code}' '{$phone}'");
			$phone = preg_replace( '@[\s\\\/=&\(\)\[\]\{\}%\^\+\-\._]+@', '', $phone );
			$pos = strpos($phone, $code);
			//$log->Write("FormatPhone2 '{$code}' '{$phone}' pos='{$pos}'");
			if( $pos===0 ) $phone = substr( $phone, strlen($code) );
			//$log->Write("FormatPhone3 '{$code}' '{$phone}'");
			
			if( $code == '380' ) {
				$zero = substr( $phone, 0, 1);
				if( $zero == '0' ) $phone = substr( $phone, 1 );
			}
		}
		
		return( $phone );
	}
	
	function FormatEmail( $email ) {
		if( $email ) {
			$email = trim( $email );
			$email = preg_replace( '@[\s\\\/=&\(\)\[\]\{\}%\+\^]+@', '', $email );
			$email = preg_replace( '/[\.]{2,}/', '.', $email );
		}
		
		return( $email );
	}
	
	function FormatPrice( $price ) {
		return( sprintf( "%01.2f", $price ) );
	}
}

?>
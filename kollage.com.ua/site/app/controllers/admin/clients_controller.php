<?php
/*****************************************************
 Class v.1.0, 02.2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

include_once( 'base_controller.php' );

class clients_controller extends base_controller
{
	var $folder = 'client';
	
	function clients_controller()
	{
		parent::base_controller();
		
		$this->menuleft->add_item( new WEMenuItem( "clients", $this->dictionary->get('/locale/clients/menuleft/clients'), "?".$this->controller_name.".index" ) );
		$this->menuleft->add_item( new WEMenuItem( "client_types", $this->dictionary->get('/locale/clients/menuleft/client_types'), "?".$this->controller_name.".list_client_types" ) );
		$this->menuleft->add_item( new WEMenuItem( "send", $this->dictionary->get('/locale/clients/menuleft/send'), "?".$this->controller_name.".form" ) );
		$this->menuleft->add_item( new WEMenuItem( "send_sms", $this->dictionary->get('/locale/clients/menuleft/send_sms'), "?".$this->controller_name.".form_sms" ) );
		$this->menuleft->add_item( new WEMenuItem( "send_push", $this->dictionary->get('/locale/clients/menuleft/send_push'), "?".$this->controller_name.".form_push" ) );
		$this->menuleft->add_item( new WEMenuItem( "stat", $this->dictionary->get('/locale/clients/menuleft/stat'), "?".$this->controller_name.".stat" ) );
		$this->menuleft->add_item( new WEMenuItem( "activity", $this->dictionary->get('/locale/clients/menuleft/activity'), "?".$this->controller_name.".activity" ) );
		//$this->menuleft->add_item( new WEMenuItem( "userfiles", $this->dictionary->get('/locale/clients/menuleft/userfiles'), "?".$this->controller_name.".list_userfiles_clients" ) );
	}
	
	function make_elements()
	{
		$retval = "";
		
		$retval .= $this->bigtabs->xml();
		$retval .= $this->smalltabs->xml();
		$retval .= $this->menuleft->xml();
		
		$retval .= "<left_panel show='yes'></left_panel>";
		
		return( $retval );
	}
	
	function index( $parameters='' )
	{
		$this->bigtabs->select_child( 'clients' );
		$this->menuleft->select_child( 'clients' );
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/shop/smalltabs/list'), "?".$this->controller_name.".index&page=". intval($_REQUEST['page'])) );
		$this->smalltabs->select_child( 'list' );
		
		$type_where = "";
		$type_id = intval( $_REQUEST['type_id'] );
		if( $type_id > 0 )
		{
			$type_where = " and type_id=". $type_id;
		}
		if( $type_id == -1 )
		{
			$type_where = " and professional=1";
		}
		
		$this->method_name = 'index';
		$xml = "<data>";
		$xml .= "<clients>";
		
		$sort_number = intval( $_REQUEST['sort'] );
		if( !$sort_number && intval($_SESSION["sort_{$this->controller_name}_{$this->method_name}"]) != 0)
		{
			$sort_number = $_SESSION["sort_{$this->controller_name}_{$this->method_name}"];
		}
		if( !$sort_number && intval($_SESSION["sort_{$this->controller_name}_{$this->method_name}"]) == 0)
		{
			$sort_number = 2;
		}
		$sort_direction = ( $sort_number < 0 )? 'desc' : '';
		switch( abs( $sort_number ) )
		{
			case 10:
				$sort = "created_at {$sort_direction}";
				break;
			case 2:
				$sort = "lastname {$sort_direction},id";
				break;
		}
		$_SESSION["sort_{$this->controller_name}_{$this->method_name}"] = $sort_number;
		
		$t = new MysqlTable( 'clients' );
		parent::define_pages( $t->get_count( "status in (0,1,2,3)".$type_where ) );
		$t->find_all( "status in (0,1,2,3)".$type_where, $sort, ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$client = new Client( $a['id'] );
			
			if( $client->position == 0 )
			{
				$tp = new MysqlTable('clients');
				$tp->fix_positions();
				$client->Load( $a['id'] );
			}
			
			$xml .= $client->Xml();
		}
		
		if( !$client ) $client = new Client();
		$xml .= "<last_position>". $client->GetLastPosition() ."</last_position>";
		$xml .= "<sort_number>{$sort_number}</sort_number>";
		$xml .= "<type_id>{$type_id}</type_id>";
		
		$xml .= "</clients>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'list', $xml );
	}
	
	function search()
	{
		global $db;

		$this->bigtabs->select_child( 'clients' );
		$this->menuleft->select_child( 'clients' );
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/shop/smalltabs/list'), "?".$this->controller_name.".index&page=". intval($_REQUEST['page'])) );
		$this->smalltabs->select_child( 'list' );
		$parameters = "";
		$name = trim( $_REQUEST["name"] );
		$phone = ( $name[0] == 0 )? substr( $name, 1 ) : $name;
		
		$this->method_name = 'search';
		$xml = "<data>";
		$xml .= "<clients>";
		$sql = "select id from clients where status<10 and (firstname like '%{$name}%' or lastname like '%{$name}%' or email like '%{$name}%' or id='{$name}' or city like '%{$name}%') OR phone like '%{$phone}%'";
		$rs = $db->query( $sql );
		while( $row = $rs->fetch_row() )
		{
			$client = new Client( $row[0] );
			$xml .= $client->Xml();
		}
		$xml .= "</clients>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		
		$this->display( 'search', $xml );
	}
	
	function edit( $save_result="" )
	{
		$last_position = intval( $_REQUEST['last_position'] );
		
		$this->bigtabs->select_child( 'clients' );
		$this->menuleft->select_child( 'clients' );
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/shop/smalltabs/list'), "?".$this->controller_name.".index&page=". intval($_REQUEST['page'])) );
		if( intval($_REQUEST['page']) > 0 ) $this->page = intval($_REQUEST['page']);
		
		$this->method_name = 'edit';
		$xml = "<data>";
		
		$xml .= "<edit>";
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= "<last_position>$last_position</last_position>";
		$client = new Client( $_REQUEST['id'] );
		$xml .= $client->Xml();
		$xml .= $this->GetRegionsXml();
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'edit', $xml );
	}
	
	function save()
	{
		$erx = "";
		
		$obj = new Client( $_REQUEST['client']['id'] );
		$last_position = intval( $_REQUEST['last_position'] );
		if( $last_position > 0 ) $_REQUEST['client']['position'] = $last_position + 1;
		
		if( $obj->Save( $_REQUEST['client'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->edit( 'true' );
		}
		else
		{
			$this->edit( 'false' );
		}
	}
	
	function activate()
	{
		$erx = "";
		
		$obj = new Client( $_REQUEST['id'] );
		if( $obj->IsLoaded() ) 
		{
			$obj->status = 2;
			if( $obj->Save() )
			{
				$_REQUEST['id'] = $obj->id;
				$this->edit( 'true' );
			}
		}
		$this->edit();
	}
	
	function clear_order_limit()
	{
		global $db;

		$erx = "";
		
		$obj = new Client( $_REQUEST['id'] );
		if( $obj->IsLoaded() ) 
		{
			$db->query( "insert into sop_orders (order_number,client_id,status) values ('col_". time() ."',{$obj->id},10)" );
			$_REQUEST['id'] = $obj->id;
			$this->edit( 'true' );
		}
		$this->edit();
	}
	
	function del()
	{
		$this->delete();
	}
	
	function delete()
	{
		$client = new Client( $_REQUEST['id'] );
		$client->Delete();
		$this->index();

	}
	
	function del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new Client( $id );
			$o->Delete();
		}
		$this->index( 'scroll_to_list' );
	}
	
	function move_up()
	{
		$t = new MysqlTable('clients');
		$t->move_position_up( $_REQUEST['id'], '', "category_id=1" );
		$this->index( 'scroll_to_list' );
	}
	
	function move_down()
	{
		$t = new MysqlTable('clients');
		$t->move_position_down( $_REQUEST['id'], '', "category_id=1" );
		$this->index( 'scroll_to_list' );
	}
	
	function swap_position()
	{
		$src = new Client( $_REQUEST['src'] );
		$src_position = $src->position;
		$dst = new Client( $_REQUEST['dst'] );
		$src->position = $dst->position;
		$src->Save();
		$dst->position = $src_position;
		$dst->Save();
		$this->index( 'scroll_to_list' );
	}
	
	function set_status()
	{
		$id = intval( $_REQUEST['id'] );
		$v = intval( $_REQUEST['v'] );
		$o = new Client( $id );
		if( $o->IsLoaded() )
		{
			$o->status = $v;
			$o->Save();
		}
		
		$this->index();
	}
	
	//Client types
	function list_client_types( $parameters='' )
	{
		$this->bigtabs->select_child( 'clients' );
		$this->menuleft->select_child( 'client_types' );
		
		$this->method_name = 'list_client_types';
		$xml = "<data>";
		$xml .= "<types>";
		
		$t = new MysqlTable( 'client_types' );
		parent::define_pages( $t->get_count() );
		$t->find_all( '', 'position,id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$client_type = new ClientType( $a['id'] );
			
			if( $client_type->position == 0 )
			{
				$tp = new MysqlTable('client_types');
				$tp->fix_positions();
				$client_type->Load( $a['id'] );
			}
			
			$xml .= $client_type->Xml();
		}
		
		$xml .= "<last_position>{$client_type->position}</last_position>";
		
		$xml .= "</types>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'list_client_types', $xml );
	}
	
	function edit_client_type( $save_result="" )
	{
		$last_position = intval( $_REQUEST['last_position'] );
		
		$this->bigtabs->select_child( 'clients' );
		$this->menuleft->select_child( 'client_types' );
		
		$this->method_name = 'edit_client_type';
		$xml = "<data>";
		
		$xml .= "<client_type>";
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= "<last_position>$last_position</last_position>";
		$client_type = new ClientType( $_REQUEST['id'] );
		$xml .= $client_type->Xml();
		$xml .= "</client_type>";
		
		$xml .= "</data>";
		
		$this->display( 'edit_client_type', $xml );
	}
	
	function client_type_save()
	{
		$erx = "";
		
		$obj = new ClientType( $_REQUEST['type']['id'] );
		$last_position = intval( $_REQUEST['last_position'] );
		if( $last_position > 0 ) $_REQUEST['type']['position'] = $last_position + 1;
		
		if( $obj->Save( $_REQUEST['type'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->edit_client_type( 'true' );
		}
		else
		{
			$this->edit_client_type( 'false' );
		}
	}
	
	function del_client_type()
	{
		$obj = new ClientType( $_REQUEST['id'] );
		$obj->Delete();
		$this->list_client_types( 'scroll_to_list' );

	}
	
	function types_del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new ClientType( $id );
			$o->Delete();
		}
		$this->list_client_types( 'scroll_to_list' );
	}
	
	function type_move_up()
	{
		$t = new MysqlTable('client_types');
		$t->move_position_up( $_REQUEST['id'] );
		$this->list_client_types( 'scroll_to_list' );
	}
	
	function type_move_down()
	{
		$t = new MysqlTable('client_types');
		$t->move_position_down( $_REQUEST['id'] );
		$this->list_client_types( 'scroll_to_list' );
	}
	
	function type_swap_position()
	{
		$src = new ClientType( $_REQUEST['src'] );
		$dst = new ClientType( $_REQUEST['dst'] );
		$this->SwapPosition( $src, $dst, "client_types" );
		$this->list_client_types( 'scroll_to_list' );
	}
	// /Client types
	
	// Purse operations
	function list_purse_operations( $parameters='' )
	{
		$this->bigtabs->select_child( 'clients' );
		$this->menuleft->select_child( 'clients' );
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/shop/smalltabs/list'), "?".$this->controller_name.".index&page=". intval($_REQUEST['page'])) );
		
		$this->method_name = 'list_purse_operations';
		$xml = "<data>";
		$xml .= "<list>";
		
		$client_id = intval( $_REQUEST['id'] );
		$client = new Client( $client_id );
		$xml .= $client->Xml();
		
		$t = new MysqlTable( 'purse_operations' );
		parent::define_pages( $t->get_count("client_id=$client_id") );
		$t->find_all( "client_id=$client_id", 'created_at,id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$o = new PurseOperation( $a['id'] );
			$xml .= $o->Xml();
		}
		
		$xml .= "</list>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'list_purse_operations', $xml );
	}
	
	function edit_purse_operation( $save_result="" )
	{
		$this->bigtabs->select_child( 'clients' );
		$this->menuleft->select_child( 'clients' );
		
		$this->method_name = 'edit_purse_operation';
		
		$client_id = intval( $_REQUEST['id'] );
		
		$xml = "<data>";
		$xml .= "<client>{$client_id}</client>";
		$xml .= $save_result;
		$xml .= "</data>";
		
		$this->display( 'edit_purse_operation', $xml );
	}
	
	function purse_operation_save()
	{
		$erx = "";
		
		$obj = new PurseOperation();
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$this->edit_purse_operation( "<reloader reload='yes' close='yes'></reloader>" );
		}
	}
	// /Purse operations
	
	//Send email to clients
	function form( $save_result="" )
	{
		global $db;
		
		$this->bigtabs->select_child( 'clients' );
		$this->menuleft->select_child( 'send' );
		
		$this->method_name = 'form';
		
		$clients = $db->getone( "select count(id) from clients where status=1" );
		
		$xml = "<data>";
		$xml .= "<clients>";
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= "<types>";
		$t = new MysqlTable('client_types');
		if( $t->find_all( "","position,id" ) )
		{
			unset( $this->types ); $this->types = Array();
			foreach( $t->data as $row )
			{
				$type = new ClientType($row['id']);
				$xml .= $type->Xml();
			}
		}
		$xml .= "</types>";

		$xml .= "<queues>";
		$t = new MysqlTable('mail_delivery_queues');
		if( $t->find_all( "status in (1,2)","id" ) )
		{
			foreach( $t->data as $row )
			{
				$q = new MailDeliveryQueue($row['id']);
				$xml .= $q->Xml();
			}
		}
		$xml .= "</queues>";

		$xml .= "</clients>";
		$xml .= "</data>";

		$this->display( 'form', $xml );
	}
	
	function send()
	{
		set_time_limit(7200);
		header("Expires: 0");
		global $db, $log;
		
		$type_id = intval( $_REQUEST['type_id'] );
		$subject = $_REQUEST['subject'];
		$body = $_REQUEST['body'];
		
		$sended = 0;
		if( $subject != '' && $body != '' )
		{
			$settings = new Settings();
			$sop_settings = new SOPSettings();

			$queue = new MailDeliveryQueue();
			$queue->name = $subject;
			$queue->subject = $subject;
			$queue->from_name = $settings->from_name;
			$queue->from_email = $settings->from_email;
			$queue->body = $body;
			$queue->status = 1;

			if( is_uploaded_file($_FILES['file']['tmp_name']) )
			{
				$aname = md5( time() . rand(1,99) );
				$dst = SITEROOT .'/'. $_ENV['mqueue_path'] .'/'. $aname;
				copy( $_FILES['file']['tmp_name'], $dst );
				$queue->attachment_file = $aname;
				$queue->attachment_name = trim( basename( $_FILES['file']['name'] ) );
			}

			//$log->Write("admin:clients.send". json_encode($queue));
			$queue_id = $queue->Save();
			
			$where = "";
			if( $type_id > 0 ) $where = " and type_id={$type_id}";
			else if( $type_id == -1 ) $where = " and professional=1";
			$sql = "select email from clients where status=2 and send_news=1 ". $where;
			$rs = $db->query( $sql );
			while( $row = $rs->fetch_row() )
			{
				$email = trim( $row[0] );
				if( $email && strpos($email,'@') )
				{
					$m = new MailDeliveryQueueEmail();
					$m->queue_id = $queue_id;
					$m->email = $email;
					$m->status = 1;
					$m->Save();

					$sended ++;
				}
			}
		}
		
		if( $sended ) $this->form('true');
		else $this->form('false');
	}
	
	//Send email to a client
	function send2client()
	{
		global $db;
		$id = intval( $_REQUEST['id'] );
		$subject = $_REQUEST['subject'];
		$body = $_REQUEST['body'];
		
		if( $id > 0 &&$subject != '' && $body != '' )
		{
			$settings = new Settings();
			$sop_settings = new SOPSettings();
			$mail = new AMail( 'windows-1251' );
			$mail->subject = $subject;
			$mail->from_name = $settings->from_name;
			$mail->from_email = $settings->from_email;
			
			if( is_uploaded_file($_FILES['file']['tmp_name']) )
			{
				$mail->AttachFile( $_FILES['file']['tmp_name'], trim( basename( $_FILES['file']['name'] ) ) );
			}
			
			$sql = "select email from clients where id={$id}";
			$email = $db->getone( $sql );
			if( $email && strpos($email,'@') )
			{
				$mail->to_email = $email;
				$mail->Send( $body );
			}
		}
		
		$this->edit();
	}
	
	//Push notification to a client
	function push2client()
	{
		global $db;
		$id = intval( $_REQUEST['id'] );
		$subject = $_REQUEST['subject'];
		$body = $_REQUEST['body'];
		
		$client = new Client( $id );
		if( $client->IsLoaded() > 0 &&$subject != '' && count($client->tokens) > 0 )
		{
			foreach( $client->tokens as $token ) {
				$this->PushNotification( $token->google_cloud_token, $subject, $body );
			}
		}
		
		$this->edit();
	}
	
	//Send SMS
	function form_sms( $save_result="" )
	{
		global $db;
		
		$this->bigtabs->select_child( 'clients' );
		$this->menuleft->select_child( 'send_sms' );
		
		$this->method_name = 'form_sms';
		
		$clients = $db->getone( "select count(id) from clients where status=1" );
		
		$xml = "<data>";
		$xml .= "<clients>";
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= "<types>";
		$t = new MysqlTable('client_types');
		if( $t->find_all( "","position,id" ) )
		{
			unset( $this->types ); $this->types = Array();
			foreach( $t->data as $row )
			{
				$type = new ClientType($row['id']);
				$xml .= $type->Xml();
			}
		}
		$xml .= "</types>";
		$xml .= "</clients>";
		$xml .= "</data>";
		
		$this->display( 'form_sms', $xml );
	}
	
	function send_sms()
	{
		set_time_limit(3600);
		header("Expires: 0");
		
		global $db;
		$type_id = intval( $_REQUEST['type_id'] );
		$message = $_REQUEST['message'];
		
		$counted = 0;
		$sended = 0;
		if( $type_id != '' && $message != '' )
		{
			$where = "";
			if( $type_id > 0 ) $where = " and type_id={$type_id}";
			//$sql = "select phone from clients where status=2 and send_news=1 ". $where;
			$sql = "select phone, country_id from clients where status=2 ". $where;
			$rs = $db->query( $sql );
			while( $row = $rs->fetch_row() )
			{
				$phone = trim( $row[0] );
				if( $phone )
				{
					$country = new Country( $row[1] );
					$this->SendSMS( $country->phone_code, $phone, "Kollage: {$message}. {$this->sms_suffix}" );
					$sended ++;
				}
				$counted ++;
			}
		}
		
		if( $sended ) $this->form_sms( "Counted: {$counted}, Sent: {$sended}" );
		else $this->form_sms('false');
	}
	//Send SMS
	
	//Send PUSH
	function form_push( $save_result="" )
	{
		global $db;
		
		$this->bigtabs->select_child( 'clients' );
		$this->menuleft->select_child( 'send_push' );
		
		$this->method_name = 'form_push';
		
		$clients = $db->getone( "select count(id) from clients where status=1" );
		
		$xml = "<data>";
		$xml .= "<clients>";
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= "<types>";
		$t = new MysqlTable('client_types');
		if( $t->find_all( "","position,id" ) )
		{
			unset( $this->types ); $this->types = Array();
			foreach( $t->data as $row )
			{
				$type = new ClientType($row['id']);
				$xml .= $type->Xml();
			}
		}
		$xml .= "</types>";
		$xml .= "</clients>";
		$xml .= "</data>";
		
		$this->display( 'form_push', $xml );
	}
	
	function send_push()
	{
		set_time_limit(3600);
		header("Expires: 0");
		
		global $db;
		$type_id = intval( $_REQUEST['type_id'] );
		$subject = $_REQUEST['subject'];
		$body = $_REQUEST['body'];
		
		$counted = 0;
		$sended = 0;
		if( $type_id >= 0 && $subject != '' )
		{
			$where = "";
			if( $type_id > 0 ) $where = " and type_id={$type_id}";
			$sql = "select id from clients where status=2 ". $where;
			$rs = $db->query( $sql );
			while( $row = $rs->fetch_row() )
			{
				$client_id = trim( $row[0] );
				$client = new Client( $client_id );
				if( $client->IsLoaded() > 0 &&$subject != '' && count($client->tokens) > 0 )
				{
					foreach( $client->tokens as $token ) {
						$this->PushNotification( $token->google_cloud_token, $subject, $body );
					}
					$sended ++;
				}
				$counted ++;
			}
		}
		
		if( $sended ) $this->form_push( "Counted: {$counted}, Sent: {$sended}" );
		else $this->form_push('false');
	}
	//Send PUSH
	
	//STAT
	function stat()
	{
		global $db, $log;
		
		$this->bigtabs->select_child( 'clients' );
		$this->menuleft->select_child( 'stat' );
		
		$this->method_name = 'stat';
		
		$md = new MysqlDateTime();
		
		//Top 10 condition by date
		$from = $_REQUEST['from'];
		$to = $_REQUEST['to'];
		if( $from == '' )
		{
			$from = $db->getone( "select subdate(now(),interval 1 month)" );
			$md->Parse( $from );
			$from = $md->GetValue("d.m.y");
		}
		if( $to == '' )
		{
			$to = date( "d.m.Y" );
		}
		$md->ParseFrontend( $from ); $from_sql = $md->GetMysqlValue();
		$md->ParseFrontend( $to ); $to_sql = $md->GetMysqlValue();
		$date_sql = " and o.created_at>='{$from_sql}' and o.created_at<='{$to_sql}'";
		
		$purse_amount = $db->getone( "select sum(amount) from purse_operations where client_id in (select id from clients where status=2)" );
		$posterminal_operations_sop_total = sprintf( "%01.2f", $db->getone( "select sum(p.quantity*p.price-p.quantity*p.price*o.discount/100) as s1 from sop_orders o left join sop_order_photos p on p.order_id=o.id  where o.status in (2,3,4) and paymentway_id=10" ) );
		$posterminal_operations_sop_total_payed = sprintf( "%01.2f", floatval($db->getone( "select sum(amount) from posterminal_operations where type=1 and amount<0" ))*-1 );
		
		$xml = "<data>";
		$xml .= "<stat>";
		$xml .= "<purse_amount>{$purse_amount}</purse_amount>";
		$xml .= "<posterminal_operations><sop_total>{$posterminal_operations_sop_total}</sop_total><sop_total_payed>{$posterminal_operations_sop_total_payed}</sop_total_payed></posterminal_operations>";
		$xml .= "<from>{$from}</from>";
		$xml .= "<to>{$to}</to>";
		
		$xml .= "<sop_sum1>";
		$sop_sum1 = 0;
		$sql = "select sum(p.quantity*p.price-p.quantity*p.price*o.discount/100) as s1, p.size_id from sop_orders o left join sop_order_photos p on p.order_id=o.id left join sop_order_photos_sizes s on s.id=p.size_id where o.status in (2,3,4) {$date_sql} group by p.size_id order by s.position";
		$rs = $db->query( $sql );
		while( $row = $rs->fetch_row() )
		{
			$sum = sprintf( "%01.2f", $row[0] );
			$size = new SOPOrderPhotoSize( $row[1] );
			$xml .= "<item sum='{$sum}'>{$size->name}</item>";
			$sop_sum1 += $row[0];
		}
		$sop_sum1 = sprintf( "%01.2f", $sop_sum1 );
		$xml .= "<total>{$sop_sum1}</total>";
		$xml .= "</sop_sum1>";
		
		$xml .= "<sop_sum2>";
		$sop_sum2 = 0;
		$sql = "select sum(p.quantity*p.price-p.quantity*p.price*o.discount/100) as s1, ct.id from sop_orders o left join sop_order_photos p on p.order_id=o.id left join clients c on c.id=o.client_id left join client_types ct on ct.id=c.type_id where o.status in (2,3,4) {$date_sql} group by ct.id order by ct.position";
		$rs = $db->query( $sql );
		while( $row = $rs->fetch_row() )
		{
			$sum = sprintf( "%01.2f", $row[0] );
			$ct = new ClientType( $row[1] );
			$xml .= "<item sum='{$sum}'>{$ct->name}</item>";
			$sop_sum2 += $row[0];
		}
		$sop_sum2 = sprintf( "%01.2f", $sop_sum2 );
		$xml .= "<total>{$sop_sum2}</total>";
		$xml .= "</sop_sum2>";
		
		$xml .= "<sop_sum4>";
		$sop_sum4 = 0;
		$sop_quantity4 = 0;
		$sql = "select sum(p.quantity*p.price-p.quantity*p.price*o.discount/100) as s1, p.size_id, sum(p.quantity) as q from sop_orders o left join sop_order_photos p on p.order_id=o.id left join sop_order_photos_sizes s on s.id=p.size_id where o.status in (2,3,4) {$date_sql} group by p.size_id order by s.position";
		$rs = $db->query( $sql );
		while( $row = $db->fetch_row($rs) )
		{
			if( $row[1] > 0 ) {
				$sum = sprintf( "%01.2f", $row[0] );
				$quantity = $row[2];
				$size = new SOPOrderPhotoSize( $row[1] );
				$xml .= "<item sum='{$sum}' quantity='{$quantity}'><name>{$size->name}</name><items>";
				
				$sop_sum4_2 = 0;
				$sop_quantity4_2 = 0;
				$sql = "select id from client_types order by position";
				$rs2 = $db->query( $sql );
				while( $row2 = $rs2->fetch_row() )
				{
					$ct = new ClientType( $row2[0] );
					$sum2 = 0; $q2 = 0;
					$sql = "select sum(p.quantity*p.price-p.quantity*p.price*o.discount/100) as s1, ct.id, sum(p.quantity) as q from sop_orders o left join sop_order_photos p on p.order_id=o.id left join sop_order_photos_sizes s on s.id=p.size_id left join clients c on c.id=o.client_id left join client_types ct on ct.id=c.type_id where o.status in (2,3,4) and s.id={$size->id} and ct.id={$ct->id} {$date_sql} group by ct.id order by ct.position";
					$rs3 = $db->query( $sql );
					if( $row3 = $db->fetch_row($rs3) )
					{
						$sum2 = $row3[0];
						$q2 = $row3[2];
						$sop_sum4_2 += $sum2;
						$sop_quantity4_2 += $q2;
					}
					$sum2 = sprintf( "%01.2f", $sum2 );
					$xml .= "<item sum='{$sum2}' quantity='{$q2}'>{$ct->name}</item>";
				}
				$xml .= "<total>{$sop_sum4_2}</total>";
				$xml .= "<total_quantity>{$sop_quantity4_2}</total_quantity>";
				$xml .= "</items></item>";
				$sop_sum4 += $row[0];
				$sop_quantity4 += $row[2];
			}
		}
		$sop_sum4 = sprintf( "%01.2f", $sop_sum4 );
		$xml .= "<total>{$sop_sum4}</total>";
		$xml .= "<total_quantity>{$sop_quantity4}</total_quantity>";
		$xml .= "</sop_sum4>";
		
		$xml .= "<types>";
		$sql = "select id from client_types order by position";
		$rs = $db->query( $sql );
		while( $row = $rs->fetch_row() )
		{
			$xml .= "<type>";
			
			$ct = new ClientType( $row[0] );
			$sql = "select count(*) from clients where type_id={$row[0]} and status=2";
			$rs2 = $db->query( $sql );
			$row2 = $rs2->fetch_row();
			$xml .= "<name>{$ct->name}</name>";
			$xml .= "<items>{$row2[0]}</items>";
			
			$xml .= "</type>";
		}
		//Professional
			$xml .= "<type>";
			$sql = "select count(*) from clients where professional=1 and status=2";
			$rs2 = $db->query( $sql );
			$row2 = $rs2->fetch_row();
			$xml .= "<name>". $this->dictionary->get('/locale/clients/stat/professional') ."</name>";
			$xml .= "<items>{$row2[0]}</items>";
			$xml .= "</type>";
		//Professional
		$xml .= "</types>";
		
		$xml .= "<sop_top10>";
		//$sql = "create temporary table co_tmp select c.id as client_id,o.id as order_id,o.order_number,sum(p.quantity*p.price-p.quantity*p.price*o.discount/100) as s1,c.type_id from clients c left join sop_orders o on o.client_id=c.id left join sop_order_photos p on p.order_id=o.id left join sop_order_photos_sizes s on s.id=p.size_id where o.status=4 {$date_sql} group by o.id";
		$sql = "create temporary table co_tmp select c.id as client_id,o.id as order_id,o.order_number,sum(p.quantity*p.price-p.quantity*p.price*o.discount/100) as s1,c.type_id,c.professional from clients c left join sop_orders o on o.client_id=c.id left join sop_order_photos p on p.order_id=o.id left join sop_order_photos_sizes s on s.id=p.size_id where o.status=4 {$date_sql} group by o.id";
		$db->query( $sql );
		$sql = "select id from client_types order by position";
		$rs = $db->query( $sql );
		while( $row = $rs->fetch_row() )
		{
			$xml .= "<type>";
			
			$ct = new ClientType( $row[0] );
			$xml .= "<name>{$ct->name}</name>";
			
			$xml .= "<clients>";
			$sql = "select client_id,sum(s1) from co_tmp where type_id={$row[0]} group by client_id order by sum(s1) desc limit 10";
			$rs2 = $db->query( $sql );
			while( $row2 = $rs2->fetch_row() )
			{
				$xml .= "<client>";
				$xml .= "<sop_amount>{$row2[1]}</sop_amount>";
				$c = new Client($row2[0]);
				$xml .= $c->Xml();
				$xml .= "</client>";
			}
			$xml .= "</clients>";
			
			$xml .= "</type>";
		}
		$xml .= "</sop_top10>";
		$db->query( "drop table co_tmp" );
		
		$xml .= "</stat>";
		$xml .= "</data>";
		
		$this->display( 'stat', $xml );
	}
	
	function activity()
	{
		global $db;
		
		$this->bigtabs->select_child( 'clients' );
		$this->menuleft->select_child( 'activity' );
		
		$this->method_name = 'activity';
		
		$limit = intval( $_REQUEST['limit'] );
		if( !$limit ) $limit = 10;
		
		$min = intval( $_REQUEST['min'] );
		if( !$min ) $min = 1;
		
		$show = intval( $_REQUEST['show'] );
		if( $show == 1 || !$show ) //íåàêòèâíûå
		{
			$show_sql = "count(order_id) asc";
		}
		else if( $show == 2 ) //àêòèâíûå
		{
			$show_sql = "count(order_id) desc";
		}
		
		$md = new MysqlDateTime();
		$from = $_REQUEST['from'];
		$to = $_REQUEST['to'];
		if( $from == '' )
		{
			$from = $db->getone( "select subdate(now(),interval 1 month)" );
			$md->Parse( $from );
			$from = $md->GetValue("d.m.y");
		}
		if( $to == '' )
		{
			$to = date( "d.m.Y" );
		}
		$md->ParseFrontend( $from ); $from_sql = $md->GetMysqlValue();
		$md->ParseFrontend( $to ); $to_sql = $md->GetMysqlValue();
		$date_sql = " and o.created_at>='{$from_sql}' and o.created_at<='{$to_sql}'";
		
		$xml = "<data>";
		$xml .= "<stat>";
		$xml .= "<show>{$show}</show>";
		$xml .= "<limit>{$limit}</limit>";
		$xml .= "<min>{$min}</min>";
		$xml .= "<from>{$from}</from>";
		$xml .= "<to>{$to}</to>";
		
		$xml .= "<sop_top>";
		
		$sql = "create temporary table co_tmp select c.id as client_id,o.id as order_id,o.created_at as order_date,o.order_number,count(o.id) as order_photos_count,sum(p.quantity*p.price-p.quantity*p.price*o.discount/100) as s1,c.type_id,c.professional from clients c left join sop_orders o on o.client_id=c.id left join sop_order_photos p on p.order_id=o.id left join sop_order_photos_sizes s on s.id=p.size_id where o.status=4 {$date_sql} group by o.id";
		$db->query( $sql );
		$sql = "select id from client_types order by position";
		$rs = $db->query( $sql );
		while( $row = $rs->fetch_row() )
		{
			$xml .= "<type>";
			
			$ct = new ClientType( $row[0] );
			$xml .= "<name>{$ct->name}</name>";
			
			$xml .= "<clients>";
			$sql = "select client_id,sum(s1),count(order_id),max(order_date) from co_tmp where type_id={$row[0]} group by client_id having count(order_id)>={$min} order by {$show_sql},max(order_date) limit {$limit}";
			$rs2 = $db->query( $sql );
			while( $row2 = $rs2->fetch_row() )
			{
				$md->Parse( $row2[3] ); $sop_date = $md->GetFrontendValue("d.m.y");
				
				$xml .= "<client>";
				$xml .= "<sop_amount>{$row2[1]}</sop_amount>";
				$xml .= "<sop_count>{$row2[2]}</sop_count>";
				$xml .= "<sop_date>{$sop_date}</sop_date>";
				$c = new Client($row2[0]);
				$xml .= $c->Xml();
				$xml .= "</client>";
			}
			$xml .= "</clients>";
			
			$xml .= "</type>";
		}
		$xml .= "</sop_top>";
		$db->query( "drop table co_tmp" );
		
		$xml .= "</stat>";
		$xml .= "</data>";
		
		$this->display( 'activity', $xml );
	}
	//STAT
}

?>
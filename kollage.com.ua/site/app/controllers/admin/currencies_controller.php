<?php
/*****************************************************
 Class v.1.0, 02.2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

include_once( 'base_controller.php' );

class currencies_controller extends base_controller
{
	var $folder;
	
	function currencies_controller()
	{
		parent::base_controller();
		
		$this->bigtabs->select_child( 'settings' );
		$this->InitSettingsMenu();
	}
	
	function index( $parameters='' )
	{
		global $db;
		
		$this->menuleft->select_child( "currencies" );
		$car = array();
		
		$this->method_name = 'list';
		$xml = "<data>";
		$xml .= "<currencies>";
		
		$arr = Array();
		$sql = "select code from currencies group by code";
		$rs2 = $db->query( $sql );
		while( $row = $rs2->fetch_row() )
		{
			$sql = "select id from currencies where code={$row[0]} order by id desc limit 1";
			$id = $db->getone( $sql );
			
			$currency = new Currency( $id );
			array_push( $arr, $currency );
		}
		
		function arr_sort( $a, $b )
		{
			if( $a->position == $b->position ) $retval = 0;
			else if( $a->position > $b->position ) $retval = 1;
			else  $retval = -1;
			
			return( $retval );
		}
		
		usort( $arr, "arr_sort" );
		
		foreach( $arr as $a )
		{
			$xml .= $a->Xml();
		}
		
		if( !$currency ) $currency = new Currency();
		$xml .= "<last_position>". $currency->GetLastPosition() ."</last_position>";
		
		$xml .= "</currencies>";
		
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'list', $xml );
	}
	
	function edit( $save_result="" )
	{
		$last_position = intval( $_REQUEST['last_position'] );
		
		$this->menuleft->select_child( "currencies" );
		
		$this->method_name = 'edit';
		$xml = "<data>";
		
		$xml .= "<edit>";
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= "<last_position>$last_position</last_position>";
		$currency = new Currency( $_REQUEST['id'] );
		$currency->position = $currency->GetLastPosition() + 1;
		$xml .= $currency->Xml();
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'edit', $xml );
	}
	
	function save()
	{
		$erx = "";
		
		$obj = new Currency( $_REQUEST['object']['id'] );
		$last_position = intval( $_REQUEST['last_position'] );
		if( $last_position > 0 ) $_REQUEST['object']['position'] = $last_position + 1;
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->edit( 'true' );
		}
		else
		{
			$this->edit( 'false' );
		}
	}
	
	function del()
	{
		$this->delete();
	}
	
	function delete()
	{
		$currency = new Currency( $_REQUEST['id'] );
		$currency->Delete();
		$this->index();
	}
	
	function del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new Currency( $id );
			$o->Delete();
		}
		$this->index( 'scroll_to_list' );
	}
	
	function swap_position()
	{
		$src = new Currency( $_REQUEST['src'] );
		$src_position = $src->position;
		$dst = new Currency( $_REQUEST['dst'] );
		$src->position = $dst->position;
		$src->Save();
		$dst->position = $src_position;
		$dst->Save();
		$this->index( 'scroll_to_list' );
	}
}

?>
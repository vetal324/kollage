<?php
/*****************************************************
 Class v.1.0, 02.2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

include_once( 'base_controller.php' );

class brands_controller extends base_controller
{
    var $folder = 'brand';
    
    function brands_controller()
    {
        parent::base_controller();
        
        $this->make_small_tabs();
        $this->make_menu();
    }
    
    function make_small_tabs()
    {
        $this->smalltabs->del_all();
        $this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/shop/smalltabs/list'), "?".$this->controller_name.".index") );
    }
    
    function make_menu()
    {
        $this->menuleft->del_all();
        $t = new MysqlTable( 'shop_product_categories' );
        $t->find_all( "parent_id is null", "position,id" );
        foreach( $t->data as $row )
        {
            $o = new ShopCategory( $row['id'] );
            $submenu = new WEMenu( "submenu{$o->id}", $o->name, "?shop.index&category_id={$o->id}" );
            $this->menuleft->add_item( $submenu );
            $t2 = new MysqlTable( 'shop_product_categories' );
            $t2->find_all( "parent_id={$o->id}", "position,id" );
            foreach( $t2->data as $row2 )
            {
                $cs = new ShopCategory( $row2['id'] );
                //$submenu->add_item( new WEMenuItem( "c{$cs->id}", $cs->name, "?".$this->controller_name.".index&category_id={$cs->id}" ) );
                $submenu2 = new WEMenu( "submenu{$cs->id}", $cs->name, "?shop.index&category_id={$cs->id}" );
                $submenu->add_item( $submenu2 );
                $t3 = new MysqlTable( 'shop_product_categories' );
                $t3->find_all( "parent_id={$cs->id}", "position,id" );
                foreach( $t3->data as $row3 )
                {
                    $cs2 = new ShopCategory( $row3['id'] );
                    $submenu2->add_item( new WEMenuItem( "c{$cs2->id}", $cs2->name, "?shop.index&category_id={$cs2->id}" ) );
                }
            }
        }
        $this->menuleft->add_item( new WEMenuSeparator() );
        $this->menuleft->add_item( new WEMenuItem( "brands", $this->dictionary->get('/locale/shop/menuleft/brands'), "?brands.index" ) );
        $this->menuleft->add_item( new WEMenuItem( "settings", $this->dictionary->get('/locale/shop/menuleft/settings'), "?shop.settings_edit" ) );
        $this->menuleft->add_item( new WEMenuItem( "points_list", $this->dictionary->get('/locale/shop/menuleft/points_list'), "?shop.points_list" ) );
        $this->menuleft->add_item( new WEMenuItem( "deliveries_list", $this->dictionary->get('/locale/shop/menuleft/deliveries_list'), "?shop.deliveries_list" ) );
        $this->menuleft->add_item( new WEMenuItem( "paymentways_list", $this->dictionary->get('/locale/shop/menuleft/paymentways_list'), "?shop.paymentways_list" ) );
        //$this->menuleft->add_item( new WEMenuItem( "discounts_list", $this->dictionary->get('/locale/shop/menuleft/discounts_list'), "?shop.discounts_list" ) );
        $this->menuleft->add_item( new WEMenuItem( "prices", $this->dictionary->get('/locale/shop/menuleft/prices'), "?shop.prices_list" ) );
        $this->menuleft->add_item( new WEMenuSeparator() );
        $this->menuleft->add_item( new WEMenuItem( "articles", $this->dictionary->get('/locale/shop/menuleft/articles'), "?shop_articles.index" ) );
        $this->menuleft->add_item( new WEMenuSeparator() );
        $this->menuleft->add_item( new WEMenuItem( "old_products", $this->dictionary->get('/locale/shop/menuleft/old'), "?shop.old_products" ) );
    }
    
    function make_elements()
    {
        $retval = "";
        
        $retval .= $this->bigtabs->xml();
        $retval .= $this->smalltabs->xml();
        $retval .= $this->menuleft->xml();
        
        $retval .= "<left_panel show='yes'><categories>";
        $t = new MysqlTable( 'shop_product_categories' );
        $t->find_all( "", "position,id" );
        foreach( $t->data as $row )
        {
            $o = new ShopCategory( $row['id'] );
            $retval .= $o->Xml();
        }
        $retval .= "</categories></left_panel>";
        
        return( $retval );
    }
    
    function index( $parameters='' )
    {
        global $db;
        $this->make_small_tabs();
        $this->make_menu();
        $this->bigtabs->select_child( 'shop' );
        $this->smalltabs->select_child( 'list' );
        $this->menuleft->select_child( "brands" );
        
        $this->method_name = 'list';
        $xml = "<data>";
        $xml .= "<brands>";
        
        $t = new MysqlTable( 'shop_brands' );
        parent::define_pages( $t->get_count("status=1") );
        $sql = "select b.id from shop_brands b left join dictionary d on d.parent_id=b.id where b.status=1 and d.folder='shop_brands' and d.name='name' order by d.value limit ". ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page;
        $rs = $db->query( $sql );
        while( $a = $rs->fetch_row() )
        {
            $brand = new ShopBrand( $a[0] );
            
            if( $brand->position == 0 )
            {
                $tp = new MysqlTable('shop_brands');
                $tp->fix_positions("","status=1");
                $brand->Load( $a['id'] );
            }
            
            $xml .= $brand->Xml();
        }
        
        if( !$brand ) $brand = new ShopBrand();
        $xml .= "<last_position>". $brand->GetLastPosition("status=1") ."</last_position>";
        
        $xml .= "</brands>";
        $xml .= "<parameters>$parameters</parameters>";
        $xml .= "</data>";
        $this->display( 'list', $xml );
    }
    
    function set_status()
    {
        $id = intval( $_REQUEST['id'] );
        $v = intval( $_REQUEST['v'] );
        $o = new ShopBrand( $id );
        if( $o->IsLoaded() )
        {
            $o->status = $v;
            $o->Save();
        }
        
        $this->index();
    }
    
    function edit( $save_result="" )
    {
        $last_position = intval( $_REQUEST['last_position'] );
        
        $this->make_small_tabs();
        $this->bigtabs->select_child( 'shop' );
        $this->smalltabs->select_child( 'list' );
        $this->menuleft->select_child( "brands" );
        
        $this->method_name = 'edit';
        $xml = "<data>";
        
        $xml .= "<edit>";
        $xml .= "<save_result>$save_result</save_result>";
        $xml .= "<last_position>$last_position</last_position>";
        $brand = new ShopBrand( $_REQUEST['id'] );
        $xml .= $brand->Xml();
        $xml .= "</edit>";
        
        $xml .= "</data>";
        
        $this->display( 'edit', $xml );
    }
    
    function save()
    {
        $erx = "";
        
        $obj = new ShopBrand( $_REQUEST['object']['id'] );
        $last_position = intval( $_REQUEST['last_position'] );
        if( $last_position > 0 ) $_REQUEST['object']['position'] = $last_position + 1;
        
        $attachment = new Attachment();
        if( $attachment->upload('image') )
        {
            $_REQUEST['object']['image'] = $attachment->filename;
        }
        
        if( $obj->Save( $_REQUEST['object'] ) )
        {
            $_REQUEST['id'] = $obj->id;
            $this->edit( 'true' );
        }
        else
        {
            $this->edit( 'false' );
        }
    }
    
    function del()
    {
        $this->delete();
    }
    
    function delete()
    {
        $brand = new ShopBrand( $_REQUEST['id'] );
        $brand->Delete();
        $this->index();

    }
    
    function del_marked()
    {
        $ids_str = $_REQUEST['ids'];
        $ids = split( ',', $ids_str );
        foreach( $ids as $id )
        {
            $o = new ShopBrand( $id );
            $o->Delete();
        }
        $this->index( 'scroll_to_list' );
    }
    
    function del_image()
    {
        $brand = new ShopBrand( $_REQUEST['id'] );
        $brand->DeleteImage();
        $this->edit( 'true' );
    }
    
    function swap_position()
    {
        $src = new ShopBrand( $_REQUEST['src'] );
        $dst = new ShopBrand( $_REQUEST['dst'] );
        $this->SwapPosition( $src, $dst, "shop_brands", "status=1" );
        $this->index( 'scroll_to_list' );
    }
}

?>
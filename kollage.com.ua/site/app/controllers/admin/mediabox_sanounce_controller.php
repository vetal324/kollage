<?php
/*****************************************************
 Class v.1.0, 2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

include_once( 'mediabox_controller.php' );

class mediabox_sanounce_controller extends mediabox_controller
{
    function mediabox_sanounce_controller()
    {
        parent::base_controller();
    }
    
    function save()
    {
        $erx = "";
        
        $obj = new Image( $_REQUEST['image']['id'] );
        
        $attachment = new Attachment();
        if( $attachment->upload('tiny') )
        {
            $obj->DeleteTiny();
            $_REQUEST['image']['tiny'] = $attachment->filename;
            $width = 181; $height = 181;
            $filename = $attachment->filename;
            if( $filename )
            {
                $io = new AImage( SITEROOT . '/'. $_ENV['data_path'] .'/'. $filename );
                if( $io->CopyFitIn( SITEROOT . '/'. $_ENV['data_path'] .'/tiny-'. $filename, $w, $h, Array(255,255,255) ) )
                {
                    $_REQUEST['image']['tiny'] = 'tiny-'. $filename;
                }
            }
        }
        
        if( $attachment->upload('thumbnail') )
        {
            $obj->DeleteThumbnail();
            $_REQUEST['image']['thumbnail'] = $attachment->filename;
        }
        else
        {
            if( $_REQUEST['image']['small'] ) $filename = $_REQUEST['image']['small'];
            if( $_REQUEST['image']['filename'] ) $filename = $_REQUEST['image']['filename'];
            if( $filename )
            {
                $obj->DeleteThumbnail();
                $io = new AImage( SITEROOT . '/'. $_ENV['data_path'] .'/'. $filename );
                if( $io->CopyResize( SITEROOT . '/'. $_ENV['data_path'] .'/thumbnail-'. $filename, 100, 90 ) )
                {
                    $_REQUEST['image']['thumbnail'] = 'thumbnail-'. $filename;
                }
            }
        }
        
        if( $obj->Save( $_REQUEST['image'] ) )
        {
            $_REQUEST['id'] = $obj->id;
            $this->edit( "<reloader reload='yes' close='no'></reloader>" );
        }
        else
        {
            $this->edit();
        }
    }
}

?>
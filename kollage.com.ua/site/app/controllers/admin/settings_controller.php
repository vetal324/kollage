<?php
/*****************************************************
 Class v.1.0, 02.2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

include_once( 'base_controller.php' );

class settings_controller extends base_controller
{
	var $folder = 'settings';
	
	function settings_controller()
	{
		parent::base_controller();
		$this->InitSettingsMenu();
	}
	
	function index()
	{
		$this->edit();
	}
	
	function edit( $save_result="" )
	{
		$this->bigtabs->select_child( 'settings' );
		$this->menuleft->select_child( 'settings' );
		
		$this->method_name = 'edit';
		$xml = "<data>";
		
		$xml .= "<edit>";
		$xml .= "<save_result>$save_result</save_result>";
		$settings = new Settings(); $xml .= $settings->Xml();
		$admin = new User( 1 ); $xml .= $admin->Xml();
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'edit', $xml );
	}
	
	function save()
	{
		$erx = "";
		
		//Save login and pwd
		/*
		if( $_REQUEST['admin_login']!="" && $_REQUEST['admin_password']!="" )
		{
			$admin = new User(1);
			$admin->login = $_REQUEST['admin_login'];
			if( $_REQUEST['admin_password'] ) $admin->pwd = md5( $_REQUEST['admin_password'] );
			$admin->Save();
		}
		*/
		
		$obj = new Settings();
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->edit( 'true' );
		}
		else
		{
			$this->edit( 'false' );
		}
	}
	
	function contacts_edit( $save_result="" )
	{
		$this->bigtabs->select_child( 'settings' );
		$this->menuleft->select_child( 'contacts' );
		
		$this->method_name = 'contacts_edit';
		$xml = "<data>";
		
		$xml .= "<article_edit>";
		$xml .= "<save_result>$save_result</save_result>";
		$article = new Article( 'contact' ); $xml .= $article->Xml();
		$xml .= "</article_edit>";
		
		$xml .= "</data>";
		
		$this->display( 'contacts_edit', $xml );
	}
	
	function contacts_save()
	{
		$erx = "";
		
		$obj = new Article( $_REQUEST['article']['id'] );
		
		$attachment = new Attachment();
		if( $attachment->upload('image') )
		{
			$_REQUEST['article']['image'] = $attachment->filename;
		}
		
		if( $obj->Save( $_REQUEST['article'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->contacts_edit( 'true' );
		}
		else
		{
			$this->contacts_edit( 'false' );
		}
	}
	
	function del_image()
	{
		$article = new Article( $_REQUEST['id'] );
		$article->DeleteImage();
		$this->contacts_edit( 'true' );
	}
	
	function del_media()
	{
		$image = new Image( $_REQUEST['image_id'] );
		$image->Delete();
		$this->contacts_edit( 'true' );
	}
	
	function move_up_gallery()
	{
		$t = new MysqlTable('images');
		$t->move_position_up( $_REQUEST['image_id'], $this->folder, "parent_id={$_REQUEST['id']}" );
		$this->contacts_edit( 'scroll_to_media' );
	}
	
	function move_down_gallery()
	{
		$t = new MysqlTable('images');
		$t->move_position_down( $_REQUEST['image_id'], $this->folder, "parent_id={$_REQUEST['id']}" );
		$this->contacts_edit( 'scroll_to_media' );
	}
}

?>
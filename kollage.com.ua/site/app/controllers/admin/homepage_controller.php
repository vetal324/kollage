<?php
/*****************************************************
 Class v.1.0, 02.2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

include_once( 'base_controller.php' );

class homepage_controller extends base_controller
{
	var $folder = 'homepage';
	
	function homepage_controller()
	{
		parent::base_controller();
		
		$this->InitSettingsMenu();
	}
	
	function index()
	{
		$this->edit();
	}
	
	function edit( $save_result="" )
	{
		$this->bigtabs->select_child( 'settings' );
		$this->menuleft->select_child( 'homepage' );
		
		$this->method_name = 'edit';
		$xml = "<data>";
		
		$xml .= "<article_edit>";
		$xml .= "<save_result>$save_result</save_result>";
		$article = new Article( $this->folder ); $xml .= $article->Xml();
		$xml .= "</article_edit>";
		
		$xml .= "</data>";
		
		$this->display( 'edit', $xml );
	}
	
	function save()
	{
		$erx = "";
		
		$obj = new Article( $_REQUEST['article']['id'] );
		
		$attachment = new Attachment();
		if( $attachment->upload('image') )
		{
			$_REQUEST['article']['image'] = $attachment->filename;
		}
		
		if( $obj->Save( $_REQUEST['article'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->edit( 'true' );
		}
		else
		{
			$this->edit( 'false' );
		}
	}
	
	function del_image()
	{
		$article = new Article( $_REQUEST['id'] );
		$article->DeleteImage();
		$this->edit( 'true' );
	}
	
	function del_media()
	{
		$image = new Image( $_REQUEST['image_id'] );
		$image->Delete();
		$this->edit( 'true' );
	}
	
	function move_up_gallery()
	{
		$t = new MysqlTable('images');
		$t->move_position_up( $_REQUEST['image_id'], $this->folder, "parent_id={$_REQUEST['id']}" );
		$this->edit( 'scroll_to_media' );
	}
	
	function move_down_gallery()
	{
		$t = new MysqlTable('images');
		$t->move_position_down( $_REQUEST['image_id'], $this->folder, "parent_id={$_REQUEST['id']}" );
		$this->edit( 'scroll_to_media' );
	}
}

?>
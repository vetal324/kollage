<?php
/*****************************************************
Class v.1.0, 02.2017
Copyright Vitaliy Koshelenko vetal324@gmail.com
 ******************************************************/

class books_controller extends base_controller {
    var $folder = 'books';

    function books_controller() {
        parent::base_controller();
        $this->menuleft->add_item(new WEMenuItem("book_types", $this->dictionary->get('/locale/books/menuleft/book_types'), "?{$this->controller_name}.book_types" ) );
    }

    function index() {
        $this->book_types();
    }

    /*================================================================================================================*/
    /*=============================================== BOOK TYPES  ====================================================*/
    /*================================================================================================================*/

    /*========================================= Books types list =====================================================*/
    function book_types(){

        $this->bigtabs->select_child('book_types');
        $this->menuleft->select_child('book_types');
        $this->smalltabs->del_all();

        $xml = "<data>";
        $xml .= "<list>";

        $bookTypesTable = new MysqlTable('book_types');
        $bookTypesTable->find_all('', 'position,id');
        $bookTypes = $bookTypesTable->data;

        foreach($bookTypes as $bookTypeRow) {
            $bookType = new BookType($bookTypeRow['id']);
            $bookTypeXml = $bookType->Xml();
            $xml .= $bookTypeXml;
        }

        $xml .= "<last_position>{$bookType->position}</last_position>";

        $xml .= "</list>";
        $xml .= "</data>";

        $this->display('book_types_list', $xml);
    }

    /*========================================= Books types edit =====================================================*/
    function book_type_edit($save_result=""){

        $last_position = intval( $_REQUEST['last_position'] );

        $this->bigtabs->select_child('book_types');
        $this->menuleft->select_child('book_types');
        $this->smalltabs->del_all();

        $xml = "<data>";
        $xml .= "<edit>";
        $xml .= "<save_result>$save_result</save_result>";
        $xml .= "<last_position>$last_position</last_position>";
        $o = new BookType( $_REQUEST['id'] );
        $xml .= $o->Xml();
        $xml .= "</edit>";
        $xml .= "</data>";

        $this->display('book_type_edit', $xml);
    }

    /*========================================= Books types save =====================================================*/
    function book_type_save() {
        $obj = new BookType( $_REQUEST['object']['id'] );

        $last_position = intval( $_REQUEST['last_position'] );
        if ($last_position > 0 ) {
            $_REQUEST['object']['position'] = $last_position + 1;
        } else {
            $_REQUEST['object']['position'] = 1;
        }

        $attachment = new Attachment();
        if( $attachment->upload('image')) {
            $_REQUEST['object']['image'] = $attachment->filename;
            $io = new AImage( SITEROOT . '/'. $_ENV['data_path'] .'/'. $attachment->filename );
            $io->CopyResize( SITEROOT . '/'. $_ENV['data_path'] .'/thumbnail-'. $attachment->filename, 200, 200 );
        }
        $bookObj = $_REQUEST['object'];
        if( $obj->Save($bookObj) ) {
            $_REQUEST['id'] = $obj->id;
            $this->book_types();
        } else {
            $this->book_type_edit('false');
        }
    }

    /*=========================================== Books type delete  =================================================*/
    function book_type_del() {
        $o = new BookType( $_REQUEST['id'] );
        $o->Delete();
        $this->book_types();
    }

    /*================================================================================================================*/
    /*============================================= BOOK OPTIONS  ====================================================*/
    /*================================================================================================================*/


    /*========================================== Books options list ==================================================*/
    function book_options(){
        $type_id = $_REQUEST['type_id'];

        $this->bigtabs->select_child('book_types');
        $this->menuleft->select_child('book_types');
        $this->smalltabs->del_all();

        $xml = "<data>";
        $xml .= "<list>";

        $bookOptionsTable = new MysqlTable('book_options');
        $bookOptionsTable->find_all('type_id='.$type_id, 'position,id');
        $bookOptions = $bookOptionsTable->data;
        foreach($bookOptions as $bookOptionRow) {
            $bookOption = new BookOption($bookOptionRow['id']);
            $bookOptionXml = $bookOption->Xml();
            $xml .= $bookOptionXml;
        }

        $xml .= "<last_position>{$bookOption->position}</last_position>";

        $o = new BookType($type_id);
        $bookTypeXml = $o->Xml(false);
        $xml .= $bookTypeXml;

        $xml .= "</list>";
        $xml .= "</data>";

        $this->display('book_options_list', $xml);
    }

    /*========================================= Books options edit =====================================================*/
    function book_option_edit($save_result=""){

        $type_id = $_REQUEST['type_id'];

        $last_position = intval( $_REQUEST['last_position'] );

        $this->bigtabs->select_child('book_types');
        $this->menuleft->select_child('book_types');
        $this->smalltabs->del_all();

        $xml = "<data>";
        $xml .= "<edit>";
        $xml .= "<save_result>$save_result</save_result>";
        $xml .= "<last_position>$last_position</last_position>";

        $o = new BookOption( $_REQUEST['id'] );
        $xml .= $o->Xml();

        $o = new BookType($type_id);
        $bookTypeXml = $o->Xml(false);
        $xml .= $bookTypeXml;

        $xml .= "</edit>";
        $xml .= "</data>";

        $this->display('book_option_edit', $xml);
    }

    /*======================================= Books options save =====================================================*/
    function book_option_save() {
        $obj = new BookOption( $_REQUEST['object']['id'] );
        $last_position = intval( $_REQUEST['last_position'] );
        if($last_position > 0 ) {
            $_REQUEST['object']['position'] = $last_position + 1;
        } else {
            $_REQUEST['object']['position'] = 1;
        }

        $o = $_REQUEST['object'];
        if ($o['price_order'] == '') {
            $o['price_order'] = 0;
        }
        if ($o['price_item'] == '') {
            $o['price_item'] = 0;
        }
        if ($o['price_page'] == '') {
            $o['price_page'] = 0;
        }
        if ($o['name'] == '') {
            $o['name'] = '-';
        }
        if ($o['description'] == '') {
            $o['description'] = '-';
        }


        $_REQUEST['type_id'] = $_REQUEST['object']['type_id'];

        if( $obj->Save($o) ) {
            $_REQUEST['id'] = $obj->id;
            $this -> book_options();
        } else {
            $this->book_option_edit('false');
        }
    }

    /*========================================= Books option delete  =================================================*/
    function book_option_del() {
        $o = new BookOption( $_REQUEST['id'] );
        $o->Delete();
        $this->book_options();
    }

    /*================================================================================================================*/
    /*============================================= BOOK FORMATS  ====================================================*/
    /*================================================================================================================*/

    /*========================================== Books formats list ==================================================*/
    function book_formats(){

        $type_id = $_REQUEST['type_id'];

        $this->bigtabs->select_child('book_formats');
        $this->menuleft->select_child('book_formats');
        $this->smalltabs->del_all();

        $xml = "<data>";
        $xml .= "<list>";

        $bookFormatsTable = new MysqlTable('book_formats');
        $bookFormatsTable->find_all('type_id='.$type_id, 'position,id');
        $bookFormat = $bookFormatsTable->data;
        foreach($bookFormat as $bookFormatRow) {
            $bookFormat = new BookFormat($bookFormatRow['id']);
            $bookFormatXml = $bookFormat->Xml();
            $xml .= $bookFormatXml;
        }

        $xml .= "<last_position>{$bookFormat->position}</last_position>";

        $o = new BookType($type_id);
        $bookTypeXml = $o->Xml(false);
        $xml .= $bookTypeXml;

        $xml .= "</list>";
        $xml .= "</data>";

        $this->display('book_formats_list', $xml);
    }

    /*========================================== Books formats edit  =================================================*/
    function book_format_edit( $save_result="" ) {

        $last_position = intval( $_REQUEST['last_position'] );

        $this->bigtabs->select_child('book_formats');
        $this->menuleft->select_child('book_formats');
        $this->smalltabs->del_all();

        $type_id = $_REQUEST['type_id'];

        $xml = "<data>";
        $xml .= "<edit>";
        $xml .= "<save_result>$save_result</save_result>";
        $xml .= "<last_position>$last_position</last_position>";

        $o = new BookFormat( $_REQUEST['id'] );
        $xml .= $o->Xml();

        $o = new BookType($type_id);
        $bookTypeXml = $o->Xml(false);
        $xml .= $bookTypeXml;

        $xml .= "</edit>";
        $xml .= "</data>";

        $this->display('book_format_edit', $xml);
    }

    /*========================================== Books formats save  =================================================*/
    function book_format_save() {

        $obj = new BookFormat( $_REQUEST['object']['id'] );

        $last_position = intval( $_REQUEST['last_position'] );
        if ($last_position > 0 ) {
            $_REQUEST['object']['position'] = $last_position + 1;
        } else {
            $_REQUEST['object']['position'] = 1;
        }

        $attachment = new Attachment();
        if( $attachment->upload('image')) {
            $_REQUEST['object']['image'] = $attachment->filename;
            $io = new AImage( SITEROOT . '/'. $_ENV['data_path'] .'/'. $attachment->filename );
            $io->CopyResize( SITEROOT . '/'. $_ENV['data_path'] .'/thumbnail-'. $attachment->filename, 200, 200 );
        }
        $bookObj = $_REQUEST['object'];

        $_REQUEST['type_id'] = $_REQUEST['object']['type_id'];

        if( $obj->Save($bookObj) ) {
            $_REQUEST['id'] = $obj->id;
            $this->book_formats();
        } else {
            $this->book_format_edit( 'false' );
        }
    }

    /*======================================== Books formats delete  =================================================*/
    function book_format_del() {
        $o = new BookFormat( $_REQUEST['id'] );
        $o->Delete();
        $this->book_formats();
    }

    /*================================================================================================================*/
    /*============================================== COVER TYPES  ====================================================*/
    /*================================================================================================================*/

    /*========================================== Books covers list ===================================================*/
    function book_covers() {
        $format_id = $_REQUEST['format_id'];

        $xml = "<data>";
        $xml .= "<list>";
        $bookCoverTypesTable = new MysqlTable('book_cover_types');
        $bookCoverTypesTable->find_all('format_id='.$format_id, 'position,id');
        $bookCoverTypes = $bookCoverTypesTable->data;
        foreach($bookCoverTypes as $bookCoverTypeRow) {
            $bookCoverType = new BookCoverType($bookCoverTypeRow['id']);
            $bookTypeXml = $bookCoverType->Xml();
            $xml .= $bookTypeXml;
        }

        $xml .= "<last_position>{$bookCoverType->position}</last_position>";

        $bookFormatObj = new BookFormat($format_id);
        $bookFormatXml = $bookFormatObj->Xml();
        $xml .= $bookFormatXml;

        $type_id = $bookFormatObj->type_id;
        $bookTypeObj = new BookType($type_id);
        $bookTypeXml = $bookTypeObj->Xml(false);
        $xml .= $bookTypeXml;

        $xml .= "</list>";
        $xml .= "</data>";

        $this->display('book_cover_list', $xml);
    }


    function book_cover_edit($save_result=""){

        $last_position = intval( $_REQUEST['last_position'] );

        $this->bigtabs->select_child('book_formats');
        $this->menuleft->select_child('book_formats');
        $this->smalltabs->del_all();

        $format_id = $_REQUEST['format_id'];

        $xml = "<data>";
        $xml .= "<edit>";
        $xml .= "<save_result>$save_result</save_result>";
        $xml .= "<last_position>$last_position</last_position>";

        $o = new BookCoverType( $_REQUEST['id'] );
        $xml .= $o->Xml();

        $o = new BookFormat($format_id);
        $xml .= $o->Xml();

        $o = new BookType($o->type_id);
        $bookTypeXml = $o->Xml(false);
        $xml .= $bookTypeXml;

        $xml .= "</edit>";
        $xml .= "</data>";

        $this->display('book_cover_edit', $xml);
    }

    /*============================================ Books cover save  =================================================*/
    function book_cover_save() {

        $obj = new BookCoverType( $_REQUEST['object']['id'] );

        $last_position = intval( $_REQUEST['last_position'] );
        if ($last_position > 0 ) {
            $_REQUEST['object']['position'] = $last_position + 1;
        } else {
            $_REQUEST['object']['position'] = 1;
        }

        $attachment = new Attachment();
        if( $attachment->upload('image')) {
            $_REQUEST['object']['image'] = $attachment->filename;
            $io = new AImage( SITEROOT . '/'. $_ENV['data_path'] .'/'. $attachment->filename );
            $io->CopyResize( SITEROOT . '/'. $_ENV['data_path'] .'/thumbnail-'. $attachment->filename, 200, 200 );
        }

        $bookObj = $_REQUEST['object'];

        $_REQUEST['format_id'] = $_REQUEST['object']['format_id'];

        if( $obj->Save($bookObj) ) {
            $_REQUEST['id'] = $obj->id;
            $this->book_covers();
        } else {
            $this->book_cover_edit('false');
        }
    }

    /*========================================== Books cover delete  =================================================*/
    function book_cover_del() {
        $o = new BookCoverType( $_REQUEST['id'] );
        $o->Delete();
        $this->book_covers();
    }

    /*================================================================================================================*/
    /*======================================= COVER PAPER TYPES ======================================================*/
    /*================================================================================================================*/

    /*=================================Books covers paper types list =================================================*/
    function book_cover_paper_types() {
        $cover_id = $_REQUEST['cover_id'];

        $xml = "<data>";
        $xml .= "<list>";
        $bookCoverPaperTypeTable = new MysqlTable('book_cover_paper_types');
        $bookCoverPaperTypeTable->find_all('cover_id='.$cover_id, 'position,id');
        $bookCoverPaperTypes = $bookCoverPaperTypeTable->data;
        foreach($bookCoverPaperTypes as $bookCoverPaperTypeRow) {
            $bookCoverPaperType = new BookCoverPaperType($bookCoverPaperTypeRow['id']);
            $bookCoverPaperTypeXml = $bookCoverPaperType->Xml();
            $xml .= $bookCoverPaperTypeXml;
        }

        $xml .= "<last_position>{$bookCoverPaperType->position}</last_position>";

        $bookCoverObj = new BookCoverType($cover_id);
        $bookCoverObjXml = $bookCoverObj->Xml();
        $xml .= $bookCoverObjXml;

        $format_id = $bookCoverObj->format_id;
        $bookFormatObj = new BookFormat($format_id);
        $bookFormatXml = $bookFormatObj->Xml();
        $xml .= $bookFormatXml;

        $type_id = $bookFormatObj->type_id;
        $bookTypeObj = new BookType($type_id);
        $bookTypeXml = $bookTypeObj->Xml(false);
        $xml .= $bookTypeXml;

        $xml .= "</list>";
        $xml .= "</data>";

        $this->display('book_cover_paper_types_list', $xml);
    }

    /*================================= Books covers paper type edit =================================================*/
    function book_cover_paper_type_edit($save_result=""){

        $last_position = intval( $_REQUEST['last_position'] );

        $this->bigtabs->select_child('book_formats');
        $this->menuleft->select_child('book_formats');
        $this->smalltabs->del_all();

        $cover_id = $_REQUEST['cover_id'];

        $xml = "<data>";
        $xml .= "<edit>";
        $xml .= "<save_result>$save_result</save_result>";
        $xml .= "<last_position>$last_position</last_position>";

        $o = new BookCoverPaperType( $_REQUEST['id'] );
        $xml .= $o->Xml();

        $o = new BookCoverType($cover_id);
        $xml .= $o->Xml();

        $o = new BookFormat($o->format_id);
        $xml .= $o->Xml();

        $o = new BookType($o->type_id);
        $bookTypeXml = $o->Xml(false);
        $xml .= $bookTypeXml;

        $xml .= "</edit>";
        $xml .= "</data>";

        $this->display('book_cover_paper_type_edit', $xml);
    }

    /*================================= Books covers paper type save =================================================*/
    function book_cover_paper_type_save() {

        $obj = new BookCoverPaperType( $_REQUEST['object']['id'] );

        $last_position = intval( $_REQUEST['last_position'] );
        if ($last_position > 0 ) {
            $_REQUEST['object']['position'] = $last_position + 1;
        } else {
            $_REQUEST['object']['position'] = 1;
        }

        $bookObj = $_REQUEST['object'];

        if ($_REQUEST['object']['price'] == '') {
            $bookObj['price'] = 0;
        }
        if ($_REQUEST['object']['density'] == '') {
            $bookObj['density'] = 0;
        }
        if ($_REQUEST['object']['name'] == '') {
            $bookObj['name'] = '-';
        }
        if ($_REQUEST['object']['description'] == '') {
            $bookObj['description'] = '-';
        }

        $_REQUEST['cover_id'] = $_REQUEST['object']['cover_id'];

        $coverId = $bookObj['cover_id'];
        $coverType = new BookCoverType($coverId);
        $formatId = $coverType->format_id;
        $format = new BookFormat($formatId);
        $typeId = $format->type_id;

        if (strcmp($format->name, 'A4') == 0) {

            $price = $bookObj['price'];
            $a4Price = floatval($price);
            $a5Price = $a4Price / 2;
            $a6Price = $a4Price / 4;

            //ChromePhp::log('a4Price: ', $a4Price);
            //ChromePhp::log('a5Price: ', $a5Price);
            //ChromePhp::log('a6Price: ', $a6Price);

            $paperName = $bookObj['name'];

            $bookCoverPaperTypesTable = new MysqlTable('book_cover_paper_types');
            $bookCoverPaperTypesTable->find_all("name='$paperName'", 'cover_id,name');
            $paperTypes = $bookCoverPaperTypesTable->data;

            foreach ($paperTypes as $paperTypeRow) {
                $paperTypeId = $paperTypeRow['id'];
                $paperType = new BookCoverPaperType($paperTypeId);
                if (strcmp($paperType->name, $paperName) == 0) {
                    $curCoverId = $paperType->cover_id;
                    $curCoverType = new BookCoverType($curCoverId);
                    $curFormatId = $curCoverType->format_id;
                    $curFormat = new BookFormat($curFormatId);
                    $curTypeId = $curFormat->type_id;
                    if ($curTypeId == $typeId) {
                        if (strcmp($curFormat->name, 'A5') == 0) {
                            //A5 format
                            $paperType->price = $a5Price;
                            $paperType->Save();
                        } else if (strcmp($curFormat->name, 'A6') == 0) {
                            //A6 format
                            $paperType->price = $a6Price;
                            $paperType->Save();
                        }
                    }
                }
            }
        }

        if( $obj->Save($bookObj) ) {
            $_REQUEST['id'] = $obj->id;
            $this->book_cover_paper_types();
        } else {
            $this->book_cover_paper_type_edit('false');
        }
    }

    /*================================= Books covers paper type delete ===============================================*/
    function book_cover_paper_type_del() {
        $o = new BookCoverPaperType( $_REQUEST['id'] );
        $o->Delete();
        $this->book_cover_paper_types();
    }

    /*================================================================================================================*/
    /*=======================================  COVER LAMINATION ======================================================*/
    /*================================================================================================================*/

    /*================================  Books covers lamination list =================================================*/
    function book_cover_laminations() {
        $cover_id = $_REQUEST['cover_id'];

        $xml = "<data>";
        $xml .= "<list>";
        $bookCoverLaminationTable = new MysqlTable('book_cover_lamination');
        $bookCoverLaminationTable->find_all('cover_id='.$cover_id, 'position,id');
        $bookCoverLaminations = $bookCoverLaminationTable->data;
        foreach($bookCoverLaminations as $bookCoverLaminationRow) {
            $bookCoverLamination = new BookCoverLamination($bookCoverLaminationRow['id']);
            $bookCoverLaminationXml = $bookCoverLamination->Xml();
            $xml .= $bookCoverLaminationXml;
        }

        $xml .= "<last_position>{$bookCoverLamination->position}</last_position>";

        $bookCoverObj = new BookCoverType($cover_id);
        $bookCoverObjXml = $bookCoverObj->Xml();
        $xml .= $bookCoverObjXml;

        $format_id = $bookCoverObj->format_id;
        $bookFormatObj = new BookFormat($format_id);
        $bookFormatXml = $bookFormatObj->Xml();
        $xml .= $bookFormatXml;

        $type_id = $bookFormatObj->type_id;
        $bookTypeObj = new BookType($type_id);
        $bookTypeXml = $bookTypeObj->Xml(false);
        $xml .= $bookTypeXml;

        $xml .= "</list>";
        $xml .= "</data>";

        $this->display('book_cover_lamination_list', $xml);
    }

    /*================================= Books covers paper type edit =================================================*/
    function book_cover_lamination_edit($save_result=""){

        $last_position = intval( $_REQUEST['last_position'] );

        $this->bigtabs->select_child('book_formats');
        $this->menuleft->select_child('book_formats');
        $this->smalltabs->del_all();

        $cover_id = $_REQUEST['cover_id'];

        $xml = "<data>";
        $xml .= "<edit>";
        $xml .= "<save_result>$save_result</save_result>";
        $xml .= "<last_position>$last_position</last_position>";

        $o = new BookCoverLamination( $_REQUEST['id'] );
        $xml .= $o->Xml();

        $o = new BookCoverType($cover_id);
        $xml .= $o->Xml();

        $o = new BookFormat($o->format_id);
        $xml .= $o->Xml();

        $o = new BookType($o->type_id);
        $bookTypeXml = $o->Xml(false);
        $xml .= $bookTypeXml;

        $xml .= "</edit>";
        $xml .= "</data>";

        $this->display('book_cover_lamination_edit', $xml);
    }

    /*================================= Books covers lamination save =================================================*/
    function book_cover_lamination_save() {

        $obj = new BookCoverLamination( $_REQUEST['object']['id'] );

        $last_position = intval( $_REQUEST['last_position'] );
        if ($last_position > 0 ) {
            $_REQUEST['object']['position'] = $last_position + 1;
        } else {
            $_REQUEST['object']['position'] = 1;
        }

        $bookObj = $_REQUEST['object'];

        $_REQUEST['cover_id'] = $_REQUEST['object']['cover_id'];

        if ($_REQUEST['object']['price'] == '') {
            $bookObj['price'] = 0;
        }
        if ($_REQUEST['object']['name'] == '') {
            $bookObj['name'] = '-';
        }
        if ($_REQUEST['object']['description'] == '') {
             $bookObj['description'] = '-';
        }

        if( $obj->Save($bookObj) ) {
            $_REQUEST['id'] = $obj->id;
            $this->book_cover_laminations();
        } else {
            $this->book_cover_lamination_edit('false');
        }
    }

    /*================================= Books covers lamination delete ===============================================*/
    function book_cover_lamination_del() {
        $o = new BookCoverLamination( $_REQUEST['id'] );
        $o->Delete();
        $this->book_cover_laminations();
    }

    /*================================================================================================================*/
    /*================================== PAPER TYPES (INNER BLOCK) ===================================================*/
    /*================================================================================================================*/


    /*=========================================== Paper types list ===================================================*/
    function book_paper_types() {

        $format_id = $_REQUEST['format_id'];

        $xml = "<data>";
        $xml .= "<list>";
        $bookPaperTypesTable = new MysqlTable('book_paper_types');
        $bookPaperTypesTable->find_all('format_id='.$format_id, 'position,id');
        $bookPaperTypes = $bookPaperTypesTable->data;
        foreach($bookPaperTypes as $bookPaperTypeRow) {
            $bookPaperType = new BookPaperType($bookPaperTypeRow['id']);
            $paperTypeXml = $bookPaperType->Xml();
            $xml .= $paperTypeXml;
        }

        $xml .= "<last_position>{$paperTypeXml->position}</last_position>";

        $bookFormatObj = new BookFormat($format_id);
        $bookFormatXml = $bookFormatObj->Xml();
        $xml .= $bookFormatXml;

        $type_id = $bookFormatObj->type_id;
        $bookTypeObj = new BookType($type_id);
        $bookTypeXml = $bookTypeObj->Xml(false);
        $xml .= $bookTypeXml;

        $xml .= "</list>";
        $xml .= "</data>";

        $this->display('book_paper_types_list', $xml);
    }

    /*==================================== Paper type edit ===========================================================*/
    function book_paper_type_edit($save_result=""){

        $last_position = intval( $_REQUEST['last_position'] );

        $this->bigtabs->select_child('book_formats');
        $this->menuleft->select_child('book_formats');
        $this->smalltabs->del_all();

        $format_id = $_REQUEST['format_id'];

        $xml = "<data>";
        $xml .= "<edit>";
        $xml .= "<save_result>$save_result</save_result>";
        $xml .= "<last_position>$last_position</last_position>";

        $o = new BookPaperType( $_REQUEST['id'] );
        $xml .= $o->Xml();

        $o = new BookFormat($format_id);
        $xml .= $o->Xml();

        $o = new BookType($o->type_id);
        $bookTypeXml = $o->Xml(false);
        $xml .= $bookTypeXml;

        $xml .= "</edit>";
        $xml .= "</data>";

        $this->display('book_paper_type_edit', $xml);
    }

    /*====================================== Paper type save  ========================================================*/
    function book_paper_type_save() {

        $obj = new BookPaperType( $_REQUEST['object']['id'] );


        $last_position = intval( $_REQUEST['last_position'] );
        if ($last_position > 0 ) {
            $_REQUEST['object']['position'] = $last_position + 1;
        } else {
            $_REQUEST['object']['position'] = 1;
        }

        $bookPaperTypeObj = $_REQUEST['object'];

        $formatId = $_REQUEST['object']['format_id'];
        $format = new BookFormat($formatId);
        $typeId = $format->type_id;

        if (strcmp($format->name, 'A4') == 0) {

            $price = $bookPaperTypeObj['price'];
            $a4Price = floatval($price);
            $a5Price = $a4Price / 2;
            $a6Price = $a4Price / 4;

            //ChromePhp::log('a4Price: ', $a4Price);
            //ChromePhp::log('a5Price: ', $a5Price);
            //ChromePhp::log('a6Price: ', $a6Price);

            $paperName = $bookPaperTypeObj['name'];

            $bookPaperTypesTable = new MysqlTable('book_paper_types');
            $bookPaperTypesTable->find_all("name='$paperName'", 'format_id,name');
            $paperTypes = $bookPaperTypesTable->data;

            foreach ($paperTypes as $paperTypeRow) {
                $paperTypeId = $paperTypeRow['id'];
                $paperType = new BookPaperType($paperTypeId);
                if (strcmp($paperType->name, $paperName) == 0) {
                    $curFormatId = $paperType->format_id;
                    if ($curFormatId != $formatId) {
                        //The same paper for different format
                        $curFormat = new BookFormat($curFormatId);
                        if ($curFormat->type_id == $typeId) {
                            if (strcmp($curFormat->name, 'A5') == 0) {
                                //A5 format
                                $paperType->price = $a5Price;
                                $paperType->Save();
                            } else if (strcmp($curFormat->name, 'A6') == 0) {
                                //A6 format
                                $paperType->price = $a6Price;
                                $paperType->Save();
                            }
                        }
                    }
                }
            }
        }

        $_REQUEST['format_id'] = $_REQUEST['object']['format_id'];

        if( $obj->Save($bookPaperTypeObj) ) {
            $_REQUEST['id'] = $obj->id;
            $this->book_paper_types();
        } else {
            $this->book_paper_type_edit('false');
        }
    }

    /*======================================== Paper type delete =====================================================*/
    function book_paper_type_del() {
        $o = new BookPaperType( $_REQUEST['id'] );
        $o->Delete();
        $this->book_paper_types();
    }


    /*================================================================================================================*/
    /*================================== PRINT TYPES (INNER BLOCK) ===================================================*/
    /*================================================================================================================*/


    /*=========================================== Print types list ===================================================*/
    function book_print_types() {

        $paper_id = $_REQUEST['paper_id'];

        $xml = "<data>";
        $xml .= "<list>";
        $bookPrintTypesTable = new MysqlTable('book_print_types');
        $bookPrintTypesTable->find_all('paper_id='.$paper_id, 'position,id');
        $bookPrintTypes = $bookPrintTypesTable->data;
        foreach($bookPrintTypes as $bookPrintTypeRow) {
            $bookPrintType = new BookPrintType($bookPrintTypeRow['id']);
            $printTypeXml = $bookPrintType->Xml();
            $xml .= $printTypeXml;
        }

        $xml .= "<last_position>{$bookPrintType->position}</last_position>";

        $paperTypeObj = new BookPaperType($paper_id);
        $paperTypeXml = $paperTypeObj->Xml();
        $xml .= $paperTypeXml;
        $format_id = $paperTypeObj->format_id;

        $bookFormatObj = new BookFormat($format_id);
        $bookFormatXml = $bookFormatObj->Xml();
        $xml .= $bookFormatXml;

        $type_id = $bookFormatObj->type_id;
        $bookTypeObj = new BookType($type_id);
        $bookTypeXml = $bookTypeObj->Xml(false);
        $xml .= $bookTypeXml;

        $xml .= "</list>";
        $xml .= "</data>";

        $this->display('book_print_types_list', $xml);
    }

    /*===================================== Print type edit ==========================================================*/
    function book_print_type_edit($save_result=""){

        $last_position = intval( $_REQUEST['last_position'] );

        $this->bigtabs->select_child('book_formats');
        $this->menuleft->select_child('book_formats');
        $this->smalltabs->del_all();

        $paper_id = $_REQUEST['paper_id'];

        $xml = "<data>";
        $xml .= "<edit>";
        $xml .= "<save_result>$save_result</save_result>";
        $xml .= "<last_position>$last_position</last_position>";

        $o = new BookPrintType( $_REQUEST['id'] );
        $xml .= $o->Xml();

        $o = new BookPaperType($paper_id);
        $xml .= $o->Xml();
        $format_id = $o->format_id;

        $o = new BookFormat($format_id);
        $xml .= $o->Xml();

        $o = new BookType($o->type_id);
        $bookTypeXml = $o->Xml(false);
        $xml .= $bookTypeXml;

        $xml .= "</edit>";
        $xml .= "</data>";

        $this->display('book_print_type_edit', $xml);
    }

    /*======================================= Books print type save  =================================================*/
    function book_print_type_save() {

        $obj = new BookPrintType( $_REQUEST['object']['id'] );


        $last_position = intval( $_REQUEST['last_position'] );
        if ($last_position > 0 ) {
            $_REQUEST['object']['position'] = $last_position + 1;
        } else {
            $_REQUEST['object']['position'] = 1;
        }

        $bookPrTypeObj = $_REQUEST['object'];

        $is_color = $bookPrTypeObj['is_color'];
        if ($is_color != null) {
            $bookPrTypeObj['is_color'] = 1;
        } else {
            $bookPrTypeObj['is_color'] = 0;
        }

        $paperId = $_REQUEST['object']['paper_id'];
        $_REQUEST['paper_id'] = $paperId;

        $paperType = new BookPaperType($paperId);
        $formatId = $paperType->format_id;
        $format = new BookFormat($formatId);
        $typeId = $format->type_id;

        //ChromePhp::log('book_print_type_save, $format: ', $format);

        if (strcmp($format->name, 'A4') == 0) {

            $price = $bookPrTypeObj['price'];
            $a4Price = floatval($price);
            $a5Price = $a4Price / 2;
            $a6Price = $a4Price / 4;

            //ChromePhp::log('a4Price: ', $a4Price);
            //ChromePhp::log('a5Price: ', $a5Price);
            //ChromePhp::log('a6Price: ', $a6Price);

            $printName = $bookPrTypeObj['name'];
            $minPrintCount = $bookPrTypeObj['min_print_count'];

            $bookPrintTypesTable = new MysqlTable('book_print_types');
            $bookPrintTypesTable->find_all("name='$printName'", 'paper_id,name');
            $printTypes = $bookPrintTypesTable->data;

            foreach ($printTypes as $printTypeRow) {
                $printTypeId = $printTypeRow['id'];
                $printType = new BookPrintType($printTypeId);
                if (strcmp($printType->name, $printName) == 0) {
                    $curPaperId = $printType->paper_id;
                    $curPaper = new BookPaperType($curPaperId);
                    $curFormatId = $curPaper->format_id;
                    $curFormat = new BookFormat($curFormatId);
                    if ($typeId == $curFormat->type_id) {
                        if (strcmp($curFormat->name, 'A4') == 0) {
                            $printType->price = $a4Price;
                        } else if (strcmp($curFormat->name, 'A5') == 0) {
                            $printType->price = $a5Price;
                        } else if (strcmp($curFormat->name, 'A6') == 0) {
                            $printType->price = $a6Price;
                        }
                        $printType->is_color = $is_color;
                        $printType->min_print_count = $minPrintCount;
                        $printType->Save();
                    }
                }
            }
        }

        if( $obj->Save($bookPrTypeObj) ) {
            $_REQUEST['id'] = $obj->id;
            $this->book_print_types();
        } else {
            $this->book_print_type_edit('false');
        }
    }

    /*===================================== Books print type delete  =================================================*/
    function book_print_type_del() {
        $o = new BookPrintType( $_REQUEST['id'] );
        $o->Delete();
        $this->book_print_types();
    }


}

?>
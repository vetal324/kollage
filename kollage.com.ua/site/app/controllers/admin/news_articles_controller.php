<?php

include_once( 'base_controller.php' );
//require( SITEROOT .'/lib/facebook/facebook.php' );

class news_articles_controller extends base_controller
{
    var $folder = 'general';
    /*var $app_id = '153029671432516';
    var $app_secret = '3bb5a62974c939b459dfe711160eafcf';*/
    var $app_id = '929931727075636';
    var $app_secret = '117f1b6c86b03653f24384bbea389857';
    var $redirect_uri = 'http://kollage.com.ua/admin/?news_articles.fb_return';
    
    function news_controller()
    {
        parent::base_controller();
        
        //$this->menuleft->add_item( new WEMenuItem( "news", $this->dictionary->get('/locale/news/menuleft/news'), "?".$this->controller_name.".index" ) );
    }
    
    function post_url( $url, $params ) {
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, $url );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, http_build_query($params, null, '&') );
        $ret = curl_exec( $ch );
        curl_close( $ch );
        return $ret;
    }
  
    function get_app_access_token( $code ) {
        $url = 'https://graph.facebook.com/oauth/access_token';
        $params = array(
            "grant_type" => "authorization_code",
            "code" => $code,
            "client_id" => $this->app_id,
            "client_secret" => $this->app_secret,
            "redirect_uri" => $this->redirect_uri
        );
        $ret = $this->post_url( $url, $params );
        parse_str( $ret, $data );
        return( $data );
    }
  
    function fb_return() {
        if( $_SESSION['fb_state'] == $_REQUEST['state'] ) {
            $data = $this->get_app_access_token( $_REQUEST['code'] );
            //print_r($data);
            $_SESSION['fb_access_token'] = $data['access_token'];

            $article = new NewsArticle( $_SESSION['fb_state_news_id'] );
            if( $article ) {
                echo $this->post_url( 'https://graph.facebook.com/v2.5/kollagestudio/feed', array(
                    'access_token' => $access_token,
                    'message' => $article->header . $article->text
                ));
            }
        }
        $this->index();
    }
    
    function fb_login() {
        $state = md5( time()*12 );
        $_SESSION['fb_state'] = $state;
        $_SESSION['fb_state_news_id'] = $_REQUEST['id'];
        $auth_url = "https://www.facebook.com/v2.4/dialog/oauth?response_type=code&sdk=5.0.0&client_id={$this->app_id}&redirect_uri={$this->redirect_uri}&state={$state}&scope=publish_actions";
        redirect( $auth_url );
    }
    
    function index( $parameters='' )
    {
        $this->bigtabs->select_child( 'news_articles' );
        //$this->menuleft->select_child( 'news' );
        
        $this->method_name = 'list';
        $xml = "<data>";
        $xml .= "<articles>";
        
        $t = new MysqlTable( 'news_articles' );
        parent::define_pages( $t->get_count() );
        $t->find_all( '', 'publish_date desc,id desc', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
        foreach( $t->data as $a )
        {
            $article = new NewsArticle( $a['id'] );
            $xml .= $article->Xml();
        }
        
        if( !$article ) $article = new NewsArticle();
        $xml .= "<last_position>". $article->GetLastPosition("category_id=2") ."</last_position>";
        
        $xml .= "</articles>";
        $xml .= "<parameters>$parameters</parameters>";
        $xml .= "</data>";
        $this->display( 'list', $xml );
    }
    
    function edit( $save_result="" )
    {
        $this->bigtabs->select_child( 'news_articles' );
        //$this->menuleft->select_child( 'news' );
        
        $this->method_name = 'edit';
        $last_position = intval( $_REQUEST['last_position'] );
        $xml = "<data>";
        
        $xml .= "<article_edit>";
        $xml .= "<save_result>$save_result</save_result>";
        $xml .= "<last_position>$last_position</last_position>";
        $article = new NewsArticle( $_REQUEST['id'] );
        if( !$article->IsLoaded() )
        {
            $article->folder = $this->folder;
        }
        $xml .= $article->Xml();
        $xml .= "</article_edit>";
        
        $xml .= "</data>";
        
        $this->display( 'edit', $xml );
    }
    
    function save()
    {
        $erx = "";
        
        $obj = new NewsArticle( $_REQUEST['article']['id'] );
        $last_position = intval( $_REQUEST['last_position'] );
        if( $last_position > 0 ) $_REQUEST['article']['position'] = $last_position + 1;
        
        $_REQUEST['article']['text'] = DeleteHTMLComments( $_REQUEST['article']['text'] );
        if( $obj->Save( $_REQUEST['article'] ) )
        {
            $_REQUEST['id'] = $obj->id;
            $this->edit( 'true' );
        }
        else
        {
            $this->edit( 'false' );
        }
    }
    
    function del()
    {
        $this->delete();
    }
    
    function delete()
    {
        $article = new NewsArticle( $_REQUEST['id'] );
        $article->Delete();
        $this->index();
    }
    
    function del_marked()
    {
        $ids_str = $_REQUEST['ids'];
        $ids = split( ',', $ids_str );
        foreach( $ids as $id )
        {
            $o = new NewsArticle( $id );
            $o->Delete();
        }
        $this->index( 'scroll_to_list' );
    }
    
    function del_media()
    {
        $image = new Image( $_REQUEST['image_id'] );
        $image->Delete();
        $this->edit( 'true' );
    }
    
    function move_up()
    {
        $t = new MysqlTable('articles');
        $t->move_position_up( $_REQUEST['id'], '', "category_id=2" );
        $this->index( 'scroll_to_list' );
    }
    
    function move_down()
    {
        $t = new MysqlTable('news_articles');
        $t->move_position_down( $_REQUEST['id'], '', "category_id=2" );
        $this->index( 'scroll_to_list' );
    }
    
    function move_up_gallery()
    {
        $t = new MysqlTable('images');
        $article = new NewsArticle( $_REQUEST['id'] );
        $t->move_position_up( $_REQUEST['image_id'], $article->folder_for_child, "parent_id={$_REQUEST['id']}" );
        $this->edit( 'scroll_to_media' );
    }
    
    function move_down_gallery()
    {
        $t = new MysqlTable('images');
        $article = new NewsArticle( $_REQUEST['id'] );
        $t->move_position_down( $_REQUEST['image_id'], $article->folder_for_child, "parent_id={$_REQUEST['id']}" );
        $this->edit( 'scroll_to_media' );
    }
}

?>
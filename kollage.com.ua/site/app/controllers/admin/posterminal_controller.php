<?php
/*****************************************************
 Class v.1.0, 02.2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

include_once( 'base_controller.php' );

class posterminal_controller extends base_controller
{
    var $folder = 'posterminal';
    
    function posterminal_controller()
    {
        parent::base_controller();
        
        $this->menuleft->add_item( new WEMenuItem( "posterminal", $this->dictionary->get('/locale/posterminal/menuleft/posterminal'), "?".$this->controller_name.".index" ) );
    }
    
    function make_elements()
    {
        $retval = "";
        
        $retval .= $this->bigtabs->xml();
        $retval .= $this->smalltabs->xml();
        $retval .= $this->menuleft->xml();
        
        return( $retval );
    }
    
    function index( $parameters='' )
    {
        $this->bigtabs->select_child( 'posterminal' );
        $this->menuleft->select_child( 'posterminal' );
        $this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/shop/smalltabs/list'), "?".$this->controller_name.".index&page=". intval($_REQUEST['page'])) );
        
        $this->method_name = 'index';
        $xml = "<data>";
        $xml .= "<list>";
        
        $t = new MysqlTable( 'posterminal_operations' );
        parent::define_pages( $t->get_count("client_id>=0") );
        $t->find_all( "client_id>=0", 'id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
        foreach( $t->data as $a )
        {
            $o = new PosterminalOperation( $a['id'] );
            $xml .= $o->Xml();
        }
        
        $xml .= "</list>";
        $xml .= "<parameters>$parameters</parameters>";
        $xml .= "</data>";
        
        $this->display( 'list', $xml );
    }
    
    function edit( $save_result="" )
    {
        $this->bigtabs->select_child( 'posterminal' );
        $this->menuleft->select_child( 'posterminal' );
        
        $this->method_name = 'edit';
        
        $xml = "<data>";
        $xml .= $save_result;
        $xml .= "</data>";
        
        $this->display( 'edit', $xml );
    }
    
    function save()
    {
        $erx = "";
        
        $obj = new PosterminalOperation();
        
        if( $obj->Save( $_REQUEST['object'] ) )
        {
            $this->edit( "<reloader reload='yes' close='yes'></reloader>" );
        }
    }
}

?>
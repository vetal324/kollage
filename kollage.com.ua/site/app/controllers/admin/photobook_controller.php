<?php
/*****************************************************
 Class v.1.0, 08.2011
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

include_once( 'base_controller.php' );

class photobook_controller extends base_controller
{
	
	function photobook_controller()
	{
		parent::base_controller();
		
		$this->bigtabs->select_child( 'photobook' );
		
		$this->menuleft->add_item( new WEMenuItem( "orders_list1", $this->dictionary->get('/locale/insit/menuleft/orders_list1'), "?".$this->controller_name.".orders_list1" ) );
		$this->menuleft->add_item( new WEMenuItem( "orders_list2", $this->dictionary->get('/locale/insit/menuleft/orders_list2'), "?".$this->controller_name.".orders_list2" ) );
		$this->menuleft->add_item( new WEMenuItem( "orders_list3", $this->dictionary->get('/locale/insit/menuleft/orders_list3'), "?".$this->controller_name.".orders_list3" ) );
		$this->menuleft->add_item( new WEMenuItem( "orders_list4", $this->dictionary->get('/locale/insit/menuleft/orders_list4'), "?".$this->controller_name.".orders_list4" ) );
		$this->menuleft->add_item( new WEMenuSeparator() );
		$this->menuleft->add_item( new WEMenuItem( "settings", $this->dictionary->get('/locale/sop/menuleft/settings'), "?".$this->controller_name.".settings_edit" ) );
	}
	
	function index()
	{
		$this->orders_list1();
	}

	// <Orders>
	function orders_list1( $parameters='' )
	{
		$this->bigtabs->select_child( 'photobook' );
		$this->menuleft->select_child( 'orders_list1' );
		
		$this->method_name = 'orders_list1';
		$xml = "<data>";
		$xml .= "<list>";
		
		$t = new MysqlTable( 'insit_orders' );
		parent::define_pages( $t->get_count('status=1') );
		$t->find_all( 'status=1', 'id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$o = new Insit_Order( $a['id'] );
			$xml .= $o->Xml();
		}
		
		$xml .= "</list>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'orders_list1', $xml );
	}
	
	function orders_list2( $parameters='' )
	{
		$this->bigtabs->select_child( 'photobook' );
		$this->menuleft->select_child( 'orders_list2' );
		
		$this->method_name = 'orders_list2';
		$xml = "<data>";
		$xml .= "<list>";
		
		$t = new MysqlTable( 'insit_orders' );
		parent::define_pages( $t->get_count('status=2') );
		$t->find_all( 'status=2', 'id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$o = new Insit_Order( $a['id'] );
			$xml .= $o->Xml();
		}
		
		$xml .= "</list>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'orders_list2', $xml );
	}
	
	function orders_list3( $parameters='' )
	{
		$this->bigtabs->select_child( 'photobook' );
		$this->menuleft->select_child( 'orders_list3' );
		
		$this->method_name = 'orders_list3';
		$xml = "<data>";
		$xml .= "<list>";
		
		$t = new MysqlTable( 'insit_orders' );
		parent::define_pages( $t->get_count('status=3') );
		$t->find_all( 'status=3', 'id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$o = new Insit_Order( $a['id'] );
			$xml .= $o->Xml();
		}
		
		$xml .= "</list>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'orders_list3', $xml );
	}
	
	function orders_list4( $parameters='' )
	{
		$this->bigtabs->select_child( 'photobook' );
		$this->menuleft->select_child( 'orders_list4' );
		
		$this->method_name = 'orders_list4';
		$xml = "<data>";
		$xml .= "<list>";
		
		$t = new MysqlTable( 'insit_orders' );
		parent::define_pages( $t->get_count('status=4') );
		$t->find_all( 'status=4', 'id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$o = new Insit_Order( $a['id'] );
			$xml .= $o->Xml();
		}
		
		$xml .= "</list>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'orders_list4', $xml );
	}
	
	function order_del()
	{
		$o = new Insit_Order( $_REQUEST['id'] );
		$status = $o->status;
		$o->updated_at = date("Y-m-d H:i:s");
		$o->status = 99;
		$o->Save();

		switch( $status )
		{
			case 2:
				$this->orders_list2();
				break;
			case 3:
				$this->orders_list3();
				break;
			case 4:
				$this->orders_list4();
				break;
			case 1:
			default:
				$this->orders_list1();
				break;
		}
	}
	
	function orders_del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new Insit_Order( $id );
			if( !$status ) $status = $o->status;
			//$o->DeleteFolder();
			$o->status = 99;
			$o->Save();
		}
		
		switch( $status )
		{
			case 2:
				$this->orders_list2();
				break;
			case 3:
				$this->orders_list3();
				break;
			case 4:
				$this->orders_list4();
				break;
			case 1:
			default:
				$this->orders_list1();
				break;
		}
	}
	
	function orders_change_status_marked()
	{
		$status = $_REQUEST['status']; if( !$status ) $status = 3;
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new Insit_Order( $id );
			$o->status = $status;
			$o->Save();
		}
		
		switch( $status-1 )
		{
			case 2:
				$this->orders_list2();
				break;
			case 3:
				$this->orders_list3();
				break;
			case 4:
				$this->orders_list4();
				break;
			case 1:
			default:
				$this->orders_list1();
				break;
		}
	}
	
	function orders_change_status_all()
	{
		global $db;

		$status = $_REQUEST['status']; if( !$status ) $status = 3;
		$sql = "update insit_orders set status={$status} where status=". ($status-1);
		$db->query( $sql );
		
		switch( $status-1 )
		{
			case 2:
				$this->orders_list2();
				break;
			case 3:
				$this->orders_list3();
				break;
			case 4:
				$this->orders_list4();
				break;
			case 1:
			default:
				$this->orders_list1();
				break;
		}
	}
	
	function order_view()
	{
		$this->bigtabs->select_child( 'photobook' );

		$o = new Insit_Order( $_REQUEST['id'], true );
		switch( $o->status )
		{
			case 2:
				$this->menuleft->select_child( 'orders_list2' );
				break;
			case 3:
				$this->menuleft->select_child( 'orders_list3' );
				break;
			case 4:
				$this->menuleft->select_child( 'orders_list4' );
				break;
			case 1:
			default:
				$this->menuleft->select_child( 'orders_list1' );
				break;
		}
		
		
		$this->method_name = 'order_view';
		$xml = "<data>";
		
		if( $o->IsLoaded() )
		{
			$xml .= $o->Xml();
		}
		
		$xml .= "</data>";
		
		$this->display( 'order_view', $xml );
	}
	
	function order_send()
	{
		$o = new Insit_Order( $_REQUEST['id'] );
		$subject = $_REQUEST['subject'];
		$body = $_REQUEST['body'];
		if( $o->IsLoaded() )
		{
			//Send email to admin and client
			$settings = new Settings();
			$sop_settings = new InsitSettings();
			$mail = new AMail( 'windows-1251' );
				$mail->subject = $subject;
				
				$mail->from_name = $settings->from_name;
				$mail->from_email = $settings->from_email;
				$mail->to_name = $o->first_name ." ". $o->last_name;
				$mail->to_email = $o->email;
			$mail->Send( $body );
		}        
		$this->order_view();
	}
	// </Orders>
	
	// <Settings>
	function settings_edit( $save_result="" )
	{
		$this->bigtabs->select_child( 'photobook' );
		$this->menuleft->select_child( 'settings' );
		
		$this->method_name = 'settings_edit';
		$xml = "<data>";
		
		$xml .= "<edit>";
		$xml .= "<save_result>$save_result</save_result>";
		$o = new InsitSettings();
		$xml .= $o->Xml();
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'settings_edit', $xml );
	}
	
	function settings_save()
	{
		$erx = "";
		
		$obj = new InsitSettings();
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->settings_edit( 'true' );
		}
		else
		{
			$this->settings_edit( 'false' );
		}
	}
	// </Settings>

}

?>
<?php
/*****************************************************
 Class v.1.0, 02.2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

include_once( 'base_controller.php' );

class users_controller extends base_controller
{
	var $folder;
	
	function users_controller()
	{
		parent::base_controller();
		
		$this->bigtabs->select_child( 'settings' );
		$this->InitSettingsMenu();
	}
	
	function index( $parameters='' )
	{
		global $db;

		$this->menuleft->select_child( "users" );
		
		$this->method_name = 'list';
		$xml = "<data>";
		$xml .= "<users>";
		
		$t = new MysqlTable( 'users' );
		parent::define_pages( $t->get_count() );
		$rs = $db->query( "select id from users order by position limit ". ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		while( $a = $rs->fetch_row() )
		{
			$street = new User( $a[0] );
			
			if( $street->position == 0 )
			{
				$tp = new MysqlTable('users');
				$tp->fix_positions();
				$street->Load( $a['id'] );
			}
			
			$xml .= $street->Xml();
		}
		
		if( !$street ) $street = new User();
		$xml .= "<last_position>". $street->GetLastPosition() ."</last_position>";
		
		$xml .= "</users>";
		
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'list', $xml );
	}
	
	function edit( $save_result="" )
	{
		$last_position = intval( $_REQUEST['last_position'] );
		
		$this->menuleft->select_child( "users" );
		
		$this->method_name = 'edit';
		$xml = "<data>";
		
		$xml .= "<edit>";
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= "<last_position>$last_position</last_position>";
		$street = new User( $_REQUEST['id'] );
		if( !$street->IsLoaded() )
		{
			$street->folder = $this->folder;
		}
		$xml .= $street->Xml();
		
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'edit', $xml );
	}
	
	function save()
	{
		global $db;
		$erx = "";
		
		$obj = new User( $_REQUEST['object']['id'] );
		$last_position = intval( $_REQUEST['last_position'] );
		if( $last_position > 0 ) $_REQUEST['object']['position'] = $last_position + 1;
		
		if( $_REQUEST['pwd']!="" )
		{
			$_REQUEST['object']['pwd'] = md5( $_REQUEST['pwd'] );
		}
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$db->query( "delete from user_roles where user_id=".$obj->id );
			foreach( $_REQUEST['role_id'] as $role_id )
			{
				$db->query( "insert into user_roles (user_id,role_id) values({$obj->id},{$role_id})" );
			}
			
			$_REQUEST['id'] = $obj->id;
			$this->edit( 'true' );
		}
		else
		{
			$this->edit( 'false' );
		}
	}
	
	function del()
	{
		$this->delete();
	}
	
	function delete()
	{
		$street = new User( $_REQUEST['id'] );
		$street->Delete();
		$this->index();
	}
	
	function del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new User( $id );
			$o->Delete();
		}
		$this->index( 'scroll_to_list' );
	}
	
	function swap_position()
	{
		$src = new User( $_REQUEST['src'] );
		$src_position = $src->position;
		$dst = new User( $_REQUEST['dst'] );
		$src->position = $dst->position;
		$src->Save();
		$dst->position = $src_position;
		$dst->Save();
		$this->index( 'scroll_to_list' );
	}
}

?>
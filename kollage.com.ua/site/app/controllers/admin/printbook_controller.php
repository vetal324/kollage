<?php
/*****************************************************
 Class v.1.0, 08.2011
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

include_once( 'base_controller.php' );

class printbook_controller extends base_controller
{
	var $box_category_folder = 'box';
	var $cover_category_folder = 'cover';
	var $design_work_category_folder = 'design_work';
	
	function printbook_controller()
	{
		parent::base_controller();
		
		$this->bigtabs->select_child( 'printbook' );
		
		$this->menuleft->add_item( new WEMenuItem( "orders_list1", $this->dictionary->get('/locale/insit/menuleft/orders_list1'), "?".$this->controller_name.".orders_list1" ) );
		$this->menuleft->add_item( new WEMenuItem( "orders_list2", $this->dictionary->get('/locale/insit/menuleft/orders_list2'), "?".$this->controller_name.".orders_list2" ) );
		$this->menuleft->add_item( new WEMenuItem( "orders_list3", $this->dictionary->get('/locale/insit/menuleft/orders_list3'), "?".$this->controller_name.".orders_list3" ) );
		$this->menuleft->add_item( new WEMenuItem( "orders_list4", $this->dictionary->get('/locale/insit/menuleft/orders_list4'), "?".$this->controller_name.".orders_list4" ) );
		$this->menuleft->add_item( new WEMenuSeparator() );
		$this->menuleft->add_item( new WEMenuItem( "sizes_list", $this->dictionary->get('/locale/printbook/menuleft/sizes_list'), "?{$this->controller_name}.pb_sizes_list" ) );
		$this->menuleft->add_item( new WEMenuItem( "settings", $this->dictionary->get('/locale/sop/menuleft/settings'), "?".$this->controller_name.".settings_edit" ) );
		$this->menuleft->add_item( new WEMenuItem( "settings2", $this->dictionary->get('/locale/sop/menuleft/settings2'), "?".$this->controller_name.".settings_edit2" ) );
		$this->menuleft->add_item( new WEMenuItem( "deliveries_list", $this->dictionary->get('/locale/sop/menuleft/deliveries_list'), "?".$this->controller_name.".deliveries_list" ) );
		$this->menuleft->add_item( new WEMenuItem( "paymentways_list", $this->dictionary->get('/locale/sop/menuleft/paymentways_list'), "?".$this->controller_name.".paymentways_list" ) );
	}
	
	function index()
	{
		$this->orders_list1();
	}
	
	// <Photo book sizes>
	function pb_sizes_list( $parameters='' )
	{
		$this->bigtabs->select_child( 'printbook' );
		$this->menuleft->select_child( 'sizes_list' );
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/printbook/smalltabs/list'), "?".$this->controller_name.".pb_sizes_list") );
		
		$this->method_name = 'pb_sizes_list';
		$xml = "<data>";
		$xml .= "<list>";
		
		$t = new MysqlTable( 'pb_sizes' );
		parent::define_pages( $t->get_count('status=1') );
		$t->find_all( 'status=1', 'position,id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$o = new PBSize( $a['id'] );
			
			if( $o->position == 0 )
			{
				$tp = new MysqlTable('pb_sizes');
				$tp->fix_positions( '', "status=1" );
				$o->Load( $a['id'] );
			}
			
			$xml .= $o->Xml();
		}
		
		$xml .= "<last_position>{$o->position}</last_position>";
		
		$xml .= "</list>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'pb_sizes_list', $xml );
	}
	
	function pb_size_edit( $save_result="" )
	{
		$last_position = intval( $_REQUEST['last_position'] );
		
		$this->bigtabs->select_child( 'printbook' );
		$this->menuleft->select_child( 'sizes_list' );
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/printbook/smalltabs/list'), "?".$this->controller_name.".pb_sizes_list") );
		
		$this->method_name = 'pb_size_edit';
		$xml = "<data>";
		
		$xml .= "<edit>";
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= "<last_position>$last_position</last_position>";
		$o = new PBSize( $_REQUEST['id'] );
		$xml .= $o->Xml();
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'pb_size_edit', $xml );
	}
	
	function pb_size_save()
	{
		$erx = "";
		
		$obj = new PBSize( $_REQUEST['object']['id'] );
		$last_position = intval( $_REQUEST['last_position'] );
		if( $last_position > 0 ) $_REQUEST['object']['position'] = $last_position + 1;
		
		$attachment = new Attachment();
		if( $attachment->upload('image') )
		{
			$_REQUEST['object']['image'] = $attachment->filename;
			$io = new AImage( SITEROOT . '/'. $_ENV['data_path'] .'/'. $attachment->filename );
			$io->CopyResize( SITEROOT . '/'. $_ENV['data_path'] .'/thumbnail-'. $attachment->filename, 200, 200 );
		}
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->pb_size_edit( 'true' );
		}
		else
		{
			$this->pb_size_edit( 'false' );
		}
	}
	
	function pb_size_del()
	{
		$o = new PBSize( $_REQUEST['id'] );
		$o->Delete();
		$this->pb_sizes_list( 'scroll_to_list' );
	}
	
	function pb_sizes_del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new PBSize( $id );
			$o->Delete();
		}
		$this->pb_sizes_list( 'scroll_to_list' );
	}
	
	function pb_size_del_image()
	{
		$o = new PBSize( $_REQUEST['id'] );
		$o->DeleteImage();
		$this->pb_size_edit( 'true' );
	}
	
	function pb_sizes_swap_position()
	{
		$src = new PBSize( $_REQUEST['src'] );
		$dst = new PBSize( $_REQUEST['dst'] );
		$this->SwapPosition( $src, $dst, "pb_sizes", "status=1" );
		$this->pb_sizes_list( 'scroll_to_list' );
	}
	// </Photo book sizes>
	
	// <Photo book covers>
	function cover_categories_list( $parameters='' )
	{
		$this->bigtabs->select_child( 'printbook' );
		$this->menuleft->select_child( 'sizes_list' );
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/printbook/smalltabs/list'), "?".$this->controller_name.".pb_sizes_list") );
		
		$this->method_name = 'cover_categories_list';
		$xml = "<data>";
		$xml .= "<list>";
		
		$pb_size_id = intval( $_REQUEST['pb_size_id'] );
		
		$pb_size = new PBSize($pb_size_id);
		$xml .= $pb_size->Xml();
		
		$t = new MysqlTable( 'pb_categories' );
		parent::define_pages( $t->get_count("status=1 and folder='{$this->cover_category_folder}' and parent_id={$pb_size_id}") );
		$t->find_all( "status=1 and folder='{$this->cover_category_folder}' and parent_id={$pb_size_id}", 'position,id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$o = new PBCategory( $a['id'] );
			
			if( $o->position == 0 )
			{
				$tp = new MysqlTable('pb_categories');
				$tp->fix_positions( '', "status=1 and folder='{$this->cover_category_folder}'" );
				$o->Load( $a['id'] );
			}
			
			$xml .= $o->Xml();
		}
		
		$xml .= "<last_position>{$o->position}</last_position>";
		
		$xml .= "</list>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'cover_categories_list', $xml );
	}
	
	function cover_category_edit( $save_result="" )
	{
		$last_position = intval( $_REQUEST['last_position'] );
		$pb_size_id = intval( $_REQUEST['pb_size_id'] );
		
		$this->bigtabs->select_child( 'printbook' );
		$this->menuleft->select_child( 'sizes_list' );
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/printbook/smalltabs/list'), "?".$this->controller_name.".pb_sizes_list") );
		
		$this->method_name = 'cover_category_edit';
		$xml = "<data>";
		
		$xml .= "<edit>";
		
		$pb_size = new PBSize($pb_size_id);
		$xml .= $pb_size->Xml();
		
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= "<last_position>$last_position</last_position>";
		$xml .= "<pb_size_id>$pb_size_id</pb_size_id>";
		$o = new PBCategory( $_REQUEST['id'] );
		$xml .= $o->Xml();
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'cover_category_edit', $xml );
	}
	
	function cover_category_save()
	{
		$erx = "";
		
		$obj = new PBCategory( $_REQUEST['object']['id'] );
		$last_position = intval( $_REQUEST['last_position'] );
		if( $last_position > 0 ) $_REQUEST['object']['position'] = $last_position + 1;
		$_REQUEST['object']['folder'] = $this->cover_category_folder;
		
		$attachment = new Attachment();
		if( $attachment->upload('image') )
		{
			$_REQUEST['object']['image'] = $attachment->filename;
			$io = new AImage( SITEROOT . '/'. $_ENV['data_path'] .'/'. $attachment->filename );
			$io->CopyResize( SITEROOT . '/'. $_ENV['data_path'] .'/thumbnail-'. $attachment->filename, 200, 200 );
		}
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->cover_category_edit( 'true' );
		}
		else
		{
			$this->cover_category_edit( 'false' );
		}
	}
	
	function cover_category_del()
	{
		$o = new PBCategory( $_REQUEST['id'] );
		$o->Delete();
		$this->cover_categories_list( 'scroll_to_list' );
	}
	
	function cover_category_del_image()
	{
		$o = new PBCategory( $_REQUEST['id'] );
		$o->DeleteImage();
		$this->cover_category_edit( 'true' );
	}
	
	function cover_categories_del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new PBCategory( $id );
			$o->Delete();
		}
		$this->cover_categories_list( 'scroll_to_list' );
	}
	
	function cover_categories_swap_position()
	{
		$src = new PBCategory( $_REQUEST['src'] );
		$dst = new PBCategory( $_REQUEST['dst'] );
		$this->SwapPosition( $src, $dst, "pb_categories", "status=1 and folder='{$this->cover_category_folder}'" );
		$this->cover_categories_list( 'scroll_to_list' );
	}
	
	function pb_covers_list( $parameters='' )
	{
		$this->bigtabs->select_child( 'printbook' );
		$this->menuleft->select_child( 'sizes_list' );
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/printbook/smalltabs/list'), "?".$this->controller_name.".pb_sizes_list") );
		
		$this->method_name = 'pb_covers_list';
		$xml = "<data>";
		$xml .= "<list>";
		
		$pb_size_id = intval( $_REQUEST['pb_size_id'] );
		$category_id = intval( $_REQUEST['pb_category_id'] );
		
		$pb_size = new PBSize($pb_size_id);
		$xml .= $pb_size->Xml();
		
		$pb_category = new PBCategory($category_id);
		$xml .= $pb_category->Xml();
		
		$t = new MysqlTable( 'pb_covers' );
		parent::define_pages( $t->get_count("status=1 and pb_size_id={$pb_size_id} and category_id={$category_id}") );
		$t->find_all( "status=1 and pb_size_id={$pb_size_id} and category_id={$category_id}", 'position,id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$o = new PBCover( $a['id'] );
			
			if( $o->position == 0 )
			{
				$tp = new MysqlTable('pb_covers');
				$tp->fix_positions( '', "status=1 and pb_size_id={$pb_size_id}" );
				$o->Load( $a['id'] );
			}
			
			$xml .= $o->Xml();
		}
		
		$xml .= "<last_position>{$o->position}</last_position>";
		
		$xml .= "</list>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'pb_covers_list', $xml );
	}
	
	function pb_cover_edit( $save_result="" )
	{
		$last_position = intval( $_REQUEST['last_position'] );
		$pb_size_id = intval( $_REQUEST['pb_size_id'] );
		
		$o = new PBCover( $_REQUEST['id'] );
		$category_id = ($o->category_id > 0)? $o->category_id : intval( $_REQUEST['pb_category_id'] );
		
		$this->bigtabs->select_child( 'printbook' );
		$this->menuleft->select_child( 'sizes_list' );
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/printbook/smalltabs/list'), "?".$this->controller_name.".pb_sizes_list") );
		
		$this->method_name = 'pb_cover_edit';
		$xml = "<data>";
		
		$xml .= "<edit>";
		
		$pb_size = new PBSize($pb_size_id);
		$xml .= $pb_size->Xml();
		
		$pb_category = new PBCategory($category_id);
		$xml .= $pb_category->Xml();
		
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= "<last_position>$last_position</last_position>";
		$xml .= "<pb_size_id>$pb_size_id</pb_size_id>";
		$xml .= "<pb_category_id>$category_id</pb_category_id>";
		$xml .= $o->Xml();
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'pb_cover_edit', $xml );
	}
	
	function pb_cover_save()
	{
		$erx = "";
		
		$obj = new PBCover( $_REQUEST['object']['id'] );
		$last_position = intval( $_REQUEST['last_position'] );
		if( $last_position > 0 ) $_REQUEST['object']['position'] = $last_position + 1;
		
		$_REQUEST['pb_size_id'] = ( $obj->pb_size_id>0 )? $obj->pb_size_id : intval($_REQUEST['pb_size_id']);
		$_REQUEST['pb_category_id'] = ( $obj->pb_category_id>0 )? $obj->pb_category_id : intval($_REQUEST['pb_category_id']);
		
		$_REQUEST['object']['category_id'] = $_REQUEST['pb_category_id'];
		
		if( !$_REQUEST['object']['price'] ) $_REQUEST['object']['price'] = 0;
		if( !$_REQUEST['object']['supercover_price'] ) $_REQUEST['object']['supercover_price'] = 0;
		if( !$_REQUEST['object']['has_supercover'] ) $_REQUEST['object']['has_supercover'] = 0;
        if( !$_REQUEST['object']['allow_min_pages'] ) $_REQUEST['object']['allow_min_pages'] = 0;
		
		$attachment = new Attachment();
		if( $attachment->upload('image') )
		{
			$_REQUEST['object']['image'] = $attachment->filename;
			$io = new AImage( SITEROOT . '/'. $_ENV['data_path'] .'/'. $attachment->filename );
			$io->CopyResize( SITEROOT . '/'. $_ENV['data_path'] .'/thumbnail-'. $attachment->filename, 200, 200 );
		}
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->pb_cover_edit( 'true' );
		}
		else
		{
			$this->pb_cover_edit( 'false' );
		}
	}
	
	function pb_cover_del()
	{
		$o = new PBCover( $_REQUEST['id'] );
		$o->Delete();
		$_REQUEST['pb_size_id'] = $o->pb_size_id;
		$_REQUEST['pb_category_id'] = $o->category_id;
		$this->pb_covers_list( 'scroll_to_list' );
	}
	
	function pb_covers_del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new PBCover( $id );
			$o->Delete();
		}
		$_REQUEST['pb_size_id'] = $o->pb_size_id;
		$_REQUEST['pb_category_id'] = $o->category_id;
		$this->pb_covers_list( 'scroll_to_list' );
	}
	
	function pb_cover_del_image()
	{
		$o = new PBCover( $_REQUEST['id'] );
		$o->DeleteImage();
		$this->pb_cover_edit( 'true' );
	}
	
	function pb_covers_swap_position()
	{
		$src = new PBCover( $_REQUEST['src'] );
		$dst = new PBCover( $_REQUEST['dst'] );
		$this->SwapPosition( $src, $dst, "pb_covers", "status=1 and pb_size_id={$src->pb_size_id}" );
		$_REQUEST['pb_size_id'] = $src->pb_size_id;
		$_REQUEST['pb_category_id'] = $src->category_id;
		$this->pb_covers_list( 'scroll_to_list' );
	}
	// </Photo book covers>
	
    // <Photo book forzaces>
	function pb_forzaces_list( $parameters='' )
	{
        //global $log;
        
		$this->bigtabs->select_child( 'printbook' );
		$this->menuleft->select_child( 'sizes_list' );
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/printbook/smalltabs/list'), "?".$this->controller_name.".pb_sizes_list") );
		
		$this->method_name = 'pb_forzaces_list';
		$xml = "<data>";
		$xml .= "<list>";
		
		$pb_size_id = intval( $_REQUEST['pb_size_id'] );
		
		$pb_size = new PBSize($pb_size_id);
		$xml .= $pb_size->Xml();
		
		$t = new MysqlTable( 'pb_forzaces' );
		parent::define_pages( $t->get_count("status=1 and pb_size_id={$pb_size_id}") );
		$t->find_all( "status=1 and pb_size_id={$pb_size_id}", 'position,id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$o = new PBForzace( $a['id'] );
			
			if( $o->position == 0 )
			{
				$tp = new MysqlTable('pb_forzaces');
				$tp->fix_positions( '', "status=1 and pb_size_id={$pb_size_id}" );
				$o->Load( $a['id'] );
			}
			
			$xml .= $o->Xml();
		}
		
		$xml .= "<last_position>{$o->position}</last_position>";
		
		$xml .= "</list>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		
        //$log->Write( "pb_forzaces_list: $xml" );
        
        $this->display( 'pb_forzaces_list', $xml );
	}
	
	function pb_forzace_edit( $save_result="" )
	{
		$last_position = intval( $_REQUEST['last_position'] );
		$pb_size_id = intval( $_REQUEST['pb_size_id'] );
		
		$o = new PBForzace( $_REQUEST['id'] );
		
		$this->bigtabs->select_child( 'printbook' );
		$this->menuleft->select_child( 'sizes_list' );
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/printbook/smalltabs/list'), "?".$this->controller_name.".pb_sizes_list") );
		
		$this->method_name = 'pb_forzace_edit';
		$xml = "<data>";
		
		$xml .= "<edit>";
		
		$pb_size = new PBSize($pb_size_id);
		$xml .= $pb_size->Xml();
		
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= "<last_position>$last_position</last_position>";
		$xml .= "<pb_size_id>$pb_size_id</pb_size_id>";
		$xml .= $o->Xml();
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'pb_forzace_edit', $xml );
	}
    
	function pb_forzace_save()
	{
		$erx = "";
		
		$obj = new PBForzace( $_REQUEST['object']['id'] );
		$last_position = intval( $_REQUEST['last_position'] );
		if( $last_position > 0 ) $_REQUEST['object']['position'] = $last_position + 1;
		
		$_REQUEST['pb_size_id'] = ( $obj->pb_size_id>0 )? $obj->pb_size_id : intval($_REQUEST['pb_size_id']);
		
		if( !$_REQUEST['object']['price'] ) $_REQUEST['object']['price'] = 0;
        if( !$_REQUEST['object']['allow_min_pages'] ) $_REQUEST['object']['allow_min_pages'] = 0;
		
		$attachment = new Attachment();
		if( $attachment->upload('image') )
		{
			$_REQUEST['object']['image'] = $attachment->filename;
			$io = new AImage( SITEROOT . '/'. $_ENV['data_path'] .'/'. $attachment->filename );
			$io->CopyResize( SITEROOT . '/'. $_ENV['data_path'] .'/thumbnail-'. $attachment->filename, 200, 200 );
		}
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->pb_forzace_edit( 'true' );
		}
		else
		{
			$this->pb_forzace_edit( 'false' );
		}
	}

	function pb_forzace_del()
	{
		$o = new PBForzace( $_REQUEST['id'] );
		$o->Delete();
		$_REQUEST['pb_size_id'] = $o->pb_size_id;
		$this->pb_forzaces_list( 'scroll_to_list' );
	}
	 
	function pb_forzaces_del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new PBForzace( $id );
			$o->Delete();
		}
		$_REQUEST['pb_size_id'] = $o->pb_size_id;
		$this->pb_forzaces_list( 'scroll_to_list' );
	}
    
	function pb_forzace_del_image()
	{
		$o = new PBForzace( $_REQUEST['id'] );
		$o->DeleteImage();
		$this->pb_forzace_edit( 'true' );
	}

	function pb_forzaces_swap_position()
	{
		$src = new PBForzace( $_REQUEST['src'] );
		$dst = new PBForzace( $_REQUEST['dst'] );
		$this->SwapPosition( $src, $dst, "pb_forzaces", "status=1 and pb_size_id={$src->pb_size_id}" );
		$_REQUEST['pb_size_id'] = $src->pb_size_id;
		$this->pb_forzaces_list( 'scroll_to_list' );
	}
    // </Photo book forzaces>
    
	// <Photobook supercovers>
	function pb_supercovers_list( $parameters='' )
	{
		$this->bigtabs->select_child( 'printbook' );
		$this->menuleft->select_child( 'sizes_list' );
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/printbook/smalltabs/list'), "?".$this->controller_name.".pb_sizes_list") );
		
		$this->method_name = 'pb_supercovers_list';
		$xml = "<data>";
		$xml .= "<list>";
		
		$pb_size_id = intval( $_REQUEST['pb_size_id'] );
		$category_id = intval( $_REQUEST['pb_category_id'] );
		$pb_cover_id = intval( $_REQUEST['pb_cover_id'] );
		
		$pb_size = new PBSize($pb_size_id);
		$xml .= $pb_size->Xml();
		
		$pb_category = new PBCategory($category_id);
		$xml .= $pb_category->Xml();
		
		$pb_cover = new PBCover($pb_cover_id);
		$xml .= $pb_cover->Xml();
		
		$t = new MysqlTable( 'pb_supercovers' );
		parent::define_pages( $t->get_count("status=1 and pb_cover_id={$pb_cover_id}") );
		$t->find_all( "status=1 and pb_cover_id={$pb_cover_id}", 'position,id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$o = new PBSuperCover( $a['id'] );
			
			if( $o->position == 0 )
			{
				$tp = new MysqlTable('pb_supercovers');
				$tp->fix_positions( '', "status=1 and pb_cover_id={$pb_cover_id}" );
				$o->Load( $a['id'] );
			}
			
			$xml .= $o->Xml();
		}
		
		$xml .= "<last_position>{$o->position}</last_position>";
		
		$xml .= "</list>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'pb_supercovers_list', $xml );
	}
	
	function pb_supercover_edit( $save_result="" )
	{
		$last_position = intval( $_REQUEST['last_position'] );
		$pb_size_id = intval( $_REQUEST['pb_size_id'] );
		$category_id = intval( $_REQUEST['pb_category_id'] );
		$pb_cover_id = intval( $_REQUEST['pb_cover_id'] );
		
		$o = new PBSuperCover( $_REQUEST['id'] );
		
		$this->bigtabs->select_child( 'printbook' );
		$this->menuleft->select_child( 'sizes_list' );
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/printbook/smalltabs/list'), "?".$this->controller_name.".pb_sizes_list") );
		
		$this->method_name = 'pb_supercover_edit';
		$xml = "<data>";
		
		$xml .= "<edit>";
		
		$pb_size = new PBSize($pb_size_id);
		$xml .= $pb_size->Xml();
		
		$pb_category = new PBCategory($category_id);
		$xml .= $pb_category->Xml();
		
		$pb_cover = new PBCover($pb_cover_id);
		$xml .= $pb_cover->Xml();
		
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= "<last_position>$last_position</last_position>";
		$xml .= "<pb_size_id>$pb_size_id</pb_size_id>";
		$xml .= "<pb_category_id>$category_id</pb_category_id>";
		$xml .= "<pb_cover_id>$pb_cover_id</pb_cover_id>";
		$xml .= $o->Xml();
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'pb_supercover_edit', $xml );
	}
	
	function pb_supercover_save()
	{
		$erx = "";
		
		$obj = new PBSuperCover( $_REQUEST['object']['id'] );
		$last_position = intval( $_REQUEST['last_position'] );
		if( $last_position > 0 ) $_REQUEST['object']['position'] = $last_position + 1;
		
		$_REQUEST['pb_size_id'] = intval($_REQUEST['pb_size_id']);
		$_REQUEST['pb_category_id'] = intval($_REQUEST['pb_category_id']);
		$_REQUEST['pb_cover_id'] = ( $obj->pb_cover_id>0 )? $obj->pb_cover_id : intval($_REQUEST['pb_cover_id']);
		
		$attachment = new Attachment();
		if( $attachment->upload('image') )
		{
			$_REQUEST['object']['image'] = $attachment->filename;
			$io = new AImage( SITEROOT . '/'. $_ENV['data_path'] .'/'. $attachment->filename );
			$io->CopyResize( SITEROOT . '/'. $_ENV['data_path'] .'/thumbnail-'. $attachment->filename, 200, 200 );
		}
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->pb_supercover_edit( 'true' );
		}
		else
		{
			$this->pb_supercover_edit( 'false' );
		}
	}
	
	function pb_supercover_del()
	{
		$o = new PBSuperCover( $_REQUEST['id'] );
		$o->Delete();
		$_REQUEST['pb_cover_id'] = $o->pb_cover_id;
		$this->pb_supercovers_list( 'scroll_to_list' );
	}
	
	function pb_supercovers_del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new PBSuperCover( $id );
			$o->Delete();
		}
		$_REQUEST['pb_cover_id'] = $o->pb_cover_id;
		$this->pb_supercovers_list( 'scroll_to_list' );
	}
	
	function pb_supercovers_swap_position()
	{
		$src = new PBSuperCover( $_REQUEST['src'] );
		$dst = new PBSuperCover( $_REQUEST['dst'] );
		$this->SwapPosition( $src, $dst, "pb_supercovers", "status=1 and pb_cover_id={$src->pb_cover_id}" );
		$_REQUEST['pb_cover_id'] = $src->pb_cover_id;
		$this->pb_supercovers_list( 'scroll_to_list' );
	}
	// </Photobook supercovers>
	
	// <Photo book papers>
	function pb_papers_list( $parameters='' )
	{
		$this->bigtabs->select_child( 'printbook' );
		$this->menuleft->select_child( 'sizes_list' );
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/printbook/smalltabs/list'), "?".$this->controller_name.".pb_sizes_list") );
		
		$this->method_name = 'pb_papers_list';
		$xml = "<data>";
		$xml .= "<list>";
		
		$pb_size_id = intval( $_REQUEST['pb_size_id'] );
		
		$pb_size = new PBSize($pb_size_id);
		$xml .= $pb_size->Xml();
		
		$t = new MysqlTable( 'pb_papers' );
		parent::define_pages( $t->get_count("status=1 and pb_size_id={$pb_size_id}") );
		$t->find_all( "status=1 and pb_size_id={$pb_size_id}", 'position,id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$o = new PBPaper( $a['id'] );
			
			if( $o->position == 0 )
			{
				$tp = new MysqlTable('pb_papers');
				$tp->fix_positions( '', "status=1 and pb_size_id={$pb_size_id}" );
				$o->Load( $a['id'] );
			}
			
			$xml .= $o->Xml();
		}
		
		$xml .= "<last_position>{$o->position}</last_position>";
		
		$xml .= "</list>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'pb_papers_list', $xml );
	}
	
	function pb_paper_edit( $save_result="" )
	{
		$last_position = intval( $_REQUEST['last_position'] );
		$pb_size_id = intval( $_REQUEST['pb_size_id'] );
		
		$this->bigtabs->select_child( 'printbook' );
		$this->menuleft->select_child( 'sizes_list' );
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/printbook/smalltabs/list'), "?".$this->controller_name.".pb_sizes_list") );
		
		$this->method_name = 'pb_paper_edit';
		$xml = "<data>";
		
		$xml .= "<edit>";
		
		$pb_size = new PBSize($pb_size_id);
		$xml .= $pb_size->Xml();
		
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= "<last_position>$last_position</last_position>";
		$xml .= "<pb_size_id>$pb_size_id</pb_size_id>";
		$o = new PBPaper( $_REQUEST['id'] );
		$xml .= $o->Xml();
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'pb_paper_edit', $xml );
	}
	
	function pb_paper_save()
	{
		$erx = "";
		
		$obj = new PBPaper( $_REQUEST['object']['id'] );
		$last_position = intval( $_REQUEST['last_position'] );
		if( $last_position > 0 ) $_REQUEST['object']['position'] = $last_position + 1;
		
		$_REQUEST['pb_size_id'] = ( $obj->pb_size_id>0 )? $obj->pb_size_id : intval($_REQUEST['pb_size_id']);
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->pb_paper_edit( 'true' );
		}
		else
		{
			$this->pb_paper_edit( 'false' );
		}
	}
	
	function pb_paper_del()
	{
		$o = new PBPaper( $_REQUEST['id'] );
		$o->Delete();
		$_REQUEST['pb_size_id'] = $o->pb_size_id;
		$this->pb_papers_list( 'scroll_to_list' );
	}
	
	function pb_papers_del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new PBPaper( $id );
			$o->Delete();
		}
		$_REQUEST['pb_size_id'] = $o->pb_size_id;
		$this->pb_papers_list( 'scroll_to_list' );
	}
	
	function pb_papers_swap_position()
	{
		$src = new PBPaper( $_REQUEST['src'] );
		$dst = new PBPaper( $_REQUEST['dst'] );
		$this->SwapPosition( $src, $dst, "pb_papers", "status=1 and pb_size_id={$src->pb_size_id}" );
		$_REQUEST['pb_size_id'] = $src->pb_size_id;
		$this->pb_papers_list( 'scroll_to_list' );
	}
	// </Photo book papers>
	
	// <Photo book lamination>
	function pb_laminations_list( $parameters='' )
	{
		$this->bigtabs->select_child( 'printbook' );
		$this->menuleft->select_child( 'sizes_list' );
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/printbook/smalltabs/list'), "?".$this->controller_name.".pb_sizes_list") );
		
		$this->method_name = 'pb_laminations_list';
		$xml = "<data>";
		$xml .= "<list>";
		
		$pb_size_id = intval( $_REQUEST['pb_size_id'] );
		
		$pb_size = new PBSize($pb_size_id);
		$xml .= $pb_size->Xml();
		
		$t = new MysqlTable( 'pb_lamination' );
		parent::define_pages( $t->get_count("status=1 and pb_size_id={$pb_size_id}") );
		$t->find_all( "status=1 and pb_size_id={$pb_size_id}", 'position,id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$o = new PBLamination( $a['id'] );
			
			if( $o->position == 0 )
			{
				$tp = new MysqlTable('pb_lamination');
				$tp->fix_positions( '', "status=1 and pb_size_id={$pb_size_id}" );
				$o->Load( $a['id'] );
			}
			
			$xml .= $o->Xml();
		}
		
		$xml .= "<last_position>{$o->position}</last_position>";
		
		$xml .= "</list>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'pb_laminations_list', $xml );
	}
	
	function pb_lamination_edit( $save_result="" )
	{
		$last_position = intval( $_REQUEST['last_position'] );
		$pb_size_id = intval( $_REQUEST['pb_size_id'] );
		
		$this->bigtabs->select_child( 'printbook' );
		$this->menuleft->select_child( 'sizes_list' );
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/printbook/smalltabs/list'), "?".$this->controller_name.".pb_sizes_list") );
		
		$this->method_name = 'pb_lamination_edit';
		$xml = "<data>";
		
		$xml .= "<edit>";
		
		$pb_size = new PBSize($pb_size_id);
		$xml .= $pb_size->Xml();
		
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= "<last_position>$last_position</last_position>";
		$xml .= "<pb_size_id>$pb_size_id</pb_size_id>";
		$o = new PBLamination( $_REQUEST['id'] );
		$xml .= $o->Xml();
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'pb_lamination_edit', $xml );
	}
	
	function pb_lamination_save()
	{
		$erx = "";
		
		$obj = new PBLamination( $_REQUEST['object']['id'] );
		$last_position = intval( $_REQUEST['last_position'] );
		if( $last_position > 0 ) $_REQUEST['object']['position'] = $last_position + 1;
		
		$_REQUEST['pb_size_id'] = ( $obj->pb_size_id>0 )? $obj->pb_size_id : intval($_REQUEST['pb_size_id']);
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->pb_lamination_edit( 'true' );
		}
		else
		{
			$this->pb_lamination_edit( 'false' );
		}
	}
	
	function pb_lamination_del()
	{
		$o = new PBLamination( $_REQUEST['id'] );
		$o->Delete();
		$_REQUEST['pb_size_id'] = $o->pb_size_id;
		$this->pb_laminations_list( 'scroll_to_list' );
	}
	
	function pb_laminations_del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new PBLamination( $id );
			$o->Delete();
		}
		$_REQUEST['pb_size_id'] = $o->pb_size_id;
		$this->pb_laminations_list( 'scroll_to_list' );
	}
	
	function pb_laminations_swap_position()
	{
		$src = new PBLamination( $_REQUEST['src'] );
		$dst = new PBLamination( $_REQUEST['dst'] );
		$this->SwapPosition( $src, $dst, "pb_lamination", "status=1 and pb_size_id={$src->pb_size_id}" );
		$_REQUEST['pb_size_id'] = $src->pb_size_id;
		$this->pb_laminations_list( 'scroll_to_list' );
	}
	// </Photo book lamination>
	
	// <Photo book paper base>
	function pb_paper_bases_list( $parameters='' )
	{
		$this->bigtabs->select_child( 'printbook' );
		$this->menuleft->select_child( 'sizes_list' );
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/printbook/smalltabs/list'), "?".$this->controller_name.".pb_sizes_list") );
		
		$this->method_name = 'pb_paper_bases_list';
		$xml = "<data>";
		$xml .= "<list>";
		
		$pb_size_id = intval( $_REQUEST['pb_size_id'] );
		
		$pb_size = new PBSize($pb_size_id);
		$xml .= $pb_size->Xml();
		
		$t = new MysqlTable( 'pb_paper_base' );
		parent::define_pages( $t->get_count("status=1 and pb_size_id={$pb_size_id}") );
		$t->find_all( "status=1 and pb_size_id={$pb_size_id}", 'position,id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$o = new PBPaperBase( $a['id'] );
			
			if( $o->position == 0 )
			{
				$tp = new MysqlTable('pb_paper_base');
				$tp->fix_positions( '', "status=1 and pb_size_id={$pb_size_id}" );
				$o->Load( $a['id'] );
			}
			
			$xml .= $o->Xml();
		}
		
		$xml .= "<last_position>{$o->position}</last_position>";
		
		$xml .= "</list>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'pb_paper_base_list', $xml );
	}
	
	function pb_paper_base_edit( $save_result="" )
	{
		$last_position = intval( $_REQUEST['last_position'] );
		$pb_size_id = intval( $_REQUEST['pb_size_id'] );
		
		$this->bigtabs->select_child( 'printbook' );
		$this->menuleft->select_child( 'sizes_list' );
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/printbook/smalltabs/list'), "?".$this->controller_name.".pb_sizes_list") );
		
		$this->method_name = 'pb_paper_base_edit';
		$xml = "<data>";
		
		$xml .= "<edit>";
		
		$pb_size = new PBSize($pb_size_id);
		$xml .= $pb_size->Xml();
		
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= "<last_position>$last_position</last_position>";
		$xml .= "<pb_size_id>$pb_size_id</pb_size_id>";
		$o = new PBPaperBase( $_REQUEST['id'] );
		$xml .= $o->Xml();
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'pb_paper_base_edit', $xml );
	}
	
	function pb_paper_base_save()
	{
		$erx = "";
		
		$obj = new PBPaperBase( $_REQUEST['object']['id'] );
		$last_position = intval( $_REQUEST['last_position'] );
		if( $last_position > 0 ) $_REQUEST['object']['position'] = $last_position + 1;
		
		$_REQUEST['pb_size_id'] = ( $obj->pb_size_id>0 )? $obj->pb_size_id : intval($_REQUEST['pb_size_id']);
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->pb_paper_base_edit( 'true' );
		}
		else
		{
			$this->pb_paper_base_edit( 'false' );
		}
	}
	
	function pb_paper_base_del()
	{
		$o = new PBPaperBase( $_REQUEST['id'] );
		$o->Delete();
		$_REQUEST['pb_size_id'] = $o->pb_size_id;
		$this->pb_paper_bases_list( 'scroll_to_list' );
	}
	
	function pb_paper_bases_del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new PBPaperBase( $id );
			$o->Delete();
		}
		$_REQUEST['pb_size_id'] = $o->pb_size_id;
		$this->pb_paper_bases_list( 'scroll_to_list' );
	}
	
	function pb_paper_bases_swap_position()
	{
		$src = new PBPaperBase( $_REQUEST['src'] );
		$dst = new PBPaperBase( $_REQUEST['dst'] );
		$this->SwapPosition( $src, $dst, "pb_paper_base", "status=1 and pb_size_id={$src->pb_size_id}" );
		$_REQUEST['pb_size_id'] = $src->pb_size_id;
		$this->pb_paper_bases_list( 'scroll_to_list' );
	}
	// </Photo book paper base>
	
	/* box */
	function box_categories_list( $parameters='' )
	{
		$this->bigtabs->select_child( 'printbook' );
		$this->menuleft->select_child( 'sizes_list' );
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/printbook/smalltabs/list'), "?".$this->controller_name.".pb_sizes_list") );
		
		$this->method_name = 'box_categories_list';
		$xml = "<data>";
		$xml .= "<list>";
		
		$pb_size_id = intval( $_REQUEST['pb_size_id'] );
		
		$pb_size = new PBSize($pb_size_id);
		$xml .= $pb_size->Xml();
		
		$t = new MysqlTable( 'pb_categories' );
		parent::define_pages( $t->get_count("status=1 and folder='{$this->box_category_folder}'") );
		$t->find_all( "status=1 and folder='{$this->box_category_folder}'", 'position,id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$o = new PBCategory( $a['id'] );
			
			if( $o->position == 0 )
			{
				$tp = new MysqlTable('pb_categories');
				$tp->fix_positions( '', "status=1 and folder='{$this->box_category_folder}'" );
				$o->Load( $a['id'] );
			}
			
			$xml .= $o->Xml();
		}
		
		$xml .= "<last_position>{$o->position}</last_position>";
		
		$xml .= "</list>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'box_categories_list', $xml );
	}
	
	function box_category_edit( $save_result="" )
	{
		$last_position = intval( $_REQUEST['last_position'] );
		$pb_size_id = intval( $_REQUEST['pb_size_id'] );
		
		$this->bigtabs->select_child( 'printbook' );
		$this->menuleft->select_child( 'sizes_list' );
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/printbook/smalltabs/list'), "?".$this->controller_name.".pb_sizes_list") );
		
		$this->method_name = 'box_edit';
		$xml = "<data>";
		
		$xml .= "<edit>";
		
		$pb_size = new PBSize($pb_size_id);
		$xml .= $pb_size->Xml();
		
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= "<last_position>$last_position</last_position>";
		$xml .= "<pb_size_id>$pb_size_id</pb_size_id>";
		$o = new PBCategory( $_REQUEST['id'] );
		$xml .= $o->Xml();
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'box_category_edit', $xml );
	}
	
	function box_category_save()
	{
		$erx = "";
		
		$obj = new PBCategory( $_REQUEST['object']['id'] );
		$last_position = intval( $_REQUEST['last_position'] );
		if( $last_position > 0 ) $_REQUEST['object']['position'] = $last_position + 1;
		$_REQUEST['object']['folder'] = $this->box_category_folder;
		
		$attachment = new Attachment();
		if( $attachment->upload('image') )
		{
			$_REQUEST['object']['image'] = $attachment->filename;
			$io = new AImage( SITEROOT . '/'. $_ENV['data_path'] .'/'. $attachment->filename );
			$io->CopyResize( SITEROOT . '/'. $_ENV['data_path'] .'/thumbnail-'. $attachment->filename, 200, 200 );
		}
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->box_category_edit( 'true' );
		}
		else
		{
			$this->box_category_edit( 'false' );
		}
	}
	
	function box_category_del()
	{
		$o = new PBCategory( $_REQUEST['id'] );
		$o->Delete();
		$this->box_categories_list( 'scroll_to_list' );
	}
	
	function box_category_del_image()
	{
		$o = new PBCategory( $_REQUEST['id'] );
		$o->DeleteImage();
		$this->box_category_edit( 'true' );
	}
	
	function box_categories_del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new PBCategory( $id );
			$o->Delete();
		}
		$this->box_categories_list( 'scroll_to_list' );
	}
	
	function box_categories_swap_position()
	{
		$src = new PBCategory( $_REQUEST['src'] );
		$dst = new PBCategory( $_REQUEST['dst'] );
		$this->SwapPosition( $src, $dst, "pb_categories", "status=1 and folder='{$this->box_category_folder}'" );
		$this->box_categories_list( 'scroll_to_list' );
	}
	
	function boxes_list( $parameters='' )
	{
		$this->bigtabs->select_child( 'printbook' );
		$this->menuleft->select_child( 'sizes_list' );
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/printbook/smalltabs/list'), "?".$this->controller_name.".pb_sizes_list") );
		
		$this->method_name = 'boxes_list';
		$xml = "<data>";
		$xml .= "<list>";
		
		$pb_size_id = intval( $_REQUEST['pb_size_id'] );
		$category_id = intval( $_REQUEST['pb_category_id'] );
		
		$pb_size = new PBSize($pb_size_id);
		$xml .= $pb_size->Xml();
		
		$pb_category = new PBCategory($category_id);
		$xml .= $pb_category->Xml();
		
		$t = new MysqlTable( 'pb_boxes' );
		parent::define_pages( $t->get_count("status=1 and pb_size_id={$pb_size_id} and category_id={$category_id}") );
		$t->find_all( "status=1 and pb_size_id={$pb_size_id} and category_id={$category_id}", 'position,id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$o = new pbBox( $a['id'] );
			
			if( $o->position == 0 )
			{
				$tp = new MysqlTable('pb_boxes');
				$tp->fix_positions( '', "status=1 and pb_size_id={$pb_size_id}" );
				$o->Load( $a['id'] );
			}
			
			$xml .= $o->Xml();
		}
		
		$xml .= "<last_position>{$o->position}</last_position>";
		
		$xml .= "</list>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'boxes_list', $xml );
	}
	
	function box_edit( $save_result="" )
	{
		$o = new pbBox( $_REQUEST['id'] );
		
		$last_position = intval( $_REQUEST['last_position'] );
		$pb_size_id = ($o->pb_size_id > 0)? $o->pb_size_id : intval( $_REQUEST['pb_size_id'] );
		$category_id = ($o->category_id > 0)? $o->category_id : intval( $_REQUEST['pb_category_id'] );
		
		$this->bigtabs->select_child( 'printbook' );
		$this->menuleft->select_child( 'sizes_list' );
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/printbook/smalltabs/list'), "?".$this->controller_name.".pb_sizes_list") );
		
		$this->method_name = 'box_edit';
		$xml = "<data>";
		
		$xml .= "<edit>";
		
		$pb_size = new PBSize($pb_size_id);
		$xml .= $pb_size->Xml();
		
		$pb_category = new PBCategory($category_id);
		$xml .= $pb_category->Xml();
		
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= "<last_position>$last_position</last_position>";
		$xml .= "<pb_size_id>$pb_size_id</pb_size_id>";
		$xml .= "<pb_category_id>$category_id</pb_category_id>";
		$xml .= $o->Xml();
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'box_edit', $xml );
	}
	
	function box_save()
	{
		$erx = "";
		
		$obj = new pbBox( $_REQUEST['object']['id'] );
		$last_position = intval( $_REQUEST['last_position'] );
		if( $last_position > 0 ) $_REQUEST['object']['position'] = $last_position + 1;
		
		$_REQUEST['pb_size_id'] = ( $obj->pb_size_id>0 )? $obj->pb_size_id : intval($_REQUEST['pb_size_id']);
		$_REQUEST['pb_category_id'] = ( $obj->pb_category_id>0 )? $obj->pb_category_id : intval($_REQUEST['pb_category_id']);
		
		$_REQUEST['object']['category_id'] = $_REQUEST['pb_category_id'];
		
		$attachment = new Attachment();
		if( $attachment->upload('image') )
		{
			$_REQUEST['object']['image'] = $attachment->filename;
			$io = new AImage( SITEROOT . '/'. $_ENV['data_path'] .'/'. $attachment->filename );
			$io->CopyResize( SITEROOT . '/'. $_ENV['data_path'] .'/thumbnail-'. $attachment->filename, 200, 200 );
		}
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->box_edit( 'true' );
		}
		else
		{
			$this->box_edit( 'false' );
		}
	}
	
	function box_del()
	{
		$o = new pbBox( $_REQUEST['id'] );
		$o->Delete();
		$_REQUEST['pb_size_id'] = $o->pb_size_id;
		$_REQUEST['pb_category_id'] = $o->category_id;
		$this->boxes_list( 'scroll_to_list' );
	}
	
	function boxes_del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new pbBox( $id );
			$o->Delete();
		}
		$_REQUEST['pb_size_id'] = $o->pb_size_id;
		$_REQUEST['pb_category_id'] = $o->category_id;
		$this->boxes_list( 'scroll_to_list' );
	}
	
	function box_del_image()
	{
		$o = new pbBox( $_REQUEST['id'] );
		$o->DeleteImage();
		$this->box_edit( 'true' );
	}
	
	function boxes_swap_position()
	{
		$src = new pbBox( $_REQUEST['src'] );
		$dst = new pbBox( $_REQUEST['dst'] );
		$this->SwapPosition( $src, $dst, "pb_boxes", "status=1 and pb_size_id={$src->pb_size_id}" );
		$_REQUEST['pb_size_id'] = $src->pb_size_id;
		$_REQUEST['pb_category_id'] = $src->category_id;
		$this->boxes_list( 'scroll_to_list' );
	}
	/* box */
	
	/* design works */
	function design_work_categories_list( $parameters='' )
	{
		$this->bigtabs->select_child( 'printbook' );
		$this->menuleft->select_child( 'sizes_list' );
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/printbook/smalltabs/list'), "?".$this->controller_name.".pb_sizes_list") );
		
		$this->method_name = 'design_work_categories_list';
		$xml = "<data>";
		$xml .= "<list>";
		
		$pb_size_id = intval( $_REQUEST['pb_size_id'] );
		
		$pb_size = new PBSize($pb_size_id);
		$xml .= $pb_size->Xml();
		
		$t = new MysqlTable( 'pb_categories' );
		parent::define_pages( $t->get_count("status=1 and folder='{$this->design_work_category_folder}'") );
		$t->find_all( "status=1 and folder='{$this->design_work_category_folder}'", 'position,id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$o = new PBCategory( $a['id'] );
			
			if( $o->position == 0 )
			{
				$tp = new MysqlTable('pb_categories');
				$tp->fix_positions( '', "status=1 and folder='{$this->design_work_category_folder}'" );
				$o->Load( $a['id'] );
			}
			
			$xml .= $o->Xml();
		}
		
		$xml .= "<last_position>{$o->position}</last_position>";
		
		$xml .= "</list>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'design_work_categories_list', $xml );
	}
	
	function design_work_category_edit( $save_result="" )
	{
		$last_position = intval( $_REQUEST['last_position'] );
		$pb_size_id = intval( $_REQUEST['pb_size_id'] );
		
		$this->bigtabs->select_child( 'printbook' );
		$this->menuleft->select_child( 'sizes_list' );
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/printbook/smalltabs/list'), "?".$this->controller_name.".pb_sizes_list") );
		
		$this->method_name = 'design_work_category_edit';
		$xml = "<data>";
		
		$xml .= "<edit>";
		
		$pb_size = new PBSize($pb_size_id);
		$xml .= $pb_size->Xml();
		
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= "<last_position>$last_position</last_position>";
		$xml .= "<pb_size_id>$pb_size_id</pb_size_id>";
		$o = new PBCategory( $_REQUEST['id'] );
		$xml .= $o->Xml();
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'design_work_category_edit', $xml );
	}
	
	function design_work_category_save()
	{
		$erx = "";
		
		$obj = new PBCategory( $_REQUEST['object']['id'] );
		$last_position = intval( $_REQUEST['last_position'] );
		if( $last_position > 0 ) $_REQUEST['object']['position'] = $last_position + 1;
		$_REQUEST['object']['folder'] = $this->design_work_category_folder;
		
		$attachment = new Attachment();
		if( $attachment->upload('image') )
		{
			$_REQUEST['object']['image'] = $attachment->filename;
			$io = new AImage( SITEROOT . '/'. $_ENV['data_path'] .'/'. $attachment->filename );
			$io->CopyResize( SITEROOT . '/'. $_ENV['data_path'] .'/thumbnail-'. $attachment->filename, 200, 200 );
		}
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->design_work_category_edit( 'true' );
		}
		else
		{
			$this->design_work_category_edit( 'false' );
		}
	}
	
	function design_work_category_del()
	{
		$o = new PBCategory( $_REQUEST['id'] );
		$o->Delete();
		$this->design_work_categories_list( 'scroll_to_list' );
	}
	
	function design_work_category_del_image()
	{
		$o = new PBCategory( $_REQUEST['id'] );
		$o->DeleteImage();
		$this->design_work_category_edit( 'true' );
	}
	
	function design_work_categories_del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new PBCategory( $id );
			$o->Delete();
		}
		$this->design_work_categories_list( 'scroll_to_list' );
	}
	
	function design_work_categories_swap_position()
	{
		$src = new PBCategory( $_REQUEST['src'] );
		$dst = new PBCategory( $_REQUEST['dst'] );
		$this->SwapPosition( $src, $dst, "pb_categories", "status=1 and folder='{$this->design_work_category_folder}'" );
		$this->design_work_categories_list( 'scroll_to_list' );
	}
	
	function design_works_list( $parameters='' )
	{
		$this->bigtabs->select_child( 'printbook' );
		$this->menuleft->select_child( 'sizes_list' );
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/printbook/smalltabs/list'), "?".$this->controller_name.".pb_sizes_list") );
		
		$this->method_name = 'design_works_list';
		$xml = "<data>";
		$xml .= "<list>";
		
		$pb_size_id = intval( $_REQUEST['pb_size_id'] );
		$category_id = intval( $_REQUEST['pb_category_id'] );
		
		$pb_size = new PBSize($pb_size_id);
		$xml .= $pb_size->Xml();
		
		$pb_category = new PBCategory($category_id);
		$xml .= $pb_category->Xml();
		
		$t = new MysqlTable( 'pb_design_works' );
		parent::define_pages( $t->get_count("status=1 and pb_size_id={$pb_size_id} and category_id={$category_id}") );
		$t->find_all( "status=1 and pb_size_id={$pb_size_id} and category_id={$category_id}", 'position,id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$o = new PBDesignWork( $a['id'] );
			
			if( $o->position == 0 )
			{
				$tp = new MysqlTable('pb_design_works');
				$tp->fix_positions( '', "status=1 and pb_size_id={$pb_size_id}" );
				$o->Load( $a['id'] );
			}
			
			$xml .= $o->Xml();
		}
		
		$xml .= "<last_position>{$o->position}</last_position>";
		
		$xml .= "</list>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'design_works_list', $xml );
	}
	
	function design_work_edit( $save_result="" )
	{
		$last_position = intval( $_REQUEST['last_position'] );
		$pb_size_id = intval( $_REQUEST['pb_size_id'] );
		
		$o = new PBDesignWork( $_REQUEST['id'] );
		$category_id = ($o->category_id > 0)? $o->category_id : intval( $_REQUEST['pb_category_id'] );
		
		$this->bigtabs->select_child( 'printbook' );
		$this->menuleft->select_child( 'sizes_list' );
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/printbook/smalltabs/list'), "?".$this->controller_name.".pb_sizes_list") );
		
		$this->method_name = 'design_work_edit';
		$xml = "<data>";
		
		$xml .= "<edit>";
		
		$pb_size = new pbSize($pb_size_id);
		$xml .= $pb_size->Xml();
		
		$pb_category = new PBCategory($category_id);
		$xml .= $pb_category->Xml();
		
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= "<last_position>$last_position</last_position>";
		$xml .= "<pb_size_id>$pb_size_id</pb_size_id>";
		$xml .= $o->Xml();
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'design_work_edit', $xml );
	}
	
	function design_work_save()
	{
		$erx = "";
		
		$obj = new PBDesignWork( $_REQUEST['object']['id'] );
		$last_position = intval( $_REQUEST['last_position'] );
		if( $last_position > 0 ) $_REQUEST['object']['position'] = $last_position + 1;
		
		$_REQUEST['pb_size_id'] = ( $obj->pb_size_id>0 )? $obj->pb_size_id : intval($_REQUEST['pb_size_id']);
		$_REQUEST['pb_category_id'] = ( $obj->pb_category_id>0 )? $obj->pb_category_id : intval($_REQUEST['pb_category_id']);
		
		$_REQUEST['object']['category_id'] = $_REQUEST['pb_category_id'];
		
		$attachment = new Attachment();
		if( $attachment->upload('image') )
		{
			$_REQUEST['object']['image'] = $attachment->filename;
			$io = new AImage( SITEROOT . '/'. $_ENV['data_path'] .'/'. $attachment->filename );
			$io->CopyResize( SITEROOT . '/'. $_ENV['data_path'] .'/thumbnail-'. $attachment->filename, 200, 200 );
		}
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->design_work_edit( 'true' );
		}
		else
		{
			$this->design_work_edit( 'false' );
		}
	}
	
	function design_work_del()
	{
		$o = new PBDesignWork( $_REQUEST['id'] );
		$o->Delete();
		$_REQUEST['pb_size_id'] = $o->pb_size_id;
		$_REQUEST['pb_category_id'] = $o->category_id;
		$this->design_works_list( 'scroll_to_list' );
	}
	
	function design_works_del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new PBDesignWork( $id );
			$o->Delete();
		}
		$_REQUEST['pb_size_id'] = $o->pb_size_id;
		$_REQUEST['pb_category_id'] = $o->category_id;
		$this->design_works_list( 'scroll_to_list' );
	}
	
	function design_work_del_image()
	{
		$o = new PBDesignWork( $_REQUEST['id'] );
		$o->DeleteImage();
		$this->design_work_edit( 'true' );
	}
	
	function design_works_swap_position()
	{
		$src = new PBDesignWork( $_REQUEST['src'] );
		$dst = new PBDesignWork( $_REQUEST['dst'] );
		$this->SwapPosition( $src, $dst, "pb_design_works", "status=1 and pb_size_id={$src->pb_size_id}" );
		$_REQUEST['pb_size_id'] = $src->pb_size_id;
		$_REQUEST['pb_category_id'] = $src->category_id;
		$this->design_works_list( 'scroll_to_list' );
	}
	/* design works */
	
	// <Settings>
	function settings_edit( $save_result="" )
	{
		$this->bigtabs->select_child( 'printbook' );
		$this->menuleft->select_child( 'settings' );
		
		$this->method_name = 'settings_edit';
		$xml = "<data>";
		
		$xml .= "<edit>";
		$xml .= "<save_result>$save_result</save_result>";
		$o = new PBSettings();
		$xml .= $o->Xml();
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'settings_edit', $xml );
	}
	
	function settings_save()
	{
		$erx = "";
		
		$obj = new PBSettings();
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->settings_edit( 'true' );
		}
		else
		{
			$this->settings_edit( 'false' );
		}
	}
	// </Settings>
	
	// <Settings2>
	function settings_edit2( $save_result="" )
	{
		$this->bigtabs->select_child( 'printbook' );
		$this->menuleft->select_child( 'settings2' );
		
		$this->method_name = 'settings_edit2';
		$xml = "<data>";
		
		$xml .= "<edit>";
		$xml .= "<save_result>$save_result</save_result>";
		$o = new PBSettings();
		$xml .= $o->Xml();
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'settings_edit2', $xml );
	}
	
	function settings_save2()
	{
		$erx = "";
		
		$obj = new PBSettings();
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->settings_edit2( 'true' );
		}
		else
		{
			$this->settings_edit2( 'false' );
		}
	}
	// </Settings2>
	
	// <Paymentways>
	function paymentways_list( $parameters='' )
	{
		$this->bigtabs->select_child( 'printbook' );
		$this->menuleft->select_child( 'paymentways_list' );
		
		$this->method_name = 'paymentways_list';
		$xml = "<data>";
		$xml .= "<list>";
		
		$t = new MysqlTable( 'pb_paymentways' );
		parent::define_pages( $t->get_count('status=1') );
		$t->find_all( 'status=1', 'position,id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$o = new PBPaymentway( $a['id'] );
			
			if( $o->position == 0 )
			{
				$tp = new MysqlTable('pb_paymentways');
				$tp->fix_positions( '', "status=1" );
				$o->Load( $a['id'] );
			}
			
			$xml .= $o->Xml();
		}
		
		$xml .= "<last_position>{$o->position}</last_position>";
		
		$xml .= "</list>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'paymentways_list', $xml );
	}
	
	function paymentway_edit( $save_result="" )
	{
		$last_position = intval( $_REQUEST['last_position'] );
		
		$this->bigtabs->select_child( 'printbook' );
		$this->menuleft->select_child( 'paymentways_list' );
		
		$this->method_name = 'paymentway_edit';
		$xml = "<data>";
		
		$xml .= "<edit>";
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= "<last_position>$last_position</last_position>";
		$o = new PBPaymentway( $_REQUEST['id'] );
		$xml .= $o->Xml();
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'paymentway_edit', $xml );
	}
	
	function paymentway_save()
	{
		$erx = "";
		
		$obj = new PBPaymentway( $_REQUEST['object']['id'] );
		$last_position = intval( $_REQUEST['last_position'] );
		if( $last_position > 0 ) $_REQUEST['object']['position'] = $last_position + 1;
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->paymentway_edit( 'true' );
		}
		else
		{
			$this->paymentway_edit( 'false' );
		}
	}
	
	function paymentway_del()
	{
		$o = new PBPaymentway( $_REQUEST['id'] );
		$o->Delete();
		$this->paymentways_list( 'scroll_to_list' );
	}
	
	function paymentways_del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new PBPaymentway( $id );
			$o->Delete();
		}
		$this->paymentways_list( 'scroll_to_list' );
	}
	
	function paymentway_swap_position()
	{
		$src = new PBPaymentway( $_REQUEST['src'] );
		$dst = new PBPaymentway( $_REQUEST['dst'] );
		$this->SwapPosition( $src, $dst, "pb_paymentways", "status=1" );
		$this->paymentways_list( 'scroll_to_list' );
	}
	// </Paymentways>

	// <Deliveries>
	function deliveries_list( $parameters='' )
	{
		$this->bigtabs->select_child( 'printbook' );
		$this->menuleft->select_child( 'deliveries_list' );
		
		$this->method_name = 'deliveries_list';
		$xml = "<data>";
		$xml .= "<list>";
		
		$t = new MysqlTable( 'pb_deliveries' );
		parent::define_pages( $t->get_count('status=1') );
		$t->find_all( 'status=1', 'position,id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$o = new PBDelivery( $a['id'] );
			
			if( $o->position == 0 )
			{
				$tp = new MysqlTable('pb_deliveries');
				$tp->fix_positions( '', "status=1" );
				$o->Load( $a['id'] );
			}
			
			$xml .= $o->Xml();
		}
		
		$xml .= "<last_position>{$o->position}</last_position>";
		
		$xml .= "</list>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'deliveries_list', $xml );
	}
	
	function delivery_edit( $save_result="" )
	{
		$last_position = intval( $_REQUEST['last_position'] );
		
		$this->bigtabs->select_child( 'printbook' );
		$this->menuleft->select_child( 'deliveries_list' );
		
		$this->method_name = 'delivery_edit';
		$xml = "<data>";
		
		$xml .= "<edit>";
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= "<last_position>$last_position</last_position>";
		$o = new PBDelivery( $_REQUEST['id'] );
		$xml .= $o->Xml();
		$xml .= "</edit>";
		
		$xml .= $this->GetPaymentwaysXml();
		$xml .= "</data>";
		
		$this->display( 'delivery_edit', $xml );
	}
	
	function GetPaymentwaysXml()
	{
		$retval = "<paymentways>";
		$t = new MysqlTable('pb_paymentways');
		$t->find_all("status=1","position,id");
		foreach( $t->data as $row )
		{
			$o = new PBPaymentway( $row['id'] );
			$retval .= $o->Xml();
		}
		$retval .= "</paymentways>";
		
		return( $retval );
	}
	
	function delivery_save()
	{
		global $db;

		$erx = "";
		
		$obj = new PBDelivery( $_REQUEST['object']['id'] );
		$last_position = intval( $_REQUEST['last_position'] );
		if( $last_position > 0 ) $_REQUEST['object']['position'] = $last_position + 1;
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$db->query( "delete from pb_deliveries_paymentways where delivery_id={$obj->id}" );
			foreach( $_REQUEST['paymentway'] as $paymentway )
			{
				$db->query( "insert into pb_deliveries_paymentways (delivery_id,paymentway_id) values ({$obj->id},{$paymentway})" );
			}
				
			$_REQUEST['id'] = $obj->id;
			$this->delivery_edit( 'true' );
		}
		else
		{
			$this->delivery_edit( 'false' );
		}
	}
	
	function delivery_del()
	{
		$o = new PBDelivery( $_REQUEST['id'] );
		$o->Delete();
		$this->deliveries_list( 'scroll_to_list' );
	}
	
	function deliveries_del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new PBDelivery( $id );
			$o->Delete();
		}
		$this->deliveries_list( 'scroll_to_list' );
	}
	
	function delivery_swap_position()
	{
		$src = new PBDelivery( $_REQUEST['src'] );
		$dst = new PBDelivery( $_REQUEST['dst'] );
		$this->SwapPosition( $src, $dst, "pb_deliveries", "status=1" );
		$this->deliveries_list( 'scroll_to_list' );
	}
	// </Deliveries>

	// <Orders>
	function orders_list1( $parameters='' )
	{
		$this->bigtabs->select_child( 'printbook' );
		$this->menuleft->select_child( 'orders_list1' );
		
		$this->method_name = 'orders_list1';
		$xml = "<data>";
		$xml .= "<list>";
		
		$t = new MysqlTable( 'pb_orders' );
		parent::define_pages( $t->get_count('status=1') );
		$t->find_all( 'status=1', 'id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$o = new PBOrder( $a['id'] );
			$xml .= $o->Xml();
		}
		
		$xml .= "</list>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'orders_list1', $xml );
	}
	
	function orders_list2( $parameters='' )
	{
		$this->bigtabs->select_child( 'printbook' );
		$this->menuleft->select_child( 'orders_list2' );
		
		$this->method_name = 'orders_list2';
		$xml = "<data>";
		$xml .= "<list>";
		
		$t = new MysqlTable( 'pb_orders' );
		parent::define_pages( $t->get_count('status=2') );
		$t->find_all( 'status=2', 'id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$o = new PBOrder( $a['id'] );
			$xml .= $o->Xml();
		}
		
		$xml .= "</list>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'orders_list2', $xml );
	}
	
	function orders_list3( $parameters='' )
	{
		$this->bigtabs->select_child( 'printbook' );
		$this->menuleft->select_child( 'orders_list3' );
		
		$this->method_name = 'orders_list3';
		$xml = "<data>";
		$xml .= "<list>";
		
		$t = new MysqlTable( 'pb_orders' );
		parent::define_pages( $t->get_count('status=3') );
		$t->find_all( 'status=3', 'id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$o = new PBOrder( $a['id'] );
			$xml .= $o->Xml();
		}
		
		$xml .= "</list>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'orders_list3', $xml );
	}
	
	function orders_list4( $parameters='' )
	{
		global $db;
		
		$this->bigtabs->select_child( 'printbook' );
		$this->menuleft->select_child( 'orders_list4' );
		
		$this->method_name = 'orders_list4';
		$xml = "<data>";
		$xml .= "<list>";
		
		$sort_number = intval( $_REQUEST['sort'] );
		if( !$sort_number && intval($_SESSION["sort_{$this->controller_name}_{$this->method_name}"]) != 0)
		{
			$sort_number = $_SESSION["sort_{$this->controller_name}_{$this->method_name}"];
		}
		if( !$sort_number && intval($_SESSION["sort_{$this->controller_name}_{$this->method_name}"]) == 0)
		{
			$sort_number = -8;
		}
		$sort_direction = ( $sort_number < 0 )? 'desc' : '';
		$order = "order by ";
		switch( abs( $sort_number ) )
		{
			case 1:
				$order .= "o.order_number {$sort_direction}";
				break;
			case 3:
				$order .= "c.lastname {$sort_direction}";
				break;
			case 4:
				$order .= "c.email {$sort_direction}";
				break;
			case 8:
			default:
				$order .= "o.id {$sort_direction}";
				break;
		}
		$_SESSION["sort_{$this->controller_name}_{$this->method_name}"] = $sort_number;
		
		parent::define_pages( $db->getone("select count(id) from pb_orders where status=4") );
		$sql = "select o.id from pb_orders o left join clients c on c.id=o.client_id where o.status=4 {$order} limit ". ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page;
		$rs = $db->query( $sql );
		while( $a = $rs->fetch_row() )
		{
			$o = new PBOrder( $a[0] );
			$xml .= $o->Xml();
		}
		
		$xml .= "<sort_number>{$sort_number}</sort_number>";
		$xml .= "</list>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'orders_list4', $xml );
	}
	
	function search()
	{
		global $db;
		
		$this->bigtabs->select_child( 'printbook' );
		$this->method_name = 'search';
		
		$order_number = trim( $_REQUEST["name"] );
		
		$xml = "<data>";
		$xml .= "<list>";
		
		parent::define_pages( $db->getone("select count(o.id) from pb_orders o left join clients c on c.id=o.client_id where o.status<9 and (o.order_number like '%{$order_number}%' or c.firstname like '%{$order_number}%' or c.lastname like '%{$order_number}%' or c.email like '%{$order_number}%')") );
		$rs = $db->query( "select o.id from pb_orders o left join clients c on c.id=o.client_id where o.status<9 and (o.order_number like '%{$order_number}%' or c.firstname like '%{$order_number}%' or c.lastname like '%{$order_number}%' or c.email like '%{$order_number}%')" );
		while( $a = $rs->fetch_row() )
		{
			$o = new PBOrder( $a[0] );
			$xml .= $o->Xml();
		}
		
		$xml .= "</list>";
		$xml .= "</data>";
		
		$this->display( 'search', $xml );
	}
	
	function order_del()
	{
		$o = new PBOrder( $_REQUEST['id'] );
		$status = $o->status;
		$o->DeleteFolder();
		$o->status = 99;
		$o->Save();

		switch( $status )
		{
			case 2:
				$this->orders_list2();
				break;
			case 3:
				$this->orders_list3();
				break;
			case 4:
				$this->orders_list4();
				break;
			case 1:
			default:
				$this->orders_list1();
				break;
		}
	}
	
	function orders_del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new PBOrder( $id );
			if( !$status ) $status = $o->status;
			$o->DeleteFolder();
			$o->status = 99;
			$o->Save();
		}
		
		switch( $status )
		{
			case 2:
				$this->orders_list2();
				break;
			case 3:
				$this->orders_list3();
				break;
			case 4:
				$this->orders_list4();
				break;
			case 1:
			default:
				$this->orders_list1();
				break;
		}
	}
	
	function orders_change_status_marked()
	{
		$status = $_REQUEST['status']; if( !$status ) $status = 3;
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new PBOrder( $id );
			$o->status = $status;
			$o->Save();
		}
		
		switch( $status-1 )
		{
			case 2:
				$this->orders_list2();
				break;
			case 3:
				$this->orders_list3();
				break;
			case 4:
				$this->orders_list4();
				break;
			case 1:
			default:
				$this->orders_list1();
				break;
		}
	}
	
	function orders_change_status_all()
	{
		global $db;

		$status = $_REQUEST['status']; if( !$status ) $status = 3;
		$sql = "update pb_orders set status={$status} where status=". ($status-1);
		$db->query( $sql );
		
		switch( $status-1 )
		{
			case 2:
				$this->orders_list2();
				break;
			case 3:
				$this->orders_list3();
				break;
			case 4:
				$this->orders_list4();
				break;
			case 1:
			default:
				$this->orders_list1();
				break;
		}
	}
	
	function order_view()
	{
		$this->bigtabs->select_child( 'printbook' );

		$o = new PBOrder( $_REQUEST['id'], true );
		$o->LoadImages();
		switch( $o->status )
		{
			case 2:
				$this->menuleft->select_child( 'orders_list2' );
				break;
			case 3:
				$this->menuleft->select_child( 'orders_list3' );
				break;
			case 4:
				$this->menuleft->select_child( 'orders_list4' );
				break;
			case 1:
			default:
				$this->menuleft->select_child( 'orders_list1' );
				break;
		}
		
		
		$this->method_name = 'order_view';
		$xml = "<data>";
		
		if( $o->IsLoaded() )
		{
			$xml .= $o->Xml();
		}
		
		$xml .= "</data>";
		
		$this->display( 'order_view', $xml );
	}
	
	function order_send()
	{
		$o = new PBOrder( $_REQUEST['id'] );
		$subject = $_REQUEST['subject'];
		$body = $_REQUEST['body'];
		if( $o->IsLoaded() )
		{
			//Send email to admin and client
			$settings = new Settings();
			$sop_settings = new PBSettings();
			$mail = new AMail( 'windows-1251' );
				$mail->subject = $subject;
				
				$mail->from_name = $settings->from_name;
				$mail->from_email = $settings->from_email;
				$mail->to_name = $o->client->firstname ." ". $o->client->lastname;
				$mail->to_email = $o->client->email;
			$mail->Send( $body );
		}        
		$this->order_view();
	}
	// </Orders>
} 	

?>
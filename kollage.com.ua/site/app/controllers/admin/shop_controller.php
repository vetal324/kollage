<?php
/*****************************************************
 Class v.1.0, 02.2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

include_once( 'base_controller.php' );

class shop_controller extends base_controller
{
	var $folder = 'shop';
	var $redirect = null;
	
	function shop_controller()
	{
		parent::base_controller();
		
		$this->make_small_tabs( $this->find_first_category() );
		$this->make_menu();
		
		$this->redirect = trim( $_REQUEST['redirect'] );
	}
	
	function make_small_tabs( $category_id=0, $category=null )
	{
		$this->smalltabs->del_all();
		if( $category_id )
		{
			$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/shop/smalltabs/list'), "?".$this->controller_name.".index&category_id={$category_id}&page=". intval($_REQUEST['page'])) );
			$this->smalltabs->add_item( new WETab('edit', $this->dictionary->get('/locale/shop/smalltabs/edit'), "?".$this->controller_name.".category_edit&category_id={$category_id}") );
		}
		$this->smalltabs->add_item( new WETab('features', $this->dictionary->get('/locale/shop/smalltabs/features'), "?".$this->controller_name.".category_features&category_id={$category_id}") );
		/*
		if( $category && $category->IsLoaded() && $category->parent_id == 0 )
		{
			$this->smalltabs->add_item( new WETab('features', $this->dictionary->get('/locale/shop/smalltabs/features'), "?".$this->controller_name.".category_features&category_id={$category_id}") );
		}*/
	}
	
	function make_menu()
	{
		$this->menuleft->del_all();
		$t = new MysqlTable( 'shop_product_categories' );
		$t->find_all( "parent_id is null", "position,id" );
		foreach( $t->data as $row )
		{
			$o = new ShopCategory( $row['id'] );
			$submenu = new WEMenu( "submenu{$o->id}", $o->name, "?".$this->controller_name.".index&category_id={$o->id}" );
			$this->menuleft->add_item( $submenu );
			$t2 = new MysqlTable( 'shop_product_categories' );
			$t2->find_all( "parent_id={$o->id}", "position,id" );
			foreach( $t2->data as $row2 )
			{
				$cs = new ShopCategory( $row2['id'] );
				//$submenu->add_item( new WEMenuItem( "c{$cs->id}", $cs->name, "?".$this->controller_name.".index&category_id={$cs->id}" ) );
				$submenu2 = new WEMenu( "submenu{$cs->id}", $cs->name, "?".$this->controller_name.".index&category_id={$cs->id}" );
				$submenu->add_item( $submenu2 );
				$t3 = new MysqlTable( 'shop_product_categories' );
				$t3->find_all( "parent_id={$cs->id}", "position,id" );
				foreach( $t3->data as $row3 )
				{
					$cs2 = new ShopCategory( $row3['id'] );
					$submenu2->add_item( new WEMenuItem( "c{$cs2->id}", $cs2->name, "?".$this->controller_name.".index&category_id={$cs2->id}" ) );
				}
			}
		}
		$this->menuleft->add_item( new WEMenuSeparator() );
		$this->menuleft->add_item( new WEMenuItem( "brands", $this->dictionary->get('/locale/shop/menuleft/brands'), "?brands.index" ) );
		$this->menuleft->add_item( new WEMenuItem( "settings", $this->dictionary->get('/locale/shop/menuleft/settings'), "?".$this->controller_name.".settings_edit" ) );
		$this->menuleft->add_item( new WEMenuItem( "points_list", $this->dictionary->get('/locale/shop/menuleft/points_list'), "?".$this->controller_name.".points_list" ) );
		$this->menuleft->add_item( new WEMenuItem( "deliveries_list", $this->dictionary->get('/locale/shop/menuleft/deliveries_list'), "?".$this->controller_name.".deliveries_list" ) );
		$this->menuleft->add_item( new WEMenuItem( "paymentways_list", $this->dictionary->get('/locale/shop/menuleft/paymentways_list'), "?".$this->controller_name.".paymentways_list" ) );
		//$this->menuleft->add_item( new WEMenuItem( "discounts_list", $this->dictionary->get('/locale/shop/menuleft/discounts_list'), "?".$this->controller_name.".discounts_list" ) );
		$this->menuleft->add_item( new WEMenuItem( "prices", $this->dictionary->get('/locale/shop/menuleft/prices'), "?".$this->controller_name.".prices_list" ) );
		$this->menuleft->add_item( new WEMenuSeparator() );
		$this->menuleft->add_item( new WEMenuItem( "articles", $this->dictionary->get('/locale/shop/menuleft/articles'), "?shop_articles.index" ) );
		$this->menuleft->add_item( new WEMenuSeparator() );
		$this->menuleft->add_item( new WEMenuItem( "old_products", $this->dictionary->get('/locale/shop/menuleft/old'), "?shop.old_products" ) );
	}
	
	function make_elements()
	{
		$retval = "";
		
		$retval .= $this->bigtabs->xml();
		$retval .= $this->smalltabs->xml();
		$retval .= $this->menuleft->xml();
		
		$retval .= "<left_panel show='yes'><categories>";
		$t = new MysqlTable( 'shop_product_categories' );
		$t->find_all( "parent_id is null", "position,id" );
		foreach( $t->data as $row )
		{
			$o = new ShopCategory( $row['id'] );
			$retval .= $o->Xml();
		}
		$retval .= "</categories></left_panel>";
		
		return( $retval );
	}
	
	function index( $parameters='' )
	{
		$category_id = intval( $_REQUEST['category_id'] );
		if( $category_id==0 ) $category_id = $this->find_first_category();
		$category = new ShopCategory( $category_id );
		
		if( $_REQUEST['action'] == 'save' )
		{
			$row = 1;
			$id = intval( $_REQUEST['cid'.$row] );
			$p = new ShopProduct(0,false);
			while( $id > 0 )
			{
				$number = $_REQUEST['number'.$row];
				$price = floatval( $_REQUEST['price'.$row] );
				$price_opt = floatval( $_REQUEST['price_opt'.$row] );
				$price_opt2 = floatval( $_REQUEST['price_opta'.$row] );
				
				//print_r(Array( 'id'=>$id, 'price'=>$price, 'price_opt'=>$price_opt, 'price_opt2'=>$price_opt2, 'number'=>$number ));
				$p->Save( Array( 'id'=>$id, 'price'=>$price, 'price_opt'=>$price_opt, 'price_opt2'=>$price_opt2, 'number'=>$number ) );
				
				$row++;
				$id = intval( $_REQUEST['cid'.$row] );
			}
		}
		
		$this->make_small_tabs( $category_id, $category );
		$this->make_menu();
		$this->bigtabs->select_child( 'shop' );
		$this->smalltabs->select_child( 'list' );
		$this->menuleft->select_child( "submenu{$category_id}" );
		$this->menuleft->select_child( "c{$category_id}" );
		
		$this->method_name = 'list';
		$xml = "<data>";
		$xml .= "<products category_id='{$category_id}'>";
		
		$t = new MysqlTable( 'shop_products' );
		parent::define_pages( $t->get_count("category_id={$category_id} and status<10") );
		$t->find_all( "category_id={$category_id} and status<10", 'brand_id,id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		//$sql = "select p.id from shop_products p left join shop_brands b on p.brand_id=b.id where p.category_id={$category_id} order by p.brand_id";
		foreach( $t->data as $a )
		{
			$product = new ShopProduct( $a['id'], false );
			
			if( $product->position == 0 )
			{
				$tp = new MysqlTable('shop_products');
				$tp->fix_positions("","category_id={$category_id}");
				$product->Load( $a['id'] );
			}
			
			$xml .= $product->Xml();
		}
		
		if( !$product ) $product = new ShopProduct(0,false);
		$xml .= "<last_position>". $product->GetLastPosition("category_id={$category_id}") ."</last_position>";
		
		$xml .= "</products>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'list', $xml );
	}
	
	function old_products( $parameters='' )
	{
		global $db;
		
		$this->make_small_tabs( $category_id, $category );
		$this->make_menu();
		$this->bigtabs->select_child( 'shop' );
		$this->smalltabs->select_child( 'list' );
		$this->menuleft->select_child( "old_products" );
		
		$monthes = intval( $_REQUEST['monthes'] ); if( $monthes == 0 ) $monthes = 4;
		
		$category_id = intval( $_REQUEST['category_id'] );
		if( $category_id > 0 )
		{
			$db->query( "set @ids=''" );
			$db->query( "set max_sp_recursion_depth=10000" );
			$db->query( "call shop_product_categories({$category_id},@ids)" );
			$ids = $db->getone( "select @ids" );
			$category_sql = " and category_id in ({$ids})";
		}
		else $category_sql = "";
		
		$this->method_name = 'old_products';
		$xml = "<data>";
		$xml .= "<products category_id='{$category_id}'>";
		
		$t = new MysqlTable( 'shop_products' );
		parent::define_pages( $t->get_count("date_sub(curdate(),interval 1 month)>=date(created_at) and (updated_at is null or date_sub(curdate(),interval {$monthes} month)>=date(updated_at)) {$category_sql}") );
		$sql = "select id from shop_products where date_sub(curdate(),interval 1 month)>=date(created_at) and (updated_at is null or date_sub(curdate(),interval {$monthes} month)>=date(updated_at))  {$category_sql} order by created_at,id limit ". ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page;
		//echo $sql;
		$rs = $db->query( $sql );
		while( $row = $rs->fetch_row() )
		{
			//$product = new ShopProduct( $a['id'], false );
			$product = new ShopProduct( $row[0], false );
			$xml .= $product->Xml();
		}
		$xml .= "<last_position>0</last_position>";
		$xml .= "<monthes>{$monthes}</monthes>";
		
		$xml .= $this->CategoriesXml();
		
		$xml .= "</products>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'old_products', $xml );
	}
	
	function search( $parameters='' )
	{
		global $db;
		
		$category_id = intval( $_REQUEST['category_id'] );
		if( $category_id==0 ) $category_id = $this->find_first_category();
		$category = new ShopCategory( $category_id );
		
		$this->make_small_tabs( $category_id, $category );
		$this->make_menu();
		$this->bigtabs->select_child( 'shop' );
		//$this->smalltabs->select_child( 'list' );
		//$this->menuleft->select_child( "submenu{$category_id}" );
		//$this->menuleft->select_child( "c{$category_id}" );
		
		$this->method_name = 'search';
		$xml = "<data>";
		$xml .= "<products category_id='{$category_id}'>";
		
		$str = trim( $_REQUEST['name'] );
		
		$t = new MysqlTable( 'shop_products' );
		$this->define_pages( $db->getone("select count(p.id) from shop_products p left join dictionary d on d.parent_id=p.id where d.folder='shop_products' and d.name='name' and (d.value like '%{$str}%' or p.number like '%{$str}%') order by p.position,p.id") );
		$sql = "select p.id from shop_products p left join dictionary d on d.parent_id=p.id where d.folder='shop_products' and d.name='name' and (d.value like '%{$str}%' or p.number like '%{$str}%') order by p.position,p.id limit ". ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page;
		$rs = $db->query( $sql );
		while( $row = $rs->fetch_row() )
		{
			$product = new ShopProduct( $row[0], false );
			$xml .= $product->Xml();
		}
		
		$xml .= "</products>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'search', $xml );
	}
	
	function set_status()
	{
		$id = intval( $_REQUEST['id'] );
		$v = intval( $_REQUEST['v'] );
		$o = new ShopProduct( $id );
		if( $o->IsLoaded() )
		{
			$o->status = $v;
			$o->Save();
		}
		
		if( $this->redirect ) redirect( $this->redirect );
		else $this->index();
	}
	
	function set_show_in_shop_news()
	{
		$id = intval( $_REQUEST['id'] );
		$v = intval( $_REQUEST['v'] );
		if( $v!=0 && $v!=1 ) $v = 0;
		$o = new ShopProduct( $id );
		if( $o->IsLoaded() )
		{
			$o->show_in_shop_news = $v;
			$o->Save();
			
			$_REQUEST['category_id'] = $o->category_id;
		}
		
		if( $this->redirect ) redirect( $this->redirect );
		else $this->index();
	}
	
	function set_show_in_best()
	{
		$id = intval( $_REQUEST['id'] );
		$v = intval( $_REQUEST['v'] );
		if( $v!=0 && $v!=1 ) $v = 0;
		$o = new ShopProduct( $id );
		if( $o->IsLoaded() )
		{
			$o->show_in_best = $v;
			$o->Save();
			
			$_REQUEST['category_id'] = $o->category_id;
		}
		
		if( $this->redirect ) redirect( $this->redirect );
		else $this->index();
	}
	
	function set_is_new()
	{
		$id = intval( $_REQUEST['id'] );
		$v = intval( $_REQUEST['v'] );
		if( $v!=0 && $v!=1 ) $v = 0;
		$o = new ShopProduct( $id );
		if( $o->IsLoaded() )
		{
			$o->is_new = $v;
			$o->Save();
			
			$_REQUEST['category_id'] = $o->category_id;
		}
		
		if( $this->redirect ) redirect( $this->redirect );
		else $this->index();
	}
	
	function set_is_action()
	{
		$id = intval( $_REQUEST['id'] );
		$v = intval( $_REQUEST['v'] );
		if( $v!=0 && $v!=1 ) $v = 0;
		$o = new ShopProduct( $id );
		if( $o->IsLoaded() )
		{
			$o->is_action = $v;
			$o->Save();
			
			$_REQUEST['category_id'] = $o->category_id;
		}
		
		if( $this->redirect ) redirect( $this->redirect );
		else $this->index();
	}
	
	function find_first_category()
	{
		$retval = 0;
		
		$t = new MysqlTable( 'shop_product_categories' );
		if( $row = $t->find_first("","position,id") )
		{
			$retval = intval( $row['id'] );
		}
		
		return( $retval );
	}
	
	function category_add()
	{
		$o = new ShopCategory();
		$o->folder = $this->folder;
		$o->name = $_REQUEST['name'];
		$o->Save();
		
		if( $this->redirect ) redirect( $this->redirect );
		else $this->index();
	}
	
	function category_del()
	{
		$o = new ShopCategory( $_REQUEST['category_id'] );
		$o->Delete();
		$_REQUEST['category_id'] = $this->find_first_category();
		
		if( $this->redirect ) redirect( $this->redirect );
		else $this->index();
	}
	
	function category_edit( $save_result="" )
	{
		$category_id = intval( $_REQUEST['category_id'] );
		if( $category_id==0 ) $category_id = $this->find_first_category();
		$category = new ShopCategory( $category_id );
		
		$this->make_small_tabs( $category_id, $category );
		$this->make_menu();
		$this->bigtabs->select_child( 'shop' );
		$this->smalltabs->select_child( 'edit' );
		$this->menuleft->select_child( "submenu{$category_id}" );
		$this->menuleft->select_child( "c{$category_id}" );
		
		$this->method_name = 'category_edit';
		$xml = "<data>";
		
		$xml .= "<edit>";
		$xml .= "<save_result>$save_result</save_result>";
		$o = new ShopCategory( $_REQUEST['category_id'] );
		$xml .= $o->Xml();
		$xml .= $this->SubcategoriesXml( $category_id );
		$xml .= $this->CategoriesXml();
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'category_edit', $xml );
	}
	
	function category_save()
	{
		$erx = "";
		
		$obj = new ShopCategory( $_REQUEST['object']['id'] );
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->category_edit( 'true' );
		}
		else
		{
			$this->category_edit( 'false' );
		}
	}
	
	function CategoriesXml()
	{
		$xml = "<categories>";
		$t = new MysqlTable('shop_product_categories');
		$t->find_all( "parent_id is null", "position,id" );
		
		foreach( $t->data as $row )
		{
			$parent_id = $row['id']; $level = 1;
			$o = new ShopCategory( $parent_id );
			$xml .= "<category id='{$o->id}' name='{$o->name}'>";
			$xml .= "<name><![CDATA[{$o->name}]]></name>";
			$xml .= $this->SubcategoriesXmlRC( $xml, $parent_id, $level );
			$xml .= "</category>";
		}
		
		$xml .= "</categories>";
		
		return( $xml );
	}
	
	function SubcategoriesXml( $root_id )
	{
		$xml = "";
		$root = new ShopCategory( $root_id );
		if( $root->IsLoaded() )
		{
			$xml .= "<subcategories>";
			$xml .= "<category id='$root_id'>";
			$xml .= "<name><![CDATA[{$root->name}]]></name>";
			$parent_id = $root_id; $level = 1;
			$xml .= $this->SubcategoriesXmlRC( $xml, $parent_id, 1 );
			$xml .= "</category>";
			$xml .= "</subcategories>";
		}
		
		return( $xml );
	}
	
	function SubcategoriesXmlRC( &$xml, $parent_id, $level )
	{
		global $db;

		$rs = $db->query( "select id,parent_id from shop_product_categories where parent_id={$parent_id} order by position" );
		while( $row = $rs->fetch_row() )
		{
			$o = new ShopCategory( $row[0] );
			$xml .= "<category id='{$o->id}' level='{$level}' prefix='". str_repeat("&#160;&#160;",$level) ."' parent_id='{$o->parent_id}'>";
			$xml .= "<name><![CDATA[{$o->name}]]></name>";
			$xml .= $this->SubcategoriesXmlRC( $xml, $o->id, $level+1 );
			$xml .= "</category>";
		}
	}
	
	function subcategory_add()
	{
		$o = new ShopCategory();
		$_REQUEST['object']['folder'] = $this->folder;
		$o->Save( $_REQUEST['object'] );
		$this->category_edit();
	}
	
	function subcategory_save()
	{
		$o = new ShopCategory();
		$o->Save( $_REQUEST['object'] );
		$this->category_edit();
	}
	
	function subcategory_del()
	{
		$o = new ShopCategory( $_REQUEST['id'] );
		$o->Delete();
		$this->category_edit();
	}
	
	function edit( $save_result="", $scroll="" )
	{
		$this->bigtabs->select_child( 'shop' );
		$this->smalltabs->select_child( 'list' );
		$this->page = intval( $_REQUEST['page'] );
		
		$this->method_name = 'edit';
		$xml = "<data>";
		
		$xml .= "<product_edit>";
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= "<last_position>$last_position</last_position>";
		$product = new ShopProduct( $_REQUEST['id'] );
		$product->LoadFeatures( true );

		
		$category_id = intval( $_REQUEST['category_id'] );
		if( !$category_id ) $category_id = $product->category_id;
		$category = new ShopCategory( $category_id );
		$this->menuleft->select_child( "submenu{$category_id}" );
		$this->menuleft->select_child( "c{$category_id}" );
		$this->make_small_tabs( $category_id, $category );
		
		if( !$product->IsLoaded() )
		{
			$product->category_id = $category_id;
			$product->folder = $this->folder;
		}
		$xml .= $product->Xml();
		
		//$xml .= $this->GetBrandsXml();
		$xml .= $this->GetPointsXml( $product->id );
		$xml .= $this->CategoriesXml();
		//$xml .= $this->FeaturesJS( $category_id );
		$xml .= $this->FeaturesXmlForProductEdit( $category_id );
		
		$xml .= "<scroll>{$scroll}</scroll>";
		
		$xml .= "</product_edit>";
		
		$xml .= "</data>";
		
		$this->display( 'edit', $xml );
	}
	
	function change_category()
	{
		$product = new ShopProduct( $_REQUEST['id'] );
		$category_id = intval( $_REQUEST['category_id'] );
		if( $product->IsLoaded() && $category_id>0 )
		{
			$product->category_id = $category_id;
			$product->Save();
		}
		$this->edit();
	}
	
	function GetBrandsXml()
	{
		global $db;

		$retval = "<brands>";
		$sql = "select b.id from shop_brands b left join dictionary d on d.parent_id=b.id where b.status=1 and d.folder='shop_brands' and d.name='name' order by d.value";
		$rs = $db->query( $sql );
		while( $row = $rs->fetch_row() )
		{
			$o = new ShopBrand( $row[0] );
			$retval .= $o->Xml();
		}
		$retval .= "</brands>";
		
		return( $retval );
	}
	
	function GetPointsXml( $product_id )
	{
		$retval = "<points>";
		$t = new MysqlTable('shop_points');
		$t->find_all( "status=1", "position,id" );
		$t2 = new MysqlTable('shop_product_availability');
		foreach( $t->data as $row )
		{
			$o = new ShopPoint( $row['id'] );
			$row2 = $t2->find_first("product_id={$product_id} and point_id={$o->id}");
			$retval .= "<point><id>{$o->id}</id><name>{$o->name}</name><address>{$o->address}</address><quantity>". intval($row2['quantity'])."</quantity></point>";
		}
		$retval .= "</points>";
		
		return( $retval );
	}
	
	function save()
	{
		$erx = "";
		
		$obj = new ShopProduct( $_REQUEST['product']['id'] );
		$last_position = intval( $_REQUEST['last_position'] );
		if( $last_position > 0 ) $_REQUEST['product']['position'] = $last_position + 1;
		
		$_REQUEST['product']['ingress'] = DeleteHTMLComments( $_REQUEST['product']['ingress'] );
		$_REQUEST['product']['description'] = DeleteHTMLComments( $_REQUEST['product']['description'] );
		if( trim($_REQUEST['product']['name'])!='' && $obj->Save( $_REQUEST['product'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$i = 1;
			$t = new MysqlTable('shop_product_availability');
			while( $point_id = $_REQUEST['point_id'.$i] )
			{
				$quantity = intval( $_REQUEST['quantity'.$i] );
				if( $row = $t->find_first("product_id={$obj->id} and point_id={$point_id}") )
				{
					$pa = new ShopAvailability( $row['id'] );
					$pa->quantity = $quantity;
					$pa->Save();
					
				}
				else
				{
					$pa = new ShopAvailability();
					$pa->quantity = $quantity;
					$pa->product_id = $obj->id;
					$pa->point_id = $point_id;
					$pa->Save();
				}
				$i++;
			}
			
			$this->edit( 'true' );
		}
		else
		{
			$this->edit( 'false' );
		}
	}
	
	function save_features() {
		global $db;
		
		$id = intval( $_REQUEST['id'] );
		
		$product = new ShopProduct( $id );
		if( $product->IsLoaded() ) {
			$db->query( "delete from shop_product_features where product_id={$id} and feature_value_id>0" );
			$i = 1;
			$feature_value_id = intval( $_REQUEST['feature_id_'.$i] );
			while( $feature_value_id >0 || $feature_value_id==-1 ) {
				if( $feature_value_id > 0 ) {
					$db->query( "insert into shop_product_features (product_id,feature_value_id) values({$id},{$feature_value_id})" );
				}
				$i++;
				$feature_value_id = intval( $_REQUEST['feature_id_'.$i] );
			}
			for( $i=1; $i<=3; $i++ ) {
				$individual_name = trim( $_REQUEST['individual_name'. $i] );
				$individual_value = trim( $_REQUEST['individual_value'. $i] );
				if( $individual_name!='' && $individual_value!='' ) {
					$db->query( "insert into shop_product_features (product_id,individual_name,individual_value) values({$id},'{$individual_name}','{$individual_value}')" );
				}
			}
		}
		
		$this->edit( 'true' );
	}
	
	function del_individual_feature() {
		global $db;
		
		$id = intval( $_REQUEST['id'] );
		$feature_id = intval( $_REQUEST['feature_id'] );
		
		$product = new ShopProduct( $id );
		if( $product->IsLoaded() ) {
			$db->query( "delete from shop_product_features where product_id={$id} and id={$feature_id}" );
		}
		
		$this->edit( 'true' );
	}
	
	function del()
	{
		$this->delete();
	}
	
	function delete()
	{
		$product = new ShopProduct( $_REQUEST['id'] );
		$product->Delete();
		if( $_REQUEST['ret'] ) $this->old_products();
		else {
			if( $this->redirect ) redirect( $this->redirect );
			else $this->index();
		}
	}
	
	function del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new ShopProduct( $id );
			$o->Delete();
		}
		if( $_REQUEST['ret'] ) $this->old_products();
		else {
			if( $this->redirect ) redirect( $this->redirect );
			else $this->index();
		}
	}
	
	function del_image()
	{
		$product = new ShopProduct( $_REQUEST['id'] );
		$product->DeleteImage();
		$this->edit( 'true' );
	}
	
	function del_media()
	{
		$image = new Image( $_REQUEST['image_id'] );
		$image->Delete();
		$this->edit( 'true' );
	}
	
	function swap_position()
	{
		$src = new ShopProduct( $_REQUEST['src'] );
		$dst = new ShopProduct( $_REQUEST['dst'] );
		$this->SwapPosition( $src, $dst, "shop_products", "category_id={$dst->category_id}" );
		$_REQUEST['category_id'] = $dst->category_id;
		
		if( $this->redirect ) redirect( $this->redirect );
		else $this->index( 'scroll_to_list' );
	}
	
	function move_up_gallery()
	{
		$t = new MysqlTable('images');
		$t->move_position_up( $_REQUEST['image_id'], $this->folder, "parent_id={$_REQUEST['id']}" );
		$this->edit( 'scroll_to_media' );
	}
	
	function move_down_gallery()
	{
		$t = new MysqlTable('images');
		$t->move_position_down( $_REQUEST['image_id'], $this->folder, "parent_id={$_REQUEST['id']}" );
		$this->edit( 'scroll_to_media' );
	}

	// <Discounts>
	function discounts_list( $parameters='' )
	{
		$this->bigtabs->select_child( 'shop' );
		$this->menuleft->select_child( 'discounts_list' );
		$this->make_small_tabs();
		
		$this->method_name = 'discounts_list';
		$xml = "<data>";
		$xml .= "<list>";
		
		$t = new MysqlTable( 'shop_discounts' );
		parent::define_pages( $t->get_count() );
		$t->find_all( '', 'position,id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$o = new ShopDiscount( $a['id'] );
			
			if( $o->position == 0 )
			{
				$tp = new MysqlTable('shop_discounts');
				$tp->fix_positions();
				$o->Load( $a['id'] );
			}
			
			$xml .= $o->Xml();
		}
		
		$xml .= "<last_position>{$o->position}</last_position>";
		
		$xml .= "</list>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'discounts_list', $xml );
	}
	
	function discount_edit( $save_result="" )
	{
		$last_position = intval( $_REQUEST['last_position'] );
		
		$this->bigtabs->select_child( 'shop' );
		$this->menuleft->select_child( 'discounts_list' );
		
		$this->method_name = 'discount_edit';
		$xml = "<data>";
		
		$xml .= "<edit>";
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= "<last_position>$last_position</last_position>";
		$o = new ShopDiscount( $_REQUEST['id'] );
		$xml .= $o->Xml();
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'discount_edit', $xml );
	}
	
	function discount_save()
	{
		$erx = "";
		
		$obj = new ShopDiscount( $_REQUEST['discount']['id'] );
		$last_position = intval( $_REQUEST['last_position'] );
		if( $last_position > 0 ) $_REQUEST['discount']['position'] = $last_position + 1;
		
		if( $obj->Save( $_REQUEST['discount'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->discount_edit( 'true' );
		}
		else
		{
			$this->discount_edit( 'false' );
		}
	}
	
	function discount_del()
	{
		$o = new ShopDiscount( $_REQUEST['id'] );
		$o->Delete();
		$this->discounts_list( 'scroll_to_list' );
	}
	
	function discounts_del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new ShopDiscount( $id );
			$o->Delete();
		}
		$this->discounts_list( 'scroll_to_list' );
	}
	
	function discount_move_up()
	{
		$t = new MysqlTable('shop_discounts');
		$t->move_position_up( $_REQUEST['id'] );
		$this->discounts_list( 'scroll_to_list' );
	}
	
	function discount_move_down()
	{
		$t = new MysqlTable('shop_discounts');
		$t->move_position_down( $_REQUEST['id'] );
		$this->discounts_list( 'scroll_to_list' );
	}
	
	function discount_swap_position()
	{
		$src = new ShopDiscount( $_REQUEST['src'] );
		$dst = new ShopDiscount( $_REQUEST['dst'] );
		$this->SwapPosition( $src, $dst, "shop_discounts", "status=1" );
		$this->discounts_list( 'scroll_to_list' );
	}
	// </Discounts>
	
	// <Paymentways>
	function paymentways_list( $parameters='' )
	{
		$this->bigtabs->select_child( 'shop' );
		$this->menuleft->select_child( 'paymentways_list' );
		$this->make_small_tabs();
		
		$this->method_name = 'paymentways_list';
		$xml = "<data>";
		$xml .= "<list>";
		
		$t = new MysqlTable( 'shop_paymentways' );
		parent::define_pages( $t->get_count('status=1') );
		$t->find_all( 'status=1', 'position,id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$o = new ShopPaymentway( $a['id'] );
			
			if( $o->position == 0 )
			{
				$tp = new MysqlTable('shop_paymentways');
				$tp->fix_positions( '', "status=1" );
				$o->Load( $a['id'] );
			}
			
			$xml .= $o->Xml();
		}
		
		$xml .= "<last_position>{$o->position}</last_position>";
		
		$xml .= "</list>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'paymentways_list', $xml );
	}
	
	function paymentway_edit( $save_result="" )
	{
		$last_position = intval( $_REQUEST['last_position'] );
		
		$this->bigtabs->select_child( 'shop' );
		$this->menuleft->select_child( 'paymentways_list' );
		
		$this->method_name = 'paymentway_edit';
		$xml = "<data>";
		
		$xml .= "<edit>";
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= "<last_position>$last_position</last_position>";
		$o = new ShopPaymentway( $_REQUEST['id'] );
		$xml .= $o->Xml();
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'paymentway_edit', $xml );
	}
	
	function paymentway_save()
	{
		$erx = "";
		
		$obj = new ShopPaymentway( $_REQUEST['object']['id'] );
		$last_position = intval( $_REQUEST['last_position'] );
		if( $last_position > 0 ) $_REQUEST['object']['position'] = $last_position + 1;
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->paymentway_edit( 'true' );
		}
		else
		{
			$this->paymentway_edit( 'false' );
		}
	}
	
	function paymentway_del()
	{
		$o = new ShopPaymentway( $_REQUEST['id'] );
		$o->Delete();
		$this->paymentways_list( 'scroll_to_list' );
	}
	
	function paymentways_del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new ShopPaymentway( $id );
			$o->Delete();
		}
		$this->paymentways_list( 'scroll_to_list' );
	}
	
	function paymentway_move_up()
	{
		$t = new MysqlTable('shop_paymentways');
		$t->move_position_up( $_REQUEST['id'] );
		$this->paymentways_list( 'scroll_to_list' );
	}
	
	function paymentway_move_down()
	{
		$t = new MysqlTable('shop_paymentways');
		$t->move_position_down( $_REQUEST['id'] );
		$this->paymentways_list( 'scroll_to_list' );
	}
	
	function paymentway_swap_position()
	{
		$src = new ShopPaymentway( $_REQUEST['src'] );
		$dst = new ShopPaymentway( $_REQUEST['dst'] );
		$this->SwapPosition( $src, $dst, "shop_paymentways", "status=1" );
		$this->paymentways_list( 'scroll_to_list' );
	}
	// </Paymentways>

	// <Deliveries>
	function deliveries_list( $parameters='' )
	{
		$this->bigtabs->select_child( 'shop' );
		$this->menuleft->select_child( 'deliveries_list' );
		$this->make_small_tabs();
		
		$this->method_name = 'deliveries_list';
		$xml = "<data>";
		$xml .= "<list>";
		
		$t = new MysqlTable( 'shop_deliveries' );
		parent::define_pages( $t->get_count('status=1') );
		$t->find_all( 'status=1', 'position,id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$o = new ShopDelivery( $a['id'] );
			$xml .= $o->Xml();
		}
		
		$xml .= "<last_position>{$o->position}</last_position>";
		
		$xml .= "</list>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'deliveries_list', $xml );
	}
	
	function delivery_edit( $save_result="" )
	{
		$last_position = intval( $_REQUEST['last_position'] );
		
		$this->bigtabs->select_child( 'shop' );
		$this->menuleft->select_child( 'deliveries_list' );
		
		$this->method_name = 'delivery_edit';
		$xml = "<data>";
		
		$xml .= "<edit>";
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= "<last_position>$last_position</last_position>";
		$o = new ShopDelivery( $_REQUEST['id'] );
		$xml .= $o->Xml();
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'delivery_edit', $xml );
	}
	
	function delivery_save()
	{
		$erx = "";
		
		$obj = new ShopDelivery( $_REQUEST['object']['id'] );
		$last_position = intval( $_REQUEST['last_position'] );
		if( $last_position > 0 ) $_REQUEST['object']['position'] = $last_position + 1;
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->delivery_edit( 'true' );
		}
		else
		{
			$this->delivery_edit( 'false' );
		}
	}
	
	function delivery_del()
	{
		$o = new ShopDelivery( $_REQUEST['id'] );
		$o->Delete();
		$this->deliveries_list( 'scroll_to_list' );
	}
	
	function deliveries_del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new ShopDelivery( $id );
			$o->Delete();
		}
		$this->deliveries_list( 'scroll_to_list' );
	}
	
	function delivery_move_up()
	{
		$t = new MysqlTable('shop_deliveries');
		$t->move_position_up( $_REQUEST['id'] );
		$this->deliveries_list( 'scroll_to_list' );
	}
	
	function delivery_move_down()
	{
		$t = new MysqlTable('shop_deliveries');
		$t->move_position_down( $_REQUEST['id'] );
		$this->deliveries_list( 'scroll_to_list' );
	}
	
	function delivery_swap_position()
	{
		$src = new ShopDelivery( $_REQUEST['src'] );
		$dst = new ShopDelivery( $_REQUEST['dst'] );
		$this->SwapPosition( $src, $dst, "shop_deliveries", "status=1" );
		$this->deliveries_list( 'scroll_to_list' );
	}
	// </Deliveries>

	// <Points>
	function points_list( $parameters='' )
	{
		$this->bigtabs->select_child( 'shop' );
		$this->menuleft->select_child( 'points_list' );
		$this->make_small_tabs();
		
		$this->method_name = 'points_list';
		$xml = "<data>";
		$xml .= "<list>";
		
		$t = new MysqlTable( 'shop_points' );
		parent::define_pages( $t->get_count('status=1') );
		$t->find_all( 'status=1', 'position,id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$o = new ShopPoint( $a['id'] );
			
			if( $o->position == 0 )
			{
				$tp = new MysqlTable('shop_points');
				$tp->fix_positions( '', "status=1" );
				$o->Load( $a['id'] );
			}
			
			$xml .= $o->Xml();
		}
		
		$xml .= "<last_position>{$o->position}</last_position>";
		
		$xml .= "</list>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'points_list', $xml );
	}
	
	function point_edit( $save_result="" )
	{
		$last_position = intval( $_REQUEST['last_position'] );
		
		$this->bigtabs->select_child( 'shop' );
		$this->menuleft->select_child( 'points_list' );
		$this->make_small_tabs();
		
		$this->method_name = 'point_edit';
		$xml = "<data>";
		
		$xml .= "<edit>";
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= "<last_position>$last_position</last_position>";
		$o = new ShopPoint( $_REQUEST['id'] );
		$xml .= $o->Xml();
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'point_edit', $xml );
	}
	
	function point_save()
	{
		$erx = "";
		
		$obj = new ShopPoint( $_REQUEST['object']['id'] );
		$last_position = intval( $_REQUEST['last_position'] );
		if( $last_position > 0 ) $_REQUEST['object']['position'] = $last_position + 1;
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->point_edit( 'true' );
		}
		else
		{
			$this->point_edit( 'false' );
		}
	}
	
	function point_del()
	{
		$o = new ShopPoint( $_REQUEST['id'] );
		$o->Delete();
		$this->points_list( 'scroll_to_list' );
	}
	
	function points_del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new ShopPoint( $id );
			$o->Delete();
		}
		$this->points_list( 'scroll_to_list' );
	}
	
	function point_clear()
	{
		global $db;

		$id = intval( $_REQUEST['id'] );
		$sql = "delete from shop_product_availability where point_id={$id}";
		$db->query( $sql );
		$this->point_edit( 'true' );
	}
	
	function point_clear_all()
	{
		global $db;

		$sql = "delete from shop_product_availability";
		$db->query( $sql );
		$this->points_list( 'scroll_to_list' );
	}
	
	function point_products()
	{
		global $db;
		
		if( $_REQUEST['action'] == 'save' )
		{
			$row = 1;
			$id = intval( $_REQUEST['cid'.$row] );
			$p = new ShopProduct(0,false);
			while( $id > 0 )
			{
				$number = $_REQUEST['number'.$row];
				$price = floatval( $_REQUEST['price'.$row] );
				$price_opt = floatval( $_REQUEST['price_opt'.$row] );
				$price_opt2 = floatval( $_REQUEST['price_opta'.$row] );
				
				$p->Save( Array( 'id'=>$id, 'price'=>$price, 'price_opt'=>$price_opt, 'price_opt2'=>$price_opt2, 'number'=>$number ) );
				
				$row++;
				$id = intval( $_REQUEST['cid'.$row] );
			}
		}
		
		$id = intval( $_REQUEST['id'] );
		
		$this->make_small_tabs();
		$this->make_menu();
		$this->bigtabs->select_child( 'shop' );
		$this->menuleft->select_child( 'points_list' );
		
		$this->method_name = 'list';
		$xml = "<data>";
		$xml .= "<products category_id='0' point_id='{$id}'>";
		
		$t = new MysqlTable( 'shop_products' );
		parent::define_pages( $db->getone( "select count( distinct p.id) from shop_products p left join shop_product_availability pa on pa.product_id=p.id where pa.point_id={$id}") );
		$sql = "select distinct p.id from shop_products p left join dictionary d on d.parent_id=p.id left join shop_product_availability pa on pa.product_id=p.id where d.folder='shop_products' and d.name='name' and pa.point_id={$id} order by d.value,p.brand_id,p.id limit ". ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page;
		$rs = $db->query( $sql );
		while( $a = $rs->fetch_row() )
		{
			$product = new ShopProduct( $a[0], false );
			$xml .= $product->Xml();
		}
		
		$xml .= "<last_position>0</last_position>";
		
		$xml .= "</products>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'point_products', $xml );
	}
	
	function point_swap_position()
	{
		$src = new ShopPoint( $_REQUEST['src'] );
		$dst = new ShopPoint( $_REQUEST['dst'] );
		$this->SwapPosition( $src, $dst, "shop_points", "status=1" );
		$this->points_list( 'scroll_to_list' );
	}
	// </Points>
	
	// <Settings>
	function settings_edit( $save_result="" )
	{
		$this->bigtabs->select_child( 'shop' );
		$this->menuleft->select_child( 'settings' );
		$this->make_small_tabs();
		
		$this->method_name = 'settings_edit';
		$xml = "<data>";
		
		$xml .= "<edit>";
		$xml .= "<save_result>$save_result</save_result>";
		$o = new ShopSettings();
		$xml .= $o->Xml();
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'settings_edit', $xml );
	}
	
	function settings_save()
	{
		$erx = "";
		
		$obj = new ShopSettings();
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->settings_edit( 'true' );
		}
		else
		{
			$this->settings_edit( 'false' );
		}
	}
	// </Settings>
	
	// <Prices>
	function prices_list( $save_result="" )
	{
		$this->bigtabs->select_child( 'shop' );
		$this->menuleft->select_child( 'prices' );
		$this->make_small_tabs();
		
		$this->method_name = 'prices_list';
		$xml = "<data>";
		
		$xml .= "<edit>";
		$xml .= "<save_result>$save_result</save_result>";
		$o = new ShopSettings();
		$xml .= $o->Xml();
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'prices_list', $xml );
	}
	
	function prices_save()
	{
		$erx = "";
		
		$obj = new ShopSettings();
		
		$attachment = new Attachment();
		if( $attachment->upload('file_price') )
		{
			$obj->DeleteFilePrice();
			$_REQUEST['object']['file_price'] = $attachment->filename;
		}
		
		$attachment = new Attachment();
		if( $attachment->upload('file_price_opt') )
		{
			$obj->DeleteFilePriceOpt();
			$_REQUEST['object']['file_price_opt'] = $attachment->filename;
		}
		
		$attachment = new Attachment();
		if( $attachment->upload('file_price_opt2') )
		{
			$obj->DeleteFilePriceOpt2();
			$_REQUEST['object']['file_price_opt2'] = $attachment->filename;
		}
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->prices_list( 'true' );
		}
		else
		{
			$this->prices_list( 'false' );
		}
	}
	
	function price_del_file_price()
	{
		$obj = new ShopSettings();
		$obj->DeleteFilePrice();
		$this->prices_list( 'true' );
	}
	
	function price_del_file_price_opt()
	{
		$obj = new ShopSettings();
		$obj->DeleteFilePriceOpt();
		$this->prices_list( 'true' );
	}
	
	function price_del_file_price_opt2()
	{
		$obj = new ShopSettings();
		$obj->DeleteFilePriceOpt2();
		$this->prices_list( 'true' );
	}
	// </Prices>
	
	// <Features>
	function category_features( $save_result="" )
	{
		$category_id = intval( $_REQUEST['category_id'] );
		$feature_id = intval( $_REQUEST['feature_id'] );
		$category = new ShopCategory( $category_id );
		
		$this->make_small_tabs( $category_id, $category );
		$this->make_menu();
		$this->bigtabs->select_child( 'shop' );
		$this->smalltabs->select_child( 'features' );
		$this->menuleft->select_child( "submenu{$category_id}" );
		$this->menuleft->select_child( "c{$category_id}" );
		
		$this->method_name = 'category_features';
		$xml = "<data>";
		
		$xml .= "<edit>";
		$xml .= "<save_result>$save_result</save_result>";
		$o = new ShopCategory( $_REQUEST['category_id'] );
		$xml .= $o->Xml();
		$xml .= $this->FeaturesXml( $category_id );
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'category_features', $xml );
	}
	
	function feature_add()
	{
		$o = new ShopFeature();
		$_REQUEST['object']['folder'] = $this->folder;
		$o->Save( $_REQUEST['object'] );
		$this->category_features();
	}
	
	function feature_save()
	{
		$o = new ShopFeature();
		$_REQUEST['object']['show_in_short_description'] = intval($_REQUEST['object']['show_in_short_description']);
		$_REQUEST['object']['match_analog'] = intval($_REQUEST['object']['match_analog']);
		$_REQUEST['object']['show_in_filter'] = intval($_REQUEST['object']['show_in_filter']);
		$_REQUEST['object']['is_numeric'] = intval($_REQUEST['object']['is_numeric']);
		$o->Save( $_REQUEST['object'] );
		$this->category_features();
	}
	
	function feature_del()
	{
		$o = new ShopFeature( $_REQUEST['id'] );
		$o->Delete();
		$this->category_features();
	}
	
	function FeaturesXml( $category_id )
	{
		$xml = "";
		$t = new MysqlTable('shop_features');
		$t->find_all( "category_id={$category_id} and status=1", "position,id" );
		$xml .= "<features>";
		foreach( $t->data as $row )
		{
			$o = new ShopFeature( $row['id'] );
			$xml .= $o->Xml();
		}
		$xml .= "</features>";
		
		return( $xml );
	}
	
	function FeaturesXmlForProductEdit( $category_id )
	{
		global $db;
		
		$category = new ShopCategory( $category_id );
		$parent_category_id = $category->parent_id;
		$root_category_id = $db->getone( "select shop_product_root_category({$category_id})" );
		
		$xml = "";
		$t = new MysqlTable('shop_features');
		
		$t->find_all( "category_id={$category_id} and status=1", "position,id,match_analog" );
		if( !count($t->data) ) {
			$t->find_all( "category_id={$parent_category_id} and status=1", "position,id,match_analog" );
		}
		if( !count($t->data) ) {
			$t->find_all( "category_id={$root_category_id} and status=1", "position,id,match_analog" );
		}
		
		$xml = "";
		$xml .= "<features>";
		foreach( $t->data as $row )
		{
			$o = new ShopFeature( $row['id'] );
			$xml .= $o->Xml();
		}
		$xml .= "</features>";
		
		return( $xml );
	}
	
	function FeaturesJS( $category_id )
	{
		global $db;
		
		$category = new ShopCategory( $category_id );
		$parent_category_id = $category->parent_id;
		$root_category_id = $db->getone( "select shop_product_root_category({$category_id})" );
		
		$xml = "";
		$t = new MysqlTable('shop_features');
		
		$t->find_all( "category_id={$category_id} and status=1", "position,match_analog,id" );
		if( !count($t->data) ) {
			$t->find_all( "category_id={$parent_category_id} and status=1", "position,match_analog,id" );
		}
		if( !count($t->data) ) {
			$t->find_all( "category_id={$root_category_id} and status=1", "position,match_analog,id" );
		}
		$xml .= "<featuresJS><![CDATA[";
		$delim = '';
		foreach( $t->data as $row )
		{
			$o = new ShopFeature( $row['id'] );
			$xml .= $delim . $o->id;
			$delim2 = '';
			if( count($o->values) > 0 )
			{
				if( $o->match_analog == 1 ) $xml .= ":['{$o->name} ***',[";
				else $xml .= ":['{$o->name}',[";
				foreach( $o->values as $v )
				{
					$xml .= $delim2 ."['{$v->name}',". $v->id ."]";
					$delim2 = ',';
				}
				$xml .= ']]';
			}
			else
			{
				$xml .= ":['{$o->name}',['']]";
			}
			$delim = ',';
		}
		$xml .= "]]></featuresJS>";
		//echo $xml;
		
		return( $xml );
	}
	// </Features>
	
	// <Feature values>
	function feature_values( $save_result="" )
	{
		$category_id = intval( $_REQUEST['category_id'] );
		$feature_id = intval( $_REQUEST['id'] );
		$category = new ShopCategory( $category_id );
		
		$this->make_small_tabs( $category_id, $category );
		$this->make_menu();
		$this->bigtabs->select_child( 'shop' );
		$this->smalltabs->select_child( 'features' );
		$this->menuleft->select_child( "submenu{$category_id}" );
		$this->menuleft->select_child( "c{$category_id}" );
		
		$this->method_name = 'feature_values';
		$xml = "<data>";
		
		$xml .= "<edit>";
		$xml .= "<save_result>$save_result</save_result>";
		$o = new ShopFeature( $feature_id );
		$xml .= $o->Xml();
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'feature_values', $xml );
	}
	
	function feature_value_add()
	{
		$o = new ShopFeatureValue();
		$_REQUEST['object']['folder'] = $this->folder;
		$o->Save( $_REQUEST['object'] );
		$_REQUEST['id'] = $o->feature_id;
		$this->feature_values();
	}
	
	function feature_value_save()
	{
		$o = new ShopFeatureValue();
		$o->Save( $_REQUEST['object'] );
		$_REQUEST['id'] = $o->feature_id;
		$this->feature_values();
	}
	
	function feature_value_del()
	{
		$o = new ShopFeatureValue( $_REQUEST['id'] );
		$_REQUEST['id'] = $o->feature_id;
		$o->Delete();
		$this->feature_values();
	}
	
	function product_feature_add()
	{
		global $db;
		$retval = 'false';
		
		$id = intval( $_REQUEST['id'] );
		$feature_value_id = intval( $_REQUEST['feature_value_id'] );
		$feature_id = $db->getone( "select f.id from shop_features f left join shop_feature_values fv on fv.feature_id=f.id where fv.id=". $feature_value_id );
		if( !$db->getone("select count(pf.id) from shop_product_features pf left join shop_feature_values fv on fv.id=pf.feature_value_id left join shop_features f on f.id=fv.feature_id where pf.product_id={$id} and f.id={$feature_id}") )
		{
			$db->query( "insert into shop_product_features (product_id,feature_value_id) values({$id},{$feature_value_id})" );
			$retval = 'true';
		}
		$this->edit( $retval, 'features' );
	}
	
	function product_feature_del()
	{
		global $db;

		$id = intval( $_REQUEST['id'] );
		$feature_value_id = intval( $_REQUEST['feature_value_id'] );
		$db->query( "delete from shop_product_features where product_id={$id} and feature_value_id={$feature_value_id}" );
		$this->edit( 'true', 'features' );
	}
	// </Feature values>
	
	//Tools
	function get_products()
	{
		global $db;

		$html = "<table>";
		$sql = "select p.id,p.number,d.value from shop_products p left join dictionary d on d.parent_id=p.id where p.status=1 and d.folder='shop_products' and d.lang='ru' and d.name='name' order by d.value";
		$rs = $db->query( $sql );
		while( $row = $rs->fetch_row() )
		{
			$html .= sprintf( "<tr><td>%s</td><td>%s</td></tr>", $row[2], $row[1] );
		}
		$html .= "</table>";
		
		echo '<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>'. $html;
	}
	//Tools
}

?>
<?php
/*****************************************************
 Class v.1.0, 02.2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

include_once( 'base_controller.php' );

class fair_controller extends base_controller
{
    var $folder = 'fair';
    
    function fair_controller()
    {
        parent::base_controller();
        
        $this->make_small_tabs( $this->find_first_category() );
        $this->make_menu();
    }
    
    function make_small_tabs( $category_id=0, $category=null )
    {
        $this->smalltabs->del_all();
        if( $category_id )
        {
            $this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/fair/smalltabs/list'), "?".$this->controller_name.".index&category_id={$category_id}&page=". intval($_REQUEST['page'])) );
            $this->smalltabs->add_item( new WETab('edit', $this->dictionary->get('/locale/fair/smalltabs/edit'), "?".$this->controller_name.".category_edit&category_id={$category_id}") );
        }
    }
    
    function make_menu()
    {
        $this->menuleft->del_all();
        
        $this->menuleft->add_item( new WEMenuItem( "new_list", $this->dictionary->get('/locale/fair/menuleft/new_list'), "?".$this->controller_name.".new_list" ) );
        $this->menuleft->add_item( new WEMenuItem( "wc_list", $this->dictionary->get('/locale/fair/menuleft/wc_list'), "?".$this->controller_name.".wc_list" ) );
        $this->menuleft->add_item( new WEMenuSeparator() );
        
        $t = new MysqlTable( 'fair_categories' );
        $t->find_all( "parent_id is null", "position,id" );
        foreach( $t->data as $row )
        {
            $o = new FairCategory( $row['id'] );
            $submenu = new WEMenu( "submenu{$o->id}", $o->name, "?".$this->controller_name.".index&category_id={$o->id}" );
            $this->menuleft->add_item( $submenu );
            $t2 = new MysqlTable( 'fair_categories' );
            $t2->find_all( "parent_id={$o->id}", "position,id" );
            foreach( $t2->data as $row2 )
            {
                $cs = new FairCategory( $row2['id'] );
                $submenu2 = new WEMenu( "submenu{$cs->id}", $cs->name, "?".$this->controller_name.".index&category_id={$cs->id}" );
                $submenu->add_item( $submenu2 );
                $t3 = new MysqlTable( 'fair_categories' );
                $t3->find_all( "parent_id={$cs->id}", "position,id" );
                foreach( $t3->data as $row3 )
                {
                    $cs2 = new FairCategory( $row3['id'] );
                    $submenu2->add_item( new WEMenuItem( "c{$cs2->id}", $cs2->name, "?".$this->controller_name.".index&category_id={$cs2->id}" ) );
                }
            }
        }
        $this->menuleft->add_item( new WEMenuSeparator() );
        $this->menuleft->add_item( new WEMenuItem( "articles", $this->dictionary->get('/locale/shop/menuleft/articles'), "?fair_articles.index" ) );
        $this->menuleft->add_item( new WEMenuSeparator() );
        $this->menuleft->add_item( new WEMenuItem( "paymentways_list", $this->dictionary->get('/locale/fair/menuleft/paymentways_list'), "?".$this->controller_name.".paymentways_list" ) );
        $this->menuleft->add_item( new WEMenuItem( "settings", $this->dictionary->get('/locale/fair/menuleft/settings'), "?".$this->controller_name.".settings_edit" ) );
    }
    
    function make_elements()
    {
        $retval = "";
        
        $retval .= $this->bigtabs->xml();
        $retval .= $this->smalltabs->xml();
        $retval .= $this->menuleft->xml();
        
        $retval .= "<left_panel show='yes'><categories>";
        $t = new MysqlTable( 'fair_categories' );
        $t->find_all( "parent_id is null", "position,id" );
        foreach( $t->data as $row )
        {
            $o = new FairCategory( $row['id'] );
            $retval .= $o->Xml();
        }
        $retval .= "</categories></left_panel>";
        
        return( $retval );
    }
    
    function index( $parameters='' )
    {
        $category_id = intval( $_REQUEST['category_id'] );
        if( $category_id==0 ) $category_id = $this->find_first_category();
        $category = new FairCategory( $category_id );
        
        $this->make_small_tabs( $category_id, $category );
        $this->make_menu();
        $this->bigtabs->select_child( 'fair' );
        $this->smalltabs->select_child( 'list' );
        $this->menuleft->select_child( "submenu{$category_id}" );
        $this->menuleft->select_child( "c{$category_id}" );
        
        $this->method_name = 'list';
        $xml = "<data>";
        $xml .= "<fair_objects category_id='{$category_id}'>";
        
        $t = new MysqlTable( 'fair_objects' );
        parent::define_pages( $t->get_count("category_id1={$category_id} and status=1") );
        $t->find_all( "category_id1={$category_id} and status=1", 'id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
        foreach( $t->data as $a )
        {
            $o = new FairObject( $a['id'], false );
            $xml .= $o->Xml();
        }
        
        $xml .= "</fair_objects>";
        $xml .= "<parameters>$parameters</parameters>";
        $xml .= "</data>";
        $this->display( 'list', $xml );
    }    
    
    function new_list( $parameters='' )
    {
        $category_id = intval( $_REQUEST['category_id'] );
        if( $category_id==0 ) $category_id = $this->find_first_category();
        $category = new FairCategory( $category_id );
        
        $this->make_small_tabs( $category_id, $category );
        $this->make_menu();
        $this->bigtabs->select_child( 'fair' );
        $this->smalltabs->del_all();
        $this->menuleft->select_child( "new_list" );
        
        $this->method_name = 'list';
        $xml = "<data>";
        $xml .= "<fair_objects category_id='{$category_id}'>";
        
        $t = new MysqlTable( 'fair_objects' );
        parent::define_pages( $t->get_count("status=0") );
        $t->find_all( "status=0", 'id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
        foreach( $t->data as $a )
        {
            $o = new FairObject( $a['id'], false );
            $xml .= $o->Xml();
        }
        
        $xml .= "</fair_objects>";
        $xml .= "<parameters>$parameters</parameters>";
        $xml .= "</data>";
        $this->display( 'new_list', $xml );
    }
    
    function wc_list( $parameters='' )
    {
        $category_id = intval( $_REQUEST['category_id'] );
        if( $category_id==0 ) $category_id = $this->find_first_category();
        $category = new FairCategory( $category_id );
        
        $this->make_small_tabs( $category_id, $category );
        $this->make_menu();
        $this->bigtabs->select_child( 'fair' );
        $this->smalltabs->del_all();
        $this->menuleft->select_child( "wc_list" );
        
        $this->method_name = 'list';
        $xml = "<data>";
        $xml .= "<fair_objects category_id='{$category_id}'>";
        
        $t = new MysqlTable( 'fair_objects' );
        parent::define_pages( $t->get_count("status=1 and (category_id1 is null or category_id1=0)") );
        $t->find_all( "status=1 and (category_id1 is null or category_id1=0)", 'id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
        foreach( $t->data as $a )
        {
            $o = new FairObject( $a['id'], false );
            $xml .= $o->Xml();
        }
        
        $xml .= "</fair_objects>";
        $xml .= "<parameters>$parameters</parameters>";
        $xml .= "</data>";
        $this->display( 'wc_list', $xml );
    }
    
    function set_status()
    {
        $id = intval( $_REQUEST['id'] );
        $v = intval( $_REQUEST['v'] );
        $o = new FairObject( $id );
        $status = $o->status;
        if( $o->IsLoaded() )
        {
            $o->status = $v;
            $o->Save();
        }
        
        if( $status == 0 )
        {
            $this->new_list();
        }
        else
        {
            $this->index();
        }
    }
    
    function find_first_category()
    {
        $retval = 0;
        
        $t = new MysqlTable( 'fair_categories' );
        if( $row = $t->find_first("","position,id") )
        {
            $retval = intval( $row['id'] );
        }
        
        return( $retval );
    }
    
    function category_add()
    {
        $o = new FairCategory();
        $o->folder = $this->folder;
        $o->name = $_REQUEST['name'];
        $o->Save();
        $this->index();
    }
    
    function category_del()
    {
        $o = new FairCategory( $_REQUEST['category_id'] );
        $o->Delete();
        $_REQUEST['category_id'] = $this->find_first_category();
        $this->index();
    }
    
    function category_edit( $save_result="" )
    {
        $category_id = intval( $_REQUEST['category_id'] );
        if( $category_id==0 ) $category_id = $this->find_first_category();
        $category = new FairCategory( $category_id );
        
        $this->make_small_tabs( $category_id, $category );
        $this->make_menu();
        $this->bigtabs->select_child( 'fair' );
        $this->smalltabs->select_child( 'edit' );
        $this->menuleft->select_child( "submenu{$category_id}" );
        $this->menuleft->select_child( "c{$category_id}" );
        
        $this->method_name = 'category_edit';
        $xml = "<data>";
        
        $xml .= "<edit>";
        $xml .= "<save_result>$save_result</save_result>";
        $o = new FairCategory( $_REQUEST['category_id'] );
        $xml .= $o->Xml();
        $xml .= $this->SubcategoriesXml( $category_id );
        $xml .= $this->CategoriesXml();
        $xml .= "</edit>";
        
        $xml .= "</data>";
        
        $this->display( 'category_edit', $xml );
    }
    
    function category_save()
    {
        $erx = "";
        
        $obj = new FairCategory( $_REQUEST['object']['id'] );
        
        if( $obj->Save( $_REQUEST['object'] ) )
        {
            $_REQUEST['id'] = $obj->id;
            $this->category_edit( 'true' );
        }
        else
        {
            $this->category_edit( 'false' );
        }
    }
    
    function CategoriesXml()
    {
        $xml = "<categories>";
        $t = new MysqlTable('fair_categories');
        $t->find_all( "parent_id is null", "position,id" );
        
        foreach( $t->data as $row )
        {
            $parent_id = $row['id']; $level = 1;
            $o = new FairCategory( $parent_id );
            $xml .= "<category id='{$o->id}' name='{$o->name}'>";
            $xml .= $this->SubcategoriesXmlRC( $xml, $parent_id, $level );
            $xml .= "</category>";
        }
        
        $xml .= "</categories>";
        
        return( $xml );
    }
    
    function SubcategoriesXml( $root_id )
    {
        $xml = "";
        $root = new FairCategory( $root_id );
        if( $root->IsLoaded() )
        {
            $xml .= "<subcategories>";
            $xml .= "<category id='$root_id' name='{$root->name}'>";
            $parent_id = $root_id; $level = 1;
            $xml .= $this->SubcategoriesXmlRC( $xml, $parent_id, 1 );
            $xml .= "</category>";
            $xml .= "</subcategories>";
        }
        
        return( $xml );
    }
    
    function SubcategoriesXmlRC( &$xml, $parent_id, $level )
    {
        global $db;

        $rs = $db->query( "select id,parent_id from fair_categories where parent_id={$parent_id} order by position" );
        while( $row = $rs->fetch_row() )
        {
            $o = new FairCategory( $row[0] );
            $xml .= "<category id='{$o->id}' name='{$o->name}' level='{$level}' prefix='". str_repeat("&#160;&#160;",$level) ."' parent_id='{$o->parent_id}'>";
            $xml .= $this->SubcategoriesXmlRC( $xml, $o->id, $level+1 );
            $xml .= "</category>";
        }
    }
    
    function subcategory_add()
    {
        $o = new FairCategory();
        $_REQUEST['object']['folder'] = $this->folder;
        $o->Save( $_REQUEST['object'] );
        $this->category_edit();
    }
    
    function subcategory_save()
    {
        $o = new FairCategory();
        $o->Save( $_REQUEST['object'] );
        $this->category_edit();
    }
    
    function subcategory_del()
    {
        $o = new FairCategory( $_REQUEST['id'] );
        $o->Delete();
        $this->category_edit();
    }
    
    function edit( $save_result="", $scroll="" )
    {
        global $db;
        
        $this->bigtabs->select_child( 'fair' );
        $this->smalltabs->select_child( 'list' );
        $this->page = intval( $_REQUEST['page'] );
        
        $this->method_name = 'edit';
        $xml = "<data>";
        
        $xml .= "<fair_edit>";
        $xml .= "<save_result>$save_result</save_result>";
        $xml .= "<last_position>$last_position</last_position>";
        $o = new FairObject( $_REQUEST['id'] );
        
        $category_id = intval( $o->category_id1 );
        $category = new FairCategory( $category_id );
        if( $o->status == 0 )
        {
            $this->smalltabs->del_all();
            $this->menuleft->select_child( "new_list" );
        }
        else
        {
            $this->menuleft->select_child( "submenu{$category_id}" );
            $this->menuleft->select_child( "c{$category_id}" );
            $this->make_small_tabs( $category_id, $category );
        }
        
        if( !$o->IsLoaded() )
        {
            $o->category_id = $category_id;
            $o->folder = $this->folder;
        }
        $xml .= $o->Xml();
        
        $xml .= $this->CategoriesXml();
        $xml .= "<categories_line>". $db->getone("select fair_categories_line({$o->category_id1})") ."</categories_line>";
        $xml .= "<categories_js><![CDATA[". $this->GetCategoriesJS() ."]]></categories_js>";
        $xml .= "<scroll>{$scroll}</scroll>";
        
        $xml .= "</fair_edit>";
        
        $xml .= "</data>";
        
        $this->display( 'edit', $xml );
    }
    
    function edit_marked( $save_result="", $scroll="" )
    {
        global $db;
        
        $this->bigtabs->select_child( 'fair' );
        $this->smalltabs->select_child( 'list' );
        $this->page = intval( $_REQUEST['page'] );
        $ids = $_REQUEST['ids'];
        $ids_a = split( ',', $ids );
        
        $this->method_name = 'edit';
        $xml = "<data>";
        
        $xml .= "<fair_edit>";
        $xml .= "<save_result>$save_result</save_result>";
        $xml .= "<ids count='". count($ids_a) ."'>$ids</ids>";
        $o = new FairObject( $ids_a[0] );
        if( $ids_a[1] > 0 ) $o2 = new FairObject( $ids_a[1] );
        if( $ids_a[2] > 0 ) $o3 = new FairObject( $ids_a[2] );
        if( $ids_a[3] > 0 ) $o4 = new FairObject( $ids_a[3] );
        if( $ids_a[4] > 0 ) $o5 = new FairObject( $ids_a[4] );
        
        $this->smalltabs->del_all();
        $this->menuleft->select_child( "wc_list" );
        
        $xml .= $o->Xml();
        if( $ids_a[1] > 0 ) $xml .= $o2->Xml();
        if( $ids_a[2] > 0 ) $xml .= $o3->Xml();
        if( $ids_a[3] > 0 ) $xml .= $o4->Xml();
        if( $ids_a[4] > 0 ) $xml .= $o5->Xml();
        
        $xml .= $this->CategoriesXml();
        $xml .= "<categories_line>". $db->getone("select fair_categories_line({$o->category_id1})") ."</categories_line>";
        $xml .= "<categories_js><![CDATA[". $this->GetCategoriesJS() ."]]></categories_js>";
        $xml .= "<scroll>{$scroll}</scroll>";
        
        $xml .= "</fair_edit>";
        
        $xml .= "</data>";
        
        $this->display( 'edit_marked', $xml );
    }
    
    function GetCategoriesJS()
    {
        global $db;

        $delim = "";
        $retval = "[";
        $rs = $db->query( "select id from fair_categories where parent_id is null or parent_id=0 order by position,id" );
        while( $row = $rs->fetch_row() )
        {
            $o = new FairCategory( $row[0] );
            $retval .= $delim . "[{$o->id},'{$o->name}',[". $this->GetSubcategoriesJS($row[0]) ."]]";
            $delim = ",";
        }
        $retval .= "]";
        
        return( $retval );
    }
    
    function GetSubcategoriesJS( $parent_id )
    {
        global $db;

        $retval = "";
        
        $delim = "";
        $rs = $db->query( "select id from fair_categories where parent_id={$parent_id} order by position,id" );
        while( $row = $rs->fetch_row() )
        {
            $o = new FairCategory( $row[0] );
            $retval .= $delim . "[{$o->id},'{$o->name}',[". $this->GetSubcategoriesJS($row[0]) ."]]";
            $delim = ",";
        }
        
        return( $retval );
    }
    
    function change_category()
    {
        $o = new FairObject( $_REQUEST['id'] );
        $category_id = intval( $_REQUEST['category_id'] );
        if( $o->IsLoaded() && $category_id>0 )
        {
            $o->category_id = $category_id;
            $o->Save();
        }
        $this->edit();
    }
    
    function save()
    {
        $erx = "";
        
        $id = intval( $_REQUEST['id'] );
        $category_id1 = intval( $_REQUEST['category_id1'] );
        $category_id2 = intval( $_REQUEST['category_id2'] );
        $category_id3 = intval( $_REQUEST['category_id3'] );
        
        if( $category_id3 > 0 ) $_REQUEST['object']['category_id1'] = $category_id3;
        else if( $category_id2 > 0 ) $_REQUEST['object']['category_id1'] = $category_id2;
        else $_REQUEST['object']['category_id1'] = $category_id1;
        
        $o = new FairObject( $id );
        
        if( intval($_REQUEST['object']['category_id1'])>0 && trim($_REQUEST['object']['title'])!='' && trim($_REQUEST['object']['description'])!='' && trim($_REQUEST['object']['keywords'])!='' && $o->Save( $_REQUEST['object'] ) )
        {
            $_REQUEST['id'] = $o->id;
            $this->edit( 'true' );
        }
        else
        {
            $this->edit( 'false' );
        }
    }
    
    function save_marked()
    {
        $erx = "";
        
        $id = intval( $_REQUEST['id'] );
        $category_id1 = intval( $_REQUEST['category_id1'] );
        $category_id2 = intval( $_REQUEST['category_id2'] );
        $category_id3 = intval( $_REQUEST['category_id3'] );
        
        $ids = $_REQUEST['ids'];
        $ids_a = split( ',', $ids );
        
        if( $category_id3 > 0 ) $_REQUEST['object']['category_id1'] = $category_id3;
        else if( $category_id2 > 0 ) $_REQUEST['object']['category_id1'] = $category_id2;
        else $_REQUEST['object']['category_id1'] = $category_id1;
        
        if( intval($_REQUEST['object']['category_id1'])>0 && trim($_REQUEST['object']['title'])!='' && trim($_REQUEST['object']['description'])!='' && trim($_REQUEST['object']['keywords'])!='' )
        {
            foreach( $ids_a as $o_id )
            {
                $o = new FairObject();
                $_REQUEST['object']['id'] = $o_id;
                $o->Save( $_REQUEST['object'] );
            }
        }
        else
        {
            $this->edit_marked( 'false' );
        }
        
        $this->wc_list();
    }
    
    function del()
    {
        $this->delete();
    }
    
    function delete()
    {
        $o = new FairObject( $_REQUEST['id'] );
        $o->Delete();
        if( $o->status == 0 ) $this->new_list();
        else $this->index();

    }
    
    function del_marked()
    {
        $ids_str = $_REQUEST['ids'];
        $ids = split( ',', $ids_str );
        foreach( $ids as $id )
        {
            $o = new FairObject( $id );
            $status = $o->status;
            $o->Delete();
        }
        
        if( $status == 0 )
        {
            $this->new_list();
        }
        else
        {
            $this->index();
        }
    }
    
    function accept_marked()
    {
        $ids_str = $_REQUEST['ids'];
        $ids = split( ',', $ids_str );
        foreach( $ids as $id )
        {
            $o = new FairObject( $id );
            $o->status = 1;
            $o->Save();
        }
        $this->new_list();
    }
    
    function del_image()
    {
        $o = new FairObject( $_REQUEST['id'] );
        $o->DeleteImage();
        $this->edit( 'true' );
    }
    
    function del_media()
    {
        $image = new Image( $_REQUEST['image_id'] );
        $image->Delete();
        $this->edit( 'true' );
    }
    
    // <Settings>
    function settings_edit( $save_result="" )
    {
        $this->bigtabs->select_child( 'fair' );
        $this->menuleft->select_child( 'settings' );
        $this->make_small_tabs();
        
        $this->method_name = 'settings_edit';
        $xml = "<data>";
        
        $xml .= "<edit>";
        $xml .= "<save_result>$save_result</save_result>";
        $o = new FairSettings();
        $xml .= $o->Xml();
        $xml .= "</edit>";
        
        $xml .= "</data>";
        
        $this->display( 'settings_edit', $xml );
    }
    
    function settings_save()
    {
        $erx = "";
        
        $obj = new FairSettings();
        
        if( $obj->Save( $_REQUEST['object'] ) )
        {
            $_REQUEST['id'] = $obj->id;
            $this->settings_edit( 'true' );
        }
        else
        {
            $this->settings_edit( 'false' );
        }
    }
    // </Settings>
    
    // <Paymentways>
    function paymentways_list( $parameters='' )
    {
        $this->bigtabs->select_child( 'fair' );
        $this->menuleft->select_child( 'paymentways_list' );
        $this->make_small_tabs();
        
        $this->method_name = 'paymentways_list';
        $xml = "<data>";
        $xml .= "<list>";
        
        $t = new MysqlTable( 'fair_paymentways' );
        parent::define_pages( $t->get_count('status=1') );
        $t->find_all( 'status=1', 'position,id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
        foreach( $t->data as $a )
        {
            $o = new FairPaymentway( $a['id'] );
            
            if( $o->position == 0 )
            {
                $tp = new MysqlTable('fair_paymentways');
                $tp->fix_positions( '', "status=1" );
                $o->Load( $a['id'] );
            }
            
            $xml .= $o->Xml();
        }
        
        $xml .= "<last_position>{$o->position}</last_position>";
        
        $xml .= "</list>";
        $xml .= "<parameters>$parameters</parameters>";
        $xml .= "</data>";
        $this->display( 'paymentways_list', $xml );
    }
    
    function paymentway_edit( $save_result="" )
    {
        $last_position = intval( $_REQUEST['last_position'] );
        
        $this->bigtabs->select_child( 'fair' );
        $this->menuleft->select_child( 'paymentways_list' );
        
        $this->method_name = 'paymentway_edit';
        $xml = "<data>";
        
        $xml .= "<edit>";
        $xml .= "<save_result>$save_result</save_result>";
        $xml .= "<last_position>$last_position</last_position>";
        $o = new FairPaymentway( $_REQUEST['id'] );
        $xml .= $o->Xml();
        $xml .= "</edit>";
        
        $xml .= "</data>";
        
        $this->display( 'paymentway_edit', $xml );
    }
    
    function paymentway_save()
    {
        $erx = "";
        
        $obj = new FairPaymentway( $_REQUEST['object']['id'] );
        $last_position = intval( $_REQUEST['last_position'] );
        if( $last_position > 0 ) $_REQUEST['object']['position'] = $last_position + 1;
        
        if( $obj->Save( $_REQUEST['object'] ) )
        {
            $_REQUEST['id'] = $obj->id;
            $this->paymentway_edit( 'true' );
        }
        else
        {
            $this->paymentway_edit( 'false' );
        }
    }
    
    function paymentway_del()
    {
        $o = new FairPaymentway( $_REQUEST['id'] );
        $o->Delete();
        $this->paymentways_list( 'scroll_to_list' );
    }
    
    function paymentways_del_marked()
    {
        $ids_str = $_REQUEST['ids'];
        $ids = split( ',', $ids_str );
        foreach( $ids as $id )
        {
            $o = new FairPaymentway( $id );
            $o->Delete();
        }
        $this->paymentways_list( 'scroll_to_list' );
    }
    
    function paymentway_swap_position()
    {
        $src = new FairPaymentway( $_REQUEST['src'] );
        $dst = new FairPaymentway( $_REQUEST['dst'] );
        $this->SwapPosition( $src, $dst, "fair_paymentways", "status=1" );
        $this->paymentways_list( 'scroll_to_list' );
    }
    // </Paymentways>
    
    function display_photo()
    {
        $f = SITEROOT.'/empty.gif';
        $type = intval( $_REQUEST['type'] ); /* 0/1 == thumbnail/preview */
        $id = intval( $_REQUEST['id'] );
        $o = new FairObject( $id );
        if( $o->IsLoaded() )
        {
            switch( $type )
            {
                case 1:
                    $ff = SITEROOT.'/'.$_ENV['fair_path'].'/'.$o->preview;
                    break;
                default:
                    $ff = SITEROOT.'/'.$_ENV['fair_path'].'/'.$o->thumbnail;
                    break;
            }
            if( file_exists($ff) )
            {
                $f = $ff;
            }
        }
        
        header( "Content-type: image/gif" );
        readfile( $f );
    }
    
    function search( $parameters='' )
    {
        global $db;
        
        $category_id = intval( $_REQUEST['category_id'] );
        if( $category_id==0 ) $category_id = $this->find_first_category();
        $category = new FairCategory( $category_id );
        
        $this->make_small_tabs( $category_id, $category );
        $this->make_menu();
        $this->bigtabs->select_child( 'fair' );
        
        $this->method_name = 'search';
        $xml = "<data>";
        $xml .= "<fair_objects category_id='{$category_id}'>";
        
        $str = trim( $_REQUEST['name'] );
        
        $t = new MysqlTable( 'fair_objects' );
        $this->define_pages( $db->getone("select count(distinct p.id) from fair_objects p left join dictionary d on d.parent_id=p.id left join clients c on c.id=p.client_id where p.status=1 and d.folder='fair_objects' and d.name='title' and (d.value like '%{$str}%' or p.keywords like '%{$str}%' or p.id='{$str}' or c.firstname like '%{$str}%' or c.lastname like '%{$str}%') order by p.position,p.id") );
        $sql = "select distinct p.id from fair_objects p left join dictionary d on d.parent_id=p.id left join clients c on c.id=p.client_id where p.status=1 and d.folder='fair_objects' and d.name='title' and (d.value like '%{$str}%' or p.keywords like '%{$str}%' or p.id='{$str}' or c.firstname like '%{$str}%' or c.lastname like '%{$str}%') order by p.position,p.id limit ". ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page;
        $rs = $db->query( $sql );
        while( $row = $rs->fetch_row() )
        {
            $product = new FairObject( $row[0], false );
            $xml .= $product->Xml();
        }
        
        $xml .= "</fair_objects>";
        $xml .= "<parameters>$parameters</parameters>";
        $xml .= "</data>";
        $this->display( 'search', $xml );
    }
    
    function exif_process_all()
    {
        $path = SITEROOT .'/'. $_ENV['fair_path'] .'/';
        $t = new MysqlTable("fair_objects");
        $t->find_all("status=1");
        $exif = new AExif();
        
        echo "<table border='1'>";
        echo "<tr><td>ID</td><td>filename</td><td>was_used_software</td><td>software</td><td>photo_model</td><td>photo_resolution</td><td>photo_iso</td><td>photo_datetime_original</td><td>photo_datetime_digitized</td></tr>";
        
        foreach( $t->data as $row )
        {
            $exif->SetFilename( $path . $row["filename"] );
            $o  = new FairObject( $row["id"] );
            if( $o->IsLoaded() )
            {
                if( $exif->error == "" )
                {
                    $o->software = $exif->parsed["software"];
                    $o->photo_make = $exif->parsed["make"];
                    $o->photo_model = $exif->parsed["model"];
                    $o->photo_resolution = $exif->parsed["resolution_x"];
                    $o->photo_iso = $exif->parsed["iso"];
                    $o->photo_datetime_original = $exif->parsed["datetime_original"];
                    $o->photo_datetime_digitized = $exif->parsed["datetime_digitized"];
                    if( $o->software != "" ) $o->was_used_software = 1;
                    else $o->was_used_software = 0;
                    $o->Save();
                    
                    echo "<tr><td>{$row["id"]}</td><td>{$row["filename"]}</td><td>{$o->was_used_software}</td><td>{$o->software}</td><td>{$o->photo_make},{$o->photo_model}</td><td>{$o->photo_resolution}</td><td>{$o->photo_iso}</td><td>{$o->photo_datetime_original}</td><td>{$o->photo_datetime_digitized}</td></tr>";
                }
            }
        }
        echo "</table>";
    }
}

?>
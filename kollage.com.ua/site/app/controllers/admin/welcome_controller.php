<?php
/*****************************************************
 Class v.1.0, 02.2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

include_once( 'base_controller.php' );

class welcome_controller extends base_controller
{
    function show()
    {
        $this->method_name = 'show';
        $this->display( 'welcome', '<data></data>' );
    }
}

?>
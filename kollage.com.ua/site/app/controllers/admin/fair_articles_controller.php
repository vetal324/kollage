<?php
/*****************************************************
 Class v.1.0, 02.2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

include_once( 'base_controller.php' );

class fair_articles_controller extends base_controller
{
    var $folder = 'fair';
    var $category_id = 0;
    
    function fair_articles_controller()
    {
        parent::base_controller();
        
        $this->bigtabs->select_child( 'fair' );
        
        $this->make_menu();
        
        $t = new MysqlTable('categories');
        if( $row = $t->find_first("folder='{$this->folder}'") )
        {
            $this->category_id = $row['id'];
        }
        
        $this->menuleft->select_child('articles');
    }
    
    function make_menu()
    {
        $this->menuleft->del_all();
        
        $this->menuleft->add_item( new WEMenuItem( "new_list", $this->dictionary->get('/locale/fair/menuleft/new_list'), "?".$this->controller_name.".new_list" ) );
        $this->menuleft->add_item( new WEMenuSeparator() );
        
        $t = new MysqlTable( 'fair_categories' );
        $t->find_all( "parent_id is null", "position,id" );
        foreach( $t->data as $row )
        {
            $o = new FairCategory( $row['id'] );
            $submenu = new WEMenu( "submenu{$o->id}", $o->name, "?".$this->controller_name.".index&category_id={$o->id}" );
            $this->menuleft->add_item( $submenu );
            $t2 = new MysqlTable( 'fair_categories' );
            $t2->find_all( "parent_id={$o->id}", "position,id" );
            foreach( $t2->data as $row2 )
            {
                $cs = new FairCategory( $row2['id'] );
                $submenu2 = new WEMenu( "submenu{$cs->id}", $cs->name, "?".$this->controller_name.".index&category_id={$cs->id}" );
                $submenu->add_item( $submenu2 );
                $t3 = new MysqlTable( 'fair_categories' );
                $t3->find_all( "parent_id={$cs->id}", "position,id" );
                foreach( $t3->data as $row3 )
                {
                    $cs2 = new FairCategory( $row3['id'] );
                    $submenu2->add_item( new WEMenuItem( "c{$cs2->id}", $cs2->name, "?".$this->controller_name.".index&category_id={$cs2->id}" ) );
                }
            }
        }
        $this->menuleft->add_item( new WEMenuSeparator() );
        $this->menuleft->add_item( new WEMenuItem( "articles", $this->dictionary->get('/locale/shop/menuleft/articles'), "?fair_articles.index" ) );
        $this->menuleft->add_item( new WEMenuSeparator() );
        $this->menuleft->add_item( new WEMenuItem( "paymentways_list", $this->dictionary->get('/locale/fair/menuleft/paymentways_list'), "?fair.paymentways_list" ) );
        $this->menuleft->add_item( new WEMenuItem( "settings", $this->dictionary->get('/locale/fair/menuleft/settings'), "?fair.settings_edit" ) );
    }
    
    function make_elements()
    {
        $retval = "";
        
        $retval .= $this->bigtabs->xml();
        $retval .= $this->smalltabs->xml();
        $retval .= $this->menuleft->xml();
        
        $retval .= "<left_panel show='yes'><categories>";
        $t = new MysqlTable( 'fair_categories' );
        $t->find_all( "parent_id is null", "position,id" );
        foreach( $t->data as $row )
        {
            $o = new FairCategory( $row['id'] );
            $retval .= $o->Xml();
        }
        $retval .= "</categories></left_panel>";
        
        return( $retval );
    }
    
    function index( $parameters='' )
    {
        $this->menuleft->select_child( 'articles_list' );
        
        $this->method_name = 'list';
        $xml = "<data>";
        $xml .= "<articles>";
        
        $t = new MysqlTable( 'articles' );
        parent::define_pages( $t->get_count("category_id={$this->category_id} and folder='{$this->folder}'") );
        $t->find_all( "category_id={$this->category_id} and folder='{$this->folder}'", 'position,id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
        foreach( $t->data as $a )
        {
            $article = new Article( $a['id'], false );
            
            if( $article->position == 0 )
            {
                $tp = new MysqlTable('articles');
                $tp->fix_positions( '', "category_id={$this->category_id} and folder='{$this->folder}'" );
                $article->Load( $a['id'] );
            }
            
            $xml .= $article->Xml();
        }
        
        if( !$article ) $article = new Article();
        $xml .= "<last_position>". $article->GetLastPosition("category_id={$this->category_id} and folder='{$this->folder}'") ."</last_position>";
        
        $xml .= "</articles>";
        $xml .= "<parameters>$parameters</parameters>";
        $xml .= "</data>";
        $this->display( 'list', $xml );
    }
    
    function set_status()
    {
        $id = intval( $_REQUEST['id'] );
        $v = intval( $_REQUEST['v'] );
        $o = new Article( $id );
        if( $o->IsLoaded() )
        {
            $o->status = $v;
            $o->Save();
        }
        
        $this->index();
    }
    
    function set_show_in_menu()
    {
        $id = intval( $_REQUEST['id'] );
        $v = intval( $_REQUEST['v'] );
        $o = new Article( $id );
        if( $o->IsLoaded() )
        {
            $o->show_in_menu = $v;
            $o->Save();
        }
        
        $this->index();
    }
    
    function edit( $save_result="" )
    {
        $last_position = intval( $_REQUEST['last_position'] );
        $this->menuleft->select_child( 'articles_list' );
        
        $this->method_name = 'edit';
        $xml = "<data>";
        
        $xml .= "<article_edit>";
        $xml .= "<save_result>$save_result</save_result>";
        $xml .= "<last_position>$last_position</last_position>";
        $article = new Article( $_REQUEST['id'] );
        $article->category_id = $this->category_id;
        $article->folder = $this->folder;
        $xml .= $article->Xml();
        $xml .= "</article_edit>";
        
        $xml .= "</data>";
        
        $this->display( 'edit', $xml );
    }
    
    function save()
    {
        $erx = "";
        
        $obj = new Article( $_REQUEST['article']['id'] );
        $last_position = intval( $_REQUEST['last_position'] );
        if( $last_position > 0 ) $_REQUEST['article']['position'] = $last_position + 1;
        
        $attachment = new Attachment();
        if( $attachment->upload('image') )
        {
            $_REQUEST['article']['image'] = $attachment->filename;
        }
        
        $_REQUEST['article']['ingress'] = DeleteHTMLComments( $_REQUEST['article']['ingress'] );
        $_REQUEST['article']['text'] = DeleteHTMLComments( $_REQUEST['article']['text'] );
        if( $obj->Save( $_REQUEST['article'] ) )
        {
            $_REQUEST['id'] = $obj->id;
            $this->edit( 'true' );
        }
        else
        {
            $this->edit( 'false' );
        }
    }
    
    function del()
    {
        $this->delete();
    }
    
    function delete()
    {
        $article = new Article( $_REQUEST['id'] );
        $article->Delete();
        $this->index();
    }
    
    function del_marked()
    {
        $ids_str = $_REQUEST['ids'];
        $ids = split( ',', $ids_str );
        foreach( $ids as $id )
        {
            $o = new Article( $id );
            $o->Delete();
        }
        $this->index( 'scroll_to_list' );
    }
    
    function del_image()
    {
        $article = new Article( $_REQUEST['id'] );
        $article->DeleteImage();
        $this->edit( 'true' );
    }
    
    function del_media()
    {
        $image = new Image( $_REQUEST['image_id'] );
        $image->Delete();
        $this->edit( 'true' );
    }
    
    function swap_position()
    {
        $src = new Article( $_REQUEST['src'] );
        $dst = new Article( $_REQUEST['dst'] );
        $this->SwapPosition( $src, $dst, "articles", "category_id={$dst->category_id} and folder='{$this->folder}'" );
        $this->index( 'scroll_to_list' );
    }
    
    function move_up_gallery()
    {
        $t = new MysqlTable('images');
        $article = new Article( $_REQUEST['id'] );
        $t->move_position_up( $_REQUEST['image_id'], $article->folder_for_child, "parent_id={$_REQUEST['id']}" );
        $this->edit( 'scroll_to_media' );
    }
    
    function move_down_gallery()
    {
        $t = new MysqlTable('images');
        $article = new Article( $_REQUEST['id'] );
        $t->move_position_down( $_REQUEST['image_id'], $article->folder_for_child, "parent_id={$_REQUEST['id']}" );
        $this->edit( 'scroll_to_media' );
    }
}

?>
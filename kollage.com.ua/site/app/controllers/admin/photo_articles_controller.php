<?php
/*****************************************************
 Class v.1.0, 02.2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

include_once( 'base_controller.php' );

class photo_articles_controller extends base_controller
{
    var $folder = 'photos';
    var $category_id = 0;
    
    function photo_articles_controller()
    {
        parent::base_controller();
        
        $this->bigtabs->select_child( 'photos' );
        
        $this->menuleft->add_item( new WEMenuItem( "orders_list1", $this->dictionary->get('/locale/sop/menuleft/orders_list1'), "?photos.orders_list1" ) );
        $this->menuleft->add_item( new WEMenuItem( "orders_list2", $this->dictionary->get('/locale/sop/menuleft/orders_list2'), "?photos.orders_list2" ) );
        $this->menuleft->add_item( new WEMenuItem( "orders_list3", $this->dictionary->get('/locale/sop/menuleft/orders_list3'), "?photos.orders_list3" ) );
        $this->menuleft->add_item( new WEMenuItem( "orders_list4", $this->dictionary->get('/locale/sop/menuleft/orders_list4'), "?photos.orders_list4" ) );
        $this->menuleft->add_item( new WEMenuSeparator() );
        $this->menuleft->add_item( new WEMenuItem( "articles_list", $this->dictionary->get('/locale/sop/menuleft/articles_list'), "?photo_articles.index" ) );
        $this->menuleft->add_item( new WEMenuSeparator() );
        $this->menuleft->add_item( new WEMenuItem( "userfiles", $this->dictionary->get('/locale/clients/menuleft/userfiles'), "?photos.list_userfiles_clients" ) );
        $this->menuleft->add_item( new WEMenuSeparator() );
        $this->menuleft->add_item( new WEMenuItem( "settings", $this->dictionary->get('/locale/sop/menuleft/settings'), "?photos.settings_edit" ) );
        $this->menuleft->add_item( new WEMenuItem( "settings2", $this->dictionary->get('/locale/sop/menuleft/settings2'), "?".$this->controller_name.".settings_edit2" ) );
        $this->menuleft->add_item( new WEMenuItem( "points_list", $this->dictionary->get('/locale/sop/menuleft/points_list'), "?photos.points_list" ) );
        $this->menuleft->add_item( new WEMenuItem( "deliveries_list", $this->dictionary->get('/locale/sop/menuleft/deliveries_list'), "?photos.deliveries_list" ) );
        $this->menuleft->add_item( new WEMenuItem( "paymentways_list", $this->dictionary->get('/locale/sop/menuleft/paymentways_list'), "?photos.paymentways_list" ) );
        $this->menuleft->add_item( new WEMenuItem( "discounts_list", $this->dictionary->get('/locale/sop/menuleft/discounts_list'), "?photos.discounts_list" ) );
        $this->menuleft->add_item( new WEMenuItem( "sizes_list", $this->dictionary->get('/locale/sop/menuleft/sizes_list'), "?photos.sizes_list" ) );
        
        $t = new MysqlTable('categories');
        if( $row = $t->find_first("folder='{$this->folder}'") )
        {
            $this->category_id = $row['id'];
        }
    }
    
    function make_elements()
    {
        $retval = "";
        
        $retval .= $this->bigtabs->xml();
        $retval .= $this->smalltabs->xml();
        $retval .= $this->menuleft->xml();
        
        $retval .= "<left_panel show='yes'></left_panel>";
        
        return( $retval );
    }
    
    function index( $parameters='' )
    {
        $this->menuleft->select_child( 'articles_list' );
        
        $this->method_name = 'list';
        $xml = "<data>";
        $xml .= "<articles>";
        
        $t = new MysqlTable( 'articles' );
        parent::define_pages( $t->get_count("category_id={$this->category_id} and folder='{$this->folder}'") );
        $t->find_all( "category_id={$this->category_id} and folder='{$this->folder}'", 'position,id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
        foreach( $t->data as $a )
        {
            $article = new Article( $a['id'] );
            
            if( $article->position == 0 )
            {
                $tp = new MysqlTable('articles');
                $tp->fix_positions( '', "category_id={$this->category_id} and folder='{$this->folder}'" );
                $article->Load( $a['id'] );
            }
            
            $xml .= $article->Xml();
        }
        
        if( !$article ) $article = new Article();
        $xml .= "<last_position>". $article->GetLastPosition("category_id={$this->category_id} and folder='{$this->folder}'") ."</last_position>";
        
        $xml .= "</articles>";
        $xml .= "<parameters>$parameters</parameters>";
        $xml .= "</data>";
        $this->display( 'list', $xml );
    }
    
    function set_status()
    {
        $id = intval( $_REQUEST['id'] );
        $v = intval( $_REQUEST['v'] );
        $o = new Article( $id );
        if( $o->IsLoaded() )
        {
            $o->status = $v;
            $o->Save();
        }
        
        $this->index();
    }
    
    function edit( $save_result="" )
    {
        $last_position = intval( $_REQUEST['last_position'] );
        $this->menuleft->select_child( 'articles_list' );
        
        $this->method_name = 'edit';
        $xml = "<data>";
        
        $xml .= "<article_edit>";
        $xml .= "<save_result>$save_result</save_result>";
        $xml .= "<last_position>$last_position</last_position>";
        $article = new Article( $_REQUEST['id'] );
        $article->category_id = $this->category_id;
        $article->folder = $this->folder;
        $xml .= $article->Xml();
        $xml .= "</article_edit>";
        
        $xml .= "</data>";
        
        $this->display( 'edit', $xml );
    }
    
    function save()
    {
        $erx = "";
        
        $obj = new Article( $_REQUEST['article']['id'] );
        $last_position = intval( $_REQUEST['last_position'] );
        if( $last_position > 0 ) $_REQUEST['article']['position'] = $last_position + 1;
        
        $attachment = new Attachment();
        if( $attachment->upload('image') )
        {
            $_REQUEST['article']['image'] = $attachment->filename;
        }
        
        $_REQUEST['article']['ingress'] = DeleteHTMLComments( $_REQUEST['article']['ingress'] );
        $_REQUEST['article']['text'] = DeleteHTMLComments( $_REQUEST['article']['text'] );
        if( $obj->Save( $_REQUEST['article'] ) )
        {
            $_REQUEST['id'] = $obj->id;
            $this->edit( 'true' );
        }
        else
        {
            $this->edit( 'false' );
        }
    }
    
    function del()
    {
        $this->delete();
    }
    
    function delete()
    {
        $article = new Article( $_REQUEST['id'] );
        $article->Delete();
        $this->index();
    }
    
    function del_marked()
    {
        $ids_str = $_REQUEST['ids'];
        $ids = split( ',', $ids_str );
        foreach( $ids as $id )
        {
            $o = new Article( $id );
            $o->Delete();
        }
        $this->index( 'scroll_to_list' );
    }
    
    function del_image()
    {
        $article = new Article( $_REQUEST['id'] );
        $article->DeleteImage();
        $this->edit( 'true' );
    }
    
    function del_media()
    {
        $image = new Image( $_REQUEST['image_id'] );
        $image->Delete();
        $this->edit( 'true' );
    }
    
    function set_show_in_menu()
    {
        $id = intval( $_REQUEST['id'] );
        $v = intval( $_REQUEST['v'] );
        $o = new Article( $id );
        if( $o->IsLoaded() )
        {
            $o->show_in_menu = $v;
            $o->Save();
        }
        
        $this->index();
    }
    
    function swap_position()
    {
        $src = new Article( $_REQUEST['src'] );
        $dst = new Article( $_REQUEST['dst'] );
        $this->SwapPosition( $src, $dst, "articles", "category_id={$dst->category_id} and folder='{$this->folder}'" );
        $this->index( 'scroll_to_list' );
    }
    
    function move_up_gallery()
    {
        $t = new MysqlTable('images');
        $article = new Article( $_REQUEST['id'] );
        $t->move_position_up( $_REQUEST['image_id'], $article->folder_for_child, "parent_id={$_REQUEST['id']}" );
        $this->edit( 'scroll_to_media' );
    }
    
    function move_down_gallery()
    {
        $t = new MysqlTable('images');
        $article = new Article( $_REQUEST['id'] );
        $t->move_position_down( $_REQUEST['image_id'], $article->folder_for_child, "parent_id={$_REQUEST['id']}" );
        $this->edit( 'scroll_to_media' );
    }
}

?>
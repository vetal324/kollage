<?php
include_once( 'base_controller.php' );
class orders_controller extends  base_controller {

    public function index($parameters=''){
        $this->bigtabs->select_child( 'orders' );
        $this->method_name = 'index';
        $xml = "<data>";
        $xml .= "<orders>";
        $t = new MysqlTable( 'canv_orders' );
        parent::define_pages( $t->get_count("id>=0") );
        $t->find_all( "id>=0", 'id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
        foreach( $t->data as $a )
        {
            $xml .= Orders::XmlStatic($a);
        }
        $xml .= "</orders>";
        $xml .= "<parameters>$parameters</parameters>";
        $xml .= "</data>";
        $this->display( 'list', $xml );
    }
    function del(){
        if(isset($_REQUEST['id'])){
            Orders::getModel()->delete($_REQUEST['id']);
            header('Location: /admin/?orders.index');
        }
    }
}
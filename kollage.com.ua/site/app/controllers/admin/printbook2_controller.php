<?php
/*****************************************************
 Class v.1.0, 08.2011
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

include_once( 'base_controller.php' );

class printbook2_controller extends base_controller
{
	var $box_category_folder = 'box';
	var $cover_category_folder = 'cover';
	var $design_work_category_folder = 'design_work';
	
	function printbook2_controller()
	{
		parent::base_controller();
		
		$this->bigtabs->select_child( 'printbook2' );

		$this->menuleft->add_item( new WEMenuItem( "sizes_list", $this->dictionary->get('/locale/printbook2/menuleft/sizes_list'), "?{$this->controller_name}.pb_sizes_list" ) );
		//$this->menuleft->add_item( new WEMenuItem( "settings", $this->dictionary->get('/locale/sop/menuleft/settings'), "?".$this->controller_name.".settings_edit" ) );
	}
	
	function index()
	{
		$this->pb_sizes_list();
	}
	
	// <Photo book sizes>
	function pb_sizes_list( $parameters='' )
	{
		$this->bigtabs->select_child( 'printbook2' );
		$this->menuleft->select_child( 'sizes_list' );
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/printbook2/smalltabs/list'), "?".$this->controller_name.".pb_sizes_list") );
		
		$this->method_name = 'pb_sizes_list';
		$xml = "<data>";
		$xml .= "<list>";
		
		$t = new MysqlTable( 'pb2_sizes' );
		parent::define_pages( $t->get_count('status=1') );
		$t->find_all( 'status=1', 'position,id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$o = new PB2Size( $a['id'] );
			
			if( $o->position == 0 )
			{
				$tp = new MysqlTable('pb2_sizes');
				$tp->fix_positions( '', "status=1" );
				$o->Load( $a['id'] );
			}
			
			$xml .= $o->Xml();
		}
		
		$xml .= "<last_position>{$o->position}</last_position>";
		
		$xml .= "</list>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'pb_sizes_list', $xml );
	}
	
	function pb_size_edit( $save_result="" )
	{
		$last_position = intval( $_REQUEST['last_position'] );
		
		$this->bigtabs->select_child( 'printbook2' );
		$this->menuleft->select_child( 'sizes_list' );
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/printbook2/smalltabs/list'), "?".$this->controller_name.".pb_sizes_list") );
		
		$this->method_name = 'pb_size_edit';
		$xml = "<data>";
		
		$xml .= "<edit>";
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= "<last_position>$last_position</last_position>";
		$o = new PB2Size( $_REQUEST['id'] );
		$xml .= $o->Xml();
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'pb_size_edit', $xml );
	}
	
	function pb_size_save()
	{
		$erx = "";
		
		$obj = new PB2Size( $_REQUEST['object']['id'] );
		$last_position = intval( $_REQUEST['last_position'] );
		if( $last_position > 0 ) $_REQUEST['object']['position'] = $last_position + 1;
		
		$attachment = new Attachment();
		if( $attachment->upload('image') )
		{
			$_REQUEST['object']['image'] = $attachment->filename;
			$io = new AImage( SITEROOT . '/'. $_ENV['data_path'] .'/'. $attachment->filename );
			$io->CopyResize( SITEROOT . '/'. $_ENV['data_path'] .'/thumbnail-'. $attachment->filename, 200, 200 );
		}
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->pb_size_edit( 'true' );
		}
		else
		{
			$this->pb_size_edit( 'false' );
		}
	}
	
	function pb_size_del()
	{
		$o = new PB2Size( $_REQUEST['id'] );
		$o->Delete();
		$this->pb_sizes_list( 'scroll_to_list' );
	}
	
	function pb_sizes_del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new PB2Size( $id );
			$o->Delete();
		}
		$this->pb_sizes_list( 'scroll_to_list' );
	}
	
	function pb_size_del_image()
	{
		$o = new PB2Size( $_REQUEST['id'] );
		$o->DeleteImage();
		$this->pb_size_edit( 'true' );
	}
	
	function pb_sizes_swap_position()
	{
		$src = new PB2Size( $_REQUEST['src'] );
		$dst = new PB2Size( $_REQUEST['dst'] );
		$this->SwapPosition( $src, $dst, "pb2_sizes", "status=1" );
		$this->pb_sizes_list( 'scroll_to_list' );
	}
	// </Photo book sizes>
	
	// <Photo book covers>
	function cover_categories_list( $parameters='' )
	{
		$this->bigtabs->select_child( 'printbook2' );
		$this->menuleft->select_child( 'sizes_list' );
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/printbook2/smalltabs/list'), "?".$this->controller_name.".pb_sizes_list") );
		
		$this->method_name = 'cover_categories_list';
		$xml = "<data>";
		$xml .= "<list>";
		
		$pb_size_id = intval( $_REQUEST['pb_size_id'] );
		
		$pb_size = new PB2Size($pb_size_id);
		$xml .= $pb_size->Xml();
		
		$t = new MysqlTable( 'pb2_categories' );
		parent::define_pages( $t->get_count("status=1 and folder='{$this->cover_category_folder}' and parent_id={$pb_size_id}") );
		$t->find_all( "status=1 and folder='{$this->cover_category_folder}' and parent_id={$pb_size_id}", 'position,id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$o = new PB2Category( $a['id'] );
			
			if( $o->position == 0 )
			{
				$tp = new MysqlTable('pb2_categories');
				$tp->fix_positions( '', "status=1 and folder='{$this->cover_category_folder}'" );
				$o->Load( $a['id'] );
			}
			
			$xml .= $o->Xml();
		}
		
		$xml .= "<last_position>{$o->position}</last_position>";
		
		$xml .= "</list>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'cover_categories_list', $xml );
	}
	
	function cover_category_edit( $save_result="" )
	{
		$last_position = intval( $_REQUEST['last_position'] );
		$pb_size_id = intval( $_REQUEST['pb_size_id'] );
		
		$this->bigtabs->select_child( 'printbook2' );
		$this->menuleft->select_child( 'sizes_list' );
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/printbook2/smalltabs/list'), "?".$this->controller_name.".pb_sizes_list") );
		
		$this->method_name = 'cover_category_edit';
		$xml = "<data>";
		
		$xml .= "<edit>";
		
		$pb_size = new PB2Size($pb_size_id);
		$xml .= $pb_size->Xml();
		
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= "<last_position>$last_position</last_position>";
		$xml .= "<pb_size_id>$pb_size_id</pb_size_id>";
		$o = new PB2Category( $_REQUEST['id'] );
		$xml .= $o->Xml();
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'cover_category_edit', $xml );
	}
	
	function cover_category_save()
	{
		$erx = "";
		
		$obj = new PB2Category( $_REQUEST['object']['id'] );
		$last_position = intval( $_REQUEST['last_position'] );
		if( $last_position > 0 ) $_REQUEST['object']['position'] = $last_position + 1;
		$_REQUEST['object']['folder'] = $this->cover_category_folder;
		
		$attachment = new Attachment();
		if( $attachment->upload('image') )
		{
			$_REQUEST['object']['image'] = $attachment->filename;
			$io = new AImage( SITEROOT . '/'. $_ENV['data_path'] .'/'. $attachment->filename );
			$io->CopyResize( SITEROOT . '/'. $_ENV['data_path'] .'/thumbnail-'. $attachment->filename, 200, 200 );
		}
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->cover_category_edit( 'true' );
		}
		else
		{
			$this->cover_category_edit( 'false' );
		}
	}
	
	function cover_category_del()
	{
		$o = new PB2Category( $_REQUEST['id'] );
		$o->Delete();
		$this->cover_categories_list( 'scroll_to_list' );
	}
	
	function cover_category_del_image()
	{
		$o = new PB2Category( $_REQUEST['id'] );
		$o->DeleteImage();
		$this->cover_category_edit( 'true' );
	}
	
	function cover_categories_del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new PB2Category( $id );
			$o->Delete();
		}
		$this->cover_categories_list( 'scroll_to_list' );
	}
	
	function cover_categories_swap_position()
	{
		$src = new PB2Category( $_REQUEST['src'] );
		$dst = new PB2Category( $_REQUEST['dst'] );
		$this->SwapPosition( $src, $dst, "pb2_categories", "status=1 and folder='{$this->cover_category_folder}'" );
		$this->cover_categories_list( 'scroll_to_list' );
	}
	
	function pb_covers_list( $parameters='' )
	{
		$this->bigtabs->select_child( 'printbook2' );
		$this->menuleft->select_child( 'sizes_list' );
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/printbook2/smalltabs/list'), "?".$this->controller_name.".pb_sizes_list") );
		
		$this->method_name = 'pb_covers_list';
		$xml = "<data>";
		$xml .= "<list>";
		
		$pb_size_id = intval( $_REQUEST['pb_size_id'] );
		$category_id = intval( $_REQUEST['pb_category_id'] );
		
		$pb_size = new PB2Size($pb_size_id);
		$xml .= $pb_size->Xml();
		
		$pb_category = new PB2Category($category_id);
		$xml .= $pb_category->Xml();
		
		$t = new MysqlTable( 'pb2_covers' );
		parent::define_pages( $t->get_count("status=1 and pb_size_id={$pb_size_id} and category_id={$category_id}") );
		$t->find_all( "status=1 and pb_size_id={$pb_size_id} and category_id={$category_id}", 'position,id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$o = new PB2Cover( $a['id'] );
			
			if( $o->position == 0 )
			{
				$tp = new MysqlTable('pb2_covers');
				$tp->fix_positions( '', "status=1 and pb_size_id={$pb_size_id}" );
				$o->Load( $a['id'] );
			}
			
			$xml .= $o->Xml();
		}
		
		$xml .= "<last_position>{$o->position}</last_position>";
		
		$xml .= "</list>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'pb_covers_list', $xml );
	}
	
	function pb_cover_edit( $save_result="" )
	{
		$last_position = intval( $_REQUEST['last_position'] );
		$pb_size_id = intval( $_REQUEST['pb_size_id'] );
		
		$o = new PB2Cover( $_REQUEST['id'] );
		$category_id = ($o->category_id > 0)? $o->category_id : intval( $_REQUEST['pb_category_id'] );
		
		$this->bigtabs->select_child( 'printbook2' );
		$this->menuleft->select_child( 'sizes_list' );
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/printbook2/smalltabs/list'), "?".$this->controller_name.".pb_sizes_list") );
		
		$this->method_name = 'pb_cover_edit';
		$xml = "<data>";
		
		$xml .= "<edit>";
		
		$pb_size = new PB2Size($pb_size_id);
		$xml .= $pb_size->Xml();
		
		$pb_category = new PB2Category($category_id);
		$xml .= $pb_category->Xml();
		
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= "<last_position>$last_position</last_position>";
		$xml .= "<pb_size_id>$pb_size_id</pb_size_id>";
		$xml .= "<pb_category_id>$category_id</pb_category_id>";
		$xml .= $o->Xml();
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'pb_cover_edit', $xml );
	}
	
	function pb_cover_save()
	{
		$erx = "";
		
		$obj = new PB2Cover( $_REQUEST['object']['id'] );
		$last_position = intval( $_REQUEST['last_position'] );
		if( $last_position > 0 ) $_REQUEST['object']['position'] = $last_position + 1;
		
		$_REQUEST['pb_size_id'] = ( $obj->pb_size_id>0 )? $obj->pb_size_id : intval($_REQUEST['pb_size_id']);
		$_REQUEST['pb_category_id'] = ( $obj->pb_category_id>0 )? $obj->pb_category_id : intval($_REQUEST['pb_category_id']);
		
		$_REQUEST['object']['category_id'] = $_REQUEST['pb_category_id'];
		
		if( !$_REQUEST['object']['price'] ) $_REQUEST['object']['price'] = 0;
		if( !$_REQUEST['object']['supercover_price'] ) $_REQUEST['object']['supercover_price'] = 0;
		if( !$_REQUEST['object']['has_supercover'] ) $_REQUEST['object']['has_supercover'] = 0;
        if( !$_REQUEST['object']['allow_min_pages'] ) $_REQUEST['object']['allow_min_pages'] = 0;
		
		$attachment = new Attachment();
		if( $attachment->upload('image') )
		{
			$_REQUEST['object']['image'] = $attachment->filename;
			$io = new AImage( SITEROOT . '/'. $_ENV['data_path'] .'/'. $attachment->filename );
			$io->CopyResize( SITEROOT . '/'. $_ENV['data_path'] .'/thumbnail-'. $attachment->filename, 200, 200 );
		}
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->pb_cover_edit( 'true' );
		}
		else
		{
			$this->pb_cover_edit( 'false' );
		}
	}
	
	function pb_cover_del()
	{
		$o = new PB2Cover( $_REQUEST['id'] );
		$o->Delete();
		$_REQUEST['pb_size_id'] = $o->pb_size_id;
		$_REQUEST['pb_category_id'] = $o->category_id;
		$this->pb_covers_list( 'scroll_to_list' );
	}
	
	function pb_covers_del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new PB2Cover( $id );
			$o->Delete();
		}
		$_REQUEST['pb_size_id'] = $o->pb_size_id;
		$_REQUEST['pb_category_id'] = $o->category_id;
		$this->pb_covers_list( 'scroll_to_list' );
	}
	
	function pb_cover_del_image()
	{
		$o = new PB2Cover( $_REQUEST['id'] );
		$o->DeleteImage();
		$this->pb_cover_edit( 'true' );
	}
	
	function pb_covers_swap_position()
	{
		$src = new PB2Cover( $_REQUEST['src'] );
		$dst = new PB2Cover( $_REQUEST['dst'] );
		$this->SwapPosition( $src, $dst, "pb2_covers", "status=1 and pb_size_id={$src->pb_size_id}" );
		$_REQUEST['pb_size_id'] = $src->pb_size_id;
		$_REQUEST['pb_category_id'] = $src->category_id;
		$this->pb_covers_list( 'scroll_to_list' );
	}
	// </Photo book covers>
	
    // <Photo book forzaces>
	function pb_forzaces_list( $parameters='' )
	{
        //global $log;
        
		$this->bigtabs->select_child( 'printbook2' );
		$this->menuleft->select_child( 'sizes_list' );
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/printbook2/smalltabs/list'), "?".$this->controller_name.".pb_sizes_list") );
		
		$this->method_name = 'pb_forzaces_list';
		$xml = "<data>";
		$xml .= "<list>";
		
		$pb_size_id = intval( $_REQUEST['pb_size_id'] );
		
		$pb_size = new PB2Size($pb_size_id);
		$xml .= $pb_size->Xml();
		
		$t = new MysqlTable( 'pb2_forzaces' );
		parent::define_pages( $t->get_count("status=1 and pb_size_id={$pb_size_id}") );
		$t->find_all( "status=1 and pb_size_id={$pb_size_id}", 'position,id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$o = new PB2Forzace( $a['id'] );
			
			if( $o->position == 0 )
			{
				$tp = new MysqlTable('pb2_forzaces');
				$tp->fix_positions( '', "status=1 and pb_size_id={$pb_size_id}" );
				$o->Load( $a['id'] );
			}
			
			$xml .= $o->Xml();
		}
		
		$xml .= "<last_position>{$o->position}</last_position>";
		
		$xml .= "</list>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		
        //$log->Write( "pb_forzaces_list: $xml" );
        
        $this->display( 'pb_forzaces_list', $xml );
	}
	
	function pb_forzace_edit( $save_result="" )
	{
		$last_position = intval( $_REQUEST['last_position'] );
		$pb_size_id = intval( $_REQUEST['pb_size_id'] );
		
		$o = new PB2Forzace( $_REQUEST['id'] );
		
		$this->bigtabs->select_child( 'printbook2' );
		$this->menuleft->select_child( 'sizes_list' );
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/printbook2/smalltabs/list'), "?".$this->controller_name.".pb_sizes_list") );
		
		$this->method_name = 'pb_forzace_edit';
		$xml = "<data>";
		
		$xml .= "<edit>";
		
		$pb_size = new PB2Size($pb_size_id);
		$xml .= $pb_size->Xml();
		
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= "<last_position>$last_position</last_position>";
		$xml .= "<pb_size_id>$pb_size_id</pb_size_id>";
		$xml .= $o->Xml();
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'pb_forzace_edit', $xml );
	}
    
	function pb_forzace_save()
	{
		$erx = "";
		
		$obj = new PB2Forzace( $_REQUEST['object']['id'] );
		$last_position = intval( $_REQUEST['last_position'] );
		if( $last_position > 0 ) $_REQUEST['object']['position'] = $last_position + 1;
		
		$_REQUEST['pb_size_id'] = ( $obj->pb_size_id>0 )? $obj->pb_size_id : intval($_REQUEST['pb_size_id']);
		
		if( !$_REQUEST['object']['price'] ) $_REQUEST['object']['price'] = 0;
        if( !$_REQUEST['object']['allow_min_pages'] ) $_REQUEST['object']['allow_min_pages'] = 0;
		
		$attachment = new Attachment();
		if( $attachment->upload('image') )
		{
			$_REQUEST['object']['image'] = $attachment->filename;
			$io = new AImage( SITEROOT . '/'. $_ENV['data_path'] .'/'. $attachment->filename );
			$io->CopyResize( SITEROOT . '/'. $_ENV['data_path'] .'/thumbnail-'. $attachment->filename, 200, 200 );
		}
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->pb_forzace_edit( 'true' );
		}
		else
		{
			$this->pb_forzace_edit( 'false' );
		}
	}

	function pb_forzace_del()
	{
		$o = new PB2Forzace( $_REQUEST['id'] );
		$o->Delete();
		$_REQUEST['pb_size_id'] = $o->pb_size_id;
		$this->pb_forzaces_list( 'scroll_to_list' );
	}
	 
	function pb_forzaces_del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new PB2Forzace( $id );
			$o->Delete();
		}
		$_REQUEST['pb_size_id'] = $o->pb_size_id;
		$this->pb_forzaces_list( 'scroll_to_list' );
	}
    
	function pb_forzace_del_image()
	{
		$o = new PB2Forzace( $_REQUEST['id'] );
		$o->DeleteImage();
		$this->pb_forzace_edit( 'true' );
	}

	function pb_forzaces_swap_position()
	{
		$src = new PB2Forzace( $_REQUEST['src'] );
		$dst = new PB2Forzace( $_REQUEST['dst'] );
		$this->SwapPosition( $src, $dst, "pb2_forzaces", "status=1 and pb_size_id={$src->pb_size_id}" );
		$_REQUEST['pb_size_id'] = $src->pb_size_id;
		$this->pb_forzaces_list( 'scroll_to_list' );
	}
    // </Photo book forzaces>
    
	// <Photobook supercovers>
	function pb_supercovers_list( $parameters='' )
	{
		$this->bigtabs->select_child( 'printbook2' );
		$this->menuleft->select_child( 'sizes_list' );
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/printbook2/smalltabs/list'), "?".$this->controller_name.".pb_sizes_list") );
		
		$this->method_name = 'pb_supercovers_list';
		$xml = "<data>";
		$xml .= "<list>";
		
		$pb_size_id = intval( $_REQUEST['pb_size_id'] );
		$category_id = intval( $_REQUEST['pb_category_id'] );
		$pb_cover_id = intval( $_REQUEST['pb_cover_id'] );
		
		$pb_size = new PB2Size($pb_size_id);
		$xml .= $pb_size->Xml();
		
		$pb_category = new PB2Category($category_id);
		$xml .= $pb_category->Xml();
		
		$pb_cover = new PB2Cover($pb_cover_id);
		$xml .= $pb_cover->Xml();
		
		$t = new MysqlTable( 'pb2_supercovers' );
		parent::define_pages( $t->get_count("status=1 and pb_cover_id={$pb_cover_id}") );
		$t->find_all( "status=1 and pb_cover_id={$pb_cover_id}", 'position,id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$o = new PB2SuperCover( $a['id'] );
			
			if( $o->position == 0 )
			{
				$tp = new MysqlTable('pb2_supercovers');
				$tp->fix_positions( '', "status=1 and pb_cover_id={$pb_cover_id}" );
				$o->Load( $a['id'] );
			}
			
			$xml .= $o->Xml();
		}
		
		$xml .= "<last_position>{$o->position}</last_position>";
		
		$xml .= "</list>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'pb_supercovers_list', $xml );
	}
	
	function pb_supercover_edit( $save_result="" )
	{
		$last_position = intval( $_REQUEST['last_position'] );
		$pb_size_id = intval( $_REQUEST['pb_size_id'] );
		$category_id = intval( $_REQUEST['pb_category_id'] );
		$pb_cover_id = intval( $_REQUEST['pb_cover_id'] );
		
		$o = new PB2SuperCover( $_REQUEST['id'] );
		
		$this->bigtabs->select_child( 'printbook2' );
		$this->menuleft->select_child( 'sizes_list' );
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/printbook2/smalltabs/list'), "?".$this->controller_name.".pb_sizes_list") );
		
		$this->method_name = 'pb_supercover_edit';
		$xml = "<data>";
		
		$xml .= "<edit>";
		
		$pb_size = new PB2Size($pb_size_id);
		$xml .= $pb_size->Xml();
		
		$pb_category = new PB2Category($category_id);
		$xml .= $pb_category->Xml();
		
		$pb_cover = new PB2Cover($pb_cover_id);
		$xml .= $pb_cover->Xml();
		
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= "<last_position>$last_position</last_position>";
		$xml .= "<pb_size_id>$pb_size_id</pb_size_id>";
		$xml .= "<pb_category_id>$category_id</pb_category_id>";
		$xml .= "<pb_cover_id>$pb_cover_id</pb_cover_id>";
		$xml .= $o->Xml();
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'pb_supercover_edit', $xml );
	}
	
	function pb_supercover_save()
	{
		$erx = "";
		
		$obj = new PB2SuperCover( $_REQUEST['object']['id'] );
		$last_position = intval( $_REQUEST['last_position'] );
		if( $last_position > 0 ) $_REQUEST['object']['position'] = $last_position + 1;
		
		$_REQUEST['pb_size_id'] = intval($_REQUEST['pb_size_id']);
		$_REQUEST['pb_category_id'] = intval($_REQUEST['pb_category_id']);
		$_REQUEST['pb_cover_id'] = ( $obj->pb_cover_id>0 )? $obj->pb_cover_id : intval($_REQUEST['pb_cover_id']);
		
		$attachment = new Attachment();
		if( $attachment->upload('image') )
		{
			$_REQUEST['object']['image'] = $attachment->filename;
			$io = new AImage( SITEROOT . '/'. $_ENV['data_path'] .'/'. $attachment->filename );
			$io->CopyResize( SITEROOT . '/'. $_ENV['data_path'] .'/thumbnail-'. $attachment->filename, 200, 200 );
		}
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->pb_supercover_edit( 'true' );
		}
		else
		{
			$this->pb_supercover_edit( 'false' );
		}
	}
	
	function pb_supercover_del()
	{
		$o = new PB2SuperCover( $_REQUEST['id'] );
		$o->Delete();
		$_REQUEST['pb_cover_id'] = $o->pb_cover_id;
		$this->pb_supercovers_list( 'scroll_to_list' );
	}
	
	function pb_supercovers_del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new PB2SuperCover( $id );
			$o->Delete();
		}
		$_REQUEST['pb_cover_id'] = $o->pb_cover_id;
		$this->pb_supercovers_list( 'scroll_to_list' );
	}
	
	function pb_supercovers_swap_position()
	{
		$src = new PB2SuperCover( $_REQUEST['src'] );
		$dst = new PB2SuperCover( $_REQUEST['dst'] );
		$this->SwapPosition( $src, $dst, "pb2_supercovers", "status=1 and pb_cover_id={$src->pb_cover_id}" );
		$_REQUEST['pb_cover_id'] = $src->pb_cover_id;
		$this->pb_supercovers_list( 'scroll_to_list' );
	}
	// </Photobook supercovers>
	
	// <Photo book papers>
	function pb_papers_list( $parameters='' )
	{
		$this->bigtabs->select_child( 'printbook2' );
		$this->menuleft->select_child( 'sizes_list' );
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/printbook2/smalltabs/list'), "?".$this->controller_name.".pb_sizes_list") );
		
		$this->method_name = 'pb_papers_list';
		$xml = "<data>";
		$xml .= "<list>";
		
		$pb_size_id = intval( $_REQUEST['pb_size_id'] );
		
		$pb_size = new PB2Size($pb_size_id);
		$xml .= $pb_size->Xml();
		
		$t = new MysqlTable( 'pb2_papers' );
		parent::define_pages( $t->get_count("status=1 and pb_size_id={$pb_size_id}") );
		$t->find_all( "status=1 and pb_size_id={$pb_size_id}", 'position,id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$o = new PB2Paper( $a['id'] );
			
			if( $o->position == 0 )
			{
				$tp = new MysqlTable('pb2_papers');
				$tp->fix_positions( '', "status=1 and pb_size_id={$pb_size_id}" );
				$o->Load( $a['id'] );
			}
			
			$xml .= $o->Xml();
		}
		
		$xml .= "<last_position>{$o->position}</last_position>";
		
		$xml .= "</list>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'pb_papers_list', $xml );
	}
	
	function pb_paper_edit( $save_result="" )
	{
		$last_position = intval( $_REQUEST['last_position'] );
		$pb_size_id = intval( $_REQUEST['pb_size_id'] );
		
		$this->bigtabs->select_child( 'printbook2' );
		$this->menuleft->select_child( 'sizes_list' );
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/printbook2/smalltabs/list'), "?".$this->controller_name.".pb_sizes_list") );
		
		$this->method_name = 'pb_paper_edit';
		$xml = "<data>";
		
		$xml .= "<edit>";
		
		$pb_size = new PB2Size($pb_size_id);
		$xml .= $pb_size->Xml();
		
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= "<last_position>$last_position</last_position>";
		$xml .= "<pb_size_id>$pb_size_id</pb_size_id>";
		$o = new PB2Paper( $_REQUEST['id'] );
		$xml .= $o->Xml();
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'pb_paper_edit', $xml );
	}
	
	function pb_paper_save()
	{
		$erx = "";
		
		$obj = new PB2Paper( $_REQUEST['object']['id'] );
		$last_position = intval( $_REQUEST['last_position'] );
		if( $last_position > 0 ) $_REQUEST['object']['position'] = $last_position + 1;
		
		$_REQUEST['pb_size_id'] = ( $obj->pb_size_id>0 )? $obj->pb_size_id : intval($_REQUEST['pb_size_id']);
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->pb_paper_edit( 'true' );
		}
		else
		{
			$this->pb_paper_edit( 'false' );
		}
	}
	
	function pb_paper_del()
	{
		$o = new PB2Paper( $_REQUEST['id'] );
		$o->Delete();
		$_REQUEST['pb_size_id'] = $o->pb_size_id;
		$this->pb_papers_list( 'scroll_to_list' );
	}
	
	function pb_papers_del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new PB2Paper( $id );
			$o->Delete();
		}
		$_REQUEST['pb_size_id'] = $o->pb_size_id;
		$this->pb_papers_list( 'scroll_to_list' );
	}
	
	function pb_papers_swap_position()
	{
		$src = new PB2Paper( $_REQUEST['src'] );
		$dst = new PB2Paper( $_REQUEST['dst'] );
		$this->SwapPosition( $src, $dst, "pb2_papers", "status=1 and pb_size_id={$src->pb_size_id}" );
		$_REQUEST['pb_size_id'] = $src->pb_size_id;
		$this->pb_papers_list( 'scroll_to_list' );
	}
	// </Photo book papers>
	
	// <Photo book lamination>
	function pb_laminations_list( $parameters='' )
	{
		$this->bigtabs->select_child( 'printbook2' );
		$this->menuleft->select_child( 'sizes_list' );
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/printbook2/smalltabs/list'), "?".$this->controller_name.".pb_sizes_list") );
		
		$this->method_name = 'pb_laminations_list';
		$xml = "<data>";
		$xml .= "<list>";
		
		$pb_size_id = intval( $_REQUEST['pb_size_id'] );
		
		$pb_size = new PB2Size($pb_size_id);
		$xml .= $pb_size->Xml();
		
		$t = new MysqlTable( 'pb2_lamination' );
		parent::define_pages( $t->get_count("status=1 and pb_size_id={$pb_size_id}") );
		$t->find_all( "status=1 and pb_size_id={$pb_size_id}", 'position,id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$o = new PB2Lamination( $a['id'] );
			
			if( $o->position == 0 )
			{
				$tp = new MysqlTable('pb2_lamination');
				$tp->fix_positions( '', "status=1 and pb_size_id={$pb_size_id}" );
				$o->Load( $a['id'] );
			}
			
			$xml .= $o->Xml();
		}
		
		$xml .= "<last_position>{$o->position}</last_position>";
		
		$xml .= "</list>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'pb_laminations_list', $xml );
	}
	
	function pb_lamination_edit( $save_result="" )
	{
		$last_position = intval( $_REQUEST['last_position'] );
		$pb_size_id = intval( $_REQUEST['pb_size_id'] );
		
		$this->bigtabs->select_child( 'printbook2' );
		$this->menuleft->select_child( 'sizes_list' );
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/printbook2/smalltabs/list'), "?".$this->controller_name.".pb_sizes_list") );
		
		$this->method_name = 'pb_lamination_edit';
		$xml = "<data>";
		
		$xml .= "<edit>";
		
		$pb_size = new PB2Size($pb_size_id);
		$xml .= $pb_size->Xml();
		
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= "<last_position>$last_position</last_position>";
		$xml .= "<pb_size_id>$pb_size_id</pb_size_id>";
		$o = new PB2Lamination( $_REQUEST['id'] );
		$xml .= $o->Xml();
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'pb_lamination_edit', $xml );
	}
	
	function pb_lamination_save()
	{
		$erx = "";
		
		$obj = new PB2Lamination( $_REQUEST['object']['id'] );
		$last_position = intval( $_REQUEST['last_position'] );
		if( $last_position > 0 ) $_REQUEST['object']['position'] = $last_position + 1;
		
		$_REQUEST['pb_size_id'] = ( $obj->pb_size_id>0 )? $obj->pb_size_id : intval($_REQUEST['pb_size_id']);
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->pb_lamination_edit( 'true' );
		}
		else
		{
			$this->pb_lamination_edit( 'false' );
		}
	}
	
	function pb_lamination_del()
	{
		$o = new PB2Lamination( $_REQUEST['id'] );
		$o->Delete();
		$_REQUEST['pb_size_id'] = $o->pb_size_id;
		$this->pb_laminations_list( 'scroll_to_list' );
	}
	
	function pb_laminations_del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new PB2Lamination( $id );
			$o->Delete();
		}
		$_REQUEST['pb_size_id'] = $o->pb_size_id;
		$this->pb_laminations_list( 'scroll_to_list' );
	}
	
	function pb_laminations_swap_position()
	{
		$src = new PB2Lamination( $_REQUEST['src'] );
		$dst = new PB2Lamination( $_REQUEST['dst'] );
		$this->SwapPosition( $src, $dst, "pb2_lamination", "status=1 and pb_size_id={$src->pb_size_id}" );
		$_REQUEST['pb_size_id'] = $src->pb_size_id;
		$this->pb_laminations_list( 'scroll_to_list' );
	}
	// </Photo book lamination>
	
	// <Photo book paper base>
	function pb_paper_bases_list( $parameters='' )
	{
		$this->bigtabs->select_child( 'printbook2' );
		$this->menuleft->select_child( 'sizes_list' );
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/printbook2/smalltabs/list'), "?".$this->controller_name.".pb_sizes_list") );
		
		$this->method_name = 'pb_paper_bases_list';
		$xml = "<data>";
		$xml .= "<list>";
		
		$pb_size_id = intval( $_REQUEST['pb_size_id'] );
		
		$pb_size = new PB2Size($pb_size_id);
		$xml .= $pb_size->Xml();
		
		$t = new MysqlTable( 'pb2_paper_base' );
		parent::define_pages( $t->get_count("status=1 and pb_size_id={$pb_size_id}") );
		$t->find_all( "status=1 and pb_size_id={$pb_size_id}", 'position,id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$o = new PB2PaperBase( $a['id'] );
			
			if( $o->position == 0 )
			{
				$tp = new MysqlTable('pb2_paper_base');
				$tp->fix_positions( '', "status=1 and pb_size_id={$pb_size_id}" );
				$o->Load( $a['id'] );
			}
			
			$xml .= $o->Xml();
		}
		
		$xml .= "<last_position>{$o->position}</last_position>";
		
		$xml .= "</list>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'pb_paper_base_list', $xml );
	}
	
	function pb_paper_base_edit( $save_result="" )
	{
		$last_position = intval( $_REQUEST['last_position'] );
		$pb_size_id = intval( $_REQUEST['pb_size_id'] );
		
		$this->bigtabs->select_child( 'printbook2' );
		$this->menuleft->select_child( 'sizes_list' );
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/printbook2/smalltabs/list'), "?".$this->controller_name.".pb_sizes_list") );
		
		$this->method_name = 'pb_paper_base_edit';
		$xml = "<data>";
		
		$xml .= "<edit>";
		
		$pb_size = new PB2Size($pb_size_id);
		$xml .= $pb_size->Xml();
		
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= "<last_position>$last_position</last_position>";
		$xml .= "<pb_size_id>$pb_size_id</pb_size_id>";
		$o = new PB2PaperBase( $_REQUEST['id'] );
		$xml .= $o->Xml();
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'pb_paper_base_edit', $xml );
	}
	
	function pb_paper_base_save()
	{
		$erx = "";
		
		$obj = new PB2PaperBase( $_REQUEST['object']['id'] );
		$last_position = intval( $_REQUEST['last_position'] );
		if( $last_position > 0 ) $_REQUEST['object']['position'] = $last_position + 1;
		
		$_REQUEST['pb_size_id'] = ( $obj->pb_size_id>0 )? $obj->pb_size_id : intval($_REQUEST['pb_size_id']);
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->pb_paper_base_edit( 'true' );
		}
		else
		{
			$this->pb_paper_base_edit( 'false' );
		}
	}
	
	function pb_paper_base_del()
	{
		$o = new PB2PaperBase( $_REQUEST['id'] );
		$o->Delete();
		$_REQUEST['pb_size_id'] = $o->pb_size_id;
		$this->pb_paper_bases_list( 'scroll_to_list' );
	}
	
	function pb_paper_bases_del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new PB2PaperBase( $id );
			$o->Delete();
		}
		$_REQUEST['pb_size_id'] = $o->pb_size_id;
		$this->pb_paper_bases_list( 'scroll_to_list' );
	}
	
	function pb_paper_bases_swap_position()
	{
		$src = new PB2PaperBase( $_REQUEST['src'] );
		$dst = new PB2PaperBase( $_REQUEST['dst'] );
		$this->SwapPosition( $src, $dst, "pb2_paper_base", "status=1 and pb_size_id={$src->pb_size_id}" );
		$_REQUEST['pb_size_id'] = $src->pb_size_id;
		$this->pb_paper_bases_list( 'scroll_to_list' );
	}
	// </Photo book paper base>
	
	/* box */
	function box_categories_list( $parameters='' )
	{
		$this->bigtabs->select_child( 'printbook2' );
		$this->menuleft->select_child( 'sizes_list' );
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/printbook2/smalltabs/list'), "?".$this->controller_name.".pb_sizes_list") );
		
		$this->method_name = 'box_categories_list';
		$xml = "<data>";
		$xml .= "<list>";
		
		$pb_size_id = intval( $_REQUEST['pb_size_id'] );
		
		$pb_size = new PB2Size($pb_size_id);
		$xml .= $pb_size->Xml();
		
		$t = new MysqlTable( 'pb2_categories' );
		parent::define_pages( $t->get_count("status=1 and folder='{$this->box_category_folder}'") );
		$t->find_all( "status=1 and folder='{$this->box_category_folder}'", 'position,id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$o = new PB2Category( $a['id'] );
			
			if( $o->position == 0 )
			{
				$tp = new MysqlTable('pb2_categories');
				$tp->fix_positions( '', "status=1 and folder='{$this->box_category_folder}'" );
				$o->Load( $a['id'] );
			}
			
			$xml .= $o->Xml();
		}
		
		$xml .= "<last_position>{$o->position}</last_position>";
		
		$xml .= "</list>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'box_categories_list', $xml );
	}
	
	function box_category_edit( $save_result="" )
	{
		$last_position = intval( $_REQUEST['last_position'] );
		$pb_size_id = intval( $_REQUEST['pb_size_id'] );
		
		$this->bigtabs->select_child( 'printbook2' );
		$this->menuleft->select_child( 'sizes_list' );
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/printbook2/smalltabs/list'), "?".$this->controller_name.".pb_sizes_list") );
		
		$this->method_name = 'box_edit';
		$xml = "<data>";
		
		$xml .= "<edit>";
		
		$pb_size = new PB2Size($pb_size_id);
		$xml .= $pb_size->Xml();
		
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= "<last_position>$last_position</last_position>";
		$xml .= "<pb_size_id>$pb_size_id</pb_size_id>";
		$o = new PB2Category( $_REQUEST['id'] );
		$xml .= $o->Xml();
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'box_category_edit', $xml );
	}
	
	function box_category_save()
	{
		$erx = "";
		
		$obj = new PB2Category( $_REQUEST['object']['id'] );
		$last_position = intval( $_REQUEST['last_position'] );
		if( $last_position > 0 ) $_REQUEST['object']['position'] = $last_position + 1;
		$_REQUEST['object']['folder'] = $this->box_category_folder;
		
		$attachment = new Attachment();
		if( $attachment->upload('image') )
		{
			$_REQUEST['object']['image'] = $attachment->filename;
			$io = new AImage( SITEROOT . '/'. $_ENV['data_path'] .'/'. $attachment->filename );
			$io->CopyResize( SITEROOT . '/'. $_ENV['data_path'] .'/thumbnail-'. $attachment->filename, 200, 200 );
		}
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->box_category_edit( 'true' );
		}
		else
		{
			$this->box_category_edit( 'false' );
		}
	}
	
	function box_category_del()
	{
		$o = new PB2Category( $_REQUEST['id'] );
		$o->Delete();
		$this->box_categories_list( 'scroll_to_list' );
	}
	
	function box_category_del_image()
	{
		$o = new PB2Category( $_REQUEST['id'] );
		$o->DeleteImage();
		$this->box_category_edit( 'true' );
	}
	
	function box_categories_del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new PB2Category( $id );
			$o->Delete();
		}
		$this->box_categories_list( 'scroll_to_list' );
	}
	
	function box_categories_swap_position()
	{
		$src = new PB2Category( $_REQUEST['src'] );
		$dst = new PB2Category( $_REQUEST['dst'] );
		$this->SwapPosition( $src, $dst, "pb2_categories", "status=1 and folder='{$this->box_category_folder}'" );
		$this->box_categories_list( 'scroll_to_list' );
	}
	
	function boxes_list( $parameters='' )
	{
		$this->bigtabs->select_child( 'printbook2' );
		$this->menuleft->select_child( 'sizes_list' );
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/printbook2/smalltabs/list'), "?".$this->controller_name.".pb_sizes_list") );
		
		$this->method_name = 'boxes_list';
		$xml = "<data>";
		$xml .= "<list>";
		
		$pb_size_id = intval( $_REQUEST['pb_size_id'] );
		$category_id = intval( $_REQUEST['pb_category_id'] );
		
		$pb_size = new PB2Size($pb_size_id);
		$xml .= $pb_size->Xml();
		
		$pb_category = new PB2Category($category_id);
		$xml .= $pb_category->Xml();
		
		$t = new MysqlTable( 'pb2_boxes' );
		parent::define_pages( $t->get_count("status=1 and pb_size_id={$pb_size_id} and category_id={$category_id}") );
		$t->find_all( "status=1 and pb_size_id={$pb_size_id} and category_id={$category_id}", 'position,id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$o = new PB2Box( $a['id'] );
			
			if( $o->position == 0 )
			{
				$tp = new MysqlTable('pb2_boxes');
				$tp->fix_positions( '', "status=1 and pb_size_id={$pb_size_id}" );
				$o->Load( $a['id'] );
			}
			
			$xml .= $o->Xml();
		}
		
		$xml .= "<last_position>{$o->position}</last_position>";
		
		$xml .= "</list>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'boxes_list', $xml );
	}
	
	function box_edit( $save_result="" )
	{
		$o = new PB2Box( $_REQUEST['id'] );
		
		$last_position = intval( $_REQUEST['last_position'] );
		$pb_size_id = ($o->pb_size_id > 0)? $o->pb_size_id : intval( $_REQUEST['pb_size_id'] );
		$category_id = ($o->category_id > 0)? $o->category_id : intval( $_REQUEST['pb_category_id'] );
		
		$this->bigtabs->select_child( 'printbook2' );
		$this->menuleft->select_child( 'sizes_list' );
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/printbook2/smalltabs/list'), "?".$this->controller_name.".pb_sizes_list") );
		
		$this->method_name = 'box_edit';
		$xml = "<data>";
		
		$xml .= "<edit>";
		
		$pb_size = new PB2Size($pb_size_id);
		$xml .= $pb_size->Xml();
		
		$pb_category = new PB2Category($category_id);
		$xml .= $pb_category->Xml();
		
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= "<last_position>$last_position</last_position>";
		$xml .= "<pb_size_id>$pb_size_id</pb_size_id>";
		$xml .= "<pb_category_id>$category_id</pb_category_id>";
		$xml .= $o->Xml();
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'box_edit', $xml );
	}
	
	function box_save()
	{
		$erx = "";
		
		$obj = new PB2Box( $_REQUEST['object']['id'] );
		$last_position = intval( $_REQUEST['last_position'] );
		if( $last_position > 0 ) $_REQUEST['object']['position'] = $last_position + 1;
		
		$_REQUEST['pb_size_id'] = ( $obj->pb_size_id>0 )? $obj->pb_size_id : intval($_REQUEST['pb_size_id']);
		$_REQUEST['pb_category_id'] = ( $obj->pb_category_id>0 )? $obj->pb_category_id : intval($_REQUEST['pb_category_id']);
		
		$_REQUEST['object']['category_id'] = $_REQUEST['pb_category_id'];
		
		$attachment = new Attachment();
		if( $attachment->upload('image') )
		{
			$_REQUEST['object']['image'] = $attachment->filename;
			$io = new AImage( SITEROOT . '/'. $_ENV['data_path'] .'/'. $attachment->filename );
			$io->CopyResize( SITEROOT . '/'. $_ENV['data_path'] .'/thumbnail-'. $attachment->filename, 200, 200 );
		}
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->box_edit( 'true' );
		}
		else
		{
			$this->box_edit( 'false' );
		}
	}
	
	function box_del()
	{
		$o = new PB2Box( $_REQUEST['id'] );
		$o->Delete();
		$_REQUEST['pb_size_id'] = $o->pb_size_id;
		$_REQUEST['pb_category_id'] = $o->category_id;
		$this->boxes_list( 'scroll_to_list' );
	}
	
	function boxes_del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new PB2Box( $id );
			$o->Delete();
		}
		$_REQUEST['pb_size_id'] = $o->pb_size_id;
		$_REQUEST['pb_category_id'] = $o->category_id;
		$this->boxes_list( 'scroll_to_list' );
	}
	
	function box_del_image()
	{
		$o = new PB2Box( $_REQUEST['id'] );
		$o->DeleteImage();
		$this->box_edit( 'true' );
	}
	
	function boxes_swap_position()
	{
		$src = new PB2Box( $_REQUEST['src'] );
		$dst = new PB2Box( $_REQUEST['dst'] );
		$this->SwapPosition( $src, $dst, "pb2_boxes", "status=1 and pb_size_id={$src->pb_size_id}" );
		$_REQUEST['pb_size_id'] = $src->pb_size_id;
		$_REQUEST['pb_category_id'] = $src->category_id;
		$this->boxes_list( 'scroll_to_list' );
	}
	/* box */
	
	/* design works */
	function design_work_categories_list( $parameters='' )
	{
		$this->bigtabs->select_child( 'printbook2' );
		$this->menuleft->select_child( 'sizes_list' );
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/printbook2/smalltabs/list'), "?".$this->controller_name.".pb_sizes_list") );
		
		$this->method_name = 'design_work_categories_list';
		$xml = "<data>";
		$xml .= "<list>";
		
		$pb_size_id = intval( $_REQUEST['pb_size_id'] );
		
		$pb_size = new PB2Size($pb_size_id);
		$xml .= $pb_size->Xml();
		
		$t = new MysqlTable( 'pb2_categories' );
		parent::define_pages( $t->get_count("status=1 and folder='{$this->design_work_category_folder}'") );
		$t->find_all( "status=1 and folder='{$this->design_work_category_folder}'", 'position,id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$o = new PB2Category( $a['id'] );
			
			if( $o->position == 0 )
			{
				$tp = new MysqlTable('pb2_categories');
				$tp->fix_positions( '', "status=1 and folder='{$this->design_work_category_folder}'" );
				$o->Load( $a['id'] );
			}
			
			$xml .= $o->Xml();
		}
		
		$xml .= "<last_position>{$o->position}</last_position>";
		
		$xml .= "</list>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'design_work_categories_list', $xml );
	}
	
	function design_work_category_edit( $save_result="" )
	{
		$last_position = intval( $_REQUEST['last_position'] );
		$pb_size_id = intval( $_REQUEST['pb_size_id'] );
		
		$this->bigtabs->select_child( 'printbook2' );
		$this->menuleft->select_child( 'sizes_list' );
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/printbook2/smalltabs/list'), "?".$this->controller_name.".pb_sizes_list") );
		
		$this->method_name = 'design_work_category_edit';
		$xml = "<data>";
		
		$xml .= "<edit>";
		
		$pb_size = new PB2Size($pb_size_id);
		$xml .= $pb_size->Xml();
		
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= "<last_position>$last_position</last_position>";
		$xml .= "<pb_size_id>$pb_size_id</pb_size_id>";
		$o = new PB2Category( $_REQUEST['id'] );
		$xml .= $o->Xml();
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'design_work_category_edit', $xml );
	}
	
	function design_work_category_save()
	{
		$erx = "";
		
		$obj = new PB2Category( $_REQUEST['object']['id'] );
		$last_position = intval( $_REQUEST['last_position'] );
		if( $last_position > 0 ) $_REQUEST['object']['position'] = $last_position + 1;
		$_REQUEST['object']['folder'] = $this->design_work_category_folder;
		
		$attachment = new Attachment();
		if( $attachment->upload('image') )
		{
			$_REQUEST['object']['image'] = $attachment->filename;
			$io = new AImage( SITEROOT . '/'. $_ENV['data_path'] .'/'. $attachment->filename );
			$io->CopyResize( SITEROOT . '/'. $_ENV['data_path'] .'/thumbnail-'. $attachment->filename, 200, 200 );
		}
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->design_work_category_edit( 'true' );
		}
		else
		{
			$this->design_work_category_edit( 'false' );
		}
	}
	
	function design_work_category_del()
	{
		$o = new PB2Category( $_REQUEST['id'] );
		$o->Delete();
		$this->design_work_categories_list( 'scroll_to_list' );
	}
	
	function design_work_category_del_image()
	{
		$o = new PB2Category( $_REQUEST['id'] );
		$o->DeleteImage();
		$this->design_work_category_edit( 'true' );
	}
	
	function design_work_categories_del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new PB2Category( $id );
			$o->Delete();
		}
		$this->design_work_categories_list( 'scroll_to_list' );
	}
	
	function design_work_categories_swap_position()
	{
		$src = new PB2Category( $_REQUEST['src'] );
		$dst = new PB2Category( $_REQUEST['dst'] );
		$this->SwapPosition( $src, $dst, "pb2_categories", "status=1 and folder='{$this->design_work_category_folder}'" );
		$this->design_work_categories_list( 'scroll_to_list' );
	}
	
	function design_works_list( $parameters='' )
	{
		$this->bigtabs->select_child( 'printbook2' );
		$this->menuleft->select_child( 'sizes_list' );
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/printbook2/smalltabs/list'), "?".$this->controller_name.".pb_sizes_list") );
		
		$this->method_name = 'design_works_list';
		$xml = "<data>";
		$xml .= "<list>";
		
		$pb_size_id = intval( $_REQUEST['pb_size_id'] );
		$category_id = intval( $_REQUEST['pb_category_id'] );
		
		$pb_size = new PB2Size($pb_size_id);
		$xml .= $pb_size->Xml();
		
		$pb_category = new PB2Category($category_id);
		$xml .= $pb_category->Xml();
		
		$t = new MysqlTable( 'pb2_design_works' );
		parent::define_pages( $t->get_count("status=1 and pb_size_id={$pb_size_id} and category_id={$category_id}") );
		$t->find_all( "status=1 and pb_size_id={$pb_size_id} and category_id={$category_id}", 'position,id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$o = new PB2DesignWork( $a['id'] );
			
			if( $o->position == 0 )
			{
				$tp = new MysqlTable('pb2_design_works');
				$tp->fix_positions( '', "status=1 and pb_size_id={$pb_size_id}" );
				$o->Load( $a['id'] );
			}
			
			$xml .= $o->Xml();
		}
		
		$xml .= "<last_position>{$o->position}</last_position>";
		
		$xml .= "</list>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'design_works_list', $xml );
	}
	
	function design_work_edit( $save_result="" )
	{
		$last_position = intval( $_REQUEST['last_position'] );
		$pb_size_id = intval( $_REQUEST['pb_size_id'] );
		
		$o = new PB2DesignWork( $_REQUEST['id'] );
		$category_id = ($o->category_id > 0)? $o->category_id : intval( $_REQUEST['pb_category_id'] );
		
		$this->bigtabs->select_child( 'printbook2' );
		$this->menuleft->select_child( 'sizes_list' );
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/printbook2/smalltabs/list'), "?".$this->controller_name.".pb_sizes_list") );
		
		$this->method_name = 'design_work_edit';
		$xml = "<data>";
		
		$xml .= "<edit>";
		
		$pb_size = new PB2Size($pb_size_id);
		$xml .= $pb_size->Xml();
		
		$pb_category = new PB2Category($category_id);
		$xml .= $pb_category->Xml();
		
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= "<last_position>$last_position</last_position>";
		$xml .= "<pb_size_id>$pb_size_id</pb_size_id>";
		$xml .= $o->Xml();
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'design_work_edit', $xml );
	}
	
	function design_work_save()
	{
		$erx = "";
		
		$obj = new PB2DesignWork( $_REQUEST['object']['id'] );
		$last_position = intval( $_REQUEST['last_position'] );
		if( $last_position > 0 ) $_REQUEST['object']['position'] = $last_position + 1;
		
		$_REQUEST['pb_size_id'] = ( $obj->pb_size_id>0 )? $obj->pb_size_id : intval($_REQUEST['pb_size_id']);
		$_REQUEST['pb_category_id'] = ( $obj->pb_category_id>0 )? $obj->pb_category_id : intval($_REQUEST['pb_category_id']);
		
		$_REQUEST['object']['category_id'] = $_REQUEST['pb_category_id'];
		
		$attachment = new Attachment();
		if( $attachment->upload('image') )
		{
			$_REQUEST['object']['image'] = $attachment->filename;
			$io = new AImage( SITEROOT . '/'. $_ENV['data_path'] .'/'. $attachment->filename );
			$io->CopyResize( SITEROOT . '/'. $_ENV['data_path'] .'/thumbnail-'. $attachment->filename, 200, 200 );
		}
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->design_work_edit( 'true' );
		}
		else
		{
			$this->design_work_edit( 'false' );
		}
	}
	
	function design_work_del()
	{
		$o = new PB2DesignWork( $_REQUEST['id'] );
		$o->Delete();
		$_REQUEST['pb_size_id'] = $o->pb_size_id;
		$_REQUEST['pb_category_id'] = $o->category_id;
		$this->design_works_list( 'scroll_to_list' );
	}
	
	function design_works_del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new PB2DesignWork( $id );
			$o->Delete();
		}
		$_REQUEST['pb_size_id'] = $o->pb_size_id;
		$_REQUEST['pb_category_id'] = $o->category_id;
		$this->design_works_list( 'scroll_to_list' );
	}
	
	function design_work_del_image()
	{
		$o = new PB2DesignWork( $_REQUEST['id'] );
		$o->DeleteImage();
		$this->design_work_edit( 'true' );
	}
	
	function design_works_swap_position()
	{
		$src = new PB2DesignWork( $_REQUEST['src'] );
		$dst = new PB2DesignWork( $_REQUEST['dst'] );
		$this->SwapPosition( $src, $dst, "pb2_design_works", "status=1 and pb_size_id={$src->pb_size_id}" );
		$_REQUEST['pb_size_id'] = $src->pb_size_id;
		$_REQUEST['pb_category_id'] = $src->category_id;
		$this->design_works_list( 'scroll_to_list' );
	}
	/* design works */
	
	// <Settings>
	function settings_edit( $save_result="" )
	{
		$this->bigtabs->select_child( 'printbook2' );
		$this->menuleft->select_child( 'settings' );
		
		$this->method_name = 'settings_edit';
		$xml = "<data>";
		
		$xml .= "<edit>";
		$xml .= "<save_result>$save_result</save_result>";
		$o = new PB2Settings();
		$xml .= $o->Xml();
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'settings_edit', $xml );
	}
	
	function settings_save()
	{
		$erx = "";
		
		$obj = new PB2Settings();
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->settings_edit( 'true' );
		}
		else
		{
			$this->settings_edit( 'false' );
		}
	}
	// </Settings>

} 	

?>
<?php
/*****************************************************
 Class v.1.0, 2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

include_once( 'mediabox_controller.php' );

class mediabox_product_controller extends mediabox_controller
{
    function mediabox_product_controller()
    {
        parent::base_controller();
    }
    
    function save()
    {
        $erx = "";
        
        $obj = new Image( $_REQUEST['image']['id'] );
        
        $attachment = new Attachment();
        if( $attachment->upload('filename') )
        {
            $obj->DeleteImage();
            $_REQUEST['image']['filename'] = $attachment->filename;
        }
        
        if( $attachment->upload('small') )
        {
            $obj->DeleteSmall();
            $_REQUEST['image']['small'] = $attachment->filename;
        }
        else
        {
            if( $_REQUEST['image']['filename'] ) $filename = $_REQUEST['image']['filename'];
            if( $filename )
            {
                $obj->DeleteSmall();
                $io = new AImage( SITEROOT . '/'. $_ENV['data_path'] .'/'. $filename );
                $w = 250; $h = 200;
                if( $io->CopyFitIn( SITEROOT . '/'. $_ENV['data_path'] .'/small-'. $filename, $w, $h, Array(255,255,255) ) )
                {
                    $_REQUEST['image']['small'] = 'small-'. $filename;
                }
            }
        }
        
        if( $attachment->upload('tiny') )
        {
            $obj->DeleteTiny();
            $_REQUEST['image']['tiny'] = $attachment->filename;
        }
        else
        {
            $w = 180; $h = 124;
            if( $_REQUEST['image']['filename'] ) $filename = $_REQUEST['image']['filename'];
            if( $filename )
            {
                $obj->DeleteTiny();
                $io = new AImage( SITEROOT . '/'. $_ENV['data_path'] .'/'. $filename );
                if( $io->CopyFitIn( SITEROOT . '/'. $_ENV['data_path'] .'/tiny-'. $filename, $w, $h, Array(255,255,255) ) )
                {
                    $_REQUEST['image']['tiny'] = 'tiny-'. $filename;
                }
            }
        }
        
        if( $attachment->upload('thumbnail') )
        {
            $obj->DeleteThumbnail();
            $_REQUEST['image']['thumbnail'] = $attachment->filename;
        }
        else
        {
            if( $_REQUEST['image']['small'] ) $filename = $_REQUEST['image']['small'];
            if( $_REQUEST['image']['filename'] ) $filename = $_REQUEST['image']['filename'];
            if( $filename )
            {
                $obj->DeleteThumbnail();
                $io = new AImage( SITEROOT . '/'. $_ENV['data_path'] .'/'. $filename );
                if( $io->CopyResize( SITEROOT . '/'. $_ENV['data_path'] .'/thumbnail-'. $filename, 100, 90 ) )
                {
                    $_REQUEST['image']['thumbnail'] = 'thumbnail-'. $filename;
                }
            }
        }
        
        if( $obj->Save( $_REQUEST['image'] ) )
        {
            $_REQUEST['id'] = $obj->id;
            $this->edit( "<reloader reload='yes' close='no'></reloader>" );
        }
        else
        {
            $this->edit();
        }
    }
}

?>
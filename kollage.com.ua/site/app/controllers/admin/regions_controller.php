<?php
/*****************************************************
 Class v.1.0, 02.2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

include_once( 'base_controller.php' );

class regions_controller extends base_controller
{
	var $folder;
	
	function regions_controller()
	{
		parent::base_controller();
		
		$this->bigtabs->select_child( 'settings' );
		$this->InitSettingsMenu();
	}
	
	function index( $parameters='' )
	{
		$this->menuleft->select_child( "regions" );
		
		$this->method_name = 'list';
		$xml = "<data>";
		$xml .= "<regions>";
		
		$t = new MysqlTable( 'regions' );
		parent::define_pages( $t->get_count() );
		$t->find_all( "", 'position,id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$region = new Region( $a['id'] );
			
			if( $region->position == 0 )
			{
				$tp = new MysqlTable('regions');
				$tp->fix_positions();
				$region->Load( $a['id'] );
			}
			
			$xml .= $region->Xml();
		}
		
		if( !$region ) $region = new Region();
		$xml .= "<last_position>". $region->GetLastPosition() ."</last_position>";
		
		$xml .= "</regions>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'list', $xml );
	}
	
	function edit( $save_result="" )
	{
		$last_position = intval( $_REQUEST['last_position'] );
		
		$this->menuleft->select_child( "regions" );
		
		$this->method_name = 'edit';
		$xml = "<data>";
		
		$xml .= "<region_edit>";
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= "<last_position>$last_position</last_position>";
		$region = new Region( $_REQUEST['id'] );
		if( !$region->IsLoaded() )
		{
			$region->folder = $this->folder;
		}
		$xml .= $region->Xml();
		$xml .= "</region_edit>";
		
		$xml .= "</data>";
		
		$this->display( 'edit', $xml );
	}
	
	function save()
	{
		$erx = "";
		
		$obj = new Region( $_REQUEST['object']['id'] );
		$last_position = intval( $_REQUEST['last_position'] );
		if( $last_position > 0 ) $_REQUEST['object']['position'] = $last_position + 1;
		
		$attachment = new Attachment();
		if( $attachment->upload('image') )
		{
			$_REQUEST['object']['image'] = $attachment->filename;
			$ai = new AImage( SITEROOT.'/'.$_ENV['data_path'].'/'.$attachment->filename );
			$_REQUEST['object']['image_width'] = $ai->GetWidth();
			$_REQUEST['object']['image_height'] = $ai->GetHeight();
		}
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->edit( 'true' );
		}
		else
		{
			$this->edit( 'false' );
		}
	}
	
	function del()
	{
		$this->delete();
	}
	
	function delete()
	{
		$region = new Region( $_REQUEST['id'] );
		$region->Delete();
		$this->index();
	}
	
	function del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new Region( $id );
			$o->Delete();
		}
		$this->index( 'scroll_to_list' );
	}
	
	function del_image()
	{
		$region = new Region( $_REQUEST['id'] );
		$region->DeleteImage();
		$this->edit( 'true' );
	}
	
	function swap_position()
	{
		$src = new Region( $_REQUEST['src'] );
		$src_position = $src->position;
		$dst = new Region( $_REQUEST['dst'] );
		$src->position = $dst->position;
		$src->Save();
		$dst->position = $src_position;
		$dst->Save();
		$this->index( 'scroll_to_list' );
	}
}

?>
<?php
/*****************************************************
 Class v.1.0, 02.2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

include_once( 'base_controller.php' );

class news_controller extends base_controller
{
    var $folder = 'news';
    
    function news_controller()
    {
        parent::base_controller();
        
        $this->menuleft->add_item( new WEMenuItem( "news", $this->dictionary->get('/locale/news/menuleft/news'), "?".$this->controller_name.".index" ) );
    }
    
    function index( $parameters='' )
    {
        $this->bigtabs->select_child( 'news' );
        $this->menuleft->select_child( 'news' );
        
        $this->method_name = 'list';
        $xml = "<data>";
        $xml .= "<articles>";
        
        $t = new MysqlTable( 'articles' );
        parent::define_pages( $t->get_count('category_id=2') );
        $t->find_all( 'category_id=2', 'position,id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
        foreach( $t->data as $a )
        {
            $article = new Article( $a['id'] );
            
            if( $article->position == 0 )
            {
                $tp = new MysqlTable('articles');
                $tp->fix_positions( '', "category_id=2" );
                $article->Load( $a['id'] );
            }
            
            $xml .= $article->Xml();
        }
        
        if( !$article ) $article = new Article();
        $xml .= "<last_position>". $article->GetLastPosition("category_id=2") ."</last_position>";
        
        $xml .= "</articles>";
        $xml .= "<parameters>$parameters</parameters>";
        $xml .= "</data>";
        $this->display( 'list', $xml );
    }
    
    function edit( $save_result="" )
    {
        $this->bigtabs->select_child( 'news' );
        $this->menuleft->select_child( 'news' );
        
        $this->method_name = 'edit';
        $xml = "<data>";
        
        $xml .= "<article_edit>";
        $xml .= "<save_result>$save_result</save_result>";
        $article = new Article( $_REQUEST['id'] );
        $article->category_id = 2; //constant value
        $xml .= $article->Xml();
        $xml .= "</article_edit>";
        
        $xml .= "</data>";
        
        $this->display( 'edit', $xml );
    }
    
    function save()
    {
        $erx = "";
        
        $obj = new Article( $_REQUEST['article']['id'] );
        
        $attachment = new Attachment();
        if( $attachment->upload('image') )
        {
            $_REQUEST['article']['image'] = $attachment->filename;
        }
        
        $_REQUEST['article']['ingress'] = DeleteFontInformation( $_REQUEST['article']['ingress'] );
        $_REQUEST['article']['text'] = DeleteFontInformation( $_REQUEST['article']['text'] );
        if( $obj->Save( $_REQUEST['article'] ) )
        {
            $_REQUEST['id'] = $obj->id;
            $this->edit( 'true' );
        }
        else
        {
            $this->edit( 'false' );
        }
    }
    
    function del()
    {
        $this->delete();
    }
    
    function delete()
    {
        $article = new Article( $_REQUEST['id'] );
        $article->Delete();
        $this->index();
    }
    
    function del_marked()
    {
        $ids_str = $_REQUEST['ids'];
        $ids = split( ',', $ids_str );
        foreach( $ids as $id )
        {
            $o = new Article( $id );
            $o->Delete();
        }
        $this->index( 'scroll_to_list' );
    }
    
    function del_image()
    {
        $article = new Article( $_REQUEST['id'] );
        $article->DeleteImage();
        $this->edit( 'true' );
    }
    
    function del_media()
    {
        $image = new Image( $_REQUEST['image_id'] );
        $image->Delete();
        $this->edit( 'true' );
    }
    
    function move_up()
    {
        $t = new MysqlTable('articles');
        $t->move_position_up( $_REQUEST['id'], '', "category_id=2" );
        $this->index( 'scroll_to_list' );
    }
    
    function move_down()
    {
        $t = new MysqlTable('articles');
        $t->move_position_down( $_REQUEST['id'], '', "category_id=2" );
        $this->index( 'scroll_to_list' );
    }
    
    function move_up_gallery()
    {
        $t = new MysqlTable('images');
        $article = new Article( $_REQUEST['id'] );
        $t->move_position_up( $_REQUEST['image_id'], $article->folder_for_child, "parent_id={$_REQUEST['id']}" );
        $this->edit( 'scroll_to_media' );
    }
    
    function move_down_gallery()
    {
        $t = new MysqlTable('images');
        $article = new Article( $_REQUEST['id'] );
        $t->move_position_down( $_REQUEST['image_id'], $article->folder_for_child, "parent_id={$_REQUEST['id']}" );
        $this->edit( 'scroll_to_media' );
    }
}

?>
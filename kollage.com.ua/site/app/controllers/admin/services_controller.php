<?php
/*****************************************************
 Class v.1.0, 02.2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

include_once('base_controller.php');

class services_controller extends base_controller
{
	var $folder = 'services';
	
	function services_controller()
	{
		parent::base_controller();
		
		$this->make_small_tabs( $this->find_first_category() );
		$this->make_menu();
	}
	
	function make_small_tabs( $category_id, $page=1 )
	{
		$this->smalltabs->del_all();
		$this->smalltabs->add_item( new WETab('list', $this->dictionary->get('/locale/services/smalltabs/list'), "?".$this->controller_name.".index&category_id={$category_id}&page={$page}") );
		$this->smalltabs->add_item( new WETab('edit', $this->dictionary->get('/locale/services/smalltabs/edit'), "?".$this->controller_name.".category_edit&category_id={$category_id}") );
	}
	
	function make_menu()
	{
		$this->menuleft->del_all();
		$t = new MysqlTable( 'categories' );
		$t->find_all( "folder='{$this->folder}'", "position,id" );
		foreach( $t->data as $row )
		{
			$o = new Category( $row['id'] );
			$this->menuleft->add_item( new WEMenuItem( "c{$o->id}", $o->name, "?".$this->controller_name.".index&category_id={$o->id}" ) );
		}
		$this->menuleft->add_item( new WEMenuSeparator() );
		$this->menuleft->add_item( new WEMenuItem( "service_anounces_right", $this->dictionary->get('/locale/services/menuleft/service_anounces_right'), "?".$this->controller_name.".service_anounces_right_edit" ) );
		$this->menuleft->add_item( new WEMenuItem( "prices", $this->dictionary->get('/locale/services/menuleft/prices'), "?".$this->controller_name.".prices_list" ) );
	}
	
	function make_elements()
	{
		$retval = "";
		
		$retval .= $this->bigtabs->xml();
		$retval .= $this->smalltabs->xml();
		$retval .= $this->menuleft->xml();
		
		$retval .= "<left_panel show='yes'><categories>";
		$t = new MysqlTable( 'categories' );
		$t->find_all( "folder='{$this->folder}'", "position,id" );
		foreach( $t->data as $row )
		{
			$o = new Category( $row['id'] );
			$retval .= $o->Xml();
		}
		$retval .= "</categories></left_panel>";
		
		return( $retval );
	}
	
	function index( $parameters='' )
	{
		$category_id = intval( $_REQUEST['category_id'] );
		if( $category_id==0 ) $category_id = $this->find_first_category();
		
		$t = new MysqlTable( 'articles' );
		parent::define_pages( $t->get_count("category_id={$category_id} and folder='{$this->folder}'") );
		
		$this->make_small_tabs( $category_id, $this->page );
		$this->make_menu();
		$this->bigtabs->select_child( 'services' );
		$this->smalltabs->select_child( 'list' );
		$this->menuleft->select_child( "c{$category_id}" );
		
		$this->method_name = 'list';
		$xml = "<data>";
		$xml .= "<articles category_id='{$category_id}'>";
		$t->find_all( "category_id={$category_id} and folder='{$this->folder}'", 'position,id', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
		foreach( $t->data as $a )
		{
			$article = new Article( $a['id'], false );
			
			if( $article->position == 0 )
			{
				$tp = new MysqlTable('articles');
				$tp->fix_positions( '', "category_id={$category_id} and folder='{$this->folder}'" );
				$article->Load( $a['id'] );
			}
			
			$xml .= $article->Xml();
		}
		
		if( !$article ) $article = new Article();
		$xml .= "<last_position>". $article->GetLastPosition("category_id={$category_id} and folder='{$this->folder}'") ."</last_position>";
		
		$xml .= "</articles>";
		$xml .= "<parameters>$parameters</parameters>";
		$xml .= "</data>";
		$this->display( 'list', $xml );
	}
	
	function set_status()
	{
		$id = intval( $_REQUEST['id'] );
		$v = intval( $_REQUEST['v'] );
		$o = new Article( $id );
		if( $o->IsLoaded() )
		{
			$o->status = $v;
			$o->Save();
		}
		
		$this->index();
	}
	
	function set_show_in_menu()
	{
		$id = intval( $_REQUEST['id'] );
		$v = intval( $_REQUEST['v'] );
		$o = new Article( $id );
		if( $o->IsLoaded() )
		{
			$o->show_in_menu = $v;
			$o->Save();
		}
		
		$this->index();
	}
	
	function find_first_category()
	{
		$retval = 0;
		
		$t = new MysqlTable( 'categories' );
		if( $row = $t->find_first("folder='{$this->folder}'","position,id") )
		{
			$retval = intval( $row['id'] );
		}
		
		return( $retval );
	}
	
	function category_add()
	{
		$o = new Category();
		$o->folder = $this->folder;
		$o->name = $_REQUEST['name'];
		$o->Save();
		$this->index();
	}
	
	function category_del()
	{
		$o = new Category( $_REQUEST['category_id'] );
		$o->Delete();
		$_REQUEST['category_id'] = $this->find_first_category();
		$this->index();
	}
	
	function category_edit( $save_result="" )
	{
		$category_id = intval( $_REQUEST['category_id'] );
		if( $category_id==0 ) $category_id = $this->find_first_category();
		
		$this->make_small_tabs( $category_id );
		$this->make_menu();
		$this->bigtabs->select_child( 'services' );
		$this->smalltabs->select_child( 'edit' );
		$this->menuleft->select_child( "c{$category_id}" );
		
		$this->method_name = 'category_edit';
		$xml = "<data>";
		
		$xml .= "<edit>";
		$xml .= "<save_result>$save_result</save_result>";
		$o = new Category( $_REQUEST['category_id'] );
		$xml .= $o->Xml();
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'category_edit', $xml );
	}
	
	function category_save()
	{
		$erx = "";
		
		$obj = new Category( $_REQUEST['object']['id'] );
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->category_edit( 'true' );
		}
		else
		{
			$this->category_edit( 'false' );
		}
	}
	
	function category_del_media()
	{
		$image = new Image( $_REQUEST['image_id'] );
		$image->Delete();
		$this->category_edit( 'true' );
	}
	
	function category_move_up_gallery()
	{
		$t = new MysqlTable('images');
		$category = new Category( $_REQUEST['id'] );
		$t->move_position_up( $_REQUEST['image_id'], $category->folder_for_child, "parent_id={$_REQUEST['id']}" );
		$_REQUEST['category_id'] = $_REQUEST['id'];
		$this->category_edit( 'scroll_to_media' );
	}
	
	function category_move_down_gallery()
	{
		$t = new MysqlTable('images');
		$category = new Category( $_REQUEST['id'] );
		$t->move_position_down( $_REQUEST['image_id'], $category->folder_for_child, "parent_id={$_REQUEST['id']}" );
		$_REQUEST['category_id'] = $_REQUEST['id'];
		$this->category_edit( 'scroll_to_media' );
	}
	
	function edit( $save_result="" )
	{
		$category_id = intval( $_REQUEST['category_id'] );
		$last_position = intval( $_REQUEST['last_position'] );
		if( $category_id==0 ) $category_id = $this->find_first_category();
		
		$this->make_small_tabs( $category_id );
		$this->bigtabs->select_child( 'services' );
		$this->smalltabs->select_child( 'list' );
		$this->menuleft->select_child( "c{$category_id}" );
		
		$this->method_name = 'edit';
		$xml = "<data>";
		
		$xml .= "<article_edit>";
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= "<last_position>$last_position</last_position>";
		$article = new Article( $_REQUEST['id'] );
		if( !$article->IsLoaded() )
		{
			$article->category_id = intval($_REQUEST['category_id']);
			$article->folder = $this->folder;
		}
		$xml .= $article->Xml();
		$xml .= "</article_edit>";
		
		$xml .= "</data>";
		
		$this->display( 'edit', $xml );
	}
	
	function save()
	{
		$erx = "";
		
		$obj = new Article( $_REQUEST['article']['id'] );
		$last_position = intval( $_REQUEST['last_position'] );
		if( $last_position > 0 ) $_REQUEST['article']['position'] = $last_position + 1;
		
		$attachment = new Attachment();
		if( $attachment->upload('image') )
		{
			$_REQUEST['article']['image'] = $attachment->filename;
		}

		if ($_REQUEST['article']['client_types']) {
			$client_types = '';
			foreach($_REQUEST['article']['client_types'] as $client_type) {
				$client_types .= $client_type . ';';
			}
			$_REQUEST['article']['client_types'] = $client_types;
		} else {
			$_REQUEST['article']['client_types'] = '';
		}
		
		$_REQUEST['article']['ingress'] = DeleteHTMLComments( $_REQUEST['article']['ingress'] );
		$_REQUEST['article']['text'] = DeleteHTMLComments( $_REQUEST['article']['text'] );
		if( $obj->Save( $_REQUEST['article'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->edit( 'true' );
		}
		else
		{
			$this->edit( 'false' );
		}
	}
	
	function service_anounces_right_edit( $save_result="" )
	{
		$category_id = intval( $_REQUEST['category_id'] );
		$last_position = intval( $_REQUEST['last_position'] );
		if( $category_id==0 ) $category_id = $this->find_first_category();
		
		$this->make_small_tabs( $category_id );
		$this->bigtabs->select_child( 'services' );
		$this->menuleft->select_child( "service_anounces_right" );
		$this->smalltabs->del_all();
		
		$this->method_name = 'edit';
		$xml = "<data>";
		
		$xml .= "<article_edit>";
		$xml .= "<save_result>$save_result</save_result>";
		$xml .= "<last_position>$last_position</last_position>";
		$article = new Article( 'service_anounces_right' );
		$xml .= $article->Xml();
		$xml .= "</article_edit>";
		
		$xml .= "</data>";
		
		$this->display( 'service_anounces_right', $xml );
	}
	
	function service_anounces_right_move_up_gallery()
	{
		$t = new MysqlTable('images');
		$article = new Article( $_REQUEST['id'] );
		$t->move_position_up( $_REQUEST['image_id'], $article->folder_for_child, "parent_id={$_REQUEST['id']}" );
		$this->service_anounces_right_edit( 'scroll_to_media' );
	}
	
	function service_anounces_right_move_down_gallery()
	{
		$t = new MysqlTable('images');
		$article = new Article( $_REQUEST['id'] );
		$t->move_position_down( $_REQUEST['image_id'], $article->folder_for_child, "parent_id={$_REQUEST['id']}" );
		$this->service_anounces_right_edit( 'scroll_to_media' );
	}
	
	function service_anounces_right_del_media()
	{
		$image = new Image( $_REQUEST['image_id'] );
		$image->Delete();
		$this->service_anounces_right_edit( 'true' );
	}
	
	function service_anounces_right_save()
	{
		$erx = "";
		$this->service_anounces_right_edit( 'true' );
	}
	
	function del()
	{
		$this->delete();
	}
	
	function delete()
	{
		$article = new Article( $_REQUEST['id'] );
		$article->Delete();
		$this->index();

	}
	
	function del_marked()
	{
		$ids_str = $_REQUEST['ids'];
		$ids = split( ',', $ids_str );
		foreach( $ids as $id )
		{
			$o = new Article( $id );
			$o->Delete();
		}
		$this->index( 'scroll_to_list' );
	}
	
	function del_image()
	{
		$article = new Article( $_REQUEST['id'] );
		$article->DeleteImage();
		$this->edit( 'true' );
	}
	
	function del_media()
	{
		$image = new Image( $_REQUEST['image_id'] );
		$image->Delete();
		$this->edit( 'true' );
	}
	
	function move_up()
	{
		$category_id = intval( $_REQUEST['category_id'] );
		$t = new MysqlTable('articles');
		$t->move_position_up( $_REQUEST['id'], '', "category_id=$category_id" );
		$this->index( 'scroll_to_list' );
	}
	
	function move_down()
	{
		$category_id = intval( $_REQUEST['category_id'] );
		$t = new MysqlTable('articles');
		$t->move_position_down( $_REQUEST['id'], '', "category_id=$category_id" );
		$this->index( 'scroll_to_list' );
	}
	
	function swap_position()
	{
		$src = new Article( $_REQUEST['src'] );
		$dst = new Article( $_REQUEST['dst'] );
		$this->SwapPosition( $src, $dst, "articles", "category_id={$dst->category_id} and folder='{$this->folder}'" );
		$this->index( 'scroll_to_list' );
	}
	
	function move_up_gallery()
	{
		$t = new MysqlTable('images');
		$article = new Article( $_REQUEST['id'] );
		$t->move_position_up( $_REQUEST['image_id'], $article->folder_for_child, "parent_id={$_REQUEST['id']}" );
		$this->edit( 'scroll_to_media' );
	}
	
	function move_down_gallery()
	{
		$t = new MysqlTable('images');
		$article = new Article( $_REQUEST['id'] );
		$t->move_position_down( $_REQUEST['image_id'], $article->folder_for_child, "parent_id={$_REQUEST['id']}" );
		$this->edit( 'scroll_to_media' );
	}
	
	// <Prices>
	function prices_list( $save_result="" )
	{
		$this->bigtabs->select_child( 'services' );
		$this->menuleft->select_child( 'prices' );
		$this->make_small_tabs();
		$this->smalltabs->del_all();
		
		$this->method_name = 'prices_list';
		$xml = "<data>";
		
		$xml .= "<edit>";
		$xml .= "<save_result>$save_result</save_result>";
		$o = new Settings();
		$xml .= $o->Xml();
		$xml .= "</edit>";
		
		$xml .= "</data>";
		
		$this->display( 'prices_list', $xml );
	}
	
	function prices_save()
	{
		$erx = "";
		
		$obj = new Settings();
		
		$attachment = new Attachment();
		if( $attachment->upload('file_price') )
		{
			$obj->DeleteServiceFilePrice();
			$_REQUEST['object']['service_file_price'] = $attachment->filename;
		}
		
		$attachment = new Attachment();
		if( $attachment->upload('file_price_opt') )
		{
			$obj->DeleteServiceFilePriceOpt();
			$_REQUEST['object']['service_file_price_opt'] = $attachment->filename;
		}
		
		$attachment = new Attachment();
		if( $attachment->upload('file_price_opt2') )
		{
			$obj->DeleteServiceFilePriceOpt2();
			$_REQUEST['object']['service_file_price_opt2'] = $attachment->filename;
		}
		
		if( $obj->Save( $_REQUEST['object'] ) )
		{
			$_REQUEST['id'] = $obj->id;
			$this->prices_list( 'true' );
		}
		else
		{
			$this->prices_list( 'false' );
		}
	}
	
	function price_del_file_price()
	{
		$obj = new Settings();
		$obj->DeleteServiceFilePrice();
		$this->prices_list( 'true' );
	}
	
	function price_del_file_price_opt()
	{
		$obj = new Settings();
		$obj->DeleteServiceFilePriceOpt();
		$this->prices_list( 'true' );
	}
	
	function price_del_file_price_opt2()
	{
		$obj = new Settings();
		$obj->DeleteServiceFilePriceOpt2();
		$this->prices_list( 'true' );
	}
	// </Prices>
	
}

?>
<?php
define('__ROOT__', dirname(dirname(dirname(__FILE__))) );
include_once( 'base_controller.php' );
include( __ROOT__ .'/lib/LiqPay.php');

class photos_controller extends base_controller
{
	var $folder = "photos";
	var $order;
	var $first_order_limit = 100;
	var $platform = 'desktop';
	
	const liqpay_public = 'i2842182438';
	const liqpay_private = 'CvZKy585e9RJOwwhm3lmSWjtpElCC1LiBTLrd1EeRP';
	
	function show()
	{
		$this->order();
	}
	
	function make_leftmenu( $category_id=0, $item_id=0 )
	{
		if( $category_id > 0 ) {
			$o = new Category( $category_id );
			$this->leftmenu->add_item( new WEMenu( "submenu". $o->id, $o->name ) );
			$this->leftmenu->select_child( "submenu". $o->id );
			$menu = $this->leftmenu->get_item( "submenu". $o->id );
			
			if( $menu )
			{
				$t2 = new MysqlTable('articles');
				$t2->find_all( "folder='{$o->folder}' and category_id={$o->id} and status=1 and show_in_menu=1", "position,id" );
				foreach( $t2->data as $row2 )
				{
					$a = new Article( $row2['id'] );
					//$menu->add_item( new WEMenuItem( "item". $a->id, $a->header, "/photos/article/{$a->id}" ) );
					$translite = url_cyrillic_translite( $a->header );
					$menu->add_item( new WEMenuItem( "item". $a->id, $a->header, "/fotografii/{$translite}/sa{$a->id}" ) );
					if( $item_id == $a->id ) $menu->select_child( "item". $a->id );
				}
			}
			
			$submenu_second = new WEMenu( "submenu_second", $this->dictionary->get('/locale/common/topmenu/photos/sop') );
			$submenu_second->add_item( new WEMenuItem( "itemOrder", $this->dictionary->get('/locale/common/topmenu/photos/order'), "/photos/order" ) );
			$this->leftmenu->add_item( $submenu_second );
		}
	}
	
	function show_article()
	{
		$this->method_name = 'show_article';
		
		$id = intval( $_REQUEST['id'] );
		$article = new Article( $id );
		$category_id = $article->category_id;
		$this->make_leftmenu( $category_id, $id );
		
		$xml = "<data>";
			$category = new Category( $category_id ); $xml .= $category->Xml( true );
			if( $article->status == 1 ) $xml .= $article->Xml(true);
		$xml .= "</data>";
		
		$this->display( 'show_article', $xml );
	}
	
	function _prepare_order()
	{
		global $log;
		
		$GLOBALS['_RESULT'] = Array();
		
		if( !$this->order->IsLoaded() )
		{
			$this->order->client_id = ($this->client->id > 0)? $this->client->id : 0;
			$this->order->platform = $this->platform;
			$this->order->Save();
			$this->store();
		}
		else if( $this->client->id>0 && $this->order->client_id!=$this->client->id ) {
			$this->order->client_id = $this->client->id;
			$this->order->platform = $this->platform;
			$this->order->Save();
			$this->store();
		}
		
		if( $this->order->IsLoaded() )
		{
			$t = new MysqlTable('sop_order_photos');
			$this->rows_per_page = 8;
			parent::define_pages( $t->get_count( "order_id=". $this->order->id ) );
			$this->order->LoadPhotos();
			$this->order->CalculatePrices();
			
			//Page parameters
			$GLOBALS['_RESULT']['page'] = $this->page;
			$GLOBALS['_RESULT']['pages'] = $this->pages;
			$GLOBALS['_RESULT']['rows_per_page'] = $this->rows_per_page;
			
			//Order parameters
			$GLOBALS['_RESULT']['address'] = $this->order->address;
			$GLOBALS['_RESULT']['comments'] = $this->order->comments;
			$GLOBALS['_RESULT']['point_id'] = $this->order->point_id;
			$GLOBALS['_RESULT']['paymentway_id'] = $this->order->paymentway_id;
			$GLOBALS['_RESULT']['delivery_id'] = $this->order->delivery_id;
			$GLOBALS['_RESULT']['tax'] = $this->order->tax;
			$GLOBALS['_RESULT']['photo_paper'] = $this->order->photo_paper;
			$GLOBALS['_RESULT']['border'] = $this->order->border;
			$GLOBALS['_RESULT']['photos_count'] = $this->order->photos_count;
			$GLOBALS['_RESULT']['photos_price_amount'] = sprintf( "%01.2f", $this->order->photos_price_amount );
			$GLOBALS['_RESULT']['delivery'] = sprintf( "%01.2f", $this->order->GetDeliveryPrice() );
			$GLOBALS['_RESULT']['discount'] = $this->order->discount;
			$GLOBALS['_RESULT']['discount_price'] = sprintf( "%01.2f", $this->order->discount_price );
			$GLOBALS['_RESULT']['total_price'] = sprintf( "%01.2f", $this->order->total_price );
		}
	}
	
	//Step 1
	function order()
	{
		$this->method_name = 'order';
		$this->_prepare_order();
		
		$xml = "<data>";
		$xml .= $this->order->Xml();
		$xml .= $this->AdditionalData();
		
		$sop_settings = new SOPSettings();
		$xml .= "<category><page_title><![CDATA[". $sop_settings->page_title ."]]></page_title><page_keywords><![CDATA[". $sop_settings->page_keywords ."]]></page_keywords></category>";		
		
		$xml .= "</data>";
		$this->display( 'order', $xml );
	}
	
	//Step 2
	function order2()
	{
		$this->method_name = 'order2';
		
		if( $this->client->id > 0 )
		{
			$this->order->Save( $_REQUEST['object'] );
			$this->_prepare_order();
			
			$err = 0;
			
			if( $this->order->photos_count > 0 )
			{
				$xml = "<data>";
				$xml .= $this->order->Xml();
				$xml .= $this->AdditionalData();
				if( $this->order->photos_price_amount>$this->first_order_limit && $this->client->order_limit && $this->order->paymentway->code != 5 && $this->order->paymentway->code != 4 )
				{
					$xml .= "<msg><![CDATA[<br/>". $this->dictionary->get('/locale/photos/order/first_order_limit')  ." ({$this->first_order_limit}". $this->dictionary->get('/locale/common/currency') .")<br/><br/>]]></msg>";
					$err = 1;
				}
				if( $this->order->paymentway->code == 4 && $this->client->purse < $this->order->photos_price_amount )
				{
					$xml .= "<msg><![CDATA[<br/>". $this->dictionary->get('/locale/photos/order/purse_limit') ."<br/><br/>]]></msg>";
					$err = 1;
				}
				if( $this->order->delivery->order_prefix == 'C' && $this->order->point->order_prefix == 'NOPOINT' ) {
					$xml .= "<msg><![CDATA[<br/>". $this->dictionary->get('/locale/photos/order/didnt_select_point') ."<br/><br/>]]></msg>";
					$err = 1;
				}
				
				$xml .= "<err>{$err}</err>";
				$xml .= "</data>";
				$this->display( 'order2', $xml );
			}
			else
			{
				//$this->order();
				redirect( '/photos/order' );
			}
		}
		else
		{
			//$this->order();
			redirect( '/photos/order' );
		}
	}
	
	//Step 3
	function order3()
	{
		$this->method_name = 'order3';
		
		if( $this->client->id > 0 )
		{
			$this->order->Save( $_REQUEST['object'] );
			$this->_prepare_order();
			
			if( $this->order->photos_count > 0 )
			{
				//��� ������ ����� ������� � ������ ������ ������ ���, ����� ����������� ������ � ��.��������� - ����������� 15��� $this->client->order_limit
				//������ ������ "����������� �������" - ��������� ������� ����������� �����
				
				$xml = "<data>";
				$xml .= $this->order->Xml();
				$xml .= $this->AdditionalData();
				
				if( $this->order->photos_price_amount>$this->first_order_limit && $this->client->order_limit && $this->order->paymentway->code != 5 && $this->order->paymentway->code != 4 )
				{
					$xml .= "<msg>". $this->dictionary->get('/locale/photos/order/first_order_limit')  ." ({$this->first_order_limit}". $this->dictionary->get('/locale/common/currency') .")</msg>";
				}
				else
				{
					switch( $this->order->paymentway->code )
					{
						case 4:
							//��.�������
							if( $this->client->purse >= $this->order->photos_price_amount )
							{
								$this->pay_with_purse();
							}
							else
							{
								$xml .= "<msg>". $this->dictionary->get('/locale/photos/order/purse_limit') ."</msg>";
							}
							break;
						case 5:
							//�����.�����
							$this->pay_with_card();
							break;
						case 8:
							//�����.����� Privat (liqpay.com)
							$this->pay_with_card_privat();
							break;
						default:
							$this->save_order();
							exit(0);
							break;
					}
				}
				
				$xml .= "</data>";
				$this->display( 'order3', $xml );
			}
			else
			{
				//$this->order();
				redirect( '/photos/order' );
			}
		}
		else
		{
			//$this->order();
			redirect( '/photos/order' );
		}
	}
	
	function pay_with_card()
	{
		$host = $_SERVER['HTTP_HOST']; if( !$host ) $host = 'kollage.com.ua';
		$this->upc_pay( $this->order->order_number, $this->order->total_price, "sop|".$host );
		exit(0);
	}
	
	function pay_with_card_privat()
	{
		$host = $_SERVER['HTTP_HOST']; if( !$host ) $host = 'kollage.com.ua';
		$this->privat_pay( $this->order->order_number, $this->order->total_price, "sop|".$host );
		exit(0);
	}
	
	function pay_with_purse()
	{
		if( $this->client->purse >= $this->order->photos_price_amount )
		{
			$o = new PurseOperation();
			$o->client_id = $this->client->id;
			$o->amount = -1 * $this->order->total_price;
			$o->description = sprintf( "%s%s", $this->dictionary->get('/locale/photos/order/purse_description'), $this->order->order_number );
			$o->Save();
			$this->client->Load( $this->client->id );
			$this->order->payed = $this->order->total_price;
			$this->order->Save();
			$this->save_order();
		}
		exit(0);
	}
	
	function order_photos()
	{
		$this->method_name = 'order_photos';
		
		header("Expires: 0");
		header("Cache-Control: private");
		header("Pragma: no-cache");
		
		$JsHttpRequest = new JsHttpRequest("utf-8");
		$this->_prepare_order();
		
		$timestamp = time();
		$xml = "<data timestamp='{$timestamp}'>";
		$xml .= $this->order->Xml();
		$xml .= $this->AdditionalData();
		$xml .= "</data>";
		$this->display( 'order_photos', $xml );
	}
	
	function changegroup()
	{
		global $log, $db;
		
		set_time_limit(3600);
		header("Expires: 0");
		
		$JsHttpRequest = new JsHttpRequest("utf-8");
		
		//Apply parameters to the uploaded photos
		$group = $_REQUEST['group'];
		$quantity = intval( $_REQUEST['quantity'] ); if( $quantity == 0 ) $quantity = 1;
		$size_id = intval( $_REQUEST['size_id'] );
		if( $size_id == 0 )
		{
			$size_id = intval( $db->getone("select id from sop_order_photos_sizes order by position") );
		}
		
		if( $quantity>0 and $size_id>0 and $group == '' )
		{
			$size = new SOPOrderPhotoSize( $size_id );
			$sql = "update sop_order_photos set quantity={$quantity},size_id={$size_id},price=". $size->GetPrice($this->client->type_id) ." where order_id=". $this->order->id;
			$db->query( $sql );
		}
		
		if( $quantity>0 and $size_id>0 and $group != '' )
		{
			$size = new SOPOrderPhotoSize( $size_id );
			$sql = "update sop_order_photos set quantity={$quantity},size_id={$size_id},price=". $size->GetPrice($this->client->type_id) ." where id in ({$group}) and order_id=". $this->order->id;
			$db->query( $sql );
		}
		
		$this->_prepare_order();
		
		//$this->CkeckupOrderLimit();
		$GLOBALS['_RESULT']['err'] = '0';
		$GLOBALS['_RESULT']['msg'] = '';
		
		if( $msg ) echo $msg;
	}
	
	function add_photos()
	{
		global $log, $db;
		
		set_time_limit(3600);
		header("Expires: 0");
		
		$JsHttpRequest = new JsHttpRequest("utf-8");
		$this->_prepare_order();
		
		$order_photo_count = intval( $db->getone( "select count(id) from sop_order_photos where order_id={$this->order->id}" ) );
		if( $this->client->id==0 && $order_photo_count>=10 ) {
			$log->Write( "photos.add_photos_flash: Unauthorized user, don't allow upload more than 10photos" );
		}
		else {
			$last_photo_id = intval( $db->getone( "select id from sop_order_photos where order_id={$this->order->id} order by id desc" ) );
			
			$msg = "";
			$err = 0; $files = 0; $files_err = 0;
			$file_prefix = 'photo';
			$filetype = new AFileType();
			for( $i=1; $i<11; $i++ )
			{
				$filetype->SetFile( $_FILES[$file_prefix.$i]['tmp_name'] );
				if( $filetype->GetType()==FILE_JPG && $_FILES[$file_prefix.$i] && $_FILES[$file_prefix.$i]['tmp_name']!='' )
				{
					$files++;
					if( $this->order->UploadPhoto( $file_prefix.$i ) )
					{
						$msg = $this->dictionary->get('/locale/photos/order/add_photos/end_upload_msg');
					}
					else
					{
						$files_err++;
						$msg = $this->dictionary->get('/locale/photos/order/add_photos/error_upload_msg');
						$err = 1;
					}
					
					//$this->_prepare_order();
				}
				else if( $filetype->GetType()==FILE_ZIP && $_FILES[$file_prefix.$i] && $_FILES[$file_prefix.$i]['tmp_name']!='' )
				{
					$log->Write( "photos.add_photos: Determine ZIP" );
					$files++;
					if( $this->order->UploadZip( $file_prefix.$i ) )
					{
						$log->Write( "photos.add_photos: ZIP was unpacked" );
						$msg = $this->dictionary->get('/locale/photos/order/add_photos/end_upload_msg');
					}
					else
					{
						$files_err++;
						$msg = $this->dictionary->get('/locale/photos/order/add_photos/error_upload_msg');
						$err = 1;
					}
					
					//$this->_prepare_order();
				}
				else if( $filetype->GetType()==FILE_RAR && $_FILES[$file_prefix.$i] && $_FILES[$file_prefix.$i]['tmp_name']!='' )
				{
					$log->Write( "photos.add_photos: Determine RAR" );
					$files++;
					if( $this->order->UploadRar( $file_prefix.$i ) )
					{
						$log->Write( "photos.add_photos: RAR was unpacked" );
						$msg = $this->dictionary->get('/locale/photos/order/add_photos/end_upload_msg');
					}
					else
					{
						$files_err++;
						$msg = $this->dictionary->get('/locale/photos/order/add_photos/error_upload_msg');
						$err = 1;
					}
					
					//$this->_prepare_order();
				}
				else if( $_FILES[$file_prefix.$i] )
				{
					$log->Write( "photos.add_photos: Error. Client_id={$this->client->id} Order_id={$this->order->id} Filetype number=". $filetype->GetType() ." tmpname=". $_FILES[$file_prefix.$i]['tmp_name'] ." name=". $_FILES[$file_prefix.$i]['name'] ." ". serialize($_FILES) );
					$files++;
					$files_err++;
					$err = 2;
					$msg = $this->dictionary->get('/locale/photos/order/add_photos/error_upload_format');
				}
			}
		}
		
		if( $files == 0 )
		{
			$err = 3;
			$msg = $this->dictionary->get('/locale/photos/order/add_photos/error_upload_nofiles');
		}
		
		//Apply parameters to the uploaded photos
		$quantity = intval( $_REQUEST['quantity'] ); if( $quantity == 0 ) $quantity = 1;
		$size_id = intval( $_REQUEST['size_id'] );
		if( $size_id == 0 )
		{
			$size_id = intval( $db->getone("select id from sop_order_photos_sizes order by position") );
		}
		if( $quantity>0 and $size_id>0 )
		{
			$size_o = new SOPOrderPhotoSize( $size_id );
			$sql = "update sop_order_photos set quantity={$quantity},size_id={$size_id},price=". $size_o->GetPrice($this->client->type_id) ." where id>{$last_photo_id} and order_id=". $this->order->id;
			$db->query( $sql );
		}
		
		$this->_prepare_order();
		
		//$this->CkeckupOrderLimit();
		$GLOBALS['_RESULT']['err'] = $err;
		$GLOBALS['_RESULT']['msg'] = $msg;
		$GLOBALS['_RESULT']['files'] = $files;
		$GLOBALS['_RESULT']['files_err'] = $files_err;
		
		if( $msg ) echo $msg;
	}
	
	function add_photos_flash()
	{
		global $log, $db;
		
		set_time_limit(3600);
		header("Expires: 0");
		
		$this->_prepare_order();
		
		$order_photo_count = intval( $db->getone( "select count(id) from sop_order_photos where order_id={$this->order->id}" ) );
		if( $this->client->id==0 && $order_photo_count>=10 ) {
			$log->Write( "photos.add_photos_flash: Unauthorized user, don't allow upload more than 10photos" );
		}
		else {
			$last_photo_id = intval( $db->getone( "select id from sop_order_photos where order_id={$this->order->id} order by id desc" ) );
			
			$msg = "";
			$err = 0; $files = 0; $files_err = 0;
			$file_prefix = 'photo';
			$filetype = new AFileType();
			for( $i=1; $i<2; $i++ ) {
				$filetype->SetFile( $_FILES[$file_prefix.$i]['tmp_name'] );
				$log->Write( "photos.add_photos_flash: file type: ". $filetype->GetType() );
				if( $filetype->GetType()==FILE_JPG && $_FILES[$file_prefix.$i] && $_FILES[$file_prefix.$i]['tmp_name']!='' )
				{
					$files++;
					if( $this->order->UploadPhoto( $file_prefix.$i ) ) {
						$msg = $this->dictionary->get('/locale/photos/order/add_photos/end_upload_msg');
					}
					else {
						$files_err++;
						$msg = $this->dictionary->get('/locale/photos/order/add_photos/error_upload_msg');
						$err = 1;
					}
				}
				else if( $filetype->GetType()==FILE_ZIP && $_FILES[$file_prefix.$i] && $_FILES[$file_prefix.$i]['tmp_name']!='' )
				{
					$log->Write( "photos.add_photos_flash: Determine ZIP" );
					$files++;
					if( $this->order->UploadZip( $file_prefix.$i ) ) {
						$log->Write( "photos.add_photos_flash: ZIP was unpacked" );
						$msg = $this->dictionary->get('/locale/photos/order/add_photos/end_upload_msg');
					}
					else {
						$files_err++;
						$msg = $this->dictionary->get('/locale/photos/order/add_photos/error_upload_msg');
						$err = 1;
					}
				}
				else if( $filetype->GetType()==FILE_RAR && $_FILES[$file_prefix.$i] && $_FILES[$file_prefix.$i]['tmp_name']!='' )
				{
					$log->Write( "photos.add_photos_flash: Determine RAR" );
					$files++;
					if( $this->order->UploadRar( $file_prefix.$i ) ) {
						$log->Write( "photos.add_photos_flash: RAR was unpacked" );
						$msg = $this->dictionary->get('/locale/photos/order/add_photos/end_upload_msg');
					}
					else {
						$files_err++;
						$msg = $this->dictionary->get('/locale/photos/order/add_photos/error_upload_msg');
						$err = 1;
					}
				}
				else if( $_FILES[$file_prefix.$i] && $_FILES[$file_prefix.$i]['tmp_name']!='' && strcasecmp( GetFileExtension($_FILES[$file_prefix.$i]['name']), 'jpg' )==0 )
				{
					$log->Write( "photos.add_photos_flash: Added by EXT. Client_id={$this->client->id} Order_id={$this->order->id} Filetype=". $filetype->GetType() ." tmp_name=". $_FILES[$file_prefix.$i]['tmp_name'] ." name=". $_FILES[$file_prefix.$i]['name'] );
					$files++;
					$files_err++;
					$err = 2;
					$msg = $this->dictionary->get('/locale/photos/order/add_photos/error_upload_format');
				}
				else {
					$log->Write( "photos.add_photos_flash: Error. Client_id={$this->client->id} Order_id={$this->order->id} Filetype=". $filetype->GetType() ." tmp_name=". $_FILES[$file_prefix.$i]['tmp_name'] ." name=". $_FILES[$file_prefix.$i]['name'] ." ". serialize($_FILES) );
				}
			}
		}
		
		if( $files == 0 )
		{
			$err = 3;
			$msg = $this->dictionary->get('/locale/photos/order/add_photos/error_upload_nofiles');
		}
		
		//Apply parameters to the uploaded photos
		$quantity = intval( $_REQUEST['quantity'] ); if( $quantity == 0 ) $quantity = 1;
		$size_id = intval( $_REQUEST['size_id'] );
		if( $size_id == 0 )
		{
			$size_id = intval( $db->getone("select id from sop_order_photos_sizes order by position") );
		}
		if( $quantity>0 and $size_id>0 )
		{
			$size_o = new SOPOrderPhotoSize( $size_id );
			$sql = "update sop_order_photos set quantity={$quantity},size_id={$size_id},price=". $size_o->GetPrice($this->client->type_id) ." where id>{$last_photo_id} and order_id=". $this->order->id;
			$db->query( $sql );
		}
		
		$this->_prepare_order();
		if( $msg ) echo $msg;
	}
		
	function add_photos_fine_uploader()
	{
		global $log, $db;
		
		set_time_limit(3600);
		header("Expires: 0");
		header("Content-Type: application/json");
		
		$this->_prepare_order();
		
		$order_photo_count = intval( $db->getone( "select count(id) from sop_order_photos where order_id={$this->order->id}" ) );
		if( $this->client->id==0 && $order_photo_count>=10 ) {
			$log->Write( "photos.add_photos_fine_uploader: Unauthorized user, don't allow upload more than 10photos" );
		}
		else {
			$last_photo_id = intval( $db->getone( "select id from sop_order_photos where order_id={$this->order->id} order by id desc" ) );
			
			$file_prefix = 'qqfile';
			$msg = "";
			$err = 0; $files = 0; $files_err = 0;
			$filetype = new AFileType();
			$filename = $_REQUEST['qqfilename'];
			$filetype->SetFile($filename);
			$blob = $_REQUEST['qqfile'];
			$file_ext = GetFileExtension($filename);

			$log->Write( "photos.add_photos_fine_uploader ". json_encode($_REQUEST) . json_encode($_FILES) );
			
			if( (strcasecmp( $file_ext, 'jpg' )==0 || strcasecmp( $file_ext, 'jpeg' )==0) && $_FILES[$file_prefix]['tmp_name']!='' )
			{
				$log->Write( "photos.add_photos_fine_uploader: file [$filename], file_ext=[$file_ext] 1111" );
				$files++;
				if( $this->order->UploadPhoto( $file_prefix ) ) {
					$msg = $this->dictionary->get('/locale/photos/order/add_photos/end_upload_msg');
				}
				else {
					$files_err++;
					$msg = $this->dictionary->get('/locale/photos/order/add_photos/error_upload_msg');
					$err = 1;
				}
			}
			else if( strcasecmp( $file_ext, 'zip' )==0 && $_FILES[$file_prefix]['tmp_name']!='' )
			{
				$log->Write( "photos.add_photos_fine_uploader: Determine ZIP" );
				$files++;
				if( $this->order->UploadZip( $file_prefix ) ) {
					$log->Write( "photos.add_photos_fine_uploader: ZIP was unpacked" );
					$msg = $this->dictionary->get('/locale/photos/order/add_photos/end_upload_msg');
				}
				else {
					$files_err++;
					$msg = $this->dictionary->get('/locale/photos/order/add_photos/error_upload_msg');
					$err = 1;
				}
			}
			else if( strcasecmp( $file_ext, 'rar' )==0 && $_FILES[$file_prefix]['tmp_name']!='' )
			{
				$log->Write( "photos.add_photos_fine_uploader: Determine RAR" );
				$files++;
				if( $this->order->UploadRar( $file_prefix ) ) {
					$log->Write( "photos.add_photos_fine_uploader: RAR was unpacked" );
					$msg = $this->dictionary->get('/locale/photos/order/add_photos/end_upload_msg');
				}
				else {
					$files_err++;
					$msg = $this->dictionary->get('/locale/photos/order/add_photos/error_upload_msg');
					$err = 1;
				}
			}			
			else {
				$log->Write( "photos.add_photos_fine_uploader: Error. Client_id={$this->client->id} Order_id={$this->order->id} Filetype=". $filetype->GetType() ." name=". $filename ." file_ext=". $file_ext ." ". strcasecmp( $file_ext, 'jpg' ) );
			}
		}
		
		if( $files == 0 )
		{
			$err = 3;
			$msg = $this->dictionary->get('/locale/photos/order/add_photos/error_upload_nofiles');
		}
		
		//Apply parameters to the uploaded photos
		$quantity = intval( $_REQUEST['quantity'] ); if( $quantity == 0 ) $quantity = 1;
		$size_id = intval( $_REQUEST['size_id'] );
		if( $size_id == 0 )
		{
			$size_id = intval( $db->getone("select id from sop_order_photos_sizes order by position") );
		}
		if( $quantity>0 and $size_id>0 )
		{
			$size_o = new SOPOrderPhotoSize( $size_id );
			$sql = "update sop_order_photos set quantity={$quantity},size_id={$size_id},price=". $size_o->GetPrice($this->client->type_id) ." where id>{$last_photo_id} and order_id=". $this->order->id;
			$db->query( $sql );
		}
		
		$this->_prepare_order();

		$response = array(
			'success' => $err === 0,
		);
		if ($err) {
			$response['error'] = $msg;
		}
		echo json_encode($response);
	}
	
	function CkeckupOrderLimit()
	{
		$run_prepare = false;
		
		if( $this->order->IsLoaded() && $this->client->order_limit )
		{
			if( $this->order->photos_count > $this->first_order_limit )
			{
				$this->order->LoadPhotos();
				for( $i=count($this->order->photos)-1; $i>=0; $i-- )
				{
					if( $this->order->photos_count > $this->first_order_limit )
					{
						$q = $this->order->photos_count - $this->first_order_limit;
						echo "photos_count={$this->order->photos_count}; q=$q\n";
						if( ($this->order->photos[$i]->quantity - $q) > 1 )
						{
							$this->order->photos[$i]->quantity -= $q;
						}
						else
						{
							$this->order->photos[$i]->quantity = 1;
						}
						$this->order->photos[$i]->Save();
						$this->order->CalculatePrices();
						$run_prepare = true;
						echo $this->order->photos_count.'|';
					}
				}
				for( $i=count($this->order->photos)-1; $i>=0; $i-- )
				{
					if( $this->order->photos_count > $this->first_order_limit )
					{
						$this->order->photos[$i]->Delete();
						$this->order->CalculatePrices();
						$run_prepare = true;
					}
				}
			}
		}
		
		if( $run_prepare ) $this->_prepare_order();
	}
	
	function del_photos()
	{
		$JsHttpRequest = new JsHttpRequest("utf-8");
		
		$ids = split( ",", $_REQUEST['ids'] );
		foreach( $ids as $id )
		{
			$obj = new SOPOrderPhoto( $id );
			$obj->Delete();
		}
		
		$this->_prepare_order();
	}
	
	function update_photo_parameters() // update photo parameters: size_id, quantity
	{
		$JsHttpRequest = new JsHttpRequest("utf-8");
		
		$o = new SOPOrderPhoto( $_REQUEST['id'] );
		if( $o->IsLoaded() )
		{
			$size_id = intval( $_REQUEST['size_id'] );
			$quantity = intval( $_REQUEST['quantity'] );
			if( $size_id > 0 ) $o->size_id = $size_id;
			if( $quantity > 0 ) $o->quantity = $quantity;
			
			if( $o->Save() )
			{
				$o->price = $o->size->GetPrice( $this->client->type_id );
				$o->Save();
				
				$this->_prepare_order();
				//$this->CkeckupOrderLimit();
				$o->Load( $o->id );
				
				$GLOBALS['_RESULT']['id'] = $o->id;
				$GLOBALS['_RESULT']['size_id'] = $o->size_id;
				$GLOBALS['_RESULT']['prefix'] = $_REQUEST['prefix'];
				$GLOBALS['_RESULT']['quantity'] = $o->quantity;
				$GLOBALS['_RESULT']['price'] = $o->price;
			}
		}
	}
	
	function logout()
	{
		$this->order = null;
		parent::logout();
	}
	
	function restore()
	{
		parent::restore();
		$this->order = new SOPOrder( intval( $_SESSION['order_id'] ) );
	}
	
	function store()
	{
		$_SESSION['order_id'] = ( is_object($this->order) )? $this->order->id : 0;
		parent::store();
	}
	
	function AdditionalData()
	{
		$xml = "";
		
		$t = new MysqlTable('sop_deliveries');
		$t->find_all( "status=1", "position,id" );
		$xml .= "<deliveries>";
		foreach( $t->data as $row )
		{
			$o = new SOPDelivery( $row['id'] );
			$xml .= $o->Xml();
		}
		$xml .= "</deliveries>";
		
		$t = new MysqlTable('sop_paymentways');
		$t->find_all( "status=1", "position,id" );
		$xml .= "<paymentways>";
		foreach( $t->data as $row )
		{
			$o = new SOPPaymentway( $row['id'] );
			if( $this->order->IsLoaded() )
			{
				$o->description = str_replace( "{ORDER_NUMBER}", $this->order->order_number, $o->description );
			}
			$xml .= $o->Xml();
		}
		$xml .= "</paymentways>";
		
		$t = new MysqlTable('sop_points');
		$t->find_all( "status=1", "position,id" );
		$xml .= "<points>";
		foreach( $t->data as $row )
		{
			$o = new SOPPoint( $row['id'] );
			$xml .= $o->Xml();
		}
		$xml .= "</points>";
		
		$t = new MysqlTable('sop_order_photos_sizes');
		$t->find_all( "status=1", "position,id" );
		$xml .= "<photo_sizes>";
		foreach( $t->data as $row )
		{
			$o = new SOPOrderPhotoSize( $row['id'] );
			$xml .= $o->Xml();
		}
		$xml .= "</photo_sizes>";
		
		$previous_id = $this->GetPreviousOrder();
		$xml .= "<have_previous_order>". $previous_id ."</have_previous_order>";
		
		return( $xml );
	}
	
	function GetPreviousOrder()
	{
		global $db;
		
		$retval = 0;
		
		if( $this->client->IsLoaded() && $this->order->IsLoaded() )
		{
			//$sql = "select id from sop_orders where status=0 and client_id={$this->client->id} and year(created_at)=year(now()) and month(created_at)=month(now()) and day(created_at)=day(now()) order by created_at desc limit 1,1";
			$sql = "select id from sop_orders where status=0 and client_id={$this->client->id} and (date(created_at)=date(now()) or date(created_at)=subdate(date(now()),interval 1 day)) order by id desc limit 1,1";
			$previous_id = intval( $db->getone($sql) );
			if( $previous_id > 0 && $previous_id < $this->order->id )
			{
				$retval = $previous_id;
			}
		}
		
		return( $retval );
	}
	
	function goto_previous_order()
	{
		global $db;

		$previous_id = $this->GetPreviousOrder();
		if( $previous_id > 0)
		{
			$this->order = new SopOrder( $previous_id );
			$db->query( "update sop_orders set status=98 where status=0 and client_id={$this->client->id} and id>{$this->order->id}" );
			$this->store();
		}
		
		redirect( '/photos/order' );
	}
	
	function save_order( $status=1 )
	{
		global $log;

		$this->method_name = 'save_order';
		$this->_prepare_order();
		
		if( $this->client->id > 0 && $this->order->photos_count > 0 )
		{
			$this->_prepare_order();
			
			$xml = "<data>";
			$xml .= $this->order->Xml();
			$xml .= $this->AdditionalData();
			$xml .= "</data>";
			$this->display( 'order_result', $xml );
			
			if( $this->order->status == 0 )
			{
				$this->order->status = 1;
				$this->order->Save();
				
				//Send email to admin and client
				$settings = new Settings();
				$sop_settings = new SOPSettings();
				
				if( $this->client->email ) {
					$mail = new AMail( 'windows-1251' );
						$subject = $sop_settings->order_mail_subject;
						$subject = str_replace( "{ORDER_NR}", $this->order->order_number, $subject );
						$mail->subject = $subject;
						
						$mail->from_name = $settings->from_name;
						$mail->from_email = $settings->from_email;
						$mail->to_email = $this->client->email;
						$body = $sop_settings->order_mail_body;
						$body = str_replace( "{CLIENT}", $this->client->firstname.' '.$this->client->lastname, $body );
						$body = str_replace( "{ORDER_NR}", $this->order->order_number, $body );
						$body = str_replace( "{ORDER_STATUS}", $this->GetOrderStatusStr(), $body );
						//$body = str_replace( "{ORDER}", $this->GetOrderSummaryTable( $this->order ), $body );
						$body = str_replace( "{ORDER}", $this->GetOrderSummaryTableExtended( $this->order ), $body );
					$mail->Send( $body );
				}
				
				// Send SMS to client
				if( $client->phone ) {
					//$ostatus = $this->GetOrderStatusStr();
					//$this->SendSMS( $phone, "Kollage. Zakaz '{$this->order->order_number}' prinyat. {$this->sms_suffix}" );
				}
				
				$mail = new AMail( 'windows-1251' );
					$subject = $sop_settings->admin_order_mail_subject;
					$subject = str_replace( "{ORDER_NR}", $this->order->order_number, $subject );
					$mail->subject = $subject;
					
					$mail->from_name = $settings->from_name;
					$mail->from_email = $settings->from_email;
					$mail->to_email = $sop_settings->admin_emails;
					$body = $sop_settings->admin_order_mail_body;
					$body = str_replace( "{CLIENT}", $this->client->firstname.' '.$this->client->lastname, $body );
					$body = str_replace( "{ORDER_NR}", $this->order->order_number, $body );
					$body = str_replace( "{ORDER_STATUS}", $this->GetOrderStatusStr(), $body );
					//$body = str_replace( "{ORDER}", $this->GetOrderSummaryTable( $this->order ), $body );
					$body = str_replace( "{ORDER}", $this->GetOrderSummaryTableExtended( $this->order ), $body );
				$mail->Send( $body );
				
				//Move order files into tree structure of the folders
				$src_dir = $this->order->dir . $this->order->order_number .'/';
				$this->order->order_number = sprintf( "%s-%s-%s", $this->order->order_number, $this->order->delivery->order_prefix, $this->order->point->order_prefix );
				$border_prefix = '';
				if( $this->order->border==1 )
				{
					$border_prefix = '_frame';
					$this->order->order_number .= '-FRAME';
				}
				$this->order->Save();
				$md = new MysqlDateTime( $this->order->created_at );
				$dir = sprintf( "%s%s/%s", $this->order->dir, $md->GetFrontEndValue('y.m.d'), $this->order->order_number );
				mkdir_r( $dir, 0775 );
				$dir .= '/';
				$this->order->LoadPhotos();
				$i = 1;
				$files_success = 0;
				foreach( $this->order->photos as $p )
				{
					$sname = $p->size->name;
					$new_dir = $dir . $sname .'/'. $this->order->GetPaperName();
					mkdir_r( $new_dir );
					$new_dir .= '/';
					$new_name = sprintf( "%03d_%s_%s%s_%03d.%s", $i, $sname, $this->order->GetPaperPrefix(), $border_prefix, $p->quantity, get_file_ext($p->filename) );
					if( copy( $src_dir.$p->filename, $new_dir.$new_name ) )
					{
						chmod( $new_dir.$new_name, 0664 );
						$files_success++;
						$log->Write( "photos.save_order id={$this->order->id} number={$this->order->order_number} copied". ($p->filename) ." to ". ($new_dir.$new_name) );
					}
					else {
						$log->Write( "photos.save_order ERROR id={$this->order->id} number={$this->order->order_number} copying ". ($src_dir.$p->filename) ." to ". ($new_dir.$new_name) );
					}
					$i++;
				}
				if( $files_success > 0 && $files_success == count($this->order->photos) )
				{
					rmdir_rf( $src_dir );
				}
			}
			 
			$this->order = null;
			$this->store();
		}
		else
		{
			//$this->order();
			redirect( '/photos/order' );
		}
	}
	
	function GetOrderStatusStr()
	{
		$retval = '';
		
		if( $this->order->IsLoaded() )
		{
			switch( $this->order->status )
			{
				case 1:
					$retval = $this->dictionary->get('/locale/my/orders/photo/statuses/s1');
					break;
				case 2:
					$retval = $this->dictionary->get('/locale/my/orders/photo/statuses/s2');
					break;
				case 3:
					$retval = $this->dictionary->get('/locale/my/orders/photo/statuses/s3');
					break;
				case 4:
					$retval = $this->dictionary->get('/locale/my/orders/photo/statuses/s4');
					break;
				case 5:
					$retval = $this->dictionary->get('/locale/my/orders/photo/statuses/s5');
					break;
				case 6:
					$retval = $this->dictionary->get('/locale/my/orders/photo/statuses/s6');
					break;
			}
		}
		
		return( $retval );
	}
	
	function GetOrderSummaryTableExtended( &$order )
	{
		$retval = '';
		
		if( $order->IsLoaded() )
		{
			$retval .= '<table style="border: 1px solid #e3bf85" cellspacing="3">';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/order_number2') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->order_number .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/created_at') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->created_at .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/updated_at') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->updated_at .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/paymentway_id') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->paymentway->name .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/delivery_id') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->delivery->name .'</td>';
			$retval .= '</tr>';
			
			if( $order->delivery_id == 1 )
			{
				$retval .= '<tr>';
				$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/point_name') .'</b></td>';
				$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->point->name .', '. $order->point->address .','. $order->point->phone .'</td>';
				$retval .= '</tr>';
			}
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/comments') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;color:#d80c76">'. $order->comments .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">&#160;</td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">&#160;</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/client') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->client->firstname .'&#160;'. $order->client->lastname .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/email') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->client->email .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/address_v') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->client->city .', '. $order->client->street->name .', '. $order->client->address .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/address2_v') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;color:#d80c76">'. $order->address .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/phone') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->client->phone .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">&#160;</td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">&#160;</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/photos') .'</b></td>';
			//$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->photos_count . $this->dictionary->get('/locale/common/items_amount') .'</td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $this->GetOrderSummaryTablePhotos( $order ) .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/photo_paper') .'</b></td>';
			$photo_paper = ($order->photo_paper == 1)? $this->dictionary->get('/locale/photos/order/photo_paper1') : $this->dictionary->get('/locale/photos/order/photo_paper2');
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $photo_paper .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/border') .'</b></td>';
			$border = ($order->border == 1)? $this->dictionary->get('/locale/common/exist') : $this->dictionary->get('/locale/common/no');
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $border .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/total_photos') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->photos_price_amount .' '. $this->dictionary->get('/locale/common/currency') .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/total_delivery') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. sprintf( "%01.2f", $order->GetDeliveryPrice() ) .' '. $this->dictionary->get('/locale/common/currency') .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/total_discount') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. sprintf( "%01.2f", $order->discount_price ) .' '. $this->dictionary->get('/locale/common/currency') .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/total_price') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. sprintf( "%01.2f", $order->total_price ) .' '. $this->dictionary->get('/locale/common/currency') .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/payed_price') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. sprintf( "%01.2f", $order->payed ) .' '. $this->dictionary->get('/locale/common/currency') .'</td>';
			$retval .= '</tr>';
			
			$retval .= '</table>';
		}
		
		return( $retval );
	}
	
	function GetOrderSummaryTablePhotos( &$order )
	{
		global $db;

		$retval = '';
		
		if( $order->IsLoaded() )
		{
			$photos = Array();
			$sql = "select sum(p.quantity),s.name,s.id from sop_order_photos p left join sop_order_photos_sizes s on s.id=p.size_id where p.order_id={$order->id} group by p.size_id order by s.position";
			$rs = $db->query( $sql );
			while( $row = $rs->fetch_row() )
			{
				$retval .= $row[1] .' - '. $row[0] .' '. $this->dictionary->get('/locale/common/items_amount') .'<br>';
			}
		}
		
		return( $retval );
	}
	
	function GetOrderSummaryTable( $order )
	{
		$retval = '';
		
		if( $order->IsLoaded() )
		{
			$retval .= '<table>';
			$retval .= "<tr><th colspan='2'>". $this->dictionary->get('/locale/photos/order/order_number') ."{$order->order_number} ({$order->photos_count} ". $this->dictionary->get('/locale/common/items_amount') .")</th></tr>";
			
			$retval .= '<tr><th width="50%" valign="top" style="padding-left: 25px;">';
			$retval .= '<table cellpadding="0" cellspacing="0">';
			
			$retval .= '<tr>';
			$retval .= '<th align="left">'. $this->dictionary->get('/locale/photos/order/total_photos') .'</th>';
			$retval .= '<th align="left">'. $order->photos_price_amount .' '. $this->dictionary->get('/locale/common/currency') .'</th>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<th align="left">'. $this->dictionary->get('/locale/photos/order/total_delivery') .'</th>';
			$retval .= '<th align="left">'. sprintf( "%01.2f", $order->GetDeliveryPrice() ) .' '. $this->dictionary->get('/locale/common/currency') .'</th>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<th align="left">'. $this->dictionary->get('/locale/photos/order/total_discount') .'</th>';
			$retval .= '<th align="left">'. sprintf( "%01.2f", $order->discount_price ) .' '. $this->dictionary->get('/locale/common/discount_price') .'</th>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<th align="left">'. $this->dictionary->get('/locale/photos/order/total_price') .'</th>';
			$retval .= '<th align="left">'. sprintf( "%01.2f", $order->total_price ) .' '. $this->dictionary->get('/locale/common/total_price') .'</th>';
			$retval .= '</tr>';
			
			$retval .= '</table></th>';
			
			$retval .= '<td width="50%" valign="top">';
			$retval .= '</table></td>';
			
			$retval .= '</tr></table>';
		}
		
		return( $retval );
	}
	
	function update_order_parameters()
	{
		$this->_prepare_order();
		$JsHttpRequest = new JsHttpRequest("utf-8");
		
		if( $this->order->IsLoaded() )
		{
			$this->order->address = $_REQUEST['address'];
			$this->order->comments = $_REQUEST['comments'];
			$point_id = intval( $_REQUEST['point_id'] ); if( $point_id > 0 ) $this->order->point_id = $point_id;
			$paymentway_id = intval( $_REQUEST['paymentway_id'] ); if( $paymentway_id > 0 ) $this->order->paymentway_id = $paymentway_id;
			$delivery_id = intval( $_REQUEST['delivery_id'] ); if( $delivery_id > 0 ) $this->order->delivery_id = $delivery_id;
			$this->order->photo_paper = intval( $_REQUEST['photo_paper'] );
			$this->order->border = intval( $_REQUEST['border'] );
			
			if( $this->order->Save() )
			{
				$this->_prepare_order();
			}
		}
	}
	
	function microtime_float()
{
	list($usec, $sec) = explode(" ", microtime());
	return ((float)$usec + (float)$sec);
}
	
	function display_photo()
	{
		global $log;
		
		$f = SITEROOT.'/empty.gif';
		
		$ts = $this->microtime_float();
		
		if( $this->order && $this->order->IsLoaded() ) {
			$type = intval( $_REQUEST['type'] );
			$order_id = intval( $_REQUEST['order_id'] );
			$id = intval( $_REQUEST['id'] );
			//$order = new SOPOrder( $order_id ); //bad way
			$order = $this->order;
			$photo = new SOPOrderPhoto( $id );
			if( $photo->IsLoaded() )
			{
				switch( $type )
				{
					case 1:
						$ff = SITEROOT.'/'.$_ENV['sop_path'].'/'.$order->order_number.'/'.$photo->thumbnail;
						break;
					case 2:
						$ff = SITEROOT.'/'.$_ENV['sop_path'].'/'.$order->order_number.'/'.$photo->small;
						break;
					default:
						$ff = SITEROOT.'/'.$_ENV['sop_path'].'/'.$order->order_number.'/'.$photo->filename;
						break;
				}
				if( file_exists($ff) )
				{
					$f = $ff;
				}
			}
		}
		
		header( 'Content-type: image/jpeg' );
		//header( 'Content-Length: ' . filesize($f));
		ob_flush(); flush();
		readfile( $f );
		$te = $this->microtime_float();
		//$log->Write( "photos.display_photo: ts={$ts} te={$te} time: ". ($te-$ts) );
	}
	
	function upc_pay( $order_number, $total_amount, $action='sop|kollage.com.ua' )
	{
		global $db;
		
		//make signature
		//$MerchantID = '1752256';  //test-server
		$MerchantID = '6984785';
		//$TerminalID = 'E7880056';  //test-server
		$TerminalID = 'E0133767';
		$PurchaseTime = date("ymdHis");
		$OrderID = $order_number;
		$Currency = '980';
		$TotalAmount = round( $total_amount * 100 );
		$SD = $action;
		$data = "$MerchantID;$TerminalID;$PurchaseTime;$OrderID;$Currency;$TotalAmount;$SD;";
		//$fp = fopen( SITEROOT ."/upc/1752256.pem", "r" );  //test-server
		$fp = fopen( SITEROOT ."/upc/6984785.pem", "r" );
		if( $fp )
		{
			$priv_key = fread( $fp, 8192 );
			fclose( $fp );
			$pkeyid = openssl_pkey_get_private( $priv_key );
			openssl_sign( $data , $signature, $pkeyid );
			openssl_free_key( $pkeyid );
			$signature = base64_encode( $signature );
		}
		
		$xml = "<data>";
		$xml .= "<upc_MerchantID>{$MerchantID}</upc_MerchantID>";
		$xml .= "<upc_TerminalID>{$TerminalID}</upc_TerminalID>";
		$xml .= "<upc_TotalAmount>{$TotalAmount}</upc_TotalAmount>";
		$xml .= "<upc_Currency>{$Currency}</upc_Currency>";
		
		if( $AltTotalAmount )
		{
			$xml .= "<upc_AltTotalAmount>{$AltTotalAmount}</upc_AltTotalAmount>";
			$xml .= "<upc_AltCurrency>{$this->currency->code}</upc_AltCurrency>";
		}
		
		$xml .= "<upc_PurchaseTime>{$PurchaseTime}</upc_PurchaseTime>";
		$xml .= "<upc_OrderID>{$OrderID}</upc_OrderID>";
		$xml .= "<upc_SD>{$SD}</upc_SD>";
		$xml .= "<upc_Signature><![CDATA[{$signature}]]></upc_Signature>";
		$xml .= "</data>";
		
		$this->display( 'upc_pay', $xml );
	}
	
	function upc_success()
	{
		$this->make_leftmenu();
		$menu = $this->leftmenu->get_item( "submenu1" );
		$menu->select_child('item0');
		
		$this->save_order();
		$OrderID = $_REQUEST['OrderID'];
		
		$xml = "<data>";
		$xml .= "<OrderID>{$OrderID}</OrderID>";
		$xml .= "<xid><![CDATA[{$xid}]]></xid>";
		$xml .= "<signature_failed>0</signature_failed>";
		$xml .= "<message>". $this->GetUPCResultMessage(0) ."</message>";
		$xml .= "</data>";
		
		$this->display( 'upc_success', $xml );
	}
	
	function upc_success_already()
	{
		$this->make_leftmenu();
		$menu = $this->leftmenu->get_item( "submenu1" );
		$menu->select_child('item0');
		
		$OrderID = $_REQUEST['OrderID'];
		
		$xml = "<data>";
		$xml .= "<OrderID>{$OrderID}</OrderID>";
		$xml .= "<xid><![CDATA[{$xid}]]></xid>";
		$xml .= "<signature_failed>0</signature_failed>";
		$xml .= "<message>". $this->GetUPCResultMessage(0) ."</message>";
		$xml .= "</data>";
		
		$this->display( 'upc_success', $xml );
	}
	
	function upc_failure()
	{
		$this->make_leftmenu();
		$menu = $this->leftmenu->get_item( "submenu1" );
		$menu->select_child('item0');
		
		$o = new SOPOrder( $_REQUEST['order_id'] );
		
		$xml = "<data>";
		$xml .= "<OrderID>{$o->order_number}</OrderID>";
		$xml .= "<message>". $this->GetUPCResultMessage( $_REQUEST['tranCode'] ) ."</message>";
		$xml .= "</data>";
		
		$this->display( 'upc_failure', $xml );
	}
	
	function privat_pay( $order_number, $total_amount, $action='sop|kollage.com.ua' )
	{
		$OrderID = $order_number .'-'. time();
		$TotalAmount = round( $total_amount, 2 );
		$result_url = "http://kollage.com.ua/photos/privat_return?PHPSESSID=". session_id();
		$server_url = "http://kollage.com.ua/photos/privat_return_server?PHPSESSID=". session_id();

		$liqpay = new LiqPay(self::liqpay_public, self::liqpay_private);
		$html = $liqpay->cnb_form(array(
			'action'         => 'pay',
			'amount'         => $TotalAmount,
			'currency'       => 'UAH',
			'description'    => "PHOTO ORDER: $OrderID",
			'order_id'       => $OrderID,
			'result_url'     => $result_url,
			'server_url'     => $server_url,
			'version'        => '3'
		));
		
		$data = "<data><form><![CDATA[$html]]></form></data>";
		
		$this->display( 'privat_pay', $data );
	}
	
	function privat_return_server()
	{
		$private_key = self::liqpay_private;
		$liqpay = new LiqPay(self::liqpay_public, $private_key);
		global $log;
		
		$signature_received = $_REQUEST['signature'];
		$base64 = $_REQUEST['data'];
		$data = base64_decode( $base64 );
		$parameters = json_decode( $data );
  
		$signature_encoded = base64_encode( sha1( $private_key . $base64 . $private_key, 1 ) );
		
		$log->Write("SOP->privat_return_server REQUEST=". json_encode($_REQUEST) );
		
		if( $signature_received==$signature_encoded ) {
			$log->Write( "SOP->privat_return_server SIGNATURE VERIFICATION OK" );
			$OrderID = $parameters->order_id;
			$order = new SOPOrder();
            list($order_id_for_load) = split('-',$OrderID);
			$order->LoadByNumber( $order_id_for_load );
			if( $order->IsLoaded() && $order->status == 0 ) {
				$log->Write("SOP->privat_return_server order->". json_encode($order) );
				$order->payed = $order->total_price;
				$order->Save();
				$this->order = $order;
				$this->save_order();
			}
			else if( !$order->IsLoaded() ) {
				$log->Write( "SOP->privat_return_server order {$order_id_for_load} NOT FOUND!" );
			}
		}
		else {
			$log->Write("SOP->privat_return_server signatures are not equal");
		}
		
	}
	
	function privat_return() {
		redirect( '/' );
	}
	
	function get_upload_information()
	{
		$JsHttpRequest = new JsHttpRequest("utf-8");
		
		echo "<br>time=". time();
		echo "<br>session time=". $_SESSION['uploaded_file_time_start'];
		echo "<br>file=". $this->FindTmpPhpFile( $_SESSION['uploaded_file_time_start'] );
	}
	
	function init_upload_information()
	{
		$_SESSION['uploaded_file_time_start'] = time();
	}
	
	function FindTmpPhpFile( $time )
	{
		$retval = "";
		$dir = "/tmp/";
		
		if( $dh = opendir($dir) )
		{
			while( ($file = readdir($dh) ) !== false )
			{
				if( is_file($dir.$file) && substr($file,0,3) == "php" )
				{
					$stat = stat( $dir.$file );
					$retval .= sprintf( "atime=%s session=%s ", $stat['atime'], $time );
					if( intval($stat['atime']) >= intval($time) && intval($stat['mtime']) <= time() )
					{
						$retval .= $file;
					}
					//$retval .= sprintf( "a=%s m=%s c=%s", $stat['atime'], $stat['mtime'], $stat['ctime'] );
				}
			}
			
			closedir( $dh );
		}
		

		return( $retval );
	}
	
	function my_purse()
	{
		if( $this->client->IsLoaded() )
		{
			$this->method_name = 'my_purse';
			
			$this->make_leftmenu();
			
			$xml = "<data>";
			$xml .= "<list>";
			
			$t = new MysqlTable( 'purse_operations' );
			parent::define_pages( $t->get_count("client_id={$this->client->id}") );
			$t->find_all( "client_id={$this->client->id}", 'id desc', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
			foreach( $t->data as $a )
			{
				$o = new PurseOperation( $a['id'] );
				$xml .= $o->Xml();
			}
			
			$xml .= "</list>";
			$xml .= "</data>";
			
			$this->display( 'my_purse', $xml );
		}
		else
		{
			$this->show();
		}
	}
	
	function load_paymentways()
	{
		global $db;

		$this->method_name = 'load_paymentways';
		
		header("Expires: 0");
		header("Cache-Control: private");
		header("Pragma: no-cache");
		
		$JsHttpRequest = new JsHttpRequest("utf-8");
		$this->_prepare_order();
		
		$id = intval( $_REQUEST['delivery_id'] );
		echo $id;
		$o = new SOPDelivery( $id );
		if( $o->IsLoaded() )
		{
			$GLOBALS['_RESULT']['paymentways'] = Array();
			
			$sql = "select dp.paymentway_id from sop_deliveries_paymentways dp left join sop_paymentways pw on pw.id=dp.paymentway_id where dp.delivery_id={$id} order by pw.position,pw.id";
			echo $sql;
			$rs = $db->query( $sql );
			$i = 0;
			while( $row = $rs->fetch_row() )
			{
				$p = new SopPaymentway( $row[0] );
				$GLOBALS['_RESULT']['paymentways'][$i++] = Array( $p->id, $p->name );
			}
			
			$GLOBALS['_RESULT']['err'] = 0;
		}
		else
		{
			$GLOBALS['_RESULT']['err'] = 1;
		}
	}
	
	function photo_rotate()
	{
		global $log, $db;
		
		set_time_limit(3600);
		header("Expires: 0");
		
		$JsHttpRequest = new JsHttpRequest("utf-8");
		
		$msg = ""; $err = 0;
		
		if( $this->order->IsLoaded() )
		{
		
			$id = intval( $_REQUEST['id'] );
			$degree = intval( $_REQUEST['degree'] );
		
			$log->Write( "photo_rotate: id={$id} degree={$degree}" );
			
			$o = new SOPOrderPhoto( $id );
			if( $o->IsLoaded() )
			{
				$dir = SITEROOT .'/'. $_ENV['sop_path'] .'/'. $this->order->order_number ."/";
				
				$log->Write( "photo_rotate: filename=". $dir . $o->filename );
				
				$io = new AImage( $dir . $o->filename );
				$io->Rotate( $dir . $o->filename, $degree );
				$io->Close();
				
				$io = new AImage( $dir . $o->filename );
				$io->CopyResize( $dir . $o->thumbnail, 120, 90 );
				
				if( $w > $h ) { $width = 600; $height = 0; }
				else { $width = 0; $height = 600; }
				if( $io->CopyResize( $dir . $o->small, $width, $height ) )
				{
					$io->SetFilename( $dir . $o->small );
					$o->small_w = $io->GetWidth();
					$o->small_h = $io->GetHeight();
				}
				
				$o->Save();
				$io->Close();
				//$this->_prepare_order();
			}
		}
		
		$GLOBALS['_RESULT']['err'] = $err;
		$GLOBALS['_RESULT']['msg'] = $msg;
		
		if( $msg ) echo $msg;
	}
	
	function pe() {
		$xml = "<data>";
		$xml .= "</data>";
		
		$this->display( 'pe', $xml );
	}
}

?>
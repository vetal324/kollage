<?php
/*****************************************************
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

class base_controller {
	var $controller_name;
	var $method_name;
	var $topmenu, $leftmenu, $bottommenu;
	var $page, $pages, $rows_per_page;
	var $dictionary;
	var $client;
	var $currency;
	var $shop_trash, $fair_trash;
	var $log;
	
	var $kpi_state = "";
	var $kpi_data = Array();

   //var $admin_email = "vetal324@gmail.com";
   var $admin_email = "koshelenkovb@kollage.com.ua";

	var $sms_suffix = 'Esli vy ne vipolnyali etu operaciyu pozvonite (066) 336-61-23, (097) 639-97-87';
	
	function __construct()
	{
		global $log;
		$this->log = $log;
		
		preg_match( '/([\w\n]+)_/', get_class($this), $matches );
		$this->controller_name = $matches[1];
		
		$this->define_dictionary();
		$this->define_welements();
		
		$this->page = 1; $this->pages = 1; $this->rows_per_page = 25;
		
		if( array_key_exists( 'currency_id', $_REQUEST ) && $_REQUEST['currency_id'] )
		{
			change_currency( $_REQUEST['currency_id'] );
		}
		
		$this->restore();
	}
	
	protected function restore()
	{
		$this->client = new Client( intval( $_SESSION['client_id'] ) );
		
		$this->currency = new Currency( intval( $_SESSION['currency_id'] ) );
		$this->shop_trash = ( $_SESSION['shop_trash'] )? unserialize( $_SESSION['shop_trash'] ) : new ShopTrash();
		$this->fair_trash = ( $_SESSION['fair_trash'] )? unserialize( $_SESSION['fair_trash'] ) : new FairTrash();
	}
	
	protected function store()
	{
		$_SESSION['client_id'] = ( is_object($this->client) )? $this->client->id : 0;
		$_SESSION['currency_id'] = ( is_object($this->currency) )? $this->currency->id : 0;
		$_SESSION['shop_trash'] = ( is_object($this->shop_trash) )? serialize($this->shop_trash) : '';
		$_SESSION['fair_trash'] = ( is_object($this->fair_trash) )? serialize($this->fair_trash) : '';
	}
	
	protected function define_dictionary()
	{
		$dpath = sprintf('%s/%s/%s.xml', SITEROOT, $_ENV['app_path'] .'/'. $_ENV['views_path'], $_SESSION['lang'] );
		$this->dictionary = new Dictionary( $dpath, 'utf-8' );
	}
	
	protected function define_welements()
	{
		global $db;
		
		$this->topmenu = new WEMenu('topmenu');
		$this->leftmenu = new WEMenu('leftmenu');
		$this->bottommenu = new WEMenu('bottommenu');
		
		/* top menu */
		$tGeneral = new WEMenu( 'general', $this->dictionary->get('/locale/common/topmenu/general'), '/' );
		$this->topmenu->add_item( $tGeneral );

        //PhotoBooks
		$tPhotobooks = new WEMenu( 'photobooks', $this->dictionary->get('/locale/common/topmenu/photobooks/title'), '' );
		$tPhotobooksArticles = new WEMenu( "submenu1", $this->dictionary->get('/locale/common/topmenu/photobooks/articles') );
		$category_id = 18;
		$rs = $db->query( "select id from articles where show_in_menu=1 and category_id={$category_id} order by position,id" );
		while( $row = $db->getrow($rs) ) {
			$a = new Article( $row[0] );
			if( $a->IsLoaded() ) {
				$translite = url_cyrillic_translite( $a->header );
				$tPhotobooksArticles->add_item( new WEMenuItem( "item{$a->id}", $a->header, "/fotoknigi/{$translite}/sa{$a->id}" ) );
			}
		}
		$tPhotobooks->add_item( $tPhotobooksArticles );
		$this->topmenu->add_item( $tPhotobooks );

        $link_to_calc = "/fotoknigi/professionalam/sa289";
        $link_to_calc2 = "/fotoknigi/gotoviy_maket_zakazat_dizayn/sa382";
        $tPhotobooksOS = new WEMenu( "submenu2", $this->dictionary->get('/locale/common/topmenu/photos/sop') );
		$tPhotobooksOS->add_item( new WEMenuItem( "itemOrder", $this->dictionary->get('/locale/common/topmenu/photobooks/order'), $link_to_calc ) );
		$tPhotobooksOS->add_item( new WEMenuItem( "itemOrder3", $this->dictionary->get('/locale/common/topmenu/photobooks/order2'), $link_to_calc2 ) );
		$tPhotobooks->add_item( $tPhotobooksOS );
		
		//kollage photo edition
		
		$tPhotos = new WEMenu( 'photos', $this->dictionary->get('/locale/common/topmenu/photos/title'), "/photos/order" );
		$t = new MysqlTable('categories');
		$tFolder = 'photos';
		if( $row = $t->find_first("folder='$tFolder'") )
		{
			//$tPhotos->add_item( new WEMenuItem( "itemOrder", "Order", "/photos/order" ) );
			$tPhotosArticles = new WEMenu( "submenu1", $this->dictionary->get('/locale/common/topmenu/photos/articles') );
			$t2 = new MysqlTable('articles');
			$t2->find_all( "category_id={$row['id']} and folder='{$tFolder}' and status=1 and show_in_menu=1", 'position,id' );
			foreach( $t2->data as $rows2 )
			{
				$a = new Article( $rows2['id'] );
				if( $a->IsLoaded() )
				{
					//$tPhotosArticles->add_item( new WEMenuItem( "item{$a->id}", $a->header, "/photos/article/{$a->id}" ) );
					$translite = url_cyrillic_translite( $a->header );
					$tPhotosArticles->add_item( new WEMenuItem( "item{$a->id}", $a->header, "/fotografii/{$translite}/pa{$a->id}" ) );
				}
			}
			$tPhotos->add_item( $tPhotosArticles );
			
			$tPhotosOS = new WEMenu( "submenu2", $this->dictionary->get('/locale/common/topmenu/photos/sop') );
			$tPhotosOS->add_item( new WEMenuItem( "itemOrder", $this->dictionary->get('/locale/common/topmenu/photos/order'), "/photos/order" ) );
			$tPhotos->add_item( $tPhotosOS );
		}
		$this->topmenu->add_item( $tPhotos );
		
		$tVideo = new WEMenu( 'video', $this->dictionary->get('/locale/common/topmenu/video') );
		$category_id = 6;
		$rs = $db->query( "select id from articles where show_in_menu=1 and category_id={$category_id} order by position,id" );
		while( $row = $db->getrow($rs) ) {
			$a = new Article( $row[0] );
			if( $a->IsLoaded() )
			{
				//$tVideo->add_item( new WEMenuItem( "item{$a->id}", $a->header, "/services/article/{$category_id}/{$a->id}" ) );
				$translite = url_cyrillic_translite( $a->header );
				$tVideo->add_item( new WEMenuItem( "item{$a->id}", $a->header, "/video/{$translite}/sa{$a->id}" ) );
			}
		}
		$this->topmenu->add_item( $tVideo );
		
		$tSuvenir = new WEMenu( 'suvenir', $this->dictionary->get('/locale/common/topmenu/suvenir'), '/services/article/23/374' );
		$category_id = 23;
		$rs = $db->query( "select id from articles where show_in_menu=1 and category_id={$category_id} order by position,id" );
		while( $row = $db->getrow($rs) ) {
			$a = new Article( $row[0] );
			if( $a->IsLoaded() )
			{
				//$tSuvenir->add_item( new WEMenuItem( "item{$a->id}", $a->header, "/services/article/{$category_id}/{$a->id}" ) );
				$translite = url_cyrillic_translite( $a->header );
				$tSuvenir->add_item( new WEMenuItem( "item{$a->id}", $a->header, "/suveniri/{$translite}/sa{$a->id}" ) );
			}
		}
		$this->topmenu->add_item( $tSuvenir );
		
		$tPoligraphy = new WEMenu( 'poligraphy', $this->dictionary->get('/locale/common/topmenu/poligraphy/title'), '/services/article/24/57' );
		$tPoligraphyArticles = new WEMenu( "submenu1", $this->dictionary->get('/locale/common/topmenu/photobooks/articles') );
		$category_id = 24;
		$rs = $db->query( "select id from articles where show_in_menu=1 and category_id={$category_id} order by position,id" );
		while( $row = $db->getrow($rs) ) {
			$a = new Article( $row[0] );
			if( $a->IsLoaded() )
			{
				//$tPoligraphyArticles->add_item( new WEMenuItem( "item{$a->id}", $a->header, "/services/article/{$category_id}/{$a->id}" ) );
				$translite = url_cyrillic_translite( $a->header );
				$tPoligraphyArticles->add_item( new WEMenuItem( "item{$a->id}", $a->header, "/poligrafia/{$translite}/sa{$a->id}" ) );
			}
		}
		$tPoligraphy->add_item( $tPoligraphyArticles );
		$tPoligraphyOS = new WEMenu( "submenu2", $this->dictionary->get('/locale/common/topmenu/photos/sop') );
		$tPoligraphyOS->add_item( new WEMenuItem( "itemcardeditor", $this->dictionary->get('/locale/common/topmenu/poligraphy/cardeditor'), "http://www.business-card-editor.com/typo_name/kollage" ) );
		$tPoligraphy->add_item( $tPoligraphyOS );
		$this->topmenu->add_item( $tPoligraphy );
		
		$tShop = new WEMenu( 'shop', $this->dictionary->get('/locale/common/topmenu/shop'), '/shop/categories' );
		$t = new MysqlTable( 'shop_product_categories' );
		$t->find_all( "parent_id is null AND show_in_menu=1", "position,id" );
		foreach( $t->data as $row )
		{
			$o = new ShopCategory( $row['id'] );
			$tShopSubcategories = new WEMenu( "submenu{$o->id}", $o->name, "/shop/category/{$o->id}" );
			$t2 = new MysqlTable( 'shop_product_categories' );
			$t2->find_all( "parent_id={$o->id} AND show_in_menu=1", "position,id" );
			foreach( $t2->data as $row2 )
			{
				$o2 = new ShopCategory( $row2['id'] );
				if( $o2->ISLoaded() ) {
					$tShopSubcategories->add_item( new WEMenuItem( "item{$o2->id}", $o2->name, "/shop/category/{$o2->id}" ) );
				}
			}
			$tShop->add_item( $tShopSubcategories );
		}
		$this->topmenu->add_item( $tShop );
		
		$tContacts = new WEMenu( 'contacts', $this->dictionary->get('/locale/common/topmenu/contacts'), '/contacts' );
		$this->topmenu->add_item( $tContacts );
		/* top menu */
	}
	
	function define_pages( $rowscount ) {
		if( $_REQUEST['rows_per_page'] ) {
			$this->rows_per_page = intval($_REQUEST['rows_per_page']);
			$_SESSION[ $this->controller_name . $this->method_name . 'rows_per_page'] = $this->rows_per_page;
			setcookie( $this->controller_name . $this->method_name . 'rows_per_page', $this->rows_per_page, time()+60*60*24*30 );
		}
		else {
			if( $_SESSION[ $this->controller_name . $this->method_name . 'rows_per_page'] ) {
				$this->rows_per_page = $_SESSION[ $this->controller_name . $this->method_name . 'rows_per_page'];
			}
			else {
				if( $_COOKIE[ $this->controller_name . $this->method_name . 'rows_per_page' ] ) {
					$this->rows_per_page = $_COOKIE[ $this->controller_name . $this->method_name . 'rows_per_page' ];
				}
				else $this->rows_per_page = 8;
			}
		}
		
		$this->pages = ceil( $rowscount / $this->rows_per_page );
		
		$this->page = intval( $_REQUEST['page'] );
		if( $this->page<1 ) $this->page = 1;
		if( $this->page>$this->pages ) $this->page = $this->pages;
	}
	
	protected function display( $view, $xml = "" )
	{
		$this->display_php5( $view, $xml );
	}
	
	protected function get( $view, $xml = "" )
	{
		return( $this->get_php5( $view, $xml ) );
	}
	
	protected function display_php5_notworking( $view, $xml = "" )
	{
		$xsl = sprintf('%s/%s/%s.xsl', SITEROOT, $_ENV['app_path'] .'/'. $_ENV['views_path'] .'/'. $this->controller_name, $view );
		if( is_file($xsl) )
		{
			$xsl = realpath( $xsl );
			if( file_exists($xsl) ) {
				$xml_obj = new DOMDocument;
				$xml .= $this->make_elements();
				$xml = xml_header() .'<content><documents>'. $xml .'</documents>'. $this->xml_global_attributes() . '</content>';
				//echo $xml;
				
				$xml_obj->loadXML( $xml );
				
				$proc = new XSLTCache();
				$proc->importStyleSheet( $xsl ); 
				echo $proc->transformToXML( $xml_obj );
			}
			else {
				user_error( sprintf('Call to undefined template at %s', $xsl), E_USER_ERROR );
			}
		}
		else
		{
			user_error( sprintf('Call to undefined template at %s', $xsl), E_USER_ERROR );
		}
	}
	
	protected function get_php5_notworking( $view, $xml = "" )
	{
		$retval = '';
		
		$xsl = sprintf('%s/%s/%s.xsl', SITEROOT, $_ENV['app_path'] .'/'. $_ENV['views_path'] .'/'. $this->controller_name, $view );
		if( is_file($xsl) ) {
			$xsl = realpath( $xsl );
			
			$xml_obj = new DOMDocument;
			$xml .= $this->make_elements();
			$xml = xml_header() .'<content><documents>'. $xml .'</documents>'. $this->xml_global_attributes() . '</content>';
			//echo $xml;
			
			if( !empty($xml) ) $xml_obj->loadXML( $xml );
			
			$proc = new XSLTCache();
			$proc->importStyleSheet( $xsl ); 
			$retval = $proc->transformToXML( $xml_obj );
		}
		else {
			user_error( sprintf('Call to undefined template at %s', $xsl), E_USER_ERROR );
		}
		
		return( $retval );
	}
	
	protected function get_php5( $view, $xml = "" )
	{
		$retval = '';
		
		$xsl = sprintf('%s/%s/%s.xsl', SITEROOT, $_ENV['app_path'] .'/'. $_ENV['views_path'] .'/'. $this->controller_name, $view );
		if( is_file($xsl) ) {
			$xsl = realpath( $xsl );
			
			$xml_obj = new DOMDocument;
			$xml .= $this->make_elements();
			$xml = xml_header() .'<content><documents>'. $xml .'</documents>'. $this->xml_global_attributes() . '</content>';
			//echo $xml;
			
			if( !empty($xml) ) $xml_obj->loadXML( $xml );
			
			$xsl_obj = new DOMDocument;
			if( $xsl_obj->load( $xsl ) ) {
				$proc = new XSLTProcessor;
				$proc->importStyleSheet( $xsl_obj ); 
			
				$retval = $proc->transformToXML( $xml_obj );
			}
		}
		else {
			user_error( sprintf('Call to undefined template at %s', $xsl), E_USER_ERROR );
		}
		
		return( $retval );
	}
	
	protected function display_php5( $view, $xml = "" ) {
		$xsl = sprintf('%s/%s/%s.xsl', SITEROOT, $_ENV['app_path'] .'/'. $_ENV['views_path'] .'/'. $this->controller_name, $view );
		if( is_file($xsl) ) {
			$xsl = realpath( $xsl );
			
			$xml_obj = new DOMDocument;
			$xml .= $this->make_elements();
			$xml = xml_header() .'<content><documents>'. $xml .'</documents>'. $this->xml_global_attributes() . '</content>';
			//echo $xml;
			
			if( !empty($xml) ) $xml_obj->loadXML( $xml );
			
			$xsl_obj = new DOMDocument;
			if( $xsl_obj->load( $xsl ) ) {
				$proc = new XSLTProcessor;
				$proc->importStyleSheet( $xsl_obj ); 
			
				echo $proc->transformToXML( $xml_obj );
			}
		}
		else {
			user_error( sprintf('Call to undefined template at %s', $xsl), E_USER_ERROR );
		}
	}
	
	protected function xml_global_attributes() {
		global $bd, $db;
		
		$xml = "<global>";

		$referer = "";
		if( array_key_exists('HTTP_REFERER',$_SERVER) ) {
			$referer = $_SERVER['HTTP_REFERER'];
			if( $referer ) $referer = '?'. $this->controller_name .'.show';
		}
		$xml .= "<referer>". $referer ."</referer>";
		$xml .= "<session_id>". session_id() ."</session_id>";
		$xml .= "<site_prefix>". SITEPREFIX ."</site_prefix>";
		$xml .= "<views_path>". SITEPREFIX . $_ENV['app_path'] .'/'. $_ENV['views_path'] ."</views_path>";
		$xml .= "<data_path>". SITEPREFIX . $_ENV['data_path'] ."</data_path>";
		$xml .= "<sop_path>". SITEPREFIX . $_ENV['sop_path'] ."</sop_path>";
		$xml .= "<fair_path>". SITEPREFIX . $_ENV['fair_path'] ."</fair_path>";
		$xml .= "<fair_documents_path>". SITEPREFIX . $_ENV['fair_documents'] ."</fair_documents_path>";
		$xml .= "<controller_name>". $this->controller_name ."</controller_name>";
		$xml .= "<method_name>". $this->method_name ."</method_name>";
		$xml .= "<lang>". $_SESSION['lang'] ."</lang>";
		$xml .= "<current_datetime>". date("d.m.Y h:i") ."</current_datetime>";
		$xml .= "<page>". $this->page ."</page>";
		$xml .= "<pages>". $this->pages ."</pages>";
		$xml .= "<rows_per_page>". $this->rows_per_page ."</rows_per_page>";
		$xml .= "<browser_type>". $bd->GetType() ."</browser_type>";
		
		$sql = "select id,UNIX_TIMESTAMP(created_at),UNIX_TIMESTAMP(updated_at) from articles order by created_at desc,updated_at desc limit 1";
		$rs = $db->query( $sql );
		if( $row = $rs->fetch_row() ) {
			if( $row[2] != '' ) {
				$last_modified = gmdate( "D, d m Y H:i:s", $row[2] ) . " GMT";
			}
			else {
				$last_modified = gmdate( "D, d m Y h:i:s", $row[1] ) . " GMT";
			}
		}
		$xml .= "<last_modified_articles>". $last_modified ."</last_modified_articles>";
		
		$sql = "select id,UNIX_TIMESTAMP(created_at),UNIX_TIMESTAMP(updated_at) from shop_products order by created_at desc,updated_at desc limit 1";
		$rs = $db->query( $sql );
		if( $row = $rs->fetch_row() )
		{
			if( $row[2] != '' )
			{
				$last_modified = gmdate( "D, d m Y H:i:s", $row[2] ) . " GMT";
			}
			else
			{
				$last_modified = gmdate( "D, d m Y h:i:s", $row[1] ) . " GMT";
			}
		}
		$xml .= "<last_modified_products>". $last_modified ."</last_modified_products>";
		
		header( "Last-Modified: {$last_modified}" );
		
		$settings = new Settings();
		$xml .= "<contacts_at_top><![CDATA[". $settings->contacts_at_top ."]]></contacts_at_top>";
		
		$service_anounces_right = new Article('service_anounces_right');
		$xml .= "<service_anounces_right>";
		foreach( $service_anounces_right->images as $sar_image )
		{
			$xml .= $sar_image->Xml();
		}
		$xml .= "</service_anounces_right>";
		
		$error_login_data = ( array_key_exists('error_login_data',$_REQUEST) && $_REQUEST['error_login_data'] )? $_REQUEST['error_login_data'] : "";
		$error_login = ( array_key_exists('error_login',$_REQUEST) && $_REQUEST['error_login'] )? $_REQUEST['error_login'] : "";
		$xml .= "<errors>";
			$xml .= "<error_login data='{$error_login_data}'>". $error_login ."</error_login>";
		$xml .= "</errors>";
		
		$xml.= $this->client->Xml();
		$xml.= $this->currency->Xml();
		$xml.= $this->shop_trash->Xml();
		$xml.= $this->fair_trash->Xml();
		$xml.= $this->GetCurrenciesXml();
		
		$xml.= $this->GetShopNewProductsXml( 20 );
		$xml.= $this->GetShopBestProductsXml( 20 );
		$xml.= $this->GetShopActionProductsXml( 20 );
		$xml.= $this->GetCountriesXml();
		
		$xml .= $this->GetAdditionXml();
		
		$xml .= "</global>";
		
		return( $xml );
	}
	
	protected function GetAdditionXml() {
		return('');
	}
	
	protected function make_elements() {
		$retval = "";
		
		$retval .= $this->topmenu->xml();
		$retval .= $this->leftmenu->xml();
		$retval .= $this->bottommenu->xml();
		
		$retval .= $this->BannersLeft();
		$retval .= $this->BannersRight();
		$retval .= $this->BannersServices();
		$retval .= $this->BannersActions();
		
		return( $retval );
	}
	
	protected function GetCountriesXml( $tag='countries' ) {
		$retval = "";
		
		$t = new MysqlTable('countries');
		$t->find_all( "enabled=1", "position" );
		$retval .= "<{$tag}>";
		foreach( $t->data as $row )
		{
			$o = new Country( $row['id'] );
			$retval .= $o->Xml();
		}
		$retval .= "</{$tag}>";
		
		return( $retval );
	}
	
	protected function GetShopFilteredProductsXml( $field, $tag, $items_count=5 ) {
		$retval = "";
		
		$t = new MysqlTable('shop_products');
		$t->find_all( "status=1 AND {$field}=1", "created_at desc,id desc", $items_count );
		$retval .= "<{$tag}>";
		foreach( $t->data as $row )
		{
			$o = new ShopProduct( $row['id'], false );
			$retval .= $o->Xml();
		}
		$retval .= "</{$tag}>";
		
		return( $retval );
	}
	
	protected function GetShopNewProductsXml( $items_count=5 ) {
		return( $this->GetShopFilteredProductsXml( 'is_new', 'new_products', $items_count ) );
	}
	
	protected function GetShopActionProductsXml( $items_count=5 ) {
		return( $this->GetShopFilteredProductsXml( 'is_action', 'action_products', $items_count ) );
	}
	
	protected function GetShopBestProductsXml( $items_count=5 ) {
		return( $this->GetShopFilteredProductsXml( 'show_in_best', 'best_products', $items_count ) );
	}
	
	/*
		items_count: 1....n
		filter: 1-is_new; 2-is_action; 3-show_in_best
	*/
	function json_get_filtered_products() {
		$retval = Array();
		
		$filter = intval( $_REQUEST['filter'] );
		$filter_field = '';
		switch( $filter ) {
			case 2:
				$filter_field = 'is_action'; break;
			case 3:
				$filter_field = 'show_in_best'; break;
			case 1:
			default:
				$filter_field = 'is_new'; break;
		}
		
		$items_count = intval( $_REQUEST['items_count'] );
		if( !$items_count ) $items_count = 5;
		
		$t = new MysqlTable('shop_products');
		$t->find_all( "status=1 AND {$filter_field}=1", "created_at desc,id desc", $items_count );
		foreach( $t->data as $row )
		{
			$o = new ShopProduct( $row['id'], false );
			
			$b = new StdClass();
			$b->id = $o->id;
			$b->name = $o->name;
			$b->price = $this->FormatPrice( $o->GetPriceByClient($this->client) * $this->currency->exchange_rate );
			$b->created_at = $o->created_at;
			
			$b->image = new StdClass();
			$b->image->id = 0;
			$b->image->path = SITEPREFIX . $_ENV['data_path'] .'/';
			$b->image->filename = '';
			$b->image->small = '';
			$b->image->tiny = '';
			$b->image->thumbnail = '';
			$b->image->type = '';
			if( count($o->images)>0 ) {
				$b->image->id = $o->images[0]->id;
				$b->image->filename = $o->images[0]->filename;
				$b->image->small = $o->images[0]->small;
				$b->image->tiny = $o->images[0]->tiny;
				$b->image->thumbnail = $o->images[0]->thumbnail;
				$b->image->type = $o->images[0]->type;
			}
			
			array_push( $retval, $b );
		}
		
		echo json_encode( $retval );
	}
	
	protected function GetCurrenciesXml() {
		global $db;
		
		$retval = "<currencies current='{$_SESSION['currency_id']}'>";
		
		$arr = Array();
		$sql = "select code from currencies group by code";
		$rs2 = $db->query( $sql );
		while( $row = $rs2->fetch_row() )
		{
			$sql = "select id from currencies where code={$row[0]} order by id desc limit 1";
			$id = $db->getone( $sql );
			
			$currency = new Currency( $id );
			array_push( $arr, $currency );
		}
		
		usort( $arr, "arr_sort" );
		
		foreach( $arr as $a )
		{
			$retval .= $a->Xml();
		}
		
		$retval .= "</currencies>";
		
		return( $retval );
	}
	
	protected function GetBannersXml( $folder, $tag ) {
		$retval = '';
		
		$retval .= "<{$tag}>";
		$t = new MysqlTable('banners');
		$t->find_all( "folder='{$folder}'", "position,id" );
		foreach( $t->data as $row )
		{
			$o = new Banner( $row['id'] );
			$retval .= $o->Xml();
		}
		$retval .= "</{$tag}>";
		
		return( $retval );
	}
	
	protected function BannersLeft() {
		return( $this->GetBannersXml( 'left_box', 'left_box' ) );
	}
	
	protected function BannersRight() {
		return( $this->GetBannersXml( 'right_box', 'banners_right' ) );
	}
	
	protected function BannersServices() {
		return( $this->GetBannersXml( 'services', 'banners_services' ) );
	}
	
	protected function BannersActions() {
		return( $this->GetBannersXml( 'actions', 'banners_actions' ) );
	}
	
	/*
		filter: 1-services; 2-actions
	*/
	function json_get_banners() {
		$retval = Array();
		
		$filter = intval( $_REQUEST['filter'] );
		$folder = 'services';
		switch( $filter ) {
			case 2:
				$folder = 'actions'; break;
			case 1:
			default:
				$folder = 'services'; break;
		}
		
		$t = new MysqlTable('banners');
		$t->find_all( "folder='{$folder}'", "position,id" );
		foreach( $t->data as $row )
		{
			$o = new Banner( $row['id'] );
			
			$b = new StdClass();
			$b->name = $o->name;
			$b->description = $o->description;
			$b->url = $o->url;
			$b->image = SITEPREFIX . $_ENV['data_path'] .'/'. $o->image;
			$b->image_type = $o->image_type;
			$b->created_at = $o->created_at;
			
			array_push( $retval, $b );
		}
		
		echo json_encode( $retval );
	}
	
	/*
		1 - successful
		-1 - parameters error
		-2 - already is logged
		-3 - client is disabled
		-4 - not found
	*/
	function json_login() {
		$retval = new StdClass();
		global $log;
		
		$email = $this->FormatEmail( $_REQUEST['email'] ); 
		$country_id = intval( trim( $_REQUEST['country'] ) );
		$country = new Country( $country_id );
		$phone = $this->FormatPhone( $country->phone_code, $_REQUEST['phone'] );
		$email_password = trim( $_REQUEST['email_password'] );
		$phone_password = trim( $_REQUEST['phone_password'] );
		
		if( !$this->client->IsLoaded() ) {
			if( ($email!='' && $email_password!='') || ($phone!='' && $phone_password!='') ) {
				$obj = new Client();
				if( $phone!='' && $phone_password!='' ) {
					$obj->LoadByPhoneAndPassword( $country_id, $phone, $phone_password );
				}
				if( !$obj->IsLoaded() && $email!='' && $email_password!='' ) {
					$obj->LoadByEmailAndPassword( $email, $email_password );
				}
				
				if( $obj->IsLoaded() && $obj->status==2 )
				{
					$this->client = $obj;
					$this->store();
					$retval->result = 1;
					$retval->action = 0;
					
					if( !$this->client->phone ) {
						$retval->action = 1; //asks for mobile
					}
					
					$log->Write( "base.json_login: user logged id={$this->client->id} email={$email} phone={$phone}" );
				}
				else if( !$obj->IsLoaded() ) {
					$retval->result = -4;
					$log->Write( "base.json_login: user not found email={$email} phone={$phone}" );
				}
				else {
					$retval->result = -3;
					$log->Write( "base.json_login: user is not activated email={$email} phone={$phone}" );
				}
			}
			else {
				$retval->result = -1;
				$log->Write( "base.json_login: parameters error email={$email} phone={$phone}" );
			}
		}
		else {
			$retval->result = -2;
			$log->Write( "base.json_login: user already logged email={$email} phone={$phone}" );
		}
		
		echo json_encode( $retval );
	}
	
	function json_client_update_phone() {
		$retval = new StdClass();
		global $log;
		
		$retval->result = -1;
		
		if( $this->client->IsLoaded() ) {
			$country_id = intval( trim( $_REQUEST['country'] ) );
			$country = new Country( $country_id );
			$phone = $this->FormatPhone( $country->phone_code, $_REQUEST['phone'] );
			$log->Write( "base.json_client_update_phone client.id={$this->client->id} client.phone={$this->client->phone} phone={$phone}" );
			if( $phone ) {
				$this->client->country_id = $country_id;
				$this->client->phone = $phone;
				$this->client->Save();
				$retval->result = 1;
			}
		}
		
		echo json_encode( $retval );
	}
	
	function FormatPhone( $code, $phone ) {
		if( $phone ) {
			$phone = trim( $phone );
			$phone = preg_replace( '@[\s\\\/=&\(\)\[\]\{\}%\^\+\-\._]+@', '', $phone );
			$pos = strpos($phone, $code);
			if( $pos===0 ) $phone = substr( $phone, strlen($code) );
			
			if( $code == '380' ) {
				$zero = substr( $phone, 0, 1);
				if( $zero == '0' ) $phone = substr( $phone, 1 );
			}
		}
		
		return( $phone );
	}
	
	function FormatEmail( $email ) {
		if( $email ) {
			$email = trim( $email );
			$email = preg_replace( '@[\s\\\/=&\(\)\[\]\{\}%\+\^]+@', '', $email );
			$email = preg_replace( '/[\.]{2,}/', '.', $email );
		}
		
		return( $email );
	}
	
	function FormatPrice( $price ) {
		return( sprintf( "%01.2f", $price ) );
	}
	
	function logout()
	{
		//$url = $_REQUEST['url']; if( $url=="" ) $url = "/";
		$url = "/";
		if( isset($_SERVER['HTTP_REFERER']) && preg_match("/kollage.com.ua/",$_SERVER['HTTP_REFERER']) )
		{
			$url = $_SERVER['HTTP_REFERER'];
		}
		
		if( preg_match("/account/",$url))
		{
			$url = str_replace('account', 'welcome', $url);
		}
		
		$this->client = null;
		$this->store();
		redirect( $url );
	}
	
	function welcome()
	{
		$this->startpage();
	}
	
	function startpage()
	{
		$this->method_name = 'startpage';
		
		$xml = "<data>";
			$xml .= "<homepage>";
			$homepage = new Article( 'homepage' ); $xml .= $homepage->Xml( true );
			$xml .= "</homepage>";
		$xml .= "</data>";
		
		$this->display( 'startpage', $xml );
	}
	
	/*
		1 - 
		-1 - parameters error
		-2 - account not found
	*/
	function json_password_reminder()
	{
		global $log;
		$retval = new StdClass();
		$retval->result = -1;
		
		$country_id = intval( trim( $_REQUEST['country'] ) );
		$country = new Country( $country_id );
		$phone = $this->FormatPhone( $country->phone_code, $_REQUEST['phone'] );
		$email = $this->FormatEmail( $_REQUEST['email'] );

		$log->Write( "base.json_password_reminder '{$country->phone_code}' '{$_REQUEST['phone']}' '{$phone}'" );

		if( $email || $phone )
		{
			$client = new Client();
			$client->LoadByPhone( $country_id, $phone );
			if( !$client->IsLoaded() && $email!='' ) {
				$client->LoadByEmail( $email );
			}
			if( $client->IsLoaded() )
			{
				//Send email with password to user
				$settings = new Settings();
				
				if( $phone ) {
					$this->SendSMS( $client->country->phone_code, $phone, "Kollage. Napominanie parolya '{$client->password}'. {$this->sms_suffix}" );
					$log->Write( "base.json_password_reminder: send sms '{$phone} Naponinaem parol'" );
				}
				
				if( $email ) {
					$mail = new AMail( 'windows-1251' );
						$mail->subject = $settings->password_reminder_mail_subject;
						$mail->from_name = $settings->from_name;
						$mail->from_email = $settings->from_email;
						$mail->to_email = $client->email;
						$body = $settings->password_reminder_mail_body;
						$body = str_replace( "{CLIENT}", $client->firstname.' '.$client->lastname, $body );
						$body = str_replace( "{PASSWORD}", $client->password, $body );
					$mail->Send( $body );
				}
				$retval->result = 1;
			}
			else $retval->result = -2;
		}
		
		echo json_encode( $retval );
	}
	
	function get_register_protect_image_new()
	{
		$code = new ProtectCode( 6, PC_NUMERIC );
		$_SESSION['register_protect_code'] = $code->GetCode();
		if( $_SESSION['register_protect_code'] )
		{
			$obj = new ProtectImage( $_SESSION['register_protect_code'] );
			$obj->PrintImage();
		}
	}
	
	/*
		1 - successful
		-1 - parameters error
		-2 - wrong protect code
		-3 - client already exists
		-4 - saving error
	*/
	function json_register_client()
	{
		global $log;
		$this->method_name = 'json_register_client';
		$result = 0;

        $isProf = $_REQUEST['is_prof'];
		$firstname = trim( $_REQUEST['firstname'] );
		$lastname = trim( $_REQUEST['lastname'] );
		$country_id = intval( trim( $_REQUEST['country'] ) );
		$country = new Country( $country_id );
		$phone = $this->FormatPhone( $country->phone_code, $_REQUEST['phone'] );
		$log->Write("json_register_client '{$country->phone_code}' '". $_REQUEST['phone'] ."' '{$phone}'");
		$email = $this->FormatEmail( $_REQUEST['email'] );
		$password = trim( $_REQUEST['password'] );
		$protect_code = trim( $_REQUEST['protect_code'] );
		$send_news = intval( trim( $_REQUEST['send_news'] ) ); if( $send_news == 0 ) $send_news = 0; else $send_news == 1;
		
		if( !$_SESSION['register_protect_code'] || $_SESSION['register_protect_code'] != $protect_code ) {
			$result = -2; //wrong protect code
		}
		else if( $protect_code!='' && $firstname!='' && $lastname!='' && $phone!='' && $password!='' ) {
			$obj = new Client();
			$obj->LoadByPhone( $country_id, $phone );
			if( !$obj->IsLoaded() && $email!='' ) {
				$obj->LoadByEmail( $email );
			}
			if( $obj->IsLoaded() ) {
				$result = -3; // client already exists
			}
			else { //save client
				$code = new ProtectCode( 8, PC_NUMERIC );
				$obj->activation_key = strtoupper( $code->GetCode() );

				$obj->type_id = 1;
				$obj->firstname = $firstname;
				$obj->lastname = $lastname;
				$obj->country_id = $country_id;
				$obj->email = $email;
				$obj->phone = $phone;
				$obj->send_news = $send_news;
				$obj->password = $password; //??? md5???

				$country = new Country( $country_id );
				
				if( $obj->Save() )
				{

					//Send activation code by mobile
					$activation_key = sprintf( "%s-%s-%s-%s", substr( $obj->activation_key, 0, 2 ), substr( $obj->activation_key, 2, 2 ), substr( $obj->activation_key, 4, 2 ), substr( $obj->activation_key, 6, 2 ) );
				    $this->SendSMS( $country->phone_code, $phone, "Kollage. Registraciya, kod aktivacii '{$activation_key}'. {$this->sms_suffix}" );
					$log->Write( "base.json_register_client: send sms 'Kod aktivacii {$activation_key}'" );

                    $settings = new Settings();
                    if ($isProf === 'true') {

                        $mail = new AMail( 'windows-1251' );

                        $subject = $settings->confirm_prof_subject;
                        $subject = str_replace( "{CLIENT}", $obj->firstname.' '.$obj->lastname, $subject );

                        $client_id = $obj->id;
                        $confirm_url = "http://kollage.com.ua/?base.confirm_prof&client_id=" . $client_id;

                        $body = $settings->confirm_prof_body;
                        $body = str_replace( "{CLIENT}", $obj->firstname.' '.$obj->lastname, $body );
                        $body = str_replace( "{EMAIL}", $obj->email, $body );
                        $body = str_replace( "{PHONE}", $phone, $body );
                        $body = str_replace( "{ACTIVATION_CODE}", $activation_key, $body );
                        $body = str_replace( "{URL}", $confirm_url, $body );

                        $mail->subject = $subject;
                        $mail->from_name = $settings->from_name;
                        $mail->from_email = $settings->from_email;
                        $mail->to_email = $this->admin_email;
                        $mail->Send( $body );
                    }


					if( false ) { //don't send it by email
						//send email with an actiovation url to client
						$mail = new AMail( 'windows-1251' );
							$mail->subject = $settings->activation_mail_subject;
							$mail->from_name = $settings->from_name;
							$mail->from_email = $settings->from_email;
							$mail->to_email = $obj->email;
							$body = $settings->activation_mail_body;
							$body = str_replace( "{CLIENT}", $obj->firstname.' '.$obj->lastname, $body );
							$body = str_replace( "{CODE}", $activation_key , $body );
						$mail->Send( $body );
						//send email with an actiovation url to client
					}
					
					//send email to admin
					$mail = new AMail( 'windows-1251' );
						$subject = $settings->register_mail2admin_subject;
						$subject = str_replace( "{CLIENT}", $obj->firstname.' '.$obj->lastname, $subject );
						$mail->subject = $subject;
						$mail->from_name = $settings->from_name;
						$mail->from_email = $settings->from_email;
						$mail->to_email = $settings->from_email;
						$body = $settings->register_mail2admin_body;
						$body = str_replace( "{CLIENT}", $obj->firstname.' '.$obj->lastname, $body );
						$body = str_replace( "{EMAIL}", $obj->email, $body );
					//$mail->Send( $body );
					//send email to admin
					
					$_SESSION['register_protect_code'] = null;
					$result = 1;
				}
				else // save error
				{
					$result = -4;
					$log->Write("json_register_client SAVE ERROR. ". json_encode($obj) );
				}
			}
		}
		else {
			$result = -1; //parameters error
			$log->Write("json_register_client PARAMETERS ERROR. '{$country->phone_code}' '". $_REQUEST['phone'] ."' '{$phone}'");
		}
		
		$retval = new StdClass();
		$retval->result = $result;
		echo json_encode( $retval );
	}

    /**
     * Confirmation for 'professional' status
     * (invoked by clicking link from emaiL)
     */
	function confirm_prof()
    {
        $client_id = $_REQUEST['client_id'];
        $client = new Client($client_id);
        if( $client->IsLoaded() ) {
            $client->professional = 1;
            $client->type_id = 5;
            $client->Save();
            //Send email to client
            $mail = new AMail( 'windows-1251' );
            $settings = new Settings();
            $body = $settings->prof_confirmed_body;
            $mail->subject = $settings->prof_confirmed_subject;
            $mail->from_name = $settings->from_name;
            $mail->from_email = $settings->from_email;
            $mail->to_email = $client->email;
            $mail->Send( $body );
            //Display info for admin
            echo("Status professionala activirovan.");
        } else {
            echo("Client ID=".$client_id." ne najden.");
        }
    }

    /**
     * Confirmation for 'ads' status
     * (invoked by clicking link from emaiL)
     */
	function confirm_ads()
    {
        $client_id = $_REQUEST['client_id'];
        $client = new Client($client_id);
        if( $client->IsLoaded() ) {
            $client->type_id = 4;
            $client->Save();
            //Send email to client
            $mail = new AMail( 'windows-1251' );
            $settings = new Settings();
            $body = $settings->ads_confirmed_body;
            $mail->subject = $settings->ads_confirmed_subject;
            $mail->from_name = $settings->from_name;
            $mail->from_email = $settings->from_email;
            $mail->to_email = $client->email;
            $mail->Send( $body );
            //Display info for admin
            echo("Status ads agency activirovan.");
        } else {
            echo("Client ID=".$client_id." ne najden.");
        }
    }

    /**
     * Client's request to get 'professional' status
     *
     *  1 - ok;
     *  -1 - client not found;
     */
    function request_prof_status()
    {
        $client_id = $_SESSION['client_id'];
        $result = 0;
        $client = new Client($client_id);
        if ($client && $client->isLoaded()) {
            $mail = new AMail( 'windows-1251' );
            $settings = new Settings();
            $subject = $settings->confirm_prof_subject;
            $subject = str_replace( "{CLIENT}", $client->firstname.' '.$client->lastname, $subject );
            $confirm_url = "http://kollage.com.ua/?base.confirm_prof&client_id=" . $client_id;

            $country = new Country($client->country_id);
            $code = $country->phone_code;
            $phone = $client->phone;
            $formattedPhone = $this->FormatPhone( $code, $phone);

            $body = $settings->confirm_prof_body;
            $body = str_replace( "{CLIENT}", $client->firstname.' '.$client->lastname, $body );
            $body = str_replace( "{EMAIL}", $client->email, $body );
            $body = str_replace( "{ACTIVATION_CODE}", "", $body );
            $body = str_replace( "{PHONE}", $formattedPhone, $body );
            $body = str_replace( "{URL}", $confirm_url, $body );

            $mail->subject = $subject;
            $mail->from_name = $settings->from_name;
            $mail->from_email = $settings->from_email;
            $mail->to_email = $this->admin_email;
            $mail->Send( $body );

            $result = 1;
        } else {
            $result = -1;
        }

        $retval = new StdClass();
        $retval->result = $result;
        echo json_encode( $retval );
    }

    /**
     * Client's request to get 'ads' status
     *
     *  1 - ok;
     *  -1 - client not found;
     */
    function request_ads_status()
    {
        $client_id = $_SESSION['client_id'];
        $result = 0;
        $client = new Client($client_id);
        if ($client && $client->isLoaded()) {
            $mail = new AMail( 'windows-1251' );
            $settings = new Settings();
            $subject = $settings->confirm_ads_subject;
            $subject = str_replace( "{CLIENT}", $client->firstname.' '.$client->lastname, $subject );
            $confirm_url = "http://kollage.com.ua/?base.confirm_ads&client_id=" . $client_id;

            $country = new Country($client->country_id);
            $code = $country->phone_code;
            $phone = $client->phone;
            $formattedPhone = $this->FormatPhone( $code, $phone);

            $body = $settings->confirm_ads_body;
            $body = str_replace( "{CLIENT}", $client->firstname.' '.$client->lastname, $body );
            $body = str_replace( "{EMAIL}", $client->email, $body );
            $body = str_replace( "{ACTIVATION_CODE}", "", $body );
            $body = str_replace( "{PHONE}", $formattedPhone, $body );
            $body = str_replace( "{URL}", $confirm_url, $body );

            $mail->subject = $subject;
            $mail->from_name = $settings->from_name;
            $mail->from_email = $settings->from_email;
            $mail->to_email = $this->admin_email;
            $mail->Send( $body );

            $result = 1;
        } else {
            $result = -1;
        }

        $retval = new StdClass();
        $retval->result = $result;
        echo json_encode( $retval );
    }

	/*
		1 - successful
		-1 - parameters error
		-2 - wrong protect code
	*/
	function json_activate_client()
	{
		$this->method_name = 'json_activate_client';
		$result = 0;
		
		$code = strtoupper( trim( $_REQUEST['code'] ) );
		$code = str_replace( '-', '', $code );
		if( $code ) {
			$obj = new Client();
			$obj->LoadByActivationKey( $code );
			if( $obj->IsLoaded() )
			{
				$obj->status = 2; //Client is active
				if( $obj->Save() )
				{
					if( $obj->email ) {
						//send email with congratulation
						$settings = new Settings();
						$mail = new AMail( 'windows-1251' );
							$mail->subject = $settings->register_mail_subject;
							$mail->from_name = $settings->from_name;
							$mail->from_email = $settings->from_email;
							//$mail->to_name = $obj->firstname ." ". $obj->lastname;
							$mail->to_email = $obj->email;
							$body = $settings->register_mail_body;
							$body = str_replace( "{CLIENT}", $obj->firstname.' '.$obj->lastname, $body );
						$mail->Send( $body );
					}
					// TODO: send SMS
					$result = 1;
				}
			}
			else {
				$result = -2;
			}
		}
		else {
			$result = -1;
		}
		
		$retval = new StdClass();
		$retval->result = $result;
		echo json_encode( $retval );
	}

	protected function SendSMS( $code, $number, $message='' ) {
		global $log;
		$retval = null;

		$number = $this->FormatPhone( $code, $number );
		$log->Write( "base.SendSMS ({$code}) ({$number}) msg:{$message} sending is in process" );
		$number = $code . $number;
		if( $number && strlen($number)>=11 ) {
			$curl = curl_init();
			$curlOptions = array(   
				CURLOPT_URL=>"https://alphasms.ua/api/http.php?version=http&key=a9d74b03a1f06ef5abb752c46e805c6422c07071&command=send&from=Kollage&to={$number}&message=". urlencode($message),
				CURLOPT_SSL_VERIFYPEER=>false,
				CURLOPT_FOLLOWLOCATION=>false,
				CURLOPT_POST=>true,
				CURLOPT_HEADER=>false,
				CURLOPT_RETURNTRANSFER=>true,
				CURLOPT_CONNECTTIMEOUT=>15,
				CURLOPT_TIMEOUT=>100,
			);  
			curl_setopt_array( $curl, $curlOptions ); 
			$result = curl_exec( $curl );
			if( $result === false ) {
				$log->Write( "base.SendSMS ({$number}) msg:{$message} Https request failed" );
			}
			else {
				$log->Write( "base.SendSMS ({$number}) msg:{$message} sending successfull" );
			}
			
			curl_close( $curl );
		}
		else {
			$log->Write( "base.SendSMS ({$number}) msg:{$message} not sent, number is wrong" );
		}
		
		return( $retval );
	}
	
	/**
	 * [SendSMS description]
	 * @param [type] $code    country phone code, from countries table
	 * @param [type] $number  number without country code, 9-10 digits
	 * @param string $message [description]
	 */
	protected function SendSMS_atompark( $code, $number, $message='' ) {
		global $log;
		$retval = null;
		$username = 'info@kollage.com.ua';
		$password = 'gtxfnmajnj';
		
		$number = $this->FormatPhone( $code, $number );
		$log->Write( "base.SendSMS ({$code}) ({$number}) msg:{$message} sending is in process" );
		$number = $code . $number;
		if( $number && strlen($number)>=11 ) {
			$src = '<?xml version="1.0" encoding="UTF-8"?>';
			$src .= '<SMS>';
			
			$src .= '<operations>';
			$src .= '<operation>SEND</operation>';
			$src .= '</operations>';
			
			$src .= '<authentification>';
			$src .= '<username>'. $username .'</username>';
			$src .= '<password>'. $password .'</password>';
			$src .= '</authentification>';
			
			$src .= '<message>';
			$src .= '<sender>Kollage</sender>';
			$src .= '<text>'. $message .'</text>';
			$src .= '</message>';
			
			$src .= '<numbers>';
			$src .= '<number messageID="msg11">'. $number .'</number>';
			$src .= '</numbers>';
			
			$src .= '</SMS>';
			
			$curl = curl_init();
			$curlOptions = array(   
				CURLOPT_URL=>'https://atompark.com/members/sms/xml.php',
				CURLOPT_SSL_VERIFYPEER=>false,
				CURLOPT_FOLLOWLOCATION=>false,
				CURLOPT_POST=>true,
				CURLOPT_HEADER=>false,
				CURLOPT_RETURNTRANSFER=>true,
				CURLOPT_CONNECTTIMEOUT=>15,
				CURLOPT_TIMEOUT=>100,
				CURLOPT_POSTFIELDS=>array('XML'=>$src)
			);  
			//$log->Write( json_encode($curlOptions) );
			curl_setopt_array( $curl, $curlOptions ); 
			$result = curl_exec( $curl );
			//$log->Write( json_encode($result) );
			if( $result === false ) {
				$log->Write( "base.SendSMS ({$number}) msg:{$message} Https request failed" );
			}
			else {
				$log->Write( "base.SendSMS ({$number}) msg:{$message} sending successfull" );
			}
			
			curl_close( $curl );
		}
		else {
			$log->Write( "base.SendSMS ({$number}) msg:{$message} not sent, number is wrong" );
		}
		
		return( $retval );
	}

	protected function PushNotification( $token, $title, $body, $category='message' ) {
		global $log;
		$api_access_key = 'AIzaSyA7-i7-SINwo7BI0REPIrcyK13aJFHD5Ec'; //Google Cloud Messages

		$log->Write( "base.PushNotification ({$token}) title:{$title} body:{$body} category:{$category}, starting" );

		$data = array(
    		'category' => $category
		);

		$notification = array(
	    	'title' => $title,
	    	'body'  => $body,
	    	'sound' => 'default',
		);

		$fields = array(
	    	//'registration_ids'  => $registrationIds, //1 to 1000
	    	'to'                => $token,
	    	'priority'          => 'normal',
	    	'collapse_key'          => $category,
	    	'content_available' => true,
	    	'data'              => $data,
	    	'notification'      => $notification
		);

		$headers = array
		(
		   'Authorization: key='. $api_access_key,
		   'Content-Type: application/json'
		);

		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_URL, 'https://gcm-http.googleapis.com/gcm/send' );
		curl_setopt( $ch, CURLOPT_POST, true );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode($fields) );
		$result = curl_exec($ch);
		$httpcode = intval( curl_getinfo($ch, CURLINFO_HTTP_CODE) );
		curl_close($ch);

		$retval = false;
		if( $httpcode==200 ) {
			try {
			   $obj = json_decode( $result );
			   $retval = $obj->success;
			   $log->Write( "base.PushNotification ({$token}) title:{$title} body:{$body} category:{$category}, success" );
			}
			catch( Exception $e ) {
				$log->Write( "base.PushNotification ({$token}) title:{$title} body:{$body} category:{$category}, error: {$e->message}" );
			}
		}

		return( $retval );
	}
	
	protected function GetStreetsXml( $region_id=0 )
	{
		$where = ($region_id>0)? "region_id={$region_id}" : "";
		$retval = "<streets>";
		$t = new MysqlTable('streets');
		$t->find_all($where,"position,id");
		foreach( $t->data as $row )
		{
			$o = new Street( $row['id'] );
			$retval .= $o->Xml();
		}
		$retval .= "</streets>";
		
		return( $retval );
	}
	
	protected function GetRegionsXml()
	{
		$retval = "<regions>";
		$t = new MysqlTable('regions');
		$t->find_all("","position,id");
		foreach( $t->data as $row )
		{
			$o = new Region( $row['id'] );
			$retval .= $o->Xml();
		}
		$retval .= "</regions>";
		
		return( $retval );
	}
	
	function get_products_news()
	{
		$JsHttpRequest = new JsHttpRequest("utf-8");
		
		$xml = "";
		$t = new MysqlTable('shop_products');
		$id = $_REQUEST['id']; $where = '';
		if( $id>0 )
		{
			$where = "and id<>{$id}";
		}
		if( $row = $t->find_first( "status=1 {$where}", "rand()" ) )
		{
			$o = new ShopProduct( $row['id'], false );
			$xml = $o->Xml();
			$GLOBALS['_RESULT']['id'] = $o->id;
		}
		
		$this->display( 'products_news', $xml );
	}
	
	function get_service_announces()
	{
		$JsHttpRequest = new JsHttpRequest("utf-8");
		
		$xml = "";
		$a = new Article('service_anounces_right');
		if( $a->IsLoaded() )
		{
			$i = rand( 0, count($a->images)-1 );
			//echo count($a->images).':'.$i.$a->images[$i]->tiny;
			$xml .= "<service_announces>";
			$xml .= $a->images[$i]->Xml();
			$xml .= "</service_announces>";
			$GLOBALS['_RESULT']['id'] = $o->id;
		}
		
		$this->display( 'service_announces', $xml );
		
		echo "";
	}
	
	function get_streets()
	{
		global $db;

		$JsHttpRequest = new JsHttpRequest("utf-8");
		
		$GLOBALS['_RESULT']['streets'] = Array();
		$region_id = $_REQUEST['region_id'];
		if( $region_id )
		{
			$GLOBALS['_RESULT']['region_id'] = $region_id;
			$where = ($region_id>0)? "and s.region_id={$region_id}" : "";
		}
		$rs = $db->query( "select s.id,d.value from streets s left join dictionary d on s.id=d.parent_id where d.folder='streets' and d.lang='{$_SESSION['lang']}' {$where} and name='name' order by d.value" );
		while( $row = $rs->fetch_row() )
		{
			$o = new Street( $row[0] );
			array_push( $GLOBALS['_RESULT']['streets'], Array( $o->id, $o->name ) );
		}
	}
	
	function search_protect_image()
	{
		if( $_SESSION['search_protect_code'] )
		{
			$obj = new ProtectImage( $_SESSION['search_protect_code'] );
			$obj->PrintImage();
		}
	}
	
	/* Delete the temporary orders ( status=0 ) which were created 1 day ago and older */
	function clear_tmp_orders()
	{
		global $db;

		//Photos
		//$sql = "select id from sop_orders where status=0 and date_sub(curdate(),interval 2 day)>=date(created_at)";
		$sql = "select id from sop_orders where status in (0,98) and date_sub(curdate(),interval 2 day)>=date(created_at)";
		$rs = $db->query( $sql );
		while( $row = $rs->fetch_array() )
		{
			$o = new SOPOrder( $row[0] );
			$o->LoadPhotos();
			$o->Delete();
		}
		
		//Shop
		$sql = "delete from shop_order_products where order_id in (select id from shop_orders where status=0 and date_sub(curdate(),interval 1 day)>=date(created_at))";
		$db->query( $sql );
		$sql = "delete from shop_orders where status=0 and date_sub(curdate(),interval 1 day)>=date(created_at)";
		$db->query( $sql );
	}

	function mail_delivery_queue() {
		global $db;

		while( true ) {
			$sql = "select id from mail_delivery_queues where status=1 order by id limit 1";
			$rs = $db->query( $sql );
			$row = $rs->fetch_array();
			$id = $row[0];
			if( $id>0 ) {
				$q = new MailDeliveryQueue( $id );
				if( $q->loaded ) {
					//$q->status = 2;
					//$q->Save();

					$this->log->Write( "base.mail_delivery_queue: new/continue mail queue id={$id}" );

					$sql = "select count(id) from mail_delivery_queue_emails where queue_id={$id} and status=1";
					$unsent_emails = intval( $db->getone( $sql ) );
					if( $unsent_emails == 0 ) {
						$q->status = 3;
						$q->Save();
						$this->log->Write( "base.mail_delivery_queue: new mail queue id={$id}, sending is completed" );
					}
					else {
						$sql = "select id from mail_delivery_queue_emails where queue_id={$id} and status=1";
						$rs2 = $db->query( $sql );
						while( $row2 = $rs2->fetch_row() ) {
							$id2 = $row2[0];
							$m = new MailDeliveryQueueEmail( $id2 );
							
							$mail = new AMailSendGrid();
							$mail->subject = $q->subject;
							$mail->from_name = $q->from_name;
							$mail->from_email = $q->from_email;
							if( $q->attachment_file ) {
								$attachment = SITEROOT .'/'. $_ENV['mqueue_path'] .'/'. $q->attachment_file;
								$mail->AttachFile( $attachment, $q->attachment_name );
							}
							$mail->to_email = $m->email;
							$response = $mail->Send( $q->body );
	                  if( $response  ) {
	                      $m->status = 2;
	                      $this->log->Write( "base.mail_delivery_queue: queue id={$id}, email={$m->email} id={$m->id}, sent: ". $response );
	                  }
	                  else {
	                      $m->status = 3;
	                      $this->log->Write( "base.mail_delivery_queue: queue id={$id}, email={$m->email} id={$m->id}, error: ". $response );
	                  }
	                  $m->Save();
							
							//sleep( mt_rand(1,3) );
						}
					}
				}
			}

			sleep( mt_rand(5,10) );
		}
	}
	
	function checkup_orders()
	{
		global $db, $log;

		//Photos
		$sql = "select id from sop_orders where status=1 and updated_at<subdate(now(),interval 3 hour)";
		$rs = $db->query( $sql );
		while( $row = $rs->fetch_array() )
		{
			$o = new SOPOrder( $row[0] );
			$md = new MysqlDateTime( $o->created_at );
			$dir = sprintf( "%s%s/%s", $o->dir, $md->GetFrontEndValue('y.m.d'), $o->order_number );
			if( !file_exists($dir) )
			{
				$o->status = 2;
				$o->Save();
				
				$client = new Client( $o->client_id );
				$o->order_number = substr( $o->order_number, 0, 10 );
				
				// Send email to client
				$settings = new Settings();
				$sop_settings = new SOPSettings();
				if( $client->email ) {
					$mail = new AMail( 'windows-1251' );
						$subject = $sop_settings->order_mail_printed_subject;
						$subject = str_replace( "{ORDER_NR}", $o->order_number, $subject );
						$mail->subject = $subject;
						
						$mail->from_name = $settings->from_name;
						$mail->from_email = $settings->from_email;
						$mail->to_email = $client->email;
						$body = $sop_settings->order_mail_printed_body;
						$body = str_replace( "{CLIENT}", $client->firstname.' '.$client->lastname, $body );
						$body = str_replace( "{ORDER_NR}", $o->order_number, $body );
						$body = str_replace( "{ORDER_STATUS}", $this->GetSOPOrderStatusStr($o), $body );
						$body = str_replace( "{ORDER}", $this->GetSOPOrderSummaryTableExtended( $o ), $body );
					$mail->Send( $body );
				}
				
				// Send SMS to client
				if( $client->phone ) {
					$this->SendSMS( $client->country->phone_code, $client->phone, "Kollage. Zakaz '{$o->order_number}' gotov. {$this->sms_suffix}" );
				}

				if( count($client->tokens) > 0 ) {
					$log->Write( "base.checkup_orders tokens exist, sending push notification" );
					foreach( $client->tokens as $token ) {
						$this->PushNotification( $token->google_cloud_token, "Заказ печати фотографий.", "Заказ номер '{$o->order_number}' готов." );
						$log->Write( "base.checkup_orders tokens exist, sending push notification to {$token->google_cloud_token} order={$o->order_number}" );
					}
				}
			}
			else
			{
				//Set the time
				$db->query( "update sop_orders set updated_at=now() where id={$row[0]}" );
			}
		}
		
		//Books
		$sql = "select id from pb_orders where status=1 and updated_at<subdate(now(),interval 3 hour)";
		$rs = $db->query( $sql );
		while( $row = $rs->fetch_array() )
		{
			$o = new PBOrder( $row[0] );
			$md = new MysqlDateTime( $o->created_at );
			$dir = sprintf( "%s%s", $o->dir, $o->order_number );
			if( !file_exists($dir) )
			{
				$o->status = 2;
				$o->Save();
			}
			else
			{
				//Set the time
				$db->query( "update pb_orders set updated_at=now() where id={$row[0]}" );
			}
		}
	}

	/*function anv_test_push() {
		global $log;

		$client = new Client(242);
		if( count($client->tokens) > 0 ) {
			$log->Write("anv_test_push");
			foreach( $client->tokens as $token ) {
				$this->PushNotification( $token->google_cloud_token, "Заказ фотографий готов.", "Заказ номер '2384908214098209' готов." );
			}
		}
	}*/
	
	function insit_checkup_orders()
	{
		global $log;
		$folder = $_ENV['insit']['path'];
		$notify_to = $_ENV['insit']['notify_to'];
		$path = SITEROOT ."/{$folder}/";
		$tmp = SITEROOT .'/'. $_ENV['tmp_path'];
		
		$zip_pwd = '12345';
		
		$dh = opendir( $path );
		if( $dh )
		{
			@chdir( $tmp );
			while( ($file = readdir($dh)) !== false )
			{
				if( $file!='.' && $file!='..' && is_dir($path.$file) ) {
					$subdir = $path . $file .'/';
					$finished_flag_file = $subdir ."Finished.txt";
					$folders_file = $subdir ."Folders.zip";
					if( file_exists($finished_flag_file) && file_exists($folders_file) ) {
						$order = new Insit_Order();
						$order->LoadByNumber( $file );
						if( $order->IsLoaded() ) {
							//$log->Write( "insit_checkup_orders file={$file} THIS ORDER EXISTS, skip checking" );
						}
						else {
                            $log->Write( "insit_checkup_orders NEW ORDER {$file}" );
							$order_file = "{$file}.txt";
                            // $cmd = "/usr/bin/unzip -qq -P {$zip_pwd} {$folders_file} {$order_file} > /dev/null";
							$cmd = "/usr/local/bin/7z e -p{$zip_pwd} {$folders_file} {$order_file} > /dev/null";
							@system( $cmd, $result );
							if( file_exists( $order_file ) ) {
                                $log->Write( "insit_checkup_orders UNPACKED {$file}.txt" );
								//read the order file
								$rows = file( $order_file );
								if( $rows ) {
									$order = new Insit_Order();
									$order->order_number = $file;
									$order->status = 1;
									$order->ParseFromFile( $rows );
									$order->Save();
									
									$insit_settings = new InsitSettings();
									
									$settings = new Settings();
									$mail = new AMail( 'windows-1251' );
									$mail->from_name = $settings->from_name;
									$mail->from_email = $settings->from_email;
									
									if( $order->email ) {
                                        $log->Write( "insit_checkup_orders SENDING EMAIL to {$order->email}, order_id={$order->id}" );
										$mail->subject = $insit_settings->order_mail_subject;
										$mail->subject = str_ireplace( "{ORDER_NR}", $order->order_number, $mail->subject );
										$mail->to_email = $order->email;
										$body = $insit_settings->order_mail_body;
										$body = str_ireplace( "{ORDER_NR}", $order->order_number, $body );
										$body = str_ireplace( "{CUSTOMER_ID}", $order->customer_id, $body );
										$body = str_ireplace( "{FIRST_NAME}", $order->first_name, $body );
										$body = str_ireplace( "{LAST_NAME}", $order->last_name, $body );
										$body = str_ireplace( "{PRODUCT_DETAILS}", $order->product_details, $body );
										$body = str_ireplace( "{STREET}", $order->street, $body );
										$body = str_ireplace( "{CITY}", $order->city, $body );
										$body = str_ireplace( "{ZIP}", $order->zip, $body );
										$body = str_ireplace( "{PHONE}", $order->phone, $body );
										$body = str_ireplace( "{EMAIL}", $order->email, $body );
										$body = str_ireplace( "{COMMENT}", $order->comment, $body );
										$body = str_ireplace( "{PAGE_COUNT}", $order->page_count, $body );
										$body = str_ireplace( "{COPIES}", $order->copies, $body );
										$body = str_ireplace( "{TOTAL_PRICE}", $order->total_price, $body );
										$mail->Send( $body );
									}
									
									//TODO: send sms to client?
									
									// to INSIT
                                    $log->Write( "insit_checkup_orders SENDING EMAIL to INSIT {$insit_settings->admin_emails}, order_id={$order->id}" );
									$mail->subject = $insit_settings->admin_order_mail_subject;
									$mail->subject = str_ireplace( "{ORDER_NR}", $order->order_number, $mail->subject );
									$mail->to_email = $notify_to;
									$mail->reply_to = $insit_settings->admin_emails;
									$body = $insit_settings->admin_order_mail_body;
									$body = str_ireplace( "{ORDER_NR}", $order->order_number, $body );
									$body = str_ireplace( "{CUSTOMER_ID}", $order->customer_id, $body );
									$body = str_ireplace( "{FIRST_NAME}", $order->first_name, $body );
									$body = str_ireplace( "{LAST_NAME}", $order->last_name, $body );
									$body = str_ireplace( "{PRODUCT_DETAILS}", $order->product_details, $body );
									$body = str_ireplace( "{STREET}", $order->street, $body );
									$body = str_ireplace( "{CITY}", $order->city, $body );
									$body = str_ireplace( "{ZIP}", $order->zip, $body );
									$body = str_ireplace( "{PHONE}", $order->phone, $body );
									$body = str_ireplace( "{EMAIL}", $order->email, $body );
									$body = str_ireplace( "{COMMENT}", $order->comment, $body );
									$body = str_ireplace( "{PAGE_COUNT}", $order->page_count, $body );
									$body = str_ireplace( "{COPIES}", $order->copies, $body );
									$body = str_ireplace( "{TOTAL_PRICE}", $order->total_price, $body );
									$mail->Send( $body );
									
									// to admin
									$mail->subject = $insit_settings->admin_order_mail_subject;
									$mail->subject = str_ireplace( "{ORDER_NR}", $order->order_number, $mail->subject );
									$mail->to_email = $insit_settings->admin_emails;
									$mail->Send( $body );
								}
								unlink( $order_file );
								$log->Write( "insit_checkup_orders file={$file} ITS ORDER DIR AND IT WAS FINISHED" );
							}
						}
					}
				}
			}
			
			closedir( $dh );
		}
	}
	
	function insit_checkup_ready_orders()
	{
		global $log;
		$folder = $_ENV['insit']['path'];
		$path = SITEROOT ."/{$folder}/ready/";
		
		$dh = opendir( $path );
		if( $dh )
		{
			@chdir( $tmp );
			while( ($file = readdir($dh)) !== false )
			{
				if( $file!='.' && $file!='..' && is_dir($path.$file) ) {
					$subdir = $path . $file .'/';
					$order = new Insit_Order();
					$order->LoadByNumber( $file );
					if( $order->IsLoaded() && $order->status==1 ) {
						$order->status = 2;
						$order->Save();
						
						$insit_settings = new InsitSettings();
						$settings = new Settings();
						$mail = new AMail( 'windows-1251' );
						$mail->from_name = $settings->from_name;
						$mail->from_email = $settings->from_email;
						$mail->subject = $insit_settings->admin_order_mail_ready_subject;
						$mail->subject = str_ireplace( "{ORDER_NR}", $order->order_number, $mail->subject );
						$body = $insit_settings->admin_order_mail_ready_body;
						$body = str_ireplace( "{ORDER_NR}", $order->order_number, $body );
						$mail->to_email = $insit_settings->admin_emails;
						$mail->Send( $body );
						
						$log->Write( "insit_checkup_ready_orders file={$file} FOUND ready order, status changed to 2, email was sent to admin" );
					}
				}
			}
		}
	}
	
	protected function GetSOPOrderStatusStr( &$order )
	{
		$retval = '';
		
		if( $order->IsLoaded() )
		{
			switch( $order->status )
			{
				case 1:
					$retval = $this->dictionary->get('/locale/my/orders/photo/statuses/s1');
					break;
				case 2:
					$retval = $this->dictionary->get('/locale/my/orders/photo/statuses/s2');
					break;
				case 3:
					$retval = $this->dictionary->get('/locale/my/orders/photo/statuses/s3');
					break;
				case 4:
					$retval = $this->dictionary->get('/locale/my/orders/photo/statuses/s4');
					break;
				case 5:
					$retval = $this->dictionary->get('/locale/my/orders/photo/statuses/s5');
					break;
				case 6:
					$retval = $this->dictionary->get('/locale/my/orders/photo/statuses/s6');
					break;
			}
		}
		
		return( $retval );
	}
	
	protected function GetSOPOrderSummaryTableExtended( &$order )
	{
		$retval = '';
		
		if( $order->IsLoaded() )
		{
			$retval .= '<table style="border: 1px solid #e3bf85" cellspacing="3">';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/order_number2') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->order_number .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/created_at') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->created_at .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/updated_at') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->updated_at .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/paymentway_id') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->paymentway->name .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/delivery_id') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->delivery->name .'</td>';
			$retval .= '</tr>';
			
			if( $order->delivery_id == 1 )
			{
				$retval .= '<tr>';
				$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/point_name') .'</b></td>';
				$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->point->name .'</td>';
				$retval .= '</tr>';
			}
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/comments') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;color:#d80c76">'. $order->comments .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">&#160;</td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">&#160;</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/client') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->client->firstname .'&#160;'. $order->client->lastname .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/email') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->client->email .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/address_v') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->client->city .', '. $order->client->street->name .', '. $order->client->address .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/address2_v') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;color:#d80c76">'. $order->address .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/phone') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->client->phone .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">&#160;</td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">&#160;</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/photos') .'</b></td>';
			//$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->photos_count . $this->dictionary->get('/locale/common/items_amount') .'</td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $this->GetOrderSummaryTablePhotos( $order ) .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/photo_paper') .'</b></td>';
			$photo_paper = ($order->photo_paper == 1)? $this->dictionary->get('/locale/photos/order/photo_paper1') : $this->dictionary->get('/locale/photos/order/photo_paper2');
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $photo_paper .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/border') .'</b></td>';
			$border = ($order->border == 1)? $this->dictionary->get('/locale/common/exist') : $this->dictionary->get('/locale/common/no');
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $border .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/total_photos') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->photos_price_amount .' '. $this->dictionary->get('/locale/common/currency') .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/total_delivery') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. sprintf( "%01.2f", $order->GetDeliveryPrice() ) .' '. $this->dictionary->get('/locale/common/currency') .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/total_discount') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. sprintf( "%01.2f", $order->discount_price ) .' '. $this->dictionary->get('/locale/common/currency') .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/total_price') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. sprintf( "%01.2f", $order->total_price ) .' '. $this->dictionary->get('/locale/common/currency') .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/payed_price') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. sprintf( "%01.2f", $order->payed ) .' '. $this->dictionary->get('/locale/common/currency') .'</td>';
			$retval .= '</tr>';
			
			$retval .= '</table>';
		}
		
		return( $retval );
	}
	
	protected function GetOrderSummaryTablePhotos( &$order )
	{
		global $db;

		$retval = '';
		
		if( $order->IsLoaded() )
		{
			$photos = Array();
			$sql = "select sum(p.quantity),s.name,s.id from sop_order_photos p left join sop_order_photos_sizes s on s.id=p.size_id where p.order_id={$order->id} group by p.size_id order by s.position";
			$rs = $db->query( $sql );
			while( $row = $rs->fetch_row() )
			{
				$retval .= $row[1] .' - '. $row[0] .' '. $this->dictionary->get('/locale/common/items_amount') .'<br>';
			}
		}
		
		return( $retval );
	}
	
	function search() {
		global $db, $log;

		$str = trim( $_REQUEST['str'] ); $str = str_replace( "'", "", $str );
		$ord = intval( $_REQUEST['ord'] ); //1-products,articles; 2-articles,products
		if( !$ord ) $ord = 1;
		
		$xml = '<data>';
		$xml .= "<str>". urlencode($str) ."</str>";
		$xml .= "<ord>{$ord}</ord>";
		
		if( $str ) {
			$words = explode( " ", $str );
			//shop
			$search = '';
			$delim = '';
			foreach( $words as $v ) {
				$search .= $delim ."d.value like '%{$v}%'";
				$delim = " and ";
			}
			$sql = "SELECT p.id FROM shop_products p LEFT JOIN dictionary d ON d.parent_id=p.id WHERE p.status=1 AND d.folder='shop_products' AND d.lang='ru' AND d.name='name' AND (({$search}) OR p.number LIKE '%{$str}%') ORDER BY p.category_id,p.id LIMIT 50";
			$xml .= "<products>";
			$rs = $db->query( $sql );
			while( $row = $rs->fetch_row() )
			{
				$o = new ShopProduct( $row[0], false );
				$xml .= $o->Xml();
			}
			$xml .= "</products>";
			
			//articles
			$folders = "'services','photos'";
			$sql = "SELECT a.id FROM articles a LEFT JOIN dictionary d ON d.parent_id=a.id WHERE a.status=1 AND a.folder IN ({$folders}) AND d.folder='articles' AND d.lang='ru' AND ((d.name='header' AND {$search}) OR (d.name='text' AND {$search})) ORDER BY a.category_id,a.id LIMIT 50";
			$xml .= "<articles>";
			$rs = $db->query( $sql );
			while( $row = $rs->fetch_row() )
			{
				$o = new Article( $row[0], false );
				$xml .= $o->Xml();
			}
			$xml .= "</articles>";
		}
		
		$xml .= '</data>';
		
		$this->display( 'search', $xml );
	}
	
	function upc_success()
	{
		global $db, $log;
		
		$log->Write( "base.upc_success was called" );
		
		//$MerchantID = '1752256';  //test-server
		$MerchantID = '6984785';
		//$TerminalID = 'E7880056';  //test-server
		$TerminalID = 'E0133767';
		$OrderID = $_REQUEST['OrderID'];
		$SD = $_REQUEST['SD'];
		list( $order_type, $order_host ) = split( "\|", $SD );
		$xid = $_REQUEST['XID'];
		$rrn = $_REQUEST['Rrn'];
		$totalAmount = $_REQUEST['TotalAmount'];
		$altTotalAmount = $_REQUEST['AltTotalAmount'];
		$tranCode = strval( $_REQUEST['tranCode'] );
		$ApprovalCode = $_REQUEST['ApprovalCode'];
		$PurchaseTime = $_REQUEST['PurchaseTime'];
		$Currency = $_REQUEST['Currency'];
		$AltCurrency = $_REQUEST['AltCurrency'];
		$Delay = $_REQUEST['Delay']; if( $Delay ) $Delay = ','. $Delay;
		$signature = $_REQUEST['Signature'];
		$data = "$MerchantID;$TerminalID;$PurchaseTime;$OrderID{$Delay};$xid;$Currency;$totalAmount;$SD;". sprintf("%03s",$tranCode) .";$ApprovalCode;";
		if( $AltCurrency && $AltCurrency != $Currency )
		{
			$data = "$MerchantID;$TerminalID;$PurchaseTime;$OrderID{$Delay};$xid;$Currency,$AltCurrency;$totalAmount,$altTotalAmount;$SD;". sprintf("%03s",$tranCode) .";$ApprovalCode;";
		}
		$log->Write( "base.upc_success\ndata=". $data );
		//$fp = fopen( SITEROOT ."/upc/test-server.cert", "r" );  //test-server
		$fp = fopen( SITEROOT ."/upc/work-server.cert", "r" );
		$ok = 0;
		if( $fp )
		{
			$key = fread( $fp, 8192 );
			fclose( $fp );
			$pkeyid = openssl_pkey_get_public( $key );
			$signature = base64_decode( $signature );
			$ok = openssl_verify( $data , $signature, $pkeyid );
			openssl_free_key( $pkeyid );
		}
		$log->Write( "base.upc_success\nopenssl_verify returned ". $ok );
		
		$xml = "<data>";
		$xml .= "<OrderID>{$OrderID}</OrderID>";
		$xml .= "<xid><![CDATA[{$xid}]]></xid>";
		if( $ok == 1 )
		{
			$xml .= "<signature_failed>0</signature_failed>";
			$xml .= "<message>". $this->GetUPCResultMessage($tranCode) ."</message>";
			$log->Write( "base.upc_success\nVerifying passed successfully, order_type=". $order_type );
			
			switch( $order_type )
			{
				case "sop":
					$order = new SOPOrder(); $order->LoadByNumber( $OrderID );
					if( $order->status == 0 )
					{
						$order->payed = $order->payed + $totalAmount/100;
						$order->status = 1;
						//$order->xid = $xid;
						//$order->rrn = $rrn;
						$order->Save();
						$log->Write( "base.upc_success\nOrder N({$OrderID}) was saved with status=1. Payed {$order->payed}. Client ID={$order->client_id}" );
						redirect( "http://{$order_host}/?photos.upc_success&order_number=". $OrderID );
					}
					else
					{
						redirect( "http://{$order_host}/?photos.upc_success_already&order_number=". $OrderID );
					}
					break;
				case "printbook":
					$order = new PBOrder(); $order->LoadByNumber( $OrderID );
					if( $order->status == 0 )
					{
						$order->payed = $order->payed + $totalAmount/100;
						$order->status = 1;
						$order->Save();
						$log->Write( "base.upc_success\nOrder N({$OrderID}) was saved with status=1. Payed {$order->payed}. Client ID={$order->client_id}" );
						redirect( "http://{$order_host}/?services.printbook_upc_success&order_number=". $OrderID );
					}
					else
					{
						redirect( "http://{$order_host}/?photos.printbook_upc_success_already&order_number=". $OrderID );
					}
					break;
				case "photobook":
					$order = new PB2Order(); $order->LoadByNumber( $OrderID );
					if( $order->status == 0 )
					{
						$order->payed = $order->payed + $totalAmount/100;
						$order->status = 1;
						$order->Save();
						$log->Write( "base.upc_success\nOrder N({$OrderID}) was saved with status=1. Payed {$order->payed}. Client ID={$order->client_id}" );
						redirect( "http://{$order_host}/?services.photobook_upc_success&order_number=". $OrderID );
					}
					else
					{
						redirect( "http://{$order_host}/?photos.photobook_upc_success_already&order_number=". $OrderID );
					}
					break;
				case "shop":
					//$uah = new Currency( $db->getone("select id from currencies where code='980'") );
					$uah = new Currency(); $uah->LoadByCode('980');
					$order = new ShopOrder(); $order->LoadByNumber( $OrderID );
					$order->payed = $order->payed + $totalAmount/(100*$uah->exchange_rate);
					$order->status = 1;
					//$order->xid = $xid;
					//$order->rrn = $rrn;
					$order->Save();
					$log->Write( "base.upc_success\nOrder N({$OrderID}) was saved with status=1. Payed {$order->payed}. Client ID={$order->client_id}" );
					redirect( "http://{$order_host}/?shop.upc_success&order_number=". $OrderID );
					break;
				
			}
		}
		else
		{
			$xml .= "<signature_failed>1</signature_failed>";
			$xml .= "<message>". $this->GetUPCResultMessage(-1) ."</message>";
			$log->Write( "base.upc_success\nVerifying is failed" );
			
			switch( $order_type )
			{
				case "sop":
					redirect( "http://{$order_host}/?photos.upc_failure&tranCode=-1"  );
					break;
				case "shop":
				default:
					redirect( "http://{$order_host}/?shop.upc_failure&tranCode=-1" );
					break;
			}
		}
		$xml .= "</data>";
	}
	
	protected function GetUPCResultMessage( $tranCode )
	{
		$retval = '';
		
		switch( $tranCode )
		{
			case -1:
				$retval = $this->dictionary->get('/locale/upc/messages/error_verify');
				break;
			case 0:
				$retval = $this->dictionary->get('/locale/upc/messages/code0');
				break;
			case 105:
				$retval = $this->dictionary->get('/locale/upc/messages/code105');
				break;
			case 116:
				$retval = $this->dictionary->get('/locale/upc/messages/code116');
				break;
			case 111:
				$retval = $this->dictionary->get('/locale/upc/messages/code111');
				break;
			case 108:
				$retval = $this->dictionary->get('/locale/upc/messages/code108');
				break;
			case 101:
				$retval = $this->dictionary->get('/locale/upc/messages/code101');
				break;
			case 130:
				$retval = $this->dictionary->get('/locale/upc/messages/code130');
				break;
			case 290:
				$retval = $this->dictionary->get('/locale/upc/messages/code290');
				break;
			case 291:
				$retval = $this->dictionary->get('/locale/upc/messages/code291');
				break;
			default:
				$retval = $this->dictionary->get('/locale/upc/messages/error');
				break;
		}
		
		return( $retval );
	}
	
	function upc_failure()
	{
		$OrderID = $_REQUEST['OrderID'];
		$tranCode = strval( $_REQUEST['TranCode'] );
		if( !$tranCode ) $tranCode = 291;
		$SD = $_REQUEST['SD'];
		list( $order_type, $order_host ) = split( "\|", $SD );
		
		switch( $order_type )
		{
			case "sop":
				redirect( "http://{$order_host}/?photos.upc_failure&order_id={$OrderID}&tranCode={$tranCode}" );
				break;
			case "shop":
				redirect( "http://{$order_host}/?shop.upc_failure&order_id={$OrderID}&tranCode={$tranCode}" );
				break;
		}
	}
	
	function upc_notify()
	{
		global $log;
		
		$log->Write( "Running base.upc_notify" );
		
		//$MerchantID = '1752256';  //test-server
		$MerchantID = '6984785';
		//$TerminalID = 'E7880056';  //test-server
		$TerminalID = 'E0133767';
		$OrderID = $_REQUEST['OrderID'];
		$SD = $_REQUEST['SD'];
		list( $order_type, $order_host ) = split( "\|", $SD );
		$xid = $_REQUEST['XID'];
		$rrn = $_REQUEST['Rrn'];
		$totalAmount = $_REQUEST['TotalAmount'];
		$altTotalAmount = $_REQUEST['AltTotalAmount'];
		$tranCode = strval( $_REQUEST['tranCode'] );
		$ApprovalCode = $_REQUEST['ApprovalCode'];
		$PurchaseTime = $_REQUEST['PurchaseTime'];
		$Currency = $_REQUEST['Currency'];
		$AltCurrency = $_REQUEST['AltCurrency'];
		$Delay = $_REQUEST['Delay']; if( $Delay ) $Delay = ','. $Delay;
		$signature = $_REQUEST['Signature'];
		$data = "$MerchantID;$TerminalID;$PurchaseTime;$OrderID{$Delay};$xid;$Currency;$totalAmount;$SD;". sprintf("%03s",$tranCode) .";$ApprovalCode;";
		if( $AltCurrency && $AltCurrency != $Currency )
		{
			$data = "$MerchantID;$TerminalID;$PurchaseTime;$OrderID{$Delay};$xid;$Currency,$AltCurrency;$totalAmount,$altTotalAmount;$SD;". sprintf("%03s",$tranCode) .";$ApprovalCode;";
		}
		$log->Write( "base.upc_notify\ndata=". $data );
		//$fp = fopen( SITEROOT ."/upc/test-server.cert", "r" );  //test-server
		$fp = fopen( SITEROOT ."/upc/work-server.cert", "r" );
		$ok = 0;
		if( $fp )
		{
			$key = fread( $fp, 8192 );
			fclose( $fp );
			$pkeyid = openssl_pkey_get_public( $key );
			$signature = base64_decode( $signature );
			$ok = openssl_verify( $data , $signature, $pkeyid );
			openssl_free_key( $pkeyid );
		}
		$log->Write( "base.upc_notify\nopenssl_verify returned ". $ok );
		
		$xml = "<data>";
		$xml .= "<OrderID>{$OrderID}</OrderID>";
		$xml .= "<xid><![CDATA[{$xid}]]></xid>";
		if( $ok == 1 )
		{
			$xml .= "<signature_failed>0</signature_failed>";
			$xml .= "<message>". $this->GetUPCResultMessage($tranCode) ."</message>";
			$log->Write( "base.upc_notify\nVerifying passed successfully, order_type=". $order_type );
			
			switch( $order_type )
			{
				case "sop":
					$order = new SOPOrder(); $order->LoadByNumber( $OrderID );
					if( $order->status == 0 )
					{
						$order->payed = $order->payed + $totalAmount/100;
						$order->status = 1;
						$order->Save();
						$log->Write( "base.upc_notify\nOrder N({$OrderID}) was saved with status=1. Payed {$order->payed}. Client ID={$order->client_id}" );
						
						$this->sop_save_order( $order );
						
						$notify_response = "MerchantID=". $MerchantID ."\n";
						$notify_response .= "TerminalID=". $TerminalID ."\n";
						$notify_response .= "OrderID=". $OrderID ."\n";
						$notify_response .= "Currency=". $Currency ."\n";
						$notify_response .= "TotalAmount=". $totalAmount ."\n";
						$notify_response .= "XID=". $xid ."\n";
						$notify_response .= "PurchaseTime=". $PurchaseTime ."\n";
						$notify_response .= "Response.action=approve\n";
						$notify_response .= "Response.reason=\n";
						$notify_response .= "Response.forwardUrl=\n";
						
						$log->Write( "base.upc_notify\nNotify response\n". $notify_response );
						echo $notify_response;
					}
					else
					{
						$log->Write( "base.upc_notify\nOrder N({$OrderID}) was already saved with status=1. Payed {$order->payed}. Client ID={$order->client_id}" );
					}
					break;
				case "printbook":
					$order = new PBOrder();
					$order->LoadByNumber( $OrderID );
					if( $order->status == 0 ) {
						$order->payed = $order->payed + $totalAmount/100;
						$order->status = 1;
						$order->Save();
						$log->Write( "base.upc_notify\nOrder N({$OrderID}) was saved with status=1. Payed {$order->payed}. Client ID={$order->client_id}" );

						// $this->printbook_save_order();
					}
					break;
				case "shop":
				/*
					//$uah = new Currency( $db->getone("select id from currencies where code='980'") );
					$uah = new Currency(); $uah->LoadByCode('980');
					$order = new ShopOrder(); $order->LoadByNumber( $OrderID );
					$order->payed = $order->payed + $totalAmount/(100*$uah->exchange_rate);
					$order->status = 1;
					//$order->xid = $xid;
					//$order->rrn = $rrn;
					$order->Save();
					$log->Write( "base.upc_success\nOrder N({$OrderID}) was saved with status=1. Payed {$order->payed}. Client ID={$order->client_id}" );
					redirect( "http://{$order_host}/?shop.upc_success&order_number=". $OrderID );
				*/
					break;
				
			}
		}
		else
		{
			$xml .= "<signature_failed>1</signature_failed>";
			$xml .= "<message>". $this->GetUPCResultMessage(-1) ."</message>";
			$log->Write( "base.upc_notify\nVerifying is failed" );
			
			switch( $order_type )
			{
				case "sop":
					break;
				case "shop":
				default:
					break;
			}
		}
		$xml .= "</data>";
	}
	
	function sop_save_order( &$order )
	{
		if( $order->IsLoaded() )
		{
				//Send email to admin and client
				$settings = new Settings();
				$sop_settings = new SOPSettings();
				
				if( $order->client->email ) {
					$mail = new AMail( 'windows-1251' );
						$subject = $sop_settings->order_mail_subject;
						$subject = str_replace( "{ORDER_NR}", $order->order_number, $subject );
						$mail->subject = $subject;
						
						$mail->from_name = $settings->from_name;
						$mail->from_email = $settings->from_email;
						$mail->to_email = $order->client->email;
						$body = $sop_settings->order_mail_body;
						$body = str_replace( "{CLIENT}", $order->client->firstname.' '.$order->client->lastname, $body );
						$body = str_replace( "{ORDER_NR}", $order->order_number, $body );
						$body = str_replace( "{ORDER_STATUS}", $this->SOPGetOrderStatusStr( $order ), $body );
						//$body = str_replace( "{ORDER}", $this->GetOrderSummaryTable( $order ), $body );
						$body = str_replace( "{ORDER}", $this->SOPGetOrderSummaryTableExtended( $order ), $body );
					$mail->Send( $body );
				}
				
				$mail = new AMail( 'windows-1251' );
					$subject = $sop_settings->admin_order_mail_subject;
					$subject = str_replace( "{ORDER_NR}", $order->order_number, $subject );
					$mail->subject = $subject;
					
					$mail->from_name = $settings->from_name;
					$mail->from_email = $settings->from_email;
					$mail->to_email = $sop_settings->admin_emails;
					$body = $sop_settings->admin_order_mail_body;
					$body = str_replace( "{CLIENT}", $order->client->firstname.' '.$order->client->lastname, $body );
					$body = str_replace( "{ORDER_NR}", $order->order_number, $body );
					$body = str_replace( "{ORDER_STATUS}", $this->SOPGetOrderStatusStr( $order ), $body );
					//$body = str_replace( "{ORDER}", $this->GetOrderSummaryTable( $order ), $body );
					$body = str_replace( "{ORDER}", $this->SOPGetOrderSummaryTableExtended( $order ), $body );
				$mail->Send( $body );
				
				//Move order files into tree structure of the folders
				$src_dir = $order->dir . $order->order_number .'/';
				$order->order_number = sprintf( "%s-%s-%s", $order->order_number, $order->delivery->order_prefix, $order->point->order_prefix );
				$border_prefix = '';
				if( $order->border==1 )
				{
					$border_prefix = '_frame';
					$order->order_number .= '-FRAME';
				}
				$order->Save();
				$md = new MysqlDateTime( $order->created_at );
				$dir = sprintf( "%s%s/%s", $order->dir, $md->GetFrontEndValue('y.m.d'), $order->order_number );
				mkdir_r( $dir, 0775 );
				$dir .= '/';
				$order->LoadPhotos();
				$i = 1;
				$files_success = 0;
				foreach( $order->photos as $p )
				{
					$new_dir = $dir . convert_encoding('utf-8','windows-1251',$p->size->name) .'/'. $order->GetPaperName();
					mkdir_r( $new_dir );
					$new_dir .= '/';
					$new_name = sprintf( "%03d_%s_%s%s_%03d.%s", $i, convert_encoding('utf-8','windows-1251',$p->size->name), $order->GetPaperPrefix(), $border_prefix, $p->quantity, get_file_ext($p->filename) );
					if( copy( $src_dir.$p->filename, $new_dir.$new_name ) )
					{
						chmod( $new_dir.$new_name, 0664 );
						$files_success++;
					}
					$i++;
				}
				if( $files_success > 0 && $files_success == count($order->photos) )
				{
					rmdir_rf( $src_dir );
				}
		}
	}
	
	protected function SOPGetOrderSummaryTableExtended( &$order )
	{
		$retval = '';
		
		if( $order->IsLoaded() )
		{
			$retval .= '<table style="border: 1px solid #e3bf85" cellspacing="3">';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/order_number2') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->order_number .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/created_at') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->created_at .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/updated_at') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->updated_at .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/paymentway_id') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->paymentway->name .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/delivery_id') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->delivery->name .'</td>';
			$retval .= '</tr>';
			
			if( $order->delivery_id == 1 )
			{
				$retval .= '<tr>';
				$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/point_name') .'</b></td>';
				$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->point->name .', '. $order->point->address .','. $order->point->phone .'</td>';
				$retval .= '</tr>';
			}
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/comments') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;color:#d80c76">'. $order->comments .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">&#160;</td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">&#160;</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/client') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->client->firstname .'&#160;'. $order->client->lastname .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/email') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->client->email .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/address_v') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->client->city .', '. $order->client->street->name .', '. $order->client->address .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/address2_v') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;color:#d80c76">'. $order->address .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/phone') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->client->phone .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">&#160;</td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">&#160;</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/photos') .'</b></td>';
			//$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->photos_count . $this->dictionary->get('/locale/common/items_amount') .'</td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $this->GetOrderSummaryTablePhotos( $order ) .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/photo_paper') .'</b></td>';
			$photo_paper = ($order->photo_paper == 1)? $this->dictionary->get('/locale/photos/order/photo_paper1') : $this->dictionary->get('/locale/photos/order/photo_paper2');
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $photo_paper .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/border') .'</b></td>';
			$border = ($order->border == 1)? $this->dictionary->get('/locale/common/exist') : $this->dictionary->get('/locale/common/no');
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $border .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/total_photos') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->photos_price_amount .' '. $this->dictionary->get('/locale/common/currency') .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/total_delivery') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. sprintf( "%01.2f", $order->GetDeliveryPrice() ) .' '. $this->dictionary->get('/locale/common/currency') .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/total_discount') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. sprintf( "%01.2f", $order->discount_price ) .' '. $this->dictionary->get('/locale/common/currency') .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/total_price') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. sprintf( "%01.2f", $order->total_price ) .' '. $this->dictionary->get('/locale/common/currency') .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/payed_price') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. sprintf( "%01.2f", $order->payed ) .' '. $this->dictionary->get('/locale/common/currency') .'</td>';
			$retval .= '</tr>';
			
			$retval .= '</table>';
		}
		
		return( $retval );
	}
	
	protected function SOPGetOrderStatusStr( &$order )
	{
		$retval = '';
		
		if( $order->IsLoaded() )
		{
			switch( $order->status )
			{
				case 1:
					$retval = $this->dictionary->get('/locale/my/orders/photo/statuses/s1');
					break;
				case 2:
					$retval = $this->dictionary->get('/locale/my/orders/photo/statuses/s2');
					break;
				case 3:
					$retval = $this->dictionary->get('/locale/my/orders/photo/statuses/s3');
					break;
				case 4:
					$retval = $this->dictionary->get('/locale/my/orders/photo/statuses/s4');
					break;
				case 5:
					$retval = $this->dictionary->get('/locale/my/orders/photo/statuses/s5');
					break;
				case 6:
					$retval = $this->dictionary->get('/locale/my/orders/photo/statuses/s6');
					break;
			}
		}
		
		return( $retval );
	}
	
	/* advertisement 1 */
	function adv1()
	{
		$img_path = "http://kollage.com.ua/data/";
		$img_adv_path = "http://kollage.com.ua/adv1/";
		$site_url = "http://kollage.com.ua/";
		$product_url = $site_url ."?shop.product&id=";
		$js = "document.write(\"<table width='100%' height='100px'><tr>";
		
		$images = Array();
		$dh = opendir( SITEROOT ."/adv1/" );
		if( $dh )
		{
			while( ($file = readdir($dh)) !== false )
			{
				$ext = strtolower(GetFileExtension($file));
				if( $ext == "jpg" || $ext == "gif" || $ext == "png" )
				{
					array_push( $images, $file );
				}
			}
		}
		
		$images_count = count( $images );
		$block1_image = ""; $block4_image = "";
		$this->rows_per_page = 1;
		if( $images_count == 1 )
		{
			$block1_image = $images[0];
			$this->rows_per_page = 3;
		}
		else if( $images_count == 2 )
		{
			$block1_image = $images[0];
			$block4_image = $images[1];
			$this->rows_per_page = 1;
		}
		else if( $images_count > 1 )
		{
			$i1 = mt_rand( 0, $images_count-1 );
			$images2 = Array();
			for( $i=0; $i<$images_count; $i++ )
			{
				if( $i != $i1 ) array_push( $images2, $images[$i] );
			}
			$images2_count = count( $images2 );
			$i2 = mt_rand( 0, $images2_count-1 );
			
			$block1_image = $images[$i1];
			$block4_image = $images2[$i2];
			
			$this->rows_per_page = 1;
		}
		
		if( $block1_image )
		{
			$js .= "<td style='border:1px solid #cccccc;width:33%;height:100px;text-align:center'>";
			$js .= "<a href='{$site_url}' target='kollage_site'><img src='{$img_adv_path}{$block1_image}' border='0' height='100' align='center'/></a>";
			$js .= "</td>";
		}
		
		$uah = new Currency(); $uah->LoadByCode('980');
		$t = new MysqlTable('shop_products');
		$t->find_all( "status=1 and show_in_best=1", "rand()", $this->rows_per_page );
		foreach( $t->data as $row )
		{
			$o = new ShopProduct( $row['id'], false );
			$price = sprintf( "%0.2f%s", $o->price * $uah->exchange_rate, $this->adv_convert( convert_encoding("UTF-8","Windows-1251",$uah->symbol) ) );
			$js .= "<td style='border:1px solid #cccccc;width:34%;height:100px;background-color:#fff'>";
			$js .= "<img src='{$img_path}". $o->images[0]->tiny ."' height='60px' border='0' align='left'/>";
			$js .= "<a target='kollage_product' href='". $product_url . $o->id ."' style='font-family:Arial;font-size:11px;font-weight:bold;color:#000;text-decoration:none'><span>". $this->adv_convert( convert_encoding("UTF-8","Windows-1251",$o->name) ) ."</span></a>";
			$js .= "<br><a target='kollage_product' href='". $product_url . $o->id ."' style='font-family:Arial;font-size:11px;color:#000;text-decoration:none'><span>". $this->adv_convert( convert_encoding("UTF-8","Windows-1251",$o->ingress) ) ."</span></a>";
			$js .= "<br><a target='kollage_product' href='". $product_url . $o->id ."' style='font-family:Arial;font-size:11px;color:#ff5252;text-decoration:none'><span>". $price ."</span></a>";
			$js .= "</td>";
		}
		
		if( $block4_image )
		{
			$js .= "<td style='border:1px solid #cccccc;width:33%;height:100px;text-align:center'>";
			$js .= "<a href='{$site_url}' target='kollage_site'><img src='{$img_adv_path}{$block4_image}' border='0' height='100' align='center'/></a>";
			$js .= "</td>";
		}
		
		$js .= "</tr></table>\");";
		
		echo $js;
	}
	
	function adv_convert( $str )
	{
		return( str_replace( "\"", "&quot;", $str ) );
	}
	/* advertisement 1 */
	
	/* account */
	function account( $active_panel='', $change_password = Array( result=>0, message=>''), $change_properties = Array( result=>0, message=>'') ) {
		global $db, $log;
		$this->method_name = 'account';
		
		if( $this->client->IsLoaded() ) {
			$xml = "<data>";
			
			$xml .= "<active_panel>{$active_panel}</active_panel>";
			
			$xml .= "<change_properties>";
				$xml .= "<result>{$change_properties['result']}</result>";
				$xml .= "<message>{$change_properties['message']}</message>";
			$xml .= "</change_properties>";
			
			$xml .= "<change_password>";
				$xml .= "<result>{$change_password['result']}</result>";
				$xml .= "<message>{$change_password['message']}</message>";
			$xml .= "</change_password>";
			
			$xml .= "<userfiles>";
			$sql = "select id from userfiles where client_id={$this->client->id} order by position,id";
			$rs = $db->query( $sql );
			while( $row = $db->getrow($rs) )
			{
				$o = new UserFile( $row[0] );
				$xml .= $o->Xml();
			}
			$xml .= "</userfiles>";
			
			$xml .= "<purse>";
			$t = new MysqlTable( 'purse_operations' );
			$t->find_all( "client_id={$this->client->id}", 'id desc' );
			foreach( $t->data as $a )
			{
				$o = new PurseOperation( $a['id'] );
				$xml .= $o->Xml();
			}
			$xml .= "</purse>";
			
			$xml .= "<orders_photo>";
			$t = new MysqlTable('sop_orders');
			$t->find_all( "client_id={$this->client->id} and status>0 and status<5", "id desc" );
			foreach( $t->data as $row )
			{
				$o = new SOPOrder($row['id']);
				$xml .= $o->Xml();
			}
			$xml .= "</orders_photo>";
			
			$xml .= "<orders_shop>";
			$t = new MysqlTable('shop_orders');
			$t->find_all( "client_id={$this->client->id} and status>0", "id desc" );
			foreach( $t->data as $row )
			{
				$o = new ShopOrder($row['id'], true);
				$xml .= $o->Xml();
			}
			$xml .= "</orders_shop>";
			
			$xml .= "<printbook>";
			$t = new MysqlTable('pb_orders');
			$t->find_all( "client_id={$this->client->id} and status>0", "id desc" );
			foreach( $t->data as $row )
			{
				$o = new PBOrder($row['id'], true);
				$xml .= $o->Xml();
			}
			$xml .= "</printbook>";
			
			$xml .= "</data>";
			$this->display( 'account', $xml );
		}
		else {
			$this->logout();
		}
	}
	/* account */
	
	/* user files */
	function add_userfiles_flash()
	{
		global $log, $db;
		
		if( $this->client->IsLoaded() )
		{
			set_time_limit(3600);
			header("Expires: 0");
			
			$msg = "";
			$err = 0; $files = 0; $files_err = 0;
			$file_prefix = 'photo'; $i = 1;
			$log->Write( "add_userfiles_flash() starting _FILES=". serialize($_FILES) );
			if( $_FILES[$file_prefix.$i] && $_FILES[$file_prefix.$i]['tmp_name']!='' && is_uploaded_file($_FILES[$file_prefix.$i]['tmp_name']) )
			{
				$files++;
				$description = trim( $_REQUEST['description'] );
				$name = $_FILES[$file_prefix.$i]['name'];
				$filename = md5( time() . rand(1,99) );
				$dir = SITEROOT .'/'. $_ENV['userfiles_path'] .'/';
				$file = $dir . $filename;
				
				$log->Write( "add_userfiles_flash() uploading file={$file}" );
				
				if( move_uploaded_file($_FILES[$file_prefix.$i]['tmp_name'], $file ) )
				{
					chmod( $file, 0664 );
			
					$o = new UserFile();
					$o->client_id = $this->client->id;
					$o->name = $name;
					$o->filename = $filename;
					$o->description = $description;
					$o->position = $o->GetLastPosition("client_id={$this->client->id}") + 1;
					$o->Save();
					
					$this->add_userfiles_mail( $o );
					$log->Write( "add_userfiles_flash() chmod file={$file} and store in the DB" );
				}
			}
			
			if( $files == 0 )
			{
				$err = 3;
				$msg = $this->dictionary->get('/locale/photos/order/add_photos/error_upload_nofiles');
			}
		}
		else
		{
			$this->logout();
		}
	}

	function add_userfiles_fine_uploader()
	{
		global $log, $db;
		
		if( $this->client->IsLoaded() )
		{
			set_time_limit(3600);
			header("Expires: 0");
			header("Content-Type: application/json");
			
			$msg = "";
			$err = 0; $files = 0; $files_err = 0;
			$file_prefix = 'qqfile';
			$log->Write( "add_userfiles_fine_uploader() starting _FILES=". serialize($_FILES) );
			if( $_FILES[$file_prefix] && $_FILES[$file_prefix]['tmp_name']!='' && is_uploaded_file($_FILES[$file_prefix]['tmp_name']) )
			{
				$files++;
				$description = trim( $_REQUEST['description'] );
				$name = $_FILES[$file_prefix]['name'];
				$filename = md5( time() . rand(1,99) );
				$dir = SITEROOT .'/'. $_ENV['userfiles_path'] .'/';
				$file = $dir . $filename;
				
				$log->Write( "add_userfiles_fine_uploader() uploading file={$file}" );
				
				if( move_uploaded_file($_FILES[$file_prefix]['tmp_name'], $file ) )
				{
					chmod( $file, 0664 );
			
					$o = new UserFile();
					$o->client_id = $this->client->id;
					$o->name = $name;
					$o->filename = $filename;
					$o->description = $description;
					$o->position = $o->GetLastPosition("client_id={$this->client->id}") + 1;
					$o->Save();
					
					$this->add_userfiles_mail( $o );
					$log->Write( "add_userfiles_fine_uploader() chmod file={$file} and store in the DB" );
				}
			}
			
			if( $files == 0 )
			{
				$err = 3;
				$msg = $this->dictionary->get('/locale/photos/order/add_photos/error_upload_nofiles');
			}

			$response = array(
				'success' => $err === 0,
			);
			if ($err) {
				$response['error'] = $msg;
			}
			echo json_encode($response);	
		}
		else
		{
			$this->logout();
		}
	}
	
	function add_userfiles_mail( &$uf )
	{
		if( $this->client->IsLoaded() && $uf->IsLoaded() )
		{
			$settings = new Settings();
			$mail = new AMail( 'windows-1251' );
				$subject = $this->dictionary->get('/locale/my/files/mail/subject') . $this->client->firstname.' '.$this->client->lastname;
				$mail->subject = $subject;
				
				$mail->from_name = $settings->from_name;
				$mail->from_email = $settings->from_email;
				$mail->to_email = "manager@kollage.com.ua, druk@kollage.com.ua";
				//$mail->to_email = "anebogin@gmail.com";
				$body = "";
				$body .= "<br>". $this->dictionary->get('/locale/my/files/mail/date') . $uf->created_at;
				$body .= "<br>". $this->dictionary->get('/locale/my/files/mail/client'). $this->client->firstname.' '.$this->client->lastname;
				$body .= "<br>". $this->dictionary->get('/locale/my/files/mail/phone') . $this->client->phone;
				$body .= "<br>";
				$body .= "<br>". $this->dictionary->get('/locale/my/files/mail/url') . "http://kollage.com.ua/admin/?clients.download_userfile&id=". $uf->id;
				$body .= "<br>". $this->dictionary->get('/locale/my/files/mail/filename') . $uf->filename;
				$body .= "<br>". $this->dictionary->get('/locale/my/files/mail/description') . $uf->description;
			$mail->Send( $body );
		}
	}
	
	function del_userfile()
	{
		$id = intval( $_REQUEST['id'] );
		if( $id > 0 && $this->client->IsLoaded() )
		{
			$o = new UserFile( $id );
			$o->Delete();
			redirect( '/base/account#my-files' );
		}
		else
		{
			$this->logout();
		}
	}
	
	function download_userfile()
	{
		global $bd;
		
		$id = intval( $_REQUEST['id'] );
		if( $id > 0 && $this->client->IsLoaded() )
		{
			$o = new UserFile( $id );
			if( $this->client->id == $o->client_id )
			{
				if( $bd->IsFirefox() ) $name = "=?UTF-8?B?". base64_encode( $o->name ) ."?=";
				else if( $bd->IsIE() ) $name = convert_encoding( "UTF-8", "windows-1251//TRANSLIT", $o->name );
				else $name = $o->name;
				
				$o->status = 1;
				$o->Save();
				
				put_file_into_browser( $o->dir . $o->filename, $name );
			}
		}
		else
		{
			$this->logout();
		}
	}
	
	function my_files_changegroup()
	{
		global $log, $db;
		
		if( $this->client->IsLoaded() )
		{
			$ids = trim( $_REQUEST['ids'] );
			$description = trim( $_REQUEST['description'] );
			$changegroup_group = intval( $_REQUEST['changegroup_group'] );
			if( $changegroup_group == 1 && $ids != '' )
			{
				//change selected
				$sql = "update userfiles set description='{$description}' where client_id={$this->client->id} and id in ({$ids})";
				$db->query( $sql );
			}
			else if( $changegroup_group == 2 )
			{
				//change all
				$sql = "update userfiles set description='{$description}' where client_id={$this->client->id}";
				$db->query( $sql );
			}
			
			redirect( '/base/account#my-files' );
		}
		else
		{
			$this->logout();
		}
	}
	/* user files */
	
	/* my properties */
	function my_properties_save()
	{
		$result = 0;
		$message = '';
		
		$firstname = $_REQUEST['firstname'];
		$lastname = $_REQUEST['lastname'];
		$email = $_REQUEST['email'];
		$country = $_REQUEST['country'];
		$phone = $_REQUEST['phone'];
		$send_news = intval( $_REQUEST['send_news'] );
		
		if( $firstname && $lastname && $country && $phone )
		{
			$clientCheck = new Client();
			$clientCheck->LoadByEmail( $email );
			if( $clientCheck->IsLoaded() && $clientCheck->id != $this->client->id ) {
				$result = 3;
				$message = $this->dictionary->get('/locale/my/properties/change_properties/messages/error_client_email_exist');
			}
			else {
				$clientCheck->LoadByPhone( $country, $phone );
				if( $clientCheck->IsLoaded() && $clientCheck->id != $this->client->id ) {
					$result = 4;
					$message = $this->dictionary->get('/locale/my/properties/change_properties/messages/error_client_phone_exist');
				}
				else {
					$this->client->firstname = $firstname;
					$this->client->lastname = $lastname;
					$this->client->email = $email;
					$this->client->country_id = $country;
					$this->client->phone = $phone;
					$this->client->send_news = $send_news;
					if( $this->client->Save() )
					{
						$result = 1;
						$message = $this->dictionary->get('/locale/my/properties/change_properties/messages/saved');
					}
				}
			}
		}
		else {
			$result = 2;
			$message = $this->dictionary->get('/locale/my/properties/change_properties/messages/error_wrong_parameters');
		}
		
		$this->account( 'div#account-my-properties', null, Array( 
			result=>$result, 
			message=>$message
		) );
	}
	/* my properties */
	
	/* my properties -> password */
	function my_properties_change_password()
	{
		$result = 0;
		$message = '';
		
		$old_password = $_REQUEST['old_password'];
		$new_password = $_REQUEST['new_password'];
		$confirm_new_password = $_REQUEST['confirm_new_password'];
		
		if( $old_password && $new_password && $confirm_new_password)
		{
			if ($this->client->password != $old_password)
			{
				$message = $this->dictionary->get('/locale/my/properties/change_password/messages/error_bad_old_password');
			}
			elseif ($new_password != $confirm_new_password)
			{
				$message = $this->dictionary->get('/locale/my/properties/change_password/messages/error_bad_new_passwords');
			}
			else
			{
				$this->client->password = $new_password;
				if( $this->client->Save() )
				{
					$result = 1;
					$message = $this->dictionary->get('/locale/my/properties/change_password/messages/passwords_saved');
				}
			}
		}
		else {
			$result = 2;
			$message = $this->dictionary->get('/locale/my/properties/change_password/messages/error_wrong_parameters');
		}
		
		$this->account( 'div#account-my-properties', Array( 
			result=>$result, 
			message=>$message
		) );
	}
	/* my properties -> password */
	
	/* KPI */
	public function kpi_import()
	{
		if( $this->kpi_download_xml() ) {
			$this->kpi_parse_xml();
		}
	}
	
	private function kpi_download_xml()
	{
		global $log;
		$retval = false;
		
		$log->Write( "base.kpi_download_xml was called" );
		$url = "www.trade.kpiservice.com.ua/?tms=". time() ."&sid=5040122054054060059060040117056055054&xml=1&query=top_tovary";
		$xml_file = "kpi.xml";
		$get = "/usr/local/bin/wget -q -O {$xml_file} ";
		chdir("kpi");
		@unlink( $xml_file );
		exec( $get ."'". $url ."'" );
		if( file_exists($xml_file) ) {
			$retval = true;
			$log->Write( "base.kpi_download_xml XML was downloaded" );
		}
		
		return( $retval );
	}
	
	private function kpi_parse_xml() {
		global $log;
		$log->Write( "base.kpi_parse_xml was called" );
		$parser = xml_parser_create();
		xml_set_element_handler( $parser, Array( $this, "kpi_parse_xml_start" ), Array( $this, "kpi_parse_xml_end" ) );
		xml_set_character_data_handler( $parser, Array( $this, "kpi_parse_xml_data" ) );
		$xml_file = "kpi.xml";
		if( $fp = fopen($xml_file, "r") ) {
			while( $data = fread($fp, 8192) ) {
				if( !xml_parse($parser, $data, feof($fp)) ) {
					$log->Write( "base.kpi_parse_xml ". sprintf( "XML error: %s at line %d", xml_error_string(xml_get_error_code($xml_parser)), xml_get_current_line_number($xml_parser)) );
				}
			}
		}
		else {
			$log->Write( "base.kpi_parse_xml error open file {$xml_file}" );
		}
	}
	
	private function kpi_parse_xml_start( $parser, $name, $attrs )
	{
		global $log;
		$this->kpi_state = $name;
	}
	
	private function kpi_parse_xml_end( $parser, $name )
	{
		global $log;
		$this->kpi_state = "";
		if( strtolower($name) == "new_row" ) {
			echo "<br/>". trim($this->kpi_data['code_tovar']);
		}
	}
	
	private function kpi_parse_xml_data( $parser, $data )
	{
		global $log;
		//echo $this->kpi_state;
		if( $this->kpi_state ) {
			switch( strtolower($this->kpi_state) ) {
				case "code_tovar":
					$this->kpi_data['code_tovar'] = $data;
					break;
			}
		}
	}
	/* KPI */
			 
	function get_any_request_protect_image_new()
	{
		$code = new ProtectCode( 6, PC_NUMERIC );
		$_SESSION['any_request_protect_code'] = $code->GetCode();
		if( $_SESSION['any_request_protect_code'] )
		{
			$obj = new ProtectImage( $_SESSION['any_request_protect_code'] );
			$obj->PrintImage();
		}
	}    

	/*
		Îòïðàâèòü ïèñüìî ìåíåäæåðó ñ çàïðîñîì êëèåíòà ñî ñòðàíèöû ïîèñêà ïðè íåãàòèâíîì ðåçóëüòàòå ïîèñêà
	*/
	function json_search_send_message()
	{
		$retval = new StdClass();
			   
		$this->method_name = 'json_any_request';
		
		$name = trim(strip_tags($_REQUEST['name']));
		$phone = trim(strip_tags($_REQUEST['phone']));
		$email = trim(strip_tags($_REQUEST['email']));
		$comment = trim(strip_tags($_REQUEST['comment']));
		$protect_code = $_REQUEST['protect_code'];

		if ($_SESSION['any_request_protect_code'] != $protect_code)
			$retval->result = -2;//íåâåðíûé ïðîâåðî÷íûé êîä
		elseif ( $name and $phone and $comment) 
		{
			//send email to manager
			$settings = new Settings();
			$shop_settings = new ShopSettings();
			
			$mail = new AMail( 'utf-8' );
				$mail->subject = "Kollage site. Request from search page, from client.";
				$mail->from_name = $settings->from_name;
				$mail->from_email = $settings->from_email;
				$mail->to_email = $shop_settings->admin_emails;
				$body = sprintf( "Name: %s<br/>Email: %s<br/>Phone: %s<br/>Page: %s<br/><br/>%s", $name, $email, $phone, $_SERVER['HTTP_REFERER'], $comment );
			$mail->Send( $body );
			
			$retval->result = 1;//âñ¸ ÎÊ
		}
		else
			$retval->result = -1;//îøèáêà ïàðàìåòðîâ
		
		echo json_encode( $retval );
	}
}

function arr_sort( $a, $b )
{
	if( $a->position == $b->position ) $retval = 0;
	else if( $a->position > $b->position ) $retval = 1;
	else  $retval = -1;
	
	return( $retval );
}

?>
<?php
/*****************************************************
 Class v.1.0, 2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

include_once( 'base_controller.php' );

class billing_gate_controller extends base_controller
{
    var $merchantid = "regulpay";
    var $ips = Array( '77.120.97.36', '193.110.16.220' );
    var $db;
    
    function __construct()
    {
        global $db;
        $this->db = $db;
        
        parent::base_controller();
    }
    
    function main()
    {
        global $log;
        
        $cmd = $_REQUEST['cmd'];
        $merchantid = $_REQUEST['merchantid'];
        
        $log->Write( "billing_gate. main. order=". $_REQUEST['order'] ." cmd=". $cmd ." merchantid=". $merchantid );
        
        if( $this->CheckIP() && $merchantid == $this->merchantid )
        {
            $log->Write( "billing_gate. main. checkup completed." );
            switch( $cmd )
            {
                case 'check_order':
                    $log->Write( "billing_gate. main. call check_order()" );
                    $this->check_order();
                    break;
                case 'check_purse':
                    $log->Write( "billing_gate. main. call check_purse()" );
                    $this->check_purse();
                    break;
                case 'balance':
                    $this->balance();
                    break;
                case 'pay_order':
                    $this->pay_order();
                    break;
                case 'pay_purse':
                    $log->Write( "billing_gate. main. call pay_purse(). ID=". $_REQUEST['id'] ." sum=". $_REQUEST['sum'] );
                    $this->pay_purse();
                    break;
                default:
                    $this->cmd_error();
            }
        }
        else $this->merchant_error();
    }
    
    function CheckIP()
    {
        global $log;
        $retval = false;
        
        for( $i=0; $i<count($this->ips); $i++ )
        {
            if( $_SERVER['REMOTE_ADDR'] == $this->ips[$i] )
            {
                $retval = true;
                break;
            }
        }
        
        $log->Write( "billing_gate. CheckIP(). ip=". $_SERVER['REMOTE_ADDR'] ." retval={$retval}" );
        
        return( $retval );
    }
    
    function merchant_error()
    {
        $xml = "<response>";
        $xml .= "<result>0</result>";
        $xml .= "<resdesc>Wrong merchant ID</resdesc>";
        $xml .= "</response>";
        
        echo $xml;
    }
    
    function cmd_error()
    {
        $xml = "<response>";
        $xml .= "<result>0</result>";
        $xml .= "<resdesc>Wrong command</resdesc>";
        $xml .= "</response>";
        
        echo $xml;
    }
    
    function GetTime()
    {
        $retval = date( "Y-m-d H:i:s" );
        return( $retval );
    }
    
    /*
        parameters:
            order - order number
    */
    function check_order()
    {
        global $log;
        
        $result = 0;
        $resdesc = "������";
        $sum = 0;
        $payed = 0;
        
        $log->Write( "billing_gate. check_order(). begin" );
        
        $order = $_REQUEST['order'];
        list($id,$type) = $this->GetOrderID( $order );
        $payed = $this->OrderIsPayed( $id, $type );
        list($sum,$payed_sum) = $this->GetOrderSum( $id, $type );
        $payed_sum = floatval( $payed_sum );
        if( $id > 0 && !$payed )
        {
            $result = 1;
            $resdesc = "����� N {$order} ����������";
        }
        else if( $id > 0 && $payed  )
        {
            $result = 101;
            $resdesc = "����� N {$order} ��� �������";
        }
        else
        {
            $result = 100;
            $resdesc = "����� N {$order} �� ����������";
        }
        
        $xml = "<response>";
        $xml .= "<result>{$result}</result>";
        $xml .= "<resdesc>{$resdesc}</resdesc>";
        $xml .= "<type>{$type}</type>";
        $xml .= "<sum>". ($sum - $payed_sum) ."</sum>";
        $xml .= "<payed_sum>{$payed_sum}</payed_sum>";
        
        $time = $this->GetTime();
        $xml .= "<time>{$time}</time>";
        
        $xml .= "</response>";
        
        $log->Write( "billing_gate. check_order(). ". $xml );
        
        echo $xml;
    }
    
    function GetOrderID( $order_number )
    {
        $retval = Array( null, null );
        $id = null; $type = null;
        
        $order_number = ltrim( $order_number, " 0\t\n\r\0" );
        
        // paymentway.code == 6 - POS-terminal
        $id = intval( $this->db->getone( "select o.id from sop_orders o left join sop_paymentways p on p.id=o.paymentway_id where o.status in (1,2,3,4) and p.code=6 and o.order_number regexp '^0*{$order_number}-.+$'" ) );
        if( $id == 0 )
        {
            $id = intval( $this->db->getone( "select o.id from shop_orders o left join shop_paymentways p on p.id=o.paymentway_id where o.status in (1,2,3) and p.code=6 and o.order_number regexp '^0*{$order_number}$'" ) );
            if( $id == 0 )
            {
                $id = intval( $this->db->getone( "select o.id from fair_orders o left join fair_paymentways p on p.id=o.paymentway_id where o.status in (1,2,3) and p.code=6 and o.order_number regexp '^0*{$order_number}$'" ) );
                if( $id > 0 ) $type = 3;
            }
            else $type = 2;
        }
        else $type = 1;
        
        if( $id > 0 )
        {
            $retval[0] = $id;
            $retval[1] = $type;
        }
        
        return( $retval );
    }
    
    function GetClientID( $id, $type )
    {
        $retval = 0;
        
        if( $type == 1 )
        {
            $retval = intval( $this->db->getone( "select client_id from sop_orders where id={$id}" ) );
        }
        else if( $type == 2 )
        {
            $retval = intval( $this->db->getone( "select client_id from shop_orders where id={$id}" ) );
        }
        else if( $type == 3 )
        {
            $retval = intval( $this->db->getone( "select client_id from fair_orders where id={$id}" ) );
        }
        
        return( $retval );
    }
    
    function GetTypeDesc( $type )
    {
        $retval = "";
        
        if( $type == 1 ) $retval = $this->dictionary->get('/locale/posterminal_operations/type_desc/td1');
        else if( $type == 2 ) $retval = $this->dictionary->get('/locale/posterminal_operations/type_desc/td2');
        else if( $type == 3 ) $retval = $this->dictionary->get('/locale/posterminal_operations/type_desc/td3');
        
        return( $retval );
    }
    
    /*
        parameters:
            id - ID of the client
    */
    function check_purse()
    {
        global $log;
        
        $result = 0;
        $resdesc = "������";
        
        $id = intval( $_REQUEST['id'] );
        $id2 = $this->db->getone( "select id from clients where status=2 and id={$id}" );
        if( $id2 > 0 )
        {
            $result = 1;
            $resdesc = "���� � ID {$id} ����������";
        }
        else
        {
            $result = 100;
            $resdesc = "���� � ID {$id} �� ����������";
        }
        
        $xml = "<response>";
        $xml .= "<result>{$result}</result>";
        $xml .= "<resdesc>{$resdesc}</resdesc>";
        
        $time = $this->GetTime();
        $xml .= "<time>{$time}</time>";
        
        $xml .= "</response>";
        
        $log->Write( "billing_gate. check_purse(). ". $xml );
        echo $xml;
    }
    
    /*
        parameters:
            date - {������������} ���� �������
    */
    function balance()
    {
        $result = 0;
        $resdesc = "������";
        
        $sum = $this->db->getone( "select sum(amount) from posterminal_operations" );
        if( $sum != null )
        {
            $sum = floatval( $sum );
            $result = 1;
            $resdesc = "������";
        }
        
        $xml = "<response>";
        $xml .= "<result>{$result}</result>";
        $xml .= "<resdesc>{$resdesc}</resdesc>";
        $xml .= "<balance>{$sum}</balance>";
        
        $time = $this->GetTime();
        $xml .= "<time>{$time}</time>";
        
        $xml .= "</response>";
        
        echo $xml;
    }
    
    /*
        parameters:
            order - order number
            point_id - terminal's ID
            sum
            pay_id - transaction ID
    */
    function pay_order()
    {
        $result = 0;
        $resdesc = "������";
        
        $tid = intval( $_REQUEST['pay_id'] );
        $order = $_REQUEST['order'];
        $point_id = $_REQUEST['point_id'];
        $sum = floatval( $_REQUEST['sum'] );
        $tid2 = intval( $this->db->getone( "select id from posterminal_operations where transaction_id='{$tid}'" ) );
        if( $tid2 == 0 )
        {
            $time = $this->GetTime();
            list($id,$type) = $this->GetOrderID( $order );
            $payed = $this->OrderIsPayed( $id, $type );
            if( $sum > 0 )
            {
                if( $id > 0 && !$payed )
                {
                    $check_order_sum = $this->CheckOrderSum( $id, $type, $sum );
                    //if( $check_order_sum >= 1 )
                    //{
                        if( !$tid ) $tid = time() . mt_rand( 100, 999 );
                        $o = new PosterminalOperation();
                        $o->point_id = $point_id;
                        $o->transaction_id = $tid;
                        $o->type = $type;
                        $o->client_id = $this->GetClientID( $id, $type );
                        $o->amount = $sum * -1;
                        $type_desc = $this->GetTypeDesc( $type );
                        $o->description = $this->dictionary->get('/locale/posterminal_operations/pay_order/description');
                        $o->description = str_replace( "{TYPE_DESC}", $type_desc, $o->description );
                        $o->description = str_replace( "{ORDER}", $order, $o->description );
                        $o->Save();
                        
                        if( $check_order_sum == 2 )
                        {
                            list($sum2,$payed2) = $this->GetOrderSum( $id, $type );
                            $o->amount = $sum2 * -1;
                            $o->Save();
                            
                            $this->PayOrder( $id, $type, $sum2-$payed2 );
                            
                            //�������-��������� ����� �� �������
                            $tid = time() . mt_rand( 100, 999 );
                            $o = new PosterminalOperation();
                            $o->point_id = $point_id;
                            $o->transaction_id = $tid;
                            $o->type = $type;
                            $o->client_id = $this->GetClientID( $id, $type );
                            $o->amount = ($sum - $sum2 + $payed2) * -1;
                            $type_desc = $this->GetTypeDesc( $type );
                            $o->description = $this->dictionary->get('/locale/posterminal_operations/pay_order/description_rest');
                            $o->description = str_replace( "{ORDER}", $order, $o->description );
                            $o->Save();
                
                            $p = new PurseOperation();
                            $p->client_id = $o->client_id;
                            $p->amount = $sum - $sum2 + $payed2;
                            $p->description = $this->dictionary->get('/locale/posterminal_operations/pay_purse/description_rest');
                            $p->description = str_replace( "{ORDER}", $order, $p->description );
                            $p->Save();
                        }
                        else
                        {
                            $this->PayOrder( $id, $type, $sum );
                        }
                        
                        //��������� ������ �� ��������� �� ��������� ������ �� ������ ����������
                        if( $type == 1 )
                        {
                            //$this->SOPSendEmail( $id );
                        }
                        
                        $result = 1;
                        $resdesc = "����� N {$order} �������";
                    //}
                    /*
                    else
                    {
                        $result = 102;
                        $resdesc = "����� ������ � N {$order} ������ ��������� �����";
                    }
                    */
                }
                else if( $id > 0 && $payed )
                {
                    $result = 104;
                    $resdesc = "����� � N {$order} ��� �������";
                }
                else
                {
                    $result = 101;
                    $resdesc = "����� � N {$order} �� ����������";
                }
            }
            else
            {
                $result = 100;
                $resdesc = "����� �� �������������";
            }
        }
        else
        {
            $time = $this->db->getone( "select created_at from posterminal_operations where transaction_id='{$tid}'" );
            $result = 103;
            $resdesc = "���������� ��� ���� ���������";
        }
        
        $xml = "<response>";
        $xml .= "<result>{$result}</result>";
        $xml .= "<resdesc>{$resdesc}</resdesc>";
        $xml .= "<order>{$order}</order>";
        $xml .= "<sum>{$sum}</sum>";
        $xml .= "<pay_id>{$tid}</pay_id>";
        
        $xml .= "<time>{$time}</time>";
        
        $xml .= "</response>";
        
        echo $xml;
    }
    
    function PayOrder( $id, $type, $sum )
    {
        if( $type == 1 )
        {
            $this->db->getone( "update sop_orders set payed=payed+{$sum} where id={$id}" );
        }
        else if( $type == 2 )
        {
            $o = new ShopOrder( $id );
            if( $o->IsLoaded() )
            {
                $o->LoadCurrency( 980 );
                $o->payed += $sum / $o->currency->exchange_rate;
                $o->Save();
            }
        }
        else if( $type == 3 )
        {
            $this->db->getone( "update fair_orders set payed={$sum} where id={$id}" );
        }
    }
    
    /*
        return:
            0 - error
            1 -sum is equal the order sum
            -1 -sum is less the order sum
            2 -sum is greater the order sum
    */
    function CheckOrderSum( $id, $type, $sum )
    {
        $retval = 0;
        
        list($sum2,$payed2) = $this->GetOrderSum( $id, $type );
        
        if( $sum2 > 0 )
        {
            if( $sum == ($sum2-$payed2) )
            {
                $retval = 1;
            }
            else if( $sum > ($sum2-$payed2) )
            {
                $retval = 2;
            }
            else if( $sum < ($sum2-$payed2) )
            {
                $retval = -1;
            }
        }
        
        return( $retval );
    }
    
    function GetOrderSum( $id, $type )
    {
        $retval = null;
        $sum = 0;
        $payed = 0;
        
        if( $type == 1 )
        {
            $o = new SopOrder( $id );
            if( $o->IsLoaded() )
            {
                $sum = $o->total_price;
                $payed = $o->payed;
            }
        }
        else if( $type == 2 )
        {
            $o = new ShopOrder( $id );
            if( $o->IsLoaded() )
            {
                $o->LoadCurrency( 980 );
                $sum = $o->amount * $o->currency->exchange_rate;
                $payed = $o->payed;
            }
        }
        else if( $type == 3 )
        {
            $o = new FairOrder( $id );
            if( $o->IsLoaded() )
            {
                $sum = $o->amount;
                $payed = $o->payed;
            }
        }
        
        $retval[0] = $sum;
        $retval[1] = $payed;
        
        return( $retval );
    }
    
    function OrderIsPayed( $id, $type )
    {
        $retval = false;
        $sum = 0;
        
        if( $type == 1 )
        {
            $o = new SopOrder( $id );
            if( $o->IsLoaded() )
            {
                $sum = $o->total_price;
                //if( $o->payed > 0 ) $retval = true;
                if( $o->payed >= $sum ) $retval = true;
            }
        }
        else if( $type == 2 )
        {
            $o = new ShopOrder( $id );
            if( $o->IsLoaded() )
            {
                $o->LoadCurrency( 980 );
                $sum = $o->amount * $o->currency->exchange_rate;
                //if( $o->payed > 0 ) $retval = true;
                if( $o->payed >= $sum ) $retval = true;
            }
        }
        else if( $type == 3 )
        {
            $o = new FairOrder( $id );
            if( $o->IsLoaded() )
            {
                $sum = $o->amount;
                //if( $o->payed > 0 ) $retval = true;
                if( $o->payed >=$sum ) $retval = true;
            }
        }
        
        return( $retval );
    }
    
    /*
        parameters:
            id - client's ID
            point_id - terminal's ID
            sum
            tid - transaction ID
    */
    function pay_purse()
    {
        global $log;
        
        
        $result = 0;
        $resdesc = "������";
        
        $id = intval( $_REQUEST['id'] );
        $tid = intval( $_REQUEST['pay_id'] );
        $point_id = $_REQUEST['point_id'];
        $sum = floatval( $_REQUEST['sum'] );
        $tid2 = intval( $this->db->getone( "select id from posterminal_operations where transaction_id='{$tid}'" ) );
        
        $log->Write( "billing_gate. pay_purse(). id=". $id ." sum=". $sum );
        
        if( $tid2 == 0 )
        {
            $time = $this->GetTime();
            $id2 = intval( $this->db->getone( "select id from clients where status=2 and id={$id}" ) );
            if( $id2 > 0 )
            {
                if( !$tid ) $tid = time() . mt_rand( 100, 999 );
                $o = new PosterminalOperation();
                $o->transaction_id = $tid;
                $o->point_id = $point_id;
                $o->type = 0;
                $o->client_id = $id;
                $o->amount = $sum * -1;
                $type_desc = $this->GetTypeDesc( $type );
                $o->description = $this->dictionary->get('/locale/posterminal_operations/pay_purse/description1');
                $o->description = str_replace( "{ID}", $id, $o->description );
                $o->Save();
                
                $p = new PurseOperation();
                $p->client_id = $o->client_id;
                $p->amount = $sum;
                $p->description = $this->dictionary->get('/locale/posterminal_operations/pay_purse/description2');
                $p->description = str_replace( "{TID}", $tid, $p->description );
                $p->Save();
                
                $result = 1;
                $resdesc = "���� N {$id} �������� �� ����� {$sum}";
            }
            else
            {
                $result = 100;
                $resdesc = "���� N {$id} �� ����������";
            }
        }
        else
        {
            $time = $this->db->getone( "select created_at from posterminal_operations where transaction_id='{$tid}'" );
            $result = 101;
            $resdesc = "���������� ��� ���� ���������";
        }
        
        $xml = "<response>";
        $xml .= "<result>{$result}</result>";
        $xml .= "<resdesc>{$resdesc}</resdesc>";
        $xml .= "<order>{$order}</order>";
        $xml .= "<sum>{$sum}</sum>";
        $xml .= "<pay_id>{$tid}</pay_id>";
        
        $xml .= "<time>{$time}</time>";
        
        $xml .= "</response>";
        
        echo $xml;
    }
}

?>
<?php

define('__ROOT__', dirname(dirname(dirname(__FILE__))) );

include_once('base_controller.php');
include_once( dirname(__FILE__) .'/../../lib/simple_html_dom.php' );
include( __ROOT__ .'/lib/LiqPay.php');

/**
 * Class services_controller
 */
class services_controller extends base_controller
{
	var $pb_order;
	
	const liqpay_merchant_id = 'i2842182438';
	const liqpay_signature = 'CvZKy585e9RJOwwhm3lmSWjtpElCC1LiBTLrd1EeRP';
	// const liqpay_merchant_id = 'I01109RD';
	// const liqpay_signature = 'c79gg19ccd075yb';

	function show()
	{
		global $log;

		$this->method_name = 'show';
		
		$id = intval( $_REQUEST['id'] );
		$article = new Article( $id );
		$category_id = $article->category_id;
		
		$this->make_leftmenu( $category_id, $id );
		
		$xml = "<data>";
		$category = new Category( $category_id );
		$xml .= $category->Xml( true );
		if( $article->status == 1 )
		{
			$isLoggedIn = $this->client && $this->client->IsLoaded();
			$auth_link = "<a href=\"#\" class=\"link-login\" onclick=\"return(false)\">Авторизуйтесь</a> или <a href=\"#\" class=\"link-register\" onclick=\"return(false)\">Зарегистрируйтесь</a><script type=\"text/javascript\">jQuery(function(){Events.publish('/Login/Show');});</script>";

			$article->text = str_replace( '{PRINTBOOK_CALC2}', $this->get_pb_calc2(), $article->text );

			if ($isLoggedIn) { // Client is logged in
				if ($this->client->isProf() || $this->client->isAds() || $this->client->isLargeOpt()) {
					//Client is professional, ads, largeopt
					$article->text = str_replace( '{PRINTBOOK_CALC}', $this->get_pb_calc(), $article->text );
					$article->text = str_replace( '{PRINT_CALCULATOR}', $this->get_pb_calculator(), $article->text );
					$article->text = str_replace( '{REQUEST_PROF}', "", $article->text );
					$article->text = str_replace( '{REQUEST_ADS}', "", $article->text );
				} else {
					$article->text = str_replace( '{PRINTBOOK_CALC}', $this->dictionary->get('/locale/photobook/calc2/msg_send_request'), $article->text );
					$article->text = str_replace( '{PRINT_CALCULATOR}', $this->dictionary->get('/locale/photobook/calc2/msg_send_request'), $article->text );
					$article->text = str_replace( '{REQUEST_PROF}', "<input type='button' class='request-prof' value='Подать заявку'/>", $article->text );
					$article->text = str_replace( '{REQUEST_ADS}', "<input type='button' class='request-ads' value='Подать заявку'/>", $article->text );
				}
			} else { // Client is not logged in
				$article->text = str_replace( '{PRINTBOOK_CALC}', $auth_link, $article->text );
				$article->text = str_replace( '{PRINT_CALCULATOR}', $auth_link, $article->text );
				$article->text = str_replace( '{REQUEST_PROF}',
					"<input type='button' disabled='disabled' value='Подать заявку' /><br/>(для авторизированых пользователей)<br/>". $auth_link,
					$article->text
				);
				$article->text = str_replace( '{REQUEST_ADS}',
					"<input type='button' disabled='disabled' value='Подать заявку' /><br/>(для авторизированых пользователей)<br/>". $auth_link,
					$article->text
				);
			}
				
			$link_exist = preg_match( "/\{LINK.+\}/i", $article->text, $link_matches );
			if ($link_exist) {
				preg_match( "/type\s*=\s*\"([^\"]+)\"/i", $link_matches[0], $link_part_matches );
				$link_type = $link_part_matches[1];
				preg_match( "/link\s*=\s*\"([^\"]+)\"/i", $link_matches[0], $link_part_matches );
				$link_link = $link_part_matches[1];
				preg_match( "/title\s*=\s*\"([^\"]+)\"/i", $link_matches[0], $link_part_matches );
				$link_title = $link_part_matches[1];
				if ($link_type == $this->client->type_id) {
					$article->text = str_replace( $link_matches[0], "<a href=\"{$link_link}\">{$link_title}</a>", $article->text );
				} else {
					$article->text = str_replace( $link_matches[0], "Чтобы увидеть ссылку, вы должны быть авторизированным как Рекламный агент", $article->text );
				}
			}

			if (!$isLoggedIn && !$article->hasClientAccess($this->client)) $article->text = $auth_link;
			else if ($isLoggedIn && !$article->hasClientAccess($this->client)) $article->text = "<strong>". $this->dictionary->get('/locale/services/no_access') ."</strong>";

			$xml .= $article->Xml(true);
		}
		$xml .= "</data>";
		$this->display( 'show', $xml );
	}
	
	function show_contacts()
	{
		$this->method_name = 'show_contacts';
		
		$this->make_leftmenu( $category_id, $id );
		$xml = "<data>";
		$article = new Article('contact');
		if( $article->status == 1 ) $xml .= $article->Xml(true);
		$xml .= "</data>";
		$this->display( 'show_contacts', $xml );
	}
	
	function make_leftmenu( $category_id=0, $item_id=0 ) {
		if( $category_id > 0 ) {
			$o = new Category( $category_id );
			$this->leftmenu->add_item( new WEMenu( "submenu". $o->id, $o->name ) );
			$this->leftmenu->select_child( "submenu". $o->id );
			$menu = $this->leftmenu->get_item( "submenu". $o->id );
			
			if( $menu )
			{
				$t2 = new MysqlTable('articles');
				$t2->find_all( "folder='{$o->folder}' and category_id={$o->id} and status=1 and show_in_menu=1", "position,id" );
				foreach( $t2->data as $row2 )
				{
					$a = new Article( $row2['id'] );
					//$menu->add_item( new WEMenuItem( "item". $a->id, $a->header, "/services/article/{$o->id}/{$a->id}" ) );
					$translite = url_cyrillic_translite( $a->header );
					$category_translite = $this->get_article_category_translite( $o->id );
					$menu->add_item( new WEMenuItem( "item". $a->id, $a->header, "/{$category_translite}/{$translite}/sa{$a->id}" ) );
					if( $item_id == $a->id ) $menu->select_child( "item". $a->id );
				}
				
				if( $category_id == 18 ) { //photobook
					// $menu->add_item( new WEMenuItem( "itemmyphotobook", $this->dictionary->get('/locale/common/topmenu/photobooks/myphotobook'), "/static/myphotobook" ) );
					
					$link_to_calc = "/fotoknigi/professionalam/sa289";
					$link_to_calc2 = "/fotoknigi/gotoviy_maket_zakazat_dizayn/sa382";
					$submenu_second = new WEMenu( "submenu_second", $this->dictionary->get('/locale/common/topmenu/photos/sop') );
					$submenu_second->add_item( new WEMenuItem( "itemOrder", $this->dictionary->get('/locale/common/topmenu/photobooks/order'), $link_to_calc ) );
					$submenu_second->add_item( new WEMenuItem( "itemOrder3", $this->dictionary->get('/locale/common/topmenu/photobooks/order2'), $link_to_calc2 ) );
					// $submenu_second->add_item( new WEMenuItem( "itemOrder2", $this->dictionary->get('/locale/common/topmenu/photobooks/order_myphotobook'), "/static/myphotobook" ) );
					
					$this->leftmenu->add_item( $submenu_second );
				}
				else if( $category_id == 24 ) { //poligraphy
					$submenu_second = new WEMenu( "submenu_second", $this->dictionary->get('/locale/common/topmenu/photos/sop') );
					$submenu_second->add_item( new WEMenuItem( "itemcardeditor", $this->dictionary->get('/locale/common/topmenu/poligraphy/cardeditor'), "http://www.business-card-editor.com/typo_name/kollage" ) );
					
					$this->leftmenu->add_item( $submenu_second );
				}
			}
		}
	}

	/*
		/services/article/8/63 -> category_id=8, article_id=63. Must ignnore category_id
		/photos/article/207 -> category_id=12
	 */
	function anv_fix_urls_in_articles() {
		echo '<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head><body>';
		$t = new MysqlTable('articles');
		$t->find_all("category_id in (18,6,23,24,12)");
		foreach( $t->data as $row )
		{
			$article = new Article( $row['id'] );
			//$article = new Article( $t->data[2]['id'] );
			//$article = new Article( $t->data[7]['id'] );
			$text = $article->text;
			//echo $text . "<br>===========================<br>";
			if( preg_match_all( "#[\"']{1}(http://kollage.com.ua)*\s*/?services/article/([^/\"']+)/([^/\"']+)/?\s*[\"']{1}#i", $text, $matches ) ) {
				$found = $matches[0];
				$category_ids = $matches[2];
				$article_ids = $matches[3];
				for( $i=0; $i<count($matches[0]); $i++ ) {
					//echo "{$found[$i]} => {$category_ids[$i]} || {$article_ids[$i]}<br>\n";
					$a = new Article( $article_ids[$i] );
					$url = "/". $this->get_article_category_translite($a->category_id) ."/". url_cyrillic_translite( $a->header ) ."/sa{$article_ids[$i]}";
					$text = str_replace( $found[$i], $url, $text );
				}
			}
			else if( preg_match_all( "#[\"']{1}(http://kollage.com.ua)*\s*/?photos/article/([^/\"']+)/?\s*[\"']{1}#i", $text, $matches ) ) {
				$found = $matches[0];
				$article_ids = $matches[2];
				for( $i=0; $i<count($matches[0]); $i++ ) {
					//echo "{$found[$i]} => {$article_ids[$i]}<br>\n";
					$a = new Article( $article_ids[$i] );
					$url = "/". $this->get_article_category_translite($a->category_id) ."/". url_cyrillic_translite( $a->header ) ."/pa{$article_ids[$i]}";
					$text = str_replace( $found[$i], $url, $text );
				}
			}
			//echo $text . "<br>===========================<br>";
			$article->text = $text;
			$article->Save();
		}
	}

	function get_article_category_translite( $category_id ) {
		$retval = "";

		if( $category_id == 18 ) $retval = "fotoknigi";
		else if( $category_id == 6 ) $retval = "video";
		else if( $category_id == 23 ) $retval = "sufeniri";
		else if( $category_id == 24 ) $retval = "poligrafia";
		else if( $category_id == 12 ) $retval = "fotografii";

		return( $retval );
	}

	function find_first_category()
	{
		$retval = 0;
		
		$t = new MysqlTable( 'categories' );
		if( $row = $t->find_first("folder='services'","position,id") )
		{
			$retval = intval( $row['id'] );
		}
		
		return( $retval );
	}
	
	function find_first_article( $category_id=0 )
	{
		$retval = 0;
		
		$t = new MysqlTable( 'articles' );
		if( $row = $t->find_first("folder='services' and category_id={$category_id}","position,id") )
		{
			$retval = intval( $row['id'] );
		}
		
		return( $retval );
	}
	
	/* common */
	function restore()
	{
		parent::restore();
		if( isset($_SESSION['pb_order_id']) ) $this->pb_order = new PBOrder( intval( $_SESSION['pb_order_id'] ), true );
	}
	
	function store()
	{
		$_SESSION['pb_order_id'] = ( is_object($this->pb_order) )? $this->pb_order->id : 0;
		parent::store();
	}
	/* common */
	
	/* printbook */
	function printbook_order() {
		$this->method_name = 'printbook_order';
		$this->make_leftmenu( 0, 0 );
		
		$this->_prepare_pb_order();
		$order_update = ( isset($_REQUEST['pb_format']) && $_REQUEST['pb_format'] > 0 )? 1 : 0;
		
		$xml = "<data>";
		$xml .= "<pb_calc><![CDATA[". $this->get_pb_calc() ."]]></pb_calc>";
		$xml .= "<order_update_flag>". $order_update ."</order_update_flag>";
		$xml .= "</data>";
		
		$this->display( 'printbook_order', $xml );
	}

	//confirm order
	function printbook_order2() {
		if( $this->client->id > 0 ) {
			$this->method_name = 'printbook_order2';
			$this->make_leftmenu( 0, 0 );
			
			$this->_prepare_pb_order();
			
			//checkup set parameters of the order
			$error_messages = $this->printbook_check_order();
			//redirect( SITEPREFIX .'?services.printbook_order' );
			
			$xml = "<data>";
			$xml .= "<error_messages><![CDATA[{$error_messages}]]></error_messages>";
			$xml .= $this->pb_order->Xml();
			$xml .= $this->printbook_order_additional_data();
			$xml .= "</data>";
			
			$this->display( 'printbook_order2', $xml );
		}
		else {
			redirect( SITEPREFIX .'?services.printbook_order' );
		}
	}
	
	function printbook_order3() {
		global $log;
		if( $this->client->id > 0 ) {
			$this->method_name = 'printbook_order3';
			$this->make_leftmenu( 0, 0 );
			
			$delivery_id = intval( $_REQUEST['object']['delivery_id'] );
			$paymentway_id = intval( $_REQUEST['object']['paymentway_id'] );
			$comments = $_REQUEST['object']['comments'];
			
			$this->_prepare_pb_order();
			
			//checkup set parameters of the order
			$error_messages = "";
			$error_messages .= $this->printbook_check_order();
			$log->Write( "printbook_order3 error_messages={$error_messages}" );
			
			if( $error_messages == "" && $this->pb_order->id > 0 ) {
				$delivery = new PBDelivery( $delivery_id );
				$log->Write( "printbook_order3 delivery_id={$delivery_id} delivery->price={$delivery->price}" );
				
				$this->pb_order->delivery_id = $delivery_id;
				$this->pb_order->delivery_price = $delivery->price;
				$this->pb_order->paymentway_id = $paymentway_id;
				$this->pb_order->comments = $comments;
				
				$this->pb_order->Save();
				//$this->_prepare_pb_order();
				
				$error_messages .= $this->printbook_check_order2();
				$log->Write( "printbook_order3 paymentway->code={$this->pb_order->paymentway->code}" );
				
				if( $error_messages == "" ) {
					switch( $this->pb_order->paymentway->code )
					{
						case 4:
							//Эл.кошелек
							if( $this->client->purse >= $this->pb_order->total_price )
							{
								$this->printbook_pay_with_purse();
							}
							else
							{
								$error_messages .= $this->dictionary->get('/locale/photos/order/purse_limit');
							}
							break;
						case 5:
							//Пласт.карта
							$this->printbook_pay_with_card();
							break;
						case 8:
							//Пласт.карта Privat (liqpay.com)
							$this->printbook_pay_with_card_privat();
							break;
						default:
							$this->printbook_save_order();
							break;
					}
					exit(0);
				}
			}
			$this->_prepare_pb_order();
			
			$xml = "<data>";
			$xml .= "<error_messages><![CDATA[{$error_messages}]]></error_messages>";
			$xml .= $this->pb_order->Xml();
			$xml .= $this->printbook_order_additional_data();
			$xml .= "</data>";
			
			$this->display( 'printbook_order3', $xml );
		}
		else {
			redirect( SITEPREFIX .'?services.printbook_order' );
		}
	}
	
	private function printbook_save_order( $status=1 )
	{
		global $log;
		$log->Write("printbook_save_order");

		$this->method_name = 'printbook_save_order';
		$this->make_leftmenu( 0, 0 );
		
		$this->_prepare_pb_order();
		$error_messages = "";
		$error_messages .= $this->printbook_check_order();
		$error_messages .= $this->printbook_check_order2();
		
		if( !$error_messages )
		{
			$log->Write("printbook_save_order CHECKING OK");
				$this->_prepare_pb_order();
				
				$xml = "<data>";
				$xml .= $this->pb_order->Xml();
				$xml .= $this->printbook_order_additional_data();
				$xml .= "</data>";
				$this->display( 'printbook_order_result', $xml );
				
				if( $this->pb_order->status == 0 )
				{
					$this->pb_order->status = 1;
					$this->pb_order->Save();
					
					//Copy order's files into tree structure of the folders
					$src_dir = $this->pb_order->dir . $this->pb_order->order_number . "/";
					$dst_dir = $src_dir ."print";
					mkdir_r( $dst_dir, 0775 ); $dst_dir .= "/";
					
					if( $this->pb_order->comments ) {
							$f = $dst_dir."order.txt";
							file_put_contents( $f, convert_encoding('utf-8', 'windows-1251', $this->pb_order->comments) );
							chmod( $f, 0664 );
					}
					
					for( $i=1; $i<=$this->pb_order->pages; $i++ ) {
						$t = new MysqlTable( 'pb_order_page_comments' );
						if( $row = $t->find_first("order_id={$this->pb_order->id} and page={$i} and (comments is not null or comments <> '')") )
						{
							$f = $dst_dir. sprintf("%02d", $i) .".txt";
							file_put_contents( $f, convert_encoding('utf-8', 'windows-1251', $row['comments']) );
							chmod( $f, 0664 );
						}
					}
					
					$i = 1;
					$files_success = 0;
					$pages = array();
					$quantity = $this->pb_order->quantity;
					foreach( $this->pb_order->images as $p )
					{
						if( $p->type == PBOrder::PB_IMAGE_TYPE_COVER ) {
							$new_dir = $dst_dir;
							if( $p->personal_page > 0 && $this->pb_order->quantity > 1 ) {
								$new_name = sprintf( "cover_%02d.jpg", $p->personal_page );
								$new_name_txt = sprintf( "cover_%02d.txt", $p->personal_page );
							}
							else {
								$new_name = "cover.jpg";
								$new_name_txt = "cover.txt";
							}
						}
						else if( $p->type == PBOrder::PB_IMAGE_TYPE_SUPERCOVER ) {
							$new_dir = $dst_dir;
							if( $p->personal_page > 0 && $this->pb_order->quantity > 1 ) {
								$new_name = sprintf( "supercover_%02d.jpg", $p->personal_page );
								$new_name_txt = sprintf( "supercover_%02d.txt", $p->personal_page );
							}
							else {
								$new_name = "supercover.jpg";
								$new_name_txt = "supercover.txt";
							}
						}
						else if( $p->type == PBOrder::PB_IMAGE_TYPE_FLYLEAF1 ) {
							$new_dir = $dst_dir;
							if( $p->personal_page > 0 ) {
								$new_name = sprintf( "f1_%02d.jpg", $p->personal_page );
								$new_name_txt = sprintf( "f1_%02d.txt", $p->personal_page );
							}
							else {
								$new_name = "f1.jpg";
								$new_name_txt = "f1.txt";
							}
						}
						else if( $p->type == PBOrder::PB_IMAGE_TYPE_FLYLEAF2 ) {
							$new_dir = $dst_dir;
							if( $p->personal_page > 0 && $this->pb_order->quantity > 1 ) {
								$new_name = sprintf( "f2_%02d.jpg", $p->personal_page );
								$new_name_txt = sprintf( "f2_%02d.txt", $p->personal_page );
							}
							else {
								$new_name = "f2.jpg";
								$new_name_txt = "f2.txt";
							}
						}
						else if( $p->type == PBOrder::PB_IMAGE_TYPE_PAGE ) {
							$new_dir = $dst_dir;
							if( $p->personal_page > 0 && $this->pb_order->quantity > 1 ) {
								$k = "{$p->page}-{$p->personal_page}";
								$pages[$k] = ( $pages[$k] > 0 )? $pages[$k]+1 : 1; 
							}
							else {
								$pages[$p->page] = ( $pages[$p->page] > 0 )? $pages[$p->page]+1 : 1; 
							}
							
							if( $this->pb_order->design_work_id > 0 ) {
								$new_dir = $dst_dir . sprintf( "%02d", $p->page );
								if( !file_exists($new_dir) ) {
									mkdir_r( $new_dir, 0775 );
								}
								$new_dir .= '/';
							}
							
							if( $p->personal_page > 0 && $this->pb_order->quantity > 1 ) {
								$k = "{$p->page}-{$p->personal_page}";
								$new_name = sprintf( "%02d_%02d-%02d.jpg", $p->page, $p->personal_page, $pages[$k] );
								$new_name_txt = sprintf( "%02d_%02d-%02d.txt", $p->page, $p->personal_page, $pages[$k] );
							}
							else {
								$new_name = sprintf( "%02d-%02d.jpg", $p->page, $pages[$p->page] );
								$new_name_txt = sprintf( "%02d-%02d.txt", $p->page, $pages[$p->page] );
							}
						}
						
						if( file_exists($new_dir.$new_name) ) {
							$parts = explode( ".", $new_name );
							$new_name = $parts[0]+"2."+$parts[1];
						}
						
						if( file_exists($new_dir.$new_name_txt) ) {
							$parts = explode( ".", $new_name_txt );
							$new_name_txt = $parts[0]+"2."+$parts[1];
						}
						
						if( copy( $src_dir.$p->filename, $new_dir.$new_name ) )
						{
							chmod( $new_dir.$new_name, 0664 );
							if( $p->comments ) {
								file_put_contents( $new_dir.$new_name_txt, convert_encoding('utf-8', 'windows-1251', $p->comments) );
								chmod( $new_dir.$new_name_txt, 0664 );
							}
							$files_success++;
						}
						$i++;
					}
					if( $files_success > 0 && $files_success == count($this->pb_order->images) )
					{
					}
				}
					
					//Send email to client
					$settings = new Settings();
					$pb_settings = new PBSettings();
					
					if( $this->client->email ) {
						$mail = new AMail( 'windows-1251' );
							$subject = $pb_settings->order_mail_subject;
							$subject = str_replace( "{ORDER_NR}", $this->pb_order->order_number, $subject );
							$mail->subject = $subject;
							
							$mail->from_name = $settings->from_name;
							$mail->from_email = $settings->from_email;
							$mail->to_email = $this->client->email;
							$body = $pb_settings->order_mail_body;
							$body = str_replace( "{CLIENT}", $this->client->firstname.' '.$this->client->lastname, $body );
							$body = str_replace( "{ORDER_NR}", $this->pb_order->order_number, $body );
							$body = str_replace( "{ORDER_STATUS}", $this->GetOrderStatusStr( $this->pb_order ), $body );
							$body = str_replace( "{ORDER}", $this->PrintbookGetOrderSummaryTableExtended( $this->pb_order ), $body );
						$mail->Send( $body );
					}
					
					//Send email to admin
					$mail = new AMail( 'windows-1251' );
						$subject = $pb_settings->admin_order_mail_subject;
						$subject = str_replace( "{ORDER_NR}", $this->pb_order->order_number, $subject );
						$mail->subject = $subject;
						
						$mail->from_name = $settings->from_name;
						$mail->from_email = $settings->from_email;
						$mail->to_email = $pb_settings->admin_emails;
						$body = $pb_settings->admin_order_mail_body;
						$body = str_replace( "{CLIENT}", $this->client->firstname.' '.$this->client->lastname, $body );
						$body = str_replace( "{ORDER_NR}", $this->pb_order->order_number, $body );
						$body = str_replace( "{ORDER_STATUS}", $this->GetOrderStatusStr( $this->pb_order ), $body );
						$body = str_replace( "{ORDER}", $this->PrintbookGetOrderSummaryTableExtended( $this->pb_order ), $body );
					$mail->Send( $body );
				 
				$this->pb_order = null;
				$this->store();
		}
		else
		{
			$log->Write("printbook_save_order ERROR: ". $error_messages);
			$this->printbook_order();
		}
	}
	
	function GetOrderStatusStr( &$order )
	{
		$retval = '';
		
		if( $order->IsLoaded() )
		{
			switch( $order->status )
			{
				case 1:
					$retval = $this->dictionary->get('/locale/my/orders/photo/statuses/s1');
					break;
				case 2:
					$retval = $this->dictionary->get('/locale/my/orders/photo/statuses/s2');
					break;
				case 3:
					$retval = $this->dictionary->get('/locale/my/orders/photo/statuses/s3');
					break;
				case 4:
					$retval = $this->dictionary->get('/locale/my/orders/photo/statuses/s4');
					break;
				case 5:
					$retval = $this->dictionary->get('/locale/my/orders/photo/statuses/s5');
					break;
				case 6:
					$retval = $this->dictionary->get('/locale/my/orders/photo/statuses/s6');
					break;
			}
		}
		
		return( $retval );
	}
	
	function PrintbookGetOrderSummaryTableExtended( &$order )
	{
		$retval = '';
		
		if( $order->IsLoaded() )
		{
			$retval .= '<table style="border: 1px solid #e3bf85" cellspacing="3">';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/order_number2') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->order_number .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/created_at') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->created_at .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/updated_at') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->updated_at .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/paymentway_id') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->paymentway->name .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/delivery_id') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->delivery->name .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/comments') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;color:#d80c76">'. $order->comments .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">&#160;</td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">&#160;</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photobook/calc/format') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->size->name .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photobook/calc/pages') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->pages .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photobook/calc/paper') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->paper->name .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photobook/calc/lamination') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->lamination->name .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photobook/calc/forzaces') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->getForzacesTitle() .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photobook/calc/personal_pages') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->getPersonalPagesTitle() .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photobook/calc/personal_cover') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->getPersonalCoverTitle() .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photobook/calc/cover') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->cover->name .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photobook/calc/box') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->box->name .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photobook/calc/design_work') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->design_work->name .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">&#160;</td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">&#160;</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/client') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->client->firstname .'&#160;'. $order->client->lastname .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/email') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->client->email .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/address_v') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->client->city .', '. $order->client->street->name .', '. $order->client->address .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/address2_v') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;color:#d80c76">'. $order->address .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/phone') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->client->phone .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">&#160;</td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">&#160;</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photobook/calc/quantity') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->quantity .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photobook/calc/price_one') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->calculated_price_one .' '. $this->dictionary->get('/locale/common/currency') .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photobook/calc/price_design') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->calculated_price_design_work .' '. $this->dictionary->get('/locale/common/currency') .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/total_delivery') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. sprintf( "%01.2f", $order->delivery_price ) .' '. $this->dictionary->get('/locale/common/currency') .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/total_discount') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. sprintf( "%01.2f", $order->discount_price ) .' '. $this->dictionary->get('/locale/common/currency') .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/total_price') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. sprintf( "%01.2f", $order->total_price ) .' '. $this->dictionary->get('/locale/common/currency') .'</td>';
			$retval .= '</tr>';
			
			$retval .= '<tr>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $this->dictionary->get('/locale/photos/order/payed_price') .'</b></td>';
			$retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. sprintf( "%01.2f", $order->payed ) .' '. $this->dictionary->get('/locale/common/currency') .'</td>';
			$retval .= '</tr>';
			
			$retval .= '</table>';
		}
		
		return( $retval );
	}
	
	function printbook_pay_with_card()
	{
		$host = $_SERVER['HTTP_HOST']; if( !$host ) $host = 'kollage.com.ua';
		$this->printbook_upc_pay( $this->pb_order->order_number, $this->pb_order->total_price, "printbook|".$host );
	}
	
	function printbook_pay_with_card_privat()
	{
		$host = $_SERVER['HTTP_HOST']; if( !$host ) $host = 'kollage.com.ua';
		$this->printbook_privat_pay( $this->pb_order->order_number, $this->pb_order->total_price, "printbook|".$host );
	}
	
	function printbook_pay_with_purse()
	{
		if( $this->client->purse >= $this->pb_order->total_price )
		{
			$o = new PurseOperation();
			$o->client_id = $this->client->id;
			$o->amount = -1 * $this->pb_order->total_price;
			$o->description = sprintf( "%s%s", $this->dictionary->get('/locale/photos/order/purse_description'), $this->pb_order->order_number );
			$o->Save();
			
			$this->pb_order->payed = $this->pb_order->total_price;
			$this->pb_order->Save();
			$this->printbook_save_order();
		}
	}
	
	function printbook_upc_pay( $order_number, $total_amount, $action='printbook|kollage.com.ua' )
	{
		global $db;
		
		//make signature
		//$MerchantID = '1752256';  //test-server
		$MerchantID = '6984785';
		//$TerminalID = 'E7880056';  //test-server
		$TerminalID = 'E0133767';
		$PurchaseTime = date("ymdHis");
		$OrderID = $order_number;
		$Currency = '980';
		$TotalAmount = round( $total_amount * 100 );
		$SD = $action;
		$data = "$MerchantID;$TerminalID;$PurchaseTime;$OrderID;$Currency;$TotalAmount;$SD;";
		//$fp = fopen( SITEROOT ."/upc/1752256.pem", "r" );  //test-server
		$fp = fopen( SITEROOT ."/upc/6984785.pem", "r" );
		if( $fp )
		{
			$priv_key = fread( $fp, 8192 );
			fclose( $fp );
			$pkeyid = openssl_pkey_get_private( $priv_key );
			openssl_sign( $data , $signature, $pkeyid );
			openssl_free_key( $pkeyid );
			$signature = base64_encode( $signature );
		}
		
		$xml = "<data>";
		$xml .= "<upc_MerchantID>{$MerchantID}</upc_MerchantID>";
		$xml .= "<upc_TerminalID>{$TerminalID}</upc_TerminalID>";
		$xml .= "<upc_TotalAmount>{$TotalAmount}</upc_TotalAmount>";
		$xml .= "<upc_Currency>{$Currency}</upc_Currency>";
		
		if( $AltTotalAmount )
		{
			$xml .= "<upc_AltTotalAmount>{$AltTotalAmount}</upc_AltTotalAmount>";
			$xml .= "<upc_AltCurrency>{$this->currency->code}</upc_AltCurrency>";
		}
		
		$xml .= "<upc_PurchaseTime>{$PurchaseTime}</upc_PurchaseTime>";
		$xml .= "<upc_OrderID>{$OrderID}</upc_OrderID>";
		$xml .= "<upc_SD>{$SD}</upc_SD>";
		$xml .= "<upc_Signature><![CDATA[{$signature}]]></upc_Signature>";
		$xml .= "</data>";
		
		$this->display( 'printbook_upc_pay', $xml );
	}
	
	function printbook_upc_success()
	{
		$this->make_leftmenu( 0, 0 );
		
		$this->printbook_save_order();
		$OrderID = $_REQUEST['OrderID'];
		
		$xml = "<data>";
		$xml .= "<OrderID>{$OrderID}</OrderID>";
		$xml .= "<xid><![CDATA[{$xid}]]></xid>";
		$xml .= "<signature_failed>0</signature_failed>";
		$xml .= "<message>". $this->GetUPCResultMessage(0) ."</message>";
		$xml .= "</data>";
		
		$this->display( 'printbook_upc_success', $xml );
	}
	
	function printbook_upc_success_already()
	{
		$this->make_leftmenu( 0, 0 );
				
		$OrderID = $_REQUEST['OrderID'];
		
		$xml = "<data>";
		$xml .= "<OrderID>{$OrderID}</OrderID>";
		$xml .= "<xid><![CDATA[{$xid}]]></xid>";
		$xml .= "<signature_failed>0</signature_failed>";
		$xml .= "<message>". $this->GetUPCResultMessage(0) ."</message>";
		$xml .= "</data>";
		
		$this->display( 'printbook_upc_success', $xml );
	}
	
	function printbook_upc_failure()
	{
		$this->make_leftmenu( 0, 0 );
		
		$o = new PBOrder( $_REQUEST['order_id'] );
		
		$xml = "<data>";
		$xml .= "<OrderID>{$o->order_number}</OrderID>";
		$xml .= "<message>". $this->GetUPCResultMessage( $_REQUEST['tranCode'] ) ."</message>";
		$xml .= "</data>";
		
		$this->display( 'printbook_upc_failure', $xml );
	}
	
	function printbook_privat_pay( $order_number, $total_amount, $action='sop|kollage.com.ua' )
	{
		global $db, $log;
		
		$OrderID = $order_number .'-'. time();
		$TotalAmount = round( $total_amount, 2 );
		$result_url = "https://kollage.com.ua/services/printbook_privat_return?PHPSESSID=". session_id();
		$server_url = "https://kollage.com.ua/services/printbook_privat_return_server?PHPSESSID=". session_id();

		$log->Write( "printbook_privat_pay order_number={$order_number} total_amount={$total_amount} action={$action}" );
		
		$liqpay = new LiqPay(self::liqpay_merchant_id, self::liqpay_signature);
		$html = $liqpay->cnb_form(array(
			'action'         => 'pay',
			'amount'         => $TotalAmount,
			'currency'       => 'UAH',
			'description'    => "PHOTO ORDER: $OrderID",
			'order_id'       => $OrderID,
			'result_url'     => $result_url,
			'server_url'     => $server_url,
			'version'        => '3'
		));
		
		$data = "<data><form><![CDATA[$html]]></form></data>";

		$log->Write( "printbook_privat_pay data={$data}" );
		
		$this->display( 'printbook_privat_pay', $data );
	}
	
	function printbook_privat_return_server()
	{
		global $log;
		$private_key = self::liqpay_signature;
		$liqpay = new LiqPay(self::liqpay_merchant_id, self::liqpay_signature);

		$signature_received = $_REQUEST['signature'];
		$base64 = $_REQUEST['data'];
		$data = base64_decode( $base64 );
		$parameters = json_decode( $data );
  
		$signature_encoded = base64_encode( sha1( $private_key . $base64 . $private_key, 1 ) );
		
		$log->Write("PB->privat_return_server REQUEST=". json_encode($_REQUEST));
		
		if( $signature_received==$signature_encoded ) {
			$OrderID = $parameters->order_id;
			list($order_id_for_load) = split('-',$OrderID);
			$order = new PBOrder();
			$order->LoadByNumber( $order_id_for_load, true );
			$amount = $order->total_price;
			$log->Write( "PB->privat_return_server SIGNATURE VERIFICATION OK ". json_encode([
				'order_id' => $order_id_for_load,
				'amount' => $amount,
			]) );
			if( $order->status == 0 ) {
				$log->Write("PB->privat_return_server SAVING ORDER");
				$order->payed = $amount;
				$order->Save();
				$this->pb_order = $order;
				$this->printbook_save_order();
			}
		}
		else {
			$log->Write("PB->privat_return_server signatures are not equal");
		}
		
	}
	
	function printbook_privat_return()
	{
		redirect( '/' );	}
	
	function printbook_load_paymentways()
	{
		global $db;

		$this->method_name = 'printbook_load_paymentways';
		
		header("Expires: 0");
		header("Cache-Control: private");
		header("Pragma: no-cache");
		
		$JsHttpRequest = new JsHttpRequest("utf-8");
		
		$id = intval( $_REQUEST['delivery_id'] );
		echo $id;
		$o = new PBDelivery( $id );
		if( $o->IsLoaded() )
		{
			$GLOBALS['_RESULT']['delivery'] = $o;
			$GLOBALS['_RESULT']['paymentways'] = Array();
			
			$sql = "select dp.paymentway_id from pb_deliveries_paymentways dp left join pb_paymentways pw on pw.id=dp.paymentway_id where dp.delivery_id={$id} order by pw.position,pw.id";
			echo $sql;
			$rs = $db->query( $sql );
			$i = 0;
			while( $row = $rs->fetch_row() )
			{
				$p = new PBPaymentway( $row[0] );
				$GLOBALS['_RESULT']['paymentways'][$i++] = Array( $p->id, $p->name );
			}
			
			$GLOBALS['_RESULT']['err'] = 0;
		}
		else
		{
			$GLOBALS['_RESULT']['err'] = 1;
		}
	}
	
	function printbook_order_additional_data()
	{
		$xml = "";
		
		$t = new MysqlTable('pb_deliveries');
		$t->find_all( "status=1", "position,id" );
		$xml .= "<deliveries>";
		foreach( $t->data as $row )
		{
			$o = new PBDelivery( $row['id'] );
			$xml .= $o->Xml();
		}
		$xml .= "</deliveries>";
		
		$t = new MysqlTable('pb_paymentways');
		$t->find_all( "status=1", "position,id" );
		$xml .= "<paymentways>";
		foreach( $t->data as $row )
		{
			$o = new PBPaymentway( $row['id'] );
			$xml .= $o->Xml();
		}
		$xml .= "</paymentways>";
		
		return( $xml );
	}
	
	function printbook_check_order() {
		$retval = '';
		global $log;
		
		//$log->Write("this->pb_order->id={$this->pb_order->id} this->pb_order->images_count={$this->pb_order->images_count} this->pb_order->pages={$this->pb_order->pages}");
		
		//check printbook format
		if( !$this->pb_order->size->id ) {
			$retval .= "<li>". $this->dictionary->get('/locale/photobook/order/error_select_size') ."</li>";
		}
		if( $this->pb_order->images_count == 0 ) {
			$retval .= "<li>". $this->dictionary->get('/locale/photobook/order/error_images_arent_uploaded') ."</li>";
		}
		if( $this->pb_order->size->id > 0 && $this->pb_order->images_count < $this->pb_order->pages ) {
			$retval .= "<li>". $this->dictionary->get('/locale/photobook/order/error_images_are_less_then_pages') ."</li>";
		}
		//check items size
		if( !$this->pb_order->design_work || $this->pb_order->design_work->id == 0 ) {
			foreach( $this->pb_order->images as $i ) {
				if( $i->type == 2 && ($i->filename_w < $this->pb_order->size->cover_min_width || $i->filename_h < $this->pb_order->size->cover_min_height) ) {
					$retval .= "<li>Изображение \"". $i->original_filename ."\" не соответствует параметрам книги</li>";
				}
				else if( $i->type == 1 && ($i->filename_w < $this->pb_order->size->page_min_width || $i->filename_h < $this->pb_order->size->page_min_height) ) {
					$retval .= "<li>Изображение \"". $i->original_filename ."\" не соответствует параметрам книги</li>";
				}
			}
		}
		
		//check items page
		$there_is_cover = false;
		$there_is_supercover = false;
		$there_is_f1 = false;
		$there_is_f2 = false;
		$there_is_forzaces = $this->pb_order->HasForzaces(); 
		$has_supercover = $this->pb_order->HasSupercover();
		$check_cover_size = $this->pb_order->cover->check_size;
		
		foreach( $this->pb_order->images as $i ) {
			if( $i->type == 1 && $i->page == 0 ) {
				$retval .= "<li>Не определена страница для изображения \"". $i->original_filename ."\"</li>";
			}
			if( $i->type == 2 ) $there_is_cover = true;
			if( $i->type == 3 ) $there_is_f1 = true;
			if( $i->type == 4 ) $there_is_f2 = true;
			if( $i->type == 5 ) $there_is_supercover = true;
		}
		
		if( $check_cover_size == 1 && !$there_is_cover ) $retval .= "<li>Не загружено изображение для обложки</li>";
		if( !$there_is_f1 && $there_is_forzaces ) $retval .= "<li>Не загружено изображение для переднего форзаца</li>";
		if( !$there_is_f2 && $there_is_forzaces ) $retval .= "<li>Не загружено изображение для заднего флрзаца</li>";
		if( !$there_is_supercover && $has_supercover ) $retval .= "<li>Не загружено изображение для суперобложки</li>";
		
		for( $p=1; $p<=$this->pb_order->pages; $p++ ) {
			$pe = false;
			foreach( $this->pb_order->images as $i ) {
				if( $p == $i->page ) {
					$pe = true;
					break;
				}
			}
			if( !$pe ) {
				$retval .= "<li>Не загружено изображение для страницы \"". $p ."\"</li>";
			}
		}
		
		//TODO: personal cover and personal pages
		
		return( $retval );
	}
	
	function printbook_check_order2() {
		$retval = '';
		global $log;
		
		if( !$this->pb_order->delivery_id ) {
			$retval .= "<li>". $this->dictionary->get('/locale/photobook/order/error_delivery_id') ."</li>";
		}
		
		if( !$this->pb_order->paymentway_id ) {
			$retval .= "<li>". $this->dictionary->get('/locale/photobook/order/error_paymentway_id') ."</li>";
		}
		
		return( $retval );
	}
	
	function _prepare_pb_order()
	{
		global $db, $log;
		
		$GLOBALS['_RESULT'] = Array();

		if( !isset($this->pb_order) ) {
			$this->pb_order = new PBOrder();
		}
		
		if( isset($this->pb_order) && !$this->pb_order->IsLoaded() && $this->client->id > 0 )
		{
			$this->pb_order->client_id = $this->client->id;
			$this->pb_order->Save();
			$this->store();
			
			$log->Write( "_prepare_pb_order saving order" );
		}
		
		if( isset($this->pb_order) && $this->pb_order->IsLoaded() )
		{
			//$log->Write( "_prepare_pb_order order is loaded" );
			$this->pb_order->LoadImages();
			$this->pb_order->CalculatePrices();
			
			//Order parameters
			$cloned_pb_order = clone $this->pb_order;
			$this->_clear_user_security_from_order( $cloned_pb_order );
			$GLOBALS['_RESULT']['pb_order'] = $cloned_pb_order;
		}
		else {
			$log->Write( "_prepare_pb_order order is NOT loaded" );
		}
	}
	
	private function _clear_user_security_from_order( &$obj ) {
		$obj->client = null;
		$obj->log = null;
		$obj->dir = null;
	}

    private function get_pb_calculator(){
        return "<iframe width='1100px' height='1000px' src='http://dev.kollage.com.ua/pb_calc/calculator.html'></iframe>";
    }

    private function get_pb_calc()
	{
		global $db, $log;
		$retval = '';
		
		// use to restore the calc parameters
		$pb_format = intval( $_REQUEST['pb_format'] );
		$pb_pages = intval( $_REQUEST['pb_pages'] );
		$pb_paper = intval( $_REQUEST['pb_paper'] );
		$pb_lamination = intval( $_REQUEST['pb_lamination'] );
		$pb_paper_base = intval( $_REQUEST['pb_paper_base'] );
		$pb_personal_pages = intval( $_REQUEST['pb_personal_pages'] );
		$pb_selected_personal_pages = $_REQUEST['pb_selected_personal_pages'];
		$pb_personal_cover = intval( $_REQUEST['pb_personal_cover'] );
		$pb_cover_category = intval( $_REQUEST['pb_cover_category'] );
		$pb_cover = intval( $_REQUEST['pb_cover'] );
		$pb_supercover = intval( $_REQUEST['pb_supercover'] );
		$pb_design_work_category = intval( $_REQUEST['pb_design_work_category'] );
		$pb_design_work = intval( $_REQUEST['pb_design_work'] );
		$pb_box_category = intval( $_REQUEST['pb_box_category'] );
		$pb_box = intval( $_REQUEST['pb_box'] );
		$pb_quantity = intval( $_REQUEST['pb_quantity'] );
		$pb_forzaces = intval( $_REQUEST['pb_forzaces'] );
		$pb_paper_base = intval( $_REQUEST['pb_paper_base'] );
		$pb_photobook_forzaces = intval( $_REQUEST['pb_photobook_forzaces'] );
		
		$pb_items = Array();
		
		$item = new StdClass();
		$item->covers = Array();
		$item->papers = Array();
		$item->lamination = Array();
		$item->paper_base = Array();
		$item->design_works = Array();
		$item->boxes = Array();
		$item->forzaces = Array();
		$item->id = 0;
		$item->name = $this->dictionary->get('/locale/photobook/select');
		$item->price_base = 0;
		$item->price_per_page = 0;
		$item->price_personal_page = 0;
		$item->price_personal_cover = 0;
		$item->start_page = 0;
		$item->page_block = 0;
		$item->discount1 = 0;
		$item->discount2 = 0;
		$item->discount3 = 0;
		
			$p_item = new StdClass();
			$p_item->id = 0;
			$p_item->name = $this->dictionary->get('/locale/photobook/select');
			$p_item->description = "";
			$p_item->price = 0;
			$p_item->is_individual = 0;
			$p_item->items = Array();
			array_push( $item->covers, $p_item );
		
			$p_item = new StdClass();
			$p_item->id = 0;
			$p_item->name = $this->dictionary->get('/locale/photobook/select');
			$p_item->price = 0;
			array_push( $item->papers, $p_item );
		
			$p_item = new StdClass();
			$p_item->id = 0;
			$p_item->name = $this->dictionary->get('/locale/photobook/select');
			$p_item->price = 0;
			array_push( $item->lamination, $p_item );
		
			$p_item = new StdClass();
			$p_item->id = 0;
			$p_item->name = $this->dictionary->get('/locale/photobook/select');
			$p_item->price = 0;
			array_push( $item->paper_base, $p_item );
		
			$p_item = new StdClass();
			$p_item->id = 0;
			$p_item->name = $this->dictionary->get('/locale/photobook/select');
			$p_item->description = "";
			$p_item->items = Array();
			array_push( $item->design_works, $p_item );
		
			$p_item = new StdClass();
			$p_item->id = 0;
			$p_item->name = $this->dictionary->get('/locale/photobook/select');
			$p_item->description = "";
			$p_item->items = Array();
			array_push( $item->boxes, $p_item );
			
			$p_item = new StdClass();
			$p_item->id = 0;
			$p_item->name = $this->dictionary->get('/locale/photobook/select');
			$p_item->description = "";
			$p_item->price = 0;
			array_push( $item->forzaces, $p_item );
		
		array_push( $pb_items, $item );
		
		$sql = "select id from pb_sizes where status=1 order by position,id";
		$rs = $db->query( $sql );
		while( $row = $db->getrow($rs) )
		{
			$s = new PBSize( $row[0] );
			if( $s->IsLoaded() )
			{
				$item = new StdClass();
				$item->covers = Array();
				$item->papers = Array();
				$item->lamination = Array();
				$item->paper_base = Array();
				$item->design_works = Array();
				$item->boxes = Array();
				$item->forzaces = Array();
				$item->id = $s->id;
				$item->type = $s->type;
				$item->name = $s->name;
				$item->price_base = $s->price_base;
				$item->price_per_page = $s->price_per_page;
				$item->price_personal_page = $s->price_personal_page;
				$item->price_personal_cover = $s->price_personal_cover;
				$item->price_design_work = $s->price_design_work;
				$item->start_page = $s->start_page;
				$item->stop_page = $s->stop_page;
				$item->page_block = $s->page_block;
				$item->discount1 = $s->discount1;
				$item->discount2 = $s->discount2;
				$item->discount3 = $s->discount3;
				
				$item->client_discount = ( $this->client->IsLoaded() )? $this->client->printbook_discount : 0; 
				
				$item->image = ( $s->image )? SITEPREFIX . $_ENV['data_path'] .'/'. $s->image : "";
				$item->thumbnail = ( $s->image )? SITEPREFIX . $_ENV['data_path'] .'/thumbnail-'. $s->image : "";
				
				$sql = "select id from pb_categories where folder='cover' and status=1 and parent_id={$s->id} order by position";
				$rs3 = $db->query( $sql );
				while( $row3 = $db->getrow($rs3) )
				{
					$cat = new PBCategory($row3[0]);
					$pcat_item = new StdClass();
					$pcat_item->id = $cat->id;
					$pcat_item->name = $cat->name;
					$pcat_item->description = $cat->description;
					$pcat_item->image = ( $cat->image )? SITEPREFIX . $_ENV['data_path'] .'/'. $cat->image : "";
					$pcat_item->thumbnail = ( $cat->image )? SITEPREFIX . $_ENV['data_path'] .'/thumbnail-'. $cat->image : "";
					$pcat_item->items = Array();
					
					$sql = "select id from pb_covers where status=1 and category_id={$row3[0]} and pb_size_id={$s->id} order by position,id";
					$rs2 = $db->query( $sql );
					while( $row2 = $db->getrow($rs2) )
					{
						$p = new PBCover( $row2[0] );
						$p_item = new StdClass();
						$p_item->id = $p->id;
						$p_item->name = $p->name;
						$p_item->description = $p->description;
						$p_item->has_supercover = $p->has_supercover;
						$p_item->supercover_price = $p->supercover_price;
						$p_item->price = $p->price;
						$p_item->allow_min_pages = $p->allow_min_pages;
						$p_item->image = ( $p->image )? SITEPREFIX . $_ENV['data_path'] .'/'. $p->image : "";
						$p_item->thumbnail = ( $p->image )? SITEPREFIX . $_ENV['data_path'] .'/thumbnail-'. $p->image : "";
						
						$p_item->supercovers = Array();
						$sql = "select id from pb_supercovers where status=1 and pb_cover_id={$p->id} order by position,id";
						$rs4 = $db->query( $sql );
						while( $row4 = $db->getrow($rs4) )
						{
							$p2 = new PBSuperCover( $row4[0] );
							$p_item2 = new StdClass();
							$p_item2->id = $p2->id;
							$p_item2->name = $p2->name;
							$p_item2->description = $p2->description;
							$p_item2->price = $p2->price;
							$p_item2->image = ( $p2->image )? SITEPREFIX . $_ENV['data_path'] .'/'. $p2->image : "";
							$p_item2->thumbnail = ( $p2->image )? SITEPREFIX . $_ENV['data_path'] .'/thumbnail-'. $p2->image : "";
							
							array_push( $p_item->supercovers, $p_item2 );
						}
						
						array_push( $pcat_item->items, $p_item );
					}
					
					array_push( $item->covers, $pcat_item );
				}
				
				$sql = "select id from pb_papers where status=1 and pb_size_id={$s->id} order by position,id";
				$rs2 = $db->query( $sql );
				while( $row2 = $db->getrow($rs2) )
				{
					$p = new PBPaper( $row2[0] );
					$p_item = new StdClass();
					$p_item->id = $p->id;
					$p_item->name = $p->name;
					$p_item->price = $p->price;
					
					array_push( $item->papers, $p_item );
				}
				
				$sql = "select id from pb_lamination where status=1 and pb_size_id={$s->id} order by position,id";
				$rs2 = $db->query( $sql );
				while( $row2 = $db->getrow($rs2) )
				{
					$p = new PBLamination( $row2[0] );
					$p_item = new StdClass();
					$p_item->id = $p->id;
					$p_item->name = $p->name;
					$p_item->price = $p->price;
					
					array_push( $item->lamination, $p_item );
				}
				
				$sql = "select id from pb_paper_base where status=1 and pb_size_id={$s->id} order by position,id";
				$rs2 = $db->query( $sql );
				while( $row2 = $db->getrow($rs2) )
				{
					$p = new PBPaperBase( $row2[0] );
					$p_item = new StdClass();
					$p_item->id = $p->id;
					$p_item->name = $p->name;
					$p_item->price = $p->price;
					
					array_push( $item->paper_base, $p_item );
				}
				
				$sql = "select id from pb_categories where folder='design_work' and status=1 order by position";
				$rs3 = $db->query( $sql );
				while( $row3 = $db->getrow($rs3) )
				{
					$cat = new PBCategory($row3[0]);
					$pcat_item = new StdClass();
					$pcat_item->id = $cat->id;
					$pcat_item->name = $cat->name;
					$pcat_item->description = $cat->description;
					$pcat_item->image = ( $cat->image )? SITEPREFIX . $_ENV['data_path'] .'/'. $cat->image : "";
					$pcat_item->thumbnail = ( $cat->image )? SITEPREFIX . $_ENV['data_path'] .'/thumbnail-'. $cat->image : "";
					$pcat_item->items = Array();
					
					$sql = "select id from pb_design_works where status=1 and category_id={$row3[0]} and pb_size_id={$s->id} order by position,id";
					$rs2 = $db->query( $sql );
					while( $row2 = $db->getrow($rs2) )
					{
						$p = new PBDesignWork( $row2[0] );
						$p_item = new StdClass();
						$p_item->id = $p->id;
						$p_item->name = $p->name;
						$p_item->url = $p->url;
						$p_item->description = $p->description;
						$p_item->base_price = $p->base_price;
						$p_item->price = $p->price;
						$p_item->image = ( $p->image )? SITEPREFIX . $_ENV['data_path'] .'/'. $p->image : "";
						$p_item->thumbnail = ( $p->image )? SITEPREFIX . $_ENV['data_path'] .'/thumbnail-'. $p->image : "";
						
						array_push( $pcat_item->items, $p_item );
					}
					
					array_push( $item->design_works, $pcat_item );
				}
				
				$sql = "select id from pb_categories where folder='box' and status=1 order by position";
				$rs3 = $db->query( $sql );
				while( $row3 = $db->getrow($rs3) )
				{
					$cat = new PBCategory($row3[0]);
					$pcat_item = new StdClass();
					$pcat_item->id = $cat->id;
					$pcat_item->name = $cat->name;
					$pcat_item->description = $cat->description;
					$pcat_item->image = ( $cat->image )? SITEPREFIX . $_ENV['data_path'] .'/'. $cat->image : "";
					$pcat_item->thumbnail = ( $cat->image )? SITEPREFIX . $_ENV['data_path'] .'/thumbnail-'. $cat->image : "";
					$pcat_item->items = Array();
					
					$sql = "select id from pb_boxes where status=1 and category_id={$row3[0]} and pb_size_id={$s->id} order by position,id";
					$rs2 = $db->query( $sql );
					while( $row2 = $db->getrow($rs2) )
					{
						$p = new PBBox( $row2[0] );
						$p_item = new StdClass();
						$p_item->id = $p->id;
						$p_item->name = $p->name;
						$p_item->description = $p->description;
						$p_item->price = $p->price;
						$p_item->image = ( $p->image )? SITEPREFIX . $_ENV['data_path'] .'/'. $p->image : "";
						$p_item->thumbnail = ( $p->image )? SITEPREFIX . $_ENV['data_path'] .'/thumbnail-'. $p->image : "";
						
						array_push( $pcat_item->items, $p_item );
					}
					
					array_push( $item->boxes, $pcat_item );
				}
				
				$sql = "select id from pb_forzaces where status=1 and pb_size_id={$s->id} order by position,id";
				$rs2 = $db->query( $sql );
				while( $row2 = $db->getrow($rs2) )
				{
					$p = new PBForzace( $row2[0] );
					$p_item = new StdClass();
					$p_item->id = $p->id;
					$p_item->name = $p->name;
					$p_item->description = $p->description;
					$p_item->price = $p->price;
					$p_item->allow_min_pages = $p->allow_min_pages;
					$p_item->image = ( $p->image )? SITEPREFIX . $_ENV['data_path'] .'/'. $p->image : "";
					$p_item->thumbnail = ( $p->image )? SITEPREFIX . $_ENV['data_path'] .'/thumbnail-'. $p->image : "";
					
					array_push( $item->forzaces, $p_item );
				}
				
				array_push( $pb_items, $item );
			}
		}

		$xml = '<data>';
		$xml .= '<pb_items><![CDATA['. json_encode($pb_items) .']]></pb_items>';
		$xml .= "<pb_format>{$pb_format}</pb_format>";
		$xml .= "<pb_pages>{$pb_pages}</pb_pages>";
		$xml .= "<pb_paper>{$pb_paper}</pb_paper>";
		$xml .= "<pb_lamination>{$pb_lamination}</pb_lamination>";
		$xml .= "<pb_paper_base>{$pb_paper_base}</pb_paper_base>";
		$xml .= "<pb_personal_pages>{$pb_personal_pages}</pb_personal_pages>";
		$xml .= "<pb_selected_personal_pages>{$pb_selected_personal_pages}</pb_selected_personal_pages>";
		$xml .= "<pb_personal_cover>{$pb_personal_cover}</pb_personal_cover>";
		$xml .= "<pb_cover_category>{$pb_cover_category}</pb_cover_category>";
		$xml .= "<pb_cover>{$pb_cover}</pb_cover>";
		$xml .= "<pb_supercover>{$pb_supercover}</pb_supercover>";
		$xml .= "<pb_design_work_category>{$pb_design_work_category}</pb_design_work_category>";
		$xml .= "<pb_design_work>{$pb_design_work}</pb_design_work>";
		$xml .= "<pb_box_category>{$pb_box_category}</pb_box_category>";
		$xml .= "<pb_box>{$pb_box}</pb_box>";
		$xml .= "<pb_quantity>{$pb_quantity}</pb_quantity>";
		$xml .= "<pb_forzaces>{$pb_forzaces}</pb_forzaces>";
		$xml .= "<pb_paper_base>{$pb_paper_base}</pb_paper_base>";
		$xml .= "<pb_photobook_forzaces>{$pb_photobook_forzaces}</pb_photobook_forzaces>";
		$xml .= '</data>';
		
		$retval = $this->get( 'pb_calc', $xml );
		
		return( $retval );
	}

	private function get_pb_calc2()
	{
		global $db, $log;
		$retval = '';

		// use to restore the calc parameters
		$pb_format = intval( $_REQUEST['pb_format'] );
		$pb_pages = intval( $_REQUEST['pb_pages'] );
		$pb_paper = intval( $_REQUEST['pb_paper'] );
		$pb_lamination = intval( $_REQUEST['pb_lamination'] );
		$pb_paper_base = intval( $_REQUEST['pb_paper_base'] );
		$pb_personal_pages = intval( $_REQUEST['pb_personal_pages'] );
		$pb_selected_personal_pages = $_REQUEST['pb_selected_personal_pages'];
		$pb_personal_cover = intval( $_REQUEST['pb_personal_cover'] );
		$pb_cover_category = intval( $_REQUEST['pb_cover_category'] );
		$pb_cover = intval( $_REQUEST['pb_cover'] );
		$pb_supercover = intval( $_REQUEST['pb_supercover'] );
		$pb_design_work_category = intval( $_REQUEST['pb_design_work_category'] );
		$pb_design_work = intval( $_REQUEST['pb_design_work'] );
		$pb_box_category = intval( $_REQUEST['pb_box_category'] );
		$pb_box = intval( $_REQUEST['pb_box'] );
		$pb_quantity = intval( $_REQUEST['pb_quantity'] );
		$pb_forzaces = intval( $_REQUEST['pb_forzaces'] );
		$pb_paper_base = intval( $_REQUEST['pb_paper_base'] );
		$pb_photobook_forzaces = intval( $_REQUEST['pb_photobook_forzaces'] );
      $currencies = Array();

		$pb_items = Array();

		$item = new StdClass();
		$item->covers = Array();
		$item->papers = Array();
		$item->lamination = Array();
		$item->paper_base = Array();
		$item->design_works = Array();
		$item->boxes = Array();
		$item->forzaces = Array();
		$item->id = 0;
		$item->name = $this->dictionary->get('/locale/photobook/select');
		$item->price_base = 0;
		$item->price_per_page = 0;
		$item->price_personal_page = 0;
		$item->price_personal_cover = 0;
		$item->start_page = 0;
		$item->page_block = 0;
		$item->discount1 = 0;
		$item->discount2 = 0;
		$item->discount3 = 0;


        $sql = "select * from currencies c inner join (select max(created_at) created_at,code from currencies group by code) a on a.created_at = c.created_at order by c.code desc;";
        $rs = $db->query( $sql );
        while( $row = $db->getrow($rs) ) {
            $currency_item = new Currency($row[0]);
            array_push($currencies, $currency_item);
        }
        $currencies = json_encode($currencies);
        
        $p_item = new StdClass();
		$p_item->id = 0;
		$p_item->name = $this->dictionary->get('/locale/photobook/select');
		$p_item->description = "";
		$p_item->price = 0;
		$p_item->is_individual = 0;
		$p_item->items = Array();
		array_push( $item->covers, $p_item );

		$p_item = new StdClass();
		$p_item->id = 0;
		$p_item->name = $this->dictionary->get('/locale/photobook/select');
		$p_item->price = 0;
		array_push( $item->papers, $p_item );

		$p_item = new StdClass();
		$p_item->id = 0;
		$p_item->name = $this->dictionary->get('/locale/photobook/select');
		$p_item->price = 0;
		array_push( $item->lamination, $p_item );

		$p_item = new StdClass();
		$p_item->id = 0;
		$p_item->name = $this->dictionary->get('/locale/photobook/select');
		$p_item->price = 0;
		array_push( $item->paper_base, $p_item );

		$p_item = new StdClass();
		$p_item->id = 0;
		$p_item->name = $this->dictionary->get('/locale/photobook/select');
		$p_item->description = "";
		$p_item->items = Array();
		array_push( $item->design_works, $p_item );

		$p_item = new StdClass();
		$p_item->id = 0;
		$p_item->name = $this->dictionary->get('/locale/photobook/select');
		$p_item->description = "";
		$p_item->items = Array();
		array_push( $item->boxes, $p_item );

		$p_item = new StdClass();
		$p_item->id = 0;
		$p_item->name = $this->dictionary->get('/locale/photobook/select');
		$p_item->description = "";
		$p_item->price = 0;
		array_push( $item->forzaces, $p_item );

		array_push( $pb_items, $item );

		$sql = "select id from pb2_sizes where status=1 order by position,id";
		$rs = $db->query( $sql );
		while( $row = $db->getrow($rs) )
		{
			$s = new PB2Size( $row[0] );
			if( $s->IsLoaded() )
			{
				$item = new StdClass();
				$item->covers = Array();
				$item->papers = Array();
				$item->lamination = Array();
				$item->paper_base = Array();
				$item->design_works = Array();
				$item->boxes = Array();
				$item->forzaces = Array();
				$item->id = $s->id;
				$item->type = $s->type;
				$item->name = $s->name;
				$item->price_base = $s->price_base;
				$item->price_per_page = $s->price_per_page;
				$item->price_personal_page = $s->price_personal_page;
				$item->price_personal_cover = $s->price_personal_cover;
				$item->price_design_work = $s->price_design_work;
				$item->start_page = $s->start_page;
				$item->stop_page = $s->stop_page;
				$item->page_block = $s->page_block;
				$item->discount1 = $s->discount1;
				$item->discount2 = $s->discount2;
				$item->discount3 = $s->discount3;

				$item->client_discount = ( $this->client->IsLoaded() )? $this->client->printbook_discount : 0;

				$item->image = ( $s->image )? SITEPREFIX . $_ENV['data_path'] .'/'. $s->image : "";
				$item->thumbnail = ( $s->image )? SITEPREFIX . $_ENV['data_path'] .'/thumbnail-'. $s->image : "";

				$sql = "select id from pb2_categories where folder='cover' and status=1 and parent_id={$s->id} order by position";
				$rs3 = $db->query( $sql );
				while( $row3 = $db->getrow($rs3) )
				{
					$cat = new PB2Category($row3[0]);
					$pcat_item = new StdClass();
					$pcat_item->id = $cat->id;
					$pcat_item->name = $cat->name;
					$pcat_item->description = $cat->description;
					$pcat_item->image = ( $cat->image )? SITEPREFIX . $_ENV['data_path'] .'/'. $cat->image : "";
					$pcat_item->thumbnail = ( $cat->image )? SITEPREFIX . $_ENV['data_path'] .'/thumbnail-'. $cat->image : "";
					$pcat_item->items = Array();

					$sql = "select id from pb2_covers where status=1 and category_id={$row3[0]} and pb_size_id={$s->id} order by position,id";
					$rs2 = $db->query( $sql );
					while( $row2 = $db->getrow($rs2) )
					{
						$p = new PB2Cover( $row2[0] );
						$p_item = new StdClass();
						$p_item->id = $p->id;
						$p_item->name = $p->name;
						$p_item->description = $p->description;
						$p_item->has_supercover = $p->has_supercover;
						$p_item->supercover_price = $p->supercover_price;
						$p_item->price = $p->price;
						$p_item->allow_min_pages = $p->allow_min_pages;
						$p_item->image = ( $p->image )? SITEPREFIX . $_ENV['data_path'] .'/'. $p->image : "";
						$p_item->thumbnail = ( $p->image )? SITEPREFIX . $_ENV['data_path'] .'/thumbnail-'. $p->image : "";

						$p_item->supercovers = Array();
						$sql = "select id from pb2_supercovers where status=1 and pb_cover_id={$p->id} order by position,id";
						$rs4 = $db->query( $sql );
						while( $row4 = $db->getrow($rs4) )
						{
							$p2 = new PB2SuperCover( $row4[0] );
							$p_item2 = new StdClass();
							$p_item2->id = $p2->id;
							$p_item2->name = $p2->name;
							$p_item2->description = $p2->description;
							$p_item2->price = $p2->price;
							$p_item2->image = ( $p2->image )? SITEPREFIX . $_ENV['data_path'] .'/'. $p2->image : "";
							$p_item2->thumbnail = ( $p2->image )? SITEPREFIX . $_ENV['data_path'] .'/thumbnail-'. $p2->image : "";

							array_push( $p_item->supercovers, $p_item2 );
						}

						array_push( $pcat_item->items, $p_item );
					}

					array_push( $item->covers, $pcat_item );
				}

				$sql = "select id from pb2_papers where status=1 and pb_size_id={$s->id} order by position,id";
				$rs2 = $db->query( $sql );
				while( $row2 = $db->getrow($rs2) )
				{
					$p = new PB2Paper( $row2[0] );
					$p_item = new StdClass();
					$p_item->id = $p->id;
					$p_item->name = $p->name;
					$p_item->price = $p->price;

					array_push( $item->papers, $p_item );
				}

				$sql = "select id from pb2_lamination where status=1 and pb_size_id={$s->id} order by position,id";
				$rs2 = $db->query( $sql );
				while( $row2 = $db->getrow($rs2) )
				{
					$p = new PB2Lamination( $row2[0] );
					$p_item = new StdClass();
					$p_item->id = $p->id;
					$p_item->name = $p->name;
					$p_item->price = $p->price;

					array_push( $item->lamination, $p_item );
				}

				$sql = "select id from pb2_paper_base where status=1 and pb_size_id={$s->id} order by position,id";
				$rs2 = $db->query( $sql );
				while( $row2 = $db->getrow($rs2) )
				{
					$p = new PB2PaperBase( $row2[0] );
					$p_item = new StdClass();
					$p_item->id = $p->id;
					$p_item->name = $p->name;
					$p_item->price = $p->price;

					array_push( $item->paper_base, $p_item );
				}

				$sql = "select id from pb2_categories where folder='design_work' and status=1 order by position";
				$rs3 = $db->query( $sql );
				while( $row3 = $db->getrow($rs3) )
				{
					$cat = new PB2Category($row3[0]);
					$pcat_item = new StdClass();
					$pcat_item->id = $cat->id;
					$pcat_item->name = $cat->name;
					$pcat_item->description = $cat->description;
					$pcat_item->image = ( $cat->image )? SITEPREFIX . $_ENV['data_path'] .'/'. $cat->image : "";
					$pcat_item->thumbnail = ( $cat->image )? SITEPREFIX . $_ENV['data_path'] .'/thumbnail-'. $cat->image : "";
					$pcat_item->items = Array();

					$sql = "select id from pb2_design_works where status=1 and category_id={$row3[0]} and pb_size_id={$s->id} order by position,id";
					$rs2 = $db->query( $sql );
					while( $row2 = $db->getrow($rs2) )
					{
						$p = new PB2DesignWork( $row2[0] );
						$p_item = new StdClass();
						$p_item->id = $p->id;
						$p_item->name = $p->name;
						$p_item->url = $p->url;
						$p_item->description = $p->description;
						$p_item->base_price = $p->base_price;
						$p_item->price = $p->price;
						$p_item->image = ( $p->image )? SITEPREFIX . $_ENV['data_path'] .'/'. $p->image : "";
						$p_item->thumbnail = ( $p->image )? SITEPREFIX . $_ENV['data_path'] .'/thumbnail-'. $p->image : "";

						array_push( $pcat_item->items, $p_item );
					}

					array_push( $item->design_works, $pcat_item );
				}

				$sql = "select id from pb2_categories where folder='box' and status=1 order by position";
				$rs3 = $db->query( $sql );
				while( $row3 = $db->getrow($rs3) )
				{
					$cat = new PB2Category($row3[0]);
					$pcat_item = new StdClass();
					$pcat_item->id = $cat->id;
					$pcat_item->name = $cat->name;
					$pcat_item->description = $cat->description;
					$pcat_item->image = ( $cat->image )? SITEPREFIX . $_ENV['data_path'] .'/'. $cat->image : "";
					$pcat_item->thumbnail = ( $cat->image )? SITEPREFIX . $_ENV['data_path'] .'/thumbnail-'. $cat->image : "";
					$pcat_item->items = Array();

					$sql = "select id from pb2_boxes where status=1 and category_id={$row3[0]} and pb_size_id={$s->id} order by position,id";
					$rs2 = $db->query( $sql );
					while( $row2 = $db->getrow($rs2) )
					{
						$p = new PB2Box( $row2[0] );
						$p_item = new StdClass();
						$p_item->id = $p->id;
						$p_item->name = $p->name;
						$p_item->description = $p->description;
						$p_item->price = $p->price;
						$p_item->image = ( $p->image )? SITEPREFIX . $_ENV['data_path'] .'/'. $p->image : "";
						$p_item->thumbnail = ( $p->image )? SITEPREFIX . $_ENV['data_path'] .'/thumbnail-'. $p->image : "";

						array_push( $pcat_item->items, $p_item );
					}

					array_push( $item->boxes, $pcat_item );
				}

				$sql = "select id from pb2_forzaces where status=1 and pb_size_id={$s->id} order by position,id";
				$rs2 = $db->query( $sql );
				while( $row2 = $db->getrow($rs2) )
				{
					$p = new PB2Forzace( $row2[0] );
					$p_item = new StdClass();
					$p_item->id = $p->id;
					$p_item->name = $p->name;
					$p_item->description = $p->description;
					$p_item->price = $p->price;
					$p_item->allow_min_pages = $p->allow_min_pages;
					$p_item->image = ( $p->image )? SITEPREFIX . $_ENV['data_path'] .'/'. $p->image : "";
					$p_item->thumbnail = ( $p->image )? SITEPREFIX . $_ENV['data_path'] .'/thumbnail-'. $p->image : "";

					array_push( $item->forzaces, $p_item );
				}

				array_push( $pb_items, $item );
			}
		}



		$xml = '<data>';
		$xml .= '<pb_items><![CDATA['. json_encode($pb_items) .']]></pb_items>';
		$xml .= "<pb_currencies>{$currencies}</pb_currencies>";
		$xml .= "<pb_format>{$pb_format}</pb_format>";
		$xml .= "<pb_pages>{$pb_pages}</pb_pages>";
		$xml .= "<pb_paper>{$pb_paper}</pb_paper>";
		$xml .= "<pb_lamination>{$pb_lamination}</pb_lamination>";
		$xml .= "<pb_paper_base>{$pb_paper_base}</pb_paper_base>";
		$xml .= "<pb_personal_pages>{$pb_personal_pages}</pb_personal_pages>";
		$xml .= "<pb_selected_personal_pages>{$pb_selected_personal_pages}</pb_selected_personal_pages>";
		$xml .= "<pb_personal_cover>{$pb_personal_cover}</pb_personal_cover>";
		$xml .= "<pb_cover_category>{$pb_cover_category}</pb_cover_category>";
		$xml .= "<pb_cover>{$pb_cover}</pb_cover>";
		$xml .= "<pb_supercover>{$pb_supercover}</pb_supercover>";
		$xml .= "<pb_design_work_category>{$pb_design_work_category}</pb_design_work_category>";
		$xml .= "<pb_design_work>{$pb_design_work}</pb_design_work>";
		$xml .= "<pb_box_category>{$pb_box_category}</pb_box_category>";
		$xml .= "<pb_box>{$pb_box}</pb_box>";
		$xml .= "<pb_quantity>{$pb_quantity}</pb_quantity>";
		$xml .= "<pb_forzaces>{$pb_forzaces}</pb_forzaces>";
		$xml .= "<pb_paper_base>{$pb_paper_base}</pb_paper_base>";
		$xml .= "<pb_photobook_forzaces>{$pb_photobook_forzaces}</pb_photobook_forzaces>";
		$xml .= '</data>';

		$retval = $this->get('pb_calc2', $xml);

		return( $retval );
	}
	
	function add_printbookfile_flash()
	{
		global $log, $db;
		
		if( $this->client->IsLoaded() )
		{
			set_time_limit(3600);
			header("Expires: 0");
			
			$this->_prepare_pb_order();
			
			$msg = "";
			$err = 0; $files = 0; $files_err = 0;
			$file_prefix = 'photo'; $i = 1;
			$log->Write( "add_printbookfiles_flash() starting _FILES=". serialize($_FILES) );
			if( $_FILES[$file_prefix.$i] && $_FILES[$file_prefix.$i]['tmp_name']!='' && is_uploaded_file($_FILES[$file_prefix.$i]['tmp_name']) )
			{
				$files++;
				if( $this->pb_order->UploadImageForm( $file_prefix.$i ) )
				{
					$msg = $this->dictionary->get('/locale/photos/order/add_photos/end_upload_msg');
				}
				else
				{
					$files_err++;
					$msg = $this->dictionary->get('/locale/photos/order/add_photos/error_upload_msg');
					$err = 1;
				}
			}
			
			if( $files == 0 )
			{
				$err = 3;
				$msg = $this->dictionary->get('/locale/photos/order/add_photos/error_upload_nofiles');
			}
			
			//$this->_prepare_pb_order();
			$log->Write( "add_printbookfiles_flash() msg result=". $msg );
			if( $msg ) echo $msg;
		}
		else
		{
			$this->logout();
		}
	}
	
	function add_printbookfile_fine_uploader()
	{
		global $log, $db;
		
		if( $this->client->IsLoaded() )
		{
			set_time_limit(3600);
			header("Expires: 0");
			
			$this->_prepare_pb_order();
			
			$msg = "";
			$err = 0; $files = 0; $files_err = 0;
			$file_prefix = 'qqfile';
			$log->Write( "add_printbookfile_fine_uploader() starting _FILES=". serialize($_FILES) );
			if( $_FILES[$file_prefix.$i] && $_FILES[$file_prefix.$i]['tmp_name']!='' && is_uploaded_file($_FILES[$file_prefix.$i]['tmp_name']) )
			{
				$files++;
				if( $this->pb_order->UploadImageForm( $file_prefix.$i ) )
				{
					$msg = $this->dictionary->get('/locale/photos/order/add_photos/end_upload_msg');
				}
				else
				{
					$files_err++;
					$msg = $this->dictionary->get('/locale/photos/order/add_photos/error_upload_msg');
					$err = 1;
				}
			}
			
			if( $files == 0 )
			{
				$err = 3;
				$msg = $this->dictionary->get('/locale/photos/order/add_photos/error_upload_nofiles');
			}
			
			$log->Write( "add_printbookfile_fine_uploader() msg result=". $msg );

			$response = array(
				'success' => $err === 0,
			);
			if ($err) {
				$response['error'] = $msg;
			}
			echo json_encode($response);	
		}
		else
		{
			$this->logout();
		}
	}
	
	function pb_upload_image() {
		global $log, $db;
		
		$msg = $this->dictionary->get('/locale/photos/order/add_photos/error_upload_msg');
		$success = false;
		$result = Array();
		if( $this->client->IsLoaded() )
		{
			set_time_limit(3600);
			header("Expires: 0");
			
			$this->_prepare_pb_order();
			
			if( isset($_GET['qqfile']) ) {
				$input = fopen("php://input", "r");
				$temp = tmpfile();
				$realSize = stream_copy_to_stream($input, $temp);
				fclose($input);
				if( $realSize == intval( $_SERVER["CONTENT_LENGTH"] ) ) {
					$path = SITEROOT .'/'. $_ENV['printbook_path'] .'/orders/'. $this->pb_order->order_number .'/';
					$suffix = md5( mt_rand(1,9) . time() );
					$ext = get_file_ext( $_GET['qqfile'] );
					$target_name = $suffix .'.'. $ext;
					$target = fopen( $path . $target_name, "w" );
					fseek($temp, 0, SEEK_SET);
					stream_copy_to_stream($temp, $target);
					fclose($target);
					
					$id = intval( $_GET['id'] );
					$this->pb_order->UpdateUploadedImage( Array(
						'id' => $id,
						'filename' => $target_name,
						'original_filename' => $_GET['qqfile']
					) );
					
					$success = true;
					$msg = "";
				}
			}
			elseif( isset($_FILES['qqfile']) ) {
				if( $this->pb_order->UploadImageForm( 'qqfile' ) )
				{
					$success = true;
					$msg = $this->dictionary->get('/locale/photos/order/add_photos/end_upload_msg');
				}
			}
		}
		else
		{
			echo json_encode( array( 'error' => $msg ) );
		}
		
		$result["error"] = $msg;
		$result["success"] = $success;
		json_encode( $result );
	}
	
	function pb_get_order() {
		$JsHttpRequest = new JsHttpRequest("utf-8");

		$this->_prepare_pb_order();

		header("Expires: 0");
		header("Cache-Control: private");
		header("Pragma: no-cache");
		
		$err = 0;
		$msg = '';
		$path = '';
		
		$GLOBALS['_RESULT']['err'] = $err;
		$GLOBALS['_RESULT']['msg'] = $msg;
	}
	
	function pb_get_images() {
		$JsHttpRequest = new JsHttpRequest("utf-8");
	
		$this->_prepare_pb_order();
			
		header("Expires: 0");
		header("Cache-Control: private");
		header("Pragma: no-cache");
		
		$items = Array();
		$err = 0;
		$msg = '';
		$path = '';
		
		if( $this->pb_order->id > 0 && $this->pb_order->images ) {
			$items = $this->pb_order->images;
		}
		
		$GLOBALS['_RESULT']['pb_order'] = null;
		
		$GLOBALS['_RESULT']['items'] = $items;
		$GLOBALS['_RESULT']['err'] = $err;
		$GLOBALS['_RESULT']['msg'] = $msg;

	}
	
	function pb_image()
	{
		$f = SITEROOT.'/empty.gif';
		$type = intval( $_REQUEST['type'] ); /* 0/1/2 == thumbnail/small/original */
		$id = intval( $_REQUEST['id'] );
		$o = new PBOrderImage( $id );
		if( $o->IsLoaded() && $this->pb_order && $this->pb_order->id == $o->order_id )
		{                             
			$path = SITEROOT .'/'. $_ENV['printbook_path'] .'/orders/'. $this->pb_order->order_number .'/';
			switch( $type )
			{
				case 1:
					$ff = $path . $o->small;
					break;
				case 2:
					$ff = $path . $o->filename;
					break;
				default:
					$ff = $path . $o->thumbnail;
					break;
			}
			if( file_exists($ff) )
			{
				$f = $ff;
			}
		}
		
		header( "Content-type: image/jpeg" );
		readfile( $f );
	}
	
	function pb_update() {
		global $log;
		
		$JsHttpRequest = new JsHttpRequest("utf-8");
	
		$this->_prepare_pb_order();
			
		header("Expires: 0");
		header("Cache-Control: private");
		header("Pragma: no-cache");
		
		$err = 0;
		$msg = '';
		$path = '';
		
		$data = Array();
		$data['id'] = $this->pb_order->id;
		$data['size_id'] = intval( $_REQUEST['size_id'] );
		$data['pages'] = intval( $_REQUEST['pages'] );
		$data['quantity'] = intval( $_REQUEST['quantity'] );
		$data['paper_id'] = intval( $_REQUEST['paper_id'] );
		$data['paper_price'] = 0;
		$data['lamination_id'] = intval( $_REQUEST['lamination_id'] );
		$data['lamination_price'] = 0;
		$data['paper_base_id'] = intval( $_REQUEST['paper_base_id'] );
		$data['paper_base_price'] = 0;
		$data['personal_pages'] = intval( $_REQUEST['personal_pages'] );
		$data['selected_personal_pages'] = $_REQUEST['selected_personal_pages'];
		$data['personal_cover_id'] = intval( $_REQUEST['personal_cover_id'] );
		$data['personal_cover_price'] = 0;
		$data['cover_id'] = intval( $_REQUEST['cover_id'] );
		$data['cover_price'] = 0;
		$data['supercover_id'] = intval( $_REQUEST['supercover_id'] );
		$data['supercover_price'] = 0;
		$data['box_id'] = intval( $_REQUEST['box_id'] );
		$data['box_price'] = floatval( $_REQUEST['box_price'] );
		$data['design_work_id'] = intval( $_REQUEST['design_work_id'] );
		$data['design_work_base_price'] = 0;
		$data['design_work_price'] = 0;
		$data['forzaces'] = intval( $_REQUEST['forzaces'] );
		$data['discount'] = $this->pb_order->discount;
		$data['forzac_id'] = intval( $_REQUEST['forzac_id'] );
		$data['forzac_price'] = 0;
		
		if( $data['size_id'] > 0 ) {
			$size = new PBSize( $data['size_id'] );
			if( $size->IsLoaded() ) {
				$data['price_base'] = $size->price_base;
				$data['price_per_page'] = $size->price_per_page;
				$data['price_personal_page'] = $size->price_personal_page;
				$data['price_personal_cover'] = $size->price_personal_cover;
			} 
		}
		
		if( $data['paper_id'] > 0 ) {
			$paper = new PBPaper( $data['paper_id'] );
			if( $paper->IsLoaded() ) {
				$data['paper_price'] = floatval( $paper->price );
			}
		}
		
		if( $data['lamination_id'] > 0 ) {
			$lamination = new PBLamination( $data['lamination_id'] );
			if( $lamination->IsLoaded() ) {
				$data['lamination_price'] = floatval( $lamination->price );
			}
		}
		
		if( $data['paper_base_id'] > 0 ) {
			$paper_base = new PBPaperBase( $data['paper_base_id'] );
			if( $paper_base->IsLoaded() ) {
				$data['paper_base_price'] = floatval( $paper_base->price );
			}
		}
		
		if( $data['personal_cover_id'] > 0 ) {
			$pcover = new PBPersonalCover( $data['personal_cover_id'] );
			if( $pcover->IsLoaded() ) {
				$data['personal_cover_price'] = floatval( $pcover->price );
			}
		}
		
		if( $data['cover_id'] > 0 ) {
			$cover = new PBCover( $data['cover_id'] );
			if( $cover->IsLoaded() ) {
				$data['cover_price'] = floatval( $cover->price );
			}
		}
		
		if( $data['supercover_id'] > 0 ) {
			$scover = new PBSuperCover( $data['supercover_id'] );
			if( $scover->IsLoaded() ) {
				$data['supercover_price'] = floatval( $scover->price );
			}
		}
		
		if( $data['box_id'] > 0 ) {
			$box = new PBBox( $data['box_id'] );
			if( $box->IsLoaded() ) {
				$data['box_price'] = floatval( $box->price );
			}
		}
		
		if( $data['design_work_id'] > 0 ) {
			$dwork = new PBDesignWork( $data['design_work_id'] );
			if( $dwork->IsLoaded() ) {
				$data['design_work_price'] = floatval( $dwork->price );
				$data['design_work_base_price'] = floatval( $dwork->base_price );
			}
		}
		
		if( $data['forzac_id'] > 0 ) {
			$forzac = new PBForzace( $data['forzac_id'] );
			if( $forzac->IsLoaded() ) {
				$data['forzac_price'] = floatval( $forzac->price );
			}
		}
		
		//$log->Write( "pb_update, id={$this->pb_order->id} data=". json_encode($data) );
		if( $this->pb_order->id > 0 ) {
			$this->pb_order->Save( $data );
			$this->_prepare_pb_order();
		}
		
		$GLOBALS['_RESULT']['path'] = $path;
		$GLOBALS['_RESULT']['err'] = $err;
		$GLOBALS['_RESULT']['msg'] = $msg;

	}
	
	function pb_update_image() {
		global $log;
		
		$JsHttpRequest = new JsHttpRequest("utf-8");
	
		$this->_prepare_pb_order();
			
		header("Expires: 0");
		header("Cache-Control: private");
		header("Pragma: no-cache");
		
		$err = 0;
		$msg = '';
		$path = '';
		$item = null;
		
		$data = Array();
		$data['id'] = intval( $_REQUEST['id'] );
		$data['page'] = intval( $_REQUEST['page'] );
		$data['personal_page'] = intval( $_REQUEST['personal_page'] );
		$data['type'] = intval( $_REQUEST['type'] );
		$data['comments'] = trim( $_REQUEST['comments'] );
		
		if( $data['page'] > $this->pb_order->pages ) $data['page'] = $this->pb_order->pages; 
		
		if( $data['id'] > 0 ) {
			$image = new PBOrderImage( $data['id'] );
			if( $image->IsLoaded() ) {
				$image->Save( $data );
				$item = $image;
			} 
			else {
				$err = 2;
				$msg = "Image wasn't loaded";
			}
		}
		else {
			$err = 1;
			$msg = "ID is null";
		}
		
		$GLOBALS['_RESULT']['item'] = $item;
		$GLOBALS['_RESULT']['path'] = $path;
		$GLOBALS['_RESULT']['err'] = $err;
		$GLOBALS['_RESULT']['msg'] = $msg;

	}
	
	function pb_delete_image() {
		global $log;
		
		$err = 0;
		$msg = '';
		
		$JsHttpRequest = new JsHttpRequest("utf-8");
		
		$GLOBALS['_RESULT'] = Array();
		
		$id = $this->pb_order->id;
		$image_id = intval( $_REQUEST['id'] );
		
		if( $id > 0 && $image_id > 0 ) {
			$o = new PBOrderImage( $image_id );
			if( $o->IsLoaded() ) {
				$o->Delete();
			}
			else {
				$err = 2;
				$msg = "This image doesn't exist";
			}
		}
		else {
			$err = 1;
			$msg = "Parameters error";
		}
		
		$GLOBALS['_RESULT']['err'] = $err;
		$GLOBALS['_RESULT']['msg'] = $msg;
	}
	
	function pb_delete_images() {
		global $log;
		
		$err = 0;
		$msg = '';
		
		$JsHttpRequest = new JsHttpRequest("utf-8");
		
		$GLOBALS['_RESULT'] = Array();
		
		$id = $this->pb_order->id;
		$ids = $_REQUEST['ids'];
		
		if( $id > 0 && $ids ) {
			$aids = split( ',', $ids );
			foreach( $aids as $image_id ) {
				$o = new PBOrderImage( $image_id );
				if( $o->IsLoaded() ) {
					$o->Delete();
				}
				else {
					$err = 2;
					$msg = "One or more images don't exist";
				}
			}
		}
		else {
			$err = 1;
			$msg = "Parameters error";
		}
		
		$GLOBALS['_RESULT']['err'] = $err;
		$GLOBALS['_RESULT']['msg'] = $msg;
	}
	
	function pb_delete_all_images() {
		global $log;
		
		$err = 0;
		$msg = '';
		
		$JsHttpRequest = new JsHttpRequest("utf-8");
		
		$GLOBALS['_RESULT'] = Array();
		
		$this->_prepare_pb_order();
		$id = $this->pb_order->id;
		$log->Write( "pb_delete_all_images, id={$this->pb_order->id}");
		if( $id > 0 ) {
			$this->pb_order->DeleteAllImages();
		}
		else {
			$err = 1;
			$msg = "Parameters error";
		}
		
		$GLOBALS['_RESULT']['err'] = $err;
		$GLOBALS['_RESULT']['msg'] = $msg;
	}
	
	function pb_order_page_comment_get() {
		global $log;
		
		$err = 0;
		$msg = '';
		
		$JsHttpRequest = new JsHttpRequest("utf-8");
		
		$GLOBALS['_RESULT'] = Array();
		
		$id = $this->pb_order->id;
		$page = intval( $_REQUEST['page'] );
		
		$value = '';
		if( $id > 0 && $page!=0 && $page!=null ) {
			$comment = new PBOrderPageComment();
			$comment->LoadByOrderAndPage( $id, $page );
			if( $comment->IsLoaded() ) {
				$value = $comment->comments;
			} 
		}
		else {
			$err = 1;
			$msg = "Parameters error";
		}
		
		$GLOBALS['_RESULT']['value'] = $value;
		$GLOBALS['_RESULT']['err'] = $err;
		$GLOBALS['_RESULT']['msg'] = $msg;
	}
	
	function pb_order_page_comment_set() {
		global $log;
		
		$err = 0;
		$msg = '';
		
		$JsHttpRequest = new JsHttpRequest("utf-8");
		
		$GLOBALS['_RESULT'] = Array();
		
		$id = $this->pb_order->id;
		$page = intval( $_REQUEST['page'] );
		$comment = trim( $_REQUEST['comment'] );
		
		if( $id > 0 && $page!=0 && $page!=null ) {
			$o = new PBOrderPageComment();
			$o->LoadByOrderAndPage( $id, $page );
			$o->order_id = $id;
			$o->page = $page;
			$o->comments = $comment;

			$o->Save();
		}
		else {
			$err = 1;
			$msg = "Parameters error";
		}
		
		$GLOBALS['_RESULT']['err'] = $err;
		$GLOBALS['_RESULT']['msg'] = $msg;
	}
	/* printbook */
	
	/*
		<static page name> - it's folder with structure:
			css/
			img/
			index.html - UTF-8
	*/
	function staticpage() {
		global $log;
		
		$this->method_name = 'show';
		$this->make_leftmenu( 0, 0 );
		
		$content = '';
		$css_files = Array();
		$name = $_REQUEST['name'];
		if( $name ) {
			$path = sprintf('%s/%s/static/%s/', SITEROOT, $_ENV['app_path'] .'/'. $_ENV['views_path'] .'/'. $this->controller_name, $name );
			
			$page_url_prefix = '/'. $_ENV['app_path'] .'/'. $_ENV['views_path'] .'/'. $this->controller_name .'/static/'. $name .'/';
			$css_dir = opendir( $path .'/css' );
			while( $f = readdir($css_dir) )
			{
				if( substr($f, -4) == '.css')
				{
					array_push( $css_files, $page_url_prefix .'css/'. $f );
				}
			}
			closedir($css_dir);
			
			$html = $path .'/index.html';
			if( is_file($html) ) {
				$content = file_get_contents($html);
				$content = preg_replace( "/src\s*=\s*[\"']{1}img/i", "src=\"". $page_url_prefix.'img', $content );
			}
			
			$head = $path .'head.html';
			$page_title = '';
			$page_keywords = '';
			if( is_file($head) ) {
				$head_content = file_get_contents($head);
				if( preg_match( "/<title>([^<]+)<\/title>/i", $head_content, $matches ) ) {
					$page_title = $matches[1];
				}
				if( preg_match( "/<meta\s+name\s*=\s*\"keywords\"\s+content\s*=\s*\"(.+)\"\s*\/?>/i", $head_content, $matches ) ) {
					$page_keywords = $matches[1];
				}
			}
		}
		
		$xml = "<data>";
		$xml .= "<css>";
		foreach( $css_files as $css ) {
			$xml .= "<item>{$css}</item>";
		}
		$xml .= "</css>";
		$xml .= "<content><![CDATA[{$content}]]></content>";
		
		$xml .= "<category>";
		$xml .= "<page_title><![CDATA[{$page_title}]]></page_title>";
		$xml .= "<page_keywords><![CDATA[{$page_keywords}]]></page_keywords>";
		$xml .= "</category>";
		
		$xml .= "</data>";
		$this->display( 'staticpage', $xml );
	}
}

?>
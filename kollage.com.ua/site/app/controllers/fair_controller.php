<?php
/*****************************************************
 Class v.1.0, 2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

include_once( 'base_controller.php' );

class fair_controller extends base_controller
{
	function GetAdditionXml()
	{
		global $db;
		
		$category_id = $db->getone( "select id from categories where folder='fair'" );
		$t = new MysqlTable('articles');
		$t->find_all( "category_id={$category_id} and folder='fair' and status=1 and show_in_menu=1", 'position,id' );
		$xml .= "<fair_articles>";
		foreach( $t->data as $a )
		{
			$article = new Article( $a['id'] );
			$xml .= $article->Xml();
		}
		$xml .= "</fair_articles>";
		
		return( $xml );
	}
	
	function show_article()
	{
		$this->method_name = 'show_article';
		
		$id = intval( $_REQUEST['id'] );
		
		$this->make_leftmenu( 0, 0 );
		
		$xml = "<data>";
		$article = new Article( $id ); if( $article->status == 1 ) $xml .= $article->Xml(true);
		$xml .= "</data>";
		$this->display( 'show_article', $xml );
	}
	
	function show()
	{
		global $db;
		
		$this->method_name = 'show';
		
		$category_id = intval( $_REQUEST['category_id'] );
		$id = intval( $_REQUEST['id'] );
		
		//if( $category_id == 0 ) $category_id = $this->find_first_category();
		if( $id == 0 ) $id = $this->find_first_article( $category_id );
		$this->make_leftmenu( $category_id, $id );
		
		$xml = "<data>";

		if( $category_id == 0 )
		{
			$xml .= "<cline>";
			$xml .= "</cline>";
			
			if( !$category_id ) $xml .= "<design type='new'/>";
			else $xml .= "<design type='category'/>";
			$xml .= "<back_url><![CDATA[{$_SESSION['back_url_for_trash']}]]></back_url>";
			
			$t = new MysqlTable('fair_objects');
			$sql = "select p.id from fair_objects p where p.status=1 and category_id1>0 order by p.id desc limit 18";
			$xml .= "<fair_objects>";
			$rs = $db->query($sql);
			while( $row = $rs->fetch_row() )
			{
				$o = new FairObject( $row[0], false );
				$xml .= $o->Xml();
			}
			$xml .= "</fair_objects>";
			
			$sql = "select p.id from fair_objects p where p.status=1 and category_id1>0 order by p.id desc limit 18,9999999";
			$xml .= "<rest_fair_objects>";
			$rs = $db->query($sql);
			while( $row = $rs->fetch_row() )
			{
				$o = new FairObject( $row[0], false );
				$xml .= $o->Xml();
			}
			$xml .= "</rest_fair_objects>";
		}
		else
		{
			$category = new FairCategory( $category_id ); $xml .= $category->Xml();
			
			$cline_ids = $db->getone( "select fair_objects_categories_line({$category_id})" );
			$cline_ids_a = split( ",", $cline_ids );
			$cline_ids_a = array_reverse( $cline_ids_a );
			
			$xml .= "<cline>";
			foreach( $cline_ids_a as $cline_item )
			{
				$cline_c = new FairCategory( $cline_item );
				if( $cline_c->IsLoaded() ) $xml .= $cline_c->Xml();
			}
			$xml .= "</cline>";
			
			$free_section_where = "";
			if( $_REQUEST['free_section'] )
			{
				$free_section = $_REQUEST['free_section'];
				$_SESSION['free_section'] = $free_section;
			}
			else
			{
				$free_section = $_SESSION['free_section'];
			}
			if( $free_section == 1 )
			{
				$free_section_where = " and p.xs_price>0";
			}
			else if( $free_section == 2 )
			{
				$free_section_where = " and (p.xs_price=0 or p.xs_price is null)";
			}
			
			$soft_section_where = "";
			if( $_REQUEST['soft_section'] )
			{
				$soft_section = $_REQUEST['soft_section'];
				$_SESSION['soft_section'] = $soft_section;
			}
			else
			{
				$soft_section = $_SESSION['soft_section'];
			}
			if( $soft_section == 1 )
			{
				$soft_section_where = " and p.was_used_software=1";
			}
			else if( $soft_section == 2 )
			{
				$soft_section_where = " and p.was_used_software=0";
			}
			
			$db->query( "set @ids=''" );
			$db->query( "set max_sp_recursion_depth=1000" );
			$db->query( "call fair_categories({$category_id},@ids)" );
			$ids = $db->getone( "select @ids" );
			
			if( !$category_id ) $xml .= "<design type='new'/>";
			else $xml .= "<design type='category'/>";
			
			$xml .= "<back_url><![CDATA[{$_SESSION['back_url_for_trash']}]]></back_url>";
			$t = new MysqlTable('fair_objects');
			$this->rows_per_page = 18;
			parent::define_pages( $db->getone("select count(p.id) from fair_objects p where p.status=1 and category_id1 in ({$ids}) {$free_section_where}{$soft_section_where}") );
			$sql = "select p.id from fair_objects p where p.status=1 and category_id1 in ({$ids}) {$free_section_where}{$soft_section_where} order by id desc limit ". ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page;
			$xml .= "<fair_objects free_section='{$free_section}' soft_section='{$soft_section}'>";
			$rs = $db->query($sql);
			while( $row = $rs->fetch_row() )
			{
				$o = new FairObject( $row[0], false );
				$xml .= $o->Xml();
			}
			$xml .= "</fair_objects>";
			
			if( $this->page > 1 )
			{
				$xml .= "<rest_fair_objects_prev free_section='{$free_section}' soft_section='{$soft_section}'>";
				$sql = "select p.id from fair_objects p where p.status=1 and category_id1 in ({$ids}) {$free_section_where}{$soft_section_where} order by id desc limit 0,". ($this->page-1)*$this->rows_per_page;
				$rs = $db->query($sql);
				while( $row = $rs->fetch_row() )
				{
					$o = new FairObject( $row[0], false );
					$xml .= $o->Xml();
				}
				$xml .= "</rest_fair_objects_prev>";
				
				$xml .= "<rest_fair_objects free_section='{$free_section}' soft_section='{$soft_section}'>";
				$sql = "select p.id from fair_objects p where p.status=1 and category_id1 in ({$ids}) {$free_section_where}{$soft_section_where} order by id desc limit ". ($this->page)*$this->rows_per_page .",9999999";
				$rs = $db->query($sql);
				while( $row = $rs->fetch_row() )
				{
					$o = new FairObject( $row[0], false );
					$xml .= $o->Xml();
				}
				$xml .= "</rest_fair_objects>";
			}
			else
			{
				$xml .= "<rest_fair_objects free_section='{$free_section}' soft_section='{$soft_section}'>";
				$sql = "select p.id from fair_objects p where p.status=1 and category_id1 in ({$ids}) {$free_section_where}{$soft_section_where} order by id desc limit ". $this->rows_per_page .",9999999";
				$rs = $db->query($sql);
				while( $row = $rs->fetch_row() )
				{
					$o = new FairObject( $row[0], false );
					$xml .= $o->Xml();
				}
				$xml .= "</rest_fair_objects>";
			}
		}
		
		$xml .= "</data>";
		
		$this->display( 'show', $xml );
	}
	
	function show_contacts()
	{
		$this->method_name = 'show_contacts';
		
		if( $category_id == 0 ) $category_id = $this->find_first_category();
		$this->make_leftmenu( $category_id, $id );
		$xml = "<data>";
		$article = new Article('contact');
		if( $article->status == 1 ) $xml .= $article->Xml(true);
		$xml .= "</data>";
		$this->display( 'show_contacts', $xml );
	}
	
	function make_leftmenu( $category_id=0, $item_id=0 )
	{
		global $db;
		
		$t = new MysqlTable('fair_categories');
		$t->find_all( "folder='fair' and parent_id is null", "position,id" );
		foreach( $t->data as $row )
		{
			$o = new FairCategory( $row['id'] );
			//$this->leftmenu->add_item( new WEMenuItem( "item". $o->id, $o->name, "?".$this->controller_name.".show&category_id={$o->id}" ) );
			$submenu = new WEMenu( "psubmenu". $o->id, $o->name, "?".$this->controller_name.".show&category_id={$o->id}" );
			$this->leftmenu->add_item( $submenu );
			$t2 = new MysqlTable('fair_categories');
			$t2->find_all( "folder='fair' and parent_id={$o->id}", "position,id" );
			foreach( $t2->data as $row2 )
			{
				$cs = new FairCategory( $row2['id'] ); if( $cs->link_to_category_id ) $cs->id = $cs->link_to_category_id;
				
				$db->query( "set @ids=''" );
				$db->query( "set max_sp_recursion_depth=1000" );
				$db->query( "call fair_categories({$cs->id},@ids)" );
				$ids = $db->getone( "select @ids" );
				$o_count = $db->getone( "select count(id) from fair_objects where status=1 and category_id1 in ({$ids})" );
				
				$submenu2 = new WEMenu( "psubmenu". $cs->id, $cs->name ." ({$o_count})", "?".$this->controller_name.".show&category_id={$cs->id}" );
				$submenu->add_item( $submenu2 );
				$t3 = new MysqlTable('fair_categories');
				$t3->find_all( "folder='fair' and parent_id={$cs->id}", "position,id" );
				foreach( $t3->data as $row3 )
				{
					$cs2 = new FairCategory( $row3['id'] ); if( $cs2->link_to_category_id ) $cs2->id = $cs2->link_to_category_id;
					$submenu2->add_item( new WEMenuItem( "c". $cs2->id, $cs2->name, "?".$this->controller_name.".show&category_id={$cs2->id}" ) );
				}
			}
		}
		
		$this->leftmenu->select_child( "psubmenu$category_id" );
		$this->leftmenu->select_child( "c$category_id" );
		//echo $this->leftmenu->Xml();
	}
	
	function find_first_category()
	{
		$retval = 0;
		
		$t = new MysqlTable( 'fair_categories' );
		if( $row = $t->find_first("folder='fair'","position,id") )
		{
			$retval = intval( $row['id'] );
		}
		
		return( $retval );
	}
	
	function find_first_article( $category_id=0 )
	{
		$retval = 0;
		
		$t = new MysqlTable( 'articles' );
		if( $row = $t->find_first("folder='fair' and category_id={$category_id}","position,id") )
		{
			$retval = intval( $row['id'] );
		}
		
		return( $retval );
	}
	
	function search()
	{
		$this->method_name = 'search';
		
		$category_id = intval( $_REQUEST['category_id'] );
		$id = intval( $_REQUEST['id'] );
		
		if( $category_id == 0 ) $category_id = $this->find_first_category();
		$this->make_leftmenu( $category_id, $id );
		
		parent::search( 'fair' );
	}
	
	function my_fair( $error=0, $msg='' )
	{
		global $db;
		$this->method_name = 'my_fair';
		
		if( $this->client->allow_fair == 1 )
		{
			$this->make_leftmenu( $category_id, $id );
			$xml = "<data>";
			$this->rows_per_page = 18;
			parent::define_pages( $db->getone("select count(id) from fair_objects where client_id={$this->client->id} and status in (0,1)") );
			$sql = "select id from fair_objects where client_id={$this->client->id} and status in (0,1) order by id limit ". ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page;
			$rs = $db->query( $sql );
			$xml .= "<objects>";
			while( $row = $rs->fetch_row() )
			{
				$o = new FairObject( $row[0] );
				$xml .= $o->Xml();
			}
			$xml .= "</objects>";
			$xml .= "<error code='{$error}'><![CDATA[{$msg}]]></error>";
			$xml .= "</data>";
			
			$this->display( 'my_fair', $xml );
		}
		else $this->show();
	}
	
	function edit_object( $error='', $msg='' )
	{
		global $db;
		$this->method_name = 'edit_object';
		$this->page = intval( $_REQUEST['page'] );
		
		if( $this->client->allow_fair == 1 )
		{
			$id = intval( $_REQUEST['id'] );
			
			$this->make_leftmenu( $category_id, $id );
			$xml = "<data>";
			$o = new FairObject( $id );
			if( $_REQUEST['object']['category_id1'] ) $o->category_id1 = $_REQUEST['object']['category_id1'];
			if( $_REQUEST['object']['title'] ) $o->title = $_REQUEST['object']['title'];
			if( $_REQUEST['object']['description'] ) $o->description = $_REQUEST['object']['description'];
			if( $_REQUEST['object']['keywords'] ) $o->keywords = $_REQUEST['object']['keywords'];
			$xml .= $o->Xml();
			if( $o->IsLoaded() )
			{
				$xml .= "<categories_line>". $db->getone("select fair_categories_line({$o->category_id1})") ."</categories_line>";
			}
			$xml .= "<categories_js><![CDATA[". $this->GetCategoriesJS() ."]]></categories_js>";
			$xml .= "<error code='{$error}'><![CDATA[{$msg}]]></error>";
			$xml .= "</data>";
			
			$this->display( 'edit_object', $xml );
		}
		else $this->show();
	}
	
	function save_object()
	{
		global $db;
		$this->method_name = 'save_object';
		
		if( $this->client->allow_fair == 1 )
		{
			$id = intval( $_REQUEST['id'] );
			$category_id1 = intval( $_REQUEST['category_id1'] );
			$category_id2 = intval( $_REQUEST['category_id2'] );
			$category_id3 = intval( $_REQUEST['category_id3'] );
			
			if( $category_id3 > 0 ) $_REQUEST['object']['category_id1'] = $category_id3;
			else if( $category_id2 > 0 ) $_REQUEST['object']['category_id1'] = $category_id2;
			else $_REQUEST['object']['category_id1'] = $category_id1;
			
			$o = new FairObject( $id );
			if( intval($_REQUEST['object']['category_id1'])>0 && trim($_REQUEST['object']['title'])!='' && trim($_REQUEST['object']['description'])!='' && trim($_REQUEST['object']['keywords'])!='' && $o->Save( $_REQUEST['object'] ) )
			{
				$this->edit_object();
			}
			else
			{
				$msg = $this->dictionary->get('/locale/my/fair/wrong_parameters');
				$this->edit_object( 1, $msg );
			}
		}
		else $this->show();
	}
	
	function add_object()
	{
		global $log, $db;
		
		if( $this->client->allow_fair == 1 )
		{
			set_time_limit(3600);
			header("Expires: 0");
			
			$filetype = new AFileType();
			$filetype->SetFile( $_FILES['file']['tmp_name'] );
			if( $filetype->GetType()==FILE_JPG && $_FILES['file'] && $_FILES['file']['tmp_name']!='' )
			{
				$o = new FairObject();
				$o->client_id = $this->client->id;
				$error = $o->upload();
				if( $error == 1 )
				{
					$_REQUEST['id'] = $o->id;
					$this->edit_object();
				}
				else
				{
					$log->Write( "fair.add_object: Error. Type number=". $filetype->GetType() ." tmpname=". $_FILES['file']['tmp_name'] );
					$msg = $this->dictionary->get('/locale/my/fair/error_upload_size');
					$this->my_fair( 2, $msg );
				}
			}
			else
			{
				$log->Write( "fair.add_object: Error. Type number=". $filetype->GetType() ." tmpname=". $_FILES['file']['tmp_name'] );
				$msg = $this->dictionary->get('/locale/my/fair/error_upload_format');
				$this->my_fair( 1, $msg );
			}
		}
		else $this->show();
	}
	
	function add_objects()
	{
		global $log, $db;
		
		if( $this->client->allow_fair == 1 )
		{
			set_time_limit(3600);
			header("Expires: 0");
			
			$filetype = new AFileType();
			
			$file_prefix = 'photo';
			$c = 0;
			$error_code = '';
			$error_msg = '';
			
			for( $i=1; $i<=16; $i++ )
			{
				$filetype->SetFile( $_FILES[$file_prefix . $i]['tmp_name'] );
				if( $filetype->GetType()==FILE_JPG && $_FILES[$file_prefix . $i] && $_FILES[$file_prefix . $i]['tmp_name']!='' )
				{
					$o = new FairObject();
					$o->client_id = $this->client->id;
					$error = $o->upload( $file_prefix . $i );
					if( $error == 1 )
					{
					}
					else
					{
						if( $_FILES[$file_prefix . $i]['tmp_name'] ) $c++;
						$error_code = 3;
						$log->Write( "fair.add_object: Error. Type number=". $filetype->GetType() ." tmpname=". $_FILES[$file_prefix . $i]['tmp_name'] );
					}
				}
				else
				{
					if( $_FILES[$file_prefix . $i]['tmp_name'] ) $c++;
					$error_code = 3;
					$log->Write( "fair.add_object: Error. Type number=". $filetype->GetType() ." tmpname=". $_FILES[$file_prefix . $i]['tmp_name'] );
				}
			}
			
			if( $c > 0 )
			{
				$error_msg = str_replace( '{COUNT}', $c, $this->dictionary->get('/locale/my/fair/error_upload_all_files') );
			}
			
			$this->my_fair( $error_code, $error_msg );
		}
		else $this->show();
	}
	
	function GetCategoriesJS()
	{
		global $db;

		$delim = "";
		$retval = "[";
		$rs = $db->query( "select id from fair_categories where parent_id is null or parent_id=0 order by position,id" );
		while( $row = $rs->fetch_row() )
		{
			$o = new FairCategory( $row[0] );
			$retval .= $delim . "[{$o->id},'{$o->name}',[". $this->GetSubcategoriesJS($row[0]) ."]]";
			$delim = ",";
		}
		$retval .= "]";
		
		return( $retval );
	}
	
	function GetSubcategoriesJS( $parent_id )
	{
		global $db;

		$retval = "";
		
		$delim = "";
		$rs = $db->query( "select id from fair_categories where parent_id={$parent_id} order by position,id" );
		while( $row = $rs->fetch_row() )
		{
			$o = new FairCategory( $row[0] );
			$retval .= $delim . "[{$o->id},'{$o->name}',[". $this->GetSubcategoriesJS($row[0]) ."]]";
			$delim = ",";
		}
		
		return( $retval );
	}
	
	function delete_marked()
	{
		if( $this->client->allow_fair == 1 )
		{
			$ids_str = $_REQUEST['ids'];
			$ids = split( ',', $ids_str );
			foreach( $ids as $id )
			{
				$o = new FairObject( $id );
				$o->Delete();
			}
			$this->my_fair();
		}
		else $this->show();
	}
	
	function object()
	{
		global $db;
		
		$this->method_name = 'object';
		
		$back_page = $_REQUEST['back_page'];
		
		$o = new FairObject( $_REQUEST['id'] );
		
		$db->query( "set @ids=''" );
		$db->query( "set max_sp_recursion_depth=10000" );
		$db->query( "call fair_categories(". $o->category_id .",@ids)" );
		$ids = $db->getone( "select @ids" );
		
		if( $o->IsLoaded() )
		{
			$c = new FairCategory( $o->category_id1 );
			$o->IncreaseViews();
			
			$back_url = "?fair.{$this->method_name}&id={$o->id}&back_page={$back_page}&page={$page}";
			$_SESSION['back_url_for_trash'] = urlencode($back_url);
			$fair_url = urlencode( "?".$this->controller_name .".show&category_id=". $c->id ."&page=". $back_page );
			
			$this->make_leftmenu( $c->id, $o->id );
			
			$xml = "<data>";
			$xml .= "<fair_url>{$fair_url}</fair_url>";
			$xml .= "<back_page>{$back_page}</back_page>";
			$xml .= "<back_url><![CDATA[{$_SESSION['back_url_for_trash']}]]></back_url>";
			$xml .= $o->Xml();
			$xml .= $c->Xml();
			$xml .= "</data>";
			$this->display( 'object', $xml );
		}
		else $this->show();
	}
	
	function display_photo()
	{
		$f = SITEROOT.'/empty.gif';
		$type = intval( $_REQUEST['type'] ); /* 0/1 == thumbnail/preview */
		$id = intval( $_REQUEST['id'] );
		$o = new FairObject( $id );
		if( $o->IsLoaded() )
		{
			switch( $type )
			{
				case 1:
					$ff = SITEROOT.'/'.$_ENV['fair_path'].'/'.$o->preview;
					break;
				default:
					$ff = SITEROOT.'/'.$_ENV['fair_path'].'/'.$o->thumbnail;
					break;
			}
			if( file_exists($ff) )
			{
				$f = $ff;
			}
		}
		
		header( "Content-type: image/gif" );
		readfile( $f );
	}
	
	function download_free_photo()
	{
		$id = intval( $_REQUEST['id'] );
		$s = intval( $_REQUEST['s'] );
		$o = new FairObject( $id );
		if( $o->IsLoaded() && $this->FileIsFree( $o, $s ) )
		{
			$field = $this->GetFieldBySize( $s );
			$file = $this->GetFilenameBySize( $o, $s );
			$file = SITEROOT .'/'. $_ENV['fair_path'] .'/'. $file;
			put_file_into_browser( $file, "{$o->id}_{$field}.jpg" );
		}
		else
		{
			$file = SITEROOT .'/empty.gif';
			put_file_into_browser( $file, "empty.gif" );
		}
	}
	
	function download_my_photo()
	{
		$id = intval( $_REQUEST['id'] );
		$s = intval( $_REQUEST['s'] );
		$o = new FairObject( $id );
		if( $o->IsLoaded() && $this->FileIsMy($o) )
		{
			$field = $this->GetFieldBySize( $s );
			$file = $this->GetFilenameBySize( $o, $s );
			$file = SITEROOT .'/'. $_ENV['fair_path'] .'/'. $file;
			put_file_into_browser( $file, "{$o->id}_{$field}.jpg" );
		}
		else
		{
			$file = SITEROOT .'/empty.gif';
			put_file_into_browser( $file, "empty.gif" );
		}
	}
	
	function GetFieldBySize( $size )
	{
		$retval = "";
		
		switch( $size )
		{
			case 1:
				$retval = 'xs'; break;
			case 2:
				$retval = 's'; break;
			case 3:
				$retval = 'm'; break;
			case 4:
				$retval = 'l'; break;
			case 5:
				$retval = 'xl'; break;
			case 6:
				$retval = 'xxl'; break;
			default:
				$retval = 'preview'; break;
		}
		
		return( $retval );
	}
	
	function GetFilenameBySize( &$fair_object, $size )
	{
		$retval = "";
		
		if( $fair_object->IsLoaded() )
		{
			switch( $size )
			{
				case 1:
					$retval = $fair_object->xs; break;
				case 2:
					$retval = $fair_object->s; break;
				case 3:
					$retval = $fair_object->m; break;
				case 4:
					$retval = $fair_object->l; break;
				case 5:
					$retval = $fair_object->xl; break;
				case 6:
					$retval = $fair_object->xxl; break;
				default:
					$retval = $fair_object->preview; break;
			}
		}
		
		return( $retval );
	}
	
	function FileIsFree( &$fair_object, $size )
	{
		$retval = false;
		
		if( $fair_object->IsLoaded() )
		{
			switch( $size )
			{
				case 1:
					if( $fair_object->xs_price == 0 ) $retval = true; break;
				case 2:
					if( $fair_object->s_price == 0 ) $retval = true; break;
				case 3:
					if( $fair_object->m_price == 0 ) $retval = true; break;
				case 4:
					if( $fair_object->l_price == 0 ) $retval = true; break;
				case 5:
					if( $fair_object->xl_price == 0 ) $retval = true; break;
				case 6:
					if( $fair_object->xxl_price == 0 ) $retval = true; break;
				default:
					$retval = false; break;
			}
		}
		
		return( $retval );
	}
	
	function FileIsMy( &$fair_object )
	{
		$retval = false;
		
		if( $fair_object->IsLoaded() && $this->client->IsLoaded() )
		{
			if( $fair_object->client_id == $this->client->id )
			{
				$retval = true;
			}
		}
		
		return( $retval );
	}
	
	function zoom_preview()
	{
		$id = intval( $_REQUEST['id'] );
		$x = intval( $_REQUEST['x'] );
		$y = intval( $_REQUEST['y'] );
		
		$o = new FairObject( $id );
		
		if( $o->IsLoaded() )
		{
			$this->GetZoomedImage( SITEROOT .'/'. $_ENV['fair_path'] .'/'. $o->filename, SITEROOT .'/'. $_ENV['fair_path'] .'/'. $o->preview, $x, $y );
		}
	}

	function GetZoomedImage( $original, $thumbnail, $left, $top )
	{
		$o = imagecreatefromjpeg( $original );
		$t = imagecreatefromjpeg( $thumbnail );
		
		$o_w = imagesx( $o );
		$o_h = imagesy( $o );
		
		$t_w = imagesx( $t );
		$t_h = imagesy( $t );
		
		$zw = $o_w / $t_w;
		$zh = $o_h / $t_h;
		
		$x = round( $left * $zw );
		$y = round( $top * $zh );
		$w = $t_w * 3;
		$h = $t_h * 3;
		
		if( $x < 0 ) $x = 0;
		if( $y < 0 ) $y = 0;
		if( ($x+$w) > $o_w ) $x = $o_w - $w;
		if( ($y+$h) > $o_h ) $y = $o_h - $h;
		
		$new = imagecreatetruecolor( $w, $h );
		if( imagecopy( $new, $o, 0, 0, $x, $y, $w, $h ) )
		{
			$font = SITEROOT ."/lib/fonts/euro.ttf";
			$txt = "kollage";
			$textcolor = imagecolorallocate( $new, 250, 250, 250 );
			
			imagettftext( $new, 24, 0, 15*3, 30*3, $textcolor, $font, $txt );
			imagettftext( $new, 24, 0, 15*3, $h/2, $textcolor, $font, $txt );
			imagettftext( $new, 24, 0, 15*3, $h - 25*3, $textcolor, $font, $txt );
			
			imagettftext( $new, 24, 0, $w/2-20*3, 30*3, $textcolor, $font, $txt );
			imagettftext( $new, 24, 0, $w/2-20*3, $h/2, $textcolor, $font, $txt );
			imagettftext( $new, 24, 0, $w/2-20*3, $h - 25*3, $textcolor, $font, $txt );
			
			imagettftext( $new, 24, 0, $w-20*3-40*3, 30*3, $textcolor, $font, $txt );
			imagettftext( $new, 24, 0, $w-20*3-40*3, $h/2, $textcolor, $font, $txt );
			imagettftext( $new, 24, 0, $w-20*3-40*3, $h - 25*3, $textcolor, $font, $txt );
			
			imagejpeg( $new );
		}
	}
	
	function add_photos_form()
	{
		$this->method_name = 'add_photos_form';
		$JsHttpRequest = new JsHttpRequest("utf-8");
		
		$xml = "<data>";
		$xml .= "</data>";
		
		$this->display( 'add_photos_form', $xml );
	}
	
	function send_order_form( $result='' )
	{
		$this->method_name = 'send_order_form';
		
		$category_id = intval( $_REQUEST['category_id'] );
		$id = intval( $_REQUEST['id'] );
		
		//if( $category_id == 0 ) $category_id = $this->find_first_category();
		if( $id == 0 ) $id = $this->find_first_article( $category_id );
		$this->make_leftmenu( $category_id, $id );

		$code = new ProtectCode();
		$_SESSION['send_order_form_protect_code'] = $code->GetCode();
		
		$xml = "<data>";
		$xml .= "<result>{$result}</result>";
		$xml .= "<body><![CDATA[". $_REQUEST['object']['body'] ."]]></body>";
		$xml .= "</data>";
		
		$this->display( 'send_order_form', $xml );
	}
	
	function send_order()
	{
		$this->method_name = 'send_order';
		$result = 0;
		
		$category_id = intval( $_REQUEST['category_id'] );
		$id = intval( $_REQUEST['id'] );
		
		//if( $category_id == 0 ) $category_id = $this->find_first_category();
		if( $id == 0 ) $id = $this->find_first_article( $category_id );
		$this->make_leftmenu( $category_id, $id );
		
		if( $this->client->IsLoaded() )
		{
			if( $_SESSION['send_order_form_protect_code'] == $_REQUEST['protect_code'] && $_REQUEST['object']['body']!='' )
			{
				//send email to admin
				$mail = new AMail( 'windows-1251' );
					$subject = $this->dictionary->get("/locale/fair/send_order_subject") .' '. $this->client->firstname.' '.$this->client->lastname;
					$mail->subject = $subject;
					$mail->from_name = $this->client->firstname.' '.$this->client->lastname;
					$mail->from_email = $this->client->email;
					$mail->to_email = $settings->from_email;
					$body = $_REQUEST['object']['body'] .'<br><br>'. $this->client->email;
				$mail->Send( $body );
				//send email to admin
				
				$result = 1;
				$this->display( 'send_order_result', "<data></data>" );
				exit(0);
			}
			
			if( $_REQUEST['protect_code'] !="" && $_SESSION['send_order_form_protect_code'] != $_REQUEST['protect_code'] )
			{
				$result = 2;
			}
			
			$this->send_order_form( $result );
		}
		else
		{
			$this->show();
		}
	}
	
	function fair_protect_image()
	{
		if( $_SESSION['send_order_form_protect_code'] )
		{
			$obj = new ProtectImage( $_SESSION['send_order_form_protect_code'] );
			$obj->PrintImage();
		}
	}
	
	function slideshow()
	{
		global $db;
		
		$this->method_name = 'slideshow';
		$JsHttpRequest = new JsHttpRequest("utf-8");
		
		$GLOBALS['_RESULT'] = Array();
		$GLOBALS['_RESULT']['ids'] = Array();
		
		$category_id = intval( $_REQUEST['category_id'] );
		$str = urldecode( $_REQUEST['search_str'] );
		
		if( $category_id > 0 )
		{
			$db->query( "set @ids=''" );
			$db->query( "set max_sp_recursion_depth=1000" );
			$db->query( "call fair_categories({$category_id},@ids)" );
			$ids = $db->getone( "select @ids" );
			
			$sql = "select p.id from fair_objects p where p.status=1 and category_id1 in ({$ids}) order by id desc";
			$rs = $db->query( $sql );
			while( $row = $rs->fetch_row() )
			{
				array_push( $GLOBALS['_RESULT']['ids'], $row[0] );
			}
		}
		else if( $str != '' )
		{
			$str2 = split( " ", $str );
			$str2_search = "";
			$str2_search2 = "";
			$str2_delim = "(";
			foreach( $str2 as $v )
			{
				//$v = $stemmer->Get( $v );
				$str2_search .= $str2_delim ."d.value like '%{$v}%'";
				$str2_search2 .= $str2_delim ."dd.value like '%{$v}%'";
				$str2_delim = " and ";
			}
			$str2_search .= ")";
			$str2_search2 .= ")";
			$str_id = str_replace( "#", "", $str );
			
			$sql = "select distinct p.id from fair_objects p left join clients c on c.id=p.client_id left join dictionary d on d.parent_id=p.id left join dictionary dd on dd.parent_id=p.id where p.category_id1>0 and p.status=1 and d.folder='fair_objects' and d.lang='ru' and d.name='title' and dd.folder='fair_objects' and dd.lang='ru' and dd.name='description' and (p.id='{$str_id}' or {$str2_search} or {$str2_search2} or p.keywords like '%{$str}%' or c.firstname='{$str}' or c.lastname='{$str}') order by p.category_id1,p.id";
			//echo $sql;
			$rs = $db->query( $sql );
			while( $row = $rs->fetch_row() )
			{
				array_push( $GLOBALS['_RESULT']['ids'], $row[0] );
			}
		}
		
		$xml = "<data>";
		$xml .= "</data>";
		
		$this->display( 'slideshow', $xml );
	}
	
	function getphotoinfo()
	{
		global $db;
		
		$this->method_name = 'getphotoinfo';
		$JsHttpRequest = new JsHttpRequest("utf-8");
		
		$GLOBALS['_RESULT'] = Array();
		$id = intval( $_REQUEST['id'] );
		$o = new FairObject( $id );
		$GLOBALS['_RESULT']['id'] = $id;
		if( $o->IsLoaded() )
		{
			$GLOBALS['_RESULT']['owner'] = $o->owner->firstname .' '. $o->owner->lastname;
			$GLOBALS['_RESULT']['title'] = $o->title;
			$GLOBALS['_RESULT']['description'] = $o->description;
			$GLOBALS['_RESULT']['keywords'] = $o->keywords;
			$GLOBALS['_RESULT']['views'] = $o->views;
		}
		else
		{
			$GLOBALS['_RESULT']['client'] = '';
			$GLOBALS['_RESULT']['title'] = '';
			$GLOBALS['_RESULT']['description'] = '';
			$GLOBALS['_RESULT']['keywords'] = '';
			$GLOBALS['_RESULT']['views'] = '';
		}
	}
	
	function add_documents_form()
	{
		global $db;

		$this->method_name = 'add_documents_form';
		$JsHttpRequest = new JsHttpRequest("utf-8");
		
		if( $this->client->allow_fair == 1 )
		{
			$object_id = intval( $_REQUEST['object_id'] );
		
			$xml = "<data>";
			$xml .= "<fair_documents fair_object_id='{$object_id}'>";
			
			$sql = "select d.id,od.fair_object_id from fair_documents d left join fair_object_documents od on od.fair_document_id=d.id where d.client_id={$this->client->id}";
			$rs = $db->query( $sql );
			while( $row = $rs->fetch_row() )
			{
				$fair_object_id = intval( $row[1] );
				if( $fair_object_id > 0 ) $selected = 1; else $selected = 0;
				$o = new FairDocument( $row[0] );
				$xml .= "<item selected='{$selected}' fair_object_id='{$object_id}'>";
				$xml .= $o->Xml();
				$xml .= "</item>";
			}
			
			$xml .= "</fair_documents>";
			$xml .= "</data>";
		
			$this->display( 'add_documents_form', $xml );
		}
	}
	
	function add_document()
	{
		global $log, $db;
		
		set_time_limit(3600);
		header("Expires: 0");
		
		if( $this->client->allow_fair == 1 )
		{
			$JsHttpRequest = new JsHttpRequest("utf-8");
			
			$o = new FairDocument();
			$o->client_id = $this->client->id;
			$o->name = $_REQUEST['name'];
			$o->description = $_REQUEST['description'];
			$attachment = new Attachment( $_ENV['fair_documents'] );
			
			if( $attachment->upload('file') )
			{
				$o->filename = $attachment->filename;
				$o->Save();
			}
			
			$this->add_documents_form();
		}
	}
	
	function belong_document()
	{
		global $log, $db;
		
		if( $this->client->allow_fair == 1 )
		{
			$JsHttpRequest = new JsHttpRequest("utf-8");
			
			$object_id = intval( $_REQUEST['object_id'] );
			$document_id = intval( $_REQUEST['document_id'] );
			$flag = intval( $_REQUEST['flag'] );
			
			$o = new FairObject( $object_id );
			$d = new FairDocument( $document_id );
			
			if( $o->IsLoaded() && $d->IsLoaded() )
			{
				if( $flag == 1 )
				{
					$sql = "insert into fair_object_documents (fair_object_id,fair_document_id) values ({$object_id},{$document_id})";
					$db->query( $sql );
				}
				else
				{
					$sql = "delete from fair_object_documents where fair_object_id={$object_id} and fair_document_id={$document_id}";
					$db->query( $sql );
				}
			}
		}
	}
	
	function del_document()
	{
		global $log, $db;
		
		set_time_limit(3600);
		header("Expires: 0");
		
		if( $this->client->allow_fair == 1 )
		{
			$JsHttpRequest = new JsHttpRequest("utf-8");
			
			$id = intval( $_REQUEST['id'] );
			$o = new FairDocument( $id );
			$o->Delete();
			
			$this->add_documents_form();
		}
	}
	
	function my_purse()
	{
		if( $this->client->IsLoaded() )
		{
			$this->method_name = 'my_purse';
			
			$this->make_leftmenu();
			
			$xml = "<data>";
			$xml .= "<list>";
			
			$t = new MysqlTable( 'purse_operations' );
			parent::define_pages( $t->get_count("client_id={$this->client->id}") );
			$t->find_all( "client_id={$this->client->id}", 'id desc', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
			foreach( $t->data as $a )
			{
				$o = new PurseOperation( $a['id'] );
				$xml .= $o->Xml();
			}
			
			$xml .= "</list>";
			$xml .= "</data>";
			
			$this->display( 'my_purse', $xml );
		}
		else
		{
			$this->show();
		}
	}
}

?>
<?php

define('__ROOT__', dirname(dirname(dirname(__FILE__))) );
include( __ROOT__ .'/lib/LiqPay.php');

class api_v1_controller {
   const liqpay_public = 'i2842182438';
   const liqpay_private = 'CvZKy585e9RJOwwhm3lmSWjtpElCC1LiBTLrd1EeRP';
   const cn = 'api_v1_controller';
   const ERROR_TOKEN = -100;

   private function _formatPhone( $phone ) {
      global $log;
      $retval = null;
      $fn = self::cn .'._formatPhone';

      if( $phone ) {
         $phone = preg_replace( '@[\s\\\/=&\(\)\[\]\{\}%\^\+\-\._]+@', '', $phone );
         $t = new MysqlTable( 'countries' );
         if( $t->find_first( "'$phone' like  concat(phone_code,'%')" ) ) {
            $retval[0] = $t->data[0]['id'];
            $code = $t->data[0]['phone_code'];
            $pos = strpos( $phone, $code );
            if( $pos===0 ) $phone = substr( $phone, strlen($code) );
            $retval[1] = $phone;
         }
         else {
            $log->Write( "{$fn}: can't determine a country for phone $retval" );
         }
      }

      return( $retval );
   }
   
   private function _formatEmail( $email ) {
      if( $email ) {
         $email = trim( $email );
         $email = preg_replace( '@[\s\\\/=&\(\)\[\]\{\}%\+\^]+@', '', $email );
         $email = preg_replace( '/[\.]{2,}/', '.', $email );
      }
      
      return( $email );
   }
   
   private function _formatPrice( $price ) {
      return( sprintf( "%01.2f", $price ) );
   }

   private function generate_token() {
      return( md5(uniqid()) . md5(uniqid()) );
   }
   
   /**
    * [SendSMS description]
    * @param [type] $code    country phone code, from countries table
    * @param [type] $number  number without country code, 9-10 digits
    * @param string $message [description]
    */
   protected function _sendSMS_Atompark( $code, $number, $message='' ) {
      $fn = self::cn .'._sendSMS';
      global $log;
      $retval = null;
      $username = 'info@kollage.com.ua';
      //$password = 'Gtxfnmajnj';
      $password = 'gtxfnmajnj';
      
      $number = $code . $number;
      $log->Write( "{$fn} ({$number}) msg:{$message} sending is in process" );
      if( $number && strlen($number)>=11 ) {
         $src = '<?xml version="1.0" encoding="UTF-8"?>';
         $src .= '<SMS>';
         
         $src .= '<operations>';
         $src .= '<operation>SEND</operation>';
         $src .= '</operations>';
         
         $src .= '<authentification>';
         $src .= '<username>'. $username .'</username>';
         $src .= '<password>'. $password .'</password>';
         $src .= '</authentification>';
         
         $src .= '<message>';
         $src .= '<sender>Kollage</sender>';
         $src .= '<text>'. $message .'</text>';
         $src .= '</message>';
         
         $src .= '<numbers>';
         $src .= '<number messageID="msg11">'. $number .'</number>';
         $src .= '</numbers>';
         
         $src .= '</SMS>';
         
         $curl = curl_init();
         $curlOptions = array(   
            CURLOPT_URL=>'https://atompark.com/members/sms/xml.php',
            CURLOPT_SSL_VERIFYPEER=>false,
            CURLOPT_FOLLOWLOCATION=>false,
            CURLOPT_POST=>true,
            CURLOPT_HEADER=>false,
            CURLOPT_RETURNTRANSFER=>true,
            CURLOPT_CONNECTTIMEOUT=>15,
            CURLOPT_TIMEOUT=>100,
            CURLOPT_POSTFIELDS=>array('XML'=>$src)
         );  
         curl_setopt_array( $curl, $curlOptions ); 
         $result = curl_exec( $curl );
         if( $result === false ) {
            $log->Write( "{$fn} ({$number}) msg:{$message} Https request failed" );
         }
         else {
            $log->Write( "{$fn} ({$number}) msg:{$message} sending successfull" );
         }
         
         curl_close( $curl );
      }
      
      return( $retval );
   }

   protected function _sendSMS( $code, $number, $message='' ) {
      global $log;
      $fn = self::cn .'._sendSMS';
      $retval = null;

      $number = format_phone( $code, $number );
      $log->Write( "{$fn} ({$number}) msg:{$message} sending is in process" );
      $number = $code . $number;
      if( $number && strlen($number)>=11 ) {
         $curl = curl_init();
         $curlOptions = array(   
            CURLOPT_URL=>"https://alphasms.ua/api/http.php?version=http&key=a9d74b03a1f06ef5abb752c46e805c6422c07071&command=send&from=Kollage&to={$number}&message=". urlencode($message),
            CURLOPT_SSL_VERIFYPEER=>false,
            CURLOPT_FOLLOWLOCATION=>false,
            CURLOPT_POST=>true,
            CURLOPT_HEADER=>false,
            CURLOPT_RETURNTRANSFER=>true,
            CURLOPT_CONNECTTIMEOUT=>15,
            CURLOPT_TIMEOUT=>100,
         );  
         curl_setopt_array( $curl, $curlOptions ); 
         $result = curl_exec( $curl );
         if( $result === false ) {
            $log->Write( "{$fn} ({$number}) msg:{$message} Https request failed" );
         }
         else {
            $log->Write( "{$fn} ({$number}) msg:{$message} sending successfull" );
         }
         
         curl_close( $curl );
      }
      else {
         $log->Write( "${$fn} ({$number}) msg:{$message} not sent, number is wrong" );
      }
      
      return( $retval );
   }

   protected function _platform() {
      return $_REQUEST['platform'];
   }

   protected function _isAndroid($order) {
      $platform = $this->_platform();
      if ($order && $order->platform) $platform = $order->platform;
      return strtoupper($platform) == 'ANDROID';
   }

   /**
      result: 
         1  = success
         -1 = wrong parameters
         -3 = user isn't activated
         -4 = user was not found
   **/
   function login() {
      global $log;
      $fn = self::cn .'.login';

      $retval = new StdClass();
      $retval->result = 0;

      $phone = trim( $_REQUEST['phone'] );
      $password = trim( $_REQUEST['password'] );
      $activation_key = trim( $_REQUEST['activationCode'] );

      if( $phone!='' ) {
         $phone_params = $this->_formatPhone( $phone );

         if( $phone_params ) {
            $obj = new Client();

            if( $activation_key != '' ) {
               $obj->LoadByActivationKey( $activation_key );
               if( $obj->IsLoaded() && $obj->status==1 && $obj->phone==$phone_params[1] ) {
                  $obj->status = 2; //Client is active
                  if( $obj->Save() )
                  {
                     if( $obj->email ) {
                        //send email with congratulation
                        $settings = new Settings();
                        $mail = new AMail( 'windows-1251' );
                           $mail->subject = $settings->register_mail_subject;
                           $mail->from_name = $settings->from_name;
                           $mail->from_email = $settings->from_email;
                           //$mail->to_name = $obj->firstname ." ". $obj->lastname;
                           $mail->to_email = $obj->email;
                           $body = $settings->register_mail_body;
                           $body = str_replace( "{CLIENT}", $obj->firstname.' '.$obj->lastname, $body );
                        $mail->Send( $body );
                     }
                  }
               }
               else {
                  $retval->result = -2;
                  $log->Write( "{$fn}: activation code {$activation_key} not found for phone={$phone}" );
               }
            }
            else $obj->LoadByPhoneAndPassword( $phone_params[0], $phone_params[1], $password );

            if( $obj->IsLoaded() && $obj->status==2 ) {
               $retval->result = 1;
               $retval->token = $this->generate_token();

               $retval->user = new StdClass();
               $retval->user->id = $obj->id;
               $retval->user->firstname = $obj->firstname;
               $retval->user->lastname = $obj->lastname;
               $retval->user->email = $obj->email;
               $retval->user->phone = $obj->phone;
               $retval->user->photo_discount = $obj->photo_discount;
               $retval->user->type_id = $obj->type_id;

               $retval->sop = new StdClass();
               $retval->sop->dont_use_discount = 0;
               $sops = new SopSettings();
               if( $sops->IsLoaded() ) $retval->sop->dont_use_discount = $sops->dont_use_discount;

               $token = new Token();
               $token->token = $retval->token;
               $token->client_id = $obj->id;
               $token->Save();

               $log->Write( "{$fn}: user logged phone={$phone}" );
            }
            else if( !$obj->IsLoaded() && $retval->result==0 ) {
               $retval->result = -4;
               $log->Write( "{$fn}: user not found phone={$phone}" );
            }
            else if( $retval->result==0 ) {
               $retval->result = -3;
               $log->Write( "{$fn}: user is not activated email={$email} phone={$phone}" );
            }
         }
         else {
            $retval->result = -1;
            $log->Write( "{$fn}: wrong parameters $phone:$password request=". json_encode($_REQUEST) );
         }
      }
      else {
         $retval->result = -1;
         $log->Write( "{$fn}: wrong parameters $phone:$password request=". json_encode($_REQUEST) );
      }

      echo json_encode( $retval );
   }

   /**
      result: 
         1  = success
         -1 = wrong parameters
         -2 = token was not found
         
   **/
   function logout() {
      $retval = new StdClass();

      $t = trim( $_REQUEST['token'] );
      if( $t ) {
         $token = new Token();
         $token->LoadByToken( $t );
         if( $token->IsLoaded ) {
            $token->Delete();
            $retval->result = 1;
         }
         else {
            $retval->result = -2;
         }
      }
      else {
         $retval->result = -1;
      }

      echo json_encode( $retval );
   }

   /**
      result: 
          1  = success, found
         -1 = wrong parameters
         -2 = token was not found
         -3 = user isn't active
         
   **/
   private function _check_token( $token, &$client=null ) {
      $retval = new StdClass();

      $t = trim( $token );
      if( $t ) {
         $token = new Token();
         $token->LoadByToken( $t );

         if( $token->IsLoaded() ) {
            $retval->token = $t;

            $retval->user = new StdClass();
            $obj = new Client( $token->client_id );
            if( $obj->IsLoaded() && $obj->status==2 ) {
               $retval->result = 1;

               $retval->user->id = $obj->id;
               $retval->user->firstname = $obj->firstname;
               $retval->user->lastname = $obj->lastname;
               $retval->user->email = $obj->email;
               $retval->user->phone = $obj->phone;
               $retval->user->photo_discount = $obj->photo_discount;
               $retval->user->type_id = $obj->type_id;

               $client = $obj;
            }
            else {
               $retval->result = -3;

               $retval->user->id = 0;
               $retval->user->firstname = null;
               $retval->user->lastname = null;
               $retval->user->email = null;
               $retval->user->phone = null;
               $retval->user->photo_discount = null;
               $retval->user->type_id = null;
            }

            $retval->sop = new StdClass();
            $retval->sop->dont_use_discount = 0;
            $sops = new SopSettings();
            if( $sops->IsLoaded() ) $retval->sop->dont_use_discount = $sops->dont_use_discount;

            $token->updated_at = date( "Y-m-d H:i:s" );
            $token->Save();
         }
         else {
            $retval->result = -2;
         }
      }
      else {
         $retval->result = -1;
      }

      return( $retval );
   }

   function check_token() {
      global $log;
      $fn = self::cn .'.check_token';

      $retval = $this->_check_token( $_REQUEST['token'] );
      $log->Write( "{$fn}: retval=". json_encode($retval) );

      echo json_encode( $retval );
   }

   function captcha() {
      $code = new ProtectCode( 6, PC_NUMERIC );
      $_SESSION['register_protect_code'] = $code->GetCode();
      $retval = new StdClass();
      $retval->result = 1;
      $retval->phpsessid = session_id();

      echo json_encode($retval);
   }

   /**
    * Generates image with captcha
    *    PHPSESSID as "frontend" parameter should be passed here
    * @return img Image
    */
   function captcha_image() {
	    global $log;
        $code = new ProtectCode( 6, PC_NUMERIC );
        $_SESSION['register_protect_code'] = $code->GetCode();
		if( $_SESSION['register_protect_code'] )
		{
			$obj = new ProtectImage( $_SESSION['register_protect_code'] );
			$obj->PrintImage();
		}
   }

   /**
    * Registers the new user
    *    PHPSESSID as "frontend" parameter should be passed here
    * @return object Result of the registration
    */
   function register() {
      global $log;

      $fn = self::cn .'.register';

      $retval = new StdClass();
      $result = 0;

      $isProf = $_REQUEST['is_prof'];
      $firstname = trim( $_REQUEST['firstname'] );
      $lastname = trim( $_REQUEST['lastname'] );

      $phone = $_REQUEST['phone'];

      $country = new Country();
      $country->LoadByPhoneNumber( $phone );
      $country_id = $country->id;

      $phone_params = $this->_formatPhone( $phone ); //0-code, 1-number
      $phone = $phone_params[1];

      $email = $this->_formatEmail( $_REQUEST['email'] );
      $password = trim( $_REQUEST['password'] );
      $send_news = ( $_REQUEST['send_news'] == 'true' )? 1 : 0;
      
      if( $firstname!='' && $lastname!='' && $phone!='' && $password!='' ) {
         $obj = new Client();
         $obj->LoadByPhone( $country_id, $phone );
         if( !$obj->IsLoaded() && $email!='' ) {
            $obj->LoadByEmail( $email );
         }
         if( $obj->IsLoaded() ) {
            $result = -3; // client already exists
            $log->Write( "{$fn}: client already exists. phone={$phone}" );
         }
         else { //save client
            $log->Write( "{$fn}: register new client. phone={$phone} country_code={$country->phone_code}" );

            $code = new ProtectCode( 8, PC_NUMERIC );
            $obj->activation_key = strtoupper( $code->GetCode() );

            $obj->type_id = 1;
            $obj->firstname = $firstname;
            $obj->lastname = $lastname;
            $obj->country_id = $country_id;
            $obj->email = $email;
            $obj->phone = $phone;
            $obj->send_news = $send_news;
            $obj->password = $password; //??? md5???
            
            if( $obj->Save() )
            {
               //Send activation code by mobile
               $activation_key = sprintf( "%s %s %s %s", substr( $obj->activation_key, 0, 2 ), substr( $obj->activation_key, 2, 2 ), substr( $obj->activation_key, 4, 2 ), substr( $obj->activation_key, 6, 2 ) );
                $this->_sendSMS( $country->phone_code, $phone, "Kollage. Registraciya, kod aktivacii '{$activation_key}'. {$this->sms_suffix}" );
               $log->Write( "{$fn}: send sms 'Kod aktivacii {$activation_key}' email={$email} phone={$phone}" );

               $settings = new Settings();
               if( $isProf === 'true' ) {

                  $mail = new AMail( 'windows-1251' );

                  $subject = $settings->confirm_prof_subject;
                  $subject = str_replace( "{CLIENT}", $obj->firstname.' '.$obj->lastname, $subject );

                  $client_id = $obj->id;
                  $confirm_url = "http://kollage.com.ua/?base.confirm_prof&client_id=" . $client_id;

                  $body = $settings->confirm_prof_body;
                  $body = str_replace( "{CLIENT}", $obj->firstname.' '.$obj->lastname, $body );
                  $body = str_replace( "{EMAIL}", $obj->email, $body );
                  $body = str_replace( "{PHONE}", $phone, $body );
                  $body = str_replace( "{ACTIVATION_CODE}", $activation_key, $body );
                  $body = str_replace( "{URL}", $confirm_url, $body );

                  $mail->subject = $subject;
                  $mail->from_name = $settings->from_name;
                  $mail->from_email = $settings->from_email;
                  $mail->to_email = $this->admin_email;
                  $mail->Send( $body );
               }
               
               $_SESSION['register_protect_code'] = null;
               $retval->id = $obj->id;
               $result = 1;

               $log->Write( "{$fn}: cleared session variable, result={$result} id={$retval->id}" );
            }
            else // save error
            {
               $result = -4;
               $log->Write("{$fn}: SAVE ERROR. ". json_encode($obj) );
            }
         }
      }
      else {
         $result = -1; //parameters error
         $log->Write("{$fn}: PARAMETERS ERROR. '{$country->phone_code}' '". $_REQUEST['phone'] ."' '{$phone}'");
      }
      
      $retval->result = $result;
      echo json_encode( $retval );
   }

   function register_device() {
      global $db, $log;

      $fn = self::cn .'.register_device';

      $retval = new StdClass();
      $retval->items = Array();

      $token_result = $this->_check_token( $_REQUEST['token'] );
      if( $token_result->result == 1 ) {
         $google_cloud_token = $_REQUEST['google_token'];
         $os = $_REQUEST['os'];

         $token = new Token();
         $token->LoadByToken( $_REQUEST['token'] );
         if( $token->IsLoaded() ) {
            $token->google_cloud_token = $google_cloud_token;
            $token->os = $os;
            $token->Save();

            $log->Write("{$fn}: Registered. ". json_encode($token) );

            $retval->result = 1;
         }
         else {
            $retval->result = -1;
         }
      }
      else {
         $retval->result = self::ERROR_TOKEN;
      }

      echo json_encode($retval);
   }

   function reset_password_request() {
      global $log;

      $fn = self::cn .'.reset_password_request';

      $retval = new StdClass();

      $phone = $_REQUEST['phone'];
      if ($phone!='') {
         $phone_params = $this->_formatPhone($phone);

			$obj = new Client();
			$obj->LoadByPhone($phone_params[0], $phone_params[1]);
         if ($obj->IsLoaded()) {
            $country = new Country($obj->country_id);
            $code = new ProtectCode( 6, PC_NUMERIC );
            $obj->reset_password_key = $code->GetCode();
            if($obj->Save()) {
               $reset_password_key = sprintf( "%s %s %s", substr($obj->reset_password_key, 0, 2), substr( $obj->reset_password_key, 2, 2 ), substr( $obj->reset_password_key, 4, 2 ));
               $this->_sendSMS($country->phone_code, $phone_params[1], "Kollage. Kod smeny parolya '{$reset_password_key}'. {$this->sms_suffix}" );
              $log->Write( "{$fn}: send sms 'Kod smeny parolya {$reset_password_key}' email={$email} phone={$phone}" );
              $retval->result = 1;
            } else {
               $retval->result = -2;
               $log->Write( "{$fn}: Save code error, parameters $phone request=". json_encode($_REQUEST) );
            }
         } else {
            $retval->result = -1;
            $log->Write( "{$fn}: wrong parameters $phone request=". json_encode($_REQUEST) );   
         }
      } else {
         $retval->result = -1;
         $log->Write( "{$fn}: wrong parameters $phone request=". json_encode($_REQUEST) );
      }

      echo json_encode($retval);
   }

   function reset_password() {
      global $log;

      $fn = self::cn .'.reset_password';

      $retval = new StdClass();

      $phone = $_REQUEST['phone'];
      $reset_password_key = $_REQUEST['reset_password_key'];
      $password = $_REQUEST['password'];
      if ($phone !='' && $reset_password_key != '' && $password != '') {
         $phone_params = $this->_formatPhone($phone);

			$obj = new Client();
			$obj->LoadByResetPasswordKey($reset_password_key);
         if ($obj->IsLoaded() && $obj->phone === $phone_params[1]) {
            $obj->password = $password;
            if($obj->Save()) {
               $log->Write( "{$fn}: reseting password $phone request=". json_encode($_REQUEST) );
               $retval->result = 1;
            } else {
               $retval->result = -2;
               $log->Write( "{$fn}: Save password error, parameters $phone request=". json_encode($_REQUEST) );
            }
         } else {
            $retval->result = -1;
            $log->Write( "{$fn}: wrong parameters $phone request=". json_encode($_REQUEST) );
         }
      } else {
         $retval->result = -1;
         $log->Write( "{$fn}: wrong parameters $phone request=". json_encode($_REQUEST) );
      }

      echo json_encode($retval);
   }

   /* photo order */
   function sop_delivery() {
      global $db;

      $retval = new StdClass();
      $retval->items = Array();

      $token_result = $this->_check_token( $_REQUEST['token'] );
      if( $token_result->result == 1 ) {
         $t = new MysqlTable('sop_deliveries');
         $t->find_all( "status=1", "position,id" );
         foreach( $t->data as $row )
         {
            $o = new SOPDelivery( $row['id'] );
            $item = new StdClass();
            $item->id = $o->id;
            $item->name = $o->name;
            $item->price = $o->price;
            $item->order_prefix = $o->order_prefix;
            $item->position = $o->position;

            $item->payment_ways = Array();
            $sql = "select dp.paymentway_id from sop_deliveries_paymentways dp left join sop_paymentways pw on pw.id=dp.paymentway_id where dp.delivery_id={$item->id}". $this->_platformWhere('pw.')  ." order by pw.position,pw.id";
            $rs = $db->query( $sql );
            while( $row = $rs->fetch_row() )
            {
               $p = new SopPaymentway( $row[0] );
               $p_item = new StdClass();
               $p_item->id = $p->id;
               $p_item->name = $p->name;
               $p_item->description = $p->description;
               $p_item->code = $p->code;
               $p_item->discount = $p->discount;
               $p_item->position = $p->position;

               array_push( $item->payment_ways, $p_item );
            }
            
            array_push( $retval->items, $item );
         }

         $retval->result = 1;
      }
      else {
         $retval->result = self::ERROR_TOKEN;
      }

      echo json_encode($retval);
   }

   private function _platformWhere( $prefix='' ) {
      $platform = $this->_platform();
      $retval = '';
      if( $platform == 'android' ) $retval = " and {$prefix}allow_in_android=1";

      return $retval;
   }

   function sop_points() {
      $retval = new StdClass();
      $retval->items = Array();

      $token_result = $this->_check_token( $_REQUEST['token'] );
      if( $token_result->result == 1 ) {
         $t = new MysqlTable('sop_points');
         $t->find_all( "status=1". $this->_platformWhere(), "position,id" );
         foreach( $t->data as $row )
         {
            $o = new SOPPoint( $row['id'] );
            $item = new StdClass();
            $item->id = $o->id;
            $item->name = $o->name;
            $item->address = $o->address;
            $item->order_prefix = $o->order_prefix;
            $item->phone = $o->phone;
            $item->email = $o->email;
            $item->position = $o->position;
            
            array_push( $retval->items, $item );
         }

         $retval->result = 1;
      }
      else {
         $retval->result = self::ERROR_TOKEN;
      }

      echo json_encode($retval);
   }

   protected function _applyAndroidDiscount($price, $order = null) {
      // return $this->_isAndroid($order) ? $price * 0.6 : $price;
      return $price;
   }

   function sop_photo_sizes() {
      $retval = new StdClass();
      $retval->items = Array();

      $token_result = $this->_check_token( $_REQUEST['token'] );
      if( $token_result->result == 1 ) {
         $t = new MysqlTable('sop_order_photos_sizes');
         $t->find_all( "status=1". $this->_platformWhere(), "position,id" );
         foreach( $t->data as $row )
         {
            $o = new SOPOrderPhotoSize( $row['id'] );
            $item = new StdClass();
            $item->id = $o->id;
            $item->name = $o->name;
            // TODO: clear this discount
            $item->price = $this->_applyAndroidDiscount($o->price);
            $item->price1 = $this->_applyAndroidDiscount($o->price1);
            $item->price2 = $o->price2;
            $item->price3 = $o->price3;
            $item->price4 = $o->price4;
            $item->price5 = $o->price5;
            $item->weight = $o->weight;
            $item->position = $o->position;

            array_push( $retval->items, $item );
         }

         $retval->result = 1;
      }
      else {
         $retval->result = self::ERROR_TOKEN;
      }

      echo json_encode($retval);
   }

   /**
    * Creates the new order
    * @return [object] {result:[-1|1|-100], order_id:[number]}
    */
   function sop_order_register() {
      global $log;
      $fn = self::cn .'.sop_order_register';

      $retval = new StdClass();
      $retval->order_id = -1;
      $retval->result = -1;

      $token_result = $this->_check_token( $_REQUEST['token'] );
      if( $token_result->result == 1 ) {
         $photo_paper = trim( $_REQUEST['photo_paper'] );
         $border = intval( $_REQUEST['border'] );
         $delivery_id = trim( $_REQUEST['delivery_id'] );
         $point_id = trim( $_REQUEST['point_id'] );
         $paymentway_id = trim( $_REQUEST['paymentway_id'] );
         $address = trim( $_REQUEST['address'] );
         $comments = trim( $_REQUEST['comments'] );
         $platform = trim( $this->_platform() );

         if( $photo_paper && ($border==0 || $border==1) && $delivery_id && $paymentway_id ) {
            $order = new SOPOrder();
            $order->client_id = $token_result->user->id;
            $order->paymentway_id = $paymentway_id;
            $order->point_id = $point_id;
            $order->delivery_id = $delivery_id;
            $order->photo_paper = $photo_paper;
            $order->border = $border;
            $order->address = $address;
            $order->comments = $comments;
            $order->platform = $platform;
            $order_id = $order->Save();
            if( $order_id > 0 ) {
               $retval->order_id = $order_id;
               $retval->order_number = $order->order_number;
               $retval->result = 1;

               $log->Write( "{$fn}: order created id={$order_id}" );
            }
            else {
               $log->Write( "{$fn}: order wasnt created request=". json_encode($_REQUEST) );
            }
         }
         else {
            $log->Write( "{$fn}: wrong parameters request=". json_encode($_REQUEST) );
         }
      }
      else {
         $retval->result = self::ERROR_TOKEN;
      }

      echo json_encode($retval);
   }

   /**
    * Uploads the new photo and connect with existing order
    * @return [object] {result:[-1|1], order_photo_id:[number]}
    */
   function sop_order_upload_photo() {
      global $log;
      $fn = self::cn .'.sop_order_upload_photo';

      $retval = new StdClass();

      $token_result = $this->_check_token( $_REQUEST['token'] );
      $retval->result = -1;
      $retval->order_photo_id = -1;

      $order_id = intval( $_REQUEST['order_id'] );
      $size_id = intval( $_REQUEST['size_id'] );
      $quantity = intval( $_REQUEST['quantity'] );
      if( $token_result->result == 1 && $order_id > 0 ) {
         $retval->result = $token_result->result;
         $order = new SopOrder($order_id);
         if( $order->IsLoaded() ) {
            $order_photo_id = $order->UploadPhoto( 'file' );
            $size_o = new SOPOrderPhotoSize( $size_id );
            $op = new SOPOrderPhoto( $order_photo_id );
            if( $op->IsLoaded() ) {
               $op->quantity = $quantity;
               $op->size_id = $size_id;
               // $op->price = $size_o->GetPrice( $token_result->user->type_id );
               $op->price = $this->_applyAndroidDiscount(
                  $size_o->GetPrice($token_result->user->type_id),
                  $order
               );
               $op->Save();
               $retval->order_photo_id = $order_photo_id;
               $retval->result = 1;
            }
            else {
               $log->Write( "{$fn}: photo object isn't loaded order_id={$order_id} order_photo_id={$order_photo_id}" );
            }
         }
         else {
            $log->Write( "{$fn}: order isn't loaded={$order_id}" );
         }

         $log->Write( "{$fn} order_id={$order_id} order_photo_id={$order_photo_id}". json_encode($_FILES) );
      }
      else {
         $retval->result = self::ERROR_TOKEN;
         $log->Write( "{$fn}: wrong parameters token={$_REQUEST['token']} order_id={$order_id}" );
      }

      echo json_encode($retval);
   }

   function sop_order_save() {
      global $log;
      $fn = self::cn .'.sop_order_save';

      $retval = new StdClass();

      $order_id = intval( $_REQUEST['order_id'] );
      $token_result = $this->_check_token( $_REQUEST['token'] );
      if( $token_result->result == 1 && $order_id > 0 ) {
         $order = new SopOrder($order_id);
         if( $order->IsLoaded() ) {
            if( $order->status != 1 ) {
               $order->status = 1;
               $order->Save();

               api_v1_controller::sop_order_final_move_files( $order );

               api_v1_controller::sop_order_sendemail_to_client( $order );
               api_v1_controller::sop_order_sendemail_to_admin( $order );
               $log->Write( "{$fn}: order saved order_id={$order_id}" );
            }
            else {
               $log->Write( "{$fn}: order is saved already order_id={$order_id}" );
            }

            $retval->result = 1;
            $retval->order_number = $order->order_number;
            $retval->created_at = $order->created_at;
            $retval->updated_at = $order->updated_at;
         }
         else {
            $retval->result = -1;
            $log->Write( "{$fn}: order isn't loaded={$order_id}" );
         }
      }
      else if( $token_result->result != 1 ) {
         $retval->result = self::ERROR_TOKEN;
      }
      else {
         $retval->result = -1;
         $log->Write( "{$fn}: wrong parameters token={$_REQUEST['token']} order_id={$order_id}" );
      }

      echo json_encode($retval);  
   }

   /**
    * Moves uploaded order file in specific structure of the dirs
    * @param  [object] $order Order to do the process
    * @return [bool]        true|false
    */
   public static function sop_order_final_move_files( $order ) {
      $retval = false;
      if( $order->isLoaded() ) {
         $src_dir = $order->dir . $order->order_number .'/';
         $order->order_number = sprintf( "%s-%s-%s", $order->order_number, $order->delivery->order_prefix, $order->point->order_prefix );
         $border_prefix = '';
         if( $order->border==1 )
         {
            $border_prefix = '_frame';
            $order->order_number .= '-FRAME';
         }
         $order->Save();
         $md = new MysqlDateTime( $order->created_at );
         $dir = sprintf( "%s%s/%s", $order->dir, $md->GetFrontEndValue('y.m.d'), $order->order_number );
         mkdir_r( $dir, 0775 );
         $dir .= '/';
         $order->LoadPhotos();
         $i = 1;
         $files_success = 0;
         foreach( $order->photos as $p )
         {
            $new_dir = $dir . convert_encoding('utf-8','windows-1251',$p->size->name) .'/'. $order->GetPaperName(); mkdir_r( $new_dir );
            $new_dir .= '/';
            $new_name = sprintf( "%03d_%s_%s%s_%03d.%s", $i, convert_encoding('utf-8','windows-1251',$p->size->name), $order->GetPaperPrefix(), $border_prefix, $p->quantity, get_file_ext($p->filename) );
            if( copy( $src_dir.$p->filename, $new_dir.$new_name ) )
            {
               chmod( $new_dir.$new_name, 0664 );
               $files_success++;
            }
            $i++;
         }
         if( $files_success > 0 && $files_success == count($order->photos) )
         {
            rmdir_rf( $src_dir );
         }
      }
      return $retval;
   }

   public static function sop_order_sendemail_to_client( $order ) {
      if( $order->IsLoaded() ) {
         $client = new Client( $order->client_id );
         $settings = new Settings();
         $sop_settings = new SOPSettings();
         
         if( $client->email ) {
            $mail = new AMail( 'windows-1251' );
               $subject = $sop_settings->order_mail_subject;
               $subject = str_replace( "{ORDER_NR}", $order->order_number, $subject );
               $mail->subject = $subject;
               
               $mail->from_name = $settings->from_name;
               $mail->from_email = $settings->from_email;
               $mail->to_email = $client->email;
               $body = $sop_settings->order_mail_body;
               $body = str_replace( "{CLIENT}", $client->firstname.' '.$client->lastname, $body );
               $body = str_replace( "{ORDER_NR}", $order->order_number, $body );
               $body = str_replace( "{ORDER_STATUS}", api_v1_controller::sop_order_get_status_str( $order ), $body );
               $body = str_replace( "{ORDER}", api_v1_controller::sop_order_summary_table( $order ), $body );
            $mail->Send( $body );
         }
      }
   }

   public static function sop_order_sendemail_to_admin( $order ) {
      if( $order->IsLoaded() ) {
         $client = new Client( $order->client_id );
         $settings = new Settings();
         $sop_settings = new SOPSettings();
         
         if( $client->email ) {
            $mail = new AMail( 'windows-1251' );
               $subject = $sop_settings->admin_order_mail_subject;
               $subject = str_replace( "{ORDER_NR}", $order->order_number, $subject );
               $mail->subject = $subject;
               
               $mail->from_name = $settings->from_name;
               $mail->from_email = $settings->from_email;
               $mail->to_email = $sop_settings->admin_emails;
               $body = $sop_settings->admin_order_mail_body;
               $body = str_replace( "{CLIENT}", $client->firstname.' '.$client->lastname, $body );
               $body = str_replace( "{ORDER_NR}", $order->order_number, $body );
               $body = str_replace( "{ORDER_STATUS}", api_v1_controller::sop_order_get_status_str( $order ), $body );
               $body = str_replace( "{ORDER}", api_v1_controller::sop_order_summary_table( $order ), $body );
            $mail->Send( $body );
         }
      }
   }

   public static function sop_order_get_status_str( $order ) {
      $retval = '';
      
      if( $order->IsLoaded() )
      {
         $dpath = sprintf('%s/%s/%s.xml', SITEROOT, $_ENV['app_path'] .'/'. $_ENV['views_path'], $_SESSION['lang'] );
         $dictionary = new Dictionary( $dpath, 'utf-8' );
         switch( $order->status )
         {
            case 1:
               $retval = $dictionary->get('/locale/my/orders/photo/statuses/s1');
               break;
            case 2:
               $retval = $dictionary->get('/locale/my/orders/photo/statuses/s2');
               break;
            case 3:
               $retval = $dictionary->get('/locale/my/orders/photo/statuses/s3');
               break;
            case 4:
               $retval = $dictionary->get('/locale/my/orders/photo/statuses/s4');
               break;
            case 5:
               $retval = $dictionary->get('/locale/my/orders/photo/statuses/s5');
               break;
            case 6:
               $retval = $dictionary->get('/locale/my/orders/photo/statuses/s6');
               break;
         }
      }
      
      return( $retval );
   }

   public static function sop_order_summary_table( &$order )
   {
      $retval = '';
      
      if( $order->IsLoaded() )
      {
         $dpath = sprintf('%s/%s/%s.xml', SITEROOT, $_ENV['app_path'] .'/'. $_ENV['views_path'], $_SESSION['lang'] );
         $dictionary = new Dictionary( $dpath, 'utf-8' );

         $retval .= '<table style="border: 1px solid #e3bf85" cellspacing="3">';
         
         $retval .= '<tr>';
         $retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $dictionary->get('/locale/photos/order/order_number2') .'</b></td>';
         $retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->order_number .'</td>';
         $retval .= '</tr>';
         
         $retval .= '<tr>';
         $retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $dictionary->get('/locale/photos/order/created_at') .'</b></td>';
         $retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->created_at .'</td>';
         $retval .= '</tr>';
         
         $retval .= '<tr>';
         $retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $dictionary->get('/locale/photos/order/updated_at') .'</b></td>';
         $retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->updated_at .'</td>';
         $retval .= '</tr>';
         
         $retval .= '<tr>';
         $retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $dictionary->get('/locale/photos/order/paymentway_id') .'</b></td>';
         $retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->paymentway->name .'</td>';
         $retval .= '</tr>';
         
         $retval .= '<tr>';
         $retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $dictionary->get('/locale/photos/order/delivery_id') .'</b></td>';
         $retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->delivery->name .'</td>';
         $retval .= '</tr>';
         
         if( $order->delivery_id == 1 )
         {
            $retval .= '<tr>';
            $retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $dictionary->get('/locale/photos/order/point_name') .'</b></td>';
            $retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->point->name .', '. $order->point->address .','. $order->point->phone .'</td>';
            $retval .= '</tr>';
         }
         
         $retval .= '<tr>';
         $retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $dictionary->get('/locale/photos/order/comments') .'</b></td>';
         $retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;color:#d80c76">'. $order->comments .'</td>';
         $retval .= '</tr>';
         
         $retval .= '<tr>';
         $retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">&#160;</td>';
         $retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">&#160;</td>';
         $retval .= '</tr>';
         
         $retval .= '<tr>';
         $retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $dictionary->get('/locale/photos/order/client') .'</b></td>';
         $retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->client->firstname .'&#160;'. $order->client->lastname .'</td>';
         $retval .= '</tr>';
         
         $retval .= '<tr>';
         $retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $dictionary->get('/locale/photos/order/email') .'</b></td>';
         $retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->client->email .'</td>';
         $retval .= '</tr>';
         
         $retval .= '<tr>';
         $retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $dictionary->get('/locale/photos/order/address_v') .'</b></td>';
         $retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->client->city .', '. $order->client->street->name .', '. $order->client->address .'</td>';
         $retval .= '</tr>';
         
         $retval .= '<tr>';
         $retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $dictionary->get('/locale/photos/order/address2_v') .'</b></td>';
         $retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;color:#d80c76">'. $order->address .'</td>';
         $retval .= '</tr>';
         
         $retval .= '<tr>';
         $retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $dictionary->get('/locale/photos/order/phone') .'</b></td>';
         $retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->client->phone .'</td>';
         $retval .= '</tr>';
         
         $retval .= '<tr>';
         $retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">&#160;</td>';
         $retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">&#160;</td>';
         $retval .= '</tr>';
         
         $retval .= '<tr>';
         $retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $dictionary->get('/locale/photos/order/photos') .'</b></td>';
         $retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. api_v1_controller::sop_order_summary_table_photos( $order ) .'</td>';
         $retval .= '</tr>';
         
         $retval .= '<tr>';
         $retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $dictionary->get('/locale/photos/order/photo_paper') .'</b></td>';
         $photo_paper = ($order->photo_paper == 1)? $dictionary->get('/locale/photos/order/photo_paper1') : $dictionary->get('/locale/photos/order/photo_paper2');
         $retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $photo_paper .'</td>';
         $retval .= '</tr>';
         
         $retval .= '<tr>';
         $retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $dictionary->get('/locale/photos/order/border') .'</b></td>';
         $border = ($order->border == 1)? $dictionary->get('/locale/common/exist') : $dictionary->get('/locale/common/no');
         $retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $border .'</td>';
         $retval .= '</tr>';
         
         $retval .= '<tr>';
         $retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $dictionary->get('/locale/photos/order/total_photos') .'</b></td>';
         $retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. $order->photos_price_amount .' '. $dictionary->get('/locale/common/currency') .'</td>';
         $retval .= '</tr>';
         
         $retval .= '<tr>';
         $retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $dictionary->get('/locale/photos/order/total_delivery') .'</b></td>';
         $retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. sprintf( "%01.2f", $order->GetDeliveryPrice() ) .' '. $dictionary->get('/locale/common/currency') .'</td>';
         $retval .= '</tr>';
         
         $retval .= '<tr>';
         $retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $dictionary->get('/locale/photos/order/total_discount') .'</b></td>';
         $retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. sprintf( "%01.2f", $order->discount_price ) .' '. $dictionary->get('/locale/common/currency') .'</td>';
         $retval .= '</tr>';
         
         $retval .= '<tr>';
         $retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $dictionary->get('/locale/photos/order/total_price') .'</b></td>';
         $retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. sprintf( "%01.2f", $order->total_price ) .' '. $dictionary->get('/locale/common/currency') .'</td>';
         $retval .= '</tr>';
         
         $retval .= '<tr>';
         $retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;"><b>'. $dictionary->get('/locale/photos/order/payed_price') .'</b></td>';
         $retval .= '<td style="height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;">'. sprintf( "%01.2f", $order->payed ) .' '. $dictionary->get('/locale/common/currency') .'</td>';
         $retval .= '</tr>';
         
         $retval .= '</table>';
      }
      
      return( $retval );
   }

   function sop_order_summary_table_photos( &$order )
   {
      global $db;

      $retval = '';
      
      if( $order->IsLoaded() )
      {
         $dpath = sprintf('%s/%s/%s.xml', SITEROOT, $_ENV['app_path'] .'/'. $_ENV['views_path'], $_SESSION['lang'] );
         $dictionary = new Dictionary( $dpath, 'utf-8' );

         $photos = Array();
         $sql = "select sum(p.quantity),s.name,s.id from sop_order_photos p left join sop_order_photos_sizes s on s.id=p.size_id where p.order_id={$order->id} group by p.size_id order by s.position";
         $rs = $db->query( $sql );
         while( $row = $rs->fetch_row() )
         {
            $retval .= $row[1] .' - '. $row[0] .' '. $dictionary->get('/locale/common/items_amount') .'<br>';
         }
      }
      
      return( $retval );
   }

   /**
    * List of the photo sizes with common price
    * @return object List of the photo sizes with common price
    */
   function sop_order_photo_sizes() {
      $retval = new StdClass();
      $retval->items = Array();

      $t = new MysqlTable('sop_order_photos_sizes');
      $t->find_all( "status=1". $this->_platformWhere(), "position,id" );
      foreach( $t->data as $row )
      {
         $o = new SOPOrderPhotoSize( $row['id'] );
         $item = new StdClass();
         $item->id = $o->id;
         $item->name = $o->name;
         $item->price = $this->_applyAndroidDiscount($o->price1);
         $item->weight = $o->weight;
         $item->position = $o->position;

         array_push( $retval->items, $item );
      }
      
      $retval->result = 1;

      echo json_encode($retval);
   }

   function liqpay_settings() {
      $retval = new StdClass();

      $token_result = $this->_check_token( $_REQUEST['token'] );
      if( $token_result->result == 1 ) {
         $retval->public_key = self::liqpay_public;
         $retval->private_key = self::liqpay_private;
         $retval->result = 1;
      }
      else {
         $retval->result = self::ERROR_TOKEN;
      }

      echo json_encode($retval);
   }

   function sop_liqpay_return() {
      global $log;
      $fn = self::cn .'.sop_liqpay_return';

      $signature_received = $_REQUEST['signature'];
      $base64 = $_REQUEST['data'];
      $data = base64_decode( $base64 );
      $parameters = json_decode( $data );

      $signature_encoded = base64_encode( sha1( self::liqpay_private . $base64 . self::liqpay_private, 1 ) );

      if( $signature_received==$signature_encoded ) {
         $order_id = intval( $parameters->{'order_id'} );
         $order = new SopOrder( $order_id );
         if( $parameters->{'status'}=='success' ) {
            if( $order->IsLoaded() ) {
               $order->payed = $order->total_price;
               $order->liqpay_payment_id = $parameters->{'payment_id'};
               $order->liqpay_status = $parameters->{'status'};
               $order->Save();
               $log->Write( "{$fn}: order {$order_id}, set payed ${$order->payed}" );
            }
            else {
               $log->Write( "{$fn}: order {$order_id} hasnt loaded, status={$parameters->{'status'}}" );
            }
         }
         else {
            $log->Write( "{$fn}: order {$order_id} status={$parameters->{'status'}}" );
         }
      }
      else {
         $log->Write( "{$fn}: signatures arent equal" );
      }

      $log->Write( "{$fn}: {$data} {$signature_received} | {$signature_encoded}" );
   }

   function sop_order_list() {
      $retval = new StdClass();
      $retval->result = 1;
      $retval->list = array();

      $token_result = $this->_check_token( $_REQUEST['token'], $client );
      if( $token_result->result == 1 ) {

         $t = new MysqlTable('sop_orders');
         $t->find_all( "client_id={$client->id} and status>0 and status<5", "id desc" );
         foreach( $t->data as $row )
         {
            $o = new SopOrder( $row['id'] );

            $item = new StdClass();
            $item->id = $o->id;
            $item->order_number = substr( $o->order_number, 0, strpos($o->order_number, '-') );
            $item->address = $o->address;
            $item->comments = $o->comments;
            $item->point_name = $o->point->name;
            $item->point_address = $o->point->address;
            $item->paymentway_name = $o->paymentway->name;
            $item->delivery_name = $o->delivery->name;
            $item->payed = sprintf( "%01.2f", $o->payed );
            $item->photo_paper = $o->photo_paper;
            $item->border = $o->border;
            $item->status = $o->status;
            $item->photos_count = $o->photos_count;
            $item->delivery_price = $o->GetDeliveryPrice();
            $item->photos_price_amount = sprintf( "%01.2f", $o->photos_price_amount );
            $item->discount_price = sprintf( "%01.2f", $o->discount_price );
            $item->total_price = sprintf( "%01.2f", $o->total_price );
            $item->created_at = $o->created_at;
            $item->updated_at = $o->updated_at;

            $o = new SOPOrder($row['id']);
            array_push( $retval->list, $item );
         }
      }
      else {
         $retval->result = self::ERROR_TOKEN;
      }

      echo json_encode($retval);
   }
   /* photo order */
}

?>
<?php
/*****************************************************
Class v.1.0, 02.2017
Copyright Vitaliy Koshelenko vetal324@gmail.com
 ******************************************************/

class calculator_controller {

    function book_calculator_json(){

        $type_id = $_REQUEST['type_id'];
        $response = new StdClass();
        $response->bookTypes = array();

        $bookTypesTable = new MysqlTable('book_types');
        if ($type_id > 0) {
            $bookTypesTable->find_all('id='.$type_id, 'position,id');
        } else {
            $bookTypesTable->find_all('', 'position,id');
        }
        $bookTypes = $bookTypesTable->data;

        foreach($bookTypes as $bookTypeRow) {
            $id = $bookTypeRow['id'];
            $bookType = new BookType($id);
            $bookTypeJson = $bookType->Json();
            $response->bookTypes[$id] = $bookTypeJson;
        }

        echo json_encode($response);
    }

    function pb_calculator_json(){

        global $db;

        $response = new StdClass();

        //Type IDs
        $printBookId = 1;
        $photoBookId = 2;
        $polyBookId = 3;
        $pb_type_ids = array(
            $printBookId,
            $photoBookId,
            $polyBookId
        );

        //Type Names
        $pb_type_names = array();
        $pb_type_names[$printBookId] = 'Полиграфическая';
        $pb_type_names[$photoBookId] = 'Photobook';
        $pb_type_names[$polyBookId] = 'Polibook';

        //Type Images
        $pb_type_images = array();
        $pb_type_images[$printBookId] = 'http://www.printbooks.md/img/printbook/3.jpg';
        $pb_type_images[$photoBookId] = 'http://www.printbooks.md/img/photobook/3.jpg.pagespeed.ce.XLfEc1AxM3.jpg';
        $pb_type_images[$polyBookId] = 'http://online.printbooks.md/thumb/268x268//img/uploaded/products/108/2/3/0538337001472109036.jpg';

        $response->pbTypes = array();
        $pbSizesTable = new MysqlTable('pb2_sizes');
        foreach ($pb_type_ids as $pb_type_id) {
            $pbSizesTable->find_all('type='.$pb_type_id.' and status=1', 'position,id');
            $pbSizes = $pbSizesTable->data;

            $response->pbTypes[$pb_type_id] = new StdClass();
            $response->pbTypes[$pb_type_id]->id = $pb_type_id;
            $response->pbTypes[$pb_type_id]->name = $pb_type_names[$pb_type_id];
            $response->pbTypes[$pb_type_id]->image = $pb_type_images[$pb_type_id];
            $response->pbTypes[$pb_type_id]->formats = array();

            foreach ($pbSizes as $pbSizeRow) {
                $id = $pbSizeRow['id'];
                $pbSize = new PB2Size($id);
                $pbSizeJson = $pbSize->Json();
                $response->pbTypes[$pb_type_id]->formats[$id] = $pbSizeJson;
            }
        }

        //Currencies
        $response->currencies = array();
        $sql = "select * from currencies c inner join (select max(created_at) created_at,code from currencies group by code) a on a.created_at = c.created_at order by c.code desc;";
        $rs = $db->query( $sql );
        while( $row = $db->getrow($rs) ) {
            $currency_item = new Currency($row[0]);
            $currency_code = $currency_item->name;
            $currency_symbol = $currency_item->symbol;
            $currency_rate = $currency_item->exchange_rate;
            $response->currencies[$currency_code]->id = $currency_code;
            $response->currencies[$currency_code]->name = $currency_code;
            $response->currencies[$currency_code]->symbol = $currency_symbol;
            $response->currencies[$currency_code]->rate = $currency_rate;
        }

        echo json_encode($response);
    }

}

?>
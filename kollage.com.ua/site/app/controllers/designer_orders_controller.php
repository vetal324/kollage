<?php
define('__ROOT__', dirname(dirname(dirname(__FILE__))) );
include_once( 'base_controller.php' );

class designer_orders_controller extends base_controller
{
	var $from_email = 'info@kollage.com.ua';
	var $db_date_format = 'Y-m-d H:i';

	function checkup_orders() {
		global $log;
		$path = SITEROOT .'/'. $_ENV['designer_orders'];
		$dh = opendir( $path );
		if( $dh )
		{
			@chdir( $path );
			while( ($file = readdir($dh)) !== false )
			{
				$item = $path .'/'. $file;
				$order_txt = "{$path}/{$file}/order.html";
				if( $file !='.' && $file !='..' && is_dir($item) ) {
					$timestamp = fileatime($item);
					$created = gmdate($this->db_date_format, $timestamp);
					$log->Write( "designer_orders::checkup_orders: checking folder {$file} created {$created}" );
					$t = new DesignerTask();
					$t->LoadByFolder($file, $created);
					$readme = file_get_contents($order_txt);
					if ($readme) {
						if (!$t->loaded) {
							$t->Save(['folder' => $file, 'readme' => $readme, 'folder_created' => $created]);
							$log->Write( "designer_orders::checkup_orders: saved folder {$file} created {$created} to DB" );
							$this->send_email_to_designer($t, $readme, 'Order for designers: новый');
						} else if ($t->status === 1) { // new => accepted
							if (preg_match('/<li\s+id="status"\s*>\s*[^:]+:\s*в работе\s*[<(]+/i', $readme)) {
								$t->status = 2;
								$t->Save();
								$this->send_email_to_operator($t, $readme, 'Order for designers: принят дизайнером');
							}
						} else if ($t->status === 2) { // accepted => done
							if (preg_match('/<li\s+id="status"\s*>\s*[^:]+:\s*готов\s*[<(]+/i', $readme)) {
								$t->status = 3;
								$t->Save();
								$this->send_email_to_operator($t, $readme, 'Order for designers: готов');
							}
						}
					} else {
						$log->Write( "designer_orders::checkup_orders: folder {$path}, readme file read failed" );
					}
				}
			}
		} else {
			$log->Write( "designer_orders::checkup_orders: open folder {$path} failed" );
		}
		closedir($dh);
	}

	function send_email_to_designer($model, $readme, $subject) {
		global $log;
		$settings = new DesignerTasksSettings();
		// $designers = $settings->designer_emails;
		$designers = 'anebogin@gmail.com';

		$mail = new AMailSendGrid();
		$mail->subject = $subject;
		$mail->from_email = $this->from_email;
		$mail->to_email = $designers;
		$response = $mail->Send($this->apply_readme($readme, $model));
  		if($response) {
			$log->Write( "designer_orders::send_email_to_designer: {$model->folder} created {$model->created_at}, sent email to designers: {$subject}" );
			$model->email_sent_to_designer = gmdate($this->db_date_format);
			$model->Save();
		}
	}

	function send_email_to_operator($model, $readme, $subject) {
		global $log;
		$settings = new DesignerTasksSettings();
		// $operators = $settings->operator_emails;
		$operators = 'anebogin@gmail.com';

		$mail = new AMailSendGrid();
		$mail->subject = $subject;
		$mail->from_email = $this->from_email;
		$mail->to_email = $operators;
		$response = $mail->Send($this->apply_readme($readme, $model));
  		if($response) {
			$log->Write( "designer_orders::email_sent_to_operator: {$model->folder} created {$model->created_at}, sent email to designers: {$subject}" );
			$model->email_sent_to_operator = gmdate($this->db_date_format);
			$model->Save();
		}
	}

	function apply_readme($readme, $model) {
		$html = $readme;
		$html = str_replace('{folder}', $model->folder, $html);
		return $html;
	}
}

?>

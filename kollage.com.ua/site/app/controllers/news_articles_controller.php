<?php

include_once( 'base_controller.php' );

class news_articles_controller extends base_controller
{
	function show() {
		$this->method_name = 'show';

		$id = intval( $_REQUEST['id'] );
		
		$xml = "<data>";
			$article = new NewsArticle( $id );
			if( $article->status == 1 ) $xml .= $article->Xml(true);
		$xml .= "</data>";

		$this->display( 'show', $xml );
	}

	function index() {
		$this->method_name = 'list';
		
		$xml = "<data>";
			$t = new MysqlTable('news_articles');
			$t->find_all( "status=1", "publish_date desc, id desc" );
			foreach( $t->data as $row ) {
				$article = new NewsArticle( $row['id'] );
				$xml .= $article->Xml(true);
			}
		$xml .= "</data>";

		$this->display( 'list', $xml );
	}
}

?>
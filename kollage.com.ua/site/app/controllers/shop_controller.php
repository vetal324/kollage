<?php
/*****************************************************
 Class v.1.0, 2007
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

include_once( 'base_controller.php' );

class shop_controller extends base_controller
{
	var $debug = false;
	const liqpay_merchant_id = 'i2842182438';
	const liqpay_signature = 'CvZKy585e9RJOwwhm3lmSWjtpElCC1LiBTLrd1EeRP';
	
	function GetAdditionXml()
	{
		global $db;
		
		$o = new ShopSettings();
		$xml .= $o->Xml();
		
		$category_id = $db->getone( "select id from categories where folder='shop'" );
		$t = new MysqlTable('articles');
		$t->find_all( "category_id={$category_id} and folder='shop' and status=1 and show_in_menu=1", 'position,id' );
		$xml .= "<shop_articles>";
		foreach( $t->data as $a )
		{
			$article = new Article( $a['id'] );
			$xml .= $article->Xml();
		}
		$xml .= "</shop_articles>";
		
		return( $xml );
	}
	
	function show_article()
	{
		$this->method_name = 'show_article';
		
		$id = intval( $_REQUEST['id'] );
		$xml = "<data>";
		$article = new Article( $id ); if( $article->status == 1 ) $xml .= $article->Xml(true);
		$xml .= "</data>";
		$this->display( 'show_article', $xml );
	}
	
	function cline( $category_id, $append=null ) {
		global $db;
		$retval = '';
		
		if( $this->debug ) $d_time = microtime(true);
		
		if( $category_id > 0 ) {
			$cline_ids = $db->getone( "select shop_product_categories_line({$category_id})" );
			$cline_ids_a = explode( ",", $cline_ids );
			$cline_ids_a = array_reverse( $cline_ids_a );
			
			$xml = "<cline>";
			foreach( $cline_ids_a as $cline_item )
			{
				$cline_c = new ShopCategory( $cline_item );
				if( $cline_c->IsLoaded() ) $xml .= $cline_c->Xml();
			}
			
			if( is_array($append) ) {
				foreach( $append as $i ) {
					$xml .= "<item><name><![CDATA[{$i->name}]]></name></item>";
				}
			}
			
			$xml .= "</cline>";
			$retval = $xml;
		}
		
		if( $this->debug ) $this->log->Write( "shop.cline: prepare cline (". (microtime(true) - $d_time) ." sec)" );
		
		return( $retval );
	}
	
	function categories_from_root( $category_id ) {
		global $db;
		
		$ids = $category_id;
		
		if( $category_id > 0 ) {
			$db->query( "set @ids=''" );
			$db->query( "set max_sp_recursion_depth=10000" );
			$db->query( "call shop_product_categories({$category_id},@ids)" );
			$ids = $db->getone( "select @ids" );
		}
		
		return( $ids );
	}
	
	function show()
	{
		global $db;
		if( $this->debug ) $d_time_all = microtime(true);
		
		$category_id = intval( $_REQUEST['category_id'] );
		$ids = $this->categories_from_root( $category_id );
		
		$xml = "<data>";
		
		$xml .= $this->cline( $_REQUEST['category_id'] );
		
		//$sql = "select max(price),min(price),max(price_opt),min(price_opt),max(price_opt2),min(price_opt2) from shop_products where status=1 and category_id={$category_id}";
		$sql = "select max(price),min(price),max(price_opt),min(price_opt),max(price_opt2),min(price_opt2) from shop_products where status=1 and category_id IN ({$ids})";
		$rs = $db->query($sql);
		if( $rs && $row = $rs->fetch_row() ) {
			$xml .= "<price_max>". floatval($row[0]) ."</price_max>";
			$xml .= "<price_min>". floatval($row[1]) ."</price_min>";
			$xml .= "<price_opt_max>". floatval($row[2]) ."</price_opt_max>";
			$xml .= "<price_opt_min>". floatval($row[3]) ."</price_opt_min>";
			$xml .= "<price_opt2_max>". floatval($row[4]) ."</price_opt2_max>";
			$xml .= "<price_opt2_min>". floatval($row[5]) ."</price_opt2_min>";
		}
			
		$category = new ShopCategory( $category_id ); $xml .= $category->Xml();
		$xml .= $this->GetFeaturesXml( $category_id, $_REQUEST['brand'] );
		$xml .= $this->GetCategoryWithChildren( $category_id );
		$xml .= "</data>";
		
		if( $this->debug ) $this->log->Write( "shop.show: all processes (". (microtime(true) - $d_time_all) ." sec)" );
		$this->display( 'show', $xml );
	}
	
	function categories() {
		
		$shop_settings = new ShopSettings();
		
		$xml = "<data>";
		
		$xml .= $this->topmenu->get_item('shop')->xml();
		
		$xml .= "<category><page_title><![CDATA[". $shop_settings->page_title ."]]></page_title><page_keywords><![CDATA[". $shop_settings->page_keywords ."]]></page_keywords></category>";
		
		$xml .= "</data>";
		
		$this->display( 'categories', $xml );
	}
	
	function GetFeaturesXml( $category_id, $selected=Array() )
	{
		global $db, $log;
		
		$retval = "<features>";
		
		$ids = $this->categories_from_root( $category_id );
		
		$category = new ShopCategory( $category_id );
		$parent_category_id = $category->parent_id;
		$root_category_id = $db->getone( "select shop_product_root_category({$category_id})" );

		$sql = "SELECT DISTINCT f.id FROM shop_features f LEFT JOIN shop_feature_values v ON f.id=v.feature_id LEFT JOIN shop_product_features p ON v.id=p.feature_value_id LEFT JOIN shop_products pr ON p.product_id=pr.id WHERE p.id>0 AND pr.status=1 AND pr.id>0 AND f.status>0 AND v.status>0 AND pr.category_id={$category_id} GROUP BY f.id ORDER BY f.position,f.id";
		$rs = $db->query( $sql );
		if( $rs && $rs->num_rows == 0 ) {
			$sql = "SELECT DISTINCT f.id FROM shop_features f LEFT JOIN shop_feature_values v ON f.id=v.feature_id LEFT JOIN shop_product_features p ON v.id=p.feature_value_id LEFT JOIN shop_products pr ON p.product_id=pr.id WHERE p.id>0 AND pr.status=1 AND pr.id>0 AND f.status>0 AND v.status>0 AND pr.category_id={$parent_category_id} GROUP BY f.id ORDER BY f.position,f.id";
			$rs = $db->query( $sql );
		}
		if( $rs && $rs->num_rows == 0 ) {
			$sql = "SELECT DISTINCT f.id FROM shop_features f LEFT JOIN shop_feature_values v ON f.id=v.feature_id LEFT JOIN shop_product_features p ON v.id=p.feature_value_id LEFT JOIN shop_products pr ON p.product_id=pr.id WHERE p.id>0 AND pr.status=1 AND pr.id>0 AND f.status>0 AND v.status>0 AND pr.category_id={$root_category_id} GROUP BY f.id ORDER BY f.position,f.id";
			$rs = $db->query( $sql );
		}
		
		if( $rs ) {
			while( $row = $rs->fetch_row() )
			{
				$f = new ShopFeature( $row[0] );
				$retval .= "<feature id='{$f->id}' name='{$f->name}' show_in_filter='{$f->show_in_filter}' is_numeric='{$f->is_numeric}'>";
				$sql = "SELECT v.id,count(*) FROM shop_feature_values v LEFT JOIN shop_product_features p ON v.id=p.feature_value_id LEFT JOIN shop_products pr ON pr.id=p.product_id WHERE v.feature_id={$row[0]} AND v.status>0 AND p.product_id>0 AND pr.status=1 AND pr.category_id IN ({$ids}) GROUP BY v.id ORDER BY v.position,v.id";
				$rs2 = $db->query( $sql );
				while( $row2 = $rs2->fetch_row() )
				{
					$v = new ShopFeatureValue( $row2[0] );
					if( count($selected) == 0 ) $s = 'yes';
					else
					{
						$s = 'no';
						foreach( $selected as $si )
						{
							if( $si == $v->id )
							{
								$s = 'yes'; break;
							}
						}
					}
					$retval .= "<value id='{$v->id}' name='{$v->name}' selected='{$s}' c='{$row2[1]}'/>";
				}
				$retval .= "</feature>";
			}
		}
		
		$retval .= "</features>";
		
		return( $retval );
	}
	
	function GetBrandsXml2( $category_id, $selected=Array() )
	{
		global $db;
		
		$retval = "<brands>";

		$sql = "select distinct p.brand_id,p.position, b.position from shop_products p left join shop_brands b on p.brand_id=b.id where p.category_id in ({$category_id}) order by b.position";
		$rs = $db->query( $sql );
		if( $rs ) {
			while( $row = $rs->fetch_row() )
			{
				$o = new ShopBrand( $row[0] );
				$count_products = intval( $db->getone("select count(id) from shop_products where status=1 and brand_id={$o->id} and category_id in ({$category_id})") );
				if( count($selected) == 0 ) $s = 'no';
				else
				{
					$s = 'no';
					foreach( $selected as $si )
					{
						if( $si == $o->id )
						{
							$s = 'yes'; break;
						}
					}
				}
				$retval .= "<brand id='{$o->id}' selected='{$s}' count_products='{$count_products}'><name><![CDATA[{$o->name}]]></name></brand>";
			}
		}
		
		$retval .= "</brands>";
		
		return( $retval );
	}
	
	function GetBrandsXml( $category_id, $selected=Array() )
	{
		global $db;

		$retval = "<brands>";

		$sql = "select distinct p.brand_id,p.position from shop_products p left join shop_brands b on p.brand_id=b.id where p.category_id={$category_id} order by b.position";
		$rs = $db->query( $sql );
		while( $row = $rs->fetch_row() )
		{
			$o = new ShopBrand( $row[0] );
			if( count($selected) == 0 ) $s = 'yes';
			else
			{
				$s = 'no';
				foreach( $selected as $si )
				{
					if( $si == $o->id )
					{
						$s = 'yes'; break;
					}
				}
			}
			$retval .= "<brand id='{$o->id}' selected='{$s}'><name><![CDATA[{$o->name}]]></name></brand>";
		}
		
		$retval .= "</brands>";
		
		return( $retval );
	}
	
	function product()
	{
		global $db;
		
		$this->method_name = 'product';
		
		$back_page = intval($_REQUEST['back_page']);
		if( $back_page == 0 ) $back_page = 1;
		
		$o = new ShopProduct( $_REQUEST['id'] );
		
		if( $o->IsLoaded() )
		{
			$c = new ShopCategory( $o->category_id );
			
			$xml = "<data>";
			
			$pClass = new StdClass();
			$pClass->name = $o->name;
			$xml .= $this->cline( $o->category_id, Array( $pClass ) );
			
			$xml .= $o->Xml();
			$xml .= $c->Xml();
			
			if( $this->IsProductInComparision($o->id) ) {
				$xml .= "<is_product_in_comparision>1</is_product_in_comparision>";
			}
			else {
				$xml .= "<is_product_in_comparision>0</is_product_in_comparision>";
			}
			
			$xml .= "</data>";
			$this->display( 'product', $xml );
		}
		else $this->show();
	}
	
	function IsProductInComparision( $id ) {
		$retval = false;
		
		$sc = new ShopComparision();
		$sc->LoadByKeyAndProductId( $this->getClientKey(), $id );
		if( $sc->IsLoaded() ) {
			$retval = true;
		}
		
		return( $retval );
	}
	
	function GetCategoryWithChildren( $category_id, $tag='categories' ) {
		$retval = "<{$tag}>";
		
		if( $category_id > 0 ) {
			$category = new ShopCategory( $category_id );
			if( $category->IsLoaded() ) {
				$t = new MysqlTable('shop_product_categories');
				$t->find_all( "folder='shop' and parent_id={$category_id} and show_in_menu=1", "position,id" );
				foreach( $t->data as $row ) {
					$o = new ShopCategory( $row['id'] );
					$retval .= "<category>";
					$retval .= "<id>{$o->id}</id>";
					$retval .= "<name><![CDATA[{$o->name}]]></name>";
					$retval .= "</category>";
				}
			}
		}
		
		$retval .= "</{$tag}>";
		return( $retval );
	}
	
	function json_order() {
		global $db, $log;
		
		$status = 0;
		$message = '';
		
		$name = trim( $_REQUEST['name'] );
		$country_id = intval( $_REQUEST['country_id'] );
		$protect_code = trim( $_REQUEST['protect_code'] );
		$comments = trim( $_REQUEST['comments'] );

		$country = new Country( $country_id );

		$phone = $this->FormatPhone( $country->phone_code, $_REQUEST['phone'] );
		$email = $this->FormatEmail( $_REQUEST['email'] );
		
		if( !$name ) {
			$status = -2; $message = $this->dictionary->get('/locale/shop/order/errors/name');
		}
		else if( !$phone ) {
			$status = -1; $message = $this->dictionary->get('/locale/shop/order/errors/phone');
		}
		else if( !$this->client->IsLoaded() && $protect_code!=$_SESSION['order_protect_code'] ) {
			$status = -3; $message = $this->dictionary->get('/locale/shop/order/errors/protect_code');
		}
		else {
			$order = new ShopOrder();
			$on = new OrderNumber(1);
			$order->order_number = $on->GetNext();
			$order->name = $name;
			$order->phone = $phone;
			$order->email = $email;
			$order->comments = $comments;
			if( $this->client->IsLoaded() ) $order->client_id = $this->client->id;
			else $order->client_id = 0;
			
			if( $order->Save() ) {
				$client_key = $this->getClientKey();
				
				if( $client_key != '' ) {
					$log->Write( "json_order order={$order->order_number} client={$order->client_id} client_key={$client_key}" );
					$trash = new MysqlTable( 'shop_trash' );
					$trash->find_all( "client_key='{$client_key}'", 'id' );
					foreach( $trash->data as $r )
					{
						$t = new ShopTrash( $r['id'] );
						$product = new ShopProduct( $t->product_id, false );
						$sql = "insert into shop_order_products (order_id,product_id,product_name,price,quantity,created_at) values ({$order->id},{$t->product_id},'{$product->name}',". $product->GetPriceByClient( $this->client ) * $this->currency->exchange_rate .",{$t->quantity},now())";
						$db->query( $sql );
					}
					
					$order->status = 1;
					$order->Save();
					$this->save_order( $order, 1, $name, $phone, $email );
					$this->ClearTrash();
					
					$status = 1;
				}
				else {
					$status = -5; $message = 'Trash init error';
				}
			}
			else {
				$status = -4; $message = 'Order saving error';
			}
		}
		
		$log->Write( "json_order status={$status}" );
		
		$retval = new StdClass();
		$retval->result = $status;
		$retval->message = $message;
		echo json_encode( $retval );
	}
	
	private function ClearTrash() {
		$client_key = $this->getClientKey();
		
		if( $client_key != '' ) {
			$trash = new MysqlTable( 'shop_trash' );
			$trash->find_all( "client_key='{$client_key}'", 'id' );
			foreach( $trash->data as $r )
			{
				$t = new ShopTrash( $r['id'] );
				$t->Delete();
			}
		}
	}
	
	//OLD
	function order2()
	{
		global $db;

		$this->method_name = 'order2';
		
		$order = new ShopOrder( intval($_REQUEST['object']['id']), true );
		$_REQUEST['object']['client_id'] = $this->client->id;
		if( !$order->IsLoaded() )
		{
			$on = new OrderNumber(1);
			$_REQUEST['object']['order_number'] = $on->GetNext();
		}
		if( count($this->shop_trash->items) > 0 && $order->Save( $_REQUEST['object'] ) )
		{
			foreach( $this->shop_trash->items as $k=>$v )
			{
				$p = new ShopProduct( $k );
				$sql = "insert into shop_order_products (order_id,product_id,price,quantity,created_at) values ({$order->id},{$k},". $this->GetPrice($p) .",{$v},now())";
				$db->query( $sql );
			}
			$order->Load( $order->id );
			$amount = $order->amount + $order->delivery->price;
			
			$xml = "<data>";
			$xml .= $order->Xml();
			$xml .= $this->AdditionalData();
			//Payment ways
			switch( $order->paymentway->code )
			{
				case 4:
					//��.�������
					if( $this->client->purse >= $order->amount + $order->delivery->price )
					{
						$this->pay_with_purse( $order );
					}
					else
					{
						$xml .= "<msg>". $this->dictionary->get('/locale/photos/order/purse_limit') ."</msg>";
					}
					break;
				case 5:
					//�����.�����
					$this->pay_with_card( $order );
					break;
				case 8:
					//�����.����� Privat (liqpay.com)
					$this->pay_with_card_privat( $order );
					break;
				default:
					$order->status = 1;
					$order->Save();
					$this->save_order( $order );
					break;
			}
			
			$xml .= "</data>";
			$this->display( 'order', $xml );
		}
		else
		{
			//$this->order();
			$this->show();
		}
	}
	
	//OLD
	function order3( &$order )
	{
		$this->method_name = 'order3';
		
		if( $order->IsLoaded() )
		{
			$order->paymentway->description = str_replace( "{ORDER_NUMBER}", $order->order_number, $order->paymentway->description );
			$order->paymentway->description = str_replace( "{SUM}", sprintf( "%01.2f %s", ($order->amount + $order->delivery->price) * $this->currency->exchange_rate, $this->currency->symbol ), $order->paymentway->description );
			$xml = "<data>";
			$xml .= $order->Xml();
			$xml .= "</data>";
			$this->display( 'order3', $xml );
		}
		else $this->show();
	}
	
	function GetPrice( &$product )
	{
		$retval = 0;
		
		if( $product->IsLoaded() )
		{
			if( $this->client->IsLoaded() )
			{
				switch( $this->client->type_id )
				{
					case 2:
						$retval = $product->GetPriceOpt();
						break;
					case 3:
						$retval = $product->GetPriceOpt2();
						break;
					default:
						$retval = $product->GetPrice();
				}
			}
			else
			{
				$retval = $product->GetPrice();
			}
		}
		
		return( $retval );
	}
	
	function AdditionalData()
	{
		$xml = "";
		
		$t = new MysqlTable('shop_deliveries');
		$t->find_all( "status=1", "position,id" );
		$xml .= "<deliveries>";
		$d_js = ""; $d_delim = '';
		foreach( $t->data as $row )
		{
			$o = new ShopDelivery( $row['id'] );
			$xml .= $o->Xml();
			$d_js .= $d_delim ."{$o->id}:[{$o->price},'{$o->name}']"; $d_delim = ',';
		}
		$xml .= "</deliveries>";
		$xml .= "<deliveries_js><![CDATA[{$d_js}]]></deliveries_js>";
		
		$t = new MysqlTable('shop_paymentways');
		$t->find_all( "status=1", "position,id" );
		$xml .= "<paymentways>";
		foreach( $t->data as $row )
		{
			$o = new ShopPaymentway( $row['id'] );
			$xml .= $o->Xml();
		}
		$xml .= "</paymentways>";
		
		return( $xml );
	}
	
	function FeaturesXml( $category_id, $id )
	{
		global $db;
		
		$category_id = $db->getone( "select shop_product_root_category({$category_id})" );
		
		$xml = "";
		$t = new MysqlTable('shop_features');
		$t->find_all( "category_id={$category_id} and status=1 and (match_analog=0 or match_analog is null)", "position,id" );
		$xml .= "<features>";
		foreach( $t->data as $row )
		{
			$o = new ShopFeature( $row['id'] );
			$xml .= $o->Xml();
		}
		$xml .= "</features>";
		
		return( $xml );
	}
	
	function FeaturesXml2( &$product )
	{
		$xml = "";
		
		$xml .= "<features>";
		if( $product->IsLoaded() )
		{
			foreach( $product->features as $f )
			{
				$xml .= $f->Xml();
			}
		}
		$xml .= "</features>";
		
		return( $xml );
	}
	
	function GetBrandsJS( $category_id, $id, $showed_ids='' )
	{
		global $db;
		
		$retval = '';
		
		$db->query( "set @ids=''" );
		$db->query( "set max_sp_recursion_depth=10000" );
		$db->query( "call shop_product_categories({$category_id},@ids)" );
		$ids = $db->getone( "select @ids" );
		
		if( $showed_ids )
		{
			$ids_sql = " and p.id not in ({$showed_ids})";
		}
		
		$sql = "select distinct b.id, p.position from shop_brands b left join shop_products p on b.id=p.brand_id where p.id is not null and b.status=1 and p.status=1 and p.category_id in ({$ids}) order by b.position,b.id";
		$rs = $db->query( $sql );
		if( $rs ) {
			$delim = '';
			while( $row = $rs->fetch_row() )
			{
				$o = new ShopBrand( $row[0] );
				$retval .= $delim .  $o->id.":['{$o->name}',{";
				
				$sql = "select p.id, p.price from shop_brands b left join shop_products p on b.id=p.brand_id where p.id is not null and p.id<>{$id} {$ids_sql} and b.status=1 and p.status=1 and p.brand_id={$o->id} and p.category_id in ({$ids}) order by p.price desc,p.id";
				$rs2 = $db->query( $sql );
				$delim2 = '';
				while( $row2 = $rs2->fetch_row() )
				{
					$p = new ShopProduct( $row2[0], false );
					$retval .= $delim2 . "{$p->id}:['{$p->name}','". $this->GetPrice( $p )*$this->currency->exchange_rate ."','". $this->currency->symbol."']";
					$delim2 = ',';
				}
				
				$retval .= '}]';
				$delim = ',';
			}
		}
		
		return( $retval );
	}
	
	function GetAnalogProductsJS( $category_id, $id, $showed_ids='' )
	{
		global $db;
		
		$retval = '';
		
		$db->query( "set @ids=''" );
		$db->query( "set max_sp_recursion_depth=10000" );
		$db->query( "call shop_product_categories({$category_id},@ids)" );
		$ids = $db->getone( "select @ids" );
		
		if( $showed_ids )
		{
			$ids_sql = " and p.id not in ({$showed_ids})";
		}
		
		//$sql = "select distinct p.id from shop_products p where p.id<>{$id} {$ids_sql} and p.status=1 and p.category_id in ({$ids}) and (select count(*) from shop_product_features pf1 left join shop_feature_values fv1 on fv1.id=pf1.feature_value_id left join shop_features f1 on f1.id=fv1.feature_id where f1.match_analog=1 and pf1.product_id=p.id and pf1.feature_value_id in (select feature_value_id from shop_product_features where product_id={$id}))=(select count(*) from shop_product_features pf2 left join shop_feature_values fv2 on fv2.id=pf2.feature_value_id left join shop_features f2 on f2.id=fv2.feature_id where f2.match_analog=1 and pf2.product_id={$id}) order by p.brand_id,p.price desc";
		$sql = "select distinct p.id, d.value, p.price from shop_products p left join shop_brands b on b.id=p.brand_id left join dictionary d on d.parent_id=b.id where p.id<>{$id} and d.folder='shop_brands' and lang='{$_SESSION['lang']}' and d.name='name' {$ids_sql} and p.status=1 and p.category_id in ({$ids}) and (select count(*) from shop_product_features pf1 left join shop_feature_values fv1 on fv1.id=pf1.feature_value_id left join shop_features f1 on f1.id=fv1.feature_id where f1.match_analog=1 and pf1.product_id=p.id and pf1.feature_value_id in (select feature_value_id from shop_product_features where product_id={$id}))=(select count(*) from shop_product_features pf2 left join shop_feature_values fv2 on fv2.id=pf2.feature_value_id left join shop_features f2 on f2.id=fv2.feature_id where f2.match_analog=1 and pf2.product_id={$id}) order by d.value,p.price desc";
		//echo $sql;
		$rs2 = $db->query( $sql );
		if( $rs2 ) {
			$delim2 = '';
			while( $row2 = $rs2->fetch_row() )
			{
				$p = new ShopProduct( $row2[0], false );
				$retval .= $delim2 . "{$p->id}:['{$p->name}','". $this->GetPrice( $p )*$this->currency->exchange_rate ."','". $this->currency->symbol."']";
				$delim2 = ',';
			}
		}
		
		return( $retval );
	}
	
	function pay_with_card( &$order )
	{
		$host = $_SERVER['HTTP_HOST']; if( !$host ) $host = 'kollage.com.ua';
		$this->upc_pay( $order->order_number, $order->amount + $order->delivery->price, "shop|".$host );
		exit(0);
	}
	
	function pay_with_card_privat( &$order )
	{
		$host = $_SERVER['HTTP_HOST']; if( !$host ) $host = 'kollage.com.ua';
		$this->privat_pay( $order->order_number, $order->amount + $order->delivery->price, "shop|".$host );
		exit(0);
	}
	
	function pay_with_purse( &$order )
	{
		if( $this->client->purse >= $order->amount + $order->delivery->price )
		{
			$o = new PurseOperation();
			$o->client_id = $this->client->id;
			$cur = new Currency(); $cur->LoadByCode('980');
			$o->amount = -1 * ($order->amount + $order->delivery->price) * $cur->exchange_rate;
			$o->description = sprintf( "%s%s", $this->dictionary->get('/locale/shop/order/purse_description'), $order->order_number );
			$o->Save();
			$this->client->Load( $this->client->id );
			$order->payed = $order->amount + $order->delivery->price;
			$order->status = 1;
			$order->Save();
			$this->save_order( $order );
		}
		exit(0);
	}
	
	function save_order( &$order, $status=1, $name='', $phone='', $email='' )
	{
		global $log;
		$this->method_name = 'save_order';
		
		if( $this->client->type_id == 1 ) //�������, �� ���������� � �������
		{
			$this->currency->LoadByCode('980');
		}
		
		if( $this->client->IsLoaded() ) {
			if( !$name ) $name = $this->client->firstname.' '.$this->client->lastname;
			if( !$phone ) $phone = $this->client->phone;
			if( !$email ) $email = $this->client->email;
		}
		
		//Send email to admin and client
		$settings = new Settings();
		$shop_settings = new ShopSettings();
		if( $email ) {
			$mail = new AMail( 'windows-1251' );
				$subject = $shop_settings->order_mail_subject;
				$subject = str_replace( "{ORDER_NR}", $order->order_number, $subject );
				$mail->subject = $subject;
				
				$mail->from_name = $settings->from_name;
				$mail->from_email = $settings->from_email;
				$mail->to_email = $email;
				$body = $shop_settings->order_mail_body;
				$body = str_replace( "{CLIENT}", $name, $body );
				$body = str_replace( "{ORDER_NR}", $order->order_number, $body );
				$body = str_replace( "{COMMENTS}", $order->comments, $body );
				$body = str_replace( "{ORDER_STATUS}", $this->GetOrderStatusStr( $order ), $body );
				$body = str_replace( "{ORDER}", $this->GetOrderSummaryTableForClient( $order ), $body );
			$mail->Send( $body );
		}
		if( $phone ) {
			//send SMS
			$this->SendSMS( $this->client->country->phone_code, $phone, "Kollage. Zakaz prinyat '{$order->order_number}'. {$this->sms_suffix}" );
		}
		
		$mail = new AMail( 'windows-1251' );
			$subject = $shop_settings->admin_order_mail_subject;
			$subject = str_replace( "{ORDER_NR}", $order->order_number, $subject );
			$mail->subject = $subject;
			
			$mail->from_name = $settings->from_name;
			$mail->from_email = $settings->from_email;
			$mail->to_email = $shop_settings->admin_emails;
			$body = $shop_settings->admin_order_mail_body;
			$body = str_replace( "{CLIENT}", $name, $body );
			$body = str_replace( "{PHONE}", $phone, $body );
			$body = str_replace( "{EMAIL}", $email, $body );
			$body = str_replace( "{ORDER_NR}", $order->order_number, $body );
			$body = str_replace( "{COMMENTS}", $order->comments, $body );
			$body = str_replace( "{ORDER_STATUS}", $this->GetOrderStatusStr( $order ), $body );
			$body = str_replace( "{ORDER}", $this->GetOrderSummaryTableForAdmin( $order ), $body );
		$mail->Send( $body );
		//Send email to admin and client
				
		$this->shop_trash = null;
		$this->store();
		$this->restore();
		//$this->order3( $order );
	}
	
	function upc_pay( $order_number, $total_amount, $action='sop|kollage.com.ua' )
	{
		global $db;
		
		//$uah = new Currency( $db->getone("select id from currencies where code='980'") );
		$uah = new Currency(); $uah->LoadByCode('980');
		
		//make signature
		//$MerchantID = '1752256';  //test-server
		$MerchantID = '6984785';
		//$TerminalID = 'E7880056';  //test-server
		$TerminalID = 'E0133767';
		$PurchaseTime = date("ymdHis");
		$OrderID = $order_number;
		$Currency = '980';
		$TotalAmount = round( $total_amount * 100 * $uah->exchange_rate );
		$SD = $action;
		$data = "$MerchantID;$TerminalID;$PurchaseTime;$OrderID,1;$Currency;$TotalAmount;$SD;";
		if( $this->currency->code != 980 )
		{
			$AltTotalAmount = round( $total_amount * 100 / $this->currency->exchange_rate );
			$AltCurrency = $this->currency->code;
			$data = "$MerchantID;$TerminalID;$PurchaseTime;$OrderID,1;$Currency,$AltCurrency;$TotalAmount,$AltTotalAmount;$SD;";
		}
		//echo $data;
		//$fp = fopen( SITEROOT ."/upc/1752256.pem", "r" );  //test-server
		$fp = fopen( SITEROOT ."/upc/6984785.pem", "r" );
		if( $fp )
		{
			$priv_key = fread( $fp, 8192 );
			fclose( $fp );
			$pkeyid = openssl_pkey_get_private( $priv_key );
			openssl_sign( $data , $signature, $pkeyid );
			openssl_free_key( $pkeyid );
			$signature = base64_encode( $signature );
		}
		
		$xml = "<data>";
		$xml .= "<upc_MerchantID>{$MerchantID}</upc_MerchantID>";
		$xml .= "<upc_TerminalID>{$TerminalID}</upc_TerminalID>";
		$xml .= "<upc_TotalAmount>{$TotalAmount}</upc_TotalAmount>";
		$xml .= "<upc_Currency>{$Currency}</upc_Currency>";
		
		if( $AltTotalAmount )
		{
			$xml .= "<upc_AltTotalAmount>{$AltTotalAmount}</upc_AltTotalAmount>";
			$xml .= "<upc_AltCurrency>{$this->currency->code}</upc_AltCurrency>";
		}
		
		$xml .= "<upc_PurchaseTime>{$PurchaseTime}</upc_PurchaseTime>";
		$xml .= "<upc_OrderID>{$OrderID}</upc_OrderID>";
		$xml .= "<upc_SD>{$SD}</upc_SD>";
		$xml .= "<upc_Signature><![CDATA[{$signature}]]></upc_Signature>";
		$xml .= "</data>";
		
		$this->display( 'upc_pay', $xml );
	}
	
	function upc_success()
	{
		$order = new ShopOrder();
		$order->LoadByNumber( $_REQUEST['order_number'] );
		$order->LoadProducts();
		$this->save_order( $order );
	}
	
	function upc_failure()
	{
		//$o = new SOPOrder( $_REQUEST['order_id'] );
		
		$xml = "<data>";
		$xml .= "<OrderID>{$o->order_number}</OrderID>";
		$xml .= "<message>". $this->GetUPCResultMessage( $_REQUEST['tranCode'] ) ."</message>";
		$xml .= "</data>";
		
		$this->display( 'upc_failure', $xml );
	}
	
	function privat_pay( $order_number, $total_amount, $action='sop|kollage.com.ua' )
	{
		global $db;
		
		$uah = new Currency(); $uah->LoadByCode('980');
		
		$MerchantID = self::liqpay_merchant_id;
		$signature = self::liqpay_signature;
		$OrderID = $order_number;
		$TotalAmount = round( $total_amount * $uah->exchange_rate, 2 );
		$result_url = "http://www.kollage.com.ua/?shop.privat_return&amp;PHPSESSID=". session_id();
		$server_url = "http://www.kollage.com.ua/?shop.privat_return_server&amp;PHPSESSID=". session_id();
		
		$xml = "<request>";
		$xml .= "<version>1.2</version>";
		$xml .= "<merchant_id>{$MerchantID}</merchant_id>";
		$xml .= "<result_url>{$result_url}</result_url>";
		$xml .= "<server_url>{$server_url}</server_url>";
		$xml .= "<order_id>{$OrderID}</order_id>";
		$xml .= "<amount>{$TotalAmount}</amount>";
		$xml .= "<currency>UAH</currency>";
		$xml .= "<description>Order N{$OrderID}, Kollage studio, shop</description>";
		$xml .= "<default_phone></default_phone>";
		$xml .= "<pay_way>card</pay_way>";
		$xml .= "<goods_id></goods_id>";
		$xml .= "</request>";
		
		$signature_encoded = base64_encode( sha1( $signature . $xml . $signature, 1 ) );
		$xml_encoded = base64_encode( $xml );
		
		$data = "<data>";
		$data .= "<signature_encoded><![CDATA[{$signature_encoded}]]></signature_encoded>";
		$data .= "<xml_encoded><![CDATA[{$xml_encoded}]]></xml_encoded>";
		$data .= "</data>";
		
		$this->display( 'privat_pay', $data );
	}
	
	function privat_return_server()
	{
		$MerchantID = self::liqpay_merchant_id;
		$signature = self::liqpay_signature;
		
		$xml_decoded = base64_decode( $_POST['operation_xml'] );
		global $log;
		$signature_received = $_POST['signature'];
		$signature_encoded = base64_encode( sha1( $signature . $xml_decoded . $signature, 1 ) );
		$log->Write("privat_return_server xml_decoded={$xml_decoded}\nsignature_received={$signature_received}\nsignature_encoded={$signature_encoded}");
		
		if( strcmp($signature_received, $signature_encoded) == 0 ) {
			$response = new SimpleXMLElement( $xml_decoded );
			$status = $response->status;
			
			if( $status == "success" ) {
				$order_number = $response->order_id;
				$amount = $response->amount;
				$transaction_id = $response->transaction_id;
				
				$order = new ShopOrder();
				$order->LoadProducts();
				$order->LoadByNumber( $order_number );
				if( $order->status == 0 ) {
					$order->status = 1;
					$order->payed = $order->amount + $order->delivery->price;
					$order->Save();
					$this->save_order( $order );
				}
			}
		}
		else {
			$log->Write("privat_return_server signatures are not equal");
		}
		
	}
	
	function privat_return()
	{
		$MerchantID = self::liqpay_merchant_id;
		$signature = self::liqpay_signature;
		
		$xml_decoded = base64_decode( $_POST['operation_xml'] );
		global $log;
		$signature_received = $_POST['signature'];
		$signature_encoded = base64_encode( sha1( $signature . $xml_decoded . $signature, 1 ) );
		$log->Write("privat_return xml_decoded={$xml_decoded}\nsignature_received={$signature_received}\nsignature_encoded={$signature_encoded}");
		
		if( strcmp($signature_received, $signature_encoded) == 0 ) {
			$response = new SimpleXMLElement( $xml_decoded );
			$status = $response->status;
			
			if( $status == "success" ) {
				$order_number = $response->order_id;
				$amount = $response->amount;
				$transaction_id = $response->transaction_id;
				
				$order = new ShopOrder();
				$order->LoadProducts();
				$order->LoadByNumber( $order_number );
				if( $order->status == 0 ) {
					$order->status = 1;
					$order->payed = $order->amount + $order->delivery->price;
					$order->Save();
					$this->save_order( $order );
				}
			}
		}
		else {
			$log->Write("privat_return signatures are not equal");
		}
		
		$response = new SimpleXMLElement( $xml_decoded );
		$status = $response->status;
		$order_number = $response->order_id;
		
		$xml = "<data>";
		$xml .= "<order_number>{$order_number}</order_number>";
		$xml .= "<status>{$status}</status>";
		$xml .= "</data>";
		
		$this->display( 'privat_return', $xml );
	}
	
	function get_xls_price()
	{
		global $db;
		
		$uah = new Currency(); $uah->LoadByCode('980');
		
		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment;filename=export". date("Ymd") .".xls" );
		header('Pragma: no-cache');
		header('Expires: 0');
		
		$xml = "<?xml version='1.0' encoding='utf-8'?>";
		$xml .= "<?mso-application progid='Excel.Sheet'?>";
		$xml .= "<Workbook xmlns='urn:schemas-microsoft-com:office:spreadsheet' xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:x='urn:schemas-microsoft-com:office:excel' xmlns:ss='urn:schemas-microsoft-com:office:spreadsheet' xmlns:html='http://www.w3.org/TR/REC-html40'>";
		$xml .= '<DocumentProperties xmlns="urn:schemas-microsoft-com:office:office"><LastAuthor>Kollage studio</LastAuthor><Version>12.00</Version></DocumentProperties>';
		$xml .= '<ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel"><ProtectStructure>False</ProtectStructure><ProtectWindows>False</ProtectWindows></ExcelWorkbook>';
		$xml .= '<Styles><Style ss:ID="sBold" ss:Name="sBold"><Font ss:FontName="Arial Cyr" ss:Bold="1"/></Style></Styles>';
		
		$sql = "select pc.id,d.value, pc.position from shop_product_categories pc left join dictionary d on d.parent_id=pc.id where pc.parent_id is null and d.folder='shop_product_categories' and d.name='name' order by pc.position,pc.id";
		$rs = $db->query( $sql );
		while( $row = $rs->fetch_row() )
		{
			$c = new ShopCategory( $row[0] );
			$c_name = $c->name;
			if( mb_strlen($c_name,"UTF-8")>30 ) $c_name = mb_substr( $c->name, 0, 25, "UTF-8" ) .'...';
			$xml .= "<Worksheet ss:Name='{$c_name}'><Table>";
			$xml .= '<Column ss:AutoFitWidth="0" ss:Width="30.5"/>';
			$xml .= '<Column ss:AutoFitWidth="0" ss:Width="301"/>';
			//$xml .= '<Column ss:AutoFitWidth="0" ss:Width="57"/>';
			$xml .= '<Column ss:AutoFitWidth="0" ss:Width="70"/>';
			//$xml .= "<Row><Cell ss:StyleID='sBold'><Data ss:Type='String'>N</Data></Cell><Cell ss:StyleID='sBold'><Data ss:Type='String'>". $this->dictionary->get('/locale/shop/xls/name') ."</Data></Cell><Cell ss:StyleID='sBold'><Data ss:Type='String'>". $this->dictionary->get('/locale/shop/xls/price') .", {$this->currency->symbol}</Data></Cell><Cell ss:StyleID='sBold'><Data ss:Type='String'>". $this->dictionary->get('/locale/shop/xls/price') .", {$uah->symbol}</Data></Cell></Row>";
			$xml .= "<Row><Cell ss:StyleID='sBold'><Data ss:Type='String'>N</Data></Cell><Cell ss:StyleID='sBold'><Data ss:Type='String'>". $this->dictionary->get('/locale/shop/xls/name') ."</Data></Cell><Cell ss:StyleID='sBold'><Data ss:Type='String'>". $this->dictionary->get('/locale/shop/xls/price') .", {$uah->symbol}</Data></Cell></Row>";
			
			$sql = "select pc.id,d.value, pc.position from shop_product_categories pc left join dictionary d on d.parent_id=pc.id where pc.parent_id={$row[0]} and d.folder='shop_product_categories' and d.name='name' order by pc.position,pc.id";
			$rs3 = $db->query( $sql );
			while( $row3 = $rs3->fetch_row() )
			{
				$c2 = new ShopCategory( $row3[0] );
				$db->query( "set @ids=''" );
				$db->query( "set max_sp_recursion_depth=100" );
				$db->query( "call shop_product_categories({$row3[0]},@ids)" );
				$ids = $db->getone( "select @ids" );
				
				$xml .= "<Row><Cell ss:StyleID='sBold'><Data ss:Type='String'>{$c2->name}</Data></Cell></Row>";
			
				$sql = "select distinct p.id,p.price,p.price_opt,p.price_opt2,d.value,p.nice_price,(now()>=nice_price_start_at and now()<nice_price_end_at) np_active from shop_products p left join dictionary d on d.parent_id=p.id left join shop_product_availability pa on pa.product_id=p.id where pa.quantity>0 and d.folder='shop_products' and d.name='name' and p.status=1 and p.category_id in ({$ids}) {$brands_where} order by d.value";
				$rs2 = $db->query( $sql );
				$rownum = 0;
				while( $row2 = $rs2->fetch_row() )
				{
					$rownum++;
					$name = $row2[4];
					
					if( intval($row2[6]) == 1 )
					{
						$price = sprintf( "%01.2f", $row2[5] );
						$price_uah = sprintf( "%01.2f", $row2[5] * $uah->exchange_rate );
					}
					else
					{
						switch( $this->client->type_id )
						{
							case 2:
								$price = sprintf( "%01.2f", $row2[2] );
								$price_uah = sprintf( "%01.2f", $row2[2] * $uah->exchange_rate );
								break;
							case 3:
								$price = sprintf( "%01.2f", $row2[3] );
								$price_uah = sprintf( "%01.2f", $row2[3] * $uah->exchange_rate );
								break;
							default:
								$price = sprintf( "%01.2f", $row2[1] );
								$price_uah = sprintf( "%01.2f", $row2[1] * $uah->exchange_rate );
								break;
						}
					}
					$name = str_replace( "&", "&amp;", $name );
					$name = str_replace( ">", "&gt;", $name );
					$name = str_replace( "<", "&lt;", $name );
					//$price = $price * $this->currency->exchange_rate;
					//$xml .= "<Row><Cell><Data ss:Type='String'>{$rownum}.</Data></Cell><Cell><Data ss:Type='String'>{$name}</Data></Cell><Cell><Data ss:Type='Number'>{$price}</Data></Cell><Cell><Data ss:Type='Number'>{$price_uah}</Data></Cell></Row>";
					$xml .= "<Row><Cell><Data ss:Type='String'>{$rownum}.</Data></Cell><Cell><Data ss:Type='String'>{$name}</Data></Cell><Cell><Data ss:Type='Number'>{$price_uah}</Data></Cell></Row>";
				}
			}
		
			$xml .= "</Table>";
			//$xml .= '<WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel"><PageSetup><PageMargins x:Bottom="0.984251969" x:Left="0.78740157499999996" x:Right="0.78740157499999996" x:Top="0.984251969"/></PageSetup><Selected/><Panes><Pane><Number>3</Number><ActiveRow>2</ActiveRow><ActiveCol>1</ActiveCol></Pane></Panes><ProtectObjects>False</ProtectObjects><ProtectScenarios>False</ProtectScenarios></WorksheetOptions>';
			$xml .= "</Worksheet>";
		}
		
		$xml .= "</Workbook>";
		
		echo $xml;
	}
	
	function GetOrderStatusStr( &$order )
	{
		$retval = '';
		
		if( $order->IsLoaded() )
		{
			switch( $order->status )
			{
				case 1:
					$retval = $this->dictionary->get('/locale/my/orders/photo/statuses/s1');
					break;
				case 2:
					$retval = $this->dictionary->get('/locale/my/orders/photo/statuses/s2');
					break;
				case 3:
					$retval = $this->dictionary->get('/locale/my/orders/photo/statuses/s3');
					break;
			}
		}
		
		return( $retval );
	}
	
	function GetOrderSummaryTableForClient( &$order )
	{
		$retval = '';
		
		if( $order->IsLoaded() )
		{
			$td_style = "style='height: 22px;vertical-align: middle;border-bottom: 1px dashed #e3bf85;'";
			
			$retval .= '<table style="border: 1px solid #e3bf85" cellspacing="3">';
			$retval .= "<tr><th width='2%' {$td_style}>#</th>";
			$retval .= "<th width='*' align='left' {$td_style}>". $this->dictionary->get('/locale/shop/order/name') ."</th>";
			$retval .= "<th width='15%' {$td_style}>". $this->dictionary->get('/locale/shop/order/product_number') ."</th>";
			$retval .= "<th width='15%' align='right' {$td_style}>". $this->dictionary->get('/locale/shop/order/price'). $this->currency->symbol ."</th>";
			$retval .= "<th width='15%' {$td_style}>". $this->dictionary->get('/locale/shop/order/quantity') ."</th>";
			$retval .= "<th width='15%' align='right' {$td_style}>". $this->dictionary->get('/locale/shop/order/total'). $this->currency->symbol ."</th></tr>";
			
			$i = 1;
			foreach( $order->products as $p )
			{
				$retval .= "<tr><td {$td_style}>{$i}.</td>";
				$retval .= "<td {$td_style}>". $p->product->name ."</td>";
				$retval .= "<td align='center' {$td_style}>". $p->product->number ."</td>";
				$retval .= "<td align='right' {$td_style}>". sprintf("%1.2f",$p->price) ."</td>";
				$retval .= "<td align='center' {$td_style}>". $p->quantity ."</td>";
				$retval .= "<td align='right' {$td_style}>". sprintf("%1.2f",$p->price * $p->quantity) ."</td></tr>";
				$i++;         
			}
			
			$retval .= "<tr><th colspan='5' align='left' {$td_style}>". $this->dictionary->get('/locale/shop/order/delivery') ."</th><th align='right' {$td_style}>". sprintf("%1.2f",$order->delivery->price * $this->currency->exchange_rate) ."</th></tr>";
			$retval .= "<tr><th colspan='5' align='left'>". $this->dictionary->get('/locale/shop/order/total2') ."</th><th align='right'>". sprintf("%1.2f",$order->amount + $order->delivery->price * $this->currency->exchange_rate) ."</th></tr>";
			
			$retval .= '</table>';
		}
		
		return( $retval );
	}
	
	function GetOrderSummaryTableForAdmin( &$order ) {
		return $this->GetOrderSummaryTableForClient( $order );
	}
	
	function my_orders()
	{
		if( $this->client->IsLoaded() )
		{
			$this->method_name = 'my_orders';
			
			$xml = "<data>";
			
			$xml .= "<shop>";
			$t = new MysqlTable('shop_orders');
			//$t->find_all( "client_id={$this->client->id} and status>0 and status<5", "id desc" );
			$t->find_all( "client_id={$this->client->id} and status>0", "id desc" );
			foreach( $t->data as $row )
			{
				$o = new ShopOrder($row['id']);
				$xml .= $o->Xml();
			}
			$xml .= "</shop>";
			
			$xml .= "</data>";
			$this->display( 'my_orders', $xml );
		}
		else
		{
			$this->show();
		}
	}
	
	function my_purse()
	{
		if( $this->client->IsLoaded() )
		{
			$this->method_name = 'my_purse';
			
			$xml = "<data>";
			$xml .= "<list>";
			
			$t = new MysqlTable( 'purse_operations' );
			parent::define_pages( $t->get_count("client_id={$this->client->id}") );
			$t->find_all( "client_id={$this->client->id}", 'id desc', ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page );
			foreach( $t->data as $a )
			{
				$o = new PurseOperation( $a['id'] );
				$xml .= $o->Xml();
			}
			
			$xml .= "</list>";
			$xml .= "</data>";
			
			$this->display( 'my_purse', $xml );
		}
		else
		{
			$this->show();
		}
	}
	
	function generate_yml()
	{
		global $db;

		set_time_limit(3600);
		
		$xml = '<?xml version="1.0" encoding="windows-1251"?><!DOCTYPE yml_catalog SYSTEM "shops.dtd"><yml_catalog date="'. date('Y-m-d h:m') .'"><shop>';
		$xml .= "\n<name>Magazin</name>\n<company>Magazin</company>\n<url>http://www.kollage.com.ua/</url>\n";
		
		$xml .= "\n<currencies>";
		
		$sql = "select id from currencies where code=980 order by id desc limit 1";
		$id = $db->getone( $sql );
		$currency1 = new Currency( $id );
		
		$sql = "select id from currencies where code=840 order by id desc limit 1";
		$id = $db->getone( $sql );
		$currency2 = new Currency( $id );
		
		$xml .= '<currency id="'. $currency1->name .'" rate="'. $currency2->exchange_rate .'" plus="0"/>';
		$xml .= '<currency id="'. $currency2->name .'" rate="'. $currency1->exchange_rate .'" plus="0"/>';
		
		/*
		$sql = "select id from currencies where code=978 order by id desc limit 1";
		$id = $db->getone( $sql );
		$currency = new Currency( $id );
		$xml .= '<currency id="'. $currency->name .'" rate="'. $currency->exchange_rate .'" plus="0"/>';
		*/
		
		$xml .= "\n</currencies>\n";
		
		$xml .= "\n<categories>";
		$t = new MysqlTable('shop_product_categories');
		$t->find_all( "folder='shop' and parent_id is null", "position,id" );
		foreach( $t->data as $row )
		{
			$o = new ShopCategory( $row['id'] );
			$xml .= "\n<category id=\"{$o->id}\">". $this->convertText2YML($o->name) ."</category>";
			
			$t2 = new MysqlTable('shop_product_categories');
			$t2->find_all( "folder='shop' and parent_id={$o->id}", "position,id" );
			foreach( $t2->data as $row2 )
			{
				$cs = new ShopCategory( $row2['id'] );
				$xml .= "\n<category id=\"{$cs->id}\" parentId=\"{$cs->parent_id}\">". $this->convertText2YML($cs->name) ."</category>";
				
				$t3 = new MysqlTable('shop_product_categories');
				$t3->find_all( "folder='shop' and parent_id={$cs->id}", "position,id" );
				foreach( $t3->data as $row3 )
				{
					$cs2 = new ShopCategory( $row3['id'] );
					$xml .= "\n<category id=\"{$cs2->id}\" parentId=\"{$cs2->parent_id}\">". $this->convertText2YML($cs2->name) ."</category>";
				}
			}
		}
		$xml .= "\n</categories>\n";
		$xml .= "\n<offers>\n";
		
		$path = "yandex/";
		$tmp_name = "products.tmp";
		$file_name = "products.xml";
		@unlink( $path.$tmp_name );
		$f = fopen( $path.$tmp_name, "a+" );
		if( $f ) fwrite( $f, $xml );
		$xml = "";
		
		$www = "http://kollage.com.ua/?shop.product&amp;id=";
		$image_www = "http://kollage.com.ua/data/";
		//$sql = "select p.id from shop_products p where p.status=1";
		$sql = "select p.id from shop_products p left join shop_product_availability pa on pa.product_id=p.id where p.status=1 and pa.quantity>0";
		$rs = $db->query($sql);
		while( $row = $rs->fetch_row() )
		{
			$o = new ShopProduct( $row[0], false );
			
			$available = ( $o->quantity_local>0 )? 'true' : 'false';
			$picture = '';
			if( count($o->images)>0 ) {
				if( $o->images[0]->small ) $picture = $image_www . $o->images[0]->small;
				else if( $o->images[0]->tiny ) $picture = $image_www . $o->images[0]->tiny;
			}
			
			$offer = "\n<offer id=\"{$o->id}\" available=\"{$available}\">\n";
			$offer .= "<url>{$www}{$o->id}</url>\n";
			$offer .= "<price>". $o->GetPrice() ."</price>\n";
			$offer .= "<currencyId>USD</currencyId>\n";
			$offer .= "<categoryId>{$o->category_id}</categoryId>\n";
			if( $picture ) $offer .= "<picture>{$picture}</picture>\n";
			$offer .= "<name>". $this->convertText2YML($o->name) ."</name>\n";
			$offer .= "<description>". $this->convertText2YML($o->ingress) ."</description>\n";
			$offer .= "<sales_notes>����������� ����������</sales_notes>\n";
			$offer .= "</offer>\n";
			
			if( $f ) fwrite( $f, $offer );
		}
		
		$xml .= "\n</offers>\n</shop>\n</yml_catalog>";
		if( $f ) fwrite( $f, $xml );
		
		fclose( $f );
		@unlink( $path.$file_name );
		rename( $path.$tmp_name, $path.$file_name );
	}
	
	function convertText2YML( $txt )
	{
		$retval = convert_encoding( 'utf-8', 'windows-1251', $txt );
		$retval = str_replace( chr(0), '', $retval );
		$retval = str_replace( '"', '&quot;', $retval );
		$retval = str_replace( '&', '&amp;', $retval );
		$retval = str_replace( '>', '&gt;', $retval );
		$retval = str_replace( '<', '&lt;', $retval );
		$retval = str_replace( "'", '&apos;', $retval );
		
		return( $retval );
	}
	
	function InitShowProductsWay() {
		$shop_show_products = 1; //1-show all products; 2-show available products only
		if( $_REQUEST['shop_show_products'] )
		{
			$shop_show_products = $_REQUEST['shop_show_products'];
			$_SESSION['shop_show_products'] = $shop_show_products;
		}
		else
		{
			$shop_show_products = $_SESSION['shop_show_products'];
			if( !$shop_show_products ) { $shop_show_products = 2; $_SESSION['shop_show_products'] = $shop_show_products; }
		}
		
		return( $shop_show_products );
	}
	
	/* JSON services */
	function json_filter_products() {
		global $db, $log;
		$retval = new StdClass();
		$retval->result = 0;
		$retval->items = Array();
		$retval->item = new StdClass();
		
		if( $this->debug ) $d_time = microtime(true);
		
		$category_id = intval( $_REQUEST['category_id'] );
		if( $category_id > 0 ) {
			$ids = $this->categories_from_root( $category_id );
			if( $this->debug ) $this->log->Write( "shop.json_filter_products: 1 (". (microtime(true) - $d_time) ." sec)" );
			
			$sql_feature_values = "";
			// features by IDs
			$features_str = trim($_REQUEST['features']);
			if( $features_str ) {
				$features = explode( ',', $features_str );
				$values_by_feature = Array();
				foreach( $features as $fv_id ) {
					$fv = new ShopFeatureValue( $fv_id );
					if( !isset($values_by_feature[$fv->feature_id]) ) $values_by_feature[$fv->feature_id] = Array();
					array_push( $values_by_feature[$fv->feature_id], $fv_id );
				}
				foreach( $values_by_feature as $f ) {
					$sql_feature_values .= " AND p.id IN ( SELECT product_id FROM shop_product_features WHERE feature_value_id IN (". join(',', $f) .") )";
				}
			}
			if( $this->debug ) $this->log->Write( "shop.json_filter_products: 2 (". (microtime(true) - $d_time) ." sec)" );
			
			// features by Values
			$keys = array_keys( $_REQUEST );
			foreach( $keys as $k ) {
				if( preg_match( '/^feature_value_from([\d]+$)/i', $k, $matches ) ) {
					$fvid = intval( $matches[1] );
					$k_from = "feature_value_from{$fvid}";
					$k_to = "feature_value_to{$fvid}";
					$fv_from = floatval( $_REQUEST[$k_from] );
					$fv_to = floatval( $_REQUEST[$k_to] );
					if( $fvid>0 && $fv_from!=null && $fv_to!=null ) {
						$sql_feature_values .= " AND p.id IN ( SELECT DISTINCT pf.product_id FROM shop_product_features pf LEFT JOIN shop_feature_values fv ON fv.id=pf.feature_value_id LEFT JOIN dictionary d ON d.parent_id=fv.id AND d.folder='shop_feature_values' AND d.lang='{$_SESSION['lang']}' AND d.name='name' WHERE d.value>={$fv_from} AND d.value<={$fv_to} )";
					}
				}
			}
			
			$price_range_set = intval( $_REQUEST['price_range_set'] );
			$price_type = intval( $_REQUEST['price_type'] );
			$price_from = floatval( $_REQUEST['price_from'] );
			$price_to = floatval( $_REQUEST['price_to'] );
			$sql_prices = "";
			if( $price_range_set == 1 ) {
				$price_field = 'p.price';
				$sql_prices = " AND {$price_field}>={$price_from} AND {$price_field}<={$price_to}";
			}
			
			$shop_show_products = $this->InitShowProductsWay();
			if( $this->debug ) $this->log->Write( "shop.json_filter_products: 3 (". (microtime(true) - $d_time) ." sec)" );
			
			if( $shop_show_products == 1 ) { //show all products
				//parent::define_pages( $db->getone("SELECT count(DISTINCT p.id) FROM shop_products p WHERE p.status=1 and p.category_id={$category_id} {$sql_prices} {$sql_feature_values}") );
				//$sql = "SELECT DISTINCT p.id FROM shop_products p WHERE p.status=1 AND p.category_id={$category_id} {$sql_prices} {$sql_feature_values} ORDER BY p.price,p.id LIMIT ". ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page;
				parent::define_pages( $db->getone("SELECT count(DISTINCT p.id) FROM shop_products p WHERE p.status=1 and p.category_id IN ({$ids}) {$sql_prices} {$sql_feature_values}") );
				$sql = "SELECT DISTINCT p.id FROM shop_products p WHERE p.status=1 AND p.category_id IN ({$ids}) {$sql_prices} {$sql_feature_values} ORDER BY p.price DESC,p.id LIMIT ". ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page;
			}
			else {
				//parent::define_pages( $db->getone("SELECT count(DISTINCT p.id) FROM shop_products p LEFT JOIN shop_product_availability pa ON pa.product_id=p.id WHERE p.status=1 and pa.quantity>0 and p.category_id={$category_id} {$sql_prices} {$sql_feature_values}") );
				//$sql = "SELECT DISTINCT p.id FROM shop_products p LEFT JOIN shop_product_availability pa ON pa.product_id=p.id WHERE p.status=1 and pa.quantity>0 AND p.category_id={$category_id} {$sql_prices} {$sql_feature_values} ORDER BY p.price,p.id LIMIT ". ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page;
				parent::define_pages( $db->getone("SELECT count(DISTINCT p.id) FROM shop_products p LEFT JOIN shop_product_availability pa ON pa.product_id=p.id WHERE p.status=1 and pa.quantity>0 and p.category_id IN ({$ids}) {$sql_prices} {$sql_feature_values}") );
				$sql = "SELECT DISTINCT p.id, p.price FROM shop_products p LEFT JOIN shop_product_availability pa ON pa.product_id=p.id WHERE p.status=1 and pa.quantity>0 AND p.category_id IN ({$ids}) {$sql_prices} {$sql_feature_values} ORDER BY p.price DESC,p.id LIMIT ". ($this->page-1)*$this->rows_per_page .','. $this->rows_per_page;
			}
			//$log->Write( "json_filter_products: sql=". $sql );
			
			$retval->page = $this->page;
			$retval->pages = $this->pages;
			$retval->rows_per_page = $this->rows_per_page;
			
			$rs = $db->query($sql);
			if( $this->debug ) $this->log->Write( "shop.json_filter_products: 4 (". (microtime(true) - $d_time) ." sec)" );
			if( $rs ) {
				$retval->result = 1;
				while( $row = $rs->fetch_row() )
				{
					$o = new ShopProduct( $row[0], false );
					if( $this->debug ) $this->log->Write( "shop.json_filter_products: 5 (". (microtime(true) - $d_time) ." sec)" );
					$i = new StdClass();
					$i->id = $o->id;
					$i->category_id = $o->category_id;
					$i->number = $this->ConvertProductNumber( $o );
					$i->name = $o->name;
					$i->price = $this->FormatPrice( $o->price );
					//$i->price_opt = $this->FormatPrice( $o->price_opt );
					//$i->price_opt2 = $this->FormatPrice( $o->price_opt2 );
					$i->images = $o->images;
					$i->features = $o->features;
					$i->individual_features = $o->individual_features;
					$i->quantity_local = $o->quantity_local;
					$i->quantity_external = $o->quantity_external;
					$i->show_in_shop_news = $o->show_in_shop_news;
					$i->show_in_best = $o->show_in_best;
					$i->is_new = $o->is_new;
					$i->is_action = $o->is_action;
					
					$i->is_product_in_comparision = ( $this->IsProductInComparision($o->id) )? 1 : 0;
					if( $this->debug ) $this->log->Write( "shop.json_filter_products: 6 (". (microtime(true) - $d_time) ." sec)" );
					
					array_push( $retval->items, $i );
				}
			}
		}
		if( $this->debug ) $this->log->Write( "shop.json_filter_products: 7 (". (microtime(true) - $d_time) ." sec)" );
		
		echo json_encode( $retval );
	}
	
	protected function ConvertProductNumber( &$product ) {
		$retval = '-';
		
		if( $product && $product->IsLoaded() ) {
			$retval = $product->number;
			//if( stripos( $retval, $product->name )!==false ) {
			if( stripos( $retval, ' ' )!==false ) {
				$retval = '-';
			}
		}
		
		return( $retval );
	}
	
	function json_get_trash( $added_updated_id=0 ) {
		global $db, $log;
		$retval = new StdClass();
		$retval->result = 1;
		$retval->items = Array();
		$retval->item = new StdClass();
		
		$client_key = $this->getClientKey();

		if( $client_key != '' ) {
			$trash = new MysqlTable( 'shop_trash' );
			$trash->find_all( "client_key='{$client_key}'", 'id' );
			foreach( $trash->data as $r )
			{
				$t = new ShopTrash( $r['id'] );
				$product = new ShopProduct( $t->product_id, false );
				if( $product->IsLoaded() && $product->status=1 ) {
					$ti = new StdClass();
					$ti->id = $t->id;
					$ti->client_key = $t->client_key;
					$ti->product_id = $t->product_id;
					$ti->product_name = $product->name;
					$ti->product_price = sprintf( "%01.2f", $product->GetPriceByClient( $this->client ) * $this->currency->exchange_rate );
					$ti->quantity = $t->quantity;
					$ti->created_at = $t->created_at;
					$ti->updated_at = $t->updated_at;
			
					array_push( $retval->items, $ti );
				}
				else {
					$t->Delete();
				}
			}
		}
		else {
			$client_key = md5( time() . mt_rand(1,99) );
			setcookie( ShopTrash::$cookie_key_name, $client_key, time()+(3600*24*365), '/' );
		}
		
		$retval->item->key = $client_key;
		$retval->item->added_updated_id = $added_updated_id;
		
		echo json_encode( $retval );
	}
	
	function json_add_to_trash() {
		global $db, $log;
		
		$id = 0;
		$client_key = $this->getClientKey();
		if( $client_key != '' ) {
			$product_id = intval( $_REQUEST['id'] );
			$quantity = intval( $_REQUEST['quantity'] );
			$product = new ShopProduct( $product_id );
			if( $product->IsLoaded() ) {
				$t = new ShopTrash();
				$data = Array();
				$data['client_key'] = $client_key;
				$data['product_id'] = $product_id;
				$data['quantity'] = $quantity;
				$id = $t->Save( $data );
			}
		}
		
		$this->json_get_trash( $id );
	}
	
	function json_update_in_trash() {
		global $db, $log;
		
		$id = 0;
		$client_key = $this->getClientKey();
		if( $client_key != '' ) {
			$product_id = intval( $_REQUEST['id'] );
			$quantity = intval( $_REQUEST['quantity'] );
			
			$t = new ShopTrash();
			$t->LoadByKeyAndProductId( $client_key, $product_id );
			if( $quantity > 0 ) {
				$t->quantity = $quantity;
				$t->Save();
			}
			else {
				$t->Delete();
			}
			$id = $product_id;
		}
		
		$this->json_get_trash( $id );
	}
	
	function json_get_comparision( $added_updated_id=0 ) {
		global $db, $log;
		$retval = new StdClass();
		$retval->result = 1;
		$retval->items = Array();
		$retval->item = new StdClass();
		$retval->item->features = Array();
		
		//$d_time = microtime(true);
		
		$client_key = $this->getClientKey();

		if( $client_key != '' ) {
			$trash = new MysqlTable( 'shop_comparision' );
			$trash->find_all( "client_key='{$client_key}'", 'id' );
			foreach( $trash->data as $r )
			{
				$t = new ShopComparision( $r['id'] );
				$product = new ShopProduct( $t->product_id, false );
				//$this->log->Write( "json_get_comparision: 1(". (microtime(true) - $d_time) ." sec)" );
				$product->LoadFeatures( false );
				//$this->log->Write( "json_get_comparision: 2(". (microtime(true) - $d_time) ." sec)" );
				$ti = new StdClass();
				$ti->id = $t->id;
				$ti->client_key = $t->client_key;
				$ti->product_id = $t->product_id;
				$ti->product_name = $product->name;
				$ti->product_price = sprintf( "%01.2f", $product->GetPriceByClient( $this->client ) * $this->currency->exchange_rate );
				$ti->image = ( count($product->images)>0 )? $product->images[0]->path . $product->images[0]->tiny : '';
				$ti->created_at = $t->created_at;
				$ti->updated_at = $t->updated_at;
				
				if( count($retval->item->features) == 0 ) {
					foreach( $product->features as $f ) {
						$fo = new StdClass();
						$fo->id = $f->id;
						$fo->name = $f->name;
						array_push( $retval->item->features, $fo );
					}
				}
				
				$ti->feature_values = Array();
				foreach( $product->feature_values as $f ) {
					$fo = new StdClass();
					$fo->id = $f->id;
					$fo->name = $f->name;
					$fo->feature_id = $f->feature_id;
					array_push( $ti->feature_values, $fo );
				}
		
				array_push( $retval->items, $ti );
				//$this->log->Write( "json_get_comparision: 3(". (microtime(true) - $d_time) ." sec)" );
			}
		}
		else {
			$client_key = md5( time() . mt_rand(1,99) );
			setcookie( ShopTrash::$cookie_key_name, $client_key, time()+(3600*24*365), '/' );
		}
		
		$retval->item->key = $client_key;
		$retval->item->added_updated_id = $added_updated_id;
		
		echo json_encode( $retval );
		//$this->log->Write( "json_get_comparision: total ". (microtime(true) - $d_time) ." sec" );
	}
	
	function getComparisionCategoryId() {
		global $log;
		$retval = 0;
		
		$client_key = $this->getClientKey();
		$trash = new MysqlTable( 'shop_comparision' );
		$trash->find_first( "client_key='{$client_key}'", 'id' );
		if( $trash->data[0] && $trash->data[0]['product_id']>0 ) {
			$product = new ShopProduct( $trash->data[0]['product_id'] );
			$retval = $product->category_id;
		}
		
		return( $retval );
	}
	
	function json_add_to_comparision() {
		global $db, $log;
		
		$id = 0;
		$client_key = $this->getClientKey();
		if( $client_key != '' ) {
			$product_id = intval( $_REQUEST['id'] );
			$product = new ShopProduct( $product_id );
			if( $product->IsLoaded() ) {
				$products_category_id = $this->getComparisionCategoryId();
				if( $products_category_id == 0 || $product->category_id == $products_category_id ) {
					$t = new ShopComparision();
					$data = Array();
					$data['client_key'] = $client_key;
					$data['product_id'] = $product_id;
					$id = $t->Save( $data );
				}
				else {
					$id = -1; //we can't add this product which is from another category
				}
			}
		}
		
		$this->json_get_comparision( $id );
	}
	
	private function clearComparision() {
		$client_key = $this->getClientKey();
		$trash = new MysqlTable( 'shop_comparision' );
		$trash->find_all( "client_key='{$client_key}'", 'id' );
		foreach( $trash->data as $r )
		{
			$t = new ShopComparision( $r['id'] );
			$t->Delete();
		}
	}
	
	function json_delete_in_comparision() {
		global $db, $log;
		
		$id = 0;
		$client_key = $this->getClientKey();
		if( $client_key != '' ) {
			$product_id = intval( $_REQUEST['id'] );
			
			$t = new ShopComparision();
			$t->LoadByKeyAndProductId( $client_key, $product_id );
			$t->Delete();
		}
		
		$this->json_get_comparision( $id );
	}
	
	function json_delete_all_in_comparision() {
		global $db, $log;
		
		$id = 0;
		$client_key = $this->getClientKey();
		if( $client_key != '' ) {
			$db->query( "delete from shop_comparision where client_key='{$client_key}'" );
		}
		
		$this->json_get_comparision( $id );
	}
	/* JSON services */
	
	function getClientKey() {
		return $_COOKIE[ ShopTrash::$cookie_key_name ];
	}
	
	function get_order_protect_image_new()
	{
		$code = new ProtectCode( 6, PC_NUMERIC );
		$_SESSION['order_protect_code'] = $code->GetCode();
		if( $_SESSION['order_protect_code'] )
		{
			$obj = new ProtectImage( $_SESSION['order_protect_code'] );
			$obj->PrintImage();
		}
	}
	
	function get_request_product_protect_image_new()
	{
		$code = new ProtectCode( 6, PC_NUMERIC );
		$_SESSION['request_product_protect_code'] = $code->GetCode();
		if( $_SESSION['request_product_protect_code'] )
		{
			$obj = new ProtectImage( $_SESSION['request_product_protect_code'] );
			$obj->PrintImage();
		}
	}
}

?>
<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" version="1.0">

<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:template match="topmenu/*">
	<li>
		<xsl:choose>
			<xsl:when test="@url!=''">
				<a href="{@url}"><xsl:value-of select="@title" disable-output-escaping="yes"/></a>
			</xsl:when>
			<xsl:otherwise>
				<a href="#" onclick="return(false)"><xsl:value-of select="@title"/></a>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:if test="count(./node()[starts-with(name(),'submenu')]) &gt; 0">
		<ul class="hidden">
			<xsl:apply-templates select="./node()[starts-with(name(),'submenu')]"/>
		</ul>
		</xsl:if>
		<xsl:if test="count(./item) &gt; 0">
		<ul class="hidden">
			<xsl:apply-templates select="./item"/>
		</ul>
		</xsl:if>
	</li>
</xsl:template>

<xsl:template match="topmenu/*/node()[starts-with(name(),'submenu')]">
	<li>
		<xsl:choose>
			<xsl:when test="@url != ''">
				<a href="{@url}"><xsl:value-of select="@title"/></a>
			</xsl:when>
			<xsl:otherwise>
				<a href="#" onclick="return(false)"><xsl:value-of select="@title"/></a>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:if test="count(./item) &gt; 0">
		<ul>
			<xsl:apply-templates select="./item"/>
		</ul>
		</xsl:if>
	</li>
</xsl:template>

<xsl:template match="topmenu/*/item">
	<xsl:if test="@title != ''">
		<li><a href="{@url}"><xsl:value-of select="@title" disable-output-escaping="yes"/></a></li>
	</xsl:if>
</xsl:template>

<xsl:template match="topmenu/*/*/item">
	<xsl:if test="@title != ''">
		<li><a href="{@url}"><xsl:value-of select="@title" disable-output-escaping="yes"/></a></li>
	</xsl:if>
</xsl:template>

<xsl:template match="leftmenu">
	<ul class="leftmenu">
		<xsl:apply-templates/>
	</ul>
</xsl:template>

<xsl:template match="leftmenu/*[starts-with(local-name(),'submenu')]">
	<li class="leftmenu-submenu">
		<span><xsl:value-of select="@title" disable-output-escaping="yes"/></span>
		<ul>
			<xsl:apply-templates/>
		</ul>
	</li>
</xsl:template>

<xsl:template match="leftmenu/*[starts-with(local-name(),'submenu')]/*[starts-with(local-name(),'item')]">
	<li><a href="{@url}"><xsl:value-of select="@title" disable-output-escaping="yes"/></a></li>
</xsl:template>

</xsl:stylesheet>

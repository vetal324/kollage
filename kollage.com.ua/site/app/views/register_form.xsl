<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
                
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="common.xsl" />
<xsl:include href="locale.xsl" />

<xsl:template match="/">
  <html>
    <head>
      <title><xsl:value-of select="$locale/common/title/text()" disable-output-escaping="yes"/></title>
      <link rel="stylesheet" href="{$views_path}/styles.css" />
      <script language="JavaScript" src="{$views_path}/common.js"></script>
      <script language="JavaScript" src="{$views_path}/shared/prototype.js"></script>
      <script language="JavaScript" src="{$views_path}/shared/JsHttpRequest.js"></script>
    </head>
    <body style="margin:10px">
        <h3 class="page_title"><xsl:value-of select="$locale/register_form/form_title/text()"/></h3>
        
        <xsl:if test="content/documents/data/result = 0 or content/documents/data/result = 2 or content/documents/data/result = 3 or content/documents/data/result = 4">
        <div style="border-bottom: solid 3px #ffdb00;text-align:center">
            <table align="center"><tr>
                <td><input type="radio" name="region_tab" onclick="$('sumy').style.display='';$('another').style.display='none';" id="region_tab1" value="1" checked="yes"/></td><td><label for="region_tab1"><xsl:value-of select="$locale/register_form/sumy/text()"/></label></td>
                <td width="20">&#160;</td>
                <td><input type="radio" name="region_tab" onclick="$('another').style.display='';$('sumy').style.display='none';" id="region_tab2" value="2"/></td><td><label for="region_tab2"><xsl:value-of select="$locale/register_form/another/text()"/></label></td>
            </tr></table>
        </div>
        </xsl:if>
        
        <div id="sumy">
        
        <table width="100%" cellpadding="0" cellspacing="0"><tr>
        <td valign="top" align="left">
        
	        <!-- Sumy register table -->
	        <table cellpadding="0" cellspacing="1">
	        
	            <xsl:if test="content/documents/data/result = 0 or content/documents/data/result = 2 or content/documents/data/result = 3 or content/documents/data/result = 4">
	            
	            <xsl:if test="content/documents/data/result = 2">
	            <tr>
	            <td class="error"><xsl:value-of select="$locale/register_form/protect_code_wrong/text()" disable-output-escaping="yes"/></td>
	            </tr>
	            </xsl:if>
	            
	            <xsl:if test="content/documents/data/result = 3">
	            <tr>
	            <td class="error"><xsl:value-of select="$locale/register_form/save_error/text()" disable-output-escaping="yes"/></td>
	            </tr>
	            </xsl:if>
	            
	            <xsl:if test="content/documents/data/result = 4">
	            <tr>
	            <td class="error"><xsl:value-of select="$locale/register_form/email_exists/text()" disable-output-escaping="yes"/></td>
	            </tr>
	            </xsl:if>
	            
	            <script>
	            function checkregform()
	            {
	                var c1, c2 = false, retval = false;
	                
	                c1 = checkform(this,[['firstname','null','<xsl:value-of select="$locale/register_form/m/firstname/text()"/>'],['lastname','null','<xsl:value-of select="$locale/register_form/m/lastname/text()"/>'],['email','null','<xsl:value-of select="$locale/register_form/m/email/text()"/>'],['street_id','select','<xsl:value-of select="$locale/register_form/m/street/text()"/>'],['password','null','<xsl:value-of select="$locale/register_form/m/password/text()"/>'],['city','null','<xsl:value-of select="$locale/register_form/m/city/text()"/>'],['address','null','<xsl:value-of select="$locale/register_form/m/address/text()"/>'],['phone','null','<xsl:value-of select="$locale/register_form/m/phone/text()"/>'],['protect_code','null','<xsl:value-of select="$locale/register_form/m/protect_code/text()"/>'],['zip','null','<xsl:value-of select="$locale/register_form/m/zip/text()"/>']])
	                if( c1 ) c2 = confirm('��� �������������� ����� �� �������� ������� ���������: ���, ������� � �������� �����.\n� ��������� ������ �� �� ������ ��������� ��� �����!\n����������� ���������� � ���������� ������.');
	                
	                if( c1 &amp;&amp; c2 ) retval = true;
	                
	                return( retval );
	            }
	            </script>
	        
	            <form method="post" id="register_form" action="?{$controller_name}.register_client"
	            onsubmit="return(checkregform())">
	            <input type="hidden" name="upc" id="upc" value=""/>
	            
	            <tr>
	            <td class="ftitle"><xsl:value-of select="$locale/register_form/firstname/text()" disable-output-escaping="yes"/>*</td>
	            </tr>
	            <tr>
	            <td><input type="text" name="object[firstname]" id="firstname" class="inputbox_middle" value="{content/documents/data/firstname}"/></td>
	            </tr>
	            
	            <tr>
	            <td class="ftitle"><xsl:value-of select="$locale/register_form/lastname/text()" disable-output-escaping="yes"/>*</td>
	            </tr>
	            <tr>
	            <td><input type="text" name="object[lastname]" id="lastname" class="inputbox_middle" value="{content/documents/data/lastname}"/></td>
	            </tr>
	            
	            <tr>
	            <td class="ftitle"><xsl:value-of select="$locale/register_form/email/text()" disable-output-escaping="yes"/>*</td>
	            </tr>
	            <tr>
	            <td><input type="text" name="object[email]" id="email" class="inputbox_middle" value="{content/documents/data/email}"/></td>
	            </tr>
	            
	            <tr>
	            <td class="ftitle"><xsl:value-of select="$locale/register_form/password/text()" disable-output-escaping="yes"/>*</td>
	            </tr>
	            <tr>
	            <td><input type="password" name="object[password]" id="password" class="inputbox_middle" value="{content/documents/data/password}"/></td>
	            </tr>
	            
	            <tr>
	            <td class="ftitle"><xsl:value-of select="$locale/register_form/city/text()" disable-output-escaping="yes"/>*</td>
	            </tr>
	            <tr>
	            <!--<td><input type="text" name="object[city]" readonly="yes" id="city" class="inputbox_middle" value="{content/documents/data/city}"/></td>-->
	            <td><input type="text" name="object[city]" readonly="yes" id="city" class="inputbox_middle" value="����"/></td>
	            </tr>
	            
	            <!--
	            <tr>
	            <td class="ftitle"><xsl:value-of select="$locale/register_form/region/text()" disable-output-escaping="yes"/></td>
	            </tr>
	            <tr><td>
	                <xsl:variable name="selected" value=""/>
	                <select name="region_id" id="region_id" onchange="FillStreets()">
	                    <xsl:for-each select="content/documents/data/regions/*">
	                        <xsl:call-template name="gen-option">
	                            <xsl:with-param name="value" select="id"/>
	                            <xsl:with-param name="title" select="name"/>
	                            <xsl:with-param name="selected" select="$selected"/>
	                        </xsl:call-template>
	                    </xsl:for-each>
	                </select>
	            </td></tr>
	            -->
	            
	            <tr>
	            <td class="ftitle"><xsl:value-of select="$locale/register_form/street/text()" disable-output-escaping="yes"/></td>
	            </tr>
	            <tr><td>
	                <xsl:variable name="selected" value=""/>
	                <select name="object[street_id]" id="street_id" style="width:200px">
	                </select>
	            </td></tr>
	            
	            <tr>
	            <td class="ftitle"><xsl:value-of select="$locale/register_form/zip/text()" disable-output-escaping="yes"/>*</td>
	            </tr>
	            <tr>
	            <td><input type="text" name="object[zip]" id="zip" class="inputbox_middle" value="{content/documents/data/zip}"/></td>
	            </tr>
	            
	            <tr>
	            <td class="ftitle"><xsl:value-of select="$locale/register_form/address/text()" disable-output-escaping="yes"/>*</td>
	            </tr>
	            <tr>
	            <td><input type="text" name="object[address]" id="address" class="inputbox_middle" value="{content/documents/data/address}"/></td>
	            </tr>
	            
	            <tr>
	            <td class="ftitle"><xsl:value-of select="$locale/register_form/phone/text()" disable-output-escaping="yes"/>*</td>
	            </tr>
	            <tr>
	            <td><input type="text" name="object[phone]" id="phone" class="inputbox_middle" value="{content/documents/data/phone}"/></td>
	            </tr>
	            
	            <tr>
	            <td class="ftitle"><xsl:value-of select="$locale/register_form/parent/text()" disable-output-escaping="yes"/></td>
	            </tr>
	            <tr>
	            <td><input type="text" name="object[parent_id]" id="parent_id" class="inputbox_middle" value=""/></td>
	            </tr>
	            
	            <tr>
	            <td class="ftitle"><xsl:value-of select="$locale/register_form/send_news/text()" disable-output-escaping="yes"/></td>
	            </tr>
	            <tr><td>
	                <xsl:choose>
	                    <xsl:when test="content/documents/data/send_news = 1"><input type="radio" name="object[send_news]" id="send_news_active" value="1" checked="yes" /></xsl:when>
	                    <xsl:otherwise><input type="radio" name="object[send_news]" id="send_news_active" value="1" /></xsl:otherwise>
	                </xsl:choose>
	                <label for="send_news_active" class="lower"><xsl:value-of select="$locale/common/yes/text()"/></label>
	                <xsl:choose>
	                    <xsl:when test="content/documents/data/send_news = 0"><input type="radio" name="object[send_news]" id="send_news_inactive" value="0" checked="yes" /></xsl:when>
	                    <xsl:otherwise><input type="radio" name="object[send_news]" id="send_news_inactive" value="0"/></xsl:otherwise>
	                </xsl:choose>
	                <label for="send_news_inactive" class="lower"><xsl:value-of select="$locale/common/no/text()"/></label>
	            </td></tr>
	            
	            <tr><td class="ftitle"><xsl:value-of select="$locale/register_form/protect/text()" disable-output-escaping="yes"/></td></tr>
	            <tr><td><table><tr>
	                    <td></td>
	                    <td><img src="?{$controller_name}.protect_image"/></td>
	                    <td><input type="button" value="{$locale/register_form/form_update/text()}" class="button" onclick="document.getElementById('register_form').value='1'; document.getElementById('register_form').submit()"/></td>
	                </tr></table>
	            </td></tr>
	            <tr><td><input type="text" name="protect_code" id="protect_code" class="inputbox_middle" value=""/></td></tr>
	            
	            <tr>
	            <td style="padding: 5px 0 5px 0;"><input type="submit" id="submit_button" value="{$locale/register_form/form_submit/text()}" class="button" style="visibility:hidden"/></td>
	            </tr>
	            
	            </form>
	            
	            <script>
	            FillStreets();
	            function FillStreets()
	            {
	                var s, region_id, d, i;
	                
	                s = $('street_id');
	                
	                s.options.length = 0;
	                s.options[0] = new Option( '��������....................', 0 );
	                d = new Date();
	                region_id = 0;
	                JsHttpRequest.query(
	                    '?base.get_streets&amp;region_id='+region_id+'&amp;date='+ d.getTime(),
	                    {
	                    },
	                    function( result, txt )
	                    {
	                        for( i=0; i&lt;result['streets'].length; i++ )
	                        {
	                            if( i == 0 ) s.options[i] = new Option( '�������� �����', 0 );
	                            else s.options[i] = new Option( result['streets'][i][1], result['streets'][i][0] );
	                        }
	                        $('submit_button').style.visibility = '';
	                    }
	                );
	            }
	            </script>
	            
	            </xsl:if>
	            
	            <xsl:if test="content/documents/data/result = 1">
	            <tr>
	            <td class="msg"><xsl:value-of select="$locale/register_form/client_was_saved/text()" disable-output-escaping="yes"/></td>
	            </tr>
	            <tr>
	            <td style="padding: 5px 0 5px 0;"><input type="button" value="{$locale/register_form/form_close/text()}" class="button" onclick="window.close()"/></td>
	            </tr>
	            </xsl:if>
	            
	        </table>
	        <!-- Sumy register table -->
	        
	      </td>
        <td valign="top" align="right">
        	<p style="padding:20px 10px 0 20px;text-align:left;font-weight:bold;line-height:130%" class="msg">��� ��������� SMS ����������  � ���������� ������ � ����� �������, ��������  ���������  � ������� 507 � ���������� �������� �� ����� 4590.</p>
        </td>
				</tr></table>
        
        </div>
        
        
        <div id="another" style="display:none">
        
        <table width="100%" cellpadding="0" cellspacing="0"><tr>
        <td valign="top" align="left">

	        <!-- Another city -->
	        <table cellpadding="0" cellspacing="1">
	        
	            <xsl:if test="content/documents/data/result = 0 or content/documents/data/result = 2 or content/documents/data/result = 3 or content/documents/data/result = 4">
	            
	            <xsl:if test="content/documents/data/result = 2">
	            <tr>
	            <td class="error"><xsl:value-of select="$locale/register_form/protect_code_wrong/text()" disable-output-escaping="yes"/></td>
	            </tr>
	            </xsl:if>
	            
	            <xsl:if test="content/documents/data/result = 3">
	            <tr>
	            <td class="error"><xsl:value-of select="$locale/register_form/save_error/text()" disable-output-escaping="yes"/></td>
	            </tr>
	            </xsl:if>
	            
	            <xsl:if test="content/documents/data/result = 4">
	            <tr>
	            <td class="error"><xsl:value-of select="$locale/register_form/email_exists/text()" disable-output-escaping="yes"/></td>
	            </tr>
	            </xsl:if>
	        
	            <form method="post" id="register_form2" action="?{$controller_name}.register_client"
	            onsubmit="return(checkform(this,[['firstname2','null','{$locale/register_form/m/lastname/text()}'],['lastname2','null','{$locale/register_form/m/firstname/text()}'],['email2','null','{$locale/register_form/m/email/text()}'],['password2','null','{$locale/register_form/m/password/text()}'],['city2','null','{$locale/register_form/m/city/text()}'],['address2','null','{$locale/register_form/m/address/text()}'],['phone2','null','{$locale/register_form/m/phone/text()}'],['protect_code2','null','{$locale/register_form/m/protect_code/text()}'],['zip2','null','{$locale/register_form/m/zip/text()}']]))">
	            <input type="hidden" name="upc" id="upc" value=""/>
	            
	            <tr>
	            <td class="ftitle"><xsl:value-of select="$locale/register_form/firstname/text()" disable-output-escaping="yes"/>*</td>
	            </tr>
	            <tr>
	            <td><input type="text" name="object[firstname]" id="firstname2" class="inputbox_middle" value="{content/documents/data/firstname}"/></td>
	            </tr>
	            
	            <tr>
	            <td class="ftitle"><xsl:value-of select="$locale/register_form/lastname/text()" disable-output-escaping="yes"/></td>
	            </tr>
	            <tr>
	            <td><input type="text" name="object[lastname]" id="lastname2" class="inputbox_middle" value="{content/documents/data/lastname}"/></td>
	            </tr>
	            
	            <tr>
	            <td class="ftitle"><xsl:value-of select="$locale/register_form/email/text()" disable-output-escaping="yes"/>*</td>
	            </tr>
	            <tr>
	            <td><input type="text" name="object[email]" id="email2" class="inputbox_middle" value="{content/documents/data/email}"/></td>
	            </tr>
	            
	            <tr>
	            <td class="ftitle"><xsl:value-of select="$locale/register_form/password/text()" disable-output-escaping="yes"/>*</td>
	            </tr>
	            <tr>
	            <td><input type="password" name="object[password]" id="password2" class="inputbox_middle" value="{content/documents/data/password}"/></td>
	            </tr>
	            
	            <tr>
	            <td class="ftitle"><xsl:value-of select="$locale/register_form/city/text()" disable-output-escaping="yes"/>*</td>
	            </tr>
	            <tr>
	            <td><input type="text" name="object[city]" id="city2" class="inputbox_middle" value="{content/documents/data/city}"/></td>
	            </tr>
	            
	            <tr>
	            <td class="ftitle"><xsl:value-of select="$locale/register_form/zip/text()" disable-output-escaping="yes"/>*</td>
	            </tr>
	            <tr>
	            <td><input type="text" name="object[zip]" id="zip2" class="inputbox_middle" value="{content/documents/data/zip}"/></td>
	            </tr>
	            
	            <tr>
	            <td class="ftitle"><xsl:value-of select="$locale/register_form/address2/text()" disable-output-escaping="yes"/>*</td>
	            </tr>
	            <tr>
	            <td><input type="text" name="object[address]" id="address2" class="inputbox_middle" value="{content/documents/data/address}"/></td>
	            </tr>
	            
	            <tr>
	            <td class="ftitle"><xsl:value-of select="$locale/register_form/phone/text()" disable-output-escaping="yes"/>*</td>
	            </tr>
	            <tr>
	            <td><input type="text" name="object[phone]" id="phone2" class="inputbox_middle" value="{content/documents/data/phone}"/></td>
	            </tr>
	            
	            <tr>
	            <td class="ftitle"><xsl:value-of select="$locale/register_form/parent/text()" disable-output-escaping="yes"/></td>
	            </tr>
	            <tr>
	            <td><input type="text" name="object[parent_id]" id="parent_id" class="inputbox_middle" value=""/></td>
	            </tr>
	            
	            <tr>
	            <td class="ftitle"><xsl:value-of select="$locale/register_form/send_news/text()" disable-output-escaping="yes"/></td>
	            </tr>
	            <tr><td>
	                <xsl:choose>
	                    <xsl:when test="content/documents/data/send_news = 1"><input type="radio" name="object[send_news]" id="send_news_active" value="1" checked="yes" /></xsl:when>
	                    <xsl:otherwise><input type="radio" name="object[send_news]" id="send_news_active" value="1" /></xsl:otherwise>
	                </xsl:choose>
	                <label for="send_news_active" class="lower"><xsl:value-of select="$locale/common/yes/text()"/></label>
	                <xsl:choose>
	                    <xsl:when test="content/documents/data/send_news = 0"><input type="radio" name="object[send_news]" id="send_news_inactive" value="0" checked="yes" /></xsl:when>
	                    <xsl:otherwise><input type="radio" name="object[send_news]" id="send_news_inactive" value="0"/></xsl:otherwise>
	                </xsl:choose>
	                <label for="send_news_inactive" class="lower"><xsl:value-of select="$locale/common/no/text()"/></label>
	            </td></tr>
	            
	            <tr><td class="ftitle"><xsl:value-of select="$locale/register_form/protect/text()" disable-output-escaping="yes"/></td></tr>
	            <tr><td><table><tr>
	                    <td></td>
	                    <td><img src="?{$controller_name}.protect_image"/></td>
	                    <td><input type="button" value="{$locale/register_form/form_update/text()}" class="button" onclick="document.getElementById('register_form').value='1'; document.getElementById('register_form').submit()"/></td>
	                </tr></table>
	            </td></tr>
	            <tr><td><input type="text" name="protect_code" id="protect_code2" class="inputbox_middle" value=""/></td></tr>
	            
	            <tr>
	            <td style="padding: 5px 0 5px 0;"><input type="submit" value="{$locale/register_form/form_submit/text()}" class="button"/></td>
	            </tr>
	            
	            </form>
	            </xsl:if>
	            
	            <xsl:if test="content/documents/data/result = 1">
	            <tr>
	            <td class="msg"><xsl:value-of select="$locale/register_form/client_was_saved/text()" disable-output-escaping="yes"/></td>
	            </tr>
	            <tr>
	            <td style="padding: 5px 0 5px 0;"><input type="button" value="{$locale/register_form/form_close/text()}" class="button" onclick="window.close()"/></td>
	            </tr>
	            </xsl:if>
	            
	        </table>
	        <!-- Another city -->
        
	      </td>
        <td valign="top" align="right">
        	<p style="padding:20px 10px 0 20px;text-align:left;font-weight:bold;line-height:130%" class="msg">��� ��������� SMS ����������  � ���������� ������ � ����� �������, ��������  ���������  � ������� 507 � ���������� �������� �� ����� 4590.</p>
        </td>
				</tr></table>
				
        </div>
        
    </body>
  </html>
</xsl:template>

</xsl:stylesheet>

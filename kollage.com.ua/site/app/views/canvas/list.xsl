<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

    <xsl:output method="html" encoding="utf-8" indent="no"/>

    <xsl:include href="../layout.xsl" />

    <xsl:template match="data">
        <h3 class="page_title">Категории</h3>
        <xsl:apply-templates select="items"/>
        <div style="clear:both"></div>
    </xsl:template>

    <xsl:template match="items">
        <xsl:choose>
            <xsl:when test="category">
                <xsl:apply-templates select="category">
                </xsl:apply-templates>
            </xsl:when>
            <xsl:otherwise>
                <tr><td colspan="20" align="center"><xsl:value-of select="$locale/categories/category/list/insert_msg/text()"/></td></tr>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="category">
        <a href="/?canvas.redactor&amp;id={id}">
        <div style="width:300px;height:300px;float:left;text-align:center;color:#000000;margin-right:15px;border:1px">
                <IMG>
                    <xsl:attribute name="src">
                        <xsl:value-of select="image"/>
                    </xsl:attribute>
                    <xsl:attribute name="width">
                        200px
                    </xsl:attribute>
                </IMG><br />
                <xsl:value-of select="name"/>
        </div>
        </a>
    </xsl:template>

</xsl:stylesheet>
<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
                
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="common.xsl" />
<xsl:include href="locale.xsl" />

<xsl:template match="/">
  <html>
    <head>
      <title><xsl:value-of select="$locale/common/title/text()" disable-output-escaping="yes"/></title>
      <link rel="stylesheet" href="{$views_path}/styles.css" />
      <script language="JavaScript" src="{$views_path}/common.js"></script>
    </head>
    <body style="margin:10px">
        <h3 class="page_title"><xsl:value-of select="$locale/password_reminder_form/form_title/text()"/></h3>
        
        <table cellpadding="0" cellspacing="1">
            <form method="post">
            
            <xsl:if test="content/documents/data/result = 0">
            <tr>
            <td class="ftitle"><xsl:value-of select="$locale/password_reminder_form/username/text()" disable-output-escaping="yes"/></td>
            </tr>
            <tr>
            <td><input type="text" name="login" class="inputbox_middle2" value=""/></td>
            </tr>
            <tr>
            <td style="padding: 5px 0 5px 0;"><input type="submit" value="{$locale/password_reminder_form/form_submit/text()}" class="button"/></td>
            </tr>
            </xsl:if>
            
            <xsl:if test="content/documents/data/result = 1">
            <tr>
            <td class="msg"><xsl:value-of select="$locale/password_reminder_form/mail_was_sent/text()" disable-output-escaping="yes"/></td>
            </tr>
            <tr>
            <td style="padding: 5px 0 5px 0;"><input type="button" value="{$locale/password_reminder_form/form_close/text()}" class="button" onclick="window.close()"/></td>
            </tr>
            </xsl:if>
            
            <xsl:if test="content/documents/data/result = 2">
            <tr>
            <td class="error"><xsl:value-of select="$locale/password_reminder_form/client_not_found/text()" disable-output-escaping="yes"/></td>
            </tr>
            <tr>
            <td class="ftitle"><xsl:value-of select="$locale/password_reminder_form/username/text()" disable-output-escaping="yes"/></td>
            </tr>
            <tr>
            <td><input type="text" name="login" class="inputbox" value=""/></td>
            </tr>
            <tr>
            <td style="padding: 5px 0 5px 0;"><input type="submit" value="{$locale/password_reminder_form/form_submit/text()}" class="button"/></td>
            </tr>
            </xsl:if>
            
            </form>
        </table>
        
    </body>
  </html>
</xsl:template>

</xsl:stylesheet>

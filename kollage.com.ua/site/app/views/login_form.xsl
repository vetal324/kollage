<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
                
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:template name="login_form">
    <!-- LOGIN BOX -->
    <table width="185" height="25" cellpadding="0" cellspacing="0" class="login_title">
        <tr>
            <td><xsl:value-of select="$locale/login_form/form_title/text()" disable-output-escaping="yes"/></td>
        </tr>
    </table>
    <table width="185" cellpadding="15" cellspacing="0" class="login">
        <tr>
            <td>
                <table cellpadding="0" cellspacing="1">
                
                    <xsl:if test="/content/global/errors/error_login = 1">
                    <tr><td class="error"><xsl:value-of select="$locale/login_form/error_login/text()" disable-output-escaping="yes"/></td></tr>
                    </xsl:if>
                    
                    <form method="post" action="/login/?url=&#63;{$controller_name}.{$method_name}">
                    <tr>
                	<td class="ftitle"><xsl:value-of select="$locale/login_form/username/text()" disable-output-escaping="yes"/></td>
                    </tr>
                    <tr>
                	<td><input type="text" name="login" class="inputbox" value="{/content/global/errors/error_login/@data}"/></td>
                    </tr>
                    <tr>
                	<td class="ftitle"><xsl:value-of select="$locale/login_form/password/text()" disable-output-escaping="yes"/></td>
                    </tr>
                    <tr>
                	<td><input type="password" name="password" class="inputbox"/></td>
                    </tr>
                    <tr>
                	<td style="padding: 5px 0 5px 0;"><input type="submit" value="{$locale/login_form/form_submit/text()}" class="button"/></td>
                    </tr>
                    </form>
                    <tr>
                	<td><small><a href="#" onclick="popup_window(400,200,'?{$controller_name}.password_reminder')"><xsl:value-of select="$locale/login_form/forget_password/text()" disable-output-escaping="yes"/></a></small></td>
                    </tr>
                    <tr>
                	<td><small><a href="#" onclick="popup_window(550,640,'?{$controller_name}.register_form')"><xsl:value-of select="$locale/login_form/register_link/text()" disable-output-escaping="yes"/></a></small></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <!-- LOGIN BOX - END -->
</xsl:template>

</xsl:stylesheet>

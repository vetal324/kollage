﻿
function delete_marked_objects( url, msg )
{
    msg = (msg)? msg : 'Are you sure to delete marked objects?';
    if( confirm(msg) )
    {
        var i = 1, item = null;
        var ids = ''; delim = '';
        while( item = document.getElementById('sitem'+i) )
        {
            if( item.checked )
            {
                ids += delim + item.value;
                delim = ',';
            }
            i++;
        }
        if(ids!='')
        {
            document.location = add_to_url(url,'&ids='+ids);
        }
    }
}

function ShowPhotosForm()
{
    var w = 520, h = 420;
    obj = new Element( 'div', {'id':"uf_layout",'style':"position:absolute;z-index:200;top:0;left:0;width:"+ w +"px;height:"+ h +"px;overflow:auto;border:2px solid #333333;background-color:white"});
    bg = new Element( 'div', {'id':"uf_bg",'class':'shadow1','style':"position:absolute;z-index:50;top:-1;left:-1px;width:1px;height:1px"} );
    obj.innerHTML = 'Загрузка формы..';
    Element.insert( GetFirstNode(GetBody()), bg );
    Element.insert( GetFirstNode(GetBody()), obj );
    if( obj != null )
    {
        obj.style.left = (GetWindowSize()[0] - w) / 2;
        obj.style.top = GetWindowScroll()[1] + (GetWindowSize()[1] - h) / 2;
        obj.style.width = w + 'px';
        obj.style.height = h + 'px';
        
        bg.style.top = '0px'; bg.style.left = '0px';
        bg.style.width = GetObjectSize(document.body)[0];
        bg.style.height = GetObjectSize(document.body)[1];
        
        HideAllSelectboxes( $('order_params') );
        
        var d = new Date();
        JsHttpRequest.query(
            '?fair.add_photos_form&date='+ d.getTime(),
            {
            },
            function( result, txt )
            {
                $('uf_layout').innerHTML = txt;
                for( i=1; i<=8; i++ )
                {
                    $('photos_inputs').innerHTML += '<div class="upload_photo"><input type="file" name="photo'+ i +'" id="photo'+ i +'"/></div>';
                }
                for( i=9; i<=16; i++ )
                {
                    $('photos_inputs2').innerHTML += '<div class="upload_photo"><input type="file" name="photo'+ i +'" id="photo'+ i +'"/></div>';
                }
            }
        );
        
    }
}

function ClosePhotosForm()
{
    var obj, bg, i;
    
    Element.remove('uf_layout');
    Element.remove('uf_bg');
    ShowAllSelectboxes( $('order_params') );
}

function SubmitPhotosFrom()
{
    $('btn_submit').setAttribute('disabled','yes');
    $('btn_close').setAttribute('disabled','yes');
    $('wait_img').style.visibility='inherit';
    
    if( BrowserDetect.browser == 'Explorer' )
    {
        $('wait_img').innerHTML = '<img src="../app/views/images/progress.gif" border="0">';
    }
    
    $('add_photos_form').submit();
}

function ShowDocumentsForm( object_id )
{
    var w = 640, h = 500;
    obj = new Element( 'div', {'id':"uf_layout",'style':"position:absolute;z-index:200;top:0;left:0;width:"+ w +"px;height:"+ h +"px;overflow:auto;border:2px solid #333333;background-color:white"});
    bg = new Element( 'div', {'id':"uf_bg",'class':'shadow1','style':"position:absolute;z-index:50;top:-1;left:-1px;width:1px;height:1px"} );
    obj.innerHTML = 'Загрузка формы..';
    Element.insert( GetFirstNode(GetBody()), bg );
    Element.insert( GetFirstNode(GetBody()), obj );
    if( obj != null )
    {
        obj.style.left = (GetWindowSize()[0] - w) / 2;
        obj.style.top = GetWindowScroll()[1] + (GetWindowSize()[1] - h) / 2;
        obj.style.width = w + 'px';
        obj.style.height = h + 'px';
        
        bg.style.top = '0px'; bg.style.left = '0px';
        bg.style.width = GetObjectSize(document.body)[0];
        bg.style.height = GetObjectSize(document.body)[1];
        
        var d = new Date();
        JsHttpRequest.query(
            '?fair.add_documents_form&date='+ d.getTime(),
            {
                object_id: object_id
            },
            function( result, txt )
            {
                $('uf_layout').innerHTML = txt;
            }
        );
        
    }
}

function CloseDocumentsForm()
{
    var obj, bg, i;
    
    Element.remove('uf_layout');
    Element.remove('uf_bg');
    
    document.location = document.location;
}

function UploadDocument()
{
    $('btn_submit').setAttribute('disabled','yes');
    $('btn_close').setAttribute('disabled','yes');
    $('wait_img').style.visibility='inherit';
    
    if( BrowserDetect.browser == 'Explorer' )
    {
        $('wait_img').innerHTML = '<img src="../app/views/images/progress.gif" border="0">';
    }
    
    var d = new Date();
    JsHttpRequest.query(
        '?fair.add_document&date='+ d.getTime(),
        {
            name: $('name').value,
            description: $('description').value,
            object_id: $('object_id').value,
            file: $('file')
        },
        function( result, txt )
        {
            $('uf_layout').innerHTML = txt;
        }
    );
}

function BelongDocument( object, object_id, document_id )
{
    if( object.checked ) flag = 1;
    else flag = 0;
    
    var d = new Date();
    JsHttpRequest.query(
        '?fair.belong_document&date='+ d.getTime(),
        {
            object_id: object_id,
            document_id: document_id,
            flag: flag
        },
        function( result, txt )
        {
        }
    );
}

function del_document( url, msg )
{
    msg = (msg)? msg : 'Are you sure to delete this object?';
    if( confirm(msg) )
    {
        $('btn_submit').setAttribute('disabled','yes');
        $('btn_close').setAttribute('disabled','yes');
        $('wait_img').style.visibility='inherit';
        
        if( BrowserDetect.browser == 'Explorer' )
        {
            $('wait_img').innerHTML = '<img src="../app/views/images/progress.gif" border="0">';
        }
        
        var d = new Date();
        JsHttpRequest.query(
            url + '&date='+ d.getTime(),
            {
            },
            function( result, txt )
            {
                $('uf_layout').innerHTML = txt;
            }
        );
    }
}

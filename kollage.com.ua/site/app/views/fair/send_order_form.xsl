<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
                
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout_fair.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:template match="data">
    <h3 class="page_title"><xsl:value-of select="$locale/fair/title_order/text()"/> <xsl:if test="/content/global/client/email != ''">&#160;&#160;(<xsl:value-of select="/content/global/client/email"/>)</xsl:if></h3>
    
    <xsl:if test="/content/global/client/id = 0">
        <xsl:call-template name="login_form3"/>
    </xsl:if>
    
    <xsl:if test="/content/global/client/id > 0 and /content/global/client/allow_fair = 1">
        <div class="error"><xsl:value-of select="$locale/fair/errors/already_exists" disable-output-escaping="yes"/></div>
    </xsl:if>
    
    <xsl:if test="/content/global/client/id > 0 and /content/global/client/allow_fair != 1">
    
        <script language="JavaScript" src="{$views_path}/{$controller_name}/functions.js"></script>
        
        <xsl:if test="result = 0">
            <div class="error"><xsl:value-of select="$locale/fair/errors/parameters" disable-output-escaping="yes"/></div>
        </xsl:if>
        
        <xsl:if test="result = 2">
            <div class="error"><xsl:value-of select="$locale/fair/errors/protect_code" disable-output-escaping="yes"/></div>
        </xsl:if>
    
        <form method="post" action="?{$controller_name}.send_order" enctype="multipart/form-data">
            <table>
                <tr><td colspan="2" class="ftitle"><xsl:value-of select="$locale/fair/send_order_body"/></td></tr>
                
                <tr><td colspan="2"><textarea name="object[body]" cols="80" rows="8"><xsl:value-of select="body"/></textarea></td></tr>
            
                <tr><td class="ftitle"><xsl:value-of select="$locale/register_form/protect/text()" disable-output-escaping="yes"/></td></tr>
                <tr><td>
                    <table cellspacing="0" cellpadding="0"><tr>
                        <td><img src="?{$controller_name}.fair_protect_image"/></td>
                        <td>&#160;&#160;&#160;</td>
                        <td><input type="text" name="protect_code" id="protect_code2" class="inputbox_middle" value=""/></td>
                    </tr></table>
                </td></tr>
            
                <tr><td>
                    <input type="submit" value="{$locale/common/buttons/form_send/text()}" class="button"/>&#160;&#160;&#160;
                </td></tr>
            
            </table>
        </form>
        
    </xsl:if>

</xsl:template>

</xsl:stylesheet>

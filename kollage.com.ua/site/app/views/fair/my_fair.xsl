<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
                
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout_fair.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:template match="data">
    <table cellspacing="0" cellpadding="0" border="0" width="100%"><tr>
        <td><h3 class="page_title"><xsl:value-of select="$locale/my/fair/title/text()"/> <xsl:if test="/content/global/client/email != ''">&#160;&#160;(<xsl:value-of select="/content/global/client/email"/>)</xsl:if></h3></td>
        <td valign="bottom"><div class="fair_tab_selected" onclick="document.location='?{$controller_name}.my_fair'">����������</div></td>
        <td valign="bottom"><div class="fair_tab" onclick="document.location='?{$controller_name}.my_fair2'">�������</div></td>
    </tr></table>
    
    <xsl:if test="/content/global/client/id = 0">
        <xsl:call-template name="login_form2"/>
    </xsl:if>
    
    <xsl:if test="/content/global/client/id > 0 and /content/global/client/allow_fair = 1">
    
        <script language="JavaScript" src="{$views_path}/{$controller_name}/functions2.js"></script>
        
        <xsl:if test="error">
            <div><xsl:value-of select="error" disable-output-escaping="yes"/></div>
        </xsl:if>
    
        <form method="post" action="?{$controller_name}.add_object" enctype="multipart/form-data">
            <div>
                <xsl:value-of select="$locale/my/fair/add_object/text()"/><input type="file" class="" name="file"/>&#160;
                <input type="submit" value="{$locale/my/fair/add_object_submit/text()}" class="button"/>&#160;&#160;&#160;
                <input type="button" value="{$locale/my/fair/add_some_objects/text()}" class="button" onclick="ShowPhotosForm()"/>&#160;&#160;
                <input type="button" value="{$locale/my/fair/delete_objects/text()}" class="button" onclick="delete_marked_objects('?{$controller_name}.delete_marked&amp;page={$page}','{$locale/my/fair/delete_objects_msg}')"/>
            </div>
        </form>
    
        <!-- PAGINATOR -->
        <div class="pagination" pages="{$pages}"><nobr>
        <xsl:if test="$pages > 1">
            <xsl:call-template name="pagination">
                <xsl:with-param name="page"><xsl:value-of select="$page"/></xsl:with-param>
                <xsl:with-param name="stop"><xsl:value-of select="$pages"/></xsl:with-param>
                <xsl:with-param name="url">?<xsl:value-of select="$controller_name"/>.my_fair</xsl:with-param>
            </xsl:call-template>
            / <a href="?{$controller_name}.my_fair&amp;page={$page - 1}"><xsl:value-of select="$locale/common/link_prev/text()" disable-output-escaping="yes"/></a> | <a href="?{$controller_name}.my_fair&amp;page={$page + 1}"><xsl:value-of select="$locale/common/link_next/text()" disable-output-escaping="yes"/></a>
        </xsl:if>
        </nobr></div>
        <!-- PAGINATOR - END -->

        <div class="fair_matrix"><xsl:apply-templates select="objects/*"/></div>
        
        <!-- PAGINATOR -->
        <div class="pagination" pages="{$pages}"><nobr>
        <xsl:if test="$pages > 1">
            <xsl:call-template name="pagination">
                <xsl:with-param name="page"><xsl:value-of select="$page"/></xsl:with-param>
                <xsl:with-param name="stop"><xsl:value-of select="$pages"/></xsl:with-param>
                <xsl:with-param name="url">?<xsl:value-of select="$controller_name"/>.my_fair</xsl:with-param>
            </xsl:call-template>
            / <a href="?{$controller_name}.my_fair&amp;page={$page - 1}"><xsl:value-of select="$locale/common/link_prev/text()" disable-output-escaping="yes"/></a> | <a href="?{$controller_name}.my_fair&amp;page={$page + 1}"><xsl:value-of select="$locale/common/link_next/text()" disable-output-escaping="yes"/></a>
        </xsl:if>
        </nobr></div>
        <!-- PAGINATOR - END -->
        
        <div class="help">
            <div><img src="{$views_path}/images/fair_wait.gif"/> - <xsl:value-of select="$locale/my/fair/status0/text()" disable-output-escaping="yes"/></div>
            <div><img src="{$views_path}/images/fair_allowed.gif"/> - <xsl:value-of select="$locale/my/fair/status1/text()" disable-output-escaping="yes"/></div>
        </div>
        
    </xsl:if>
    
</xsl:template>

<xsl:template match="object">
    <div class="fair_cell">
        <table width="100%" height="100%">
            <tr>
                <td class="product" valign="top">
                    <table cellspacing="0" cellpadding="0" width="100%" height="100%">
                        <tr>
                            <td height="10%" align="center">
                                <xsl:choose>
                                    <xsl:when test="thumbnail != ''">
                                        <a href="?{$controller_name}.edit_object&amp;id={id}&amp;page={$page}"><img src="?{$controller_name}.display_photo&amp;id={id}" border="0" width="{thumbnail_w}" height="{thumbnail_h}"/></a>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <img src="{$views_path}/images/empty.gif" width="110" height="110" border="0"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </td>
                        </tr>
                        <tr>
                            <td height="*" valign="top">
                                <table width="100%" cellpadding="0" cellspacing="0" class="product_info" border="0">
                                    <tr>
                                        <td>
                                            <xsl:choose>
                                                <xsl:when test="status = 1"><img src="{$views_path}/images/fair_allowed.gif"/></xsl:when>
                                                <xsl:otherwise><img src="{$views_path}/images/fair_wait.gif"/></xsl:otherwise>
                                            </xsl:choose>
                                        </td>
                                        <td align="right"><input type="checkbox" id="sitem{position()}" value="{id}"/></td>
                                    </tr>
                                    <tr>
                                        <td align="left"><xsl:value-of select="$locale/my/fair/views/text()" disable-output-escaping="yes"/></td>
                                        <td><xsl:value-of select="views"/></td>
                                    </tr>
                                    <tr>
                                        <td align="left"><xsl:value-of select="$locale/my/fair/sales/text()" disable-output-escaping="yes"/></td>
                                        <td><xsl:value-of select="sales"/></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="left" style="padding-top:3px;padding-bottom: 3px"><a href="?{$controller_name}.edit_object&amp;id={id}&amp;page={$page}">��������� >></a></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="left" style="padding-top:3px;padding-bottom: 3px"><xsl:value-of select="title" disable-output-escaping="yes"/>&#160;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</xsl:template>

</xsl:stylesheet>

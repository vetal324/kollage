<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../login_form.xsl" />
<xsl:include href="../login_form2.xsl" />

<xsl:template match="/">
    <form name="add_photos_form" id="add_photos_form" method="post" action="?{$controller_name}.add_objects" enctype="multipart/form-data">
    <table cellpadding="10" cellspacing="0" class="content_table" width="100%" height="100%">
        <tr>
            <td colspan="2" height="24"><h3 class="page_title"><xsl:value-of select="$locale/photos/order/title2/text()"/></h3></td>
        </tr>
        <tr>
            <td valign="top" colspan="2">
                <xsl:value-of select="$locale/my/fair/add_some_objects_help/text()" disable-output-escaping="yes"/>
            </td>
        </tr>
        <tr>
            <td width="50%" valign="top">
                <div id="photos_inputs"></div>
            </td>
            <td width="50%" valign="top">
                <div id="photos_inputs2"></div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table cellspacing="0" cellpadding="2"><tr>
                    <td><input type="button" class="button" id="btn_submit" value="{$locale/photos/order/add_photos/button_upload/text()}" onclick="SubmitPhotosFrom()"/></td>
                    <td><input type="button" class="button" id="btn_close" value="{$locale/photos/order/add_photos/button_close/text()}" onclick="ClosePhotosForm()"/></td>
                    <td><div style="padding:left:20px" name="add_photos_msg" id="add_photos_msg"></div></td>
                    <td><div style="padding:left:20px;visibility:hidden" name="wait_img" id="wait_img">&#160;<img src="{$views_path}/images/progress.gif" border="0"/></div></td>
                </tr></table>
            </td>
        </tr>
    </table>
    </form>
</xsl:template>

</xsl:stylesheet>

<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../login_form.xsl" />
<xsl:include href="../login_form2.xsl" />

<xsl:template match="/">
    <table cellpadding="10" cellspacing="0" class="content_table" width="100%" height="100%">
        <tr>
            <td height="24"><h3 class="page_title"><xsl:value-of select="$locale/fair/slideshow/title/text()"/><span id="title" style="padding-left:20px;font-size:11px;color:black"></span></h3></td>
        </tr>
        <tr>
            <td align="center" valign="middle"><img id="ss_img" src="{$views_path}/images/empty.gif"/></td>
        </tr>
        <tr>
            <td valign="bottom">
                <table cellspacing="0" cellpadding="2"><tr>
                    <td><input type="button" class="button" id="btn_close" value="{$locale/fair/slideshow/buttons/close/text()}" onclick="CloseSSForm()"/></td>
                    <td><input type="button" class="button" id="btn_stop" value="{$locale/fair/slideshow/buttons/stop/text()}" onclick="SSStop()"/></td>
                    <td><input type="button" class="button" id="btn_start" value="{$locale/fair/slideshow/buttons/start/text()}" onclick="SSShowImage()"/></td>
                    <td>
                        <select onchange="ss_interval=this.options[this.selectedIndex].value">
                            <option value="3000">3 ���</option>
                            <option value="5000">5 ���</option>
                            <option value="6000" selected="yes">6 ���</option>
                            <option value="7000">7 ���</option>
                            <option value="10000">10 ���</option>
                            <option value="20000">20 ���</option>
                        </select>
                    </td>
                </tr></table>
            </td>
        </tr>
    </table>
</xsl:template>

</xsl:stylesheet>

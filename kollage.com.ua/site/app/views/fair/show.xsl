<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout_fair.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:template match="data">

	<h1 class="fair_title">
		<table cellspacing="0" cellpadding="0" width="100%"><tr>
		<td width="90%">
		<xsl:choose>
			<xsl:when test="design/@type = 'new'">
				<xsl:value-of select="$locale/fair/title/text()" disable-output-escaping="yes"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:for-each select="cline/*">
					<a href="?{$controller_name}.show&amp;category_id={id}"><xsl:value-of select="name"/></a>&#160;&#187;&#160;
				</xsl:for-each>
				<!-- <xsl:value-of select="category/name"/> -->
			</xsl:otherwise>
		</xsl:choose>
		</td>
		
		<td width="*" align="right">
			<xsl:call-template name="fair_trash">
				<xsl:with-param name="back_url" select="back_url"/>
			</xsl:call-template>
		</td>
		</tr></table>
		
	</h1>
	<xsl:if test="category/id > 0">
	<form action="?{$controller_name}.show" method="get" style="border:1px solid white">
		<input type="hidden" name="{$controller_name}.show" value=""/>
		<input type="hidden" name="category_id" value="{category/id}"/>
		<input type="hidden" name="page" value="1"/>
		<div class="brands_selection">
			[<input type="radio" name="free_section" value="1" id="free_section_1"><xsl:if test="fair_objects/@free_section = 1"><xsl:attribute name="checked"/></xsl:if></input>&#160;<label for="free_section_1"><xsl:value-of select="$locale/fair/filter/paid/text()" disable-output-escaping="yes"/></label>&#160;&#160;
			<input type="radio" name="free_section" value="2" id="free_section_2"><xsl:if test="fair_objects/@free_section = 2"><xsl:attribute name="checked"/></xsl:if></input>&#160;<label for="free_section_2"><xsl:value-of select="$locale/fair/filter/free/text()" disable-output-escaping="yes"/></label>&#160;&#160;
			<input type="radio" name="free_section" value="-1" id="free_section_3"><xsl:if test="fair_objects/@free_section = -1 or string(fair_objects/@free_section) = ''"><xsl:attribute name="checked"/></xsl:if></input>&#160;<label for="free_section_3"><xsl:value-of select="$locale/fair/filter/all/text()" disable-output-escaping="yes"/></label>&#160;]
			&#160;&#160;&#160;
			[<input type="radio" name="soft_section" value="1" id="soft_section_1"><xsl:if test="fair_objects/@soft_section = 1"><xsl:attribute name="checked"/></xsl:if></input>&#160;<label for="soft_section_1"><xsl:value-of select="$locale/fair/filter/software/text()" disable-output-escaping="yes"/></label>&#160;&#160;
			<input type="radio" name="soft_section" value="2" id="soft_section_2"><xsl:if test="fair_objects/@soft_section = 2"><xsl:attribute name="checked"/></xsl:if></input>&#160;<label for="soft_section_2"><xsl:value-of select="$locale/fair/filter/without_software/text()" disable-output-escaping="yes"/></label>&#160;&#160;
			<input type="radio" name="soft_section" value="-1" id="soft_section_3"><xsl:if test="fair_objects/@soft_section = -1 or string(fair_objects/@soft_section) = ''"><xsl:attribute name="checked"/></xsl:if></input>&#160;<label for="soft_section_3"><xsl:value-of select="$locale/fair/filter/all/text()" disable-output-escaping="yes"/></label>&#160;]
			&#160;&#160;
			<input type="submit" class="button" value="{$locale/shop/list/brands_submit/text()}"/>
		</div>
	</form>
	</xsl:if>
	
	<xsl:if test="count(fair_objects/*) > 6 and category/id > 0">
		<script language="JavaScript" src="{$views_path}/{$controller_name}/slideshow.js"></script>
		<div style="clear:left;float:left;padding-top: 10px;padding-bottom: 10px;"><a href="#" onclick="ShowSSForm('{category/id}');return(false)"><xsl:value-of select="$locale/fair/slideshow_link/text()" disable-output-escaping="yes"/></a></div>
	</xsl:if>
	
	<!-- PAGINATOR -->
	<div class="pagination" style="clear:right" pages="{$pages}"><nobr>
	<xsl:if test="$pages > 1">
	
		<xsl:choose>
			<xsl:when test="design/@type = 'new'">
		<xsl:call-template name="pagination">
			<xsl:with-param name="page"><xsl:value-of select="$page"/></xsl:with-param>
			<xsl:with-param name="stop"><xsl:value-of select="$pages"/></xsl:with-param>
			<xsl:with-param name="url">?<xsl:value-of select="$controller_name"/>.show</xsl:with-param>
		</xsl:call-template>
		/ <a href="?{$controller_name}.show&amp;page={$page - 1}"><xsl:value-of select="$locale/common/link_prev/text()" disable-output-escaping="yes"/></a> | <a href="?{$controller_name}.show&amp;page={$page + 1}"><xsl:value-of select="$locale/common/link_next/text()" disable-output-escaping="yes"/></a>
			</xsl:when>
			<xsl:otherwise>
		<xsl:call-template name="pagination">
			<xsl:with-param name="page"><xsl:value-of select="$page"/></xsl:with-param>
			<xsl:with-param name="stop"><xsl:value-of select="$pages"/></xsl:with-param>
			<xsl:with-param name="url">?<xsl:value-of select="$controller_name"/>.show&amp;category_id=<xsl:value-of select="category/id"/></xsl:with-param>
		</xsl:call-template>
		/ <a href="?{$controller_name}.show&amp;category_id={category/id}&amp;page={$page - 1}"><xsl:value-of select="$locale/common/link_prev/text()" disable-output-escaping="yes"/></a> | <a href="?{$controller_name}.show&amp;category_id={category/id}&amp;page={$page + 1}"><xsl:value-of select="$locale/common/link_next/text()" disable-output-escaping="yes"/></a>
			</xsl:otherwise>
		</xsl:choose>
		
	</xsl:if>
	</nobr></div>
	<!-- PAGINATOR - END -->
	
	<div style="display:none"><xsl:apply-templates select="rest_fair_objects_prev/*"/></div>
	
	<div class="matrix"><xsl:apply-templates select="fair_objects/*"/></div>
	
	<div style="display:none"><xsl:apply-templates select="rest_fair_objects/*"/></div>
	
	<!-- PAGINATOR -->
	<div class="pagination" pages="{$pages}">
	<xsl:if test="$pages > 1">
	<table cellspacing="0" cellpadding="0" border="0" width="100%"><tr>
	
		<td align="left">
		<select id="rows_per_page_top" onchange="document.location='?fair.show&amp;category_id={category/id}&amp;page=1&amp;rows_per_page='+this.options[this.selectedIndex].value">
		<xsl:variable name="selected" select="$rows_per_page"/>
			<xsl:call-template name="gen-option">
				<xsl:with-param name="value">8</xsl:with-param>
				<xsl:with-param name="title">8</xsl:with-param>
				<xsl:with-param name="selected" select="$selected"/>
			</xsl:call-template>
			<xsl:call-template name="gen-option">
				<xsl:with-param name="value">12</xsl:with-param>
				<xsl:with-param name="title">12</xsl:with-param>
				<xsl:with-param name="selected" select="$selected"/>
			</xsl:call-template>
			<xsl:call-template name="gen-option">
				<xsl:with-param name="value">20</xsl:with-param>
				<xsl:with-param name="title">20</xsl:with-param>
				<xsl:with-param name="selected" select="$selected"/>
			</xsl:call-template>
			<xsl:call-template name="gen-option">
				<xsl:with-param name="value">32</xsl:with-param>
				<xsl:with-param name="title">32</xsl:with-param>
				<xsl:with-param name="selected" select="$selected"/>
			</xsl:call-template>
			<xsl:call-template name="gen-option">
				<xsl:with-param name="value">1000000</xsl:with-param>
				<xsl:with-param name="title"><xsl:value-of select="$locale/shop/list/all"/></xsl:with-param>
				<xsl:with-param name="selected" select="$selected"/>
			</xsl:call-template>
		</select>
		<span>&#160;<xsl:value-of select="$locale/shop/list/rows_per_page"/></span>
		</td>
	
		<td align="right">
		<xsl:choose>
			<xsl:when test="design/@type = 'new'">
		<xsl:call-template name="pagination">
			<xsl:with-param name="page"><xsl:value-of select="$page"/></xsl:with-param>
			<xsl:with-param name="stop"><xsl:value-of select="$pages"/></xsl:with-param>
			<xsl:with-param name="url">?<xsl:value-of select="$controller_name"/>.show</xsl:with-param>
		</xsl:call-template>
		/ <a href="?{$controller_name}.show&amp;page={$page - 1}"><xsl:value-of select="$locale/common/link_prev/text()" disable-output-escaping="yes"/></a> | <a href="?{$controller_name}.show&amp;page={$page + 1}"><xsl:value-of select="$locale/common/link_next/text()" disable-output-escaping="yes"/></a>
			</xsl:when>
			<xsl:otherwise>
		<xsl:call-template name="pagination">
			<xsl:with-param name="page"><xsl:value-of select="$page"/></xsl:with-param>
			<xsl:with-param name="stop"><xsl:value-of select="$pages"/></xsl:with-param>
			<xsl:with-param name="url">?<xsl:value-of select="$controller_name"/>.show&amp;category_id=<xsl:value-of select="category/id"/></xsl:with-param>
		</xsl:call-template>
		/ <a href="?{$controller_name}.show&amp;category_id={category/id}&amp;page={$page - 1}"><xsl:value-of select="$locale/common/link_prev/text()" disable-output-escaping="yes"/></a> | <a href="?{$controller_name}.show&amp;category_id={category/id}&amp;page={$page + 1}"><xsl:value-of select="$locale/common/link_next/text()" disable-output-escaping="yes"/></a>
			</xsl:otherwise>
		</xsl:choose>
		</td>
		
	</tr></table>
	</xsl:if>
	</div>
	<!-- PAGINATOR - END -->
</xsl:template>

<xsl:template match="object_old">
	<div class="fair_cell">
		<table width="100%" height="100%">
			<tr>
				<td class="product" valign="top">
					<table cellspacing="0" cellpadding="0" width="100%" height="100%">
						<tr>
							<td height="10%" align="center">
								<xsl:choose>
									<xsl:when test="thumbnail != ''">
										<a href="#" onclick="PreviewImage(event,'?{$controller_name}.display_photo&amp;id={id}&amp;type=1','{preview_w}','{preview_h}','{$locale/photos/order/zoom2/text()}','{$locale/photos/order/zoom_logo2/text()}');return(false)"><img src="?{$controller_name}.display_photo&amp;id={id}" width="{thumbnail_w}" height="{thumbnail_h}" border="0"/></a>
									</xsl:when>
									<xsl:otherwise>
										<img src="{$views_path}/images/empty.gif" width="180" height="124" border="0"/>
									</xsl:otherwise>
								</xsl:choose>
							</td>
						</tr>
						<tr>
							<td height="*" valign="top">
								<table width="100%" cellpadding="0" cellspacing="0" class="product_info" border="0">
									<tr>
										<td align="left">#<xsl:value-of select="id" disable-output-escaping="yes"/></td>
										<td align="right">XS-<xsl:choose>
												<xsl:when test="xxl !=''">XXL</xsl:when>
												<xsl:when test="xl !=''">XL</xsl:when>
												<xsl:when test="l !=''">L</xsl:when>
												<xsl:when test="m !=''">M</xsl:when>
												<xsl:otherwise>S</xsl:otherwise>
											</xsl:choose>
										</td>
									</tr>
									
									<tr>
										<td colspan="2" align="left"><a href="?{$controller_name}.object&amp;id={id}&amp;back_page={$page}"><xsl:value-of select="$locale/shop/list/details" disable-output-escaping="yes"/></a></td>
									</tr>
									
									<tr>
										<td colspan="2" align="left" style="padding-top:1px;padding-bottom:3px"><xsl:value-of select="title" disable-output-escaping="yes"/></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
</xsl:template>

<xsl:template match="fair_objects/object">
	<div class="fair_cell">
		<table width="100%" height="100%">
			<tr>
				<td class="product" valign="top">
					<table cellspacing="0" cellpadding="0" width="100%" height="100%">
						<tr>
							<td height="10%" align="center">
								<xsl:choose>
									<xsl:when test="thumbnail != ''">
										<a class="highslide" href="?{$controller_name}.display_photo&amp;id={id}&amp;type=1" onclick="return hs.expand(this, {{ slideshowGroup: 1, captionText: '{client/firstname} {client/lastname}: {title}'}} )"><img src="?{$controller_name}.display_photo&amp;id={id}" width="{thumbnail_w}" height="{thumbnail_h}" border="0"/></a>
									</xsl:when>
									<xsl:otherwise>
										<img src="{$views_path}/images/empty.gif" width="180" height="124" border="0"/>
									</xsl:otherwise>
								</xsl:choose>
							</td>
						</tr>
						<tr>
							<td height="*" valign="top">
								<table width="100%" cellpadding="0" cellspacing="0" class="product_info" border="0">
									<tr>
										<td align="left">#<xsl:value-of select="id" disable-output-escaping="yes"/></td>
										<td align="right">XS-<xsl:choose>
												<xsl:when test="xxl !=''">XXL</xsl:when>
												<xsl:when test="xl !=''">XL</xsl:when>
												<xsl:when test="l !=''">L</xsl:when>
												<xsl:when test="m !=''">M</xsl:when>
												<xsl:otherwise>S</xsl:otherwise>
											</xsl:choose>
										</td>
									</tr>
									
									<tr>
										<td colspan="2" align="left"><a href="?{$controller_name}.object&amp;id={id}&amp;back_page={$page}"><xsl:value-of select="$locale/shop/list/details" disable-output-escaping="yes"/></a></td>
									</tr>
									
									<tr>
										<td colspan="2" align="left" style="padding-top:1px;padding-bottom:3px"><xsl:value-of select="title" disable-output-escaping="yes"/></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
</xsl:template>

<xsl:template match="rest_fair_objects_prev/object">
<xsl:choose>
	<xsl:when test="thumbnail != ''">
		<a class="highslide" href="?{$controller_name}.display_photo&amp;id={id}&amp;type=1" onclick="return hs.expand(this, {{ slideshowGroup: 1, captionText: '{client/firstname} {client/lastname}: {title}'}} )"><img src="?{$controller_name}.display_photo&amp;id={id}" width="{thumbnail_w}" height="{thumbnail_h}" border="0"/></a>
	</xsl:when>
</xsl:choose>
</xsl:template>

<xsl:template match="rest_fair_objects/object">
<xsl:choose>
	<xsl:when test="thumbnail != ''">
		<a class="highslide" href="?{$controller_name}.display_photo&amp;id={id}&amp;type=1" onclick="return hs.expand(this, {{ slideshowGroup: 1, captionText: '{client/firstname} {client/lastname}: {title}'}} )"><img src="?{$controller_name}.display_photo&amp;id={id}" width="{thumbnail_w}" height="{thumbnail_h}" border="0"/></a>
	</xsl:when>
</xsl:choose>
</xsl:template>

</xsl:stylesheet>

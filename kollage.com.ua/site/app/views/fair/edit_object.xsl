<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
                
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout_fair.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:template match="data">
    <h3 class="page_title">
        <xsl:value-of select="$locale/my/fair/title/text()"/> (<xsl:value-of select="/content/global/client/email"/>)&#160;&#160;&#160;&#160;
        <input type="button" value="{$locale/my/fair/return_to_list/text()}" class="button" onclick="document.location='?{$controller_name}.my_fair&amp;page={$page}'"/>
    </h3>
    
    <xsl:if test="/content/global/client/id = 0">
        <xsl:call-template name="login_form2"/>
    </xsl:if>
    
    <xsl:if test="/content/global/client/id > 0 and /content/global/client/allow_fair = 1">
    
        <script language="JavaScript" src="{$views_path}/{$controller_name}/functions2.js"></script>
        
        <xsl:if test="error">
            <div><xsl:value-of select="error" disable-output-escaping="yes"/></div>
        </xsl:if>
        
        <form method="post" action="?{$controller_name}.save_object">
        <input type="hidden" name="id" value="{object/id}"/>
        <input type="hidden" name="page" value="{$page}"/>
        <input type="hidden" name="object[id]" value="{object/id}"/>
        <input type="hidden" name="object[lang]" value="{$lang}"/>
        <table cellpadding="3" cellspacing="1">
            
            
            <tr>
                <td valign="top">
                    <img src="?{$controller_name}.display_photo&amp;id={object/id}&amp;type=1" border="0" width="{object/preview_w}" height="{object/preview_h}"/>
                    <div style="margin-top:10px">
                        <table class="trash_list" width="100%">
                            <tr>
                                <th colspan="2"><xsl:value-of select="$locale/my/fair/edit/documents/title/text()" disable-output-escaping="yes"/></th>
                            </tr>
                            
                            <xsl:choose>
                                <xsl:when test="count(object/documents/fair_document) > 0">
                                    <xsl:apply-templates select="object/documents/fair_document"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <tr><td colspan="2" align="center"><xsl:value-of select="$locale/my/fair/edit/documents/no_documents/text()" disable-output-escaping="yes"/></td></tr>
                                </xsl:otherwise>
                            </xsl:choose>
                            
                        </table>
                        
                        <div style="margin-top:3px"><input type="button" class="button" value="{$locale/my/fair/edit/documents/add_del/text()}" onclick="ShowDocumentsForm('{object/id}')"/></div>
                    </div>
                </td>
                <td valign="top">
                    <table width="350px">
                        <tr>
                            <td><xsl:value-of select="$locale/my/fair/edit/title/text()" disable-output-escaping="yes"/>*</td>
                            <td><input type="text" name="object[title]" id="title" class="inputbox_middle2" value="{object/title}"/></td>
                        </tr>
                        
                        <tr>
                            <td><xsl:value-of select="$locale/my/fair/edit/category1/text()" disable-output-escaping="yes"/>*</td>
                            <td>
                                <select name="category_id1" id="category_id1" onchange="FillSubCategoriesBox()"></select><br/>
                                <select name="category_id2" id="category_id2" onchange="FillSubSubCategoriesBox()"></select><br/>
                                <select name="category_id3" id="category_id3"></select>
                            </td>
                        </tr>
                        
                        <tr><td colspan="2"><xsl:value-of select="$locale/my/fair/edit/description/text()" disable-output-escaping="yes"/>*</td></tr>
                        <tr>
                            <td colspan="2"><textarea cols="30" rows="3" name="object[description]" id="description" style="width:335px;height:80px"><xsl:value-of select="object/description"/></textarea></td>
                        </tr>
                        
                        <tr><td colspan="2"><xsl:value-of select="$locale/my/fair/edit/keywords/text()" disable-output-escaping="yes"/>&#160;*</td></tr>
                        <tr><td colspan="2"><input type="text" name="object[keywords]" id="keywords" class="inputbox_middle2" style="width:335px" value="{object/keywords}"/></td></tr>
                        
                        <!--
                        <tr>
                            <td colspan="2"><xsl:value-of select="$locale/my/fair/edit/with_processing/text()" disable-output-escaping="yes"/>&#160;&#160;&#160;
                                <xsl:choose>
                                    <xsl:when test="object/with_processing = 1"><input type="radio" name="object[with_processing]" id="with_processing_active" value="1" checked="yes"/></xsl:when>
                                    <xsl:otherwise><input type="radio" name="object[with_processing]" id="with_processing_active" value="1" /></xsl:otherwise>
                                </xsl:choose>
                                <label for="with_processing_active" class="lower"><xsl:value-of select="$locale/common/yes/text()"/></label>
                                <xsl:choose>
                                    <xsl:when test="object/with_processing = 0 or object/with_processing = ''"><input type="radio" name="object[with_processing]" id="with_processing_inactive" value="0" checked="yes"/></xsl:when>
                                    <xsl:otherwise><input type="radio" name="object[with_processing]" id="with_processing_inactive" value="0"/></xsl:otherwise>
                                </xsl:choose>
                                <label for="with_processing_inactive" class="lower"><xsl:value-of select="$locale/common/no/text()"/></label>
                            </td>
                        </tr>
                        -->
                        
                        <tr>
                            <td colspan="2"><xsl:value-of select="$locale/my/fair/edit/license/text()" disable-output-escaping="yes"/>&#160;&#160;&#160;
                                <xsl:choose>
                                    <xsl:when test="object/license = 1"><input type="radio" name="object[license]" id="license_active" value="1" checked="yes"/></xsl:when>
                                    <xsl:otherwise><input type="radio" name="object[license]" id="license_active" value="1" /></xsl:otherwise>
                                </xsl:choose>
                                <label for="license_active" class="lower"><xsl:value-of select="$locale/common/yes/text()"/></label>
                                <xsl:choose>
                                    <xsl:when test="object/license = 0 or object/license = ''"><input type="radio" name="object[license]" id="license_inactive" value="0" checked="yes"/></xsl:when>
                                    <xsl:otherwise><input type="radio" name="object[license]" id="license_inactive" value="0"/></xsl:otherwise>
                                </xsl:choose>
                                <label for="license_inactive" class="lower"><xsl:value-of select="$locale/common/no/text()"/></label>
                            </td>
                        </tr>
                        
                        <tr>
                            <td colspan="2"><xsl:value-of select="$locale/my/fair/edit/license_price/text()" disable-output-escaping="yes"/>&#160;&#160;&#160;
                            <input type="text" name="object[license_price]" id="license_price" class="inputbox_small2" value="{object/license_price}"/></td>
                        </tr>
            
                        <tr><td colspan="2">
                            <table class="trash_list" width="335px">
                                <tr>
                                    <th><xsl:value-of select="$locale/my/fair/edit/size/text()" disable-output-escaping="yes"/></th>
                                    <th><xsl:value-of select="$locale/my/fair/edit/price/text()" disable-output-escaping="yes"/></th>
                                </tr>
                                
                                <xsl:if test="object/xs !=''">
                                <tr>
                                    <td>
                                        <table cellspacing="0" cellpadding="0" width="100%"><tr>
                                            <td>XS (<xsl:value-of select="object/xs_w"/>x<xsl:value-of select="object/xs_h"/>px)</td>
                                            <td align="right"><a href="#" onclick="document.location='?{$controller_name}.download_my_photo&amp;id={object/id}&amp;s=1';return(false)"><xsl:value-of select="$locale/my/fair/show/download"/></a></td>
                                        </tr></table>
                                    </td>
                                    <td align="center"><input type="text" name="object[xs_price]" id="xs_price" class="inputbox_small2" value="{object/xs_price}"/></td>
                                </tr>
                                </xsl:if>
                                
                                <xsl:if test="object/s !=''">
                                <tr>
                                    <td>
                                        <table cellspacing="0" cellpadding="0" width="100%"><tr>
                                            <td>S (<xsl:value-of select="object/s_w"/>x<xsl:value-of select="object/s_h"/>px)</td>
                                            <td align="right"><a href="#" onclick="document.location='?{$controller_name}.download_my_photo&amp;id={object/id}&amp;s=2';return(false)"><xsl:value-of select="$locale/my/fair/show/download"/></a></td>
                                        </tr></table>
                                    </td>
                                    <td align="center"><input type="text" name="object[s_price]" id="s_price" class="inputbox_small2" value="{object/s_price}"/></td>
                                </tr>
                                </xsl:if>
                                
                                <xsl:if test="object/m !=''">
                                <tr>
                                    <td>
                                        <table cellspacing="0" cellpadding="0" width="100%"><tr>
                                            <td>M (<xsl:value-of select="object/m_w"/>x<xsl:value-of select="object/m_h"/>px)</td>
                                            <td align="right"><a href="#" onclick="document.location='?{$controller_name}.download_my_photo&amp;id={object/id}&amp;s=3';return(false)"><xsl:value-of select="$locale/my/fair/show/download"/></a></td>
                                        </tr></table>
                                    </td>
                                    <td align="center"><input type="text" name="object[m_price]" id="m_price" class="inputbox_small2" value="{object/m_price}"/></td>
                                </tr>
                                </xsl:if>
                                
                                <xsl:if test="object/l !=''">
                                <tr>
                                    <td>
                                        <table cellspacing="0" cellpadding="0" width="100%"><tr>
                                            <td>L (<xsl:value-of select="object/l_w"/>x<xsl:value-of select="object/l_h"/>px)</td>
                                            <td align="right"><a href="#" onclick="document.location='?{$controller_name}.download_my_photo&amp;id={object/id}&amp;s=4';return(false)"><xsl:value-of select="$locale/my/fair/show/download"/></a></td>
                                        </tr></table>
                                    </td>
                                    <td align="center"><input type="text" name="object[l_price]" id="l_price" class="inputbox_small2" value="{object/l_price}"/></td>
                                </tr>
                                </xsl:if>
                                
                                <xsl:if test="object/xl !=''">
                                <tr>
                                    <td>
                                        <table cellspacing="0" cellpadding="0" width="100%"><tr>
                                            <td>XL (<xsl:value-of select="object/xl_w"/>x<xsl:value-of select="object/xl_h"/>px)</td>
                                            <td align="right"><a href="#" onclick="document.location='?{$controller_name}.download_my_photo&amp;id={object/id}&amp;s=5';return(false)"><xsl:value-of select="$locale/my/fair/show/download"/></a></td>
                                        </tr></table>
                                    </td>
                                    <td align="center"><input type="text" name="object[xl_price]" id="xl_price" class="inputbox_small2" value="{object/xl_price}"/></td>
                                </tr>
                                </xsl:if>
                                
                                <xsl:if test="object/xxl !=''">
                                <tr>
                                    <td>
                                        <table cellspacing="0" cellpadding="0" width="100%"><tr>
                                            <td>XXL (<xsl:value-of select="object/xxl_w"/>x<xsl:value-of select="object/xxl_h"/>px)</td>
                                            <td align="right"><a href="#" onclick="document.location='?{$controller_name}.download_my_photo&amp;id={object/id}&amp;s=6';return(false)"><xsl:value-of select="$locale/my/fair/show/download"/></a></td>
                                        </tr></table>
                                        </td>
                                    <td align="center"><input type="text" name="object[xxl_price]" id="xxl_price" class="inputbox_small2" value="{object/xxl_price}"/></td>
                                </tr>
                                </xsl:if>
                                
                            </table>
                        </td></tr>
            
                        <tr><td colspan="2">
                            <input type="submit" value="{$locale/my/fair/edit/submit/text()}" class="button"/>
                        </td></tr>
                    </table>
                </td>
            </tr>
            
        </table>
        </form>
        <script>
        var categories = <xsl:value-of select="categories_js"/>;
        function FillCategoriesBox()
        {
            var i=0, category_id1;
            category_id1 = $('category_id1');
            category_id1.options.length = 0;
            for( i=0; i &lt; categories.length; i++ )
            {
                category_id1.options[i] = new Option( categories[i][1], categories[i][0] );
            }
        }
        function FillSubCategoriesBox()
        {
            var parent_id, parent, i=0, category_id1, category_id2, category_id3;
            category_id1 = $('category_id1'); category_id2 = $('category_id2'); category_id3 = $('category_id3');
            category_id2.options.length = 0;
            parent_id = category_id1.options[category_id1.selectedIndex].value;
            parent = FindIndexByValue( categories, parent_id );
            for( i=0; i &lt; categories[parent][2].length; i++ )
            {
                category_id2.options[i] = new Option( categories[parent][2][i][1], categories[parent][2][i][0] );
            }
            FillSubSubCategoriesBox();
        }
        function FillSubSubCategoriesBox()
        {
            var parent_id, parent, parent_id2, parent2, i=0, category_id1, category_id2, category_id3;
            category_id1 = $('category_id1'); category_id2 = $('category_id2'); category_id3 = $('category_id3');
            category_id3.options.length = 0;
            parent_id = category_id1.options[category_id1.selectedIndex].value;
            parent = FindIndexByValue( categories, parent_id );
            parent_id2 = category_id2.options[category_id2.selectedIndex].value;
            parent2 = FindIndexByValue( categories[parent][2], parent_id2 );
            if( categories[parent][2][parent2][2].length > 0 )
            {
                category_id3.style.visibility = '';
                for( i=0; i &lt; categories[parent][2][parent2][2].length; i++ )
                {
                    category_id3.options[i] = new Option( categories[parent][2][parent2][2][i][1], categories[parent][2][parent2][2][i][0] );
                }
            }
            else
            {
                category_id3.style.visibility = 'hidden';
            }
        }
        function FindIndexByValue( arr, val )
        {
            var i = 0, retval = 0;
            for( i; i &lt; arr.length; i++ )
            {
                if( arr[i][0] == val )
                {
                    retval = i;
                    break;
                }
            }
            return( retval );
        }
        FillCategoriesBox();
        FillSubCategoriesBox();
        FillSubSubCategoriesBox();
        <xsl:if test="object/category_id1 > 0">
        var categories_line = '<xsl:value-of select="categories_line"/>';
        var cl_arr = categories_line.split( ',');
        if( cl_arr.length > 0 )
        {
            var i = cl_arr.length - 1;
            var category_id1, category_id2, category_id3;
            category_id1 = $('category_id1'); category_id2 = $('category_id2'); category_id3 = $('category_id3');
            var ob = 1;
            for( i; i>=0; i-- )
            {
                if( ob == 1 )
                {
                    select_select( category_id1, cl_arr[i] );
                    FillSubCategoriesBox();
                }
                else if( ob == 2 )
                {
                    select_select( category_id2, cl_arr[i] );
                    FillSubSubCategoriesBox();
                }
                else if( ob == 3 ) select_select( category_id3, cl_arr[i] );
                ob++;
            }
        }
        </xsl:if>
        </script>
        
    </xsl:if>
    
</xsl:template>

<xsl:template match="fair_document">
    <tr>
        <td><a href="{$fair_documents_path}/{filename}" target="fd_{id}"><xsl:value-of select="name"/></a></td>
        <td><xsl:value-of select="description"/></td>
    </tr>
</xsl:template>

</xsl:stylesheet>

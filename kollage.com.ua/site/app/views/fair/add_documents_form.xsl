<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../login_form.xsl" />
<xsl:include href="../login_form2.xsl" />

<xsl:template match="/">
    <table cellpadding="10" cellspacing="0" class="content_table" width="100%" height="100%">
        <tr>
            <td colspan="2" height="24">
                <div class="page_title">
                <table cellspacing="0" cellpadding="0" width="100%"><tr>
                    <td align="left"><div class="title"><xsl:value-of select="$locale/my/fair/documents/title/text()"/></div></td>
                    <td align="right"><input type="button" class="button" id="btn_close" value="{$locale/photos/order/add_photos/button_close/text()}" onclick="CloseDocumentsForm()"/></td>
                </tr></table>
                </div>
            </td>
        </tr>
        <tr>
            <td valign="top" colspan="2">
                <table width="100%" cellspacing="0" cellpadding="0">
                    <tr>
                        <td><b>&#160;#</b></td>
                        <td><b><xsl:value-of select="$locale/my/fair/documents/list/name"/></b></td>
                        <td><b><xsl:value-of select="$locale/my/fair/documents/list/description"/></b></td>
                        <td width="2%">&#160;</td>
                    </tr>
                    
                    <xsl:apply-templates select="/content/documents/data/fair_documents/item"/>
                    
                    <tr><td colspan="4"><div class="page_title"><p class="help"><i><xsl:value-of select="$locale/my/fair/documents/list/help"/></i></p></div></td></tr>
                    
                    <form name="add_documents_form" id="add_documents_form" method="post" action="?{$controller_name}.add_objects" enctype="multipart/form-data">
                    <tr>
                        <td colspan="4">
                            <div><b><xsl:value-of select="$locale/my/fair/documents/list/add_new"/></b></div>
                            <table>
                                <tr>
                                    <td><xsl:value-of select="$locale/my/fair/documents/list/name"/></td>
                                    <td><input type="text" class="inputbox_middle2" name="name" id="name"/></td>
                                </tr>
                                <tr>
                                    <td><xsl:value-of select="$locale/my/fair/documents/list/description"/></td>
                                    <td><textarea name="description" id="description" rows="5" cols="50"></textarea></td>
                                </tr>
                                <tr>
                                    <td><xsl:value-of select="$locale/my/fair/documents/list/file"/></td>
                                    <td>
                                        <input type="file" name="file" id="file"/>&#160;
                                        <input type="hidden" name="object_id" id="object_id" value="{/content/documents/data/fair_documents/@fair_object_id}"/>
                                        <input type="button" class="button" id="btn_submit" value="{$locale/photos/order/add_photos/button_upload/text()}" onclick="UploadDocument()"/>&#160;
                                        <span style="padding:left:20px;visibility:hidden" name="wait_img" id="wait_img">&#160;<img src="{$views_path}/images/progress.gif" border="0"/></span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    </form>
                    
                </table>
            </td>
        </tr>
    </table>
</xsl:template>

<xsl:template match="item">
    <tr>
        <td><input type="checkbox" onclick="BelongDocument(this,'{@fair_object_id}','{fair_document/id}')">
            <xsl:if test="@selected = 1"><xsl:attribute name="checked"/></xsl:if>
        </input></td>
        <td><xsl:value-of select="fair_document/name"/></td>
        <td><xsl:value-of select="fair_document/description"/></td>
        <td><a title="{$locale/common/del/text()}" href="#" onclick="del_document('?{$controller_name}.del_document&amp;id={fair_document/id}&amp;object_id={@fair_object_id}','{$locale/common/msgs/delete_item/text()}');return(false)"><img  src="{$views_path}/images/del.gif" border="0"/></a></td>
    </tr>
</xsl:template>

</xsl:stylesheet>

<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
                
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout_fair.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:template match="data">
    <h3 class="page_title"><xsl:value-of select="article/header"/></h3>
    <xsl:value-of select="article/text_transformed" disable-output-escaping="yes"/>
</xsl:template>

</xsl:stylesheet>

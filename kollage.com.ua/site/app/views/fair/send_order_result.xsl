<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
                
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout_fair.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:template match="data">
    <h3 class="page_title"><xsl:value-of select="$locale/fair/title_order/text()"/> <xsl:if test="/content/global/client/email != ''">&#160;&#160;(<xsl:value-of select="/content/global/client/email"/>)</xsl:if></h3>
    
    <xsl:if test="/content/global/client/id = 0">
        <xsl:call-template name="login_form3"/>
    </xsl:if>
    
    <xsl:if test="/content/global/client/id > 0 and /content/global/client/allow_fair != 1">
    
        <script language="JavaScript" src="{$views_path}/{$controller_name}/functions.js"></script>
        
        <div class="msg"><xsl:value-of select="$locale/fair/send_order_result" disable-output-escaping="yes"/></div>
        
    </xsl:if>

</xsl:template>

</xsl:stylesheet>

﻿
var ss_ids = [];
var ss_i = 0;
var ss_timer = null;
var ss_interval_default = 6000;
var ss_interval = ss_interval_default;

function ShowSSForm( category_id, search_str )
{
    ss_i = 0; //start from the first
    ss_interval = ss_interval_default;
    ss_ids.length = 0;
    
    var w = 440, h = 540;
    obj = new Element( 'div', {'id':"ss_layout",'style':"position:absolute;z-index:200;top:0;left:0;width:"+ w +"px;height:"+ h +"px;overflow:auto;border:2px solid #333333;background-color:white"});
    bg = new Element( 'div', {'id':"ss_bg",'class':'shadow1','style':"position:absolute;z-index:50;top:-1;left:-1px;width:1px;height:1px"} );
    obj.innerHTML = 'Загрузка...';
    Element.insert( GetFirstNode(GetBody()), bg );
    Element.insert( GetFirstNode(GetBody()), obj );
    if( obj != null )
    {
        obj.style.left = (GetWindowSize()[0] - w) / 2;
        obj.style.top = GetWindowScroll()[1] + (GetWindowSize()[1] - h) / 2;
        obj.style.width = w + 'px';
        obj.style.height = h + 'px';
        
        bg.style.top = '0px'; bg.style.left = '0px';
        bg.style.width = GetObjectSize(document.body)[0];
        bg.style.height = GetObjectSize(document.body)[1];
        
        var d = new Date();
        JsHttpRequest.query(
            '?fair.slideshow&date='+ d.getTime(),
            {
                category_id : category_id,
                search_str : search_str
            },
            function( result, txt )
            {
                $('ss_layout').innerHTML = txt;
                ss_ids = result['ids'];
                SSShowImage(); 
            }
        );
    }
}

function CloseSSForm()
{
    var obj, bg, i;
    
    Element.remove('ss_layout');
    Element.remove('ss_bg');
    
    SSStop();
}

function SSStop()
{
    if( ss_timer != null ) clearTimeout( ss_timer ); ss_timer = null;
}

function SSShowImage()
{
    if( ss_timer != null ) clearTimeout( ss_timer );
    if( ss_i >= ss_ids.length ) ss_i = 0;
    if( ss_ids.length > ss_i )
    {
        var img = new Image();
        img.onload = function()
        {
            $('ss_img').src = this.src;
            $('ss_img').hide();
            Effect.Appear( 'ss_img' );
            $('ss_img').appear();
            SSSetTitle( ss_ids[ss_i] );
            ss_i++;
            ss_timer = setTimeout( SSShowImage, ss_interval );
        }
        img.src = "?fair.display_photo&id="+ ss_ids[ss_i] +"&type=1";
        
    }
    else
    {
        $('ss_img').src = "../app/views/images/empty.gif";
    }
}

function SSSetTitle( id )
{
    $('title').innerHTML = '';
    if( id > 0 )
    {
        var d = new Date();
        JsHttpRequest.query(
            '?fair.getphotoinfo&date='+ d.getTime(),
            {
                id : id
            },
            function( result, txt )
            {
                $('title').innerHTML = '#'+ id +' '+ result['owner'];
            }
        );
    }
}


<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
                
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout_fair.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:template match="data">

    <h1 class="fair_title">
        <table cellspacing="0" cellpadding="0" width="100%"><tr>
        <td width="90%"><xsl:value-of select="$locale/fair/title/text()" disable-output-escaping="yes"/></td>
        
        <td width="*" align="right">
            <xsl:call-template name="fair_trash">
                <xsl:with-param name="back_url" select="back_url"/>
            </xsl:call-template>
        </td>
        </tr></table>
        
    </h1>
    
    <xsl:if test="count(fair_objects/*) > 6">
        <script language="JavaScript" src="{$views_path}/{$controller_name}/slideshow.js"></script>
        <div style="clear:left;float:left;padding-top: 10px;padding-bottom: 10px;"><a href="#" onclick="ShowSSForm('','{str}');return(false)"><xsl:value-of select="$locale/fair/slideshow_link/text()" disable-output-escaping="yes"/></a></div>
    </xsl:if>
    
    <!-- PAGINATOR -->
    <div class="pagination" pages="{$pages}"><nobr>
    <xsl:if test="$pages > 1">
    
        <xsl:call-template name="pagination">
            <xsl:with-param name="page"><xsl:value-of select="$page"/></xsl:with-param>
            <xsl:with-param name="stop"><xsl:value-of select="$pages"/></xsl:with-param>
            <xsl:with-param name="url">?<xsl:value-of select="$controller_name"/>.search&amp;str=<xsl:value-of select="str"/></xsl:with-param>
        </xsl:call-template>
        / <a href="?{$controller_name}.search&amp;str={str}&amp;page={$page - 1}"><xsl:value-of select="$locale/common/link_prev/text()" disable-output-escaping="yes"/></a> | <a href="?{$controller_name}.search&amp;str={str}&amp;page={$page + 1}"><xsl:value-of select="$locale/common/link_next/text()" disable-output-escaping="yes"/></a>
        
    </xsl:if>
    </nobr></div>
    <!-- PAGINATOR - END -->
    
    <div class="matrix"><xsl:apply-templates select="fair_objects/*"/></div>
    
    <!-- PAGINATOR -->
    <div class="pagination" pages="{$pages}"><nobr>
    <xsl:if test="$pages > 1">
    
        <xsl:call-template name="pagination">
            <xsl:with-param name="page"><xsl:value-of select="$page"/></xsl:with-param>
            <xsl:with-param name="stop"><xsl:value-of select="$pages"/></xsl:with-param>
            <xsl:with-param name="url">?<xsl:value-of select="$controller_name"/>.search&amp;str=<xsl:value-of select="str"/></xsl:with-param>
        </xsl:call-template>
        / <a href="?{$controller_name}.search&amp;str={str}&amp;page={$page - 1}"><xsl:value-of select="$locale/common/link_prev/text()" disable-output-escaping="yes"/></a> | <a href="?{$controller_name}.search&amp;str={str}&amp;page={$page + 1}"><xsl:value-of select="$locale/common/link_next/text()" disable-output-escaping="yes"/></a>
        
    </xsl:if>
    </nobr></div>
    <!-- PAGINATOR - END -->
</xsl:template>

<xsl:template match="object">
    <div class="fair_cell">
        <table width="100%" height="100%">
            <tr>
                <td class="product" valign="top">
                    <table cellspacing="0" cellpadding="0" width="100%" height="100%">
                        <tr>
                            <td height="10%" align="center">
                                <xsl:choose>
                                    <xsl:when test="thumbnail != ''">
                                        <a href="#" onclick="PreviewImage(event,'?{$controller_name}.display_photo&amp;id={id}&amp;type=1','{preview_w}','{preview_h}','{$locale/photos/order/zoom2/text()}','{$locale/photos/order/zoom_logo2/text()}');return(false)"><img src="?{$controller_name}.display_photo&amp;id={id}" width="{thumbnail_w}" height="{thumbnail_h}" border="0"/></a>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <img src="{$views_path}/images/empty.gif" width="180" height="124" border="0"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </td>
                        </tr>
                        <tr>
                            <td height="*" valign="top">
                                <table width="100%" cellpadding="0" cellspacing="0" class="product_info" border="0">
                                    <tr>
                                        <td align="left">#<xsl:value-of select="id" disable-output-escaping="yes"/></td>
                                        <td align="right">XS-<xsl:choose>
                                                <xsl:when test="xxl !=''">XXL</xsl:when>
                                                <xsl:when test="xl !=''">XL</xsl:when>
                                                <xsl:when test="l !=''">L</xsl:when>
                                                <xsl:when test="m !=''">M</xsl:when>
                                                <xsl:otherwise>S</xsl:otherwise>
                                            </xsl:choose>
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td colspan="2" align="left"><a href="?{$controller_name}.object&amp;id={id}&amp;back_page={$page}"><xsl:value-of select="$locale/shop/list/details" disable-output-escaping="yes"/></a></td>
                                    </tr>
                                    
                                    <tr>
                                        <td colspan="2" align="left" style="padding-top:1px;padding-bottom:3px"><xsl:value-of select="title" disable-output-escaping="yes"/></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</xsl:template>

</xsl:stylesheet>

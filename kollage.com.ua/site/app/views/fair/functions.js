﻿
function ShowZoomRect()
{
    Element.show('zoom');
}

function HideZoomRect()
{
    Element.hide('zoom');
}

function MoveZoomRect( obj, e )
{
    var xCord;
    var yCord;
    
    if( document.captureEvents )
    {
        xCord = e.pageX;
        yCord = e.pageY;
    }
    else if( window.event.clientX )
    {
        xCord = window.event.clientX + document.documentElement.scrollLeft;
        yCord = window.event.clientY + document.documentElement.scrollTop;
    }
    
    var size_zoom = GetObjectVisibleSize( 'zoom' );
    var size_cont = GetObjectVisibleSize( obj );
    var scroll = GetWindowScroll();

    var left = Math.ceil(xCord - obj.offsetLeft + scroll[0] - size_zoom[0] / 2);
    var top = Math.ceil(yCord - obj.offsetTop + scroll[1] - size_zoom[1] / 2);

    if (left < 0) {
        left = 0;
    } else if (left >= (size_cont[0] - size_zoom[0])) {
        left = size_cont[0] - size_zoom[0];
    }

    if (top < 0) {
        top = 0;
    } else if (top >= (size_cont[1] - size_zoom[1])) {
        top = size_cont[1] - size_zoom[1];
    }

    if (isNaN(top)) {
        top = 0;
    }

    if (isNaN(left)) {
        left = 0;
    }

    Element.setStyle(
        'zoom',
        {
            top: top + 'px',
            left: left + 'px'
        }
    );
}

function ShowZoomed( obj, object_id )
{
    if( Element.getStyle('zoomed','display') == 'none' )
    {
        var left = $('zoom').offsetLeft;
        var top = $('zoom').offsetTop;
        
        var img = new Image();
        $('zoomed_image').src = "../app/views/images/progress.gif";
        img.onload = function()
        {
            $('zoomed_image').src = this.src;
        }
        img.src = "?fair.zoom_preview&x="+ String(left) +"&y="+ String(top) +"&id="+ String(object_id);
        Element.show( 'zoomed' );
    }
    else
    {
        Element.hide( 'zoomed' );
    }
}

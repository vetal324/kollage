<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
                
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout_fair.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:template match="data">
    <script language="JavaScript" src="{$views_path}/{$controller_name}/functions.js"></script>
    
    <xsl:variable name="back_page" select="back_page"/>
    
    <h1 class="products_title">
        <table cellspacing="0" cellpadding="0" with="100%"><tr>
        <td width="90%">
            <xsl:for-each select="object/categories/*"><a href="?{$controller_name}.show&amp;category_id={id}&amp;page={$back_page}"><xsl:value-of select="name"/></a>&#160;&#187;&#160;</xsl:for-each><xsl:value-of select="object/title"/>
        </td>
        <td width="*" align="right">
            <xsl:call-template name="fair_trash">
                <xsl:with-param name="back_url" select="back_url"/>
            </xsl:call-template>
        </td>
        </tr></table>
        
    </h1>
    
    <table cellpadding="3" cellspacing="0" width="100%" border="0">
        <tr>
            <td width="420px" align="center" valign="top">
                <div id="preview" onclick="ShowZoomed(this,{object/id})" onmousemove="MoveZoomRect(this,event)" onmouseover="ShowZoomRect(this)" onmouseout="HideZoomRect(this)" style="position:relative;overflow:hidden;width:{object/preview_w}px;height:{object/preview_h}px">
                    <img src="?{$controller_name}.display_photo&amp;id={object/id}&amp;type=1" width="{object/preview_w}" height="{object/preview_h}" border="0"/>
                    <div id="zoom" style="z-index:101;position:absolute;left:0px;top:0px;width:{object/preview_w div 3}px;height:{object/preview_h div 3};border:1px solid #777777"></div>
                    <div id="zoomed" style="z-index:102;position:absolute;left:0px;top:0px;width:{object/preview_w}px;height:{object/preview_h}"><img id="zoomed_image" src="/empty.gif" border="0" width="{object/preview_w}" height="{object/preview_h}"/></div>
                </div>
                <xsl:value-of select="object/description" disable-output-escaping="yes"/>
                
                <div style="margin-top:10px">
                    <table class="trash_list" width="400">
                        <tr>
                            <th colspan="2"><xsl:value-of select="$locale/my/fair/edit/documents/title/text()" disable-output-escaping="yes"/></th>
                        </tr>
                        
                        <xsl:choose>
                            <xsl:when test="count(object/documents/fair_document) > 0">
                                <xsl:apply-templates select="object/documents/fair_document"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <tr><td colspan="2" align="center"><xsl:value-of select="$locale/my/fair/edit/documents/no_documents/text()" disable-output-escaping="yes"/></td></tr>
                            </xsl:otherwise>
                        </xsl:choose>
                        
                    </table>
                </div>
            </td>
            <td align="left" valign="top">
                
                <div style="margin-bottom: 5px;"><b>#<xsl:value-of select="object/id"/>, <xsl:value-of select="$locale/my/fair/show/owner"/>:&#160;<xsl:value-of select="object/client/firstname"/>&#160;<xsl:value-of select="object/client/lastname"/></b></div>
                <!--
                <div style="margin-bottom: 5px;">
                    <xsl:choose>
                        <xsl:when test="object/with_processing = 1"><xsl:value-of select="$locale/my/fair/show/with_processing"/></xsl:when>
                        <xsl:otherwise><xsl:value-of select="$locale/my/fair/show/without_processing"/></xsl:otherwise>
                    </xsl:choose>
                </div>
                -->
                
                <div style="padding-top:10px;padding-bottom:5px"><a href="?{$controller_name}.show_article&amp;id=219" target="standard_licence"><xsl:value-of select="$locale/fair/standard_licence"/></a></div>
                <table class="trash_list" width="100%">
                    <tr>
                        <th>&#160;</th>
                        <th><xsl:value-of select="$locale/my/fair/show/size/text()" disable-output-escaping="yes"/></th>
                        <th><xsl:value-of select="$locale/my/fair/show/mp/text()" disable-output-escaping="yes"/></th>
                        <th><xsl:value-of select="$locale/my/fair/show/price/text()" disable-output-escaping="yes"/></th>
                        <th>&#160;</th>
                    </tr>
                    
                    <xsl:if test="object/xs !=''">
                    <tr>
                        <td width="25">XS</td>
                        <td align="center"><xsl:value-of select="object/xs_w"/>x<xsl:value-of select="object/xs_h"/></td>
                        <td align="center"><xsl:value-of select="format-number(number(object/xs_w * object/xs_h) div 1000000, '0.00')"/></td>
                        <td align="center"><xsl:value-of select="object/xs_price"/></td>
                        <td align="center">
                            <xsl:if test="number(object/xs_price) > 0"><a href="#" onclick="return(false)"><xsl:value-of select="$locale/my/fair/show/buy"/></a></xsl:if>
                            <xsl:if test="number(object/xs_price) = 0"><a href="#" onclick="document.location='?{$controller_name}.download_free_photo&amp;id={object/id}&amp;s=1';return(false)"><xsl:value-of select="$locale/my/fair/show/download"/></a></xsl:if>
                        </td>
                    </tr>
                    </xsl:if>
                    
                    <xsl:if test="object/s !=''">
                    <tr>
                        <td width="25">S</td>
                        <td align="center"><xsl:value-of select="object/s_w"/>x<xsl:value-of select="object/s_h"/></td>
                        <td align="center"><xsl:value-of select="format-number(number(object/s_w * object/s_h) div 1000000, '0.00')"/></td>
                        <td align="center"><xsl:value-of select="object/s_price"/></td>
                        <td align="center">
                            <xsl:if test="number(object/s_price) > 0"><a href="#" onclick="return(false)"><xsl:value-of select="$locale/my/fair/show/buy"/></a></xsl:if>
                            <xsl:if test="number(object/s_price) = 0"><a href="#" onclick="return(false)"><xsl:value-of select="$locale/my/fair/show/download"/></a></xsl:if>
                        </td>
                    </tr>
                    </xsl:if>
                    
                    <xsl:if test="object/m !=''">
                    <tr>
                        <td width="25">M</td>
                        <td align="center"><xsl:value-of select="object/m_w"/>x<xsl:value-of select="object/m_h"/></td>
                        <td align="center"><xsl:value-of select="format-number(number(object/m_w * object/m_h) div 1000000, '0.00')"/></td>
                        <td align="center"><xsl:value-of select="object/m_price"/></td>
                        <td align="center">
                            <xsl:if test="number(object/m_price) > 0"><a href="#" onclick="return(false)"><xsl:value-of select="$locale/my/fair/show/buy"/></a></xsl:if>
                            <xsl:if test="number(object/m_price) = 0"><a href="#" onclick="return(false)"><xsl:value-of select="$locale/my/fair/show/download"/></a></xsl:if>
                        </td>
                    </tr>
                    </xsl:if>
                    
                    <xsl:if test="object/l !=''">
                    <tr>
                        <td width="25">L</td>
                        <td align="center"><xsl:value-of select="object/l_w"/>x<xsl:value-of select="object/l_h"/></td>
                        <td align="center"><xsl:value-of select="format-number(number(object/l_w * object/l_h) div 1000000, '0.00')"/></td>
                        <td align="center"><xsl:value-of select="object/l_price"/></td>
                        <td align="center">
                            <xsl:if test="number(object/l_price) > 0"><a href="#" onclick="return(false)"><xsl:value-of select="$locale/my/fair/show/buy"/></a></xsl:if>
                            <xsl:if test="number(object/l_price) = 0"><a href="#" onclick="return(false)"><xsl:value-of select="$locale/my/fair/show/download"/></a></xsl:if>
                        </td>
                    </tr>
                    </xsl:if>
                    
                    <xsl:if test="object/xl !=''">
                    <tr>
                        <td width="25">XL</td>
                        <td align="center"><xsl:value-of select="object/xl_w"/>x<xsl:value-of select="object/xl_h"/></td>
                        <td align="center"><xsl:value-of select="format-number(number(object/xl_w * object/xl_h) div 1000000, '0.00')"/></td>
                        <td align="center"><xsl:value-of select="object/xl_price"/></td>
                        <td align="center">
                            <xsl:if test="number(object/xl_price) > 0"><a href="#" onclick="return(false)"><xsl:value-of select="$locale/my/fair/show/buy"/></a></xsl:if>
                            <xsl:if test="number(object/xl_price) = 0"><a href="#" onclick="return(false)"><xsl:value-of select="$locale/my/fair/show/download"/></a></xsl:if>
                        </td>
                    </tr>
                    </xsl:if>
                    
                    <xsl:if test="object/xxl !=''">
                    <tr>
                        <td width="25">XXL</td>
                        <td align="center"><xsl:value-of select="object/xxl_w"/>x<xsl:value-of select="object/xxl_h"/></td>
                        <td align="center"><xsl:value-of select="format-number(number(object/xxl_w * object/xxl_h) div 1000000, '0.00')"/></td>
                        <td align="center"><xsl:value-of select="object/xxl_price"/></td>
                        <td align="center">
                            <xsl:if test="number(object/xxl_price) > 0"><a href="#" onclick="return(false)"><xsl:value-of select="$locale/my/fair/show/buy"/></a></xsl:if>
                            <xsl:if test="number(object/xxl_price) = 0"><a href="#" onclick="return(false)"><xsl:value-of select="$locale/my/fair/show/download"/></a></xsl:if>
                        </td>
                    </tr>
                    </xsl:if>
                    
                </table>
                
                <xsl:if test="object/license = 1 and object/license_price > 0 and object/filename !=''">
                
                <div style="padding-top:10px;padding-bottom:5px"><a href="?{$controller_name}.show_article&amp;id=220" target="extended_licence"><xsl:value-of select="$locale/fair/extended_licence"/></a></div>
                <table class="trash_list" width="100%">
                
                    <tr>
                        <th>&#160;</th>
                        <th><xsl:value-of select="$locale/my/fair/show/size/text()" disable-output-escaping="yes"/></th>
                        <th><xsl:value-of select="$locale/my/fair/show/mp/text()" disable-output-escaping="yes"/></th>
                        <th><xsl:value-of select="$locale/my/fair/show/price/text()" disable-output-escaping="yes"/></th>
                        <th>&#160;</th>
                    </tr>
                
                    <tr>
                        <td width="25" style="background-color:#e8f27e" align="center">X</td>
                        <td align="center"><xsl:value-of select="object/filename_w"/>x<xsl:value-of select="object/filename_h"/></td>
                        <td align="center"><xsl:value-of select="format-number(number(object/filename_w * object/filename_h) div 1000000, '0.00')"/></td>
                        <td align="center"><xsl:value-of select="object/license_price"/></td>
                        <td align="center">
                            <xsl:if test="number(object/license_price) > 0"><a href="#" onclick="return(false)"><xsl:value-of select="$locale/my/fair/show/buy"/></a></xsl:if>
                        </td>
                    </tr>
                    
                </table>
                
                </xsl:if>
                
                <table cellspacing="0" cellpadding="0" style="margin-top:10px" width="100%">
                    <tr>
                        <td colspan="2"><div style="margin-bottom:5px"><u>Photo tecnical information</u></div></td>
                    </tr>
                    
                    <xsl:if test="object/photo_model != ''">
                        <tr>
                            <td>Maker</td>
                            <td><xsl:value-of select="object/photo_model"/>&#160;(<xsl:value-of select="object/photo_make"/>)</td>
                        </tr>
                    </xsl:if>
                    
                    <xsl:if test="object/photo_resolution != ''">
                        <tr>
                            <td>Resolution, dpi</td>
                            <td><xsl:value-of select="object/photo_resolution"/></td>
                        </tr>
                    </xsl:if>
                    
                    <xsl:if test="object/photo_iso != ''">
                        <tr>
                            <td>ISO</td>
                            <td><xsl:value-of select="object/photo_iso"/></td>
                        </tr>
                    </xsl:if>
                    
                    <xsl:if test="object/software != ''">
                        <tr>
                            <td>Software</td>
                            <td><xsl:value-of select="object/software"/></td>
                        </tr>
                    </xsl:if>
                    
                    <xsl:if test="object/photo_datetime_digitized != ''">
                        <tr>
                            <td>Datetime digitized</td>
                            <td><xsl:value-of select="object/photo_datetime_digitized"/></td>
                        </tr>
                    </xsl:if>
                </table>
                
            </td>
        </tr>
    </table>
    
    <script>
        Element.hide('zoom');
        Element.hide('zoomed');
    </script>
    
</xsl:template>

<xsl:template match="fair_document">
    <tr>
        <td><a href="{$fair_documents_path}/{filename}" target="fd_{id}"><xsl:value-of select="name"/></a></td>
        <td><xsl:value-of select="description"/></td>
    </tr>
</xsl:template>

</xsl:stylesheet>

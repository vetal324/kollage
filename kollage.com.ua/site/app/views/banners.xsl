<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" version="1.0">

<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:template match="banners_services/banner">
	<xsl:variable name="style">
		<xsl:if test="position() > 1">display:none;</xsl:if>
	</xsl:variable>
	
	<div class="banner_item" style="{$style}">
			<xsl:choose>
				<xsl:when test="image/@type = 104">
					<script type="text/javascript">
						swfobject.embedSWF('<xsl:value-of select="$data_path"/>/<xsl:value-of select="image"/>', 
							'banner_service<xsl:value-of select="id"/>', 
							'110', 
							'110', 
							'9.0.45', 
							'', 
							{}, 
							{bgcolor: '#ffffff', menu: 'false', wmode: 'opaque'}, 
							{id: 'banner_left<xsl:value-of select="id"/>'});
					</script>
					<div id="banner_left{id}">
						<p>����� ����������� ������ �������� ��� ������� ������ ����� ������������� Flash Player 9+!</p>
						<p>
							<a href="http://www.adobe.com/go/getflashplayer">
								<img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
							</a>
						</p>
					</div>
				</xsl:when>
				<xsl:otherwise>
					<xsl:choose>
						<xsl:when test="url != ''">
							<a href="{url}"><img src="{$data_path}/{image}" border="0" height="110"/></a>
						</xsl:when>
						<xsl:otherwise>
							<img src="{$data_path}/{image}" border="0" height="110"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
	</div>
</xsl:template>

<xsl:template match="banners_left">
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="banners_left/banner">
	<div class="banners_box_item">
			<xsl:choose>
				<xsl:when test="image/@type = 104">
					<script type="text/javascript">
						swfobject.embedSWF('<xsl:value-of select="$data_path"/>/<xsl:value-of select="image"/>', 
							'banner_left<xsl:value-of select="id"/>', 
							'162', 
							'162', 
							'9.0.45', 
							'', 
							{}, 
							{bgcolor: '#ffffff', menu: 'false', wmode: 'opaque'}, 
							{id: 'banner_left<xsl:value-of select="id"/>'});
					</script>
					<div id="banner_left{id}">
						<p>����� ����������� ������ �������� ��� ������� ������ ����� ������������� Flash Player 9+!</p>
						<p>
							<a href="http://www.adobe.com/go/getflashplayer">
								<img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
							</a>
						</p>
					</div>
				</xsl:when>
				<xsl:otherwise>
					<xsl:choose>
						<xsl:when test="url != ''">
							<a href="{url}"><img src="{$data_path}/{image}" border="0" width="162"/></a>
						</xsl:when>
						<xsl:otherwise>
							<img src="{$data_path}/{image}" border="0" width="162"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
	</div>
</xsl:template>

<xsl:template match="banners_right/banner">
	<div class="banners_box_item">
			<xsl:choose>
				<xsl:when test="image/@type = 104">
					<script type="text/javascript">
						swfobject.embedSWF('<xsl:value-of select="$data_path"/>/<xsl:value-of select="image"/>', 
							'banner_right<xsl:value-of select="id"/>', 
							'185', 
							'185', 
							'9.0.45', 
							'', 
							{}, 
							{bgcolor: '#ffffff', menu: 'false', wmode: 'opaque'}, 
							{id: 'banner_right<xsl:value-of select="id"/>'});
					</script>
					<div id="banner_right{id}">
						<p>����� ����������� ������ �������� ��� ������� ������ ����� ������������� Flash Player 9+!</p>
						<p>
							<a href="http://www.adobe.com/go/getflashplayer">
								<img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
							</a>
						</p>
					</div>
				</xsl:when>
				<xsl:otherwise>
					<xsl:choose>
						<xsl:when test="url != ''">
							<a href="{url}"><img src="{$data_path}/{image}" border="0" width="162"/></a>
						</xsl:when>
						<xsl:otherwise>
							<img src="{$data_path}/{image}" border="0" width="162"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
	</div>
</xsl:template>

</xsl:stylesheet>

<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
                
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout_photos.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:template match="data">
    <form id="fu" method="post" enctype="multipart/form-data">
        <input type="hidden" name="action" value="upload"/>
        <input type="file" name="file"/>
        <input type="button" value="Upload" onclick="UploadFile()"/>
    </form>
    <div name="i_debug" id="i_debug">debug string</div>
    <script>
        function UploadFile()
        {
            InitUploadProgress();
            ShowUploadProgress();
            $('fu').submit();
        }
        
        function ShowUploadProgress()
        {
            $('i_debug').innerHTML = "";
            var d = new Date();
            JsHttpRequest.query(
                '?photos.get_upload_information&amp;date='+ d.getTime(),
                {
                },
                function( result, txt )
                {
                    $('i_debug').innerHTML = txt;
                    ShowUploadProgress();
                }
            );
        }
        
        function InitUploadProgress()
        {
            var d = new Date();
            JsHttpRequest.query(
                '?photos.init_upload_information&amp;date='+ d.getTime(),
                {
                },
                function( result, txt )
                {
                }
            );
        }
    </script>
</xsl:template>

</xsl:stylesheet>

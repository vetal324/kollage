<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../layout.xsl" />

<xsl:template match="data">
	<div class="article-wrapper">
		<div class="article-content">
			<div class="page_title"><h1><span class="red"><xsl:value-of select="$locale/photos/article/title_prefix/text()"/> &#8226; </span><xsl:if test="article/header !=''"><xsl:value-of select="article/header"/></xsl:if></h1></div>
			<xsl:value-of select="article/text_transformed" disable-output-escaping="yes"/>
		</div>
	</div>
	<div class="left-barside">
		<xsl:apply-templates select="/content/documents/leftmenu"/>
		
		<div class="left-barside-sn">
			<xsl:variable name="share_title" select="article/header"/>
			<xsl:variable name="share_url">http://www.kollage.com.ua/photos/article/<xsl:value-of select="article/id"/></xsl:variable>

			<div class="header-left-barside-sn">����������:</div>
			<a href="http://vkontakte.ru/share.php?url={$share_url}&amp;title={$share_title}&amp;description=&amp;noparse=true" target="_blank" class="sn-vk vtip" title="������������ ������ ���������">&#160;</a>
			<a href="http://www.facebook.com/sharer.php?s=100&amp;p[title]={$share_title}&amp;p[url]={$share_url}&amp;p[summary]=" target="_blank" class="sn-fb vtip" title="������������ ������ � Facebook">&#160;</a>
			<a href="https://plus.google.com/share?url={$share_url}" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=700,width=600');return false;" class="sn-gl vtip" title="������������ ������ Google+">&#160;</a>
		</div>
	</div>
	<div class="page-clear-both"></div>
</xsl:template>

</xsl:stylesheet>

<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../layout.xsl" />
<xsl:include href="order_layouts.xsl" />

<xsl:template match="data"><link rel="stylesheet" href="/js/swfupload/styles.css" />
	<xsl:call-template name="order_layouts"/>
	
	<h1><span class="red"><xsl:value-of select="$locale/photos/order/title"/></span><xsl:value-of select="$locale/photos/order/title5"/></h1>
	
	<script src="{$views_path}/{$controller_name}/functions.js?201909051824"></script>
	<script src="/js/swfupload/swfupload.js"></script>
	<script src="/js/swfupload/swfupload.queue.js"></script>
	<script src="/js/swfupload/swfupload.speed.js"></script>
	<script src="/js/swfupload/fileprogress.js"></script>
	<script src="/js/swfupload/handlers.js"></script>  
	
	<xsl:if test="/content/global/client/id >= 0">
	
		<!-- ORDER FORM -->
		<table cellpadding="3" cellspacing="1" class="sop-order-content_table">
			<tr>
				<th colspan="2" class="sop-order-number"><xsl:value-of select="$locale/photos/order/order_number/text()"/><xsl:value-of select="order/order_number"/><xsl:if test="have_previous_order > 0">&#160;&#160;&#160;<a href="/{$controller_name}/goto_previous_order">������� � ����������� �������������� ������</a></xsl:if></th>
			</tr>
			
			<tr>
				<td colspan="2">
					<b><xsl:value-of select="$locale/photos/order/title2/text()"/></b> (<span name="photos_count" id="photos_count">0</span>&#160;<xsl:value-of select="$locale/common/items_amount/text()"/>)
					<!--<xsl:if test="/content/global/client/order_limit = 0 or /content/global/client/order_limit = 1"><span class="error"><xsl:value-of select="$locale/photos/order/order_limit/text()"/></span></xsl:if>-->
				</td>
			</tr>
			<tr>
				<td colspan="2">
				<!-- PHOTOS -->
				<xsl:choose>
					<xsl:when test="/content/global/browser_type = 1">
						<div name="order_photos" id="order_photos" style="border:0;width:100%;min-height:350px;overflow:auto;padding-bottom:20px;">&#160;</div>
					</xsl:when>
					<xsl:otherwise>
						<div name="order_photos" id="order_photos" style="border:0;width:100%;min-height:350px;overflow:auto;">&#160;</div>
					</xsl:otherwise>
				</xsl:choose>
				
				<div>
					<table cellspacing="0" cellpadding="0" border="0" align="center">
					<tr>
						<td><input type="button" value="{$locale/photos/order/order_photos/button_add/text()}" class="button" style="margin: 3px;" onclick="ShowPhotosForm()"/></td>
						<td><input type="button" value="{$locale/photos/order/order_photos/button_del/text()}" class="button" style="margin: 3px;" onclick="DelPhotos('{$locale/photos/order/order_photos/confirm_delete/text()}')"/></td>
						<td><input type="button" value="{$locale/photos/order/order_photos/button_del_all/text()}" class="button" style="margin: 3px;" onclick="DelAllPhotos('{$locale/photos/order/order_photos/confirm_delete_all/text()}')"/></td>
						<td><input type="button" value="{$locale/photos/order/order_photos/button_changegroup/text()}" class="button" style="margin: 3px;" onclick="ShowChangeGroupForm()"/></td>
						<td><input type="button" value="{$locale/photos/order/order_photos/button_save/text()}" class="button" style="margin: 3px;"/></td>
					</tr>
					</table>
				</div>
				<!-- PHOTOS - END -->
				</td>
			</tr>
			
			<!-- ORDER PARAMETERS -->
			<form method="post" name="order_params" id="order_params" action="/{$controller_name}/order2" onsubmit="return(PhotosOnSubmit())">
			<input type="hidden" name="object[id]" value="{order/id}"/>
			<tr>
				<td colspan="2" class="sop-order-countfoto"><b><xsl:value-of select="$locale/photos/order/title3/text()"/></b></td>
			</tr>
			<tr>
				<td width="50%" valign="top" style="width:50%">
					<table cellpadding="0" cellspacing="3">
					
						<tr>
							<td class="sop-order-ftitle"><xsl:value-of select="$locale/photos/order/photo_paper/text()"/></td>
						</tr>
						<tr>
							<td>
								<xsl:choose>
									<xsl:when test="order/photo_paper = 1 or order/photo_paper = ''"><input type="radio" name="object[photo_paper]" id="photo_paper_active" value="1" checked="yes" /></xsl:when>
									<xsl:otherwise><input type="radio" name="object[photo_paper]" id="photo_paper_active" value="1" /></xsl:otherwise>
								</xsl:choose>
								<label for="photo_paper_active" class="sop-order-input"><xsl:value-of select="$locale/photos/order/photo_paper1/text()"/></label>
								<xsl:choose>
									<xsl:when test="order/photo_paper = 2"><input type="radio" name="object[photo_paper]" id="photo_paper_inactive" value="2" checked="yes"/></xsl:when>
									<xsl:otherwise><input type="radio" name="object[photo_paper]" id="photo_paper_inactive" value="2"/></xsl:otherwise>
								</xsl:choose>
								<label for="photo_paper_inactive" class="sop-order-input"><xsl:value-of select="$locale/photos/order/photo_paper2/text()"/></label>
							</td>
						</tr>
					
						<tr>
							<td class="sop-order-ftitle"><xsl:value-of select="$locale/photos/order/border/text()"/></td>
						</tr>
						<tr>
							<td>
								<xsl:choose>
									<xsl:when test="order/border = 1"><input type="radio" name="object[border]" id="border1" value="1" checked="yes" /></xsl:when>
									<xsl:otherwise><input type="radio" name="object[border]" id="border1" value="1" /></xsl:otherwise>
								</xsl:choose>
								<label for="border1" class="sop-order-input"><xsl:value-of select="$locale/common/exist/text()"/></label>
								<xsl:choose>
									<xsl:when test="order/border = 0 or order/border=''"><input type="radio" name="object[border]" id="border0" value="0" checked="yes"/></xsl:when>
									<xsl:otherwise><input type="radio" name="object[border]" id="border0" value="0"/></xsl:otherwise>
								</xsl:choose>
								<label for="border0" class="sop-order-input"><xsl:value-of select="$locale/common/no/text()"/></label>
							</td>
						</tr>
					
						<tr>
							<td class="sop-order-ftitle"><xsl:value-of select="$locale/photos/order/address/text()"/></td>
						</tr>
						<tr>
							<td><input type="text" name="object[address]" id="address" value="{order/address}" style="width:99%;"/></td>
						</tr>
						
					</table>
				</td>
				
				<td width="50%" valign="top" style="width:50%">
					<table cellpadding="0" cellspacing="3">
					
						<tr>
							<td class="sop-order-ftitle"><xsl:value-of select="$locale/photos/order/delivery_id/text()"/></td>
						</tr>
						<tr>
							<td>
								<xsl:variable name="delivery_id" select="order/delivery_id"/>
								<select name="object[delivery_id]" class="middle" id="delivery_id" onchange="LoadPaymentways($('delivery_id'),$('paymentway_id'),{order/paymentway_id});" style="width:100%;">
									<xsl:for-each select="deliveries/*">
										<xsl:call-template name="gen-option">
											<xsl:with-param name="value" select="id"/>
											<xsl:with-param name="title" select="name"/>
											<xsl:with-param name="selected" select="$delivery_id"/>
										</xsl:call-template>
									</xsl:for-each>
								</select>
							</td>
						</tr>
					
						<tr>
							<td class="sop-order-ftitle"><div id="point_id_title"><xsl:value-of select="$locale/photos/order/point_id/text()"/></div></td>
						</tr>
						<tr>
							<td>
								<xsl:variable name="point_id" select="order/point_id"/>
								<select name="object[point_id]" class="middle" id="point_id" style="width:100%;" onchange="UpdateOrderParameters()">
									<xsl:for-each select="points/*">
										<xsl:call-template name="gen-option">
											<xsl:with-param name="value" select="id"/>
											<xsl:with-param name="title" select="name"/>
											<xsl:with-param name="selected" select="$point_id"/>
										</xsl:call-template>
									</xsl:for-each>
								</select>
							</td>
						</tr>
					
						<tr>
							<td class="sop-order-ftitle"><xsl:value-of select="$locale/photos/order/paymentway_id/text()"/></td>
						</tr>
						<tr>
							<td>
								<xsl:variable name="paymentway_id" select="order/paymentway_id"/>
								<select name="object[paymentway_id]" class="middle" id="paymentway_id" onchange="UpdateOrderParameters()" style="width:100%;">
								</select>
							</td>
						</tr>
						
					</table>
				</td>
			</tr>
			
			<tr>
				<th width="50%" valign="top" style="padding-left: 25px;">
					<table cellpadding="0" cellspacing="0">
						<tr><th>
							<table cellpadding="0" cellspacing="0">
								<tr>
									<th class="normal"><xsl:value-of select="$locale/photos/order/total_photos/text()"/></th>
									<th class="normal"><span name="total_photos" id="total_photos"><xsl:value-of select="order/photos_price_amount"/></span>&#160;<xsl:value-of select="$locale/common/currency/text()"/></th>
								</tr>
								<tr>
									<th class="normal"><xsl:value-of select="$locale/photos/order/total_delivery/text()"/></th>
									<th class="normal"><span name="total_delivery" id="total_delivery"><xsl:value-of select="order/delivery"/></span>&#160;<xsl:value-of select="$locale/common/currency/text()"/></th>
								</tr>
								<tr>
									<th class="normal"><xsl:value-of select="$locale/photos/order/total_discount/text()"/></th>
									<th class="normal"><span name="total_discount" id="total_discount">0.00</span>&#160;<xsl:value-of select="$locale/common/currency/text()"/></th>
								</tr>
								<tr>
									<th><xsl:value-of select="$locale/photos/order/total_price/text()"/></th>
									<th><span name="total_price" id="total_price">0.00</span>&#160;<xsl:value-of select="$locale/common/currency/text()"/></th>
								</tr>
							</table>
						</th></tr>
					</table>
				</th>
				
				<td width="50%" valign="top">
					<table cellpadding="0" cellspacing="3">
					
						<tr>
							<td class="sop-order-ftitle"><xsl:value-of select="$locale/photos/order/comments/text()"/></td>
						</tr>
						<tr>
							<td><textarea cols="30" rows="3" name="object[comments]" id="comments" onchange="UpdateOrderParameters()" style="width:260px;height:50px"><xsl:value-of select="order/comments"/></textarea></td>
						</tr>
						
						<td>
							<input type="submit" value="{$locale/photos/order/order/text()}" class="button"/>
						</td>
					</table>
				</td>
			</tr>
			</form>
			<!-- ORDER PARAMETERS - END -->
				
		</table>
		
		<div class="order-photo-description">
			<p>������ �������� ����������. ������ � ���?</p>
			<p>���������� ������, ��������������� ��������������� ��������� �������� ���������, ��� ����������� ����� ����� ������� ������ � ������������� �����, ������������ �� ������������ ������ �� ����������� � ������, �������� ����������� ��������� ���� �������������� �������� ���������, �� ������� ���������� ��������� �� ���������� ���������� ���� ����� �������� ���������� �������.</p>
			<p>�� ���������� ���������� ���������� ������ ����������, ������������ � ���� ����������� ���������� ��������� ��������� ����������� �� ������������������� ������ � ������������ ����� ������� ���������� ����� ����������, �������, ������ � ������������. �������� ��������� ���� ����������� �������� ��������, � ������������ ������� ����� �������� ������ ��������� ����� ������������ � ���������� ��� ������������� �� �������������� ����. ���������� � ������ ����� ���� ������.</p>
			<p>������ � ����� ������� ������� ��� ������ �� ������������. �� ������ ������ �������� ������ �����������������, �� �������� ������� ������ ���������. ��������������� ������ ��������� �� �������� ������, �������� ����������� ������, ������� ��������� ��� ������������� �������� � ����������� ������. � ���� ���������, ��� � �������� ��� ������ ���������� ����������� �������� EPSON, � ������� ��� ������������� ������������ ������ � ���������� ������������� ���������� ���� ������������� ����������, ��������� �� ���������������, ��� ������� � ��� ����, ������� �������������� ��������������� ���������� ����� ������� ���������� � ���� (������� ����������� ������ ������) �� ���������, ��� ������������� ��������� ���������������, �� ���� ����������� �������� ���� ����� ������� �������, ��� ��������� ������� ������������� ������. � ����� ����� ����� ����� � ���� ��������� �� ������ ����������?</p>
			<p>� ����� ���������� ������� ����. �� ������ � ��� ����������� ���������� ����������� ��������� ��� �����. ������ ���������� ����� �������� � �������� �� ������ �� ����. ������� ����� �������� � ���������� ���������� �� ����� ����� www.kollage.com.ua ��������� ������ ��������� � ������������� ����������, ������� ����� ������� ��� ������ ������ � ��������, � ����� �������������� ������� ���������� ������ �� ������ �����, �� ������ ������ �� ���������. �� ��������� ��������� ������ �������������� ����� ��������: ��������� ����� ���� �� �����������, ��� ������� ����������� ������, ������ � ���������� �������� �� ����� ������, � ����� ��������� ������� ����� �������� ���������� �� ������ ������.</p>
			<p>�� ����� ���� ������� ������������� ��, ����� ������� ��� ������ ���������� � ����������, ����� ������ ���� ��� �����������, � ���� ������� ������ ���������� ONLINE ������ ����� �������� ������������ ��� ������ ����������, � �������� ���������� ����������� ������ ������������� ����� ����������.</p>
			<p>������ ������ƻ</p>
		</div>
		<script type="text/javascript">
		var pFirst = $j('.order-photo-description p').first();
		var showLink = $j('<a href="#" onclick="return(false)">������ ���������</a>').on('click', function(){
			$j(this).prev().remove();
			$j(this).remove();
			$j('.order-photo-description p:not(:first-child)').show();
		});
		pFirst.append( $j('<span>... </span>') );
		pFirst.append( showLink );
		$j('.order-photo-description p:not(:first-child)').hide();
		</script>
		
		<xsl:if test="/content/global/client/order_limit = 1">
		<script>
			$j(function(){
				Alert.message( "������ �����������!\n��� ������ ����� ��������� �� ����� 30���.\n����� � ������� ������ ������ �� �����.\n\n����� ������� ��������� ������ ������ ����������� ����� �����." );
				UpdateOrderParameters();
			});
		</script>
		</xsl:if>
		
		<!-- ORDER FORM - END -->
		<script>
			load_photo_start_msg = '<div style="text-align:center;padding:40px"><xsl:value-of select="$locale/photos/order/order_photos_start_message/text()"/></div>';
			page = '<xsl:value-of select="$page"/>';
			pages = '<xsl:value-of select="$pages"/>';
			rows_per_page = '<xsl:value-of select="$rows_per_page"/>';
			
			LoadPaymentways($('delivery_id'),$('paymentway_id'),<xsl:value-of select="order/paymentway_id"/>)
			
			$j(function(){
				LoadPhotosForm( load_photo_start_msg, <xsl:value-of select="$page"/> );
				UpdateOrderParameters();
				
				$j('#dlg-add-photos-flash-container').dialog({
					autoOpen: false,
					resizable: false,
					minHeight: 400,
					minWidth: 720,
					modal: true,
					open: function(event, ui){
						if( Client &amp;&amp; !Client.isLogged() ) {
							$j('.msg-limit-unauthed-uploading').removeClass('hidden');
						}
						else {
							$j('.msg-limit-unauthed-uploading').addClass('hidden');
						}
					}
				});
				
				$j('#dlg-add-photos-container').dialog({
					autoOpen: false,
					resizable: false,
					minHeight: 400,
					minWidth: 820,
					modal: true,
					open: function(event, ui){
						if( Client &amp;&amp; !Client.isLogged() ) {
							$j('.msg-limit-unauthed-uploading').removeClass('hidden');
						}
						else {
							$j('.msg-limit-unauthed-uploading').addClass('hidden');
						}
					}
				});
				
				$j('#dlg-changegroup-container').dialog({
					autoOpen: false,
					resizable: false,
					minHeight: 200,
					minWidth: 450,
					modal: true,
					open: function(event, ui){
						//dialog adjustments
					}
				});
			});
			
		</script>
	</xsl:if>
</xsl:template>

</xsl:stylesheet>

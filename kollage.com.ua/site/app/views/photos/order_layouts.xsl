<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../fine-uploader.xsl" />

<xsl:template name="order_layouts">
	<xsl:call-template name="fine_uploader_layouts"/>

	<div id="dlg-add-photos-flash-container" class="hidden" title="�������� ����������">
		<div id="dlg-content">
			<table cellpadding="10" cellspacing="0" class="content_table" width="100%" height="100%">
				<tr>
					<td colspan="2" height="24"><a href="#" onclick="ShowPhotosForm();ClosePhotosFlashForm();return(false)">HTML ������ ������� �������� ����������</a></td>
				</tr>
				<tr>
					<td width="*" valign="top">
						<xsl:value-of select="$locale/photos/order/add_photos/help/text()" disable-output-escaping="yes"/>
						<xsl:value-of select="$locale/photos/order/add_photos/help_flash/text()" disable-output-escaping="yes"/>
						<div>
							<div><xsl:value-of select="$locale/photos/order/add_photos/apply_parameters/text()" disable-output-escaping="yes"/></div>
							<table cellspacing="0" cellpadding="0" style="margin-top:5px">
							<tr>
								<td>
									<input type="text" style="width:30px" name="apply_quantity" id="apply_quantity_flash" value="1"/>
								</td>
								<td>&#160;X&#160;</td>
								<td>
								<select class="selectbox" name="apply_size_id" id="apply_size_id_flash">
									<xsl:for-each select="/content/documents/data/photo_sizes/*">
										<xsl:call-template name="gen-option">
											<xsl:with-param name="value" select="id"/>
											<xsl:with-param name="title" select="name"/>
											<xsl:with-param name="selected" select="0"/>
										</xsl:call-template>
									</xsl:for-each>
								</select>
								</td>
							</tr>
							</table>
						</div>
						<div style="margin-top:10px">
							<table cellspacing="0" cellpadding="0" width="100%">
								<tr>
									<td align="left" colspan="2"><b>�������</b></td>
								</tr>
								<tr>
									<td align="left">������� ��������</td>
									<td align="right"><div id="tdAverageSpeed">-</div></td>
								</tr>
								<tr>
									<td align="left">����� ��������</td>
									<td align="right"><div id="tdTimeRemainingQueue">-</div></td>
								</tr>
								<tr>
									<td align="left">������</td>
									<td align="right"><div id="tdTimeElapsedQueue">-</div></td>
								</tr>
							</table>
						</div>
						<div class="red hidden msg-limit-unauthed-uploading" style="margin-top:10px; font-weight: bold;">
							��� ����������� - �������� ������ �� 10-�� ����������!
						</div>
					</td>
					<td width="420" valign="top">
						<div style="width:420px;height:340px;overflow:auto;padding:0;padding-top:20px"><div id="fsUploadProgress" class="flash"><span class="legend">������� ��������</span></div></div>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<table cellspacing="0" cellpadding="2" border="0"><tr>
							<td style="position:relative"><span id="spanButtonPlaceHolder">�������������, ��������...</span></td>
							<td><input type="button" class="button" value="{$locale/photos/order/add_photos/button_upload/text()}" onclick="UploadPhotosFlash()" id="flash_upload_button" style="visibility:hidden"/></td>
							<td><input type="button" class="button" value="{$locale/photos/order/add_photos/button_cancel/text()}" onclick="CancelUploadPhotosFlash()" id="flash_cancel_upload_button" style="visibility:hidden"/></td>
						</tr></table>
					</td>
				</tr>
				<tr>
				</tr>
			</table>
		</div>
		<!--<div id="dlg-footer">
			<div class="dlg-footer-loader hidden">
				<div class="loader1"></div>
			</div>
			<div class="dlg-footer-buttons">
				<a class="dlg-btn-close" onclick="ClosePhotosFlashForm()"></a>
			</div>
			<div class="clear-both"></div>
		</div>-->
	</div>
	
	<div id="dlg-add-photos-container" class="hidden" title="�������� ����������">
		<div id="dlg-content">
			<form name="add_photos_form" id="add_photos_form" onsubmit="add_photos_form_submit()" method="post" enctype="multipart/form-data">
			<table cellpadding="10" cellspacing="0" class="content_table" width="100%" height="100%">
				<tr>
					<td width="40%" valign="top">
						<xsl:value-of select="$locale/photos/order/add_photos/help/text()" disable-output-escaping="yes"/>
						<xsl:choose>
							<xsl:when test="/content/global/client/network_name != ''">
								<xsl:value-of select="$locale/photos/order/add_photos/help_localnet/text()" disable-output-escaping="yes"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$locale/photos/order/add_photos/help_inet128/text()" disable-output-escaping="yes"/>
							</xsl:otherwise>
						</xsl:choose>
						<div>
							<div><xsl:value-of select="$locale/photos/order/add_photos/apply_parameters/text()" disable-output-escaping="yes"/></div>
							<table cellspacing="0" cellpadding="0" style="margin-top:5px">
							<tr>
								<td>
									<input type="text" style="width:30px" name="apply_quantity" id="apply_quantity" value="1"/>
								</td>
								<td>&#160;X&#160;</td>
								<td>
								<select class="selectbox" name="apply_size_id" id="apply_size_id">
									<xsl:for-each select="/content/documents/data/photo_sizes/*">
										<xsl:call-template name="gen-option">
											<xsl:with-param name="value" select="id"/>
											<xsl:with-param name="title" select="name"/>
											<xsl:with-param name="selected" select="0"/>
										</xsl:call-template>
									</xsl:for-each>
								</select>
								</td>
							</tr>
							</table>
						</div>
						<div class="red hidden msg-limit-unauthed-uploading" style="margin-top:10px; font-weight: bold;">
							��� ����������� - �������� ������ �� 10-�� ����������!
						</div>
					</td>
					<td width="60%" valign="top">
						<div id="fine-uploader-manual-trigger"></div>
					</td>
				</tr>
			</table>
			</form>
		</div>
	</div>
	
	<div id="dlg-changegroup-container" class="hidden" title="{$locale/photos/order/title4/text()}">
		<div id="dlg-content">
			<form name="changegroup_form" id="changegroup_form" onsubmit="changegroup_form_submit()" method="post">
			<table cellpadding="10" cellspacing="0" class="content_table" width="100%" height="100%">
				<tr>
					<td valign="top">
					
						<table cellspacing="0" cellpadding="0"><tr>
							<td><input type="radio" checked="yes" name="changegroup_group" id="changegroup_group_selected" value="1"/></td>
							<td><label for="changegroup_group_selected"><xsl:value-of select="$locale/photos/order/add_photos/apply_parameters_to_selected/text()" disable-output-escaping="yes"/></label></td>
						</tr></table>
					
						<table cellspacing="0" cellpadding="0"><tr>
							<td><input type="radio" name="changegroup_group" id="changegroup_group_all" value="2"/></td>
							<td><label for="changegroup_group_all"><xsl:value-of select="$locale/photos/order/add_photos/apply_parameters_to_all/text()" disable-output-escaping="yes"/></label></td>
						</tr></table>
						
						<table cellspacing="0" cellpadding="0" style="margin-top:5px;margin-left:2px">
						<tr>
							<td>
								<input type="text" style="width:30px" name="changegroup_quantity" id="changegroup_quantity" value="1"/>
							</td>
							<td>&#160;X&#160;</td>
							<td>
							<select class="selectbox" name="changegroup_size_id" id="changegroup_size_id">
								<xsl:for-each select="/content/documents/data/photo_sizes/*">
									<xsl:call-template name="gen-option">
										<xsl:with-param name="value" select="id"/>
										<xsl:with-param name="title" select="name"/>
										<xsl:with-param name="selected" select="0"/>
									</xsl:call-template>
								</xsl:for-each>
							</select>
							</td>
						</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<table cellspacing="0" cellpadding="2"><tr>
							<td><input type="button" class="button" value="{$locale/photos/order/add_photos/button_apply/text()}" onclick="ChangeGroup()"/></td>
							<td><input type="button" class="button" value="{$locale/photos/order/add_photos/button_close/text()}" onclick="CloseChangeGroupForm()"/></td>
							<td><div style="padding:left:20px" name="add_photos_msg" id="add_photos_msg"></div></td>
						</tr></table>
					</td>
				</tr>
				</table>
			</form>
		</div>
	</div>
	
</xsl:template>

</xsl:stylesheet>

<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../layout.xsl" />

<xsl:template match="data">
	<h1><span class="red"><xsl:value-of select="$locale/photos/order/title"/></span><xsl:value-of select="$locale/photos/order/title5"/></h1>
	
	<xsl:if test="/content/global/client/id = 0">
		<script type="text/javascript">
		$j(function(){
			Events.publish('/Login/Show');
		});
		</script>
	</xsl:if>
	
	<xsl:if test="/content/global/client/id > 0">
		<!-- UPC FORM -->
			<div class="p1">
				<xsl:value-of select="OrderID"/><br/>
				<xsl:value-of select="message"/>
			</div>
		<!-- UPC FORM - END -->
	</xsl:if>
	
</xsl:template>

</xsl:stylesheet>

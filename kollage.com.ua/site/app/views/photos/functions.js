
var load_photo_start_msg = '';
var page=1, pages=1, rows_per_page=8;

function LoadPhotosForm( msg, p )
{
	var d = new Date();
	var page_add = '';
	
	if( p>0 ) page_add = "&page="+ p;
	
	document.getElementById('order_photos').innerHTML = '';
	document.getElementById('order_photos').innerHTML = msg;
	var req = new JsHttpRequest();
	req.caching = false;
	req.loader = 'FORM';
	req.onreadystatechange = function()
		{
			if( req.readyState == 4 )
			{
				page = req.responseJS.page; pages = req.responseJS.pages; rows_per_page = req.responseJS.rows_per_page;
				document.getElementById('order_photos').innerHTML = req.responseText;
				if( req.responseJS.photos_count > 0 )
				{
					document.getElementById('order_photos').style.height = '425px';
				}
				else
				{
					document.getElementById('order_photos').style.height = 'auto';
				}
			}
		}
	req.open( null, '/?photos.order_photos&date='+ d.getTime() + page_add, true );
	req.send( null );
}

function ShowPhotosForm()
{
	$j('#dlg-add-photos-container').dialog('open');
	var uploader = FineUploaderWrapper.init({
		callbacks: {
			onComplete: function() {
				LoadPhotosForm( load_photo_start_msg, 0 );
				UpdateOrderParameters();
			}
		}
	});
	if (uploader) {
		$j('#fine-uploader-trigger-upload').click(function() {
			uploader.setParams({
				quantity: document.getElementById('apply_quantity').value,
				size_id: document.getElementById('apply_size_id').options[document.getElementById('apply_size_id').selectedIndex].value
			});
			uploader.uploadStoredFiles();
		});
	}
}

function ShowPhotosFlashForm()
{
	var container = '#dlg-add-photos-flash-container';
	$j(container).dialog('open');
}

function CheckupInputZip()
{
	retval = true;
   
	var msg = 'Загружайте ZIP-архивы по одному пожалуйста!';
	
	var r, exist = 0;
	var i = 1;
	var o = document.getElementById( 'photo' + i );
	while( o )
	{
		if( o.value != '' )
		{
		  r = new RegExp('^.*(zip)+$','i').test( o.value );
		  if( r )
		  {
			exist ++;
		  }
		  if( exist > 1 )
		  {
			Alert.error( msg );
			retval = false;
		  }
		}
		i++;
		o = document.getElementById( 'photo' + i );
	}
	return( retval );
}

function ClosePhotosForm()
{
	$j('#dlg-add-photos-container').dialog('close');
}

function CancelUploadPhotosFlash()
{
	swfu.cancelQueue();
	$("flash_cancel_upload_button").style.display = 'none';
	$("flash_close_button").style.display = 'block';
	$("flash_show_html_version_button").style.display = 'block';
}

function ClosePhotosFlashForm()
{
	$j('#dlg-add-photos-flash-container').dialog('close');
}

function UploadPhotos( msg )
{
	var d;
	
	document.getElementById('add_photos_msg').innerHTML = msg;
	d = new Date();
	JsHttpRequest.query(
		'/?photos.add_photos&date='+ d.getTime(),
		{
			'photo1': document.getElementById('photo1'),
			'photo2': document.getElementById('photo2'),
			'photo3': document.getElementById('photo3'),
			'photo4': document.getElementById('photo4'),
			'photo5': document.getElementById('photo5'),
			'quantity': document.getElementById('apply_quantity').value,
			'size_id': document.getElementById('apply_size_id').options[document.getElementById('apply_size_id').selectedIndex].value,
			'page': page
		},
		function( result, txt )
		{
			document.getElementById('add_photos_msg').innerHTML = txt;
			page = result['page']; pages = result['pages']; rows_per_page = result['rows_per_page'];
			if( result['err'] == 0 || ( result['files'] > 0 && result['files'] > result['files_err'] ) )
			{
				LoadPhotosForm( load_photo_start_msg, page );
				UpdateOrderScreen( result );
				ClosePhotosForm();
			}
		}
	);
}

function UploadPhotosFlash()
{
	if( swfu )
	{
		swfu.addPostParam( "quantity", document.getElementById("apply_quantity_flash").value );
		swfu.addPostParam( "size_id", document.getElementById("apply_size_id_flash").options[document.getElementById("apply_size_id_flash").selectedIndex].value );
		swfu.startUpload();
	}
}

function DelPhotos( msg )
{
	var ids = "", i, cb, h, delim = '', d;
	
	var i = 1;
	while( document.getElementById("photo_del"+i) )
	{
		cb = document.getElementById("photo_del"+i);
		if( cb.checked && (h = document.getElementById("photo_id"+i)) )
		{
			ids += delim + h.value;
			delim = ',';
		}
		++i;
	}
	
	if( ids!='' && confirm(msg) )
	{
		d = new Date();
		JsHttpRequest.query(
			'/?photos.del_photos&date='+ d.getTime(),
			{
				'ids': ids,
				'page': page
			},
			function( result, txt )
			{
				page = result['page']; pages = result['pages']; rows_per_page = result['rows_per_page'];
				LoadPhotosForm( load_photo_start_msg, page );
				UpdateOrderScreen( result );
			}
		);
	}
}

function DelAllPhotos( msg )
{
	var ids = "", i, cb, h, delim = '', d;
	
	var i = 1;
	while( document.getElementById("photo_del"+i) )
	{
		cb = document.getElementById("photo_del"+i);
		if( h = document.getElementById("photo_id"+i) )
		{
			ids += delim + h.value;
			delim = ',';
		}
		++i;
	}
	
	if( ids!='' && confirm(msg) )
	{
		d = new Date();
		JsHttpRequest.query(
			'/?photos.del_photos&date='+ d.getTime(),
			{
				'ids': ids,
				'page': page
			},
			function( result, txt )
			{
				page = result['page']; pages = result['pages']; rows_per_page = result['rows_per_page'];
				LoadPhotosForm( load_photo_start_msg, page );
				UpdateOrderScreen( result );
			}
		);
	}
}

function UpdatePhotoParameters( prefix )
{
	var d, size_id, quantity, id;
	var pq, p;
	
	pq = $('photo_quantity'+prefix);
	ps = $('photo_size'+prefix);
	id = $('photo_id'+prefix).value;
	
	if( prefix!='' && pq && ps && id>0 )
	{
		size_id = ps.options[ps.selectedIndex].value;
		quantity = pq.value;
		
		d = new Date();
		JsHttpRequest.query(
			'/?photos.update_photo_parameters&date='+ d.getTime(),
			{
				'id': id,
				'size_id': size_id,
				'quantity': quantity,
				'prefix': prefix,
				'page': page
			},
			function( result, txt )
			{
				if( result['id']>0 && result['prefix'] )
				{
					$('photo_quantity'+result['prefix']).value = result['quantity'];
				
					UpdateOrderScreen( result );
				}
			}
		);
	}
}

function UpdateOrderParameters()
{
	var photo_paper_active, border1, address;
	var delivery_id, point_id, paymentway_id, comments;
	
	photo_paper_active = $('photo_paper_active'); border1 = $('border1'); address = $('address');
	delivery_id = $('delivery_id'); point_id = $('point_id'); paymentway_id = $('paymentway_id'); comments = $('comments');
	
	if( photo_paper_active && border1 && address && delivery_id && point_id && paymentway_id && comments )
	{
		photo_paper = ( photo_paper_active.checked )? 1 : 2;
		border = ( border1.checked )? 1 : 0;
		
		/*if( $('delivery_id').selectedIndex == 0 )
		{
			$('point_id_title').style.visibility = 'visible';
			$('point_id').style.visibility = 'visible';
		}
		else
		{
			$('point_id_title').style.visibility = 'hidden';
			$('point_id').style.visibility = 'hidden';
		}*/
		$('point_id_title').style.visibility = 'hidden';
		$('point_id').style.visibility = 'hidden';
		
		d = new Date();
		JsHttpRequest.query(
			'/?photos.update_order_parameters&date='+ d.getTime(),
			{
				'border': border,
				'photo_paper': photo_paper,
				'address': address.value,
				'delivery_id': delivery_id.options[delivery_id.selectedIndex].value,
				'point_id': point_id.options[point_id.selectedIndex].value,
				'paymentway_id': paymentway_id.options[paymentway_id.selectedIndex].value,
				'comments': comments.value,
				'page': page
			},
			function( result, txt )
			{
				if( $('delivery_id').selectedIndex == 0 )
				{
					$('point_id_title').style.visibility = 'visible';
					$('point_id').style.visibility = 'visible';
				}
				UpdateOrderScreen( result );
			}
		);
	}
}

function UpdateOrderScreen( result )
{
	var photo_paper_active, border1, address;
	var delivery_id, point_id, paymentway_id, comments;
	var total_price = '0.00';
	
	photo_paper_active = $('photo_paper_active'); border1 = $('border1'); address = $('address');
	delivery_id = $('delivery_id'); point_id = $('point_id'); paymentway_id = $('paymentway_id'); comments = $('comments');
	
	if( address ) address.value = result['address'];
	if( comments ) comments.value = result['comments'];
	
	$('photos_count').update( result['photos_count'] );
	$('total_photos').update( result['photos_price_amount'] );
	$('total_delivery').update( result['delivery'] );
	$('total_discount').update( result['discount_price'] );
	$('total_price').update( result['total_price'] );
}

function ShowChangeGroupForm()
{
	var ids = "", i, cb, h, delim = '', d;
	
	var i = 1;
	while( document.getElementById("photo_del"+i) )
	{
		cb = document.getElementById("photo_del"+i);
		if( h = document.getElementById("photo_id"+i) )
		{
			ids += delim + h.value;
			delim = ',';
		}
		++i;
	}
	
	if( ids!='' )
	{
		$j('#dlg-changegroup-container').dialog('open');
	}
}

function CloseChangeGroupForm()
{
	$j('#dlg-changegroup-container').dialog('close');
}

function ChangeGroup( msg )
{
	var d;
	var group = '';
	if( document.getElementById('changegroup_group_selected').checked )
	{
		var cb, h, delim = '';
		var i = 1;
		while( document.getElementById("photo_del"+i) )
		{
			cb = document.getElementById("photo_del"+i);
			if( cb.checked && (h = document.getElementById("photo_id"+i)) )
			{
				group += delim + h.value;
				delim = ',';
			}
			++i;
		}
		if( group == '' )
		{
			CloseChangeGroupForm();
			return;
		}
	}
	
	document.getElementById('add_photos_msg').innerHTML = msg;
	d = new Date();
	JsHttpRequest.query(
		'/?photos.changegroup&date='+ d.getTime(),
		{
			'group': group,
			'quantity': document.getElementById('changegroup_quantity').value,
			'size_id': document.getElementById('changegroup_size_id').options[document.getElementById('changegroup_size_id').selectedIndex].value,
			'page': page
		},
		function( result, txt )
		{
			page = result['page']; pages = result['pages']; rows_per_page = result['rows_per_page'];
			if( result['err'] == 0 )
			{
				LoadPhotosForm( load_photo_start_msg, page );
				UpdateOrderScreen( result );
				CloseChangeGroupForm();
			}
		}
	);
}

function LoadPaymentways( delivery, paymentway, paymentway_id )
{
	var d;
	
	paymentway.options.length = 0;
	paymentway.options[0] = new Option( 'загрузка...', 0 );
	
	d = new Date();
	JsHttpRequest.query(
		'/?photos.load_paymentways&date='+ d.getTime(),
		{
			'delivery_id': delivery.options[delivery.selectedIndex].value
		},
		function( result, txt )
		{
			if( result['err'] == 0 && result['paymentways'] )
			{
				paymentway.options.length = 0;
				for( var i=0; i<result['paymentways'].length; i++ )
				{
					paymentway.options[i] = new Option( result['paymentways'][i][1], result['paymentways'][i][0] );
					if( result['paymentways'][i][0] == paymentway_id )
					{
						paymentway.selectedIndex = i;
					}
				}
				UpdateOrderParameters();
			}
		}
	);
}

function PhotosOnSubmit() {
	var point_id = $('point_id');
	var id = point_id.options[point_id.selectedIndex].value;
	var retval = true;
	
	if( $('delivery_id').selectedIndex == 0 && id == 24 ) {
		Alert.message("Выберите место выдачи заказа");
		retval = false;
	}
	else if( !Client.isLogged() ) {
		Events.publish('/Login/Show',[{gotourl:'/photos/order2'}]);
		retval = false;
	}
	
	return(retval);
}

/* submenu */
var photos_submenu = null;
function PhotosShowSubmenu( event, photo_id, preview_url, preview_width, preview_height, preview_title, preview_title2 )
{
	if( photos_submenu != null )
	{
		photos_submenu.parentNode.removeChild( photos_submenu );
		photos_submenu = null;
	}
	
	var x=0, y=0;
	x = Event.pointerX( event );
	y = Event.pointerY( event );
	x += 4; y += 4;
	photos_submenu = new Element( "div", {"id":"photos_submenu_"+ photo_id,"class":"photos_submenu","style":"background-image:url(/app/views/images/opacity1.png)"} );
	photos_submenu.setStyle( {
		"position":"absolute",
		"z-index": "150",
		"top": y +"px",
		"left": x +"px"
	} );
	photos_submenu.canBeClosed = true;
	photos_submenu.photo_id = photo_id;
	photos_submenu.close = function()
	{
		if( photos_submenu.canBeClosed )
		{
			photos_submenu.parentNode.removeChild( photos_submenu );
			photos_submenu = null;
		}
	}
	photos_submenu.observe( "mouseover", function(event) {
		photos_submenu.canBeClosed = false;
	} );
	photos_submenu.observe( "mouseout", function(event) {
		photos_submenu.canBeClosed = true;
		if( event.relatedTarget.parentNode != photos_submenu && event.relatedTarget != photos_submenu && event.relatedTarget.id != "photo_image_thumnail_"+photos_submenu.photo_id )
		{
			photos_submenu.close();
		}
	} );
	photos_submenu.innerHTML = "<div onclick=\"return PreviewImage(event,'"+ preview_url +"','"+ preview_width +"','"+ preview_height +"','"+ preview_title +"','"+ preview_title2 +"')\">- увеличить</div>";
	photos_submenu.innerHTML += "<div onclick=\"PhotoRotate('"+ photo_id +"','-90')\">- повернуть на 90° по часовой</div>";
	photos_submenu.innerHTML += "<div onclick=\"PhotoRotate('"+ photo_id +"','90')\">- повернуть на 90° против часовой</div>";
	photos_submenu.innerHTML += "<div onclick=\"PhotoRotate('"+ photo_id +"','180')\">- повернуть на 180°</div>";
	document.body.appendChild( photos_submenu );
	
	Event.observe( "photo_image_thumnail_"+photos_submenu.photo_id, "mousemove", function( event ) {
		if( photos_submenu != null )
		{
			var x=0, y=0;
			x = Event.pointerX( event );
			y = Event.pointerY( event );
			
			pX = photos_submenu.cumulativeOffset().left;
			pY = photos_submenu.cumulativeOffset().top;
			
			if( (pX-4) > x )
			{
				photos_submenu.setStyle( {
					"left": (x+4) +"px"
				});
			}
			
			if( (pY-4) > y )
			{
				photos_submenu.setStyle( {
					"top": (y+4) +"px"
				});
			}
		}
		
	} );
}

function PreviewImage( event, image, width, height, close_logo, close_text ) {
	return hs.expand( null, {
		src: image,
		slideshowGroup: 1
	});
}

function PhotosHideSubmenu()
{
	if( photos_submenu != null )
	{
		var t = setTimeout( function() {
			if( photos_submenu != null ) photos_submenu.close();
		}, 200 );
	}
}

function PhotoRotate( photo_id, degree )
{
	var d;
	
	LoadPhotosForm( load_photo_start_msg, page );
	if( photos_submenu != null )
	{
		photos_submenu.canBeClosed = true;
		photos_submenu.close();
	}
	
	d = new Date();
	JsHttpRequest.query(
		'/?photos.photo_rotate&date='+ d.getTime(),
		{
			'id': photo_id,
			'degree': degree
		},
		function( result, txt )
		{
			if( photos_submenu != null )
			{
				photos_submenu.canBeClosed = true;
				photos_submenu.close();
			}
			LoadPhotosForm( load_photo_start_msg, page );
		}
	);
}
/* submenu */


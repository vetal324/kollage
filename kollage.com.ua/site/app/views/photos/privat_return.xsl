<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../layout.xsl" />

<xsl:template match="data">
	<h1><span class="red"><xsl:value-of select="$locale/photos/order/title"/></span><xsl:value-of select="$locale/photos/order/title5"/></h1>
	
	<xsl:if test="/content/global/client/id = 0">
		<script type="text/javascript">
		$j(function(){
			Events.publish('/Login/Show');
		});
		</script>
	</xsl:if>
	
	<xsl:if test="/content/global/client/id > 0">
			<div class="p1">
				<xsl:value-of select="$locale/photos/order/order_number" disable-output-escaping="yes"/><xsl:value-of select="order_number"/><br/>
				<xsl:choose>
					<xsl:when test="responsecode=1 and reasoncode=1">
						<xsl:value-of select="$locale/liqpay/messages/success" disable-output-escaping="yes"/>
					</xsl:when>
					<xsl:otherwise test="status = 'failure'">
						<xsl:value-of select="$locale/liqpay/messages/failure" disable-output-escaping="yes"/><br/><br/>
						responsecode = <xsl:value-of select="responsecode"/><br/>
						reasoncode = <xsl:value-of select="reasoncode"/><br/>
						reasoncodedesc = <xsl:value-of select="reasoncodedesc"/><br/>
					</xsl:otherwise>
				</xsl:choose>
			</div>
	</xsl:if>
	
</xsl:template>

</xsl:stylesheet>

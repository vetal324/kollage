<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../layout.xsl" />

<xsl:template match="data">
	<h1><span class="red"><xsl:value-of select="$locale/photos/order/title"/></span><xsl:value-of select="$locale/photos/order/title5"/></h1>
	
	<xsl:if test="/content/global/client/id = 0">
		<script type="text/javascript">
		$j(function(){
			Events.publish('/Login/Show');
		});
		</script>
	</xsl:if>
	
	<xsl:if test="/content/global/client/id > 0">
		<!-- ORDER FORM -->
		<table cellpadding="3" cellspacing="1" class="sop-order-content_table">
			<tr>
				<th colspan="2" class="sop-order-number"><xsl:value-of select="$locale/photos/order/order_number/text()"/><xsl:value-of select="order/order_number"/></th>
			</tr>
			
			<tr>
				<td colspan="2">
					<b><xsl:value-of select="$locale/photos/order/title2/text()"/></b> (<span name="photos_count" id="photos_count"><xsl:value-of select="order/photos_count"/></span>&#160;<xsl:value-of select="$locale/common/items_amount/text()"/>)
				</td>
			</tr>
			<tr>
				<th width="50%" valign="top" style="padding-left: 25px;">
					<table cellpadding="0" cellspacing="0">
						<tr><th>
							<table cellpadding="0" cellspacing="0">
								<tr>
									<th class="normal"><xsl:value-of select="$locale/photos/order/total_photos/text()"/></th>
									<th class="normal"><span name="total_photos" id="total_photos"><xsl:value-of select="order/photos_price_amount"/></span>&#160;<xsl:value-of select="$locale/common/currency/text()"/></th>
								</tr>
								<tr>
									<th class="normal"><xsl:value-of select="$locale/photos/order/total_delivery/text()"/></th>
									<th class="normal"><span name="total_delivery" id="total_delivery"><xsl:value-of select="order/delivery"/></span>&#160;<xsl:value-of select="$locale/common/currency/text()"/></th>
								</tr>
								<tr>
									<th class="normal"><xsl:value-of select="$locale/photos/order/total_discount/text()"/></th>
									<th class="normal"><span name="total_discount" id="total_discount"><xsl:value-of select="order/discount_price"/></span>&#160;<xsl:value-of select="$locale/common/currency/text()"/></th>
								</tr>
								<tr>
									<th><xsl:value-of select="$locale/photos/order/total_price/text()"/></th>
									<th><span name="total_price" id="total_price"><xsl:value-of select="order/total_price"/></span>&#160;<xsl:value-of select="$locale/common/currency/text()"/></th>
								</tr>
							</table>
						</th></tr>
					</table>
				</th>
				
				<td width="50%" valign="top">
					<table cellpadding="0" cellspacing="3">
						<xsl:choose>
						<xsl:when test="order/photos/photo[position()=1]/size/description != ''">
						<tr>
							<td class="sop-order-ftitle">
								<xsl:value-of select="order/photos/photo[position()=1]/size/description" disable-output-escaping="yes"/>
							</td>
						</tr>
						</xsl:when>
						<xsl:otherwise>
						<tr>
							<td class="sop-order-ftitle"><xsl:value-of select="$locale/photos/order/photo_paper/text()"/>: 
								<xsl:if test="order/photo_paper = 1 or order/photo_paper = ''"><xsl:value-of select="$locale/photos/order/photo_paper1/text()"/></xsl:if>
								<xsl:if test="order/photo_paper = 2"><xsl:value-of select="$locale/photos/order/photo_paper2/text()"/></xsl:if>
							</td>
						</tr>
					
						<tr>
							<td class="sop-order-ftitle"><xsl:value-of select="$locale/photos/order/border/text()"/>: 
								<xsl:if test="order/border = 1"><xsl:value-of select="$locale/common/exist/text()"/></xsl:if>
								<xsl:if test="order/border = 0 or order/border=''"><xsl:value-of select="$locale/common/no/text()"/></xsl:if>
							</td>
						</tr>
						</xsl:otherwise>
						</xsl:choose>
					
						<tr>
							<td class="sop-order-ftitle"><xsl:value-of select="$locale/photos/order/delivery_id/text()"/>: <xsl:value-of select="deliveries/delivery[id=../../order/delivery_id]/name"/></td>
						</tr>
					
						<tr>
							<td class="sop-order-ftitle"><xsl:value-of select="$locale/photos/order/point/text()"/>: <xsl:value-of select="points/point[id=../../order/point_id]/name"/></td>
						</tr>
					
						<tr>
							<td class="sop-order-ftitle"><xsl:value-of select="$locale/photos/order/paymentway_id/text()"/>: <xsl:value-of select="paymentways/paymentway[id=../../order/paymentway_id]/name"/></td>
						</tr>
						
					</table>
				</td>
			</tr>
			
			<tr>
				<td colspan="2">
				<!-- PHOTOS -->
					<div class="error2"><xsl:value-of select="msg"/></div>
					<div><input type="button" class="button" value="{$locale/photos/order/back_order/text()}" onclick="document.location='/{$controller_name}/order'"/></div>
				<!-- PHOTOS - END -->
				</td>
			</tr>
				
		</table>
		<!-- ORDER FORM - END -->
	</xsl:if>
	
</xsl:template>

</xsl:stylesheet>

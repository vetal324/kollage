<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />

<xsl:template match="/">
	<div id="photos_order_container" style="margin:0;background-color:#eeeeee;background-image:none" timestamp="{/content/documents/data/@timestamp}">
	
	<xsl:if test="/content/documents/data/order/id > 0">
	
		<!-- ORDER PHOTOS FORM -->
		<xsl:choose>
		<xsl:when test="count(/content/documents/data/order/photos/*) > 0">
			<xsl:apply-templates select="/content/documents/data/order/photos"/>
		</xsl:when>
		<xsl:otherwise>
			<div style="text-align:center;vertical-align:top;margin:10px 0;">
				<iframe width="420" height="315" src="http://www.youtube.com/embed/95rp1-KL8uI?wmode=transparent" frameborder="0" allowfullscreen="1">Видеоинструкция</iframe>
			</div>
		</xsl:otherwise>
		</xsl:choose>
		<!-- ORDER PHOTOS FORM - END -->
		
	</xsl:if>
	
	</div>
</xsl:template>

<xsl:template match="photo">
<xsl:variable name="size_id" select="size_id"/>
<div style="clear:none;float:left;margin:3px;background-color:#ffffff;text-align:center">

		<xsl:choose>
			<xsl:when test="thumbnail != ''">
				<div style="margin-bottom:2px;position:relative;border:1px solid #fff">
					<img src="{$sop_path}/{../../order_number}/{thumbnail}?{../../../@timestamp}" id="photo_image_thumnail_{id}" width="120" height="90" border="0" align="center" onmouseover="PhotosShowSubmenu(event,{id},'{$sop_path}/{../../order_number}/{small}?{../../../@timestamp}','{small/@width}','{small/@height}','{$locale/photos/order/zoom_logo2/text()}','{$locale/photos/order/zoom_logo2/text()}')" onmouseout="PhotosHideSubmenu()"/>
				</div>
			</xsl:when>
			<xsl:otherwise>
				<div style="margin-bottom:2px"><img src="{$views_path}/images/opacity1.png" border="0" width="120" height="90" align="center"/></div>
			</xsl:otherwise>
		</xsl:choose>
		
		<div style="clear:left;float:left"><input type="hidden" name="photo_id{position()}" id="photo_id{position()}" value="{id}"/><input type="checkbox" name="photo_del{position()}" id="photo_del{position()}"/></div>
		<div style="clear:none;float:left"><input type="text" class="sop-order-inputbox2" name="photo_quantity{position()}" id="photo_quantity{position()}" value="{quantity}" onchange="UpdatePhotoParameters({position()})"/></div>
		<div style="clear:none;float:left;padding:4px 2px 0 2px">X</div>
		<div style="clear:none;float:left">
			<select class="selectbox" name="photo_size{position()}" id="photo_size{position()}" onchange="UpdatePhotoParameters({position()})">
				<xsl:for-each select="/content/documents/data/photo_sizes/*">
					<xsl:call-template name="gen-option">
						<xsl:with-param name="value" select="id"/>
						<xsl:with-param name="title" select="name"/>
						<xsl:with-param name="selected" select="$size_id"/>
					</xsl:call-template>
				</xsl:for-each>
			</select>
		</div>
</div>
</xsl:template>

</xsl:stylesheet>

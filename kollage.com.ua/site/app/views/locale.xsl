<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:variable name="locale-filename">
    <xsl:value-of select="$lang"/>
    <xsl:text>.xml</xsl:text>
</xsl:variable>

<xsl:variable name="locale" select="document ($locale-filename)/locale"/>

</xsl:stylesheet>

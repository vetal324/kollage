<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:template name="login_form2">
		<xsl:param name="show_print_video"/>
		
	<!-- LOGIN BOX -->
	<table width="100%" cellpadding="0" cellspacing="0" class="login2">
		<tr>
			<td valign="top" align="left">
				<p><xsl:value-of select="$locale/photos/order/login_form/form_title/text()" disable-output-escaping="yes"/></p>
				<table cellpadding="0" cellspacing="1">
				
					<xsl:if test="/content/global/errors/error_login = 1">
					<tr><td class="error"><xsl:value-of select="$locale/photos/order/login_form/error_login/text()" disable-output-escaping="yes"/></td></tr>
					</xsl:if>
					
					<form method="post" action="/login/?url=&#63;{$controller_name}.{$method_name}">
					<tr>
						<td class="ftitle"><label for="login"><xsl:value-of select="$locale/photos/order/login_form/username/text()" disable-output-escaping="yes"/></label></td>
					</tr>
					<tr>
						<td><input type="text" name="login" id="login" class="small" value="{/content/global/errors/error_login/@data}"/></td>
					</tr>
					<tr>
						<td class="ftitle"><label for="password"><xsl:value-of select="$locale/photos/order/login_form/password/text()" disable-output-escaping="yes"/></label></td>
					</tr>
					<tr>
						<td><input type="password" name="password" id="password" class="small"/></td>
					</tr>
					<tr>
						<td style="padding: 5px 0 5px 0;"><input type="submit" value="{$locale/photos/order/login_form/form_submit/text()}" class="button"/></td>
					</tr>
					</form>
					<tr>
						<td><small><a href="#" onclick="popup_window(400,200,'?{$controller_name}.password_reminder')"><xsl:value-of select="$locale/photos/order/login_form/forget_password/text()" disable-output-escaping="yes"/></a></small></td>
					</tr>
					<tr>
						<td><small><a href="#" onclick="popup_window(550,640,'?{$controller_name}.register_form')"><xsl:value-of select="$locale/photos/order/login_form/register_link/text()" disable-output-escaping="yes"/></a></small></td>
					</tr>
				</table>
			</td>
			
			<xsl:if test="$show_print_video = 1">
			<td valign="top" align="center">
				<h2>�����-����������:</h2>
				<iframe width="420" height="315" src="http://www.youtube.com/embed/95rp1-KL8uI?wmode=transparent" frameborder="0" allowfullscreen="1"></iframe>
			</td>
			</xsl:if>
		</tr>
	</table>
	<!-- LOGIN BOX - END -->
</xsl:template>

</xsl:stylesheet>

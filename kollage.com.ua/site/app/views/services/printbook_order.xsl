<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../layout.xsl" />
<xsl:include href="../fine-uploader.xsl" />

<xsl:template match="data">
	<xsl:call-template name="fine_uploader_layouts"/>

	<h1><span class="red"><xsl:value-of select="$locale/photobook/order/printbook_title" disable-output-escaping="yes"/></span><xsl:value-of select="$locale/photobook/order/printbook_title3" disable-output-escaping="yes"/></h1>
	
	<xsl:if test="/content/global/client/id = 0">
		<script type="text/javascript">
		$j(function(){
			Events.publish('/Login/Show');
		});
		</script>
	</xsl:if>
	
	<xsl:if test="/content/global/client/id > 0">
		<script language="JavaScript" src="{$views_path}/{$controller_name}/functions.js"></script>
		<div id="dlg-add-images-flash-container" class="hidden" title="{$locale/my/files/upload/title}">
			<div id="dlg-content">
				<table cellpadding="10" cellspacing="0" class="content_table" width="100%" height="100%">
					<tr>
						<td width="*" valign="top">
							<xsl:value-of select="$locale/my/files/upload/help/text()" disable-output-escaping="yes"/>
							<div style="margin-top:10px">
								<table cellspacing="0" cellpadding="0" width="100%">
									<tr>
										<td align="left" colspan="2"><b>�������</b></td>
									</tr>
									<tr>
										<td align="left">������� ��������</td>
										<td align="right"><div id="tdAverageSpeed">-</div></td>
									</tr>
									<tr>
										<td align="left">����� ��������</td>
										<td align="right"><div id="tdTimeRemainingQueue">-</div></td>
									</tr>
									<tr>
										<td align="left">������</td>
										<td align="right"><div id="tdTimeElapsedQueue">-</div></td>
									</tr>
								</table>
								<br/>
								<table cellspacing="0" cellpadding="0" width="100%">
									<tr>
										<td align="left" colspan="2"><b>����������</b></td>
									</tr>
									<tr>
										<td align="left" colspan="2">
											<p>��� ��������� �������� �������� � ������������ ����������� ����������� ������������� �������� ���������� ������:</p>
											<p>
												<table cellspacing="0" cellpadding="0" width="100%">
													<tr>
														<td>�������</td>
														<td><b>cover.jpg</b></td>
													</tr>
													<tr>
														<td>�������� ������</td>
														<td><b>f1.jpg</b></td>
													</tr>
													<tr>
														<td>������ ������</td>
														<td><b>f2.jpg</b></td>
													</tr>
													<tr>
														<td>�������� �������</td>
														<td><b>01.jpg, 02.jpg, ...</b></td>
													</tr>
												</table>
											</p>
											<p>
												<p>��� ������������� ��������������:</p>
												<table cellspacing="0" cellpadding="0" width="100%">
													<tr>
														<td>�������</td>
														<td><b>cover_<i>01</i>.jpg</b></td>
													</tr>
													<tr>
														<td>�������� ������</td>
														<td><b>f1_<i>01</i>.jpg</b></td>
													</tr>
													<tr>
														<td>������ ������</td>
														<td><b>f2_<i>01</i>.jpg</b></td>
													</tr>
													<tr>
														<td>�������� �������</td>
														<td><b>01_<i>01</i>.jpg, ...</b></td>
													</tr>
												</table>
												<p><i>*��� ������� � ������������ <b>"01"</b> ��������� ����� ����������</i></p> 
											</p>
											<a href="/services/article/18/291" target="_kollage_tech">����������� ����������</a>
										</td>
									</tr>
								</table>
							</div>
						</td>
						<td width="420" valign="top">
							<div id="fine-uploader-manual-trigger"></div>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<!-- UPLOAD POPUP -->
		
		<xsl:value-of select="pb_calc" disable-output-escaping="yes"/>
		
		<div>
			<input type="button" style="margin:20px 10px 20px 0px" onclick="ShowUploadPrintbookFilesForm()" id="btn_printbook_select_files"  class="button" value="{$locale/photobook/order/btn_upload}"/>
			<input type="button" style="margin:20px 10px 20px 0px" onclick="pb_order.delete_selected_items()" id="btn_printbook_delete_selected_items"  class="button" value="{$locale/photobook/order/btn_delete_selected_files}"/>
			<input type="button" style="margin:20px 10px 20px 0px" onclick="pb_order.delete_all_items()" id="btn_printbook_delete_all_items"  class="button" value="{$locale/photobook/order/btn_delete_all_files}"/>
		</div>

		<div>
			<input type="button" style="margin:0 10px 30px 0px" onclick="pb_order.continue_order()" id="btn_printbook_make_order"  class="button-green" value="{$locale/photobook/order/btn_printbook_make_order}"/>
		</div>
		
		<div id="pb_uploaded_items" class="pb_order_items"></div>
		
		<script type="text/javascript">
			var order_update_flag = '<xsl:value-of select="order_update_flag"/>'; 
			
			$j(function(){
				$j('#dlg-add-images-flash-container').dialog({
					autoOpen: false,
					resizable: false,
					minHeight: 430,
					minWidth: 720,
					modal: true,
					open: function(event, ui){
						if( Client &amp;&amp; !Client.isLogged() ) {
						}
						else {
						}
					}
				});
				
				if( order_update_flag == 1 ) {
					pb_order.update( pb );
				}
				else {
					pb_order.get_order( function( result ) {
						var order = result.pb_order;
						if( order ) {
							pb.applyParametersFromOrder( pb_order.make_params_for_calculator(order) );
						}
					});
				}
			});
			
			pb_order.init( 'pb_uploaded_items' );
		</script>
		
	</xsl:if>
</xsl:template>

</xsl:stylesheet>

<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../layout.xsl" />

<xsl:template match="data">
	<h1><span class="red"><xsl:value-of select="$locale/photobook/order/printbook_title" disable-output-escaping="yes"/></span><xsl:value-of select="$locale/photobook/order/printbook_title3" disable-output-escaping="yes"/></h1>
	
	<xsl:if test="/content/global/client/id = 0">
		<script type="text/javascript">
		$j(function(){
			Events.publish('/Login/Show');
		});
		</script>
	</xsl:if>
	
	<xsl:if test="/content/global/client/id > 0">
		<!-- UPC FORM -->
			<div class="p1">
				<span style="color:#336699"><xsl:value-of select="$locale/upc/wait/text()" disable-output-escaping="yes"/></span>
				<form id="upc" action="" method="post">
					<input type="hidden" name="Version" value="1"/>
					<input type="hidden" name="MerchantID" value="{upc_MerchantID}"/>
					<input type="hidden" name="TerminalID" value="{upc_TerminalID}"/>
					<input type="hidden" name="TotalAmount" value="{upc_TotalAmount}"/>
					<input type="hidden" name="Currency" value="{upc_Currency}"/>
					<xsl:if test="upc_AltTotalAmount">
						<input type="hidden" name="AltTotalAmount" value="{upc_AltTotalAmount}"/>
						<input type="hidden" name="AltCurrency" value="{upc_AltCurrency}"/>
					</xsl:if>
					<input type="hidden" name="locale" value="ru"/>
					<input type="hidden" name="SD" value="{upc_SD}"/>
					<input type="hidden" name="OrderID" value="{upc_OrderID}"/>
					<input type="hidden" name="PurchaseTime" value="{upc_PurchaseTime}"/>
					<input type="hidden" name="Signature" value="{upc_Signature}"/>
				</form>
				<script>
				$j(function OnLoad()
				{
					//document.getElementById('upc').action = 'https://secure.upc.ua/ecgtest/enter';
					document.getElementById('upc').action = 'https://secure.upc.ua/go/enter';
					document.getElementById('upc').submit();
				});
				</script>
			</div>
		<!-- UPC FORM - END -->
	</xsl:if>
	
</xsl:template>

</xsl:stylesheet>

<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../layout.xsl" />

<xsl:template match="data">
	<h1><span class="red"><xsl:value-of select="$locale/photobook/order/printbook_title" disable-output-escaping="yes"/></span><xsl:value-of select="$locale/photobook/order/printbook_title3" disable-output-escaping="yes"/></h1>
	
	<xsl:if test="/content/global/client/id = 0">
		<script type="text/javascript">
		$j(function(){
			Events.publish('/Login/Show');
		});
		</script>
	</xsl:if>
	
	<xsl:if test="/content/global/client/id > 0">
		<!-- ORDER RESULT -->
		<table cellpadding="3" cellspacing="1" class="content_table">
			<tr>
				<th colspan="2"><xsl:value-of select="$locale/photos/order/order_number/text()"/><xsl:value-of select="order/order_number"/> (<span name="photos_count" id="photos_count"><xsl:value-of select="order/quantity"/></span>&#160;<xsl:value-of select="$locale/common/items_amount/text()"/>)</th>
			</tr>
			<tr>
				<th width="50%" valign="top" style="padding-left: 25px;">
					<table cellpadding="0" cellspacing="0" class="photobook order">
						<tr><th>
							<table cellpadding="0" cellspacing="0">
								<tr>
									<th class="normal"><xsl:value-of select="$locale/photobook/calc/quantity" disable-output-escaping="yes"/></th>
									<th class="normal"><span name="quantity" id="quantity"><xsl:value-of select="order/quantity"/></span></th>
								</tr>
								<tr>
									<th class="normal"><xsl:value-of select="$locale/photobook/calc/price_one" disable-output-escaping="yes"/></th>
									<th class="normal"><span name="calculated_price_one" id="calculated_price_one"><xsl:value-of select="order/calculated_price_one"/></span>&#160;<xsl:value-of select="$locale/common/currency/text()"/></th>
								</tr>
								<tr>
									<th class="normal"><xsl:value-of select="$locale/photobook/calc/price_design" disable-output-escaping="yes"/></th>
									<th class="normal"><span name="price_design_work" id="price_design_work"><xsl:value-of select="order/calculated_price_design_work"/></span>&#160;<xsl:value-of select="$locale/common/currency/text()"/></th>
								</tr>
								<tr>
									<th class="normal"><xsl:value-of select="$locale/photos/order/total_delivery/text()"/></th>
									<th class="normal"><span name="total_delivery" id="total_delivery"><xsl:value-of select="order/delivery_price"/></span>&#160;<xsl:value-of select="$locale/common/currency/text()"/></th>
								</tr>
								<tr>
									<th class="normal"><xsl:value-of select="$locale/photos/order/total_discount/text()"/></th>
									<th class="normal"><span name="total_discount" id="total_discount"><xsl:value-of select="order/discount_price"/></span>&#160;<xsl:value-of select="$locale/common/currency/text()"/></th>
								</tr>
								<tr>
									<th><xsl:value-of select="$locale/photos/order/total_price/text()"/></th>
									<th><span name="total_price" id="total_price"><xsl:value-of select="order/total_price"/></span>&#160;<xsl:value-of select="$locale/common/currency/text()"/></th>
								</tr>
							</table>
						</th></tr>
					</table>
				</th>
				
				<td width="50%" valign="top">
					<table cellpadding="0" cellspacing="3" class="photobook order">
						<tr>
							<td class="normal"><xsl:value-of select="$locale/photos/order/delivery_id/text()"/>: <xsl:value-of select="order/delivery/name"/></td>
						</tr>
					
						<tr>
							<td class="normal"><xsl:value-of select="$locale/photos/order/paymentway_id/text()"/>: <xsl:value-of select="order/paymentway/name"/></td>
						</tr>
						
					</table>
				</td>
			</tr>
		</table>
		
		<p><xsl:value-of select="order/paymentway/description" disable-output-escaping="yes"/></p>
		
		<!-- ORDER RESULT - END -->
	</xsl:if>
</xsl:template>

</xsl:stylesheet>

<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../layout.xsl" />

<xsl:template match="data">
	<h1><span class="red"><xsl:value-of select="$locale/photobook/order/printbook_title" disable-output-escaping="yes"/></span><xsl:value-of select="$locale/photobook/order/printbook_title3" disable-output-escaping="yes"/></h1>
	
	<xsl:if test="/content/global/client/id = 0">
		<script type="text/javascript">
		$j(function(){
			Events.publish('/Login/Show');
		});
		</script>
	</xsl:if>
	
	<xsl:if test="/content/global/client/id > 0">
		<script language="JavaScript" src="{$views_path}/{$controller_name}/functions.js"></script>
		<xsl:choose>
		
			<xsl:when test="error_messages != ''">
				<br/><br/>
				<div class="error2"><xsl:value-of select="$locale/photobook/order/error_title" disable-output-escaping="yes"/></div>
				<ul><xsl:value-of select="error_messages" disable-output-escaping="yes"/></ul>
				<input type="button" onclick="document.location='/?services.printbook_order'" value="&lt;&lt; ��������� � �������������� ������" class="button-green" style="margin:20px 10px 20px 0px"/>
			</xsl:when>
			
			<xsl:otherwise>
				<form action="/services/printbook_order3" method="post" name="order">
				<table width="100%"><tr>
					<td width="40%" valign="top">
						<table class="photobook order">
							<tr>
								<th><label><xsl:value-of select="$locale/photobook/calc/quantity" disable-output-escaping="yes"/></label></th>
								<td><div class="price_regular" id="quantity"><xsl:value-of select="order/quantity"/></div></td>
							</tr>
							<tr>
								<th><label><xsl:value-of select="$locale/photobook/calc/price_one" disable-output-escaping="yes"/></label></th>
								<td><div class="price_regular" id="price_one"><xsl:value-of select="order/calculated_price_one"/></div></td>
							</tr>
							<tr>
								<th><label><xsl:value-of select="$locale/photobook/calc/price_design" disable-output-escaping="yes"/></label></th>
								<td><div class="price_regular" id="price_design_work"><xsl:value-of select="order/calculated_price_design_work"/></div></td>
							</tr>
							<tr>
								<th><label><xsl:value-of select="$locale/photobook/order/price_delivery" disable-output-escaping="yes"/></label></th>
								<td><div class="price_regular" id="price_delivery">0.00</div></td>
							</tr>
							<tr>
								<th><label><xsl:value-of select="$locale/photobook/calc/price_total" disable-output-escaping="yes"/></label></th>
								<td><div class="price" id="price_total"><xsl:value-of select="order/price_amount"/></div></td>
							</tr>
						</table>
					</td>
					<td width="60%" valign="top">
			<table cellpadding="0" cellspacing="3" class="photobook order">
			
				<tr>
					<th><label><xsl:value-of select="$locale/photos/order/delivery_id/text()"/></label></th>
				</tr>
				<tr>
					<td>
						<xsl:variable name="delivery_id" select="order/delivery_id"/>
						<select name="object[delivery_id]" class="middle" id="delivery_id" onchange="LoadPaymentways($('delivery_id'),$('paymentway_id'),{order/paymentway_id});">
							<xsl:for-each select="deliveries/*">
								<xsl:call-template name="gen-option">
									<xsl:with-param name="value" select="id"/>
									<xsl:with-param name="title" select="name"/>
									<xsl:with-param name="selected" select="$delivery_id"/>
								</xsl:call-template>
							</xsl:for-each>
						</select>
					</td>
				</tr>
			
				<tr>
					<th><label><xsl:value-of select="$locale/photos/order/paymentway_id/text()"/></label></th>
				</tr>
				<tr>
					<td>
						<xsl:variable name="paymentway_id" select="order/paymentway_id"/>
						<select name="object[paymentway_id]" class="middle" id="paymentway_id" onchange="UpdateOrderParameters()">
						</select>
					</td>
				</tr>
				
				<tr>
					<th><label><xsl:value-of select="$locale/photos/order/comments/text()"/></label></th>
				</tr>
				<tr>
					<td><textarea cols="30" rows="3" name="object[comments]" id="comments" style="width:260px;height:50px"><xsl:value-of select="order/comments"/></textarea></td>
				</tr>
				
			</table>
			<input type="button" onclick="document.forms['order'].submit()" value="��������" class="button" style="margin:20px 10px 20px 0px"/>
			<input type="button" onclick="document.location='/?services.printbook_order'" value="&lt;&lt; ��������� � �������������� ������" class="button-green" style="margin:20px 10px 20px 0px"/>
			<script type="text/javascript">
				LoadPaymentways($('delivery_id'),$('paymentway_id'),<xsl:value-of select="order/paymentway_id"/>)
			</script>
					</td>
				</tr></table>
				</form>
			</xsl:otherwise>
			
		</xsl:choose>
	</xsl:if>
</xsl:template>

</xsl:stylesheet>

<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../layout.xsl" />

<xsl:template match="data">
	<h1 class="red"><xsl:value-of select="article/header"/></h1>
	<xsl:value-of select="article/text_transformed" disable-output-escaping="yes"/>
</xsl:template>

</xsl:stylesheet>

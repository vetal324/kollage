<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../layout.xsl" />

<xsl:template match="data">
	<h1><span class="red"><xsl:value-of select="$locale/photobook/order/printbook_title" disable-output-escaping="yes"/></span><xsl:value-of select="$locale/photobook/order/printbook_title3" disable-output-escaping="yes"/></h1>
	
	<xsl:if test="/content/global/client/id = 0">
		<script type="text/javascript">
		$j(function(){
			Events.publish('/Login/Show');
		});
		</script>
	</xsl:if>
	
	<xsl:if test="/content/global/client/id > 0">
			<div class="p1">
				<xsl:value-of select="order_number"/><br/>
				<xsl:choose>
					<xsl:when test="status = 'success'">
						<xsl:value-of select="$locale/liqpay/messages/success" disable-output-escaping="yes"/>
					</xsl:when>
					<xsl:when test="status = 'failure'">
						<xsl:value-of select="$locale/liqpay/messages/failure" disable-output-escaping="yes"/>
					</xsl:when>
					<xsl:when test="status = 'wait_secure'">
						<xsl:value-of select="$locale/liqpay/messages/wait_secure" disable-output-escaping="yes"/>
					</xsl:when>
				</xsl:choose>
			</div>
	</xsl:if>
	
	<!-- <xsl:call-template name="new_products"/> -->
	<!-- <xsl:call-template name="best_products"/> -->
	
</xsl:template>

</xsl:stylesheet>

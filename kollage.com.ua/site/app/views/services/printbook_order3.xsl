<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../layout.xsl" />

<xsl:template match="data">
	<h1><span class="red"><xsl:value-of select="$locale/photobook/order/printbook_title" disable-output-escaping="yes"/></span><xsl:value-of select="$locale/photobook/order/printbook_title3" disable-output-escaping="yes"/></h1>
	
	<xsl:if test="/content/global/client/id = 0">
		<script type="text/javascript">
		$j(function(){
			Events.publish('/Login/Show');
		});
		</script>
	</xsl:if>
	
	<xsl:if test="/content/global/client/id > 0">
		<script language="JavaScript" src="{$views_path}/{$controller_name}/functions.js"></script>
		<xsl:choose>
		
			<xsl:when test="error_messages != ''">
				<br/><br/>
				<div class="error2"><xsl:value-of select="$locale/photobook/order/error_title" disable-output-escaping="yes"/></div>
				<ul><xsl:value-of select="error_messages" disable-output-escaping="yes"/></ul>
				<input type="button" onclick="document.location='/services/printbook_order2'" value="&lt;&lt; ��������� � �������������� ������" class="button-green" style="margin:20px 10px 20px 0px"/>
			</xsl:when>
			
			<xsl:otherwise>
			</xsl:otherwise>
			
		</xsl:choose>
	</xsl:if>
</xsl:template>

</xsl:stylesheet>

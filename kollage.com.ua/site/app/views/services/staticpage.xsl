<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../layout.xsl" />

<xsl:template match="data">
	<xsl:for-each select="css/item">
		<xsl:choose>
			<xsl:when test="contains(., '_ie.css')">
				<xsl:text disable-output-escaping="yes">&lt;!--[if lt IE 9]&gt;</xsl:text><link rel="stylesheet" href="{.}" type="text/css" charset="utf-8"/><xsl:text disable-output-escaping="yes">&lt;![endif]--&gt;</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<link rel="stylesheet" href="{.}" type="text/css" charset="utf-8"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:for-each>
	<xsl:value-of select="content" disable-output-escaping="yes"/>
</xsl:template>

</xsl:stylesheet>

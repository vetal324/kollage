<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />

<xsl:template match="/">
	<xsl:apply-templates select="content/documents/data"/>
</xsl:template>

<xsl:template match="data">
	<div class="printbook">
		<div class="fotobook-zakaz">
			<div class="fotobook-zakaz-left">
				<div class="book-format" id="book-format">
					<label class="printbook-displayblock"><xsl:value-of select="$locale/photobook/calc/format" disable-output-escaping="yes"/></label><div class="comment">&#160;</div><select id="pb_format"></select>
					<div style="clear: both;"></div>
				</div>
				
				<div class="block-fotobook">
					<label id="pb_pages_label1"><xsl:value-of select="$locale/photobook/calc/pages" disable-output-escaping="yes"/></label><label id="pb_pages_label2"><xsl:value-of select="$locale/photobook/calc/pages2" disable-output-escaping="yes"/></label><div class="comment"><xsl:value-of select="$locale/photobook/calc/pages_comment" disable-output-escaping="yes"/></div><select id="pb_pages"></select>
				</div>
				<div class="block-fotobook">
					<label class="printbook-displayblock"><xsl:value-of select="$locale/photobook/calc/quantity" disable-output-escaping="yes"/></label><div class="comment">&#160;</div>
					<select id="pb_quantity">
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5">5</option>
						<option value="6">6</option>
						<option value="7">7</option>
						<option value="8">8</option>
						<option value="9">9</option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option>
						<option value="13">13</option>
						<option value="14">14</option>
						<option value="15">15</option>
						<option value="16">16</option>
						<option value="17">17</option>
						<option value="18">18</option>
						<option value="19">19</option>
						<option value="20">20</option>
						<option value="21">21</option>
						<option value="22">22</option>
						<option value="23">23</option>
						<option value="24">24</option>
						<option value="25">25</option>
						<option value="26">26</option>
						<option value="27">27</option>
						<option value="28">28</option>
						<option value="29">29</option>
						<option value="30">30</option>
						<option value="31">31</option>
						<option value="32">32</option>
						<option value="33">33</option>
						<option value="34">34</option>
						<option value="35">35</option>
						<option value="36">36</option>
						<option value="37">37</option>
						<option value="38">38</option>
						<option value="39">39</option>
						<option value="40">40</option>
					</select>
				</div>
				
				<div class="block-fotobook">
					<label class="printbook-displayblock"><xsl:value-of select="$locale/photobook/calc/paper" disable-output-escaping="yes"/></label><div class="comment"><xsl:value-of select="$locale/photobook/calc/paper_comment" disable-output-escaping="yes"/></div><select id="pb_paper"></select>
				</div>
				<div class="block-fotobook">
					<div class="wrapper"><label class="printbook-displayblock"><xsl:value-of select="$locale/photobook/calc/lamination" disable-output-escaping="yes"/></label><div class="comment"><xsl:value-of select="$locale/photobook/calc/lamination_comment" disable-output-escaping="yes"/></div><select id="pb_lamination"></select></div>
				</div>
				
				<div class="block-fotobook">
					<div class="wrapper"><label class="printbook-displayblock"><xsl:value-of select="$locale/photobook/calc/forzaces" disable-output-escaping="yes"/></label><div class="comment"><xsl:value-of select="$locale/photobook/calc/forzaces_comment" disable-output-escaping="yes"/></div><select id="pb_forzaces"></select></div>
				</div>
				<div class="block-fotobook">
					<div class="wrapper"><label class="printbook-displayblock"><xsl:value-of select="$locale/photobook/calc/paper_base" disable-output-escaping="yes"/></label><div class="comment"><xsl:value-of select="$locale/photobook/calc/paper_base_comment" disable-output-escaping="yes"/></div><select id="pb_paper_base"></select></div>
				</div>
				
				<div class="block-fotobook">
					<div class="wrapper"><label class="printbook-displayblock"><xsl:value-of select="$locale/photobook/calc/personal_pages" disable-output-escaping="yes"/></label><div class="comment"><xsl:value-of select="$locale/photobook/calc/personal_pages_comment" disable-output-escaping="yes"/></div><select id="pb_personal_pages"></select></div>
				</div>
				<div class="block-fotobook">
					<div class="wrapper"><label class="printbook-displayblock"><xsl:value-of select="$locale/photobook/calc/personal_cover" disable-output-escaping="yes"/></label><div class="comment"><xsl:value-of select="$locale/photobook/calc/personal_cover_comment" disable-output-escaping="yes"/></div><select id="pb_personal_cover"><option value="0">���</option><option value="1">��</option></select></div>
				</div>
				<div style="clear: both;"></div>

				<div id="select_personal_pages" style="display:none"></div>
				<div id="addition_row_space" style="height:0;overflow:hidden"></div>
			</div>
			
			<div class="fotobook-zakaz-right">
				<div id="pb_size_image"></div>
				<div id="pb_box_category_image"></div>				

				<div class="cena" id="sticker">
					<label class="regular regular-dubl">���������, ���</label>
					<div class="price_regular price_regular-dubl" id="price_one2">0.00</div>
							
					<label class="regular regular-dubl">������, ���</label>
					<div class="price_regular price_regular-dubl" id="price_design2">0.00</div>
					
					<label class="regular regular-dubl">����/������, ���</label>
					<div class="price_regular price_regular-dubl" id="price_box2">0.00</div>
					
					<label class="printbook-displayblock printbook-displayblock-dubl">���������, ���</label>
					<div class="price price-dubl" id="price_total2">0.00</div>
				</div>
				<script type="text/javascript">
					$j(function(){
						$j("#sticker").sticky({topSpacing:10});
					});
				</script>
			</div>
			<div style="clear: both;"></div>
			<div class="fotobook-zakaz-bottom">
				<div style="clear: both;"></div>
				
				<div class="cover-category">
					<label class="printbook-displayblock"><xsl:value-of select="$locale/photobook/calc/cover" disable-output-escaping="yes"/></label>
					<div id="pb_cover_category"></div>
					<div style="clear: both;"></div>
				</div>
				<div style="clear: both;"></div>
				<div id="pb_cover_items"></div>
				<div style="clear: both;"></div>
				
				<div class="photobook-forzaces-fotobook">
					<label class="printbook-displayblock"><xsl:value-of select="$locale/photobook/calc/forzaces" disable-output-escaping="yes"/></label>
					<div id="pb_photobook_forzaces"></div>
				</div>
				
				<div style="clear: both;"></div>				
				<div class="box-category-fotobook">
					<label class="printbook-displayblock"><xsl:value-of select="$locale/photobook/calc/box" disable-output-escaping="yes"/></label> <select id="pb_box_category"></select>
				</div>
				<div class="box-description-fotobook">
					<div class="comment_black" id="pb_box_description"></div>
					<div id="pb_box_items"></div>
				</div>
				
				<div class="bdesign-work-category-fotobook">
					<label class="printbook-displayblock"><xsl:value-of select="$locale/photobook/calc/design_work" disable-output-escaping="yes"/></label><div class="comment">�������� ����� </div><select id="pb_design_work_category"></select>
				</div>
				<div class="design-work-description-fotobook">
					<div class="comment_black" id="pb_design_work_description"></div>
				</div>
				<div id="pb_design_work_items"></div>
				<div style="clear: both;"></div>
				<div class="button-fotobook">
					<p></p>
					<p><input class="button" type="button" value="���������"/></p>
				</div>
				<div class="message-fotobook">
					<div id="pb_message"></div>
				</div>
				<div style="clear: both;"></div>
				<div class="fotobook-price">
					<div class="bordered">
						<label class="regular"><xsl:value-of select="$locale/photobook/calc/price_one" disable-output-escaping="yes"/></label>
					</div>
					<div class="bordered">
						<div class="price_regular" id="price_one">0.00</div>
					</div>
				</div>
				<div class="fotobook-price">
					<div class="bordered">
						<label class="regular"><xsl:value-of select="$locale/photobook/calc/price_design" disable-output-escaping="yes"/></label>
					</div>
					<div class="bordered">
						<div class="price_regular" id="price_design">0.00</div>
					</div>
				</div>
				<div class="fotobook-price">
					<div class="bordered">
						<label class="regular"><xsl:value-of select="$locale/photobook/calc/price_box" disable-output-escaping="yes"/></label>
					</div>
					<div class="bordered">
						<div class="price_regular" id="price_box">0.00</div>
					</div>
				</div>
				<div class="fotobook-price">
					<div class="bordered">
						<label class="printbook-displayblock"><xsl:value-of select="$locale/photobook/calc/price_total" disable-output-escaping="yes"/></label>
					</div>
					<div class="bordered">
						<div class="price" id="price_total">0.00</div>
					</div>
				</div>
				<div style="clear: both;"></div>
			</div>	
		</div>
	
	
	
	
		<script type="text/javascript">
		var pb_parameters = {
			pb_format: <xsl:value-of select="pb_format"/>,
			pb_pages: <xsl:value-of select="pb_pages"/>,
			pb_paper: <xsl:value-of select="pb_paper"/>,
			pb_lamination: <xsl:value-of select="pb_lamination"/>,
			pb_paper_base: <xsl:value-of select="pb_paper_base"/>,
			pb_personal_pages: <xsl:value-of select="pb_personal_pages"/>,
			pb_selected_personal_pages: '<xsl:value-of select="pb_selected_personal_pages"/>',
			pb_personal_cover: <xsl:value-of select="pb_personal_cover"/>,
			pb_cover_category: <xsl:value-of select="pb_cover_category"/>,
			pb_cover: <xsl:value-of select="pb_cover"/>,
			pb_supercover: <xsl:value-of select="pb_supercover"/>,
			pb_design_work_category: <xsl:value-of select="pb_design_work_category"/>,
			pb_design_work: <xsl:value-of select="pb_design_work"/>,
			pb_box_category: <xsl:value-of select="pb_box_category"/>,
			pb_box: <xsl:value-of select="pb_box"/>,
			pb_quantity: <xsl:value-of select="pb_quantity"/>,
			pb_forzaces: <xsl:value-of select="pb_forzaces"/>,
			pb_photobook_forzaces: <xsl:value-of select="pb_photobook_forzaces"/>,
			pb_paper_base: <xsl:value-of select="pb_paper_base"/>
		};
		var pb = {
			pb_items: <xsl:value-of select="pb_items"/>,
			options: {
				supercoversID: 'pb_cover_supercovers',
				supercoverSelectID: 'pb_cover_supercover',
				formatItemClass: 'pb-format-item'
			},
			
			price_one: null,
			price_one2: null,
			price_design: null,
			price_design2: null,
			price_box: null,
			price_box2: null,
			price_total: null,
			price_total2: null,
			
			pb_format: null,
			pb_pages: null,
			pb_paper: null,
			pb_lamination: null,
			
			pb_design_work_category: null,
			pb_design_work_items: null,
			
			pb_box_category: null,
			pb_box_items: null,
			
			pb_cover_category: null,
			pb_cover_items: null,
			
			pb_photobook_forzaces: null,

			pb_personal_pages: null,
			pb_personal_cover: null,
			pb_select_personal_pages: null,
			
			pb_forzaces: null,
			pb_paper_base: null,
			
			pb_quantity: null,
			pb_message: null,
			link_to_order_printbook: null,
			btn_printbook_select_files: null,
			
			//on_calculate: null,
			on_calculate_disabled: false,
			
			init: function() {
				var self = this;
				
				this.pb_format = $('pb_format');
				this.initFormats();
				
				this.pb_pages = $('pb_pages');
				this.pb_pages.observe( 'change', this.onChangePages );
				
				this.pb_paper = $('pb_paper');
				this.pb_paper.observe( 'change', this.onChangePaper );
				
				this.pb_lamination = $('pb_lamination');
				this.pb_lamination.observe( 'change', this.onChangeLamination );
				
				this.pb_design_work_category = $('pb_design_work_category');
				this.pb_design_work_category.observe( 'change', this.onChangeDesignWork );
				this.pb_design_work_items = $('pb_design_work_items');
				
				this.pb_box_category = $('pb_box_category');
				this.pb_box_category.observe( 'change', this.onChangeBox );
				this.pb_box_items = $('pb_box_items');
				
				this.pb_cover_category = $('pb_cover_category');
				this.pb_cover_items = $('pb_cover_items');
				
				this.pb_photobook_forzaces = $('pb_photobook_forzaces');
				//this.pb_photobook_forzaces.observe( 'change', this.onChangePhotobookForzaces );
				
				this.pb_forzaces = $('pb_forzaces');
				this.pb_forzaces.observe( 'change', this.onChangeForzaces );
				
				this.pb_paper_base = $('pb_paper_base');
				this.pb_paper_base.observe( 'change', this.onChangePaperBase );
				
				this.pb_personal_pages = $('pb_personal_pages');
				this.pb_personal_pages.observe( 'change', this.onChangePersonalPages );
				
				this.pb_personal_cover = $('pb_personal_cover');
				this.pb_personal_cover.observe( 'change', this.onChangePersonalCover );
				
				this.pb_select_personal_pages = $('select_personal_pages');
				
				this.pb_quantity = $('pb_quantity');
				this.pb_quantity.observe( 'change', this.onChangeQuantity );
				
				this.price_one = $('price_one');
				this.price_one2 = $('price_one2');
				this.price_design = $('price_design');
				this.price_design2 = $('price_design2');
				this.price_box = $('price_box');
				this.price_box2 = $('price_box2');
				this.price_total = $('price_total');
				this.price_total2 = $('price_total2');
				
				this.pb_message = $('pb_message');
				
				this.btn_printbook_select_files = $('btn_printbook_select_files');
				
				this.onChangeFormat();
				this.onChangeQuantity();
				this.applyParameters();
				
				this.calculate();
			},
			
			postInit: function() {
				this.link_to_order_printbook = $('link_to_order_printbook');
				if( this.link_to_order_printbook ) {
					this.link_to_order_printbook.observe( 'click', this.onOrderClick );
				}
			},
			
			applyParameters: function() {
				if( pb_parameters.pb_format ) {
					this.pb_format.selectedIndex = pb_parameters.pb_format;
					this.onChangeFormat();
				}
				if( pb_parameters.pb_quantity ) {
					this.pb_quantity.value = pb_parameters.pb_quantity;
					this.onChangeQuantity();
				}
				if( pb_parameters.pb_pages ) {
					this.pb_pages.selectedIndex = pb_parameters.pb_pages;
					this.onChangePages();
				}
				if( pb_parameters.pb_paper ) {
					this.pb_paper.selectedIndex = pb_parameters.pb_paper;
					this.onChangePaper();
				}
				if( pb_parameters.pb_lamination ) {
					this.pb_lamination.selectedIndex = pb_parameters.pb_lamination;
					this.onChangeLamination();
				}
				if( pb_parameters.pb_personal_pages > 0 ) {
					this.pb_personal_pages.selectedIndex = pb_parameters.pb_personal_pages;
					this.onChangePersonalPages();
				}
				if( pb_parameters.pb_personal_cover ) {
					this.pb_personal_cover.selectedIndex = pb_parameters.pb_personal_cover;
					this.onChangePersonalCover();
				}
				if( pb_parameters.pb_cover_category ) {
					//this.pb_cover_category.selectedIndex = pb_parameters.pb_cover_category;
					$$('input[name="cover_category_item"]').each( function(el) {
						el.checked = false;
					} );
					this.setCheckedItemByAttribute( 'cover_category_item', 'value', pb_parameters.pb_cover_category );
					this.onChangeCover();
				}
				if( pb_parameters.pb_cover > 0 ) {
					$$('input[name="cover_item"]').each( function(el) {
						el.checked = false;
					} );
					this.setCheckedItemByAttribute( 'cover_item', 'value', pb_parameters.pb_cover );				
					this.calculate();
				}
				if( pb_parameters.pb_supercover > 0 ) {
					$$('input[name="supercover_item"]').each( function(el) {
						el.checked = false;
					} );
					this.setCheckedItemByAttribute( 'supercover_item', 'value', pb_parameters.pb_supercover );				
					this.calculate();
				}
				if( pb_parameters.pb_design_work_category ) {
					this.pb_design_work_category.selectedIndex = pb_parameters.pb_design_work_category;
					this.onChangeDesignWork();
				}
				if( pb_parameters.pb_design_work > 0 ) {
					$$('input[name="design_work_item"]').each( function(el) {
						el.checked = false;
					} );
					this.setCheckedItemByAttribute( 'design_work_item', 'value', pb_parameters.pb_design_work );				
					this.calculate();
				}
				if( pb_parameters.pb_box_category ) {
					this.pb_box_category.selectedIndex = pb_parameters.pb_box_category;
					this.onChangeBox();
				}
				if( pb_parameters.pb_box > 0 ) {
					this.setCheckedItemByAttribute( 'box_item', 'value', pb_parameters.pb_box );				
					this.calculate();
				}
				if( pb_parameters.pb_forzaces ) {
					this.pb_forzaces.selectedIndex = pb_parameters.pb_forzaces;
					this.onChangeForzaces();
				}
				if( pb_parameters.pb_photobook_forzaces ) {
					//this.pb_photobook_forzaces.selectedIndex = pb_parameters.pb_photobook_forzaces;
					$$('input[name="photobook_forzace_item"]').each( function(el) {
						el.checked = false;
					} );
					this.setCheckedItemByAttribute( 'photobook_forzace_item', 'value', pb_parameters.pb_photobook_forzaces );
					this.onChangePhotobookForzaces();
				}
				if( pb_parameters.pb_paper_base ) {
					this.pb_paper_base.selectedIndex = pb_parameters.pb_paper_base;
					this.onChangePaperBase();
				}
				if( pb_parameters.pb_selected_personal_pages ) {
					var sppa = pb_parameters.pb_selected_personal_pages.split(',');
					for( var i=0; i&lt;sppa.length; i++ ) {
						pb.checkPersonalPage( sppa[i] );
					}
				}
			},
			
			findIdx: function( name, value, level1, level2 ) {
				var retval = -1;
				var self = pb;
				
				var list = Array();
				var items = self.pb_items;
				
				if( !level1 ) list = items;
				else if( level1 &amp;&amp; !level2 ) list = items[self.pb_format.selectedIndex][level1];
				else if( level1 &amp;&amp; level2 ) list = items[self.pb_format.selectedIndex][level1][level2];
				
				for( var i=0; i&lt;list.length; i++ ) {
					if( value == list[i][name] ) {
						retval = i;
						//console.log( 'findIdx, FOUND name='+ name +', value='+ value +', i='+ i +', list[i][name]=', list[i][name] );
					} 
				}
				
				return( retval );
			},
			
			findParentIdxByChildValue: function( name, value, level1, level2 ) {
				var retval = -1;
				var self = pb;
				
				var items = self.pb_items;
				var list = items[self.pb_format.selectedIndex][level1];
				
				for( var i=0; i&lt;list.length; i++ ) {
					for( var j=0; j&lt;list[i][level2].length; j++ ) {
						if( value == list[i][level2][j][name] ) {
							retval = i;
							break;
						} 
					}
					if( retval >= 0 ) break;
				}
				
				return( retval );
			},

			findParentValueByChildValue: function( name, value, level1, level2 ) {
				var retval = -1;
				var self = pb;

				var items = self.pb_items;
				var list = items[self.pb_format.selectedIndex][level1];

				for( var i=0; i&lt;list.length; i++ ) {
					for( var j=0; j&lt;list[i][level2].length; j++ ) {
						if( value == list[i][level2][j][name] ) {
							retval = list[i].id;
							break;
						}
					}
					if( retval >= 0 ) break;
				}

				return( retval );
			},
			
			findIdxInSelect: function( sel, val ) {
				var retval = -1;
				
				if( sel.options ) {
					for( var i=0; i&lt;sel.options.length; i++ ) {
						if( sel.options[i].value == val ) {
							retval = i;
							break;
						}
					}
				}
				
				return( retval );
			},
			
			applyParametersFromOrder: function( params ) {
				var self = pb;
				
				self.on_calculate_disabled = true;
				if( params.size_id >= 0 ) {
					var idx = self.findIdx( "id", params.size_id, null, null );
					if( idx >= 0 ) {
						self.pb_format.selectedIndex = idx;
						self.onChangeFormat();
					}
				}
				if( params.quantity ) {
					self.pb_quantity.value = params.quantity;
					self.onChangeQuantity();
				}
				if( params.pages > 0 ) {
					var idx = this.findIdxInSelect( self.pb_pages, params.pages );
					if( idx >= 0 ) {
						self.pb_pages.selectedIndex = idx;
						self.onChangePages();
					}
				}
				if( params.paper_id >= 0 ) {
					var idx = self.findIdx( "id", params.paper_id, "papers", null );
					if( idx >= 0 ) {
						self.pb_paper.selectedIndex = idx;
						self.onChangePaper();
					}
				}
				if( params.lamination_id >= 0 ) {
					var idx = self.findIdx( "id", params.lamination_id, "lamination", null );
					if( idx >= 0 ) {
						self.pb_lamination.selectedIndex = idx;
						self.onChangeLamination();
					}
				}
				if( params.paper_base_id >= 0 ) {
					var idx = self.findIdx( "id", params.paper_base_id, "paper_base", null );
					if( idx >= 0 ) {
						self.pb_paper_base.selectedIndex = idx;
						self.onChangePaperBase();
					}
				}
				if( params.personal_pages > 0 ) {
					self.pb_personal_pages.selectedIndex = 1;
					self.onChangePersonalPages();
				}
				if( params.personal_cover_id >= 0 ) {
					self.pb_personal_cover.selectedIndex = params.personal_cover_id;
					self.onChangePersonalCover();
				}
				if( params.cover_id >= 0 ) {
					var parVal = self.findParentValueByChildValue( "id", params.cover_id, "covers", "items" );
					if( idx >= 0 ) {
						$$('input[name="cover_category_item"]').each( function(el) {
							el.checked = false;
						} );
						self.setCheckedItemByAttribute( 'cover_category_item', 'value', parVal );
						self.onChangeCover();
					}
				}
				if( params.cover_id >= 0 ) {
					$$('input[name="cover_item"]').each( function(el) {
						el.checked = false;
					} );
					self.setCheckedItemByAttribute( 'cover_item', 'value', params.cover_id );				
				}
				if( params.supercover_id >= 0 ) {
					$$('input[name="supercover_item"]').each( function(el) {
						el.checked = false;
					} );
					self.setCheckedItemByAttribute( 'supercover_item', 'value', params.supercover_id );				
				}
				if( params.design_work_id >= 0 ) {
					var idx = self.findParentIdxByChildValue( "id", params.design_work_id, "design_works", "items" );
					if( idx >= 0 ) {
						self.pb_design_work_category.selectedIndex = idx;
						self.onChangeDesignWork();
					}
				}
				if( params.design_work_id >= 0 ) {
					$$('input[name="design_work_item"]').each( function(el) {
						el.checked = false;
					} );
					self.setCheckedItemByAttribute( 'design_work_item', 'value', params.design_work_id );				
				}
				if( params.box_id >= 0 ) {
					var idx = self.findParentIdxByChildValue( "id", params.box_id, "boxes", "items" );
					if( idx >= 0 ) {
						self.pb_box_category.selectedIndex = idx;
						self.onChangeBox();
					}
				}
				if( params.box_id >= 0 ) {
					self.setCheckedItemByAttribute( 'box_item', 'value', params.box_id );				
				}
				if( params.forzaces >= 0 ) {
					var idx = this.findIdxInSelect( self.pb_forzaces, params.forzaces );
					if( idx >= 0 ) {
						self.pb_forzaces.selectedIndex = idx;
						self.onChangeForzaces();
					}
				}
				
				if( params.forzac_id >= 0 ) {
					$$('input[name="photobook_forzace_item"]').each( function(el) {
						el.checked = false;
					} );
					self.setCheckedItemByAttribute( 'photobook_forzace_item', 'value', params.forzac_id );				
				}

				if( params.selected_personal_pages ) {
					var sppa = params.selected_personal_pages.split(',');
					for( var i=0; i&lt;sppa.length; i++ ) {
						self.checkPersonalPage( sppa[i] );
					}
				}
				
				self.on_calculate_disabled = false;
				self.calculate();
			},
			
			initFormats: function() {
				var self = this, o = this.options;
				var p = $('book-format');
				for( var i=0; i&lt;this.pb_items.length; i++ ) {
					var item = this.pb_items[i];
					var f = new Element( 'div', {
						'class': o.formatItemClass,
						'id': item.id
					} );
					var img = new Element( 'img', {
						'src': item.thumbnail,
						'id': item.id
					});
					img.observe( 'click', function() {
						var item = $(this.id);
						var cn = 'selected';
						var items = item.up().select('.'+ o.formatItemClass);
						for( var i=0; i&lt;items.length; i++ ) items[i].removeClassName(cn);
						item.addClassName(cn);
					} );
					f.appendChild( img );
					p.appendChild(f);
				}
				p.appendChild( new Element('div', {style:'clear:both'}) );

				this.pb_format.options.length = 0;
				for( var i=0; i&lt;this.pb_items.length; i++ )
				{
					this.pb_format.options[i] = new Option(this.pb_items[i].name, this.pb_items[i].id);
				}
				this.pb_format.observe( 'change', this.onChangeFormat );
			},

			formatSelectedIndex: function() {
				var self = this, o = this.options;
				var retval = null;
				var items = $$('.book-format .'+ o.formatItemClass );
				for( var i=0; i&lt;items.length; i++ ) {
					if( items[i].hasClassName('selected') ) {
						retval = i;
						break;
					}
				}
				console.log(retval);

				return( retval );
			},

			formatSelectIndex: function( idx ) {
				var self = this, o = this.options;
				var cn = 'selected';
				var items = $$('.book-format .'+ o.formatItemClass );
				for( var i=0; i&lt;items.length; i++ ) items[i].removeClassName(cn);
				if( idx >= 0 &amp;&amp; idx &lt; items.length ) items[idx].addClassName(cn);
			},
			
			onChangeFormat: function() {
				var idx = pb.pb_format.selectedIndex;
				var s = pb.pb_items[idx];
				
				pb.fillPages( s );
				pb.fillPaper( s );
				pb.fillLamination( s );
				pb.fillDesignWork( s ); pb.onChangeDesignWork();
				pb.fillBox( s ); pb.onChangeBox();
				pb.fillCover( s ); pb.onChangeCover();
				pb.fillForzaces( s ); 
				pb.fillPhotobookForzaces( s ); 
				pb.fillPaperBase( s ); pb.onChangePaperBase();
				pb.calculate();
				
				$('pb_size_image').childElements().each( Element.remove )
				if( s.image ) {
					var ca = new Element( 'a', {
						'class': 'printbook-highslide',
						'href': s.image,
						'id': 'pb_size_image_'+ s.id, 
						'title': s.name
					} );
					ca.onclick = function( event ) {
						var id = this.getAttribute('id');
						var description = this.getAttribute('title');
						return hs.expand( this, { slideshowGroup: 'pb_image_image_' + id, captionText: description } );
					};
					ca.appendChild( new Element( 'img', {
						src: s.thumbnail,
						width: 100,
						height: 100
					}) );
					$('pb_size_image').appendChild( ca );
					$('pb_size_image').appendChild( new Element( 'div', {
						'class': 'comment'
					} ).update( s.name ) );
				}
			},
			
			onChangePages: function() {
				var idx = pb.pb_format.selectedIndex;
				var s = pb.pb_items[idx];

				//���������� ������� �� �������� allow_min_pages
				var coverID = pb.getCoverId();
                var parVal = pb.findParentValueByChildValue( "id", coverID, "covers", "items" );
				pb.fillCover( s );
                $$('input[name="cover_category_item"]').each( function(el) {
                    el.checked = false;
                } );
                pb.setCheckedItemByAttribute( 'cover_category_item', 'value', parVal );
                if( pb.AreAllItemsUnchecked('cover_category_item') ) {
                    pb.setCheckedFirstItem( 'cover_category_item' );
                }

				pb.onChangeCover();
				$$('input[name="cover_item"]').each( function(el) {
					el.checked = false;
				} );
				pb.setCheckedItemByAttribute( 'cover_item', 'value', coverID );
				if( pb.AreAllItemsUnchecked('cover_item') ) {
					pb.setCheckedFirstItem( 'cover_item' );
				}

				//���������� �������� �� �������� allow_min_pages
				var forzacID = pb.getPhotobookForzaceId();
				pb.fillPhotobookForzaces(pb.pb_items[pb.pb_format.selectedIndex]);
				$$('input[name="photobook_forzace_item"]').each( function(el) {
					el.checked = false;
				} );
				pb.setCheckedItemByAttribute( 'photobook_forzace_item', 'value', forzacID );
				if( pb.AreAllItemsUnchecked('photobook_forzace_item') ) {
					pb.setCheckedFirstItem( 'photobook_forzace_item' );
				}

				pb.applySelectPersonalPages();
				pb.calculate();
			},
			
			onChangePaper: function() { pb.calculate(); },
			onChangeLamination: function() { pb.calculate(); },
			onChangePaperBase: function() {	pb.calculate();	},
			onChangePhotobookForzaces: function() {	pb.calculate();	},
			
			onChangeForzaces: function() {
				pb.applySelectPersonalPages(); 
				pb.calculate();
			},
			
			onChangePersonalPages: function() {
				pb.applySelectPersonalPages();
				pb.calculate();
			},
			
			onChangePersonalCover: function() {
				pb.calculate();
			},
			
			applySelectPersonalPages: function() {
				if( pb.hasPersonalPages() ) {
					
					var format = pb.getFormat();
					var title = '<label class="printbook-displayblock">������� ����� �������� ����� �����������������:</label>';
					var page_prefix = '���. ';
					if( pb.isPhotobook() ) {
						title = '<label class="printbook-displayblock">������� ����� ��������� ����� �����������������:</label>';
						page_prefix = '���. ';
					}
					
					pb.pb_select_personal_pages.show();
					$A( pb.pb_select_personal_pages.childElements() ).each( Element.remove );
					pb.pb_select_personal_pages.update( title );
					var pages = pb.getPages();
					
					if( pb.getSelectedForzac() == 1 ) {
						//add 2 forzac
						var f1 = new Element( 'div', {
							'class': 'select_personal_pages'
						} );
						f1.appendChild( new Element( 'input', {
							type: 'checkbox',
							'class': 'select_personal_pages_checkbox',
							'id': 'select_personal_pages_f1',
							value: 'f1'
						} ) );
						f1.observe( 'click', function(event) {
							pb.calculate();
						} );
						f1.appendChild( new Element( 'label', {
							'class': 'inline',
							'for': 'select_personal_pages_f1'
						} ).update( '������ 1' ) );
						pb.pb_select_personal_pages.appendChild( f1 );
						
						var f2 = new Element( 'div', {
							'class': 'select_personal_pages'
						} );
						f2.appendChild( new Element( 'input', {
							type: 'checkbox',
							'class': 'select_personal_pages_checkbox',
							'id': 'select_personal_pages_f2',
							value: 'f2'
						} ) );
						f2.observe( 'click', function(event) {
							pb.calculate();
						} );
						f2.appendChild( new Element( 'label', {
							'class': 'inline',
							'for': 'select_personal_pages_f2'
						} ).update( '������ 2' ) );
						pb.pb_select_personal_pages.appendChild( f2 );
					}
					
					for( var i=0; i&lt;pages; i++ ) {
						var c = new Element( 'div', {
							'class': 'select_personal_pages'
						} );
						c.appendChild( new Element( 'input', {
							type: 'checkbox',
							'class': 'select_personal_pages_checkbox',
							'id': 'select_personal_pages_'+ (i+1),
							value: i+1
						} ) );
						c.observe( 'click', function(event) {
							pb.calculate();
						} );
						c.appendChild( new Element( 'label', {
							'class': 'inline',
							'for': 'select_personal_pages_'+ (i+1)
						} ).update( page_prefix + (i+1) ) );
						pb.pb_select_personal_pages.appendChild( c );
					}
				}
				else {
					pb.pb_select_personal_pages.hide();
				}
			},
			
			onChangeQuantity: function() { 
				if( pb.pb_quantity.value &lt;= 0 ) pb.pb_quantity.value = 1;
				
				pb.applyPersonalPages();
				pb.applyPersonalCover();
				
				pb.calculate();
			},
			
			onChangeDesignWork: function() {
				var selCategory = pb.pb_design_work_category;
				var catIdx = selCategory.selectedIndex;
				var divItems = pb.pb_design_work_items;
				var idx = pb.pb_format.selectedIndex;
				
				$A( divItems.childElements() ).each( Element.remove );
				var category = pb.pb_items[idx].design_works[catIdx];
				
				/*
				$('pb_design_work_category_image').childElements().each( Element.remove )
				if( category.image ) {
					var ca = new Element( 'a', {
						'class': 'printbook-highslide',
						'href': category.image,
						'id': 'pb_design_work_category_image_'+ category.id, 
						'title': category.description
					} );
					ca.onclick = function( event ) {
						var id = this.getAttribute('id');
						var description = this.getAttribute('title');
						return hs.expand( this, { slideshowGroup: 'pb_design_work_category_image_' + id, captionText: description } );
					};
					ca.appendChild( new Element( 'img', {
						src: category.thumbnail,
						width: 100,
						height: 100
					}) );
					$('pb_design_work_category_image').appendChild( ca );
					$('pb_design_work_category_image').appendChild( new Element( 'div', {
						'class': 'comment'
					} ).update( category.name ) );
				}*/
				
				$('pb_design_work_description').update( category.description );
				
				if( category.items.length == 0 )
				{
					divItems.appendChild( new Element( 'input', { 'type': 'hidden', 'name': 'design_work_item', 'id': 'design_work_item', 'value': 0, 'price': 0 } ) );
				}
				else
				{
					divItems.appendChild( new Element( 'label', {
						'id': 'design_work_select_label',
						'class': 'printbook-displayblock'
					} ).update('�������� �����:') );
				}
				for( var i=0; i&lt;category.items.length; i++ ) 
				{
					var item = category.items[i];
					var div = new Element( 'div', {
						'class': 'category_item'
					} );
					var a = new Element( 'a', {
						'class': 'printbook-highslide',
						'href': item.image,
						'id': 'design_work_item_'+ item.id, 
						'title': item.description
					} );
					a.onclick = function( event ) {
						var id = this.getAttribute('id');
						var description = this.getAttribute('title');
						return hs.expand( this, { slideshowGroup: 'design_work_item_' + id, captionText: description } );
					};
					a.appendChild( new Element( 'img', { 'src': item.thumbnail, 'width': 100, 'height': 100 } ) );
					div.appendChild( a );
					var p = new Element( 'p' );
					var cb = new Element( 'input', { 'type': 'checkbox', 'name': 'design_work_item', 'id': 'design_work_item', 'value': item.id, 'base_price': item.base_price, 'price': item.price } );
					cb.observe( 'click', function(event) {
						var t = $(this);
						$$('input[name="design_work_item"]').each( function(el) {
							if( t != el ) el.checked = false;
						} );
						if( pb.AreAllItemsUnchecked('design_work_item') ) {
							pb.setCheckedFirstItem( 'design_work_item' );							
						}
						pb.calculate();
					} );
					p.appendChild( cb );
					//p.appendChild( new Element( 'span' ).update( item.name ) );
					p.appendChild( new Element( 'a', {href:item.url,target:'_design_work_template'} ).update( item.name ) );
					div.appendChild( p );
					divItems.appendChild( div );
				}
				pb.setCheckedFirstItem( 'design_work_item' );
				pb.calculate();
			},

			getFilteredCategoryItems: function(category) {
				var retval = [];
				if( category &amp;&amp; category.items &amp;&amp; category.items.length > 0 ) {
					for( var i=0; i&lt;category.items.length; i++ )
					{
						var item = category.items[i];
						if (item.allow_min_pages &lt;= pb.pb_pages[pb.pb_pages.selectedIndex].value) {
							retval.push(item);
						}
					}
				}

				return( retval );
			},

			onChangeCover: function() {
				var catIdx = pb.getCoverCategoryIdx();
				var divItems = pb.pb_cover_items;
				var idx = pb.pb_format.selectedIndex;
				
				$A( divItems.childElements() ).each( Element.remove );
				var category = pb.pb_items[idx].covers[catIdx];
				category.filteredItems = pb.getFilteredCategoryItems(category);

				if( category &amp;&amp; category.filteredItems.length == 0 )
				{
					divItems.appendChild( new Element( 'input', { 'type': 'hidden', 'name': 'cover_item', 'id': 'cover_item', 'value': 0, 'price': 0 } ) );
					divItems.hide();
				}
				else if( category &amp;&amp; category.filteredItems.length == 1 ) {
					var item = category.items[0]; 
					divItems.appendChild( new Element( 'input', {
						'type': 'checkbox', 
						'name': 'cover_item', 
						'id': 'cover_item', 
						'value': item.id, 
						'price': item.price, 
						'supercover_price': item.supercover_price,
						'class': 'hidden'
					} ) );
					divItems.hide();
				}
				else
				{
					divItems.appendChild( new Element( 'label', {
						'id': 'cover_select_label',
						'class': 'printbook-displayblock'
					} ).update('�������� �������� �������:') );
					divItems.show();
				}
				
				if( category &amp;&amp; category.filteredItems &amp;&amp; category.filteredItems.length > 1 ) {
					for( var i=0; i&lt;category.filteredItems.length; i++ )
					{
						var item = category.filteredItems[i];
						if (item.allow_min_pages &lt;= pb.pb_pages[pb.pb_pages.selectedIndex].value) {
							var div = new Element( 'div', {
								'class': 'category_item'
							} );
							var a = new Element( 'a', {
								'class': 'printbook-highslide',
								'href': item.image,
								'id': 'cover_item_'+ item.id,
								'title': item.description
							} );
							a.onclick = function( event ) {
								var id = this.getAttribute('id');
								var description = this.getAttribute('title');
								return hs.expand( this, { slideshowGroup: 'cover_item_' + id, captionText: description } );
							};
							a.appendChild( new Element( 'img', { 'src': item.thumbnail, 'width': 100, 'height': 100 } ) );
							div.appendChild( a );
							var p = new Element( 'p' );
							var cb = new Element( 'input', { 'type': 'checkbox', 'name': 'cover_item', 'id': 'cover_item', 'value': item.id, 'price': item.price, 'supercover_price': item.supercover_price } );
							cb.observe( 'click', function(event) {
								var t = $(this);
								$$('input[name="cover_item"]').each( function(el) {
									if( t != el ) el.checked = false;
								} );
								if( pb.AreAllItemsUnchecked('cover_item') ) {
									pb.setCheckedFirstItem( 'cover_item' );
								}
								pb.ApplySupercovers( t, false );
								pb.calculate();
							} );
							p.appendChild( cb );
							p.appendChild( new Element( 'span' ).update( item.name ) );
							div.appendChild( p );
							divItems.appendChild( div );
						}
					}
				}
				pb.setCheckedFirstItem( 'cover_item', function( el ) {
					pb.ApplySupercovers( el, false );
				} );
				pb.calculate();
			},
			
			ApplySupercovers: function( el, calculate ) {
				var self = this; var o = self.options;
				
				var divItems = pb.pb_cover_items;
				var idx = pb.pb_format.selectedIndex;
				var catIdx = pb.getCoverCategoryIdx();
				var category = pb.pb_items[idx].covers[catIdx];
				
				var cover = null;
				var id = ( el )? parseInt( el.value ) : 0;
				if( category ) {
					for( var i=0; i&lt;category.items.length; i++ ) {
						if( category.items[i].id == id ) {
							cover = category.items[i];
						}
					}
				}
				
				var scID = o.supercoversID;
				var scSelectID = o.supercoverSelectID; 
				divItems.select('#'+ scID).each( Element.remove );
				var div = new Element( 'div', {
					'id': scID
				} );
				if( cover &amp;&amp; cover.has_supercover == 1 ) {
					div.appendChild( new Element( 'label', {
						'id': 'cover_select_label',
						'class': 'printbook-displayblock'
					} ).update('������������:') );
					
					var scSelect = new Element( 'select', {
						'id': scSelectID
					});
					scSelect.observe( 'change', function(event) {
						self.calculate();
					});
					scSelect.appendChild( new Element( 'option', {
						value: 0
					} ).update('���') );
					scSelect.appendChild( new Element( 'option', {
						value: 1
					} ).update('����') );
					div.appendChild( scSelect );
				}
				else if( cover ) {
					div.appendChild( new Element( 'input', {
						'id': scSelectID,
						type: 'hidden',
						name: '',
						value: 0
					} ) );
				}
				divItems.appendChild( div );
				
				if( calculate == true ) {
					pb.calculate();
				}
			},
			
			onChangeBox: function() {
				var selCategory = pb.pb_box_category;
				var catIdx = selCategory.selectedIndex;
				var divItems = pb.pb_box_items;
				var idx = pb.pb_format.selectedIndex;
				
				$A( divItems.childElements() ).each( Element.remove );
				var category = pb.pb_items[idx].boxes[catIdx];
				
				$('pb_box_category_image').childElements().each( Element.remove )
				if( category.image ) {
					var ca = new Element( 'a', {
						'class': 'printbook-highslide',
						'href': category.image,
						'id': 'pb_box_category_image_'+ category.id, 
						'title': category.description
					} );
					ca.onclick = function( event ) {
						var id = this.getAttribute('id');
						var description = this.getAttribute('title');
						return hs.expand( this, { slideshowGroup: 'pb_box_category_image_' + id, captionText: description } );
					};
					ca.appendChild( new Element( 'img', {
						src: category.thumbnail,
						width: 100,
						height: 100
					}) );
					$('pb_box_category_image').appendChild( ca );
					$('pb_box_category_image').appendChild( new Element( 'div', {
						'class': 'comment'
					} ).update( category.name ) );
				}
				
				$('pb_box_description').update( category.description );
				
				if( category.items.length == 0 )
				{
					divItems.appendChild( new Element( 'input', { 'type': 'hidden', 'name': 'box_item', 'id': 'box_item', 'value': 0, 'price': 0 } ) );
				}
				else
				{
					divItems.appendChild( new Element( 'label', {
						'class': 'printbook-displayblock'
					} ).update('�������� �������� ����� ��� �������:') );
				}
				for( var i=0; i&lt;category.items.length; i++ ) 
				{
					var item = category.items[i];
					var div = new Element( 'div', {
						'class': 'category_item'
					} );
					var a = new Element( 'a', {
						'class': 'printbook-highslide',
						'href': item.image,
						'id': 'box_item_'+ item.id, 
						'title': item.description
					} );
					a.onclick = function( event ) {
						var id = this.getAttribute('id');
						var description = this.getAttribute('title');
						return hs.expand( this, { slideshowGroup: 'box_item_' + id, captionText: description } );
					};
					a.appendChild( new Element( 'img', { 'src': item.thumbnail, 'width': 100, 'height': 100 } ) );
					div.appendChild( a );
					var p = new Element( 'p' );
					var cb = new Element( 'input', { 'type': 'checkbox', 'name': 'box_item', 'id': 'box_item', 'value': item.id, 'price': item.price } );
					cb.observe( 'click', function(event) {
						var t = $(this);
						$$('input[name="box_item"]').each( function(el) {
							if( t != el ) el.checked = false;
						} );
						pb.calculate();
					} );
					p.appendChild( cb );
					p.appendChild( new Element( 'span' ).update( item.name ) );
					div.appendChild( p );
					divItems.appendChild( div );
				}
				pb.calculate();
			},
			
			getCheckedItemAttribute : function( id, attr_name, def_value ) {
				var retval = def_value;
						$$('input[id="'+ id +'"]').each( function(el) {
							if( el.checked ) retval = el.getAttribute(attr_name);
						} );
						
				if( !retval ) retval = def_value;
						
				return( retval );
			},
			
			setCheckedItemByAttribute : function( id, attr_name, attr_value ) {
						$$('input[id="'+ id +'"]').each( function(el) {
							if( el.getAttribute(attr_name) ) {
								var a = el.getAttribute(attr_name);
								if( a == attr_value ) el.checked = true;
							}
						} );
			},
			
			AreAllItemsUnchecked : function( id ) {
					var retval = true;
						$$('input[id="'+ id +'"]').each( function(el) {
							if( el.checked ) {
								retval = false;
							}
						} );
					return( retval );
			},
			
			setCheckedFirstItem : function( id, cb ) {
					var first = true;
					var selected = null;
					
					$$('input[id="'+ id +'"]').each( function(el) {
						var parents = el.ancestors();
						var is_hidden = false;
						for (var key in parents) {
							var val = parents[key];
							if (val.className == 'hidden') {
								is_hidden = true;
							}
						}
						if( first &amp;&amp; !(is_hidden)) {
							el.checked = true;
							first = false;
							selected = el;
						}
					} );
					
					if( typeof(cb) == 'function' ) cb( selected );
			},
			
			onOrderClick: function( e ) {
				var l = '/photobook/order/?passparam=1';
					if( pb.pb_format.selectedIndex > 0 ) l += '&amp;pb_format='+ pb.pb_format.selectedIndex; 
					if( pb.pb_pages.selectedIndex > 0 ) l += '&amp;pb_pages='+ pb.pb_pages.selectedIndex;
					if( pb.pb_paper.selectedIndex > 0 ) l += '&amp;pb_paper='+ pb.pb_paper.selectedIndex;
					if( pb.pb_lamination.selectedIndex > 0 ) l += '&amp;pb_lamination='+ pb.pb_lamination.selectedIndex;
					if( pb.pb_forzaces.selectedIndex > 0 ) l += '&amp;pb_forzaces='+ pb.pb_forzaces.selectedIndex;
					var photobook_forzace_item = pb.getCheckedItemAttribute( 'photobook_forzace_item', 'value', 0 );
					if( photobook_forzace_item > 0 ) l += '&amp;pb_photobook_forzaces='+ photobook_forzace_item;
					if( pb.pb_paper_base.selectedIndex > 0 ) l += '&amp;pb_paper_base='+ pb.pb_paper_base.selectedIndex;
					if( pb.pb_personal_pages.selectedIndex > 0 ) l += '&amp;pb_personal_pages='+ pb.pb_personal_pages.selectedIndex;
					if( pb.hasPersonalPages() ) l += '&amp;pb_selected_personal_pages='+ pb.getSelectedPersonalPagesAsString();
					if( pb.pb_personal_cover.selectedIndex > 0 ) l += '&amp;pb_personal_cover='+ pb.pb_personal_cover.selectedIndex;
					var cover_category_item = pb.getCheckedItemAttribute( 'cover_category_item', 'value', 0 );
					if( cover_category_item > 0 ) l += '&amp;pb_cover_category='+ cover_category_item;
					var cover_item = pb.getCheckedItemAttribute( 'cover_item', 'value', 0 );
					if( cover_item > 0 ) l += '&amp;pb_cover='+ cover_item;
					if( pb.pb_design_work_category.selectedIndex > 0 ) l += '&amp;pb_design_work_category='+ pb.pb_design_work_category.selectedIndex;
					var design_work_item = pb.getCheckedItemAttribute( 'design_work_item', 'value', 0 );
					if( design_work_item > 0 ) l += '&amp;pb_design_work='+ design_work_item;
					if( pb.pb_box_category.selectedIndex > 0 ) l += '&amp;pb_box_category='+ pb.pb_box_category.selectedIndex;
					var box_item = pb.getCheckedItemAttribute( 'box_item', 'value', 0 );
					if( box_item > 0 ) l += '&amp;pb_box='+ box_item;
					l += '&amp;pb_quantity='+ pb.pb_quantity.value;
					
					document.location = l;
			},
			
			isPrintbook: function() {
				var format = this.getFormat();
				//return( format.type != 2 );
				return( format.type == 1 );
			},
			
			isPhotobook: function() {
				var format = this.getFormat();
				return( format.type == 2 || format.type == 3 ); //photobook and polibook
			},
			
			fillPages: function( format ) {
				this.pb_pages.options.length = 0;
				
				if( this.isPhotobook() ) { //photobook
					$('pb_pages_label1').removeClassName('printbook-displayblock');
					$('pb_pages_label1').addClassName('hidden');
					$('pb_pages_label2').removeClassName('hidden');
					$('pb_pages_label2').addClassName('printbook-displayblock');
				}
				else {
					$('pb_pages_label1').removeClassName('hidden');
					$('pb_pages_label1').addClassName('printbook-displayblock');
					$('pb_pages_label2').removeClassName('printbook-displayblock');
					$('pb_pages_label2').addClassName('hidden');
				}
				
				if( format.start_page > 0 )
				{
					var max_page = ( format.stop_page > 0 )? format.stop_page : 26;
					for( var i=format.start_page; i&lt;=max_page; i+=format.page_block )
					{
						this.pb_pages.options[this.pb_pages.options.length] = new Option(i, i);
					}
				}
				else this.pb_pages.options[this.pb_pages.options.length] = new Option('&lt;��������&gt;', 0);
				pb.fillPersonalPages( format );
			},
			
			fillPaper: function( format ) {
				this.pb_paper.options.length = 0;
				for( var i=0; i&lt;format.papers.length; i++ )
				{
					this.pb_paper.options[this.pb_paper.options.length] = new Option(format.papers[i].name, format.papers[i].price);
				}
				if( format.papers.length > 0 ) this.pb_paper.disabled = false;
				else this.pb_paper.disabled = true;
			},
			
			fillPhotobookForzaces: function( format ) {
				$A( this.pb_photobook_forzaces.childElements() ).each( Element.remove );
				for( var i=0; i&lt;format.forzaces.length; i++ )
				{
					var forzace = format.forzaces[i];
					if( forzace &amp;&amp; forzace.image &amp;&amp; (forzace.allow_min_pages &lt;= pb.pb_pages[pb.pb_pages.selectedIndex].value)) {
						var div = new Element( 'div', {
							'class': 'photobook_forzace_item'
						} );
						var ca = new Element( 'a', {
							'class': 'printbook-highslide',
							'href': forzace.image,
							'id': 'pb_photobook_forzace_image_'+ forzace.id,
							'title': forzace.description
						} );
						ca.onclick = function( event ) {
							var id = this.getAttribute('id');
							var description = this.getAttribute('title');
							return hs.expand( this, { slideshowGroup: 'pb_photobook_forzace_image_' + id, captionText: description } );
						};
						ca.appendChild( new Element( 'img', {
							src: forzace.thumbnail,
							width: 100,
							height: 100
						}) );
						div.appendChild( ca );

						var p = new Element( 'p' );
						var cb = new Element( 'input', { 'type': 'checkbox', 'name': 'photobook_forzace_item', 'id': 'photobook_forzace_item', 'value': forzace.id, 'price': forzace.price } );
						cb.observe( 'click', function(event) {
							var t = $(this);
							$$('input[name="photobook_forzace_item"]').each( function(el) {
								if( t != el ) el.checked = false;
							} );
							if( pb.AreAllItemsUnchecked('photobook_forzace_item') ) {
								pb.setCheckedFirstItem( 'photobook_forzace_item' );
							}
							pb.onChangePhotobookForzaces();
						} );
						p.appendChild( cb );
						p.appendChild( new Element( 'span' ).update( forzace.name ) );
						div.appendChild( p );

						this.pb_photobook_forzaces.appendChild( div );
					}
				}
				
				pb.setCheckedFirstItem( 'photobook_forzace_item', function( el ) {
					pb.onChangePhotobookForzaces();
				} );
				
				if( format.forzaces.length == 1 &amp;&amp; !format.forzaces[0].id ) this.pb_photobook_forzaces.up().addClassName('hidden');
				else if( format.forzaces.length > 0 ) this.pb_photobook_forzaces.up().removeClassName('hidden');
				else this.pb_photobook_forzaces.up().addClassName('hidden');
			},
			
			fillLamination: function( format ) {
				this.pb_lamination.options.length = 0;
				
				if( this.isPhotobook() ) this.wrapperEnable( this.pb_lamination.up() );
				else if( this.isPrintbook() ) this.wrapperEnable( this.pb_lamination.up() );
				else this.wrapperDisable( this.pb_lamination.up() );
				
				for( var i=0; i&lt;format.lamination.length; i++ )
				{
					this.pb_lamination.options[this.pb_lamination.options.length] = new Option(format.lamination[i].name, format.lamination[i].price);
				}
				if( format.lamination.length > 0 ) this.pb_lamination.disabled = false;
				else this.pb_lamination.disabled = true;
			},
			
			fillDesignWork: function( format ) {
				this.pb_design_work_category.options.length = 0;
				for( var i=0; i&lt;format.design_works.length; i++ )
				{
					this.pb_design_work_category.options[this.pb_design_work_category.options.length] = new Option(format.design_works[i].name, format.design_works[i].id);
				}
				if( format.design_works.length > 0 ) this.pb_design_work_category.disabled = false;
				else this.pb_design_work_category.disabled = true;
			},
			
			fillBox: function( format ) {
				this.pb_box_category.options.length = 0;
				for( var i=0; i&lt;format.boxes.length; i++ )
				{
					this.pb_box_category.options[this.pb_box_category.options.length] = new Option(format.boxes[i].name, format.boxes[i].id);
				}
				if( format.boxes.length > 0 ) this.pb_box_category.disabled = false;
				else this.pb_box_category.disabled = true;
			},
			
			fillCover: function( format ) {
				if( format.covers.length &amp;&amp; format.covers.length>0 ) {
					this.pb_cover_category.show();
				}
				else this.pb_cover_category.hide();
				$A( this.pb_cover_category.childElements() ).each( Element.remove );
				for( var i=0; i&lt;format.covers.length; i++ )
				{
					var category = format.covers[i];

					category.filteredItems = pb.getFilteredCategoryItems(category);

					var emptyCategory = true;
					if( category &amp;&amp; category.filteredItems.length > 0 ) {
						emptyCategory = false;
					}

					if( category &amp;&amp; category.image) {
						var div = new Element( 'div', {
							'class': 'category_item'
						} );
						var ca = new Element( 'a', {
							'class': 'printbook-highslide',
							'href': category.image,
							'id': 'pb_cover_category_image_'+ category.id, 
							'title': category.description
						} );
						ca.onclick = function( event ) {
							var id = this.getAttribute('id');
							var description = this.getAttribute('title');
							return hs.expand( this, { slideshowGroup: 'pb_cover_category_image_' + id, captionText: description } );
						};
						ca.appendChild( new Element( 'img', {
							src: category.thumbnail,
							width: 100,
							height: 100
						}) );
						div.appendChild( ca );
						
						var p = new Element( 'p' );
						var cb = new Element( 'input', { 'type': 'checkbox', 'name': 'cover_category_item', 'id': 'cover_category_item', 'value': category.id } );
						cb.observe( 'click', function(event) {
							var t = $(this);
							$$('input[name="cover_category_item"]').each( function(el) {
								if( t != el ) el.checked = false;
							} );
							if( pb.AreAllItemsUnchecked('cover_category_item') ) {
								pb.setCheckedFirstItem( 'cover_category_item' );							
							}
							pb.onChangeCover();
						} );
						p.appendChild( cb );
						p.appendChild( new Element( 'span' ).update( category.name ) );
						div.appendChild( p );

						if (emptyCategory) {
							div.removeClassName('category_item');
							div.addClassName('hidden');
						}

						this.pb_cover_category.appendChild( div );
					}
				}
				
				pb.setCheckedFirstItem( 'cover_category_item', function( el ) {
					pb.onChangeCover();
				} );
				
				if( format.covers.length == 1 &amp;&amp; !format.covers[0].id ) this.pb_cover_category.up().addClassName('hidden');
				else if( format.covers.length > 0 ) this.pb_cover_category.up().removeClassName('hidden');
				else this.pb_cover_category.up().addClassName('hidden');
			},
			
			fillPaperBase: function( format ) {
				this.pb_paper_base.options.length = 0;
				
				if( this.isPrintbook() ) this.wrapperDisable( this.pb_paper_base.up() );
				else if( this.isPhotobook() ) this.wrapperEnable( this.pb_paper_base.up() );
				else this.wrapperDisable( this.pb_paper_base.up() );
				
				for( var i=0; i&lt;format.paper_base.length; i++ )
				{
					this.pb_paper_base.options[this.pb_paper_base.options.length] = new Option(format.paper_base[i].name, format.paper_base[i].price);
				}
				if( format.paper_base.length > 0 ) this.pb_paper_base.disabled = false;
				else this.pb_paper_base.disabled = true;
			},
			
			fillPersonalPages: function( format ) {
				this.pb_personal_pages.options.length = 0;
				
				this.applyPersonalPages();
				
				this.pb_personal_pages.options[this.pb_personal_pages.options.length] = new Option( '���', 0 );
				this.pb_personal_pages.options[this.pb_personal_pages.options.length] = new Option( '��', 1 );
			},
			
			applyPersonalPages: function() {
				if( this.getQuantity() &lt;= 1 ) {
					this.wrapperDisable( this.pb_personal_pages.up() );
				}
				else {
					this.wrapperEnable( this.pb_personal_pages.up() );
				}
			},
			
			applyPersonalCover: function() {
				if( this.getQuantity() &lt;= 1 ) {
					this.wrapperDisable( this.pb_personal_cover.up() );
				}
				else {
					this.wrapperEnable( this.pb_personal_cover.up() );
				}
			},
			
			fillForzaces: function( format ) {
				this.pb_forzaces.options.length = 0;
				
				if( this.isPrintbook() ) {
					this.wrapperEnable( this.pb_forzaces.up() );
					
					this.pb_forzaces.options[this.pb_forzaces.options.length] = new Option( '��������������', 1 );
					this.pb_forzaces.options[this.pb_forzaces.options.length] = new Option( '����������������', 2 );
				}
				else this.wrapperDisable( this.pb_forzaces.up() );
				
				//this.wrapperEnable( this.pb_forzaces.up() );
			},
			
			getCoverCategoryIdx: function() {
				var retval = 0;
				var a = $$('input[name="cover_category_item"]');
				for( var i=0; i&lt;a.length; i++ ) {
					if( a[i].checked ) {
						retval = i;
						break;
					}
				}
				return( retval );
			},
			
			getFormat: function() {
				return( this.pb_items[this.pb_format.selectedIndex] );
			},
			
			getPages: function() {
				return( parseInt( this.pb_pages.options[this.pb_pages.selectedIndex].value ) );
			},
			
			getQuantity: function() {
				return( parseInt(this.pb_quantity.value) );
			},
			
			hasSupercover: function() {
				var retval = false;
				
				var self = this; var o = self.options;
				var i = $(o.supercoverSelectID);
				if( i &amp;&amp; i.value == 1 ) retval = true;
				
				return( retval );
			},
			
			hasPersonalPages: function() {
				return( this.pb_personal_pages.selectedIndex > 0 );
			},
			
			hasPersonalCover: function() {
				return( this.pb_personal_cover.selectedIndex > 0 );
			},
			
			getPersonalPages: function() {
				var retval = 0;
				
				if( pb.hasPersonalPages() ) {
					$$('input[class="select_personal_pages_checkbox"]').each( function(el) {
						if( el.checked ) retval ++;
					} );
				}

				return( retval );
			},
			
			getSelectedPersonalPagesAsString: function() {
				var retval = '';
				
				if( pb.hasPersonalPages() ) {
					var attr_name = 'value';
					var delim = '';
					$$('input[class="select_personal_pages_checkbox"]').each( function(el) {
						if( el.checked ) {
							if( el.getAttribute(attr_name) ) {
								var a = el.getAttribute(attr_name);
								retval += delim + a;
								delim = ',';
							}
						}
					} );
				}

				return( retval );
			},
			
			checkPersonalPage: function( personalPageNumber ) {
				//personalPageNumber = parseInt( personalPageNumber );
				//if( personalPageNumber > 0 &amp;&amp; pb.hasPersonalPages() ) {
				if( personalPageNumber &amp;&amp; pb.hasPersonalPages() ) {
					var attr_name = 'value';
					$$('input[class="select_personal_pages_checkbox"]').each( function(el) {
						if( el.getAttribute(attr_name) ) {
							var a = el.getAttribute(attr_name);
							if( a == personalPageNumber ) {
								el.checked = true;
								//el.setAttribute( 'checked', 'true' );
							}
						}
					} );
				}
			},
			
			isThisPersonalPage: function( personalPageNumber ) {
				var retval = false;
				
				//personalPageNumber = parseInt( personalPageNumber );
				if( personalPageNumber &amp;&amp; pb.hasPersonalPages() ) {
					var attr_name = 'value';
					$$('input[class="select_personal_pages_checkbox"]').each( function(el) {
						if( el.checked &amp;&amp; el.getAttribute(attr_name) == personalPageNumber ) {
							retval = true;
						}
					} );
				}
				
				return( retval );
			},
			
			wrapperDisable: function( wrapper ) {
				if( wrapper ) {
					wrapper.up().hide();
					/*this.wrapperEnable( wrapper );
					wrapper.appendChild( new Element('div',{
						'class': 'disabled'
					}));*/
				}
			},
			
			wrapperEnable: function( wrapper ) {
				if( wrapper ) {
					wrapper.up().show();
					/*wrapper.select('div.disabled').each( function(el) {
						el.remove();
					} );*/
				}
			},
			
			getPaperPrice: function() {
				return( this.pb_paper.options[this.pb_paper.selectedIndex].value );
			},
			
			getSelectedForzac: function() {
				var retval = 0;
				
				if (this.pb_forzaces.options.length == 0)
					retval = -1
				else
					retval = this.pb_forzaces.options[this.pb_forzaces.selectedIndex].value;
				
				return (retval);
			},
			
			getLaminationPrice: function() {
				return( ( this.pb_lamination.options.length > 0 )? this.pb_lamination.options[this.pb_lamination.selectedIndex].value : 0 );
			},
			
			getPaperBasePrice: function() {
				return( (this.pb_paper_base.options.length>0)? this.pb_paper_base.options[this.pb_paper_base.selectedIndex].value : 0 );
			},
			
			getPhotobookForzacePrice: function() {
				return( parseFloat( this.getCheckedItemAttribute('photobook_forzace_item','price',0) ) );
			},
			
			getCoverPrice: function() {
				return( parseFloat( this.getCheckedItemAttribute('cover_item','price',0) ) );
			},
			
			getSuperCoverPrice: function() {
				var retval = 0;
				
				if( this.hasSupercover() ) {
					retval = parseFloat( this.getCheckedItemAttribute('cover_item','supercover_price',0) );
				} 
				
				return( retval );
			},
			
			getPersonalCoverPrice: function() {
				var format = this.getFormat();
				return( format.price_personal_cover );
			},
			
			getBoxPrice: function() {
				return( parseFloat( this.getCheckedItemAttribute('box_item','price',0) ) );
			},
			
			getDesignWorkBasePrice: function() {
				return( parseFloat( this.getCheckedItemAttribute('design_work_item','base_price',0) ) );
			},
			
			getDesignWorkPrice: function() {
				return( parseFloat( this.getCheckedItemAttribute('design_work_item','price',0) ) );
			},
			
			getSizeId: function() {
				var format = this.getFormat();
				return( format.id );
			},
			
			getPaperId: function() {
				var format = this.getFormat();
				return( format.papers[this.pb_paper.selectedIndex].id );
			},
			
			getLaminationId: function() {
				var format = this.getFormat();
				return( format.lamination[this.pb_lamination.selectedIndex].id );
			},
			
			getPaperBaseId: function() {
				var format = this.getFormat();
				return( format.paper_base[this.pb_paper_base.selectedIndex].id );
			},
			
			getPersonalCoverId: function() {
				return( this.pb_personal_cover.value );
			},
			
			getCoverId: function() {
				return( parseFloat( this.getCheckedItemAttribute('cover_item','value',0) ) );
			},
			
			getSuperCoverId: function() {
				return( parseFloat( this.getCheckedItemAttribute('supercover_item','value',0) ) );
			},
			
			getBoxId: function() {
				return( parseFloat( this.getCheckedItemAttribute('box_item','value',0) ) );
			},
			
			getDesignWorkId: function() {
				return( parseFloat( this.getCheckedItemAttribute('design_work_item','value',0) ) );
			},
			
			getPhotobookForzaceId: function() {
				return( parseFloat( this.getCheckedItemAttribute('photobook_forzace_item','value',0) ) );
			},
			
			getDiscountInPercent: function() {
				var retval = 0;
				var format = this.getFormat();
				var quantity = this.getQuantity();
				
				if( format.discount1 > 0 &amp;&amp; quantity >= 3 &amp;&amp; quantity &lt;= 4 ) retval = format.discount1;
				else if( format.discount2 > 0 &amp;&amp; quantity >= 5 &amp;&amp; quantity &lt;= 9 ) retval = format.discount2;
				else if( format.discount3 > 0 &amp;&amp; quantity >= 10 ) retval = format.discount3;
				
				if( format.client_discount > 0 ) retval += format.client_discount; 
				
				return( retval );
			},
			
			getDiscountPrice: function( format, price_one ) {
				return( price_one * this.getDiscountInPercent() / 100 );
			},
			
			calculate: function( msg ) {
				var self = this;
				
				this.alert(msg);
				var format = this.getFormat();
				var price_one = format.price_base;
				var pages = this.getPages();
				var quantity = this.getQuantity();
				var personal_pages = this.getPersonalPages();
				
				price_one += format.price_per_page * pages;
				price_one += this.getPaperPrice() * pages;
				price_one += this.getLaminationPrice() * pages;
				price_one += this.getPaperBasePrice() * pages;
				
				price_one += this.getPhotobookForzacePrice();
				
				var price_cover = this.getCoverPrice();
				price_one += price_cover;
				
				var price_supercover = this.getSuperCoverPrice();
				price_one += price_supercover; 
				
				if( quantity == 1 &amp;&amp; (this.hasPersonalPages() || this.pb_personal_cover.value == 1) )
				{
					this.pb_personal_pages.selectedIndex = 0; personal_pages = 0;
					this.pb_personal_cover.selectedIndex = 0;
					this.onChangePersonalPages();
					alert( '���������� ����������� ������ ���� ��� ������� 2, ����� ������������ ��������������. �������� ���������� ����������� �� ��������� � ����� ��������� �������������� ������� ��� �������.' );
				}
				
				price_one += personal_pages * format.price_personal_page - personal_pages * format.price_per_page;
				if( this.pb_personal_cover.value == 1 )
				{
					price_one += this.getPersonalCoverPrice();
				}
				
				price_one = price_one - this.getDiscountPrice( format, price_one );
				price_one = Math.floor(price_one);
				this.price_one.innerHTML = price_one +'.00';
				this.price_one2.innerHTML = price_one +'.00';
				
				var price_design = Math.floor( this.getDesignWorkBasePrice() + this.getDesignWorkPrice() * pages );
				this.price_design.innerHTML = price_design +'.00';
				this.price_design2.innerHTML = price_design +'.00';
				
				var price_box = parseFloat( this.getBoxPrice() * quantity );
				this.price_box.innerHTML = price_box +'.00';
				this.price_box2.innerHTML = price_box +'.00';
				
				this.price_total.innerHTML = (price_one * quantity + price_design + price_box) +'.00';
				this.price_total2.innerHTML = (price_one * quantity + price_design + price_box) +'.00';
				
				if( this.btn_printbook_select_files ) {
					if( price_one > 0 ) this.btn_printbook_select_files.setStyle( {display:''} );
					else this.btn_printbook_select_files.setStyle( {display:'none'} );
				}
				
				//if( typeof(this.on_calculate) == 'function' &amp;&amp; !this.on_calculate_disabled ) this.on_calculate( this );
				if( !this.on_calculate_disabled ) Events.publish( '/PB/AfterCalculate', [this] );
			},
			
			alert: function( msg )
			{
				if( !msg ) msg = '';
				this.pb_message.innerHTML = msg;
			}
		};
		pb.init();
		window.onload = function() {
			pb.postInit();
			
			var today = new Date();
			var pbDate = new Date();
			pbDate.setDate( today.getDate() + 10 );
			function pad(s) { return (s &lt; 10) ? '0' + s : s; }
			var pbDateFormatted = [pad(pbDate.getDate()), pad(pbDate.getMonth()+1), pbDate.getFullYear()].join('.');
			Events.publish( '/Anounce/Show', [{
				message: '��������, ����� ����� ����� '+ pbDateFormatted +'. ���� ����� ����������� ��������� ����� �����, �� �� � ���� ��������.',
				cookie: 'printbook_start_message_show',
				cookieTrigger: 0
			}] );
		}
		</script>
	</div>
</xsl:template>

</xsl:stylesheet>

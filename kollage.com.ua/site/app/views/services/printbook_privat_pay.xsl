<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../layout.xsl" />

<xsl:template match="data">
	<h1><span class="red"><xsl:value-of select="$locale/photobook/order/printbook_title" disable-output-escaping="yes"/></span><xsl:value-of select="$locale/photobook/order/printbook_title3" disable-output-escaping="yes"/></h1>
	
	<xsl:if test="/content/global/client/id = 0">
		<script type="text/javascript">
		$j(function(){
			Events.publish('/Login/Show');
		});
		</script>
	</xsl:if>
	
	<xsl:if test="/content/global/client/id > 0">
		<!-- FORM -->
			<div class="p1">
				<span style="color:#336699"><xsl:value-of select="$locale/liqpay/wait/text()" disable-output-escaping="yes"/></span>
				<div style="display:none"><xsl:value-of select="form" disable-output-escaping="yes"/></div>
				<script>
				$j(function OnLoad()
				{
					document.getElementById('liqpay').action = 'https://www.liqpay.ua/api/3/checkout';
					document.getElementById('liqpay').submit();
				});
				</script>
			</div>
		<!-- FORM - END -->
	</xsl:if>
</xsl:template>

</xsl:stylesheet>

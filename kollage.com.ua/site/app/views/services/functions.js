
function ShowUploadPrintbookFilesForm()
{
	$j('#dlg-add-images-flash-container').dialog('open');
	var uploader = FineUploaderWrapper.init({
		request: {
			endpoint: '/?services.add_printbookfile_fine_uploader',
		},
		validation: {
			allowedExtensions: ['jpeg', 'jpg'],
			sizeLimit: 900 * 1024 * 1024
		},
		callbacks: {
			onAllComplete: function() {
				CloseUploadPrintbookFilesForm();
				pb_order.refresh_items();
			}
		}
	});
	if (uploader) {
		$j('#fine-uploader-trigger-upload').click(function() {
			uploader.uploadStoredFiles();
		});
	}
}
function CloseUploadPrintbookFilesForm()
{
	$j('#dlg-add-images-flash-container').dialog('close');
}

/* queue */
function AQueue() {      
	this._items = [];
	this._timer = null;
	this._timeout = 1500;
}

AQueue.prototype = {
	init: function( options ) {
		var self = this;
		this._timer = setTimeout( function() {
			self.process();
		}, this._timeout );
	},
	
	process: function() {
		var self = this;
		var items = this.normalize();
		
		for( var i=0; i<items.length; i++ ) {
			var params = ( items[i].options && items[i].options.length == 1 )? items[i].options[0] : items[i].options;
			if( items[i].context ) {
				var args = (typeof(params) == 'array')? params : [params];
				items[i].fn.apply( items[i].context, args );
			}
			else items[i].fn( params );
		}
		
		this._items.length = 0;
		
		clearTimeout( this._timer );
		this._timer = setTimeout( function() {
			self.process();
		}, this._timeout );
	},
	
	add: function( fn, options, context ) {
		this._items.push( new AQueueItem(fn, options, context) );
	},
	
	normalize: function() {
		var retval = new Array();
		
		if( this._items.length > 0 ) retval.push( this._items[0] );
		for( var i=0; i<this._items.length; i++ ) {
			var w = true;
			for( var j=0; j<retval.length; j++ ) {
				if( retval[j].fn == this._items[i].fn && retval[j].options == this._items[i].options ) {
					w = false;
					break;
				}
				if( w ) retval.push( this._items[i] );
			}
		}
		
		return( retval );
	}
};

function AQueueItem( fn, options, context ) {
	this.fn = fn;
	this.options = options;
	this.context = context;
}
/* queue */

/* pb order */
var pb_order = {
	order: {
		images: []
	},
	items: [],
	div_items_id: null,
	div_items: null,
	queue: null,
	itemsPaginator: null,
	
	init: function( div_items_id ) {
		var self = this;
		
		this.div_items_id = div_items_id;
		this.div_items = $(this.div_items_id);
		
		this.queue = new AQueue();
		this.queue.init();
		
		this.get_order();
		
		Events.subscribe( '/PB/AfterCalculate', function( pb ){
			self.on_pb_calculate( pb );
		} );
	},
	
	get_order: function( cb ) {
		var self = this;
		if( this.div_items ) {
			this.items_show_loader();
				d = new Date();
				JsHttpRequest.query(
				'/?services.pb_get_order&date='+ d.getTime(),
				{
				},
				function( result, txt )
				{
					self.items_hide_loader();
					if( result['err'] == 0 )
					{
						self.order = result['pb_order'];
						self.items = self.order.images;
						
						if( typeof(cb) == 'function' ) cb( result );
					}
				}
			);
		}
	},
	
	refresh_items: function( cb ) {
		var self = this;
		if( this.div_items ) {
			this.items_show_loader();
				d = new Date();
				JsHttpRequest.query(
				'/?services.pb_get_images&date='+ d.getTime(),
				{
				},
				function( result, txt )
				{
					self.items_hide_loader();
					if( result['err'] == 0 )
					{
						//self.order = result['pb_order'];
						//self.items = self.order.images;
						self.items = result['items'];
						
						if( typeof(cb) == 'function' ) cb( result );
						
						var savePage = -1;
						if( self.itemsPaginator ) {
							savePage = self.itemsPaginator.options[self.itemsPaginator.selectedIndex].value;
						}
						
						$A( self.div_items.childElements() ).each( Element.remove );
						
						if( self.items.length > 0 ) {
							var itemsHeader = new Element('div');
							itemsHeader.appendChild( new Element( 'b' ).update('Загруженные изображения:<span style="padding-left:225px">&nbsp;</span>Страница:&nbsp;') );
							self.itemsPaginator = new Element('select');
							itemsHeader.appendChild( self.itemsPaginator );
							self.div_items.appendChild( itemsHeader );
							self.fill_paginator();
							self.itemsPaginator.observe('change',function(event){
								//self.show_page( this.options[event.target.selectedIndex].value );
								self.show_page( this.options[this.selectedIndex].value );
							});
							
							self.init_order_page_comments();
						}
						
						for( var i=0; i<self.items.length; i++ ) {
							var item = self.make_item( self.items[i], result, i );
							self.div_items.appendChild( item );
							self.item_select_type( item, self.items[i].type );
						}
						self.div_items.appendChild( new Element('div',{'class':'clear_both'}) );
						
						self.check_items();
						if( self.items.length > 0 ) {
							for( var i=0; i<self.itemsPaginator.options.length; i++ ) {
								if( self.itemsPaginator.options[i].value == savePage ) {
									self.itemsPaginator.selectedIndex = i;
								}
							}
							if( self.itemsPaginator.selectedIndex == 0 ) savePage = -1; //All 
							self.show_page( savePage );
						}
					}
				}
			);
		}
	},
	
	init_order_page_comments: function( page ) {
		var self = this;
		
		var div = new Element( 'div', {
			'class': 'photobook',
			'id': 'div_order_page_comment'
		} );
		div.update( '<table><tr><td><table><tr><td><label>Описание для страницы</label></td><td><div style="padding-top:13px"><input type="button" disabled="true" style="margin-left:10px" id="btn_pb_order_page_save"  class="button" value="сохранить"/></div></td></tr></table></td></tr><tr><td><textarea id="pb_order_page_comment" class="pb_order_page_comment"></textarea></td></tr></table>' );
		self.div_items.appendChild( div );
		
		var btn = $('btn_pb_order_page_save');
		btn.disabled = false;
		btn.observe('click', function(event) {
			self.order_page_comment_set();
		});
	},
	
	order_page_comment_hide: function() {
		$('div_order_page_comment').hide();
	},
	
	order_page_comment_get: function( page ) {
		var self = this;
		
		var t = $('pb_order_page_comment');
		t.value = '';
		
		d = new Date();
		JsHttpRequest.query(
			'/?services.pb_order_page_comment_get&date='+ d.getTime(),
			{
				'page': page
			},
			function( result, txt )
			{
				if( result['err'] == 0 )
				{
					$('div_order_page_comment').show();
					t.value = result['value'];
				}
			}
		);
	},
	
	order_page_comment_set: function() {
		var self = this;
		
		var page = self.itemsPaginator.options[self.itemsPaginator.selectedIndex].value;
		var t = $('pb_order_page_comment');
		
		d = new Date();
		JsHttpRequest.query(
			'/?services.pb_order_page_comment_set&date='+ d.getTime(),
			{
				'comment': t.value,
				'page': page 
			},
			function( result, txt )
			{
				if( result['err'] == 0 )
				{
				}
			}
		);
	},
	
	fill_paginator: function() {
		var p = pb_order.itemsPaginator;
		if( p ) {
			p.options.length = 0;
			
			p.appendChild( new Element('option',{
				'value': -1
			}).update('Все') );
			
			p.appendChild( new Element('option',{
				'value': 0
			}).update('Непронумерованные') );
			
			p.appendChild( new Element('option',{
				'value': -2
			}).update('Обложка') );
			
			p.appendChild( new Element('option',{
				'value': -3
			}).update('Форзац 1') );
			
			p.appendChild( new Element('option',{
				'value': -4
			}).update('Форзац 2') );
			
			p.appendChild( new Element('option',{
				'value': -5
			}).update('Суперобложка') );
			
			for( var i=1; i<=pb.getPages(); i++ ) {
				p.appendChild( new Element('option',{
					'value': i
				}).update(i) );
			}
		}
	},
	
	show_page: function( page ) {
		var self = this;
		var items = self.get_all_items();
		for( var i=0; i<items.length; i++ ) {
			// region: adjust item block according to the selected parameters
			var el = items[i];
			this.item_apply_type_personal( el );
			// /region: adjust item block according to the selected parameters
						
			if( page == -1 ) { //All
				items[i].show();
			}
			else if( page == -2 && self.item_get_selected_type_id(items[i]) == 2 ) { //Cover
				items[i].show();
			}
			else if( page == -3 && self.item_get_selected_type_id(items[i]) == 3 ) { //Forzac1
				items[i].show();
			}
			else if( page == -4 && self.item_get_selected_type_id(items[i]) == 4 ) { //Forzac2
				items[i].show();
			}
			else if( page == -5 && self.item_get_selected_type_id(items[i]) == 5 ) { //Supercover
				items[i].show();
			}
			else {
				if( parseInt(page) == parseInt( self.item_get_page(items[i])) && parseInt(self.item_get_selected_type_id(items[i])) == 1 ) {
					items[i].show();
				}
				else items[i].hide();
			}
		}
		
		if( page==-2 || page==-3 || page==-4 || page==-5 || page>0 ) {
			self.order_page_comment_get( page );
		}
		else {
			self.order_page_comment_hide();
		}
	},
	
	item_apply_type_personal: function( el ) {
		var self = this;
		
		if( el ) {
			var p = el.select("p.pb_order_item_page_p")[0];
			var el_type = self.item_get_selected_type_id(el);
			if( el_type != 1 && p ) {
				p.setStyle({
					'visibility': 'hidden'
				});
			}
			else if( p ) {
				p.setStyle({
					'visibility': 'visible'
				});
			}
			
			var pp = el.select("p.pb_order_item_personal_page_p")[0];
			if( pp && pb.getQuantity() > 1 && el_type == 2 && pb.pb_personal_cover.value == 1 ) {
				pp.setStyle({
					'visibility': 'visible'
				});
			}
			else if( pp && pb.getQuantity() > 1 && el_type == 1 && pb.isThisPersonalPage(this.item_get_page(el)) ) {
				pp.setStyle({
					'visibility': 'visible'
				});
			}
			else if( pp && pb.getQuantity() > 1 && (el_type == 3) && pb.isThisPersonalPage("f1") ) {
				pp.setStyle({
					'visibility': 'visible'
				});
			}
			else if( pp && pb.getQuantity() > 1 && (el_type == 4) && pb.isThisPersonalPage("f2") ) {
				pp.setStyle({
					'visibility': 'visible'
				});
			}
			else {
				pp.setStyle({
					'visibility': 'hidden'
				});
				el.select('select.pb_order_item_personal_page')[0].selectedIndex = 0;
			}
		}
	},
	
	// it solves the problem with IE
	item_select_type: function( el, type ) {
		var idx = 0;
		type = parseInt( type );
		switch( type ) {
			case 1:
				idx = 0;
				break;
			case 2:
				idx = 1;
				break;
			case 3:
				idx = 2;
				break;
			case 4:
				idx = 3;
				break;
			case 5:
				idx = 4;
				break;
		}
		el.select('input[type=radio]')[idx].checked = true;
	},
	
	item_update: function( el ) {
		var self = this;
		if( el ) {
				d = new Date();
				JsHttpRequest.query(
				'/?services.pb_update_image&date='+ d.getTime(),
				{
					'id': self.item_get_id(el),
					'page': self.item_get_page(el),
					'personal_page': self.item_get_personal_page(el),
					'comments': self.item_get_comments(el),
					'type': self.item_get_selected_type_id(el)
				},
				function( result, txt )
				{
					if( result['err'] == 0 )
					{
						self.item_clear_changed_flag( el );
						//pb_order.show_page( pb_order.itemsPaginator.options[pb_order.itemsPaginator.selectedIndex].value );
						
						self.show_page( pb_order.itemsPaginator.options[pb_order.itemsPaginator.selectedIndex].value );
					}
				}
			);
		}
	},
	
	delete_selected_items: function() {
		var self = this;
		var cbs = this.get_selected_items();
		if( cbs.length > 0 ) {
			var ids = '';
			var glue = '';
			for( var i=0; i<cbs.length; i++ ) {
				var id = this.item_get_id( cbs[i] );
				ids += glue + id;
				glue = ',';
			} 
			
			d = new Date();
			JsHttpRequest.query(
				'/?services.pb_delete_images&date='+ d.getTime(),
				{
					'ids': ids
				},
				function( result, txt )
				{
					if( result['err'] == 0 )
					{
						self.refresh_items();
					}
					else {
						Alert.error( result['msg'] );
					}
				}
			);
		}
	},
	
	delete_all_items: function() {
		var self = this;
		d = new Date();
		JsHttpRequest.query(
			'/?services.pb_delete_all_images&date='+ d.getTime(),
			{},
			function( result, txt )
			{
				if( result['err'] == 0 )
				{
					self.refresh_items();
				}
				else {
					Alert.error( result['msg'] );
				}
			}
		);
	},
	
	delete_item: function( el ) {
		var self = this;
		var id = 0;
		if( el ) {
			if( el.hasClassName('pb_order_item') ) id = pb_order.item_get_id( el );
			else if( el.hasClassName('order_item_select_bdel') ) id = el.readAttribute('id');
			else if( el.hasClassName('ie_order_item_select_bdel') ) id = el.readAttribute('id');
			
			d = new Date();
			JsHttpRequest.query(
				'/?services.pb_delete_image&date='+ d.getTime(),
				{
					'id': id
				},
				function( result, txt )
				{
					if( result['err'] == 0 )
					{
						self.refresh_items();
					}
					else {
						Alert.error( result['msg'] );
					}
				}
			);
		}
	},
	
	get_all_items: function() {
		return( $(this.div_items).select('div.pb_order_item') );
	},
	
	get_selected_items: function() {
		var retval = new Array();
		var all = this.get_all_items();
		for( var i=0; i<all.length; i++ ) {
			if( all[i].select('input.order_item_select_cb')[0].checked ) retval.push( all[i] );
		}
		return( retval );
	},
	
	get_item_from_child: function( child_el ) {
		var retval = null;
		
		if( child_el ) {
			child_el = $(child_el);
			retval = child_el.up('div.pb_order_item');
		}
		
		return( retval );
	},
	
	item_set_changed_flag: function( el ) {
		var retval = false;
		
		if( el && el.hasClassName('pb_order_item') ) {
			el.writeAttribute( 'changed', 1 );
			this.queue.add( this.item_update, el, this );
			retval = true;
		} 
		
		return( retval );
	},
	
	item_clear_changed_flag: function( el ) {
		var retval = false;
		
		if( el && el.hasClassName('pb_order_item') ) {
			el.writeAttribute( 'changed', 0 );
			retval = true;
		} 
		
		return( retval );
	},
	
	item_is_changed: function( el ) {
		var retval = false;
		
		if( el && el.hasClassName('pb_order_item') ) {
			var changed = parseInt( el.readAttribute( 'changed' ) );
			retval = (changed == 1)? true : false;
		} 
		
		return( retval );
	},
	
	item_get_id: function( el ) {
		var retval = 0;
		
		if( el && el.hasClassName('pb_order_item') ) {
			retval = parseInt( el.readAttribute('pb_order_image_id') );
		} 
		
		return( retval );
	},
	
	item_get_idx: function( el ) {
		var retval = 0;
		
		if( el && el.hasClassName('pb_order_item') ) {
			retval = parseInt( el.readAttribute('id').split('_')[3] );
		} 
		
		return( retval );
	},
	
	item_get_selected_type_id: function( el ) {
		var retval = 0;
		
		if( el && el.hasClassName('pb_order_item') ) {
			var types = el.select('div.pb_order_item_types')[0].select('input[type=radio]');
			for( var i=0; i<types.length; i++ ) {
				if( types[i].checked ) {
					var id_str = types[i].readAttribute('id'); //format pb_order_item_type_<element idx>_<type id>
					if( id_str && id_str != '' ) retval = parseInt( id_str.split('_')[5] );
					break;
				}
			}
		} 
		
		return( retval );
	},
	
	item_get_page: function( el ) {
		var retval = 0;
		
		if( el && el.hasClassName('pb_order_item') ) {
			//retval = parseInt( el.select('input.pb_order_item_page')[0].value );
			var o = this.get_selected_option( el.select('select.pb_order_item_page')[0] );
			retval = o.value;
		} 
		
		return( retval );
	},
	
	item_get_personal_page: function( el ) {
		var retval = 0;
		
		if( el && el.hasClassName('pb_order_item') ) {
			var o = this.get_selected_option( el.select('select.pb_order_item_personal_page')[0] );
			retval = o.value;
		} 
		
		return( retval );
	},
	
	item_get_comments: function( el ) {
		var retval = 0;
		
		if( el && el.hasClassName('pb_order_item') ) {
			retval = el.select('textarea.pb_order_item_comment')[0].value;
		} 
		
		return( retval );
	},
	
	check: function() {
		var retval = '';
		
		//check selecting the size
		if( pb.getSizeId() <= 0 ) retval += "\n<br/>- Не выбран формат книги";
		
		//check images
		var items_err_str = this.check_items();
		if( items_err_str != '' ) retval += items_err_str;
		
		return( retval );
	},
	
	check_items: function() {
		var self = this;
		var items = this.get_all_items();
		var retval = '';
		
		if( items.length == 0 ) retval += "\n<br/>- Изображения не загружены";
		
		if( !retval ) {
			var there_is_cover = false;
			var there_is_supercover = false;
			var there_is_f1 = false;
			var there_is_f2 = false;
			var there_is_forzaces = (pb.getSelectedForzac()==1);
			var has_supercover = pb.hasSupercover();
			var check_cover_size = pb.getCoverCheckSize();
			for( var i=0; i<items.length; i++ ) {
				this.item_hide_message( items[i] );
				var type = this.item_get_selected_type_id( items[i] );
				if( parseInt(this.order.design_work.id) == 0 ) {
					var check_item_size_err = this.check_item_size( items[i] );
					if( check_item_size_err != '' ) retval += check_item_size_err;
				}
				if( type == 1 ) {
					if( this.item_get_page(items[i]) == '0' ) retval += "\n<br/>- Не определена страница у изображения '"+ this.items[i].original_filename +"'";
				}
				if( type == 2 ) there_is_cover = true;
				if( type == 3 ) there_is_f1 = true;
				if( type == 4 ) there_is_f2 = true;
				if( type == 5 ) there_is_supercover = true;
			}
			
			if( check_cover_size == 1 && !there_is_cover ) retval += "\n<br/>- Не загружено изображение для обложки";
			if( !there_is_supercover && has_supercover ) retval += "\n<br/>- Не загружено изображение для суперобложки";
			if( !there_is_f1 && there_is_forzaces ) retval += "\n<br/>- Не загружено изображение для переднего форзаца";
			if( !there_is_f2 && there_is_forzaces ) retval += "\n<br/>- Не загружено изображение для заднего форзаца";
		}
		
		if( !retval ) {
			for( var p=1; p<=pb.getPages(); p++ ) {
				var pe = false;
				for( var i=0; i<items.length; i++ ) {
					var type = this.item_get_selected_type_id( items[i] );
					if( type == 1 && self.item_get_page(items[i]) == p ) {
						pe = true;
					}
				}
				if( !pe ) {
					retval += "\n<br/>- Не загружено изображение для страницы \""+ p +"\"";
				}
			}
		}
			
		var design_work_id = pb.getDesignWorkId();
		if( !retval && pb.hasPersonalCover() ) {
			if( design_work_id > 0 ) retval += this.check_personal_cover( items );
			else retval += this.check_personal_cover_without_personalyzing( items );
		}
			
		if( !retval && pb.hasPersonalPages() ) {
			if( design_work_id > 0 ) retval += this.check_personal_pages( items );
			else retval += this.check_personal_pages_without_personalyzing( items );
		}
		
		return( retval );
	},
	
	check_personal_cover: function( items ) {
		var retval = '';
		
		var quantity = pb.getQuantity();
			for( var j=1; j<=quantity; j++ ) {
				var q_exist = false;
				for( var k=0; k<items.length; k++ ) {
					var type = this.item_get_selected_type_id( items[k] );
					if( type == 2 && this.item_get_personal_page(items[k]) == j ) {
						q_exist = true;
					}
				}
				if( !q_exist ) {
					retval += "\n<br/>- Для экземпляра "+ j +", обложка: не загружено персональное изображение";
				}
			}
		
		return( retval );
	},
	
	check_personal_cover_without_personalyzing: function( items ) {
		var retval = '';
		
		var quantity = pb.getQuantity();
			for( var j=1; j<=quantity; j++ ) {
				var q_exist = 0;
				for( var k=0; k<items.length; k++ ) {
					var type = this.item_get_selected_type_id( items[k] );
					if( type == 2 ) {
						q_exist++;
					}
				}
				if( q_exist != quantity ) {
					retval += "\n<br/>- Для обложки: не загружены все персональные изображения ("+ q_exist +"/"+ quantity +")";
					break;
				}
			}
		
		return( retval );
	},
	
	check_personal_pages: function( items ) {
		var retval = '';
		
		var quantity = pb.getQuantity();
		var sppa = this.order.selected_personal_pages.split(',');
		for( var i=0; i<sppa.length; i++ ) {
			for( var j=1; j<=quantity; j++ ) {
				var q_exist = false;
				for( var k=0; k<items.length; k++ ) {
					var type = this.item_get_selected_type_id( items[k] );
					if( type == 1 && this.item_get_page(items[k]) == sppa[i] && this.item_get_personal_page(items[k]) == j ) {
						q_exist = true;
					}
					else if( type == 3 && "f1" == sppa[i] && this.item_get_personal_page(items[k]) == j ) {
						q_exist = true;
					}
					else if( type == 4 && "f2" == sppa[i] && this.item_get_personal_page(items[k]) == j ) {
						q_exist = true;
					}
				}
				if( !q_exist ) {
					retval += "\n<br/>- Для экземпляра "+ j +", страница/форзац "+ sppa[i] +": не загружено персональное изображение";  
				}
			}
		}
		
		return( retval );
	},
	
	check_personal_pages_without_personalyzing: function( items ) {
		var retval = '';
		
		var quantity = pb.getQuantity();
		var sppa = this.order.selected_personal_pages.split(',');
		for( var i=0; i<sppa.length; i++ ) {
			for( var j=1; j<=quantity; j++ ) {
				var q_exist = 0;
				for( var k=0; k<items.length; k++ ) {
					var type = this.item_get_selected_type_id( items[k] );
					if( type == 1 && this.item_get_page(items[k]) == sppa[i] ) {
						q_exist++;
					}
					else if( type == 3 && "f1" == sppa[i] ) {
						q_exist++;
					}
					else if( type == 4 && "f2" == sppa[i] ) {
						q_exist++;
					}
				}
				if( q_exist != quantity ) {
					retval += "\n<br/>- Для экземпляра "+ j +", страница/форзац "+ sppa[i] +": загружены не все персональные изображения ("+ q_exist +"/"+ quantity +")";
					break;
				}
			}
		}
		
		return( retval );
	},
	
	check_item_size: function( el ) {
		var retval = '';
		var self = this;
		var pmw = parseInt( this.order.size.page_min_width );
		var pmh = parseInt( this.order.size.page_min_height );
		var cmw = parseInt( this.order.size.cover_min_width );
		var cmh = parseInt( this.order.size.cover_min_height );
		
		var type = this.item_get_selected_type_id( el );
		
		var idx = this.item_get_idx( el );
		var msg = '';
		if( type == 2 ) {
			if( this.items[idx].filename_w < cmw || this.items[idx].filename_h < cmh ) {
				var needed_size = cmw +"x"+ cmh +"px";
				msg = "Размер обложки меньше требуемого ("+ needed_size +"). Удалите этот элемент или загрузите другое изображение:";
			}
		}
		else {
			if( this.items[idx].filename_w < pmw || this.items[idx].filename_h < pmh ) {
				var needed_size = pmw +"x"+ pmh +"px";
				msg = "Размер страницы меньше требуемого ("+ needed_size +"). Удалите этот элемент или загрузите другое изображение:";
			}
		}
		if( msg ) {
			retval = "\n<br/>- Изображение '"+ this.items[idx].original_filename +"' не соответствует параметрам книги";
			this.item_show_message( el, msg, function( element, message ) {
				var c = el.select('div.pb_order_item_message_content')[0];
				var f = new Element( 'div', {
					'type': 'file',
					'id': el.id +'_file',
					'class': 'file'
				} );
				f.update('Загрузить');
				c.appendChild( new Element('p') );
				c.appendChild( f );
			} );
		}
		
		return( retval );
	},
	
	item_show_loader: function( el ) {
		if( el && el.hasClassName('pb_order_item') ) {
			el.addClassName('progress_hr');
		}
	},
	
	item_hide_loader: function( el ) {
		if( el && el.hasClassName('pb_order_item') ) {
			el.removeClassName('progress_hr');
		}
	},
	
	item_show_message: function( el, text, cb ) {
		var retval = 0;
		
		if( el && el.hasClassName('pb_order_item') ) {
			var msg = el.select('div.pb_order_item_message')[0];
			if( msg ) {
				msg.select('div.pb_order_item_message_content')[0].update( text );
				msg.show();
				if( typeof(cb) == 'function' ) cb( el, msg ); 
			}
		} 
		
		return( retval );
	},
	
	item_hide_message: function( el ) {
		var retval = 0;
		
		if( el && el.hasClassName('pb_order_item') ) {
			var msg = el.select('div.pb_order_item_message')[0];
			if( msg ) {
				msg.select('div.pb_order_item_message_content')[0].update('');
				msg.hide();
			}
		} 
		
		return( retval );
	},
	
	make_item: function( item, result, idx ) {
		var self = this;
		
		var d = new Date();
		var a = new Element( 'a', {
			'class': 'highslide',
			'href': '/?services.pb_image&type=1&id='+ item.id +'&date='+ d.getTime(),
			'id': 'pb_image_'+ item.id,
			'title': item.comments +'&#160;'
		} );
		var img = new Element( 'img', {
			'src': '/?services.pb_image&type=0&id='+ item.id +'&date='+ d.getTime(),
			'width': '133',
			'height': '100'
		} );
		a.onclick = function( event ) {
			var id = this.getAttribute('id');
			var description = this.getAttribute('title');
			return hs.expand( this, { slideshowGroup: 100 + id, captionText: description } );
		};
		a.appendChild( img );
		var divImage = new Element( 'div', {
			'class': 'pb_order_item_image float_left clear_none' 
		});
		divImage.appendChild( a );
							
		var divComments = new Element( 'div', {
			'class': 'pb_order_item_comments float_left clear_none' 
		} );
		var pageP = new Element( 'p', {
			'class': 'pb_order_item_page_p'
		} ).update( 'Страница:&#160;&#160;&#160;' );
		var page = new Element( 'select', {
			'class': 'pb_order_item_page inputbox_small',
			'id': 'pb_order_item_page_'+ idx
		} )
		for( var i=0; i<=pb.getPages(); i++ ) {
			page.appendChild( new Element('option',{
				'value': i
			}).update( (i==0)? 'непронумерованая' : i ) );
		}
		self.set_selected_option( page, item.page );
		page.observe( 'change', function(event) {
			var el = self.get_item_from_child( event.target );
			self.item_apply_type_personal( el );
			self.item_set_changed_flag(el);
		} );
		pageP.appendChild( page );
		var personalPageP = new Element( 'p', {
			'class': 'pb_order_item_personal_page_p'
		} ).update( 'Экземпляр:&#160;' );
		var personalPage = new Element( 'select', {
			'class': 'pb_order_item_personal_page inputbox_small',
			'id': 'pb_order_item_personal_page_'+ idx
		} )
		personalPage.appendChild( new Element('option',{
			'value': 0
		}).update('-') );
		for( var i=1; i<=pb.getQuantity(); i++ ) {
			personalPage.appendChild( new Element('option',{
				'value': i
			}).update(i) );
		}
		self.set_selected_option( personalPage, item.personal_page );
		personalPage.observe( 'change', function(event) {
			var el = self.get_item_from_child( event.target );
			self.item_apply_type_personal( el );
			self.item_set_changed_flag(el);
		} );
		personalPageP.appendChild( personalPage );
		var comments = new Element( 'textarea', {
			'class': 'pb_order_item_comment',
			'placeholder': 'введите подпись или коментарий'
		} );
		comments.update( item.comments );
		comments.observe( 'change', function(event) {
			var el = self.get_item_from_child( event.target );
			self.item_set_changed_flag(el);
		} );
		divComments.appendChild( pageP );
		divComments.appendChild( personalPageP );
		divComments.appendChild( comments );
							
		var div = new Element( 'div', {
			'id': 'pb_order_item_'+ idx,
			'pb_order_image_id': item.id,
			'name': 'pb_order_item',
			'class': 'pb_order_item',
			'changed': 0 
		} );
							
		var divContent = new Element( 'div', {
			'class': 'pb_order_item_content'
		} );
		
		div.appendChild( divContent );
							
		var header = new Element( 'div', {
			'class': 'pb_order_item_header'
		} ).update( "<b>"+ item.original_filename +" ("+ item.filename_w +" x "+ item.filename_h +"px)</b>" );
		var cb = new Element( 'input', {
			'type': "checkbox",
			'class': 'order_item_select_cb'
		} );
		var bdel = new Element( 'a', {
			'class': (Prototype.Browser.IE)? 'ie_order_item_select_bdel' : 'order_item_select_bdel',
			'id': item.id,
			'title': 'Удалить данный элемент'
		} );
		bdel.onclick = function( event ) {
			self.delete_item( this );
		};
		header.appendChild( bdel );
		header.appendChild( cb );
							
		var divTypes = new Element( 'div', {
			'class': 'pb_order_item_types float_right clear_none' 
		} );
		var typeId = "pb_order_item_type_"+ idx; 
		var type1 = new Element( 'input', {
			'type': 'radio',
			'name': typeId,
			'id': typeId +'_1',
			'value': 1
		});
		type1.observe( 'click', function(event) {
			var el = self.get_item_from_child( event.target );
			self.item_apply_type_personal( el );
			self.item_set_changed_flag(el);
		} );
		var type1Label = new Element( 'label', {
			'for': typeId +'_1'
		} ); type1Label.update('страница');
		
		var type2 = new Element( 'input', {
			'type': 'radio',
			'name': typeId,
			'id': typeId +'_2',
			'value': 2
		});
		type2.observe( 'click', function(event) {
			var el = self.get_item_from_child( event.target );
			self.item_apply_type_personal( el );
			self.item_set_changed_flag(el);
		} );
		var type2Label = new Element( 'label', {
			'for': typeId +'_2'
		} ); type2Label.update('обложка');
		
		var type3 = new Element( 'input', {
			'type': 'radio',
			'name': typeId,
			'id': typeId +'_3',
			'value': 3
		});
		type3.setAttribute( 'checked', 'true' );
		type3.observe( 'click', function(event) {
			var el = self.get_item_from_child( event.target );
			self.item_apply_type_personal( el );
			self.item_set_changed_flag(el);
		} );
		var type3Label = new Element( 'label', {
			'for': typeId +'_3'
		} ); type3Label.update('форзац1');
		
		var type4 = new Element( 'input', {
			'type': 'radio',
			'name': typeId,
			'id': typeId +'_4',
			'value': 4
		});
		type4.observe( 'click', function(event) {
			var el = self.get_item_from_child( event.target );
			self.item_apply_type_personal( el );
			self.item_set_changed_flag(el);
		} );
		var type4Label = new Element( 'label', {
			'for': typeId +'_4'
		} ); type4Label.update('форзац2');
		
		var type5 = new Element( 'input', {
			'type': 'radio',
			'name': typeId,
			'id': typeId +'_5',
			'value': 5
		});
		type5.observe( 'click', function(event) {
			var el = self.get_item_from_child( event.target );
			self.item_apply_type_personal( el );
			self.item_set_changed_flag(el);
		} );
		var type5Label = new Element( 'label', {
			'for': typeId +'_5'
		} ); type5Label.update('суперобложка');
				
		divTypes.update( "<p>Тип элемента:</p>" );
		var type1Row = new Element('div'); type1Row.appendChild( type1 ); type1Row.appendChild( type1Label );
		var type2Row = new Element('div'); type2Row.appendChild( type2 ); type2Row.appendChild( type2Label );
		var type3Row = new Element('div'); type3Row.appendChild( type3 ); type3Row.appendChild( type3Label );
		var type4Row = new Element('div'); type4Row.appendChild( type4 ); type4Row.appendChild( type4Label );
		var type5Row = new Element('div'); type5Row.appendChild( type5 ); type5Row.appendChild( type5Label );
		
		var forzac = pb.getSelectedForzac() || 1;
		if( forzac == 2 ) {
			type3Row.hide();
			type4Row.hide();
		}
		
		var supercover = ( pb.getSuperCoverPrice() > 0 )? true : false;
		if( !supercover ) {
			type5Row.hide();
		} 
		
		divTypes.appendChild( type1Row );
		divTypes.appendChild( type2Row );
		divTypes.appendChild( type3Row );
		divTypes.appendChild( type4Row );
		divTypes.appendChild( type5Row );
							
		divContent.appendChild( header );
		divContent.appendChild( divImage );
		divContent.appendChild( divComments );
		divContent.appendChild( divTypes );
		
		var message = new Element( 'div', {
			'class': (Prototype.Browser.IE)? 'pb_order_item_message ie_pb_order_item_message' : 'pb_order_item_message',
			'style': 'display:none',
			id: 'pb_order_item_message_'+ idx
		} );
		message.appendChild( new Element( 'div', {
			'class': 'pb_order_item_message_content'
		} ) );
		var message_button = new Element( 'input', {
			'type': 'button',
			'class': 'button close_button',
			'value': 'Закрыть сообщение'
		} );
		message_button.observe( 'click', function(event) {
			self.item_hide_message( div );
		} );
		message.appendChild( message_button );
		div.appendChild( message );
		
		return( div );
	},
	
	items_show_loader: function() {
	},
	
	items_hide_loader: function() {
	},
	
	/* is called when a parameter in the calculator is changed */
	on_pb_calculate: function( calc ) {
		var self = pb_order;
		self.queue.add( self.update, [calc] );
		//self.fill_paginator();
		//if( self.itemsPaginator ) self.show_page( self.itemsPaginator.options[self.itemsPaginator.selectedIndex].value );
	},
	
	/*
		Update pb_order object according to the calculator selected parameters 
		pb-calculator object
	*/
	updateTimer: null,
	update: function( obj, cb ) {
		var calc = obj;
		var self = this;
		var d = new Date();
		if( self.updateTimer ) {
			clearTimeout( self.updateTimer );
		}
		self.updateTimer = setTimeout(function(){
			JsHttpRequest.query(
				'/?services.pb_update&date='+ d.getTime(),
				{
					"size_id": calc.getSizeId(),
					"pages": calc.getPages(),
					"quantity": calc.getQuantity(),
					"paper_id": calc.getPaperId(),
					"paper_price": calc.getPaperPrice(),
					"lamination_id": calc.getLaminationId(),
					"lamination_price": calc.getLaminationPrice(),
					"paper_base_id": calc.getPaperBaseId(),
					"paper_base_price": calc.getPaperBasePrice(),
					"personal_pages": calc.getPersonalPages(),
					"selected_personal_pages": calc.getSelectedPersonalPagesAsString(),
					"personal_cover_id": calc.getPersonalCoverId(),
					"personal_cover_price": calc.getPersonalCoverPrice(),
					"cover_id": calc.getCoverId(),
					"cover_price": calc.getCoverPrice(),
					"supercover_id": calc.getSuperCoverId(),
					"supercover_price": calc.getSuperCoverPrice(),
					"box_id": calc.getBoxId(),
					"box_price": calc.getBoxPrice(),
					"design_work_id": calc.getDesignWorkId(),
					"design_work_base_price": calc.getDesignWorkBasePrice(),
					"design_work_price": calc.getDesignWorkPrice(),
					"forzaces": calc.getSelectedForzac(),
                    "forzac_id": calc.getPhotobookForzaceId(),
					"forzac_price": calc.getPhotobookForzacePrice()
				},
				function( result, txt )
				{
					pb_order.order = result['pb_order'];
					pb_order.queue.add( pb_order.refresh_items, null, pb_order );
					if( typeof(cb) == 'function' ) cb( result );
				}
			);
		}, 500);
	},
	
	make_params_for_calculator: function( order ) {
		return({
			"size_id": parseInt(order.size_id),
			"pages": parseInt(order.pages),
			"quantity": parseInt(order.quantity),
			"paper_id": parseInt(order.paper_id),
			"lamination_id": parseInt(order.lamination_id),
			"paper_base_id": parseInt(order.paper_base_id),
			"personal_pages": order.personal_pages,
			"selected_personal_pages": order.selected_personal_pages,
			"personal_cover_id": parseInt(order.personal_cover_id),
			"cover_id": parseInt(order.cover_id),
			"supercover_id": parseInt(order.supercover_id),
			"box_id": parseInt(order.box_id),
			"design_work_id": parseInt(order.design_work_id),
			"forzaces": parseInt(order.forzaces),
            "forzac_id": parseInt(order.forzac_id),
		});
	},
	
	continue_order: function() {
		var err = this.check();
		if( err =='' ) {
			document.location = '/services/printbook_order2';
		}
		else {
			Alert.error( "Список ошибок:"+ err );
		}
	},
	
	/* common */
	get_selected_option: function( el ) {
		var retval = new Element('option');
		
		if( el ) {
			var options = el.select('option');
			retval = options.find(function(ele){
				return(!!ele.selected);
			});
		}
		
		return( retval );
	},
	
	set_selected_option: function( el, val ) {
		var retval = new Element('option');
		
		if( el ) {
			var options = el.select('option');
			retval = options.each(function(ele){
				if( ele.value == val ) ele.selected = true;
			});
		}
		
		return( retval );
	}
	/* common */
};


function LoadPaymentways( delivery, paymentway, paymentway_id )
{
	var d;
	paymentway.options.length = 0;
	paymentway.options[0] = new Option( 'загрузка...', 0 );
	
	d = new Date();
	JsHttpRequest.query(
		'/?services.printbook_load_paymentways&date='+ d.getTime(),
		{
			'delivery_id': delivery.options[delivery.selectedIndex].value
		},
		function( result, txt )
		{
			if( result['err'] == 0 && result['paymentways'] )
			{
				paymentway.options.length = 0;
				for( var i=0; i<result['paymentways'].length; i++ )
				{
					paymentway.options[i] = new Option( result['paymentways'][i][1], result['paymentways'][i][0] );
					if( result['paymentways'][i][0] == paymentway_id )
					{
						paymentway.selectedIndex = i;
					}
				}
				$('price_delivery').innerHTML = (Math.floor( parseFloat( result['delivery'].price ) )).toFixed(2);
				$('price_total').innerHTML = (parseFloat( result['delivery'].price ) +
									parseFloat( $('price_one').innerHTML ) * parseFloat( $('quantity').innerHTML ) +
									parseFloat( $('price_design_work').innerHTML )).toFixed(2);
			}
		}
	);
}
/* pb order */

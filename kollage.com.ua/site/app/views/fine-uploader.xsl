<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:template name="fine_uploader_layouts">
	<script type="text/template" id="qq-template-manual-trigger">
		<div class="qq-uploader-selector qq-uploader" qq-drop-area-text="������ ������������� ����� � ��� ����" style="width:450px;height:490px;overflow-y:hidden;overflow-x:hidden">
			<div class="qq-upload-drop-area-selector qq-upload-drop-area qq-hide-dropzone">
				<span class="qq-upload-drop-area-text-selector"></span>
			</div>
			<div class="buttons" style="position:relative;z-index:10;">
				<div class="qq-upload-button-selector qq-upload-button" style="margin-right:10px">
					<div>������� �����</div>
				</div>
				<button type="button" id="fine-uploader-trigger-upload" class="qq-upload-button">
					<div>���������</div>
				</button>
			</div>
			<span class="qq-drop-processing-selector qq-drop-processing">
				<span>���������...</span>
				<span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
			</span>
			<ul class="qq-upload-list-selector qq-upload-list" aria-live="polite" aria-relevant="additions removals"  style="position:relative;z-index:10">
				<li>
					<div class="qq-progress-bar-container-selector">
						<div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
					</div>
					<span class="qq-upload-spinner-selector qq-upload-spinner"></span>
					<span style="display:inline-block;width:80px"><img class="qq-thumbnail-selector" style="max-height:40px" /></span>
					<span class="qq-upload-file-selector qq-upload-file" style="width:200px"></span>
					<span class="qq-upload-size-selector qq-upload-size"></span>
					<button type="button" class="qq-btn qq-upload-cancel-selector qq-upload-cancel">��������</button>
					<button type="button" class="qq-btn qq-upload-retry-selector qq-upload-retry">�������������</button>
					<button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete">�������</button>
					<span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
				</li>
			</ul>

			<dialog class="qq-alert-dialog-selector">
				<div class="qq-dialog-message-selector"></div>
				<div class="qq-dialog-buttons">
					<button type="button" class="qq-cancel-button-selector">�������</button>
				</div>
			</dialog>

			<dialog class="qq-confirm-dialog-selector">
				<div class="qq-dialog-message-selector"></div>
				<div class="qq-dialog-buttons">
					<button type="button" class="qq-cancel-button-selector">���</button>
					<button type="button" class="qq-ok-button-selector">��</button>
				</div>
			</dialog>

			<dialog class="qq-prompt-dialog-selector">
				<div class="qq-dialog-message-selector"></div>
				<input type="text" />
				<div class="qq-dialog-buttons">
					<button type="button" class="qq-cancel-button-selector">������</button>
					<button type="button" class="qq-ok-button-selector">Ok</button>
				</div>
			</dialog>
		</div>
	</script>
</xsl:template>

</xsl:stylesheet>

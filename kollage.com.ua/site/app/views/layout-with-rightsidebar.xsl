<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output 
	media-type="text/html"
	method="html"
	encoding="utf-8"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	indent="no"/>

<xsl:include href="login_form.xsl" />
<xsl:include href="login_form2.xsl" />
<xsl:include href="client_form.xsl" />

<xsl:template match="/">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru-ru" lang="ru-ru">
	<head>
		<meta name="robots" content="index, follow" />
		<meta name="keywords" content="Kollage, ����������, ����������, ���������, �����������"/>
		<meta name="description" content="Kollage, ����������, ����������, ���������, �����������"/>
		<title>
		<xsl:choose>
			<xsl:when test="/content/documents/data/article/header">
				<xsl:value-of select="/content/documents/data/category/name"/> / <xsl:value-of select="/content/documents/data/article/header"/>, <xsl:value-of select="$locale/common/title" disable-output-escaping="yes"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$locale/common/title/text()" disable-output-escaping="yes"/>
			</xsl:otherwise>
		</xsl:choose>
		</title>
		
		<script type="text/javascript" src="{$views_path}/shared/JsHttpRequest.js"></script>
		<script type="text/javascript" src="{$views_path}/shared/swfobject/swfobject.js"></script>
		
		<link rel="stylesheet" href="/js/highslide/highslide.css" type="text/css" charset="utf-8"/>
		<script type="text/javascript" src="/js/highslide/highslide-with-gallery.packed.js"></script>
		
		<link rel="stylesheet" href="/js/valumsfu/fileuploader.css" type="text/css" charset="utf-8"/>
		<script type="text/javascript" src="/js/valumsfu/fileuploader.js"></script>
		
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/prototype/1.7.0.0/prototype.js"></script>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/scriptaculous/1.8.3/scriptaculous.js?load=effects,dragdrop"></script>
		
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
	
		<script type="text/javascript" src="/js/pubsub.js"></script>
		<link rel="stylesheet" type="text/css" href="/js/jquery/plugins/jquery-mega-drop-down-menu/css/dcmegamenu.css"/>
		<link rel="stylesheet" type="text/css" href="/js/jquery/plugins/jquery-mega-drop-down-menu/css/skins/custom.css"/>
		<script type="text/javascript" src="/js/jquery/plugins/jquery-mega-drop-down-menu/js/jquery.hoverIntent.minified.js"></script>
		<script type="text/javascript" src="/js/jquery/plugins/jquery-mega-drop-down-menu/js/jquery.dcmegamenu.1.3.3.min.js"></script>

		<link rel="stylesheet" href="/fonts/zrnic/stylesheet.css" type="text/css" charset="utf-8"/>
		<link rel="stylesheet" href="/fonts/droidsans-bold/stylesheet.css" type="text/css" charset="utf-8"/>
		<link rel="stylesheet" href="{$views_path}/styles.css" type="text/css" charset="utf-8"/>
	
		<script type="text/javascript">
		var $j = jQuery.noConflict();
		</script>
		<script type="text/javascript" src="https://apis.google.com/js/plusone.js">
		{lang: 'ru'}
		</script>
		
		<script type="text/javascript" src="{$views_path}/common.js"></script>
	</head>
<body>
	<div class="header-wrapper">
		<div class="header">
			<div class="header-main-block-wrapper">
				<div class="header-main-block">
					<div class="login-register"><a href="#" class="link-register" onclick="return(false)">�����������</a> / <a href="#" class="link-login" onclick="return(false)">����</a></div>
					<div class="header-phones">
						<div class="header-phone">(095) 128 15 51</div>
						<div class="header-phone hidden">(0542) 61 20 23</div>
					</div>
					<div class="header-search">
						<div class="header-search-bg">
							<input type="text" value="������..."/>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="top-menu-wrapper">
		<div class="top-menu black">
			<ul class="mega-menu">
				<xsl:apply-templates select="content/documents/topmenu"/>
			</ul>
		</div>
	</div>
	
	<div class="page-wrapper">
		<div class="page">
			<div class="page-content">
				<xsl:apply-templates select="content/documents/data"/>
				<div class="clear-both"></div>
			</div>
		</div>
		<div class="page-rightsidebar">
			<xsl:call-template name="best_products"/>
		</div>
	</div>
	<div class="page-clear-both"></div>
	
	<div class="footer-wrapper">
		<div class="footer">
			<div class="copyr"><xsl:value-of select="$locale/common/footer_text" disable-output-escaping="yes"/></div>
			<div class="phones"><xsl:value-of select="$locale/common/footer_phones" disable-output-escaping="yes"/></div>
		</div>
	</div>
	
	<!-- google -->
	<script type="text/javascript">
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-28053790-2']);
	  _gaq.push(['_trackPageview']);
	
	  (function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
	</script>
	<!-- google -->
	
	<!--  siteheart -->
	<script type="text/javascript"> _shcp = []; _shcp.push({widget_id : 606801, widget : "Chat"}); (function() { var hcc = document.createElement("script"); hcc.type = "text/javascript"; hcc.async = true; hcc.src = ("https:" == document.location.protocol ? "https" : "http")+"://widget.siteheart.com/apps/js/sh.js"; var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(hcc, s.nextSibling); })();</script>
	<!--  siteheart -->
</body>
</html>
</xsl:template>

<xsl:template match="topmenu/*">
	<li>
		<xsl:choose>
			<xsl:when test="@url!=''">
				<a href="{@url}"><xsl:value-of select="@title" disable-output-escaping="yes"/></a>
			</xsl:when>
			<xsl:otherwise>
				<a href="#" onclick="return(false)"><xsl:value-of select="@title"/></a>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:if test="count(./node()[starts-with(name(),'submenu')]) &gt; 0">
		<ul>
			<xsl:apply-templates select="./node()[starts-with(name(),'submenu')]"/>
		</ul>
		</xsl:if>
		<xsl:if test="count(./item) &gt; 0">
		<ul>
			<xsl:apply-templates select="./item"/>
		</ul>
		</xsl:if>
	</li>
</xsl:template>

<xsl:template match="topmenu/*/node()[starts-with(name(),'submenu')]">
	<li><a href="#" onclick="return(false)"><xsl:value-of select="@title"/></a>
		<xsl:if test="count(./item) &gt; 0">
		<ul>
			<xsl:apply-templates select="./item"/>
		</ul>
		</xsl:if>
	</li>
</xsl:template>

<xsl:template match="topmenu/*/item">
	<xsl:if test="@title != ''">
		<li><a href="{@url}"><xsl:value-of select="@title" disable-output-escaping="yes"/></a></li>
	</xsl:if>
</xsl:template>

<xsl:template match="topmenu/*/*/item">
	<xsl:if test="@title != ''">
		<li><a href="{@url}"><xsl:value-of select="@title" disable-output-escaping="yes"/></a></li>
	</xsl:if>
</xsl:template>

</xsl:stylesheet>

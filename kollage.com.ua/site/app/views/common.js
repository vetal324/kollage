
var Alert = {
	options: {
		message: {
			title: 'Сообщение'
		},
		error: {
			title: 'Ошибка'
		}
	},
	
	message: function( text ) {
		var o = this.options;
		jAlert( text, o.message.title );
	},
	
	error: function( text ) {
		var o = this.options;
		jAlert( text, o.error.title );
	}
};

var ADialog = {
	options: {
	},
	
	adjust: function( dialog ) {
		dialog = $j(dialog);
		var p = dialog.parent();
		p.find('.ui-dialog-titlebar').removeClass('ui-corner-all');
		p.find('.ui-dialog-titlebar').addClass('ui-corner-tl ui-corner-tr');
	}
};

var TopMenu = {
	options : {
		selector: '.top-menu ul.mega-menu'
	},
	
	init: function() {
		var self = this, o = this.options;
		
		$j(o.selector).show();
		
		$j(o.selector).dcMegaMenu({
			rowItems: '4',
			speed: 'fast',
			effect: 'fade'
		});
	}
};

var Tools = {
	getTimestamp: function() {
		var retval = new Date().getTime();
		return( retval );
	},
	
	formatEmail: function( email ) {
		if( email ) {
			var v = jQuery.trim( email );
			v = v.replace(/[\s\\\/=&\(\)\[\]\{\}%\+\^]+/gi,'');
			v = v.replace(/[\.]{2}/,'.');
			v = v.replace(/[@]{2}/,'.');
			email = v;
		}
		else email = '';
		
		return( email );
	},
	
	formatPhone: function( phone ) {
		if( phone ) {
			var v = jQuery.trim( phone );
			v = v.replace(/[\s\\\/=&\(\)\[\]\{\}%\+\^\.]+/gi,'');
			phone = v;
		}
		else phone = '';
		
		return( phone );
	},
	
	escapeHtml: function( unsafe ) {
		return unsafe
			 .replace(/&/g, "&amp;")
			 .replace(/</g, "&lt;")
			 .replace(/>/g, "&gt;")
			 .replace(/"/g, "&quot;")
			 .replace(/'/g, "&#039;");
	}
};

var UrlTools = {
	getCurrentHost: function() {
		var http = location.protocol;
		var slashes = http.concat("//");
		var host = slashes.concat(window.location.hostname);
		return( host );
	}
};

(function($){
	$.widget( 'ui.kRegistration', {
		options: {
			dialog: '#dlg-registration-container',
			dialogContent: '#dlg-content',
			dialogLoader: '.dlg-footer-loader',
			
			dialogFooter: '#dlg-footer',
			dialogBtnClose: '.dlg-btn-close',
			dialogBtnRegister: '.dlg-btn-register',
			
			form: {
				protectImage: 'img.protect_image',
				reload: '.btn-reload',
				
				firstname: 'input[name="firstname"]',
				lastname: 'input[name="lastname"]',
				email: 'input[name="email"]',
				country: 'select[name="country"]',
				phone: 'input[name="phone"]',
				password: 'input[name="password"]',
				is_prof: 'input[name="is_professional"]',
				send_news: 'input[name="send_news"]',
				protect_code: 'input[name="protect_code"]'
			}
		},
		
		_create: function() {
			var self=this; var o=this.options;
			
			Events.subscribe( '/Registration/Show', function(info) {
				self.info = info;
				self._showDlg();
			});
			
			this._initDialog();
		},
	
		_initDialog: function() {
			var self = this, o = this.options;
			
			$(o.dialog).dialog({
				autoOpen: false,
				resizable: true,
				minHeight: 500,
				minWidth: 380,
				modal: true,
				open: function(event, ui){
					self._applyDialog();
				}
			});
			
			$(o.dialogBtnClose, o.dialog).click(function(){
				self._closeDlg();
			});
			
			$(o.dialogBtnRegister, o.dialog).click(function(){
				self._register();
			});
		},
		
		_showDlg: function(){
			var o=this.options;
			$(o.dialog).dialog('open');
		},
		
		_closeDlg: function(){
			var o=this.options;
			$(o.dialog).dialog("close");
		},
		
		_applyDialog: function() {
			var self=this, o=this.options;
			var tplPath = Site.ViewsPath;
			require(['text!'+ tplPath +'/registration-form.html?13'], function(template){
				self.formTemplate = template;
				self._applyRegisterForm();
			});
			
			self._applyRegisterForm();
		},
		
		_applyRegisterForm: function() {
			var self=this, o=this.options;
			if( this.formTemplate ) {
				$(o.dialogContent,o.dialog).html( this.formTemplate );
				var img = $(o.form.protectImage,o.dialog).first();
				img.attr( 'src', '/base/get_register_protect_image_new?'+ Tools.getTimestamp() );
				var reload = $(o.form.reload,o.dialog).first();
				reload.click(function(){
					img.attr( 'src', '/app/views/images/loader1.gif' );
					img.attr( 'src', '/base/get_register_protect_image_new?'+ Tools.getTimestamp() );
				});

				$.mask.definitions['9'] = '';
				$.mask.definitions['d'] = '[0-9]';
				
				var country = $(o.form.country,o.dialog).first();
				country.empty();
				for( var i=0; i<Site.Countries.items.length; i++ ) {
					var ctr = Site.Countries.items[i];
					country.append( $('<option value="'+ ctr.id +'" data-code="'+ ctr.phone_code +'">'+ ctr.name +'</option>') );
				}
				country.change(function(){
					var optionSelected = $("option:selected", this);
					var code = optionSelected.attr('data-code');
					$(o.form.phone,o.dialog).val('').mask( '+'+ code +' ddddddddd?d');
					$(o.form.phone,o.dialog).focus();
				});
				country.trigger( 'change' );
				$(o.form.firstname,o.dialog).focus();

				$(o.form.email,o.dialog).keyup(function(evnt){
					var e = $(this);
					e.val( Tools.formatEmail(e.val()) );
				});
			}
		},
		
		_register: function() {
			var self=this, o=this.options;
			
			if( this._checkForm() ) {
				this._showDialogLoader();
				var fv = this._getFormItems();
				$.ajax({
					url: '/base/json_register_client',
					data: {
						firstname: fv['firstname'].val,
						lastname: fv['lastname'].val,
						password: fv['password'].val,
						protect_code: fv['protect_code'].val,
						country: fv['country'].val,
						phone: fv['phone'].val,
						email: fv['email'].val,
						send_news: fv['send_news'].val,
						is_prof: fv['is_prof'].val
					},
					dataType: 'json',
					type: 'get',
					success: function(data) {
						var r = data.result;
						if( r == 1 ) {
							self._closeDlg();
							Events.publish( '/RegisterActivation/Show' );
						}
						else if( r == -1 ) Alert.error('Ошибка параметров.<br/>Внимательно проверьте поля формы.');
						else if( r == -2 ) Alert.error('Ошибка проверочного кода.<br/>Вы можете обновить код, нажав кнопку справа от картинки с кодом.');
						else if( r == -3 ) Alert.error('Пользователь с таким телефоном или(и) ел. адресом уже существует.<br/>Вы можете воспользоваться формой восстановления пароля: закройте данную форму и откройте форму "Вход".');
						else if( r == -4 ) Alert.error('Ошибка создания пользователя. Повторите попытку или свяжитесь с службой поддержки');
						self._hideDialogLoader();
					}
				});
			}
		},
		
		_checkForm: function() {
			var self=this, o=this.options;
			var retval = true;
			
			var fv = this._getFormItems();
			
			if( !fv['firstname'].val ) {
				fv['firstname'].obj.addClass('inp-error');
				retval = false;
			}
			else fv['firstname'].obj.removeClass('inp-error');
			
			if( !fv['lastname'].val ) {
				fv['lastname'].obj.addClass('inp-error');
				retval = false;
			}
			else fv['lastname'].obj.removeClass('inp-error');
			
			if( !fv['password'].val ) {
				fv['password'].obj.addClass('inp-error');
				retval = false;
			}
			else fv['password'].obj.removeClass('inp-error');
			
			if( !fv['protect_code'].val ) {
				fv['protect_code'].obj.addClass('inp-error');
				retval = false;
			}
			else fv['protect_code'].obj.removeClass('inp-error');
			
			if( !fv['phone'].val ) {
				fv['phone'].obj.addClass('inp-error');
				retval = false;
			}
			else fv['phone'].obj.removeClass('inp-error');
			
			/*if( !fv['email'].val && !fv['phone'].val ) {
				fv['email'].obj.addClass('inp-error');
				fv['phone'].obj.addClass('inp-error');
				retval = false;
			}
			else {
				if( (fv['email'].val && !fv['phone'].val) || (!fv['email'].val && fv['phone'].val) ) {
					fv['email'].obj.removeClass('inp-error');
					fv['phone'].obj.removeClass('inp-error');
				}
			}*/
			
			return( retval );
		},
		
		_getFormItems: function() {
			var self=this, o=this.options;
			var retval = [];
			
			var firstname = $(o.form.firstname,o.dialog);
			var firstnameVal = $.trim( firstname.val() );
			retval['firstname'] = {
				obj: firstname,
				val: firstnameVal
			};
			
			var lastname = $(o.form.lastname,o.dialog);
			var lastnameVal = $.trim( lastname.val() );
			retval['lastname'] = {
				obj: lastname,
				val: lastnameVal
			};
			
			var country = $(o.form.country,o.dialog);
			var countryVal = $.trim( country.val() );
			retval['country'] = {
				obj: country,
				val: countryVal
			};
			
			var email = $(o.form.email,o.dialog);
			var emailVal = $.trim( email.val() );
			retval['email'] = {
				obj: email,
				val: Tools.formatEmail(emailVal)
			};
			
			var phone = $(o.form.phone,o.dialog);
			var phoneVal = $.trim( phone.val() );
			retval['phone'] = {
				obj: phone,
				val: Tools.formatPhone(phoneVal)
			};
			
			var password = $(o.form.password,o.dialog);
			var passwordVal = $.trim( password.val() );
			retval['password'] = {
				obj: password,
				val: passwordVal
			};
			
			var protect_code = $(o.form.protect_code,o.dialog);
			var protect_codeVal = $.trim( protect_code.val() );
			retval['protect_code'] = {
				obj: protect_code,
				val: protect_codeVal
			};
			
			var send_news = $(o.form.send_news,o.dialog).filter(':checked');
			var send_newsVal = $.trim( send_news.val() );
			retval['send_news'] = {
				obj: send_news,
				val: send_newsVal
			};

            var is_prof = $(o.form.is_prof,o.dialog);
            var is_profVal = is_prof.is(":checked");
            retval['is_prof'] = {
                obj: is_prof,
                val: is_profVal
			};

			return( retval );
		},
		
		_showDialogLoader: function() {
			var self = this, o = this.options;
			$(o.dialogLoader, o.dialog).show();
		},
		_hideDialogLoader: function() {
			var self = this, o = this.options;
			$(o.dialogLoader, o.dialog).hide();
		}
	});
})(jQuery);

(function($){
	$.widget( 'ui.kLogin', {
		options: {
			dialog: '#dlg-login-container',
			dialogContent: '#dlg-content',
			dialogLoader: '.dlg-footer-loader',
			
			dialogFooter: '#dlg-footer',
			dialogBtnClose: '.dlg-btn-close',
			dialogBtnRegistration: '.dlg-btn-registration',
			dialogBtnLogin: '.dlg-btn-login',
			dialogLnkReminder: '.lnk-reminder',
			
			form: {
				tabs: '.tabs',
				email: 'input[name="email"]',
				phone: 'input[name="phone"]',
				country: 'select[name="country"]',
				emailPassword: 'input[name="email_password"]',
				phonePassword: 'input[name="phone_password"]'
			}
		},
		
		info: {
			gotourl: null
		},
		
		_create: function() {
			var self=this; var o=this.options;
			
			Events.subscribe( '/Login/Show', function(info) {
				self.info = info;
				self._showDlg();
			});
			
			this._initDialog();
		},
	
		_initDialog: function() {
			var self = this, o = this.options;
			
			$(o.dialog).dialog({
				autoOpen: false,
				resizable: true,
				minHeight: 240,
				minWidth: 380,
				modal: true,
				open: function(event, ui){
					self._applyDialog();
				}
			});
			
			$(o.dialogBtnClose, o.dialog).click(function(){
				self._closeDlg();
			});
			
			$(o.dialogBtnLogin, o.dialog).click(function(){
				self._login();
			});
			
			$(o.dialogBtnRegistration, o.dialog).click(function(){
				self._closeDlg();
				Events.publish('/Registration/Show');
			});
		},
		
		_showDlg: function(){
			var o=this.options;
			$(o.dialog).dialog('open');
			$(o.form.phone,o.dialog).focus();
		},
		
		_closeDlg: function(){
			var o=this.options;
			$(o.dialog).dialog("close");
		},
		
		_login: function() {
			var self=this, o=this.options;
			
			var email = $.trim( $(o.form.email, o.dialog).val() );
			var phone = $.trim( $(o.form.phone, o.dialog).val() );
			var country = $.trim( $(o.form.country, o.dialog).val() );
			var emailPassword = $.trim( $(o.form.emailPassword, o.dialog).val() );
			var phonePassword = $.trim( $(o.form.phonePassword, o.dialog).val() );
			
			if( (email!='' && emailPassword!='') || (phone!='' && phonePassword!='') ) {
				this._showDialogLoader();
				$.ajax({
					url: '/base/json_login',
					data: {
						'email': email,
						'email_password': emailPassword,
						'country': country,
						'phone': phone,
						'phone_password': phonePassword
					},
					dataType: 'json',
					type: 'get',
					success: function(data) {
						var r = data.result;
						if( r == 1 ) {
							if( self.info && self.info.gotourl ) document.location = self.info.gotourl;
							else document.location.reload(false);
						}
						else if( r == -1 ) Alert.error('Ошибка параметров');
						else if( r == -2 )  Alert.message('Учетная запись уже авторизирована, обновите страницу');
						else if( r == -3 ) Alert.error('Учетная запись не активна, обратитесь в службу поддержки');
						else if( r == -4 ) Alert.error('Пользователь не найден, введите верные параметры формы <br/>или воспользуйтесь процедурой восстановления пароля');
						self._hideDialogLoader();
					}
				});
			}
			else {
				Alert.error('Заполните все поля.');
			}
		},
		
		_applyDialog: function() {
			var self=this, o=this.options;
			var tplPath = Site.ViewsPath;
			require(['text!'+ tplPath +'/login-form.html?16'], function(template){
				self.formTemplate = template;
				self._applyForm();
			});
			
			self._applyForm();
		},
		
		_applyForm: function() {
			var self=this, o=this.options;
			if( this.formTemplate ) {
				$(o.dialogContent,o.dialog).html( this.formTemplate );
				$(o.form.email, o.dialog).focus();
				
				$(o.form.tabs, o.dialog).tabs({
					activate: function( event, ui ) {
						ui.newPanel.find('input').first().focus();
					}
				});
				$(o.form.email,o.dialog).keyup(function(evnt){
					var e = $(this);
					e.val( Tools.formatEmail(e.val()) );
				});

				$.mask.definitions['9'] = '';
				$.mask.definitions['d'] = '[0-9]';

				var country = $(o.form.country,o.dialog).first();
				country.empty();
				for( var i=0; i<Site.Countries.items.length; i++ ) {
					var ctr = Site.Countries.items[i];
					country.append( $('<option value="'+ ctr.id +'" data-code="'+ ctr.phone_code +'">'+ ctr.name +'</option>') );
				}
				country.change(function(){
					var optionSelected = $("option:selected", this);
					var code = optionSelected.attr('data-code');
					$(o.form.phone,o.dialog).val('').mask( code +' ddddddddd?d');
					$(o.form.phone,o.dialog).focus();
				});
				country.trigger( 'change' );
				
				$(o.dialogLnkReminder, o.dialog).click(function(){
					self._closeDlg();
					Events.publish('/Reminder/Show');
				});
				
				$(o.dialogBtnRegistration, o.dialog).click(function(){
					self._closeDlg();
					Events.publish('/Registration/Show');
				});
				
				$(o.form.phone,o.dialog).focus();
			}
		},
		
		_showDialogLoader: function() {
			var self = this, o = this.options;
			$(o.dialogLoader, o.dialog).show();
		},
		_hideDialogLoader: function() {
			var self = this, o = this.options;
			$(o.dialogLoader, o.dialog).hide();
		}
	});
})(jQuery);

(function($){
	$.widget( 'ui.kReminder', {
		options: {
			dialog: '#dlg-reminder-container',
			dialogContent: '#dlg-content',
			dialogLoader: '.dlg-footer-loader',
			
			dialogFooter: '#dlg-footer',
			dialogBtnClose: '.dlg-btn-close',
			dialogBtnSend: '.dlg-btn-send',
			
			form: {
				tabs: '.tabs',
				email: 'input[name="email"]',
				country: 'select[name="country"]',
				phone: 'input[name="phone"]'
			}
		},
		
		_create: function() {
			var self=this; var o=this.options;
			
			Events.subscribe( '/Reminder/Show', function(info) {
				self.info = info;
				self._showDlg();
			});
			
			this._initDialog();
		},
	
		_initDialog: function() {
			var self = this, o = this.options;
			
			$(o.dialog).dialog({
				autoOpen: false,
				resizable: true,
				minHeight: 160,
				minWidth: 380,
				modal: true,
				open: function(event, ui){
					self._applyDialog();
				}
			});
			
			$(o.dialogBtnClose, o.dialog).click(function(){
				self._closeDlg();
			});
			
			$(o.dialogBtnSend, o.dialog).click(function(){
				self._send();
			});
		},
		
		_showDlg: function(){
			var o=this.options;
			$(o.dialog).dialog('open');
			$(o.form.phone,o.dialog).focus();
		},
		
		_closeDlg: function(){
			var o=this.options;
			$(o.dialog).dialog("close");
		},
		
		_send: function() {
			var self=this, o=this.options;
			
			var email = $.trim( $(o.form.email, o.dialog).val() );
			var phone = $.trim( $(o.form.phone, o.dialog).val() );
			var country = $.trim( $(o.form.country, o.dialog).val() );
			
			if( email!='' || phone!='' ) {
				this._showDialogLoader();
				$.ajax({
					url: '/base/json_password_reminder',
					data: {
						'email': email,
						'country': country,
						'phone': phone
					},
					dataType: 'json',
					type: 'get',
					success: function(data) {
						self._hideDialogLoader();
						
						var r = data.result;
						if( r == 1 ) {
							Alert.message('Пароль был выслан.');
							self._closeDlg();
							Events.publish( '/Login/Show' );
						}
						else if( r == -1 ) Alert.error('Ошибка параметра');
						else if( r == -2 )  Alert.message('Учетная запись не найдена');
					}
				});
			}
			else {
				Alert.error('Заполните все поля.');
			}
		},
		
		_applyDialog: function() {
			var self=this, o=this.options;
			var tplPath = Site.ViewsPath;
			require(['text!'+ tplPath +'/reminder-form.html?5'], function(template){
				self.formTemplate = template;
				self._applyForm();
			});
			
			self._applyForm();
		},
		
		_applyForm: function() {
			var self=this, o=this.options;
			if( this.formTemplate ) {
				$(o.dialogContent,o.dialog).html( this.formTemplate );
				
				$(o.form.tabs, o.dialog).tabs({
					activate: function( event, ui ) {
						ui.newPanel.find('input').first().focus();
					}
				});
				$(o.form.email,o.dialog).keyup(function(evnt){
					var e = $(this);
					e.val( Tools.formatEmail(e.val()) );
				});

				$.mask.definitions['9'] = '';
				$.mask.definitions['d'] = '[0-9]';
				
				var country = $(o.form.country,o.dialog).first();
				country.empty();
				for( var i=0; i<Site.Countries.items.length; i++ ) {
					var ctr = Site.Countries.items[i];
					country.append( $('<option value="'+ ctr.id +'" data-code="'+ ctr.phone_code +'">'+ ctr.name +'</option>') );
				}
				country.change(function(){
					var optionSelected = $("option:selected", this);
					var code = optionSelected.attr('data-code');
					$(o.form.phone,o.dialog).val('').mask( code +' ddddddddd?d');
					$(o.form.phone,o.dialog).focus();
				});
				country.trigger( 'change' );
			}
		},
		
		_showDialogLoader: function() {
			var self = this, o = this.options;
			$(o.dialogLoader, o.dialog).show();
		},
		_hideDialogLoader: function() {
			var self = this, o = this.options;
			$(o.dialogLoader, o.dialog).hide();
		}
	});
})(jQuery);

(function($){
	$.widget( 'ui.kRegisterActivation', {
		options: {
			dialog: '#dlg-registeractivation-container',
			dialogContent: '#dlg-content',
			dialogLoader: '.dlg-footer-loader',
			
			dialogFooter: '#dlg-footer',
			dialogBtnClose: '.dlg-btn-close',
			dialogBtnSend: '.dlg-btn-send',
			
			form: {
				code: 'input[name="code"]'
			}
		},
		
		_create: function() {
			var self=this; var o=this.options;
			
			Events.subscribe( '/RegisterActivation/Show', function(info) {
				self.info = info;
				self._showDlg();
			});
			
			this._initDialog();
		},
	
		_initDialog: function() {
			var self = this, o = this.options;
			
			$(o.dialog).dialog({
				autoOpen: false,
				resizable: true,
				minHeight: 160,
				minWidth: 380,
				modal: true,
				open: function(event, ui){
					self._applyDialog();
				}
			});
			
			$(o.dialogBtnClose, o.dialog).click(function(){
				self._closeDlg();
			});
			
			$(o.dialogBtnSend, o.dialog).click(function(){
				self._send();
			});
		},
		
		_showDlg: function(){
			var o=this.options;
			$(o.dialog).dialog('open');
		},
		
		_closeDlg: function(){
			var o=this.options;
			$(o.dialog).dialog("close");
		},
		
		_send: function() {
			var self=this, o=this.options;
			
			var code = $.trim( $(o.form.code, o.dialog).val() );
			
			if( code!='' ) {
				this._showDialogLoader();
				$.ajax({
					url: '/base/json_activate_client',
					data: {
						'code': code
					},
					dataType: 'json',
					type: 'get',
					success: function(data) {
						self._hideDialogLoader();
						
						var r = data.result;
						if( r == 1 ) {
							Alert.message('Учетная запись была активирована.<br/>Благодарим за доверие!');
							self._closeDlg();
							Events.publish( '/Login/Show' );
						}
						else if( r == -1 ) Alert.error('Ошибка параметра');
						else if( r == -2 )  Alert.message('Код активации неверен.<br/>Свяжитесь со службой проверки.');
						else Alert.message('Ошибка не определена.<br/>Свяжитесь со службой проверки.');
					}
				});
			}
			else {
				Alert.error('Заполните все поля.');
			}
		},
		
		_applyDialog: function() {
			var self=this, o=this.options;
			var tplPath = Site.ViewsPath;
			require(['text!'+ tplPath +'/registeractivation-form.html?1'], function(template){
				self.formTemplate = template;
				self._applyForm();
			});
			
			self._applyForm();
		},
		
		_applyForm: function() {
			var self=this, o=this.options;
			if( this.formTemplate ) {
				$(o.dialogContent,o.dialog).html( this.formTemplate );
				$(o.form.code, o.dialog).focus();
			}
		},
		
		_showDialogLoader: function() {
			var self = this, o = this.options;
			$(o.dialogLoader, o.dialog).show();
		},
		_hideDialogLoader: function() {
			var self = this, o = this.options;
			$(o.dialogLoader, o.dialog).hide();
		}
	});
})(jQuery);

(function($){
	$.widget( 'ui.kAction', {
		options: {
			dialog: '#dlg-action-container',
			dialogContent: '#dlg-content',
			dialogLoader: '.dlg-footer-loader',
			
			dialogFooter: '#dlg-footer',
			dialogBtnClose: '.dlg-btn-close',
			dialogBtnSend: '.dlg-btn-send',
			
			actions: {
				askPhone: {
					template: 'action-askphone.html?5',
					url: '/base/json_client_update_phone',
					cookie: 'action-askphone',
					cookieTrigger: 1, // 0-always; 1-one time per day
					form: {
						country: 'select[name="country"]',
						phone: 'input[name="phone"]'
					},
					afterSendMessage: 'Мобильный телефон был сохранен.'
				}
			}
		},
		
		info: {
			action: 0
		},
		
		_getAction: function() {
			var retval = null;
			
			if( this.info && this.info.action ) {
				if( this.info.action == 1 ) {
					retval = this.options.actions.askPhone;
				}
			}
			
			return( retval );
		},
		
		_getData2Send: function() {
			var self=this; var o=this.options;
			var retval = null;
				
			var action = this._getAction();
			if( action ) {
				for( var key in action.form ) {
					if( key === 'length' || !action.form.hasOwnProperty(key) ) continue;
					var val = action.form[key];
					if( !retval ) retval = {};
					retval[key] = $.trim( $(val, o.dialog).val() );
				}
			}
			
			return( retval );
		},
		
		_checkData2Send: function( data ) {
			var retval = true;
			
			if( data ) {
				for( var key in data ) {
					if( !data[key] ) {
						retval = false;
						break;
					}
				}
			}
			
			return( retval );
		},
		
		_create: function() {
			var self=this; var o=this.options;
			
			Events.subscribe( '/Action/Show', function(info) {
				if( info ) {
					self.info = info;
					var action = self._getAction();
					if( action.cookieTrigger == 0 ) {
						self._showDlg();
					}
					else if( action.cookieTrigger == 1 ) {
						var c = $j.cookie(action.cookie);
						if( c != 1 ) {
							self._showDlg();
							var today = new Date();
							$j.cookie( action.cookie, 1, {
								expires: new Date( today.getFullYear(), today.getMonth(), today.getDate(), 23, 59, 59 ),
								path: '/'
							});
						}
					}
				}
				else Alert.error( 'Ошибка параметров' );
			});
			
			this._initDialog();
		},
	
		_initDialog: function() {
			var self = this, o = this.options;
			
			$(o.dialog).dialog({
				autoOpen: false,
				resizable: true,
				minHeight: 160,
				minWidth: 380,
				modal: true,
				open: function(event, ui){
					self._applyDialog();
				}
			});
			
			$(o.dialogBtnClose, o.dialog).click(function(){
				self._closeDlg();
			});
			
			$(o.dialogBtnSend, o.dialog).click(function(){
				self._send();
			});
		},
		
		_showDlg: function(){
			var o=this.options;
			$(o.dialog).dialog('open');
		},
		
		_closeDlg: function(){
			var o=this.options;
			$(o.dialog).dialog("close");
		},
		
		_send: function() {
			var self=this, o=this.options;
			
			var action = this._getAction();
			var data = this._getData2Send();
			
			if( data && this._checkData2Send(data) ) {
				this._showDialogLoader();
				$.ajax({
					url: action.url,
					data: data,
					dataType: 'json',
					type: 'get',
					success: function(data) {
						self._hideDialogLoader();
						
						var r = data.result;
						if( r == 1 ) {
							Alert.message(action.afterSendMessage);
							self._closeDlg();
						}
					}
				});
			}
			else {
				Alert.error('Заполните поля.');
			}
		},
		
		_applyDialog: function() {
			var self=this, o=this.options;
			var tplPath = Site.ViewsPath;
			var action = this._getAction();
			require(['text!'+ tplPath +'/'+ action.template +'?2'], function(template){
				self.formTemplate = template;
				self._applyForm();
			});
			
			self._applyForm();
		},
		
		_applyForm: function() {
			var self=this, o=this.options;
			if( this.formTemplate ) {
				$(o.dialogContent,o.dialog).html( this.formTemplate );
				
				var action = this._getAction();

				$.mask.definitions['9'] = '';
				$.mask.definitions['d'] = '[0-9]';
				if( action.form.phone ) {
					var f = action.form;
					var country = $(f.country,o.dialog).first();
					country.empty();
					for( var i=0; i<Site.Countries.items.length; i++ ) {
						var ctr = Site.Countries.items[i];
						country.append( $('<option value="'+ ctr.id +'" data-code="'+ ctr.phone_code +'">'+ ctr.name +'</option>') );
					}
					country.change(function(){
						var optionSelected = $("option:selected", this);
						var code = optionSelected.attr('data-code');
						$(f.phone,o.dialog).val('').mask( code +' ddddddddd?d');
						$(f.phone,o.dialog).focus();
					});
					country.trigger( 'change' );
				}
			}
		},
		
		_showDialogLoader: function() {
			var self = this, o = this.options;
			$(o.dialogLoader, o.dialog).show();
		},
		_hideDialogLoader: function() {
			var self = this, o = this.options;
			$(o.dialogLoader, o.dialog).hide();
		}
	});
})(jQuery);

(function($){
	$.widget( 'ui.kAnounce', {
		options: {
			dialog: '#dlg-anounce-container',
			dialogContent: '#dlg-content',
			dialogLoader: '.dlg-footer-loader',
			
			dialogFooter: '#dlg-footer',
			dialogBtnClose: '.dlg-btn-close'
		},
		
		info: {
			message: '',
			cookie: 'anounce',
			cookieTrigger: 1, // 0-each time; 1-once
		},
		
		_create: function() {
			var self=this; var o=this.options;
			
			Events.subscribe( '/Anounce/Show', function(info) {
				self.info = info;
				if( self.info && self.info.message ) {
					var cookie = self.info.cookie;
					var trigger = self.info.cookieTrigger || 0;
					if( trigger == 0 ) {
						self._showDlg();
					}
					else if( cookie && trigger == 1 ) {
						var c = $j.cookie(cookie);
						
						if( c != 1 ) {
							self._showDlg();
							
							var today = new Date();
							$j.cookie( cookie, 1, {
								expires: new Date( today.getFullYear()+1, today.getMonth(), today.getDate(), 23, 59, 59 ),
								path: '/'
							})
						}
					}
				}
			});
			
			this._initDialog();
		},
	
		_initDialog: function() {
			var self = this, o = this.options;
			
			$(o.dialog).dialog({
				autoOpen: false,
				resizable: true,
				minHeight: 160,
				minWidth: 380,
				modal: true,
				open: function(event, ui){
					self._applyDialog();
				}
			});
			
			$(o.dialogBtnClose, o.dialog).click(function(){
				self._closeDlg();
			});
		},
		
		_showDlg: function(){
			var o=this.options;
			$(o.dialog).dialog('open');
		},
		
		_closeDlg: function(){
			var o=this.options;
			$(o.dialog).dialog("close");
		},
		
		_applyDialog: function() {
			var self=this, o=this.options;
			
			if( this.info && this.info.message ) {
				var c = $(o.dialogContent, o.dialog);
				var m = $('<p>').html( this.info.message );
				c.append( m );
			}
		},
		
		_showDialogLoader: function() {
			var self = this, o = this.options;
			$(o.dialogLoader, o.dialog).show();
		},
		_hideDialogLoader: function() {
			var self = this, o = this.options;
			$(o.dialogLoader, o.dialog).hide();
		}
	});
})(jQuery);

(function($){
	$.widget( 'ui.kPlaceholder', {
		options: {
			elementAttr: 'rel',
			placeholderClass: 'placeholder'
		},
		
		_create: function() {
			var self=this; var o=this.options;
			
			if( this.element.is('select') ) {
				this.element.change(function(){
					self._initSelect( this );
				});
				self._initSelect( this.element );
			}
			else {
				this._initInput( this.element );
				
				this.element.focusin(function(){
					self._onFocusIn(this);
				});
				
				this.element.focusout(function(){
					self._onFocusOut(this);
				});
			}
		},
		
		_initInput: function( i ) {
			var o = this.options;
			i = $(i);
			if( $.trim(i.val()) == $.trim(i.attr(o.elementAttr)) || $.trim(i.val()) == '' ) {
			  i.val( i.attr(o.elementAttr) );
			  i.addClass( o.placeholderClass );
			}
			else {
			  i.removeClass( o.placeholderClass );
			}
		},
		
		_initSelect: function( sel ) {
			var o = this.options;
			sel = $(sel);
			if( sel.val() ) sel.removeClass( o.placeholderClass );
			else sel.addClass( o.placeholderClass );
		},
		
		_onFocusIn: function( i ) {
			var o = this.options;
			i = $(i);
			if( $.trim(i.val()) == $.trim(i.attr(o.elementAttr)) ) {
				i.val('');
				i.removeClass( o.placeholderClass );
			}
		  },
		  
		_onFocusOut: function( i ) {
			var o = this.options;
			i = $(i);
			if( $.trim(i.val()) == '' ) {
				i.val( i.attr(o.elementAttr) );
				i.addClass( o.placeholderClass );
			}
		}
  });
})(jQuery);

(function($){
	$.widget( 'ui.kBannerBox', {
		options: {
			filter: 1,
			containerClass: 'banner-box-container',
			contentClass: 'banner-box-content',
			footerClass: 'banner-box-footer'
		},
		
		items: [],
		
		_create: function() {
			var self=this; var o=this.options;
			
			this._get(function(){
				self._apply();
			});
		},
		
		_apply: function() {
			var self=this; var o=this.options; var e=this.element;
			
			e.children().remove();
			if( this.items && this.items.length>0 ) {
				var container = $('<div>').addClass( o.containerClass );
				var content = $('<div>').addClass( o.contentClass );
				var footer = $('<div>').addClass( o.footerClass );
				
				for( var i=0; i<this.items.length; i++ ) {
					var im = this.items[i];
					
					/* box */
					var imgBox = $('<div>').attr({
						idx: i
					});
					var img = $('<img>').attr({
						src: im.image
					});
					if( im.url ) {
						var l = $('<a>').attr({
							href: im.url
						});
						l.append( img );
						img = l;
					}
					imgBox.append( img );
					content.append( imgBox );
					/* box */
					
					/* footer */
					var linkBox = $('<div>').attr({
						idx: i
					});
					var link = $('<a>');
					if( im.url ) link.attr({href: im.url});
					link.html( im.name );
					linkBox.append( link );
					footer.append( linkBox );
					/* footer */
					
					if( i>0 ) {
						imgBox.hide();
						linkBox.hide();
					}
				}
				/*content.cycle({
					fx: 'fade',
					sync:  false,
					timeout: 5000
				});
				footer.cycle({
					fx: 'fade',
					sync:  false,
					timeout: 5000
				});*/
				
				container.append( content );
				container.append( footer );
			}
			e.append( container );
			
			if( this.items.length > 1 ) {
				setTimeout( function(){
					self._runAnimation();
				}, 5000 );
			}
		},
		
		_runAnimation: function() {
			var self=this, o=this.options;
			
			var c = $('.'+o.contentClass,this.element).first();
			var cur = c.children('div:visible').first();
			var next = ( cur.length && cur.next().length )? cur.next() : c.children().first();
			cur.fadeOut( 400, function(){
				next.fadeIn();
			});
			
			var fc = $('.'+o.footerClass,this.element).first();
			var fcur = fc.children('div:visible').first();
			var fnext = ( fcur.length && fcur.next().length )? fcur.next() : fc.children().first();
			fcur.fadeOut( 400, function(){
				fnext.fadeIn();
			});
			
			setTimeout( function(){
				self._runAnimation();
			}, 5000 );
		},
		
		_get: function( cb ) {
			var self=this, o=this.options;
			
			if( Site ) {
				if( this.options.filter == 1 && Site.BannersServices && Site.BannersServices.items )  {
					self.items = Site.BannersServices.items;
				}
				else if( this.options.filter == 2 && Site.BannersActions && Site.BannersActions.items )  {
					self.items = Site.BannersActions.items;
				}
			}
			
			if( typeof(cb) == 'function' ) cb();
		}
	});
})(jQuery);

(function($){
	$.widget( 'ui.kNewProductsBox', {
		options: {
			containerClass: 'banner-box-container',
			contentClass: 'banner-box-content',
			footerClass: 'banner-box-footer',
			PriceClass: 'banne-box-price'
		},
		
		items: [],
		
		_create: function() {
			var self=this; var o=this.options;
			
			this._get(function(){
				self._apply();
			});
		},
		
		_apply: function() {
			var self=this; var o=this.options; var e=this.element;
			
			e.children().remove();
			if( this.items && this.items.length>0 ) {
				var container = $('<div>').addClass( o.containerClass );
				var content = $('<div>').addClass( o.contentClass );
				var footer = $('<div>').addClass( o.footerClass );
				
				for( var i=0; i<this.items.length; i++ ) {
					var im = this.items[i];
					var url = '/shop/product/'+ im.id;
					
					/* box */
					var imgBox = $('<div>').attr({
						idx: i
					}).addClass('new-product-icon');
					var img = $('<img>').attr({
						//src: im.image.path+im.image.tiny
						src: im.image.tiny
					});
					if( url ) {
						var l = $('<a>').attr({
							href: url
						});
						l.append( img );
						img = l;
					}
					imgBox.append( img );
					
					var price = $('<div>').addClass( o.PriceClass );
					price.html( im.price + Site.Currency.Symbol );
					imgBox.append( price );
					
					content.append( imgBox );
					/* box */
					
					/* footer */
					var linkBox = $('<div>').attr({
						idx: i
					});
					var link = $('<a>');
					if( url ) link.attr({href: url});
					link.html( im.name );
					linkBox.append( link );
					footer.append( linkBox );
					/* footer */
					
					if( i>0 ) {
						imgBox.hide();
						linkBox.hide();
					}
				}
				
				container.append( content );
				container.append( footer );
			}
			e.append( container );
			
			if( this.items.length > 1 ) {
				setTimeout( function(){
					self._runAnimation();
				}, 5000 );
			}
		},
		
		_runAnimation: function() {
			var self=this, o=this.options;
			
			var c = $('.'+o.contentClass,this.element).first();
			var cur = c.children('div:visible').first();
			var next = ( cur.length && cur.next().length )? cur.next() : c.children().first();
			cur.fadeOut( 400, function(){
				next.fadeIn();
			});
			
			var fc = $('.'+o.footerClass,this.element).first();
			var fcur = fc.children('div:visible').first();
			var fnext = ( fcur.length && fcur.next().length )? fcur.next() : fc.children().first();
			fcur.fadeOut( 400, function(){
				fnext.fadeIn();
			});
			
			setTimeout( function(){
				self._runAnimation();
			}, 5000 );
		},
		
		_get: function( cb ) {
			var self=this, o=this.options;
			
			if( Site && Site.ProductsNew && Site.ProductsNew.items )  {
				self.items = Site.ProductsNew.items;
			}
			if( typeof(cb)=='function' ) cb();
		}
	});
})(jQuery);

(function($){
	$.widget( 'ui.kActionProductsBox', {
		options: {
			containerClass: 'banner-box-container',
			contentClass: 'banner-box-content',
			footerClass: 'banner-box-footer',
			PriceClass: 'banne-box-price'
		},
		
		items: [],
		
		_create: function() {
			var self=this; var o=this.options;
			
			this._get(function(){
				self._apply();
			});
		},
		
		_apply: function() {
			var self=this; var o=this.options; var e=this.element;
			
			e.children().remove();
			if( this.items && this.items.length>0 ) {
				var container = $('<div>').addClass( o.containerClass );
				var content = $('<div>').addClass( o.contentClass );
				var footer = $('<div>').addClass( o.footerClass );
				
				for( var i=0; i<this.items.length; i++ ) {
					var im = this.items[i];
					var url = '/shop/product/'+ im.id;
					
					/* box */
					var imgBox = $('<div>').attr({
						idx: i
					}).addClass('action-product-icon');
					var img = $('<img>').attr({
						//src: im.image.path+im.image.tiny
						src: im.image.tiny
					});
					if( url ) {
						var l = $('<a>').attr({
							href: url
						});
						l.append( img );
						img = l;
					}
					imgBox.append( img );
					
					var price = $('<div>').addClass( o.PriceClass );
					price.html( im.price + Site.Currency.Symbol );
					imgBox.append( price );
					
					content.append( imgBox );
					/* box */
					
					/* footer */
					var linkBox = $('<div>').attr({
						idx: i
					});
					var link = $('<a>');
					if( url ) link.attr({href: url});
					link.html( im.name );
					linkBox.append( link );
					footer.append( linkBox );
					/* footer */
					
					if( i>0 ) {
						imgBox.hide();
						linkBox.hide();
					}
				}
				
				container.append( content );
				container.append( footer );
			}
			e.append( container );
			
			if( this.items.length > 1 ) {
				setTimeout( function(){
					self._runAnimation();
				}, 5000 );
			}
		},
		
		_runAnimation: function() {
			var self=this, o=this.options;
			
			var c = $('.'+o.contentClass,this.element).first();
			var cur = c.children('div:visible').first();
			var next = ( cur.length && cur.next().length )? cur.next() : c.children().first();
			cur.fadeOut( 400, function(){
				next.fadeIn();
			});
			
			var fc = $('.'+o.footerClass,this.element).first();
			var fcur = fc.children('div:visible').first();
			var fnext = ( fcur.length && fcur.next().length )? fcur.next() : fc.children().first();
			fcur.fadeOut( 400, function(){
				fnext.fadeIn();
			});
			
			setTimeout( function(){
				self._runAnimation();
			}, 5000 );
		},
		
		_get: function( cb ) {
			var self=this, o=this.options;
			
			if( Site && Site.ProductsAction && Site.ProductsAction.items )  {
				self.items = Site.ProductsAction.items;
			}
			if( typeof(cb)=='function' ) cb();
		}
	});
})(jQuery);

/* hislide initialization */
hs.graphicsDir = '/js/highslide/graphics/';
hs.align = 'center';
hs.transitions = ['expand', 'crossfade'];
hs.outlineType = 'rounded-white';
hs.showCredits = false;
hs.fadeInOut = true;
hs.wrapperClassName = 'controls-in-heading';
hs.captionOverlay.position = 'above';
hs.captionText = 'увеличенное изображение';
// Add the controlbar
if (hs.addSlideshow) hs.addSlideshow({
	//slideshowGroup: 'group1',
	interval: 5000,
	repeat: false,
	useControls: true,
	fixedControls: false,
	overlayOptions: {
		opacity: 1,
		position: 'top right',
		hideOnMouseOut: false
	}
});
/* hislide initialization */

(function($){
    $(document).on('click', '.request-prof', function(){
        $.ajax({
            url: '/base/request_prof_status',
            dataType: 'json',
            type: 'get',
            success: function(data) {
                var r = data.result;
                if( r == 1 ) {
                    Alert.message('Запрос статуса професионала отправлен.');
                } else {
                    Alert.error('Ошибка выполнения запроса.');
                }
            }
        });
    });
})(jQuery);

(function($){
    $(document).on('click', '.request-ads', function(){
        $.ajax({
            url: '/base/request_ads_status',
            dataType: 'json',
            type: 'get',
            success: function(data) {
                var r = data.result;
                if( r == 1 ) {
                    Alert.message('Запрос статуса "Рекламного агенства" отправлен.');
                } else {
                    Alert.error('Ошибка выполнения запроса.');
                }
            }
        });
    });
})(jQuery);

$j(function(){
	TopMenu.init();
	$j('#dlg-registration-container').kRegistration();
	$j('#dlg-registeractivation-container').kRegisterActivation();
	$j('#dlg-login-container').kLogin();
	$j('#dlg-reminder-container').kReminder();
	$j('#dlg-action-container').kAction();
	$j('#dlg-anounce-container').kAnounce();
	
	$j('.link-register').click(function(){
		Events.publish('/Registration/Show');
	});
	$j('.link-login').click(function(){
		Events.publish('/Login/Show');
	});
	$j('.slideshow').cycle({
		fx: 'fade',
		speed: 200, 
		timeout: 3000
	});
	
	if( Client && Client.isLogged() && !Client.phone ) {
		Events.publish( '/Action/Show', [{action:1}] );
	}

	if( Client && Client.isLogged() && Client.type == 5) {
		$j('*[data-hide-when-logged]').hide();
	}
});

var FineUploaderWrapper = {
	options: {
		element: 'fine-uploader-manual-trigger',
		template: 'qq-template-manual-trigger',
		request: {
			endpoint: '/?photos.add_photos_fine_uploader',
		},
		validation: {
			allowedExtensions: ['jpeg', 'jpg', 'zip', 'rar'],
			sizeLimit: 900 * 1024 * 1024
		},
		autoUpload: false,
		debug: false
	},
	uploader: null,
	init: function(options) {
		this.options = Object.assign(this.options, options);
		var o = this.options;
		o.element = document.getElementById(o.element);
		this.uploader = new qq.FineUploader(o);
		return this.uploader;
	},
	setParams: function(params) {
		this.uploader.setParams(params);
	},
	upload: function() {
		this.uploader.uploadStoredFiles();
	}
};

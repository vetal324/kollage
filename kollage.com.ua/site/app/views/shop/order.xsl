<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:exsl="http://exslt.org/common"
                extension-element-prefixes="exsl"
                version="1.0">
                
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout_shop.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:template match="data">
    <h3 class="products_title">
        <table cellspacing="0" cellpadding="0" with="100%"><tr>
        <td width="90%"><xsl:value-of select="$locale/shop/order/title/text()" disable-output-escaping="yes"/></td>
        <td width="*" align="right">
            <xsl:call-template name="shop_trash">
                <xsl:with-param name="back_url" select="back_url"/>
            </xsl:call-template>
        </td>
        </tr></table>
        
    </h3>
    
    <xsl:if test="/content/global/client/id = 0">
        <xsl:call-template name="login_form2"/>
    </xsl:if>
    
    <xsl:if test="/content/global/client/id > 0">
        <script src="{$views_path}/{$controller_name}/functions.js"></script>

        <script>
            var deliveries = { <xsl:value-of select="deliveries_js"/> };

            function UpdateOrder()
            {
                var delivery_id = $('delivery_id').options[ $('delivery_id').selectedIndex ].value;
                var delivery = Number( deliveries[delivery_id][0] ) * <xsl:value-of select="/content/global/currency/exchange_rate"/>;
                $('delivery').update( delivery.toFixed(2) );
                $('total').update( (delivery + Number($('total_products').value)).toFixed(2) );
            }
        </script>
        <form action="?{$controller_name}.order2" method="post">
            <xsl:variable name="total_products"><xsl:apply-templates select="/content/global/shop_trash"/></xsl:variable>
            <input type="hidden" name="total_products" id="total_products" value="{$total_products}"/>
            <input type="hidden" name="object[id]" value="{order/id}"/>
        <table width="100%" cellspacing="1" cellpadding="3" class="trash_list">
            <tr>
                <th width="2%">#</th>
                <th width="*"><xsl:value-of select="$locale/shop/trash/name/text()" disable-output-escaping="yes"/></th>
                <th width="15%"><xsl:value-of select="$locale/shop/trash/price/text()" disable-output-escaping="yes"/><xsl:value-of select="/content/global/currency/symbol" disable-output-escaping="yes"/></th>
                <th width="15%"><xsl:value-of select="$locale/shop/trash/quantity/text()" disable-output-escaping="yes"/></th>
                <th width="15%"><xsl:value-of select="$locale/shop/trash/total/text()" disable-output-escaping="yes"/>, <xsl:value-of select="/content/global/currency/symbol" disable-output-escaping="yes"/></th>
            </tr>
            
            <xsl:apply-templates select="/content/global/shop_trash/*"/>
            
            <tr>
                <td colspan="4"><b><xsl:value-of select="$locale/shop/trash/delivery/text()" disable-output-escaping="yes"/></b></td>
                <td><b><span id="delivery">0.0</span>&#160;<xsl:value-of select="/content/global/currency/symbol" disable-output-escaping="yes"/></b></td>
            </tr>
            
            <tr>
                <td colspan="4"><b><xsl:value-of select="$locale/shop/trash/total2/text()" disable-output-escaping="yes"/></b></td>
                <td><b><span id="total">0.0</span>&#160;<xsl:value-of select="/content/global/currency/symbol" disable-output-escaping="yes"/></b></td>
            </tr>
            
            <tr>
                <td colspan="5">
                    <table cellpadding="0" cellspacing="3">
                    
                        <tr>
                            <td class="ftitle"><xsl:value-of select="$locale/photos/order/delivery_id/text()"/></td>
                        </tr>
                        <tr>
                            <td>
                                <select name="object[delivery_id]" class="middle" id="delivery_id" onchange="UpdateOrder()">
                                    <xsl:variable name="delivery_id" select="order/delivery_id"/>
                                    <xsl:for-each select="deliveries/*">
                                        <xsl:call-template name="gen-option">
                                            <xsl:with-param name="value" select="id"/>
                                            <xsl:with-param name="title" select="name"/>
                                            <xsl:with-param name="selected" select="$delivery_id"/>
                                        </xsl:call-template>
                                    </xsl:for-each>
                                </select>
                            </td>
                        </tr>
                    
                        <tr>
                            <td class="ftitle"><xsl:value-of select="$locale/photos/order/paymentway_id/text()"/></td>
                        </tr>
                        <tr>
                            <td>
                                <select name="object[paymentway_id]" class="middle" id="paymentway_id">
                                    <xsl:variable name="paymentway_id" select="order/paymentway_id"/>
                                    <xsl:for-each select="paymentways/*">
                                        <xsl:call-template name="gen-option">
                                            <xsl:with-param name="value" select="id"/>
                                            <xsl:with-param name="title" select="name"/>
                                            <xsl:with-param name="selected" select="$paymentway_id"/>
                                        </xsl:call-template>
                                    </xsl:for-each>
                                </select>
                            </td>
                        </tr>
                    
                        <tr>
                            <td class="ftitle"><xsl:value-of select="$locale/photos/order/address/text()"/></td>
                        </tr>
                        <tr>
                            <td><input type="text" class="inputbox_middle2" name="object[address]" id="address" value="{order/address}"/></td>
                        </tr>
                    
                        <tr>
                            <td class="ftitle"><xsl:value-of select="$locale/photos/order/comments/text()"/></td>
                        </tr>
                        <tr>
                            <td><textarea cols="30" rows="3" name="object[comments]" id="comments" style="width:260px;height:50px"><xsl:value-of select="order/comments" disable-output-escaping="yes"/></textarea></td>
                        </tr>
                        
                    </table>
                </td>
            </tr>
            
            <tr>
                <td colspan="10">
                    <input type="submit" class="trash_button" value="{$locale/common/buttons/order/text()}"/>&#160;&#160;
                    <span class="error2"><xsl:value-of select="msg"/></span>
                </td>
            </tr>
        </table>
        </form>
        <script>UpdateOrder()</script>
    </xsl:if>
    
    <!-- <xsl:call-template name="new_products"/> -->
    <xsl:call-template name="best_products"/>
    
</xsl:template>

<xsl:template match="shop_trash">
    <xsl:variable name="tmpTotal">
        <xsl:for-each select="./*">
            <xsl:choose>
                <xsl:when test="product/nice_price_active = 1">
                    <item>
                        <xsl:value-of select="number(product/nice_price)* /content/global/currency/exchange_rate * @quantity"/>
                    </item>
                </xsl:when>
                <xsl:otherwise>
                    <item>
                        <xsl:choose>
                            <xsl:when test="/content/global/client/type_id = 2">
                                <xsl:value-of select="number(product/price_opt)* /content/global/currency/exchange_rate * @quantity"/>
                            </xsl:when>
                            <xsl:when test="/content/global/client/type_id = 3">
                                <xsl:value-of select="number(product/price_opt2)* /content/global/currency/exchange_rate * @quantity"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="number(product/price)* /content/global/currency/exchange_rate * @quantity"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </item>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:variable>
    <xsl:variable name="myTotal" select="exsl:node-set($tmpTotal)/*"/>
    <xsl:if test="$myTotal > 0"><xsl:value-of select="sum($myTotal)"/></xsl:if>
</xsl:template>

<xsl:template match="item">
    <tr>
        <td><xsl:value-of select="position()"/>.</td>
        <td><xsl:value-of select="product/name"/></td>
        <td>
            <xsl:choose>
                <xsl:when test="product/nice_price_active = 1">
                    <item>
                        <xsl:value-of select="number(product/nice_price)* /content/global/currency/exchange_rate"/>
                    </item>
                </xsl:when>
                <xsl:otherwise>
                    <item>
                        <xsl:choose>
                            <xsl:when test="/content/global/client/type_id = 2">
                                <xsl:value-of select="number(product/price_opt)* /content/global/currency/exchange_rate"/>
                            </xsl:when>
                            <xsl:when test="/content/global/client/type_id = 3">
                                <xsl:value-of select="number(product/price_opt2)* /content/global/currency/exchange_rate"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="number(product/price)* /content/global/currency/exchange_rate"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </item>
                </xsl:otherwise>
            </xsl:choose>
        </td>
        <td align="center"><xsl:value-of select="@quantity"/></td>
        <td>
            <xsl:choose>
                <xsl:when test="product/nice_price_active = 1">
                    <item>
                        <xsl:value-of select="number(product/nice_price)* /content/global/currency/exchange_rate * @quantity"/>
                    </item>
                </xsl:when>
                <xsl:otherwise>
                    <item>
                        <xsl:choose>
                            <xsl:when test="/content/global/client/type_id = 2">
                                <xsl:value-of select="number(product/price_opt)* /content/global/currency/exchange_rate * @quantity"/>
                            </xsl:when>
                            <xsl:when test="/content/global/client/type_id = 3">
                                <xsl:value-of select="number(product/price_opt2)* /content/global/currency/exchange_rate * @quantity"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="number(product/price)* /content/global/currency/exchange_rate * @quantity"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </item>
                </xsl:otherwise>
            </xsl:choose>
        </td>
    </tr>
</xsl:template>

</xsl:stylesheet>

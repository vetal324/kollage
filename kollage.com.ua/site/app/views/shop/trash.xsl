<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:exsl="http://exslt.org/common"
                extension-element-prefixes="exsl"
                version="1.0">
                
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout_shop.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:template match="data">
    <h3 class="products_title">
        <table cellspacing="0" cellpadding="0" width="100%" border="0"><tr>
        <td width="60%"><xsl:value-of select="$locale/shop/trash/title/text()" disable-output-escaping="yes"/></td>
        <td width="*" align="right"><a style="color:#cc0000;font-size:11pt" href="{back_url}"><xsl:value-of select="$locale/shop/trash/back/text()" disable-output-escaping="yes"/></a></td>
        </tr></table>
    </h3>

    <table width="100%" cellspacing="1" cellpadding="3" class="trash_list">
        <tr>
            <th width="2%">#</th>
            <th width="*"><xsl:value-of select="$locale/shop/trash/name/text()" disable-output-escaping="yes"/></th>
            <th width="15%"><xsl:value-of select="$locale/shop/trash/price/text()" disable-output-escaping="yes"/><xsl:value-of select="/content/global/currency/symbol" disable-output-escaping="yes"/></th>
            <th width="15%"><xsl:value-of select="$locale/shop/trash/quantity/text()" disable-output-escaping="yes"/></th>
            <th width="15%"><xsl:value-of select="$locale/shop/trash/total/text()" disable-output-escaping="yes"/>, <xsl:value-of select="/content/global/currency/symbol" disable-output-escaping="yes"/></th>
            <th width="1%">&#160;</th>
        </tr>
        <form action="?{$controller_name}.trash_update" method="post">
        <xsl:apply-templates select="/content/global/shop_trash/*"/>
        
        <tr>
            <td colspan="4"><b><xsl:value-of select="$locale/shop/trash/total2/text()" disable-output-escaping="yes"/></b></td>
            <td colspan="2"><xsl:apply-templates select="/content/global/shop_trash"/></td>
        </tr>
        
        <tr>
            <td colspan="10">
                <input type="button" class="trash_button" value="{$locale/common/buttons/order/text()}" onclick="document.location='?{$controller_name}.order'"/>
                &#160;
                <input type="submit" class="trash_button" value="{$locale/common/buttons/form_update_trash/text()}"/>
            </td>
        </tr>
        </form>
    </table>
    
    <!-- <xsl:call-template name="new_products"/> -->
    <xsl:call-template name="best_products"/>
    
</xsl:template>

<xsl:template match="shop_trash">
    <xsl:variable name="tmpTotal">
        <xsl:for-each select="./*">
            <xsl:choose>
                <xsl:when test="product/nice_price_active = 1">
                    <item>
                        <xsl:value-of select="number(product/nice_price)* /content/global/currency/exchange_rate * @quantity"/>
                    </item>
                </xsl:when>
                <xsl:otherwise>
                    <item>
                        <xsl:choose>
                            <xsl:when test="/content/global/client/type_id = 2">
                                <xsl:value-of select="number(product/price_opt)* /content/global/currency/exchange_rate * @quantity"/>
                            </xsl:when>
                            <xsl:when test="/content/global/client/type_id = 3">
                                <xsl:value-of select="number(product/price_opt2)* /content/global/currency/exchange_rate * @quantity"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="number(product/price)* /content/global/currency/exchange_rate * @quantity"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </item>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:variable>
    <xsl:variable name="myTotal" select="exsl:node-set($tmpTotal)/*"/>
    <xsl:if test="$myTotal > 0">
        <b><xsl:value-of select="format-number( sum($myTotal), '#0.00' )"/>&#160;<xsl:value-of select="/content/global/currency/symbol" disable-output-escaping="yes"/></b>
    </xsl:if>
</xsl:template>

<xsl:template match="item">
    <tr>
        <td><xsl:value-of select="position()"/>.</td>
        <td><xsl:value-of select="product/name"/></td>
        <td>
            <xsl:choose>
                <xsl:when test="product/nice_price_active = 1">
                    <item>
                        <xsl:value-of select="format-number( number(product/nice_price)* /content/global/currency/exchange_rate, '#0.00' )"/>
                    </item>
                </xsl:when>
                <xsl:otherwise>
                    <item>
                        <xsl:choose>
                            <xsl:when test="/content/global/client/type_id = 2">
                                <xsl:value-of select="format-number( number(product/price_opt)* /content/global/currency/exchange_rate, '#0.00' )"/>
                            </xsl:when>
                            <xsl:when test="/content/global/client/type_id = 3">
                                <xsl:value-of select="format-number( number(product/price_opt2)* /content/global/currency/exchange_rate, '#0.00' )"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="format-number( number(product/price)* /content/global/currency/exchange_rate, '#0.00' )"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </item>
                </xsl:otherwise>
            </xsl:choose>
        </td>
        <td align="center"><input type="hidden" name="id{position()}" value="{product/id}"/><input type="text" name="quantity{position()}" value="{@quantity}" class="inputbox_small"/></td>
        <td>
            <xsl:choose>
                <xsl:when test="product/nice_price_active = 1">
                    <item>
                        <xsl:value-of select="format-number( number(product/nice_price)* /content/global/currency/exchange_rate * @quantity, '#0.00' )"/>
                    </item>
                </xsl:when>
                <xsl:otherwise>
                    <item>
                        <xsl:choose>
                            <xsl:when test="/content/global/client/type_id = 2">
                                <xsl:value-of select="format-number( number(product/price_opt)* /content/global/currency/exchange_rate * @quantity, '#0.00' )"/>
                            </xsl:when>
                            <xsl:when test="/content/global/client/type_id = 3">
                                <xsl:value-of select="format-number( number(product/price_opt2)* /content/global/currency/exchange_rate * @quantity, '#0.00' )"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="format-number( number(product/price)* /content/global/currency/exchange_rate * @quantity, '#0.00' )"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </item>
                </xsl:otherwise>
            </xsl:choose>
        </td>
        <td align="center"><a href="#" onclick="del_item('?{$controller_name}.trash_remove&amp;id={product/id}','{$locale/common/msgs/delete_item/text()}')"><img src="{$views_path}/images/close_red.gif" border="0" alt="{$locale/common/msgs/delete_item_alt/text()}"/></a></td>
    </tr>
</xsl:template>

</xsl:stylesheet>

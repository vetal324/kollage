<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
                
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout_shop.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:template match="data">
    <h3 class="products_title"><xsl:value-of select="$locale/my/orders/title/text()"/></h3>
    
    <xsl:if test="/content/global/client/id = 0">
        <xsl:call-template name="login_form2"/>
    </xsl:if>
    
    <xsl:if test="/content/global/client/id > 0">
        <!-- ORDERS FORM -->
        
            <!-- shop -->
            <xsl:if test="count(shop/*)">
                <h3 class="form_header"><xsl:value-of select="$locale/my/orders/shop/title/text()"/></h3>
                <table cellpadding="3" cellspacing="1" class="content_table">
                
                <tr>
                    <th width="20"><xsl:value-of select="$locale/my/orders/photo/n/text()"/></th>
                    <th width="128"><xsl:value-of select="$locale/my/orders/photo/number/text()"/></th>
                    <th width="90"><xsl:value-of select="$locale/my/orders/photo/status/text()"/></th>
                    <th width="*"><xsl:value-of select="$locale/my/orders/photo/comments/text()"/></th>
                    <th width="80"><xsl:value-of select="$locale/my/orders/shop/total_price/text()"/>, <xsl:value-of select="shop/order[position()=1]/currency/symbol"/></th>
                </tr>
                
                <xsl:apply-templates select="shop"/>
                
                </table>
            </xsl:if>
            <!-- shop - end-->
            
        <!-- ORDERS FORM - END -->
    </xsl:if>
    
    <!-- <xsl:call-template name="new_products"/> -->
    <xsl:call-template name="best_products"/>
    
</xsl:template>

<xsl:template match="shop/order">
<tr>
    <td><xsl:value-of select="position()"/>.</td>
    <td><b><xsl:value-of select="substring(order_number,1,10)"/></b><br/><xsl:value-of select="created_at"/></td>
    <td>
        <xsl:choose>
            <xsl:when test="status = 1">
                <xsl:value-of select="$locale/my/orders/shop/statuses/s1/text()"/>
            </xsl:when>
            <xsl:when test="status = 2">
                <xsl:value-of select="$locale/my/orders/shop/statuses/s2/text()"/>
            </xsl:when>
            <xsl:when test="status = 3">
                <xsl:value-of select="$locale/my/orders/shop/statuses/s3/text()"/>
            </xsl:when>
            <xsl:when test="status = 4">
                <xsl:value-of select="$locale/my/orders/shop/statuses/s4/text()"/>
            </xsl:when>
            <xsl:when test="status = 5">
                <xsl:value-of select="$locale/my/orders/shop/statuses/s5/text()"/>
            </xsl:when>
            <xsl:when test="status = 99">
                <xsl:value-of select="$locale/my/orders/shop/statuses/s99/text()"/>
            </xsl:when>
        </xsl:choose>
    </td>
    <td>
        <xsl:value-of select="$locale/my/orders/photo/delivery/text()"/>: <xsl:value-of select="delivery/name"/>;
        
        <xsl:if test="delivery != '0.00' or photos_count > 100">
        <xsl:value-of select="$locale/my/orders/photo/address/text()"/>: 
        <xsl:choose>
            <xsl:when test="address != ''">
                <xsl:value-of select="address"/>;
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="/content/global/client/address"/>;
            </xsl:otherwise>
        </xsl:choose>
        </xsl:if>
        
    </td>
    <td align="right"><xsl:value-of select="format-number(amount * currency/exchange_rate, '0.00')"/></td>
</tr>
</xsl:template>

</xsl:stylesheet>

<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
                
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout_shop.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:template match="data">
    <h3 class="products_title"><xsl:value-of select="$locale/client_activate/title/text()" disable-output-escaping="yes"/></h3>
    <xsl:choose>
        <xsl:when test="result = 1">
            <p class="msg"><xsl:value-of select="$locale/client_activate/you_were_activated/text()" disable-output-escaping="yes"/></p>
        </xsl:when>
        <xsl:otherwise>
            <p class="error"><xsl:value-of select="$locale/client_activate/error/text()" disable-output-escaping="yes"/></p>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

</xsl:stylesheet>

<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../layout.xsl" />
<xsl:include href="shop_layout.xsl" />

<xsl:template match="data">
	<xsl:call-template name="shop_layout"/>
	
	<script type="text/javascript">
	var Category = {
		Id: parseInt('<xsl:value-of select="category/id" disable-output-escaping="yes"/>'),
		Name: '<xsl:value-of select="category/name" disable-output-escaping="yes"/>'
	};
	var Prices = {
		exchangeRate: parseFloat('<xsl:value-of select="$exchange_rate"/>'),
		priceMin: parseFloat('<xsl:value-of select="price_min"/>'),
		priceMax: parseFloat('<xsl:value-of select="price_max"/>'),
		
		getPriceMin: function() {
			return( (this.exchangeRate * this.priceMin).toFixed(2) );
		},
		
		getPriceMax: function() {
			return( (this.exchangeRate * this.priceMax).toFixed(2) );
		}
	};
	</script>
	<div class="shop">
		<div class="shop-top">
			<xsl:for-each select="cline/*">
				<xsl:if test="position() > 1">&#160;&#187;&#160;</xsl:if>
				<xsl:choose>
					<xsl:when test="position() = 1">
						<h1><a href="/shop/category/{id}"><xsl:value-of select="name"/></a></h1>
					</xsl:when>
					<xsl:when test="position() = 2">
						<h2><a href="/shop/category/{id}"><xsl:value-of select="name"/></a></h2>
					</xsl:when>
					<xsl:otherwise>
						<h3><xsl:value-of select="name"/></h3>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</div>
		<div class="shop-left-column">
			<div class="accordion-block">
				<h3>����</h3>
				<div class="slider-container slider-price"></div>
				<table class="slider-values"><tr>
					<td><span>��</span></td>
					<td><input type="text" class="slider-value-from price-from" value="{format-number( price_min*$exchange_rate,'0.00')}"/></td>
					<td><span>��</span></td>
					<td><input type="text" class="slider-value-to price-to" value="{format-number( price_max*$exchange_rate,'0.00')}"/></td>
				</tr></table>
			</div>
			
			<xsl:if test="count(categories/category) > 0">
				<div class="accordion-block">
					<h3>������������</h3>
					<ul>
					<xsl:for-each select="categories/category">
						<li><a href="/shop/category/{id}"><xsl:value-of select="name"/></a></li>
					</xsl:for-each>
					</ul>
				</div>
			</xsl:if>
			
			<xsl:apply-templates select="features/feature"/>
		</div>
		<div class="shop-right-column">
			<div class="shop-products-wrapper">
			
				<div class="shop-nav-panel">
					<table width="100%"><tr>
						<td><a class="shop-nav-listview vtip inactive" title="�������� ������� � ���� ������"></a></td>
						<td><a class="shop-nav-gridview vtip active" title="�������� ������� � ���� �������"></a></td>
						<td>
							<select class="shop-nav-items-per-page for-listview hidden" title="������� �������� ������� �� ��������">
								<option value="20">20</option>
								<option value="30">30</option>
								<option value="40">40</option>
								<option value="50">50</option>
							</select>
						</td>
						<td>
							<select class="shop-nav-items-per-page for-gridview vtip" title="������� �������� ������� �� ��������">
								<option value="12">12</option>
								<option value="15">15</option>
								<option value="20">20</option>
								<option value="25">25</option>
								<option value="50">50</option>
							</select>
						</td>
						<td>&#160;</td>
						<td>
							<select class="shop-nav-show-products">
								<option value="2">��������� �����</option>
								<option value="1">���� �����</option>
							</select>
						</td>
						<td align="right" width="70%">
							<table><tr>
							<td>
								<table class="trash-icon"><tr>
									<td><a class="btn-trash" onclick="return false" href="#"></a></td>
									<td><span><a title="{$locale/shop/btn_trash_tip}" class="trash-link vtip" href="#" onclick="return(false)"><xsl:value-of select="$locale/shop/trash_icon_text" disable-output-escaping="yes"/></a><span class="red2"><b> (<span class="trash-counter">0</span>)</b></span></span></td>
								</tr></table>
							</td>
							<td>
								<table class="compare-icon"><tr>
									<td><a class="btn-compare" onclick="return false" href="#"></a></td>
									<td><span><a title="{$locale/shop/btn_compare_tip}" class="compare-link vtip" href="#" onclick="return(false)"><xsl:value-of select="$locale/shop/compare_icon_text" disable-output-escaping="yes"/></a><span class="red2"><b> (<span class="compare-counter">0</span>)</b></span></span></td>
								</tr></table>
							</td>
							</tr></table>
						</td>
					</tr></table>
				</div>
				
				<div class="shop-products"></div>
				<div class="clear-both"></div>
				<div class="shop-products-paginator-wrapper">
					<div class="shop-products-paginator"></div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="{$views_path}/{$controller_name}/functions.js"></script>
</xsl:template>

<xsl:template match="feature">
	<xsl:if test="@show_in_filter=1 and count(value) > 0">
		<div class="accordion-block">
			<h3><xsl:value-of select="@name"/></h3>
			<xsl:choose>
			
				<xsl:when test="@is_numeric = 1">
					<xsl:variable name="feature_from">
						<xsl:call-template name="min_number">
							<xsl:with-param name="list" select="value"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="feature_to">
						<xsl:call-template name="max_number">
							<xsl:with-param name="list" select="value"/>
						</xsl:call-template>
					</xsl:variable>
					<div class="slider-container slider-feature-value" id="id{@id}"></div>
					<table class="slider-values"><tr>
						<td><span>��</span></td>
						<td><input type="text" class="slider-value-from feature-value-from" id="id{@id}" value="{$feature_from}"/></td>
						<td><span>��</span></td>
						<td><input type="text" class="slider-value-to feature-value-to" id="id{@id}" value="{$feature_to}"/></td>
					</tr></table>
				</xsl:when>
				
				<xsl:otherwise>
					<xsl:apply-templates select="value"/>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:if>
</xsl:template>

<xsl:template name="max_number">
	<xsl:param name="list"/>
	<xsl:for-each select="$list">
		<xsl:sort select="@name" data-type="number" order="descending"/>
		<xsl:if test="position()=1">
			<xsl:value-of select="@name"/>
		</xsl:if>
	</xsl:for-each>
</xsl:template>

<xsl:template name="min_number">
	<xsl:param name="list"/>
	<xsl:for-each select="$list">
		<xsl:sort select="@name" data-type="number" order="ascending"/>
		<xsl:if test="position()=1">
			<xsl:value-of select="@name"/>
		</xsl:if>
	</xsl:for-each>
</xsl:template>

<xsl:template match="feature/value">
	<div class="feature-value">
		<input type="checkbox" class="feature-value-checkbox" id="feature_value_{@id}" value="{@id}"/><label  class="feature-value-label" for="feature_value_{@id}"><xsl:value-of select="@name"/>&#160;(<xsl:value-of select="@c"/>)</label>
	</div>
</xsl:template>

</xsl:stylesheet>

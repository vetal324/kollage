
var Shop = {
	$: jQuery,
	
	options: {
		pageCover: '.page-cover',
		filterContainer: '.shop-left-column',
		feature: '.feature-value-checkbox',
		featureLabel: '.feature-value-label',
		featureValueSlider: '.slider-feature-value',
		featureValueContainer: '.slider-values',
		featureValueFrom: '.feature-value-from',
		featureValueTo: '.feature-value-to',
		sliderPrice: '.slider-price',
		priceFrom: '.price-from',
		priceTo: '.price-to',
		nav: {
			container: '.shop-nav-panel',
			listView: '.shop-nav-listview',
			gridView: '.shop-nav-gridview',
			itemsPerPageList: '.shop-nav-items-per-page.for-listview',
			itemsPerPageGrid: '.shop-nav-items-per-page.for-gridview',
			showProducts: '.shop-nav-show-products'
		},
		productsContainer: '.shop-products',
		productPaginator: '.shop-products-paginator',
		product: {
			productGridItem: '.product-grid-item',
			productListItem: '.product-list-item',
			price: '.price',
			btnBuyNow: '.btn-buy-now',
			btnAddToCompare: '.btn-add-to-compare',
			btnAddedToCompare: '.btn-added-to-compare'
		}
	},
	
	category: Category || { Id:0, Name:'' },
	
	loadTimer: {
		timer: null,
		timeout: 30
	},
	
	page: 1,
	productGridItemTemplate: null,
	
	productListHeaderTemplate: null,
	productListItemTemplate: null,
	
	data: null, //loaded items
	
	init: function() {
		var self = this, o = this.options;
		
		// do nothing if it's not product list page
		if( this.$(o.productsContainer).length == 0 ) return;
		
		var tplPath = Site.ViewsPath +'/'+ Site.ControllerName;
		require(['text!'+ tplPath +'/product-grid-item.html'], function(template1){
			self.productGridItemTemplate = template1;
			if( self.isGridActive() ) {
				self.loadProducts();
			}
		});
		require([
			'text!'+ tplPath +'/product-list-header.html',
			'text!'+ tplPath +'/product-list-item.html'
			], function( header, item, footer ){
				self.productListHeaderTemplate = header;
				self.productListItemTemplate = item;
				if( !self.isGridActive() ) {
					self.loadProducts();
				}
		});
		
		this.$(o.featureLabel, o.filterContainer).click(function(){
			var cb = self.$(this).prev();
			if( cb.is(':checked') ) cb.removeClass('cb-checked');
			else cb.addClass('cb-checked');
		});
		
		this.$(o.feature, o.filterContainer).change(function(){
			self.page = 1;
			Events.publish( '/Shop/Category/LoadProducts' );
		});
		
		this.$(o.nav.itemsPerPageList, o.nav.container).change(function(){
			self.page = 1;
			Events.publish( '/Shop/Category/LoadProducts' );
		});
		
		this.$(o.nav.itemsPerPageGrid, o.nav.container).change(function(){
			self.page = 1;
			Events.publish( '/Shop/Category/LoadProducts' );
		});
		
		this.$(o.nav.showProducts, o.nav.container).change(function(){
			self.page = 1;
			Events.publish( '/Shop/Category/LoadProducts' );
		});
		
		this.$(o.nav.listView, o.nav.container).click(function(){
			self.goListView();
		});
		
		this.$(o.nav.gridView, o.nav.container).click(function(){
			self.goGridView();
		});
		
		this.$(o.sliderPrice, o.filterContainer).slider({
			min: this.getPriceFrom(),
			max: this.getPriceTo(),
			step: 10,
			range: true,
			values: [ this.getPriceFrom(), this.getPriceTo()],
			change: function( event, ui ) {
				self.page = 1;
				Events.publish( '/Shop/Category/LoadProducts' );
			},
			slide: function( event, ui ) {
				var v = ui.values;
				self.setPriceFrom( v[0] );
				self.setPriceTo( v[1] );
			}
		});
		
		this.$(o.priceFrom, o.filterContainer).keyup(function(){
			self.$(this).val( self._fixNumber(self.$(this).val()) );
			self.$(o.sliderPrice, o.filterContainer).slider( 'values', 0, self.getPriceFrom() );
		});
		this.$(o.priceFrom, o.filterContainer).change(function(){
			self.setPriceFrom( self.getPriceFrom() );
		});
		
		this.$(o.priceTo, o.filterContainer).keyup(function(){
			self.$(this).val( self._fixNumber(self.$(this).val()) );
			self.$(o.sliderPrice, o.filterContainer).slider( 'values', 1, self.getPriceTo() );
		});
		this.$(o.priceTo, o.filterContainer).change(function(){
			self.setPriceTo( self.getPriceTo() );
		});
		
		var featureValuesFrom = self.$(o.featureValueFrom, o.filterContainer);
		var featureValuesTo = self.$(o.featureValueTo, o.filterContainer);
		for( var i=0; i<featureValuesFrom.length; i++ ) {
			var f = this.$(featureValuesFrom[i]);
			var t = this.$(featureValuesTo[i]);
			var id = f.attr('id');
			if( id ) {
				id = id.substring( 2 );
				var fv = parseFloat( f.val() );
				var tv = parseFloat( t.val() );
				this.$(o.featureValueSlider+'#id'+id, o.filterContainer).slider({
					min: fv,
					max: tv,
					step: 0.1,
					range: true,
					values: [ fv, tv ],
					change: function( event, ui ) {
						self.page = 1;
						Events.publish( '/Shop/Category/LoadProducts' );
					},
					slide: function( event, ui ) {
						var v = ui.values;
						var id = self.$(this).attr('id');
						if( id ) {
							id = id.substring( 2 );
							self.$(o.featureValueFrom+'#id'+id).val( v[0] );
							self.$(o.featureValueTo+'#id'+id).val( v[1] );
						}
					}
				});
				f.keyup(function(){
					self.$(this).val( self._fixNumber(self.$(this).val()) );
					self.$(this).parents(o.featureValueContainer).first().prev(o.featureValueSlider).slider( 'values', 0, self.$(this).val() );
				});
				t.keyup(function(){
					self.$(this).val( self._fixNumber(self.$(this).val()) );
					self.$(this).parents(o.featureValueContainer).first().prev(o.featureValueSlider).slider( 'values', 1, self.$(this).val() );
				});
			}
		}
		
		Events.subscribe( '/Shop/Category/LoadProducts', function(){
			if( self.loadTimer.timer ) {
				clearTimeout( self.loadTimer.timer );
			}
			self.loadTimer.timer = setTimeout(function(){
				self.loadProducts();
				self.loadTimer.timer = null;
			}, self.loadTimer.timeout );
		});
		
		Events.subscribe( '/Shop/Category/ApplyProducts', function(){
			self._applyProducts( self.data );
		});
	},
	
	goListView: function() {
		var self = this, o = this.options;
		
		self.$(o.nav.listView, o.nav.container).removeClass('inactive');
		self.$(o.nav.listView, o.nav.container).addClass('active');
		
		self.$(o.nav.gridView, o.nav.container).addClass('inactive');
		self.$(o.nav.gridView, o.nav.container).removeClass('active');
		
		self.$( o.nav.itemsPerPageGrid, o.nav.container ).hide();
		self.$( o.nav.itemsPerPageList, o.nav.container ).show();
		
		Events.publish( '/Shop/Category/LoadProducts' );
	},
	
	goGridView: function() {
		var self = this, o = this.options;
		
		self.$(o.nav.listView, o.nav.container).removeClass('active');
		self.$(o.nav.listView, o.nav.container).addClass('inactive');
		
		self.$(o.nav.gridView, o.nav.container).addClass('active');
		self.$(o.nav.gridView, o.nav.container).removeClass('inactive');
		
		self.$( o.nav.itemsPerPageList, o.nav.container ).hide();
		self.$( o.nav.itemsPerPageGrid, o.nav.container ).show();
		
		Events.publish( '/Shop/Category/LoadProducts' );
	},
	
	_showCover: function() {
		this.$( this.options.pageCover ).show();
	},
	
	_hideCover: function() {
		this.$( this.options.pageCover ).hide();
	},
	
	_fixNumber: function( v ) {
		v = v.replace( /,/g, '.' );
		return( v );
	},
	
	getPriceType: function() {
		return( 1 );
	},
	
	getPriceFrom: function() {
		return( parseFloat(this.$(this.options.priceFrom).val()) );
	},
	setPriceFrom: function( v ) {
		this.$(this.options.priceFrom).val( parseFloat(v).toFixed(2) );
	},
	
	getPriceTo: function() {
		return( parseFloat(this.$(this.options.priceTo).val()) );
	},
	setPriceTo: function( v ) {
		this.$(this.options.priceTo).val( parseFloat(v).toFixed(2) );
	},
	
	_isPriceRangeSet: function() {
		var retval = false;
		if( Prices.getPriceMin() != this.getPriceFrom() || Prices.getPriceMax() != this.getPriceTo() ) {
			retval = true;
		}
		
		return( retval );
	},
	
	getItemsCountPerPage: function() {
		var o = this.options;
		var retval;
		
		if( this.isGridActive() ) {
			retval = parseInt( this.$( o.nav.itemsPerPageGrid, o.nav.container ).val() );
		}
		else {
			retval = parseInt( this.$( o.nav.itemsPerPageList, o.nav.container ).val() );
		}
		
		if( retval < 8 ) retval = 8;
		
		return( retval );
	},
	
	isGridActive: function() {
		return( this.$( this.options.nav.gridView, this.options.nav.container ).hasClass('active') );
	},
	
	isListActive: function() {
		return( !this.isGridActive() );
	},
	
	getShowProducts: function() {
		var o = this.options;
		var retval = parseInt( this.$( o.nav.showProducts, o.nav.container ).val() );
		if( retval<1 ) retval = 1;
		return( retval );
	},
	
	loadProducts: function() {
		var self = this, o = this.options;
		
		this._showCover();
		
		var features = [];
		var fe = this.$(o.feature, o.filterContainer);
		this.$.each( fe, function(){
			if( self.$(this).is(':checked') ) features.push( self.$(this).val() );
		});
		
		var data = {
			category_id: this.category.Id,
			page: self.page,
			rows_per_page: this.getItemsCountPerPage(),
			features: features.join(','),
			price_range_set: ( this._isPriceRangeSet() )? 1 : 0,
			price_type: this.getPriceType(),
			price_from:  (this.getPriceFrom()/Prices.exchangeRate).toFixed(2),
			price_to: (this.getPriceTo()/Prices.exchangeRate).toFixed(2),
			shop_show_products: this.getShowProducts()
		};
		
		var fvFrom = this.$(o.featureValueFrom, o.filterContainer);
		this.$.each( fvFrom, function(){
			var min = self.$(this).parents(o.featureValueContainer).first().prev(o.featureValueSlider).slider( 'option', 'min' );
			var max = self.$(this).parents(o.featureValueContainer).first().prev(o.featureValueSlider).slider( 'option', 'max' );
			var iFrom = self.$(this);
			var vFrom = parseFloat( iFrom.val() );
			var iTo = self.$(this).parents(o.featureValueContainer).first().find(o.featureValueTo).first();
			var vTo = parseFloat( iTo.val() );
			if( vFrom!=null && vTo!=null && (vFrom>min || vTo<max) ) {
				var id = iFrom.attr('id').substring( 2 );
				data['feature_value_from'+ id] = vFrom;
				data['feature_value_to'+ id] = vTo;
			}
		});
		
		this.$.ajax({
			url: '/shop/json_filter_products',
			data: data,
			dataType: 'json',
			type: 'post',
			success: function(data) {
				self.data = data;
				self._applyProducts( data );
				vtip();
				self._hideCover();
			}
		});
	},
	
	_applyProducts: function( data ) {
		var self = this, o = this.options;
		var container = this.$(o.productsContainer).first();
		container.empty();
		if( data && data.result && data.result==1 ) {
			var template;
			if( self.isGridActive() ) {
				template = this.productGridItemTemplate;
			}
			else {
				template = this.productListItemTemplate;
				container.append( this.$('<table>') );
				container = container.find('table').first();
				container.append( this.productListHeaderTemplate );
			}
			for( var i=0; i<data.items.length; i++ ) {
				var item = data.items[i];
				item = this._formatPrices( item );
				item = this._addShareInformation( item );
				var html = jsontemplate.Template(template).expand({item:item});
				html = this.$( html );
				if( this._isProductActive(item) ) {
					html.addClass('active');
				}
				else {
					html.addClass('inactive');
				}
				
				if( self.isGridActive() ) {
					var img = this.$( this._wrapHighSlide( item, this._getProductThumbnail(item) ) );
					html.find('.picture').first().append( img );
					if( item.is_new==1 ) html.find('.picture').first().addClass( 'new-product-icon' );
					if( item.is_action==1 ) html.find('.picture').first().addClass( 'action-product-icon' );
				}
				else {
					var thumb = this.$( this._wrapHighSlide( item, this._getProductThumbnail(item,0,16) ) );
					html.find('.actions').first().append( thumb );
					var newActionEtc = html.find('.new-action-etc');
					if( item.is_new==1 ) newActionEtc.append( this.$('<span>').attr('title','новый товар').html('new') );
					if( item.is_action==1 ) newActionEtc.append( this.$('<span>').attr('title','горячее предложение').html('hot') );
				}
				
				if( item.is_product_in_comparision == 1 ) {
					html.find(o.product.btnAddToCompare).hide();
					html.find(o.product.btnAddedToCompare).show();
				}
				else {
					html.find(o.product.btnAddToCompare).show();
					html.find(o.product.btnAddedToCompare).hide();
				}
				
				container.append( html );
			}
		
			this.$(o.product.btnAddToCompare, o.productsContainer).click(function(){
				var i = self.$(this);
				var id = i.attr('rel');
				Events.publish( '/Shop/Comparision/Add', [id, function(result) {
					self._applyComparision( i, result );
				}] );
			});
			
			this.$(o.product.btnBuyNow,o.product.productGridItem+'.active').click(function(){
				var i = self.$(this);
				var id = i.attr('rel');
				Events.publish( '/Shop/Trash/Add', [id,1] );
			});
			
			this.$(o.product.btnBuyNow,o.product.productGridItem+'.inactive').click(function(){
				var i = self.$(this);
				var id = i.attr('rel');
				Events.publish( '/RequestProduct/Show', [{id:id}] );
			});
			self._makePaginator( data.page, data.pages );
		}
	},
	
	_applyComparision: function( btnAddToCompare, result ) {
		var self=this, o=this.options;
		
		btnAddToCompare = this.$(btnAddToCompare);
		var btnAddedToCompare = btnAddToCompare.next();
		
		if( result ) {
			btnAddToCompare.hide();
			btnAddedToCompare.show();
		}
		else {
			btnAddToCompare.show();
			btnAddedToCompare.hide();
		}
	},
	
	_makePaginator: function( page, pages ) {
		var o = this.options;
		var c = this.$( o.productPaginator );
		c.empty();
		var p;
		var items = 9;
		c.append( this._makePaginatorPrev( pages ) );
		if( pages > items ) {
			if( page < 6 ) { //1234567...n
				for( var i=1; i<=7; i++ ) {
					c.append( this._makePaginatorItem( i, page ) );
				}
				c.append( this._makePaginatorDots() );
				c.append( this._makePaginatorItem( pages, page ) );
			}
			else if( page > (pages-4) ) { //1...14 15 16 17 18 19 20
				c.append( this._makePaginatorItem( 1, page ) );
				c.append( this._makePaginatorDots() );
				for( var i=(pages-6); i<=pages; i++ ) {
					c.append( this._makePaginatorItem( i, page ) );
				}
			}
			else { //1..56 7 89..n
				c.append( this._makePaginatorItem( 1, page ) );
				c.append( this._makePaginatorDots() );
				for( var i=page-2; i<=(page+2); i++ ) {
					c.append( this._makePaginatorItem( i, page ) );
				}
				c.append( this._makePaginatorDots() );
				c.append( this._makePaginatorItem( pages, page ) );
			}
		}
		else {
			for( var i=1; i<=pages; i++ ) {
				p = this._makePaginatorItem( i, page );
				c.append( p );
			}
		}
		c.append( this._makePaginatorNext( pages ) );
	},
	
	_makePaginatorDots: function() {
		var d = this.$('<span>...</span>');
		d.addClass('dots');
		
		return( d );
	},
	
	_makePaginatorPrev: function( pages ) {
		var self = this;
		var p;
		if( this.page == 1 ) {
			p = this.$('<span class="prev">&lt;</span>');
		}
		else {
			var page = this.page - 1; if( page < 1 ) page = 1; if( page > this.pages ) page = pages;
			p = this.$('<a class="prev">&lt;</a>');
			p.attr({
				href: '#',
				rel: page
			});
			p.click(function(){
				var page = parseInt( self.$(this).attr('rel') );
				self.page = page;
				Events.publish( '/Shop/Category/LoadProducts' );
				return( false );
			});
		}
		
		return( p );
	},
	
	_makePaginatorNext: function( pages ) {
		var self = this;
		var p;
		if( this.page == pages ) {
			p = this.$('<span class="next">&gt;</span>');
		}
		else {
			var page = this.page + 1; if( page < 1 ) page = 1; if( page > this.pages ) page = pages;
			p = this.$('<a class="next">&gt;</a>');
			p.attr({
				href: '#',
				rel: page
			});
			p.click(function(){
				var page = parseInt( self.$(this).attr('rel') );
				self.page = page;
				Events.publish( '/Shop/Category/LoadProducts' );
				return( false );
			});
		}
		
		return( p );
	},
	
	_makePaginatorItem: function( i, page ) {
		var self = this;
		var p;
		
		if( i==page ) {
			p = this.$('<span></span>');
			p.addClass('selected');
		}
		else {
			p = this.$('<a></a>');
			p.attr({
				href: '#',
				rel: i
			});
			p.click(function(){
				var page = parseInt( self.$(this).attr('rel') );
				self.page = page;
				Events.publish( '/Shop/Category/LoadProducts' );
				return( false );
			});
		}
		p.html( i );
		
		return( p );
	},
	
	_addShareInformation: function( item ) {
		var host = UrlTools.getCurrentHost();
		item.share = {
			url: encodeURIComponent( host +'/shop/product/'+ item.id ),
			title: encodeURIComponent(item.name)
		};
		return( item );
	},
	
	_formatPrices: function( item ) {
		var symbol = Site.Currency.Symbol;
		var rate = Site.CurrencyUSD.Rate;
		switch( Client.type ) {
			case 2:
				item.priceFormatted = item.price_opt;
				break;
			case 3:
				item.priceFormatted = item.price_opt2;
				break;
			default:
				item.priceFormatted = item.price;
				break;
		}
		item.priceFormatted = (parseFloat(item.priceFormatted) * rate).toFixed(2) +' '+ symbol;
		return( item );
	},
	
	_isProductActive: function( item ) {
		var retval = false;
		
		if( item && (item.quantity_local>0 || item.quantity_external>0) ) retval = true;
		
		return( retval );
	},
	
	_getProductThumbnail: function( item, width, height ) {
		var retval = '';
		
		if( item && item.images && item.images.length>0 ) {
			var img = item.images[0].path + item.images[0].tiny;
			var w = (width>0)? 'width="'+ width +'"' : '';
			var h = (height>0)? 'height="'+ height +'"' : '';
			var name = Tools.escapeHtml( item.images[0].name );
			retval = '<img src="'+ img +'" border="0" title="'+ name +'" alt="'+ name +'" '+ w +' '+ h +'/>';
		}
		
		return( retval );
	},
	
	_wrapHighSlide: function( item, img ) {
		var retval = '';
		
		if( img && item && item.images && item.images.length>0 ) {
			var large = item.images[0].path + item.images[0].filename;
			retval = '<a href="'+ large +'" class="highslide" onclick="return hs.expand(this,{captionText:\'\',headingText:\''+ item.name.replace(/"/g, '&quot;') +'\'})">'+ img +'</a>';
		}
		else if( img ) {
			retval = img;
		}
		
		return( retval );
	}
};

/* shop trash object */
var ShopTrash = {
	$: jQuery,
	
	options: {
		button: '.btn-trash',
		link: '.trash-link',
		counter: '.trash-counter',
		
		dialog: '#dlg-shop-trash-container',
		dialogContent: '#dlg-content',
		dialogProducts: 'table.trash-products',
		dialogProductLine: 'trash-product-line',
		dialogProductsTotal: 'trash-products-total',
		
		dialogContent2: '#dlg-content2',
		
		dialogLoader: '.dlg-footer-loader',
		
		dialogFooter: '#dlg-footer',
		dialogBtnClose: '.dlg-btn-close',
		dialogBtnOrder: '.dlg-btn-order',
		dialogBtnOrderSave: '.dlg-btn-order-save',
		
		form: {
			name: 'input[name="name"]',
			country: 'select[name="country"]',
			phone: 'input[name="phone"]',
			email: 'input[name="email"]',
			comments: 'input[name="comments"]',
			protectImage: 'img.protect_image',
			reload: '.btn-reload',
			protect_code: 'input[name="protect_code"]'
		}
	},
	
	data: [],
	dialogContentHeight: 0,
	
	init: function() {
		var self = this, o = this.options;
		
		Events.subscribe( '/Shop/Trash/Add', function( id, quantity ){
			self.add( id, quantity );
		});
		Events.subscribe( '/Shop/Trash/Update', function( id, quantity ){
			self.update( id, quantity );
		});
		
		this.$(o.button).click(function(){
			self.open();
		});
		this.$(o.link).click(function(){
			self.open();
		});
		
		this.load();
		this._initDialog();
	},
	
	_initDialog: function() {
		var self = this, o = this.options;
		
		this.$(o.dialog).dialog({
			autoOpen: false,
			resizable: false,
			minHeight: 450,
			minWidth: 640,
			modal: true,
			open: function(event, ui){
				//dialog adjustments
			},
			beforeClose: function(event, ui) {
				self._resetDialog();
			}
		});
		
		this.dialogContentHeight = this.$(o.dialogContent, o.dialog).height();
		
		this.$(o.dialogBtnClose, o.dialog).click(function(){
			self.close();
		});
		
		this.$(o.dialogBtnOrder, o.dialog).click(function(){
			self._showOrderPanel();
		});
		
		this.$(o.dialogBtnOrderSave, o.dialog).click(function(){
			self._order();
		});
		
		this.$.mask.definitions['9'] = '';
		this.$.mask.definitions['d'] = '[0-9]';
		this.$(o.form.phone, o.dialog).mask(' ddddddddd?d');
	},
	
	_resetDialog: function() {
		var self = this, o = this.options;
		
		self.$(o.dialogContent2, o.dialog).height(0);
		self.$(o.dialogContent, o.dialog).height( self.dialogContentHeight );
		self.$(o.dialogBtnOrder, o.dialog).show();
		self.$(o.dialogBtnOrderSave, o.dialog).hide();
	},
	
	load: function( cb ) {
		var self = this, o = this.options;
		
		this.$.ajax({
			url: '/shop/json_get_trash',
			data: {},
			dataType: 'json',
			type: 'get',
			success: function(data) {
				self.data = data;
				self._applyCounter( data );
				if( typeof(cb) == 'function' ) cb();
			}
		});
	},
	
	add: function( id, quantity ) {
		var self = this, o = this.options;
		
		if( quantity < 0 ) quantity = 1;
		this.$.ajax({
			url: '/shop/json_add_to_trash',
			data: {
				id: id,
				quantity: quantity
			},
			dataType: 'json',
			type: 'get',
			success: function(data) {
				self.data = data;
				self._applyCounter( data );
				self._applyDialog();
				if( data.item.added_updated_id > 0 ) {
					Alert.message( 'Товар добавлен в корзину' );
				}
				else {
					Alert.message( 'Товар уже находится в корзине' );
				}
			}
		});
	},
	
	update: function( id, quantity ) {
		var self = this, o = this.options;
		
		this._showDialogLoader();
		if( quantity < 0 ) quantity = 0;
		this.$.ajax({
			url: '/shop/json_update_in_trash',
			data: {
				id: id,
				quantity: quantity
			},
			dataType: 'json',
			type: 'get',
			success: function(data) {
				self.data = data;
				self._applyCounter( data );
				self._applyDialog();
				if( data.item.added_updated_id > 0 ) {
					//msg updated/deleted
				}
				else {
					Alert.error( 'Товар не был обновлен' );
				}
				self._hideDialogLoader();
			}
		});
	},
	
	_order: function() {
		var self = this, o = this.options;
		
		if( this._checkForm() ) {
			var fi = this._getFormItems();
			this._showDialogLoader();
			this.$.ajax({
				url: '/shop/json_order',
				data: {
					name: fi.name,
					phone: fi.phone,
					email: fi.email,
					comments: fi.comments,
					protect_code: fi.protect_code
				},
				dataType: 'json',
				type: 'get',
				success: function(data) {
					if( data.result == 1 ) {
						self.load( function(){
							self._applyDialog();
						});
						self.close();
						Alert.message( 'Спасибо за заказ. Менеджер свяжется с Вами.' );
					}
					else {
						Alert.error( data.message );
					}
					self._hideDialogLoader();
				}
			});
		}
	},
	
	_getFormItems: function() {
		var self = this, o = this.options;
		
		var retval = {
			name: this.$.trim( this.$(o.form.name, o.dialog).val() ),
			country_id: this.$.trim( this.$(o.form.country, o.dialog).val() ),
			phone: Tools.formatPhone( self.$(o.form.phone, o.dialog).val() ),
			email: Tools.formatEmail( self.$(o.form.email, o.dialog).val() ),
			protect_code: this.$.trim( this.$(o.form.protect_code, o.dialog).val() ),
			comments: this.$.trim( this.$(o.form.comments, o.dialog).val() )
		};
		
		return( retval );
	},
	
	_checkForm: function() {
		var self=this, o=this.options;
		var retval = true;
		
		var fi = this._getFormItems();
		
		var obj = self.$(o.form.name, o.dialog);
		if( !fi.name ) {
			obj.addClass('inp-error');
			retval = false;
		} else obj.removeClass('inp-error');
		
		var obj = self.$(o.form.phone, o.dialog);
		if( !fi.phone ) {
			obj.addClass('inp-error');
			retval = false;
		} else obj.removeClass('inp-error');
		
		var obj = self.$(o.form.protect_code, o.dialog);
		if( Client && !Client.isLogged() && !fi.protect_code ) {
			obj.addClass('inp-error');
			retval = false;
		} else obj.removeClass('inp-error');
		
		return( retval );
	},
	
	isEmpty: function() {
		return( !this.data.items.length );
	},
	
	open: function() {
		var self = this, o = this.options;
		
		if( this.isEmpty() ) {
			Alert.message( 'Добавляйте пожалуйста товары в корзину.' );
		}
		else {
			this.$(o.dialog).dialog('open');
			this._applyDialog();
		}
	},
	
	close: function() {
		var self = this, o = this.options;
		this.$(o.dialog).dialog('close');
	},
	
	_applyCounter: function( data ) {
		var self = this, o = this.options;
		
		if( data && data.items && data.items.length >= 0 ) {
			this.$(o.counter).html( data.items.length );
		}
	},
	
	_applyDialog: function() {
		var self = this, o = this.options;
		
		self._resetDialog();
		
		var t = this.$(o.dialogProducts, o.dialog).first();
		t.find('tr.'+o.dialogProductLine).remove();
		t.find('tr.'+o.dialogProductsTotal).remove();
		var totalSum = 0;
		for( var i=0; i<this.data.items.length; i++ ) {
			var it = this.data.items[i];
			var price = this._formatPrices( it.product_price );
			t.find('tr:last').after('<tr class="'+ o.dialogProductLine +'"><td>'+ (i+1) +'.</td><td>'+ it.product_name +'</td><td><span class="price">'+ price +'</span></td><td><input type="text" rel="'+ it.product_id+'" name="quantity" value="'+ it.quantity +'"/></td><td><span class="sum"></span></td><td><a href="#" rel="'+ it.product_id +'" class="remove-link" onclick="return(false)"></a></td></tr>');
			totalSum += parseFloat(price) * parseInt(it.quantity);
		}
		this.$('input[name="quantity"]', o.dialog).spinner({
			min: 1,
			max: 1000,
			stop: function( event, ui ) {
				var e = self.$(this);
				var q = e.spinner('value');
				var id = e.attr('rel');
				self.update( id, q );
				self._calcSum();
			}
		});
		this.$('.remove-link', o.dialog).click(function(){
			var e = self.$(this);
			var id = e.attr('rel');
			self.update( id, 0 );
		});
		self._calcSum();
		
		totalSum = totalSum.toFixed(2);
		t.find('tr:last').after('<tr class="'+ o.dialogProductsTotal +'"><th colspan="4">Сумма заказа: </th><td colspan="2">'+ totalSum +'</td></tr>' );

		function onCountryChange( el ) {
			var optionSelected = self.$("option:selected", el);
			var code = optionSelected.attr('data-code');
			self.$(o.form.phone,o.dialog).val('').mask( code +' ddddddddd?d');
			self.$(o.form.phone,o.dialog).focus();
		}
		var country = this.$(o.form.country,o.dialog).first();
		country.empty();
		for( var i=0; i<Site.Countries.items.length; i++ ) {
			var ctr = Site.Countries.items[i];
			country.append( this.$('<option value="'+ ctr.id +'" data-code="'+ ctr.phone_code +'">'+ ctr.name +'</option>') );
		}
		country.change(function(){
			onCountryChange( this );
		});
		setTimeout( function(){onCountryChange(country)}, 200);
		//country.trigger('change');
	},
	
	_calcSum: function() {
		var self = this, o = this.options;
		var t = this.$(o.dialogProducts, o.dialog).first();
		t.find('tr.'+o.dialogProductLine).each(function(){
			var r = self.$(this);
			var p = r.find('.price').text();
			var q = r.find('input[name="quantity"]').first().spinner('value');
			r.find('.sum').text( (parseFloat(p)*parseInt(q)).toFixed(2) );
		});
	},
	
	_showOrderPanel: function() {
		var self = this, o = this.options;
		
		if( Client && Client.isLogged() ) {
			this.$('.shop-order-code-panel', o.dialog).hide();
		}
		else {
			this.$('.shop-order-code-panel', o.dialog).show();
		}
		
		this.$(o.dialogBtnOrder, o.dialog).hide();
		
		var h = this.dialogContentHeight;
		var hTop = parseInt(h*0.42);
		var hBot = h - hTop;
		this.$(o.dialogContent, o.dialog).animate({
			height: hTop
		},'fast',function(){
		});
		
		this.$(o.dialogContent2, o.dialog).height(0).animate({
			height: hBot
		},'fast',function(){
			self.$(o.dialogBtnOrderSave, o.dialog).show();
			if( Client && Client.isLogged() ) {
				if( !self.$(o.form.name, o.dialog).val() ) self.$(o.form.name, o.dialog).val( Client.firstname +' '+ Client.lastname );
				if( !self.$(o.form.phone, o.dialog).val() ) self.$(o.form.phone, o.dialog).val( Client.phone );
				if( !self.$(o.form.email, o.dialog).val() ) self.$(o.form.email, o.dialog).val( Client.email );
			}
			
			self.$(o.form.name, o.dialog).focus();
			var loader = '/app/views/images/loader1.gif';
			
			var img = self.$(o.form.protectImage, o.dialog).first();
			img.attr( 'src', loader );
			img.attr( 'src', '/shop/get_order_protect_image_new?'+ Tools.getTimestamp() );
			var reload = self.$(o.form.reload, o.dialog).first();
			reload.click(function(){
				img.attr( 'src', loader );
				img.attr( 'src', '/shop/get_order_protect_image_new?'+ Tools.getTimestamp() );
			});
		});
	},
	
	_showDialogLoader: function() {
		var self = this, o = this.options;
		this.$(o.dialogLoader, o.dialog).show();
	},
	_hideDialogLoader: function() {
		var self = this, o = this.options;
		this.$(o.dialogLoader, o.dialog).hide();
	},
	
	_formatPrices: function( price ) {
		var rate = Site.CurrencyUSD.Rate;
		var retval = (parseFloat(price) * rate).toFixed(2);
		return( retval );
	},
};

var ShopComparision = {
	$: jQuery,
	
	options: {
		button: '.btn-compare',
		link: '.compare-link',
		counter: '.compare-counter',
		
		dialog: '#dlg-comparision-container',
		dialogContent: '#dlg-content',
		dialogLoader: '.dlg-footer-loader',
		
		dialogFooter: '#dlg-footer',
		dialogBtnClose: '.dlg-btn-close',
		dialogBtnDeleteAll: '.dlg-btn-delete-all'
	},
	
	data: [],
	
	init: function() {
		var self = this, o = this.options;
		
		Events.subscribe( '/Shop/Comparision/Add', function( id, cb ){
			self.add( id, cb );
		});
		Events.subscribe( '/Shop/Comparision/Del', function( id ){
			self.del( id );
		});
		
		this.$(o.button).click(function(){
			self.open();
		});
		this.$(o.link).click(function(){
			self.open();
		});
		
		this.load();
		this._initDialog();
	},
	
	_initDialog: function() {
		var self = this, o = this.options;
		
		this.$(o.dialog).dialog({
			autoOpen: false,
			resizable: true,
			minHeight: 560,
			minWidth: 860,
			modal: true,
			open: function(event, ui){
				self._applyDialog();
			}
		});
		
		this.$(o.dialogBtnClose, o.dialog).click(function(){
			self.close();
		});
		
		this.$(o.dialogBtnDeleteAll, o.dialog).click(function(){
			self.deleteAll();
		});
	},
	
	load: function() {
		var self = this, o = this.options;
		
		this.$.ajax({
			url: '/shop/json_get_comparision',
			data: {},
			dataType: 'json',
			type: 'get',
			success: function(data) {
				self.data = data;
				self._applyCounter( data );
			}
		});
	},
	
	/*
		cb(true|false) - function to run after receiving ajax response
	*/
	add: function( id, cb ) {
		var self = this, o = this.options;
		
		this.$.ajax({
			url: '/shop/json_add_to_comparision',
			data: {
				id: id
			},
			dataType: 'json',
			type: 'get',
			success: function(data) {
				self.data = data;
				self._applyCounter( data );
				if( data.item.added_updated_id > 0 ) {
					Alert.message( 'Товар добавлен в таблицу сравнения' );
					if( typeof(cb)=='function' ) cb( true );
				}
				else {
					Alert.error( 'Товар не добавлен в таблицу сравнения. <br/>В таблице находятся товары из другой категории, <br/>пожалуйста очистьте таблицу сравнения вначале.' );
					if( typeof(cb)=='function' ) cb( false );
				}
			}
		});
	},
	
	del: function( id ) {
		var self = this, o = this.options;
		
		this._showDialogLoader();
		this.$.ajax({
			url: '/shop/json_delete_in_comparision',
			data: {
				id: id
			},
			dataType: 'json',
			type: 'get',
			success: function(data) {
				self.data = data;
				self._applyCounter( data );
				self._applyDialog();
				Events.publish( '/Shop/Category/LoadProducts' );
				Events.publish( '/Shop/Product/ApplyComparision', [id, false] );
				if( data.item.added_updated_id > 0 ) {
					//msg updated/deleted
				}
				else {
				}
				self._hideDialogLoader();
			}
		});
	},
	
	deleteAll: function() {
		var self = this, o = this.options;
		
		this._showDialogLoader();
		this.$.ajax({
			url: '/shop/json_delete_all_in_comparision',
			data: {},
			dataType: 'json',
			type: 'get',
			success: function(data) {
				self.data = data;
				self._applyCounter( data );
				self._hideDialogLoader();
				self._applyDialog();
				self.close();
				Events.publish( '/Shop/Category/LoadProducts' );
				Events.publish( '/Shop/Product/ApplyComparision', [-1, false] );
			}
		});
	},
	
	open: function() {
		var self = this, o = this.options;
		
		this.$(o.dialog).dialog('open');
	},
	
	close: function() {
		var self = this, o = this.options;
		this.$(o.dialog).dialog('close');
	},
	
	_applyDialog: function() {
		var self = this, o = this.options;
		
		var dialog = this.$(o.dialog);
		var c = dialog.find( o.dialogContent );
		c.empty();
		var f = this.data.item.features;
		if( f.length > 0 ) {
			var t = this.$('<table>').addClass('comparision-features').attr({
				cellspacing: 0,
				cellpadding: 0
			});
			t.append('<tr><th class="comparision-product-image">&nbsp;</th></tr>');//image
			t.append('<tr><th>&nbsp;</th></tr>');//product name
			for( var i=0; i<f.length; i++ ) {
				var fr = this.$('<tr>');
				var th = this.$('<th>');
				if( (i+1)%2 == 0 ) th.addClass('even'); else th.addClass('uneven');
				th.html( f[i].name );
				fr.append( th );
				t.append( fr );
			}
			c.append( t );
			
			var products = this.$('<div>');
			for( var i=0; i<this.data.items.length; i++ ) {
				var p = this.data.items[i];
				var image = this.$('<div>').addClass('comparision-product-image').html('<img src="'+ p.image +'"/>');
				var name = this.$('<div>').addClass('comparision-product-name').html( p.product_name );
				var removeLink = this.$('<a>').addClass('remove-link');
				removeLink.attr( 'rel', p.product_id );
				removeLink.click(function(){
					var e = self.$(this);
					var id = e.attr('rel');
					self.del( id );
				});
				name.append( removeLink );
				var product = this.$('<div>').addClass('comparision-product');
				product.append( image );
				product.append( name );
				for( var j=0; j<f.length; j++ ) {
					var row = this.$('<div>');
					if( (j+1)%2 == 0 ) row.addClass('even'); else row.addClass('uneven');
					row.html( this._productGetFeatureValue( p, f[j].id ) );
					
					product.append( row );
				}
				products.append( product );
			}
			
			var tProducts = this.$('<table cellspacing="0" cellpadding="0"><tr></tr></table>');
			products.find('.comparision-product').appendTo( tProducts.find('tr') );
			tProducts.find('.comparision-product').wrap('<td>');
			c.append( this.$('<div>').addClass('comparision-products').append(tProducts) );
		}
		else {
			c.append( this.$('<div>').html('Нет характеристик, предназначенных для сравнения.') );
		}
	},
	
	_productGetFeatureValue: function( product, feature_id ) {
		var retval = 'n/a';
		
		for( var i=0; i<product.feature_values.length; i++ ) {
			if( product.feature_values[i].feature_id == feature_id ) {
				retval = product.feature_values[i].name;
				break;
			}
		}
		
		return( retval );
	},
	
	_applyCounter: function( data ) {
		var self = this, o = this.options;
		
		if( data && data.items && data.items.length >= 0 ) {
			this.$(o.counter).html( data.items.length );
		}
	},
	
	_showDialogLoader: function() {
		var self = this, o = this.options;
		this.$(o.dialogLoader, o.dialog).show();
	},
	_hideDialogLoader: function() {
		var self = this, o = this.options;
		this.$(o.dialogLoader, o.dialog).hide();
	}
};

var ShopProduct = {
	$: jQuery,
	
	options: {
		container: '.shop-product-container',
		productInfo: '.product-info',
		btnBuyNow: '.btn-buy-now',
		btnAddToCompare: '.btn-add-to-compare',
		btnAddedToCompare: '.btn-added-to-compare'
	},
	
	id: 0,
	
	init: function() {
		var self=this, o=this.options;
		
		if( this.$(o.container).length == 0 ) return;
		
		this.id = parseInt( this.$(o.container).attr('rel') );
		
		this.$(o.btnAddToCompare, o.container).click(function(){
			var i = self.$(this);
			var id = i.attr('rel');
			Events.publish( '/Shop/Comparision/Add', [id, function(result) {
				self._applyComparision(id, result);
			}] );
		});
		
		Events.subscribe( '/Shop/Product/ApplyComparision', function(id, result) {
			self._applyComparision(id, result);
		} );
		
		this.$(o.productInfo+'.active '+ o.btnBuyNow, o.container).click(function(){
			var i = self.$(this);
			var id = i.attr('rel');
			Events.publish( '/Shop/Trash/Add', [id,1] );
		});
		
		this.$(o.productInfo+'.inactive '+ o.btnBuyNow, o.container).click(function(){
			var i = self.$(this);
			var id = i.attr('rel');
			Events.publish( '/RequestProduct/Show', [{id:id}] );
		});
	},
	
	_applyComparision: function( id, result ) {
		var self=this, o=this.options;
		
		id = parseInt( id );
		if( id==-1 || id==this.id ) {
			if( result ) {
				this.$(o.btnAddToCompare, o.container).hide();
				this.$(o.btnAddedToCompare, o.container).show();
			}
			else {
				this.$(o.btnAddToCompare, o.container).show();
				this.$(o.btnAddedToCompare, o.container).hide();
			}
		}
	}
};

var RequestProduct = {
	$: jQuery,
	
	options: {
		dialog: '#dlg-request-product-container',
		dialogContent: '#dlg-content',
		dialogLoader: '.dlg-footer-loader',
		
		protectImage: 'img.protect_image',
		reload: '.btn-reload',
		form: {
			name: 'input[name="name"]',
			phone: 'input[name="phone"]',
			email: 'input[name="email"]',
			comment: 'input[name="comment"]',
			protect_code: 'input[name="protect_code"]'
		},
		
		dialogFooter: '#dlg-footer',
		dialogBtnClose: '.dlg-btn-close',
		dialogBtnSend: '.dlg-btn-send'
	},
	
	info: {
		id: 0 //product ID
	},
	
	init: function() {
		var self = this, o = this.options;
		
        this.$(o.dialog).dialog({
			autoOpen: false,
			resizable: true,
			minHeight: 160,
			minWidth: 380,
			modal: true,
			open: function(event, ui){
				self._applyDialog();
			}
		});
		
		this.$(o.dialogBtnClose, o.dialog).click(function(){
			self._closeDlg();
		});
		
		Events.subscribe( '/RequestProduct/Show', function(info) {
			self.info = info;
			self._showDlg();
		});
	},
	
	_showDlg: function(){
		var o=this.options;
		this.$(o.dialog).dialog('open');
	},
	
	_closeDlg: function(){
		var o=this.options;
		this.$(o.dialog).dialog("close");
	},
	
	_applyDialog: function() {
		var self=this, o=this.options;
		
		this._resetDialog();
		
		var img = this.$(o.protectImage,o.dialog).first();
		img.attr( 'src', '/shop/get_request_product_protect_image_new?'+ Tools.getTimestamp() );
		var reload = this.$(o.reload,o.dialog).first();
		reload.click(function(){
			img.attr( 'src', '/app/views/images/loader1.gif' );
			img.attr( 'src', '/shop/get_request_product_protect_image_new?'+ Tools.getTimestamp() );
		});
		
		this.$.mask.definitions['9'] = '';
		this.$.mask.definitions['d'] = '[0-9]';
		this.$(o.form.phone,o.dialog).mask(' ddddddddd?d');
		this.$(o.form.email,o.dialog).keyup(function(evnt){
			var e = self.$(this);
			e.val( Tools.formatEmail(e.val()) );
		});
		
		this.$(o.dialogBtnSend, o.dialog).click(function(){
			self.send();
		});
	},
	
	_resetDialog: function() {
		var self=this, o=this.options;
		
		/*for( var f in o.form ) {
			this.$(o.form[f], o.dialog).val('');
		}*/
		this.$(o.form.protect_code, o.dialog).val('');
	},
	
	send: function() {
		var self=this, o=this.options;
		
		if( this._checkForm() ) {
			this._showDialogLoader();
			var fv = this._getFormItems();
			$.ajax({
				url: '/shop/json_request_product',
				data: {
					id: self.info.id,
					name: fv['name'].val,
					phone: fv['phone'].val,
					email: fv['email'].val,
					comment: fv['comment'].val,
					protect_code: fv['protect_code'].val
				},
				dataType: 'json',
				type: 'get',
				success: function(data) {
					var r = data.result;
					if( r == 1 ) {
						self._closeDlg();
						//TODO: alert?
					}
					else if( r == -1 ) Alert.error('Ошибка параметров.<br/>Внимательно проверьте поля формы.');
					else if( r == -2 ) Alert.error('Ошибка проверочного кода.<br/>Вы можете обновить код, нажав кнопку справа от картинки с кодом.');
					self._hideDialogLoader();
				}
			});
		}
	},
	
	_checkForm: function() {
		var self=this, o=this.options;
		var retval = true;
		
		var fv = this._getFormItems();
		
		if( !fv['name'].val ) {
			fv['name'].obj.addClass('inp-error');
			retval = false;
		}
		
		if( !fv['phone'].val ) {
			fv['phone'].obj.addClass('inp-error');
			retval = false;
		}
		else fv['phone'].obj.removeClass('inp-error');
		
		/*if( !fv['email'].val ) {
			fv['email'].obj.addClass('inp-error');
			retval = false;
		}
		else fv['email'].obj.removeClass('inp-error');*/
		
		/*if( !fv['comment'].val ) {
			fv['comment'].obj.addClass('inp-error');
			retval = false;
		}
		else fv['comment'].obj.removeClass('inp-error');*/
		
		if( !fv['protect_code'].val ) {
			fv['protect_code'].obj.addClass('inp-error');
			retval = false;
		}
		else fv['protect_code'].obj.removeClass('inp-error');
		
		return( retval );
	},
	
	_getFormItems: function() {
		var self=this, o=this.options;
		var retval = [];
		
		var name = this.$(o.form.name,o.dialog);
		var nameVal = this.$.trim( name.val() );
		retval['name'] = {
			obj: name,
			val: nameVal
		};
		
		var email = this.$(o.form.email,o.dialog);
		var emailVal = this.$.trim( email.val() );
		retval['email'] = {
			obj: email,
			val: Tools.formatEmail(emailVal)
		};
		
		var phone = this.$(o.form.phone,o.dialog);
		var phoneVal = this.$.trim( phone.val() );
		retval['phone'] = {
			obj: phone,
			val: Tools.formatPhone(phoneVal)
		};
		
		var comment = this.$(o.form.comment,o.dialog);
		var commentVal = this.$.trim( comment.val() );
		retval['comment'] = {
			obj: comment,
			val: commentVal
		};
		
		var protect_code = this.$(o.form.protect_code,o.dialog);
		var protect_codeVal = this.$.trim( protect_code.val() );
		retval['protect_code'] = {
			obj: protect_code,
			val: protect_codeVal
		};
		
		return( retval );
	},
	
	_showDialogLoader: function() {
		var self = this, o = this.options;
		this.$(o.dialogLoader, o.dialog).show();
	},
	_hideDialogLoader: function() {
		var self = this, o = this.options;
		this.$(o.dialogLoader, o.dialog).hide();
	}
};

$j(function(){
	Shop.init();
	ShopTrash.init();
	ShopComparision.init();
	ShopProduct.init();
	RequestProduct.init();
});

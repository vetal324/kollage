<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
                
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout_shop.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:template match="data">
    <h3 class="page_title"><xsl:value-of select="$locale/upc/title/text()" disable-output-escaping="yes"/></h3>
    
    <xsl:if test="/content/global/client/id = 0">
        <xsl:call-template name="login_form2"/>
    </xsl:if>
    
    <xsl:if test="/content/global/client/id > 0">
        <!-- UPC FORM -->
            <div class="p1">
                <xsl:value-of select="OrderID"/><br/>
                <xsl:value-of select="message"/>
            </div>
        <!-- UPC FORM - END -->
    </xsl:if>
    
    <!-- <xsl:call-template name="new_products"/> -->
    <xsl:call-template name="best_products"/>
    
</xsl:template>

</xsl:stylesheet>

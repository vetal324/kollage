<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:exsl="http://exslt.org/common"
                extension-element-prefixes="exsl"
                version="1.0">
                
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout_shop.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:template match="data">
    <h3 class="products_title">
        <table cellspacing="0" cellpadding="0" with="100%"><tr>
        <td width="90%"><xsl:value-of select="$locale/shop/order/title/text()" disable-output-escaping="yes"/></td>
        <td width="*" align="right">
            <xsl:call-template name="shop_trash">
                <xsl:with-param name="back_url" select="back_url"/>
            </xsl:call-template>
        </td>
        </tr></table>
        
    </h3>
    
    <xsl:if test="/content/global/client/id = 0">
        <xsl:call-template name="login_form2"/>
    </xsl:if>
    
    <xsl:if test="/content/global/client/id > 0">
        <div><xsl:value-of select="order/paymentway/description" disable-output-escaping="yes"/></div>
    </xsl:if>
    
    <!-- <xsl:call-template name="new_products"/> -->
    <xsl:call-template name="best_products"/>
    
</xsl:template>

<xsl:template match="shop_trash">
    <xsl:variable name="tmpTotal">
        <xsl:for-each select="./*">
            <xsl:choose>
                <xsl:when test="product/nice_price_active = 1">
                    <item>
                        <xsl:value-of select="number(product/nice_price)* /content/global/currency/exchange_rate * @quantity"/>
                    </item>
                </xsl:when>
                <xsl:otherwise>
                    <item>
                        <xsl:choose>
                            <xsl:when test="/content/global/client/type_id = 2">
                                <xsl:value-of select="number(product/price_opt)* /content/global/currency/exchange_rate * @quantity"/>
                            </xsl:when>
                            <xsl:when test="/content/global/client/type_id = 3">
                                <xsl:value-of select="number(product/price_opt2)* /content/global/currency/exchange_rate * @quantity"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="number(product/price)* /content/global/currency/exchange_rate * @quantity"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </item>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:variable>
    <xsl:variable name="myTotal" select="exsl:node-set($tmpTotal)/*"/>
    <xsl:if test="$myTotal > 0"><xsl:value-of select="sum($myTotal)"/></xsl:if>
</xsl:template>

<xsl:template match="item">
    <tr>
        <td><xsl:value-of select="position()"/>.</td>
        <td><xsl:value-of select="product/name"/></td>
        <td>
            <xsl:choose>
                <xsl:when test="product/nice_price_active = 1">
                    <item>
                        <xsl:value-of select="number(product/nice_price)* /content/global/currency/exchange_rate"/>
                    </item>
                </xsl:when>
                <xsl:otherwise>
                    <item>
                        <xsl:choose>
                            <xsl:when test="/content/global/client/type_id = 2">
                                <xsl:value-of select="number(product/price_opt)* /content/global/currency/exchange_rate"/>
                            </xsl:when>
                            <xsl:when test="/content/global/client/type_id = 3">
                                <xsl:value-of select="number(product/price_opt2)* /content/global/currency/exchange_rate"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="number(product/price)* /content/global/currency/exchange_rate"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </item>
                </xsl:otherwise>
            </xsl:choose>
        </td>
        <td align="center"><xsl:value-of select="@quantity"/></td>
        <td>
            <xsl:choose>
                <xsl:when test="product/nice_price_active = 1">
                    <item>
                        <xsl:value-of select="number(product/nice_price)* /content/global/currency/exchange_rate * @quantity"/>
                    </item>
                </xsl:when>
                <xsl:otherwise>
                    <item>
                        <xsl:choose>
                            <xsl:when test="/content/global/client/type_id = 2">
                                <xsl:value-of select="number(product/price_opt)* /content/global/currency/exchange_rate * @quantity"/>
                            </xsl:when>
                            <xsl:when test="/content/global/client/type_id = 3">
                                <xsl:value-of select="number(product/price_opt2)* /content/global/currency/exchange_rate * @quantity"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="number(product/price)* /content/global/currency/exchange_rate * @quantity"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </item>
                </xsl:otherwise>
            </xsl:choose>
        </td>
    </tr>
</xsl:template>

</xsl:stylesheet>

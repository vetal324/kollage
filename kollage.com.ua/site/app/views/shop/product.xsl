<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../layout.xsl" />
<xsl:include href="shop_layout.xsl" />

<xsl:template match="data">
	<xsl:call-template name="shop_layout"/>
	
	<script type="text/javascript">
	var Category = {
		Id: parseInt('<xsl:value-of select="category/id" disable-output-escaping="yes"/>'),
		Name: '<xsl:value-of select="category/name" disable-output-escaping="yes"/>'
	};
	var Prices = {
		exchangeRate: parseFloat('<xsl:value-of select="$exchange_rate"/>'),
	};
	</script>
	<xsl:variable name="name" select="product/name"/>
	<div class="shop">
	
		<div class="shop-top">
			<xsl:for-each select="cline/*">
				<xsl:if test="position() > 1">&#160;&#187;&#160;</xsl:if>
				<xsl:choose>
					<xsl:when test="position() = 1">
						<h1><a href="/shop/category/{id}"><xsl:value-of select="name"/></a></h1>
					</xsl:when>
					<xsl:when test="position() = 2">
						<h2><a href="/shop/category/{id}"><xsl:value-of select="name"/></a></h2>
					</xsl:when>
					<xsl:otherwise>
						<h3><xsl:value-of select="name"/></h3>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</div>
		
		<div class="shop-product-wrapper shop-product-container" rel="{product/id}">
			<div class="page_title"><h1><span class="red">&#8226; </span><xsl:value-of select="product/name"/></h1></div>
			
			<div class="shop-product-pictures">
				<xsl:if test="count(product/images/*) > 0">
					<xsl:variable name="first_picture" select="product/images/*[position()=1]"/>
					<a href="{$first_picture/path}{$first_picture/filename}" class="highslide" onclick="return hs.expand(this,{{captionText:'',headingText:'{$name}'}})">
						<img src="{$first_picture/path}{$first_picture/tiny}" border="0" title="{$first_picture/name}" alt="{$first_picture/name}"/>
					</a>
					<div class="panel-small">
						<xsl:for-each select="product/images/*">
							<xsl:if test="position() > 1">
								<div class="item">
									<a href="{path}{filename}" class="highslide" onclick="return hs.expand(this,{{captionText:'',headingText:'{$name}'}})">
										<img src="{path}{tiny}" border="0" title="{name}" alt="{name}" width="54"/>
									</a>
								</div>
							</xsl:if>
						</xsl:for-each>
					</div>
				</xsl:if>
				<div class="top-right">
					<xsl:choose>
						<xsl:when test="is_product_in_comparision = 1">
							<a title="{$locale/shop/btn_add_compare_tip}" rel="{product/id}" class="btn-add-to-compare vtip" style="display:none" onclick="return false" href="#"></a>
							<a title="{$locale/shop/btn_added_compare_tip}" rel="{product/id}" class="btn-added-to-compare vtip" onclick="return false" href="#"></a>
						</xsl:when>
						<xsl:otherwise>
							<a title="{$locale/shop/btn_add_compare_tip}" rel="{product/id}" class="btn-add-to-compare vtip" onclick="return false" href="#"></a>
							<a title="{$locale/shop/btn_added_compare_tip}" rel="{product/id}" class="btn-added-to-compare vtip" style="display:none" onclick="return false" href="#"></a>
						</xsl:otherwise>
					</xsl:choose>
				</div>
			</div>
			
			<xsl:variable name="active_style">
				<xsl:choose>
					<xsl:when test="product/quantity_local&gt;0 or product/quantity_external&gt;0">active</xsl:when>
					<xsl:otherwise>inactive</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			
			<div class="product-info {$active_style}">
				<div class="price">
				<xsl:choose>
					<xsl:when test="/content/global/client/type_id = 2">
						<xsl:value-of select="format-number( product/price_opt*$exchange_rate,'0.00')"/>&#160;<xsl:value-of select="/content/global/currency/symbol" disable-output-escaping="yes"/>
					</xsl:when>
					<xsl:when test="/content/global/client/type_id = 3">
						<xsl:value-of select="format-number( product/price_opt2*$exchange_rate,'0.00')"/>&#160;<xsl:value-of select="/content/global/currency/symbol" disable-output-escaping="yes"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="format-number( product/price*$exchange_rate,'0.00')"/>&#160;<xsl:value-of select="/content/global/currency/symbol" disable-output-escaping="yes"/>
					</xsl:otherwise>
				</xsl:choose>
				</div>
				<div class="availability-yes"><xsl:value-of select="$locale/shop/availability" disable-output-escaping="yes"/>: <span class="green"><xsl:value-of select="$locale/shop/availability_yes" disable-output-escaping="yes"/></span></div>
				<div class="availability-no"><xsl:value-of select="$locale/shop/availability" disable-output-escaping="yes"/>: <span class="red"><xsl:value-of select="$locale/shop/availability_no" disable-output-escaping="yes"/></span></div>
				<div class="hr"></div>
				<p>
					<xsl:for-each select="product/features/*[@show_in_short_description=1]">
						<xsl:if test="position() &gt; 1"> / </xsl:if>
						
						<xsl:value-of select="name" disable-output-escaping="yes"/>: <xsl:value-of select="value_name" disable-output-escaping="yes"/>
					</xsl:for-each>
				</p>
				
				<div class="product-button">
					<div class="btn-buy-now" rel="{product/id}"></div>
				</div>
				
				<p class="p-clear">&#160;</p>
				<div class="hr"></div>
				
				<xsl:if test="count(product/features/*)>0 or count(product/individual_features/*)>0">
					<h2><xsl:value-of select="$locale/shop/details/features_title" disable-output-escaping="yes"/></h2>
					<table class="product-features">
					<xsl:for-each select="product/features/*">
						<xsl:variable name="row_style">
							<xsl:choose>
								<xsl:when test="position() mod 2 = 0">row2</xsl:when>
								<xsl:otherwise>row1</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<tr class="{$row_style}">
							<th><xsl:value-of select="name" disable-output-escaping="yes"/></th>
							<td><xsl:value-of select="value_name" disable-output-escaping="yes"/></td>
						</tr>
					</xsl:for-each>
					<xsl:variable name="features_count" select="count(product/features/*)"/>
					<xsl:for-each select="product/individual_features/*">
						<xsl:variable name="row_style">
							<xsl:choose>
								<xsl:when test="$features_count mod 2 = 0">
									<xsl:choose>
										<xsl:when test="position() mod 2 = 0">row2</xsl:when>
										<xsl:otherwise>row1</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:otherwise>
									<xsl:choose>
										<xsl:when test="position() mod 2 = 0">row1</xsl:when>
										<xsl:otherwise>row2</xsl:otherwise>
									</xsl:choose>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<tr class="{$row_style}">
							<th><xsl:value-of select="individual_name" disable-output-escaping="yes"/></th>
							<td><xsl:value-of select="individual_value" disable-output-escaping="yes"/></td>
						</tr>
					</xsl:for-each>
					</table>
				</xsl:if>
			</div>
			
			<div class="top-right">
				<table class="trash-icon"><tr>
					<td><a class="btn-trash" onclick="return false" href="#"></a></td>
					<td><span><a title="{$locale/shop/btn_trash_tip}" class="trash-link vtip" href="#" onclick="return(false)"><xsl:value-of select="$locale/shop/trash_icon_text" disable-output-escaping="yes"/></a><span class="red2"><b> (<span class="trash-counter">0</span>)</b></span></span></td>
				</tr></table>
				<table class="compare-icon"><tr>
					<td><a class="btn-compare" onclick="return false" href="#"></a></td>
					<td><span><a title="{$locale/shop/btn_compare_tip}" class="compare-link vtip" href="#" onclick="return(false)"><xsl:value-of select="$locale/shop/compare_icon_text" disable-output-escaping="yes"/></a><span class="red2"><b> (<span class="compare-counter">0</span>)</b></span></span></td>
				</tr></table>
			</div>
		</div>
	</div>
	
	<script type="text/javascript" src="{$views_path}/{$controller_name}/functions.js"></script>
</xsl:template>

</xsl:stylesheet>

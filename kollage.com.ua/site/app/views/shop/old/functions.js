
function shop_trash_add( id, quantity )
{
    if( quantity > 0 )
    {
        d = new Date();
        JsHttpRequest.query(
            '?shop.shop_trash_add&amp;date='+ d.getTime(),
            {
                'id': id,
                'quantity': quantity
            },
            function( result, txt )
            {
                if( result['shop_trash_update']==1 )
                {
                    get_shop_trash();
                    ShowAddingResult( 1 );
                }
                else
                {
                    //ShowAddingResult( 2 );
                }
            }
        );
    }
}

function ShowAddingResult( result )
{
    ww = GetWindowSize()[0];
    wh = GetWindowSize()[1];
    
    var w = 300, h = 160;
    b = document.getElementsByTagName('body')[0];
    
    bg = document.createElement('div');
    bg.id = "d_bg";
    bg.style.left = "0px";
    bg.style.top = "0px";
    
    bw = GetObjectSize(document.body)[0];
    bh = GetObjectSize(document.body)[1];
    bg.style.width = (bw>ww)? bw:ww;
    bg.style.height = (bh>wh)? bh:wh;
        
    bg.style.position = "absolute";
    bg.style.zIndex = "50";
    bg.style.display = "block";
    bg.style.backgroundImage = "url(/app/views/images/opacity1.png)";
    
    b.appendChild( bg );
    HideAllSelectboxes( $('order_params') );
    
    var obj = document.createElement('div');
    obj.id = "d_content";
    obj.style.left = (ww - w) / 2;
    obj.style.top = GetWindowScroll()[1] + (wh - h) / 2;
    obj.style.width = w + 'px';
    obj.style.height = h + 'px';
    obj.style.position = "absolute";
    obj.style.zIndex = "100";
    obj.style.display = "block";
    obj.style.borderWidth = "2px";
    obj.style.borderStyle = "solid";
    obj.style.borderColor = "#333333";
    obj.style.overflow = "hidden";
    obj.innerHTML = '<div style="padding:10px;background-color:#ffffff;width:100%;height:100%"><p style="padding-top:20px;padding-bottom:30px">Товар был добавлен в корзину.</p><input type="button" class="button" value="Закрыть" onclick="document.body.removeChild( document.getElementById(\'d_bg\') );document.body.removeChild( document.getElementById(\'d_content\') )"/>&#160;<input type="button" class="button" value="Просмотр корзины" onclick="document.location=\'?shop.trash\'"/></div>';
    
    b.appendChild( obj );
}

function toggleBrands(e)
{
    if (!e) var e = window.event;
    var posx = 0, posy = 0;
    if (e.pageX || e.pageY) 
    {
        posx = e.pageX; posy = e.pageY;
    }
    else if (e.clientX || e.clientY)
    {
        posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
        posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
    }

    $("brands_panel").setStyle({
        top: posy +"px",
        left: posx +"px"
    });
    
    $("brands_panel").toggle();
}

function getBodyScrollTop()
{
  return self.pageYOffset || (document.documentElement && document.documentElement.scrollTop) || (document.body && document.body.scrollTop);
}

function getBodyScrollLeft()
{
  return self.pageXOffset || (document.documentElement && document.documentElement.scrollLeft) || (document.body && document.body.scrollLeft);
}

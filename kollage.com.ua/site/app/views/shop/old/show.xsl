<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout.xsl" />

<xsl:template match="data">
	<script type="text/javascript" src="{$views_path}/{$controller_name}/functions.js"></script>
	
	<xsl:variable name="brands_str">
		<xsl:for-each select="brands/*"><xsl:if test="@selected = 'yes'">&amp;brand[]=<xsl:value-of select="@id"/></xsl:if></xsl:for-each>
	</xsl:variable>

	<h1 class="products_title">
		<table cellspacing="0" cellpadding="0"><tr>
		<td width="90%">
		<xsl:choose>
			<xsl:when test="design/@type = 'new'">
				<xsl:value-of select="$locale/shop/title/text()" disable-output-escaping="yes"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:for-each select="cline/*">
					<a href="?{$controller_name}.show&amp;category_id={id}{$brands_str}"><xsl:value-of select="name"/></a>&#160;&#187;&#160;
				</xsl:for-each>
				<!-- <xsl:value-of select="category/name"/> -->
			</xsl:otherwise>
		</xsl:choose>
		</td>
		
		<!--<td width="*" align="right">
			<xsl:call-template name="shop_trash">
				<xsl:with-param name="back_url" select="back_url"/>
			</xsl:call-template>
		</td>-->
		</tr></table>
		
	</h1>
	<xsl:if test="design/@type = 'category' and category/id > 0 and count(brands/*) > 0">
	<form action="?{$controller_name}.show" method="get" style="border:1px solid white">
		<input type="hidden" name="{$controller_name}.show" value=""/>
		<input type="hidden" name="category_id" value="{category/id}"/>
		<input type="hidden" name="page" value="1"/>
		<div class="brands_selection">
			<div style="float:left;clear:left;text-decoration:underline;padding-right:10px;cursor:pointer;padding-top:3px" onclick="toggleBrands(event)"><b><xsl:value-of select="$locale/shop/list/brands/text()" disable-output-escaping="yes"/></b></div>
			<div id="brands_panel" style="width:580px;position:absolute;top:0;left:0;background-color:#fff;border:2px solid #333333"><xsl:apply-templates select="brands/*"/><div><a href="#" onclick="toggleBrands(event);return(false)">������� ������</a></div><input type="submit" class="button" value="{$locale/shop/list/brands_submit/text()}"/></div>
			<script>$("brands_panel").hide()</script>
			<div style="float:left;clear:none">
			<xsl:choose>
				<xsl:when test="products/@show_all = 2 or products/@show_all = -1">
					<input type="radio" name="show_all" value="1" id="show_all_1"/>&#160;<label for="show_all_1"><b><xsl:value-of select="$locale/shop/list/show_all/text()" disable-output-escaping="yes"/></b></label>
					<input type="radio" name="show_all" value="2" id="show_all_2" checked="yes"/>&#160;<label for="show_all_2"><b><xsl:value-of select="$locale/shop/list/show_exist/text()" disable-output-escaping="yes"/></b></label>&#160;
				</xsl:when>
				<xsl:otherwise>
					<input type="radio" name="show_all" value="1" id="show_all_1" checked="yes"/>&#160;<label for="show_all_1"><b><xsl:value-of select="$locale/shop/list/show_all/text()" disable-output-escaping="yes"/></b></label>
					<input type="radio" name="show_all" value="2" id="show_all_2"/>&#160;<label for="show_all_2"><b><xsl:value-of select="$locale/shop/list/show_exist/text()" disable-output-escaping="yes"/></b></label>&#160;
				</xsl:otherwise>
			</xsl:choose>
			<input type="submit" class="button" value="{$locale/shop/list/brands_submit/text()}"/></div>
		</div>
	</form>
	</xsl:if>
	
	<!-- PAGINATOR -->
	<div class="pagination" pages="{$pages}"><table cellspacing="0" cellpadding="0" border="0" width="100%"><tr>
	
	<td align="left">
	<xsl:if test="design/@type = 'category'">
		<select id="rows_per_page_top" onchange="document.location='?shop.show&amp;category_id={category/id}&amp;show_all={products/@show_all}&amp;{$brands_str}&amp;rows_per_page='+this.options[this.selectedIndex].value">
		<xsl:variable name="selected" select="$rows_per_page"/>
			<xsl:call-template name="gen-option">
				<xsl:with-param name="value">8</xsl:with-param>
				<xsl:with-param name="title">8</xsl:with-param>
				<xsl:with-param name="selected" select="$selected"/>
			</xsl:call-template>
			<xsl:call-template name="gen-option">
				<xsl:with-param name="value">12</xsl:with-param>
				<xsl:with-param name="title">12</xsl:with-param>
				<xsl:with-param name="selected" select="$selected"/>
			</xsl:call-template>
			<xsl:call-template name="gen-option">
				<xsl:with-param name="value">20</xsl:with-param>
				<xsl:with-param name="title">20</xsl:with-param>
				<xsl:with-param name="selected" select="$selected"/>
			</xsl:call-template>
			<xsl:call-template name="gen-option">
				<xsl:with-param name="value">32</xsl:with-param>
				<xsl:with-param name="title">32</xsl:with-param>
				<xsl:with-param name="selected" select="$selected"/>
			</xsl:call-template>
			<xsl:call-template name="gen-option">
				<xsl:with-param name="value">1000000</xsl:with-param>
				<xsl:with-param name="title"><xsl:value-of select="$locale/shop/list/all"/></xsl:with-param>
				<xsl:with-param name="selected" select="$selected"/>
			</xsl:call-template>
		</select>
		<span>&#160;<xsl:value-of select="$locale/shop/list/rows_per_page"/></span>
	</xsl:if>
	</td>
	<td align="right">
	<xsl:if test="$pages > 1">
		<xsl:choose>
			<xsl:when test="design/@type = 'new'">
		<xsl:call-template name="pagination">
			<xsl:with-param name="page"><xsl:value-of select="$page"/></xsl:with-param>
			<xsl:with-param name="stop"><xsl:value-of select="$pages"/></xsl:with-param>
			<xsl:with-param name="url">?<xsl:value-of select="$controller_name"/>.show</xsl:with-param>
		</xsl:call-template>
		/ <a href="?{$controller_name}.show&amp;page={$page - 1}"><xsl:value-of select="$locale/common/link_prev/text()" disable-output-escaping="yes"/></a> | <a href="?{$controller_name}.show&amp;page={$page + 1}"><xsl:value-of select="$locale/common/link_next/text()" disable-output-escaping="yes"/></a>
			</xsl:when>
			<xsl:otherwise>
		<xsl:call-template name="pagination">
			<xsl:with-param name="page"><xsl:value-of select="$page"/></xsl:with-param>
			<xsl:with-param name="stop"><xsl:value-of select="$pages"/></xsl:with-param>
			<xsl:with-param name="url">?<xsl:value-of select="$controller_name"/>.show&amp;category_id=<xsl:value-of select="category/id"/>&amp;show_all=<xsl:value-of select="products/@show_all"/><xsl:value-of select="$brands_str"/></xsl:with-param>
		</xsl:call-template>
		/ <a href="?{$controller_name}.show&amp;category_id={category/id}&amp;page={$page - 1}&amp;show_all={products/@show_all}{$brands_str}"><xsl:value-of select="$locale/common/link_prev/text()" disable-output-escaping="yes"/></a> | <a href="?{$controller_name}.show&amp;category_id={category/id}&amp;page={$page + 1}&amp;show_all={products/@show_all}{$brands_str}"><xsl:value-of select="$locale/common/link_next/text()" disable-output-escaping="yes"/></a>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:if>
	</td>
		
	</tr></table></div>
	<!-- PAGINATOR - END -->
	
	<div class="matrix"><xsl:apply-templates select="products/*"/></div>
	
	<!-- PAGINATOR -->
	<div class="pagination" pages="{$pages}"><nobr>
	<xsl:if test="$pages > 1">
	
		<xsl:choose>
			<xsl:when test="design/@type = 'new'">
		<xsl:call-template name="pagination">
			<xsl:with-param name="page"><xsl:value-of select="$page"/></xsl:with-param>
			<xsl:with-param name="stop"><xsl:value-of select="$pages"/></xsl:with-param>
			<xsl:with-param name="url">?<xsl:value-of select="$controller_name"/>.show</xsl:with-param>
		</xsl:call-template>
		/ <a href="?{$controller_name}.show&amp;page={$page - 1}"><xsl:value-of select="$locale/common/link_prev/text()" disable-output-escaping="yes"/></a> | <a href="?{$controller_name}.show&amp;page={$page + 1}"><xsl:value-of select="$locale/common/link_next/text()" disable-output-escaping="yes"/></a>
			</xsl:when>
			<xsl:otherwise>
		<xsl:call-template name="pagination">
			<xsl:with-param name="page"><xsl:value-of select="$page"/></xsl:with-param>
			<xsl:with-param name="stop"><xsl:value-of select="$pages"/></xsl:with-param>
			<xsl:with-param name="url">?<xsl:value-of select="$controller_name"/>.show&amp;category_id=<xsl:value-of select="category/id"/>&amp;show_all=<xsl:value-of select="products/@show_all"/><xsl:value-of select="$brands_str"/></xsl:with-param>
		</xsl:call-template>
		/ <a href="?{$controller_name}.show&amp;category_id={category/id}&amp;page={$page - 1}&amp;show_all={products/@show_all}{$brands_str}"><xsl:value-of select="$locale/common/link_prev/text()" disable-output-escaping="yes"/></a> | <a href="?{$controller_name}.show&amp;category_id={category/id}&amp;page={$page + 1}&amp;show_all={products/@show_all}{$brands_str}"><xsl:value-of select="$locale/common/link_next/text()" disable-output-escaping="yes"/></a>
			</xsl:otherwise>
		</xsl:choose>
		
	</xsl:if>
	</nobr></div>
	<!-- PAGINATOR - END -->
	
	<xsl:if test="design/@type = 'category' and category/id > 0">
		<!--<xsl:call-template name="new_products"/>-->
		<xsl:call-template name="best_products"/>
	</xsl:if>
</xsl:template>

<xsl:template match="feature">
<div>
	<b><xsl:value-of select="@name"/></b>:&#160;
	<xsl:apply-templates/>
</div>
</xsl:template>

<xsl:template match="feature/value">
	<xsl:choose>
		<xsl:when test="@selected = 'yes'"><input type="checkbox" checked='{@selected}' value="{@id}" id="feature_value_{@id}" name="feature_value[]"/></xsl:when>
		<xsl:otherwise><input type="checkbox" value="{@id}" id="feature_value_{@id}" name="feature_value[]"/></xsl:otherwise>
	</xsl:choose>
	<label for="feature_value_{@id}"><xsl:value-of select="@name"/></label>&#160;(<xsl:value-of select="@c"/>)&#160;
</xsl:template>

<xsl:template match="brand">
	<div style="float:left;clear:none;width:160px;height:20px;overflow:hidden;display:block;">
	<xsl:choose>
		<xsl:when test="@selected = 'yes'"><input type="checkbox" checked='{@selected}' value="{@id}" id="brand_{@id}" name="brand[]"/></xsl:when>
		<xsl:otherwise><input type="checkbox" value="{@id}" id="brand_{@id}" name="brand[]"/></xsl:otherwise>
	</xsl:choose>
	<label for="brand_{@id}"><xsl:value-of select="name"/>(<xsl:value-of select="@count_products"/>)</label>&#160;
	</div>
</xsl:template>

<xsl:template match="product">
	
	<xsl:variable name="brands_str">
		<xsl:for-each select="../../brands/*"><xsl:if test="@selected = 'yes'">&amp;brand[]=<xsl:value-of select="@id"/></xsl:if></xsl:for-each>
	</xsl:variable>
	
	<div class="cell">
		<table width="100%" height="100%">
			<tr>
				<td class="product_title"><xsl:value-of select="name" disable-output-escaping="yes"/></td>
			</tr>
			<tr>
				<td class="product" valign="top">
					<table cellspacing="0" cellpadding="0" width="100%" height="100%">
						<tr>
							<td height="10%" align="center">
								<xsl:choose>
									<xsl:when test="images/image/tiny != ''">
										<img src="{images/image/tiny}" border="0"/>
									</xsl:when>
									<xsl:otherwise>
										<img src="{$views_path}/images/empty.gif" width="180" height="124" border="0"/>
									</xsl:otherwise>
								</xsl:choose>
							</td>
						</tr>
						<tr>
							<td height="*" valign="top">
								<table width="100%" cellpadding="0" cellspacing="0" class="product_info" border="0">
									<tr>
									<th>
										<xsl:choose>
											<xsl:when test="nice_price_active = 1">
												<xsl:value-of select="format-number( nice_price*/content/global/currency/exchange_rate,'0.00')"/>&#160;<xsl:value-of select="/content/global/currency/symbol" disable-output-escaping="yes"/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:choose>
													<xsl:when test="/content/global/client/type_id = 2">
														<xsl:value-of select="format-number( price_opt*/content/global/currency/exchange_rate,'0.00')"/>&#160;<xsl:value-of select="/content/global/currency/symbol" disable-output-escaping="yes"/>
													</xsl:when>
													<xsl:when test="/content/global/client/type_id = 3">
														<xsl:value-of select="format-number( price_opt2*/content/global/currency/exchange_rate,'0.00')"/>&#160;<xsl:value-of select="/content/global/currency/symbol" disable-output-escaping="yes"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="format-number( price*/content/global/currency/exchange_rate,'0.00')"/>&#160;<xsl:value-of select="/content/global/currency/symbol" disable-output-escaping="yes"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:otherwise>
										</xsl:choose>
									</th>
									<td align="right"><a href="?{$controller_name}.product&amp;id={id}&amp;back_page={$page}{$brands_str}"><xsl:value-of select="$locale/shop/list/details" disable-output-escaping="yes"/></a></td>
									</tr>
									
									<xsl:if test="/content/global/client/type_id > 1 and number != ''">
									<tr>
										<td colspan="3">
											<table cellspacing="0" cellpadding="0" border="0"><tr>
												<td style="color:#e20011">
													<xsl:value-of select="$locale/shop/list/number/text()" disable-output-escaping="yes"/>
												</td>
												<td style="color:#e20011;padding-left:5px">
													<xsl:value-of select="number"/>
												</td>
											</tr></table>
										</td>
									</tr>
									</xsl:if>
									
									<tr>
										<td colspan="3" align="left">
											<table cellspacing="0" cellpadding="0" border="0" align="left" width="100%"><tr>
												<td style="color:black" align="left">
													<xsl:value-of select="$locale/shop/list/availability_local/text()" disable-output-escaping="yes"/>&#160;
													<xsl:choose>
														<xsl:when test="quantity_local > 0">
															<xsl:value-of select="$locale/common/exist/text()" disable-output-escaping="yes"/>
														</xsl:when>
														<xsl:otherwise>
															<xsl:value-of select="$locale/common/no/text()" disable-output-escaping="yes"/>
														</xsl:otherwise>
													</xsl:choose>
													<br/>
													<xsl:value-of select="$locale/shop/list/availability_external/text()" disable-output-escaping="yes"/>&#160;
													<xsl:choose>
														<xsl:when test="quantity_external > 0">
															<xsl:value-of select="$locale/common/exist/text()" disable-output-escaping="yes"/>
														</xsl:when>
														<xsl:otherwise>
															<xsl:value-of select="$locale/common/no/text()" disable-output-escaping="yes"/>
														</xsl:otherwise>
													</xsl:choose>
												</td>
												<td align="right" width="10%">
													<xsl:choose>
													<xsl:when test="quantity_local > 0 or quantity_external > 0">
														<table cellspacing="0" cellpadding="0"><tr>
															<td><input type="text" name="quantity{id}" id="quantity{id}" value="" maxlength="3" style="margin:0px;border:1px solid #333333;font-size:9px;width:27px"/></td>
															<td style="padding-left: 2px;"><img src="{$views_path}/images/trash2.gif" border="0" height="14" onclick="shop_trash_add({id},$('quantity{id}').value)" style="cursor:pointer"/></td>
														</tr></table>
													</xsl:when>
													<xsl:otherwise>
														&#160;
													</xsl:otherwise>
													</xsl:choose>
												</td>
											</tr></table>
										</td>
									</tr>
									<tr>
										<td colspan="3" align="left" style="padding-top:3px"><xsl:value-of select="ingress" disable-output-escaping="yes"/></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
</xsl:template>

</xsl:stylesheet>

<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
                
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />


<xsl:template match="/">
  <html>
    <head>
      <title><xsl:value-of select="$locale/common/title/text()" disable-output-escaping="yes"/></title>
      <link rel="stylesheet" href="{$views_path}/styles.css" />
    </head>
    <body>
    <table width="96%" height="100%" align="center" class="page" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td valign="top">
                <table width="100%" cellpadding="0" cellspacing="0" style="background: url({$views_path}/images/store_header_bg.jpg) center">
                    <tr>
                        <td width="17%"><img src="{$views_path}/images/logo2.png" style="margin: 6px 0 6px 6px;" alt="{$locale/common/logo_url_alt/text()}"/></td>
                        <td width="33%"><h3 class="compare"><xsl:value-of select="$locale/shop/compare/title/text()" disable-output-escaping="yes"/></h3></td>
                        <td width="50%" align="center" style="padding-top: 24px">
                            <form method="post" action="?{$controller_name}.compare2">
                                <input type="hidden" name="id" value="{/content/documents/data/product/id}"/>
                                <input type="hidden" name="ids" value="{/content/documents/data/ids}"/>
                                <select id="brand_id" onchange="Fill_product_id( document.getElementById('brand_id').options[document.getElementById('brand_id').selectedIndex].value )"></select>
                                <select name="add_id" id="product_id"></select>&#160;
                                <input type="submit" id="fsubmit" value="��������"/>
                            </form>
                            <script>
                                var brands_ids = {<xsl:value-of select="/content/documents/data/brands_js"/>};
                                Fill_brand_id();
                                Fill_product_id(document.getElementById('brand_id').options[document.getElementById('brand_id').selectedIndex].value);
                                function Fill_brand_id()
                                {
                                    var obj = document.getElementById('brand_id');
                                    obj.options.length = 0;
                                    i = 0;
                                    for( v in brands_ids )
                                    {
                                        obj.options[i] = new Option( brands_ids[v][0], v, false, false );
                                        i++;
                                    }
                                    
                                    /*
                                    if( obj.options.length == 0 )
                                    {
                                        obj.style.display = 'none';
                                        document.getElementById('fsubmit').style.display = 'none';
                                    }
                                    else
                                    {
                                        if( obj.style.display ) obj.style.display = 'box';
                                        if( document.getElementById('fsubmit').style.display ) document.getElementById('fsubmit').style.display = 'box';
                                    }
                                    */
                                }
                                function Fill_product_id( brand_id )
                                {
                                    var obj = document.getElementById('product_id');
                                    obj.options.length = 0;
                                    products = brands_ids[brand_id][1];
                                    i = 0;
                                    for( v in products )
                                    {
                                        obj.options[i] = new Option( products[v][0] +' '+ '('+ products[v][1] +' '+ products[v][2] +')' , v, false, false );
                                        i++;
                                    }
                                    
                                    /*
                                    if( obj.options.length == 0 )
                                    {
                                        obj.style.display = 'none';
                                        document.getElementById('fsubmit').style.display = 'none';
                                    }
                                    else
                                    {
                                        if( obj.style.display ) obj.style.display = 'box';
                                        if( document.getElementById('fsubmit').style.display ) document.getElementById('fsubmit').style.display = 'box';
                                    }
                                    */
                                }
                            </script>
                        </td>
                    </tr>
                </table>
                    
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <table cellspacing="1" cellpadding="2" class="compare">
                                <xsl:apply-templates select="content/documents/data"/>
                            </table>
                            <div><br/>*�/� - ��� ������</div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </body>
  </html>
</xsl:template>

<xsl:template match="data">

    <tr>
        <td rowspan="2" align="center"><a href="{back_url}"><xsl:value-of select="$locale/common/back/text()" disable-output-escaping="yes"/></a></td>
        <xsl:for-each select="product">
            <xsl:if test="position()>1"><td rowspan="100" style="width:5px">&#160;</td></xsl:if>
            <td class="product_title" style="height:auto">
                <div style="float:left;clear:none"><xsl:value-of select="name" disable-output-escaping="yes"/></div>
                <xsl:if test="position()>1">    
                    <div style="float:right;clear:none"><a href="?{$controller_name}.compare2&amp;id={/content/documents/data/product/id}&amp;del_id={id}&amp;ids={/content/documents/data/ids}"><img src="{$views_path}/images/close_red.gif" border="0"/></a></div>
                </xsl:if>
            </td>
        </xsl:for-each>
    </tr>

    <tr>
        <xsl:for-each select="product">
            <td align="center">
                    <xsl:choose>
                        <xsl:when test="images/image/tiny != ''">
                            <img src="{images/image/tiny}" border="0"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <img src="{$views_path}/images/empty.gif" width="180" height="124" border="0"/>
                        </xsl:otherwise>
                    </xsl:choose>
            </td>
        </xsl:for-each>
    </tr>

    <tr>
        <th><xsl:value-of select="$locale/shop/compare/price/text()" disable-output-escaping="yes"/></th>
        <xsl:for-each select="product">
            <td>
                <table width="100%" cellspacing="0" cellpadding="0"><tr>
                <td align="left" style="color:#e20011">
                <xsl:choose>
                    <xsl:when test="nice_price_active = 1">
                        <xsl:value-of select="format-number( nice_price*/content/global/currency/exchange_rate,'0.00')"/>&#160;<xsl:value-of select="/content/global/currency/symbol" disable-output-escaping="yes"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:choose>
                            <xsl:when test="/content/global/client/type_id = 2">
                                <xsl:value-of select="format-number( price_opt*/content/global/currency/exchange_rate,'0.00')"/>&#160;<xsl:value-of select="/content/global/currency/symbol" disable-output-escaping="yes"/>
                            </xsl:when>
                            <xsl:when test="/content/global/client/type_id = 3">
                                <xsl:value-of select="format-number( price_opt2*/content/global/currency/exchange_rate,'0.00')"/>&#160;<xsl:value-of select="/content/global/currency/symbol" disable-output-escaping="yes"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="format-number( price*/content/global/currency/exchange_rate,'0.00')"/>&#160;<xsl:value-of select="/content/global/currency/symbol" disable-output-escaping="yes"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:otherwise>
                </xsl:choose>
                </td>
                <td align="right">
                    <a target="compare2{id}" href="?{$controller_name}.product&amp;id={id}"><xsl:value-of select="$locale/shop/list/details" disable-output-escaping="yes"/></a>
                </td>
                </tr></table>
            </td>
        </xsl:for-each>
    </tr>

    <tr>
        <th><xsl:value-of select="$locale/shop/compare/ingress/text()" disable-output-escaping="yes"/></th>
        <xsl:for-each select="product">
            <td width="200"><xsl:value-of select="ingress" disable-output-escaping="yes"/></td>
        </xsl:for-each>
    </tr>

    <tr>
        <th colspan="{count(product) * 2}"><xsl:value-of select="$locale/shop/compare/features/text()" disable-output-escaping="yes"/></th>
    </tr>
    
    <xsl:for-each select="features/*">
        <tr>
            <td><xsl:value-of select="name" disable-output-escaping="yes"/></td>
            <xsl:variable name="f_id" select="id"/>
            
            <xsl:for-each select="../../product">
                <xsl:for-each select="features/*">
                    <xsl:if test="@id = $f_id">
                        <td align="center"><xsl:value-of select="value_name"/></td>
                    </xsl:if>
                </xsl:for-each>
                
                <xsl:if test="count( features/*[@id = $f_id] ) = 0">
                    <td align="center">&#160;�/�&#160;</td>
                </xsl:if>
                
            </xsl:for-each>
            
        </tr>
    </xsl:for-each>
    
</xsl:template>

<xsl:template match="product">
    <td>
        <table cellspacing="1" class="compare">
            <tr><td class="product_title"><xsl:value-of select="name" disable-output-escaping="yes"/></td></tr>
            
            <tr>
                <td height="10%" align="center">
                    <xsl:choose>
                        <xsl:when test="images/image/tiny != ''">
                            <img src="{images/image/tiny}" border="0"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <img src="{$views_path}/images/empty.gif" width="180" height="124" border="0"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </td>
            </tr>
            
            <tr>
                <td>
                    <xsl:choose>
                        <xsl:when test="nice_price_active = 1">
                            <xsl:value-of select="format-number( nice_price*/content/global/currency/exchange_rate,'0.00')"/>&#160;<xsl:value-of select="/content/global/currency/symbol" disable-output-escaping="yes"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:choose>
                                <xsl:when test="/content/global/client/type_id = 2">
                                    <xsl:value-of select="format-number( price_opt*/content/global/currency/exchange_rate,'0.00')"/>&#160;<xsl:value-of select="/content/global/currency/symbol" disable-output-escaping="yes"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="format-number( price*/content/global/currency/exchange_rate,'0.00')"/>&#160;<xsl:value-of select="/content/global/currency/symbol" disable-output-escaping="yes"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>
                    </xsl:choose>
                </td>
            </tr>
            
            <tr>
                <td><xsl:value-of select="ingress" disable-output-escaping="yes"/></td>
            </tr>
            
            <xsl:apply-templates select="features/*"/>
            
        </table>
    </td>
</xsl:template>

<xsl:template match="feature">
    <tr>
        <td><xsl:value-of select="value_name"/></td>
    </tr>
</xsl:template>

</xsl:stylesheet>

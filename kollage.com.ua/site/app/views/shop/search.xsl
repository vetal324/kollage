<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
                
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout_shop.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:template match="data">

    <h3 class="products_title">
        <table cellspacing="0" cellpadding="0" width="100%"><tr>
        <td width="90%">
            <xsl:value-of select="$locale/common/search_results_title/text()" disable-output-escaping="yes"/>
        </td>
        
        <td width="*" align="right"><xsl:call-template name="shop_trash"/></td>
        </tr></table>
    </h3>
    
    <!-- PAGINATOR -->
    <div class="pagination" pages="{$pages}"><table cellspacing="0" cellpadding="0" border="0" width="100%"><tr>
    <td align="left">
        <select id="rows_per_page_top" onchange="document.location='?{$controller_name}.search&amp;str={str}&amp;page={$page - 1}&amp;rows_per_page='+this.options[this.selectedIndex].value">
        <xsl:variable name="selected" select="$rows_per_page"/>
            <xsl:call-template name="gen-option">
                <xsl:with-param name="value">8</xsl:with-param>
                <xsl:with-param name="title">8</xsl:with-param>
                <xsl:with-param name="selected" select="$selected"/>
            </xsl:call-template>
            <xsl:call-template name="gen-option">
                <xsl:with-param name="value">12</xsl:with-param>
                <xsl:with-param name="title">12</xsl:with-param>
                <xsl:with-param name="selected" select="$selected"/>
            </xsl:call-template>
            <xsl:call-template name="gen-option">
                <xsl:with-param name="value">20</xsl:with-param>
                <xsl:with-param name="title">20</xsl:with-param>
                <xsl:with-param name="selected" select="$selected"/>
            </xsl:call-template>
            <xsl:call-template name="gen-option">
                <xsl:with-param name="value">32</xsl:with-param>
                <xsl:with-param name="title">32</xsl:with-param>
                <xsl:with-param name="selected" select="$selected"/>
            </xsl:call-template>
            <xsl:call-template name="gen-option">
                <xsl:with-param name="value">1000000</xsl:with-param>
                <xsl:with-param name="title"><xsl:value-of select="$locale/shop/list/all"/></xsl:with-param>
                <xsl:with-param name="selected" select="$selected"/>
            </xsl:call-template>
        </select>
        <span>&#160;<xsl:value-of select="$locale/shop/list/rows_per_page"/></span>
    </td>
    
    <td align="right">
    <xsl:if test="$pages > 1">
        <xsl:call-template name="pagination">
            <xsl:with-param name="page"><xsl:value-of select="$page"/></xsl:with-param>
            <xsl:with-param name="stop"><xsl:value-of select="$pages"/></xsl:with-param>
            <xsl:with-param name="url">?<xsl:value-of select="$controller_name"/>.search&amp;str=<xsl:value-of select="str"/></xsl:with-param>
        </xsl:call-template>
        / <a href="?{$controller_name}.search&amp;str={str}&amp;page={$page - 1}"><xsl:value-of select="$locale/common/link_prev/text()" disable-output-escaping="yes"/></a> | <a href="?{$controller_name}.search&amp;str={str}&amp;page={$page + 1}"><xsl:value-of select="$locale/common/link_next/text()" disable-output-escaping="yes"/></a>
    </xsl:if>
    </td>
        
    </tr></table></div>
    <!-- PAGINATOR - END -->
    
    <xsl:choose>
        <xsl:when test="count(products/*) > 0">
            <xsl:apply-templates select="products/*"/>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="$locale/common/search_no_results/text()" disable-output-escaping="yes"/>
            
            <div class="shop_search_send_form">
            <xsl:choose>
            
                <xsl:when test="was_sent = 1">
                    <p class="msg"><xsl:value-of select="$locale/common/search_form_was_sent/text()" disable-output-escaping="yes"/></p>
                </xsl:when>
                
                <xsl:otherwise>
                <xsl:if test="error_message = 1">
                    <p class="error"><xsl:value-of select="$locale/common/search_form_error/text()" disable-output-escaping="yes"/></p>
                </xsl:if>
                <p><xsl:value-of select="$locale/common/search_form_title/text()" disable-output-escaping="yes"/></p>
                <form method="post" action="?{$controller_name}.search_send_message">
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td><xsl:value-of select="$locale/common/search_form_email/text()" disable-output-escaping="yes"/></td>
                            <td><input type="text" name="email" class="search_inputbox" value="{email}"/></td>
                        </tr>
                        <tr>
                            <td><xsl:value-of select="$locale/common/search_form_message/text()" disable-output-escaping="yes"/></td>
                            <td><textarea name="message" rows="10" cols="70"><xsl:value-of select="message"/></textarea></td>
                        </tr>
                        
                        <tr>
                            <td><xsl:value-of select="$locale/register_form/protect/text()" disable-output-escaping="yes"/></td>
                            <td colspan="2"><table><tr>
                                <td></td>
                                <td><img src="?{$controller_name}.search_protect_image"/></td>
                                <td><input type="text" name="protect_code" id="protect_code" class="inputbox_middle" value="{code}"/></td>
                            </tr></table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"><input type="submit" value="{$locale/common/buttons/form_send}" class="button"/></td>
                        </tr>
                    </table>
                </form>
                </xsl:otherwise>
                
            </xsl:choose>
            </div>
        </xsl:otherwise>
    </xsl:choose>
    
    <!-- PAGINATOR -->
    <div class="pagination" pages="{$pages}"><nobr>
    <xsl:if test="$pages > 1">
        <xsl:call-template name="pagination">
            <xsl:with-param name="page"><xsl:value-of select="$page"/></xsl:with-param>
            <xsl:with-param name="stop"><xsl:value-of select="$pages"/></xsl:with-param>
            <xsl:with-param name="url">?<xsl:value-of select="$controller_name"/>.search&amp;str=<xsl:value-of select="str"/></xsl:with-param>
        </xsl:call-template>
        / <a href="?{$controller_name}.search&amp;str={str}&amp;page={$page - 1}"><xsl:value-of select="$locale/common/link_prev/text()" disable-output-escaping="yes"/></a> | <a href="?{$controller_name}.search&amp;str={str}&amp;page={$page + 1}"><xsl:value-of select="$locale/common/link_next/text()" disable-output-escaping="yes"/></a>
    </xsl:if>
    </nobr></div>
    <!-- PAGINATOR - END -->
    
    <!-- <xsl:call-template name="new_products"/> -->
    <xsl:call-template name="best_products"/>
    
</xsl:template>

<xsl:template match="product">
    <xsl:variable name="p" select="page_in_category"/>
    
    <div class="search_item">
        <div>
            <b><xsl:value-of select="position() + ($page - 1) * $rows_per_page"/>.&#160;<xsl:value-of select="name" disable-output-escaping="yes"/></b>
            (<xsl:choose>
                    <xsl:when test="nice_price_active = 1">
                        <xsl:value-of select="format-number( nice_price*/content/global/currency/exchange_rate,'0.00')"/>&#160;<xsl:value-of select="/content/global/currency/symbol" disable-output-escaping="yes"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:choose>
                            <xsl:when test="/content/global/client/type_id = 2">
                                <xsl:value-of select="format-number( price_opt*/content/global/currency/exchange_rate,'0.00')"/>&#160;<xsl:value-of select="/content/global/currency/symbol" disable-output-escaping="yes"/>
                            </xsl:when>
                            <xsl:when test="/content/global/client/type_id = 3">
                                <xsl:value-of select="format-number( price_opt2*/content/global/currency/exchange_rate,'0.00')"/>&#160;<xsl:value-of select="/content/global/currency/symbol" disable-output-escaping="yes"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="format-number( price*/content/global/currency/exchange_rate,'0.00')"/>&#160;<xsl:value-of select="/content/global/currency/symbol" disable-output-escaping="yes"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:otherwise>
                </xsl:choose>)
        </div>
        <div class="scontent"><xsl:apply-templates select="categories/*"/></div>
        <div class="scontent"><xsl:value-of select="ingress" disable-output-escaping="yes"/></div>
        <div class="scontent"><a href="?{$controller_name}.product&amp;id={id}&amp;rows_per_page=8&amp;back_page={$p}"><xsl:value-of select="$locale/shop/list/details" disable-output-escaping="yes"/></a></div>
    </div>
</xsl:template>

<xsl:template match="category">
        
        <xsl:variable name="p">
            <xsl:choose>
                <xsl:when test="position() = count(../*)"><xsl:value-of select="../../page_in_category"/></xsl:when>
                <xsl:otherwise>1</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        
    <xsl:if test="position() = 1"><xsl:value-of select="$locale/common/category/text()" disable-output-escaping="yes"/>:&#160;</xsl:if>
    <xsl:if test="position() > 1">&#160;&#187;&#160;</xsl:if>
    <a href="?shop.show&amp;category_id={id}&amp;rows_per_page=8&amp;page={$p}&amp;show_all=1"><xsl:value-of select="name" disable-output-escaping="yes"/></a>
</xsl:template>

</xsl:stylesheet>

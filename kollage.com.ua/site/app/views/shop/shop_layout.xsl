<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:template name="shop_layout">
	<!-- Request Product Container -->
	<div id="dlg-request-product-container" class="hidden" title="�������� ������ �� �����">
		<div id="dlg-content"></div>
		<div id="dlg-footer">
			<div class="dlg-footer-loader hidden">
				<div class="loader1"></div>
			</div>
			<div class="dlg-footer-buttons">
				<a class="dlg-btn-send"></a>
				<a class="dlg-btn-close"></a>
			</div>
			<div class="clear-both"></div>
		</div>
	</div>
	<!-- Request Product Container -->
</xsl:template>

</xsl:stylesheet>
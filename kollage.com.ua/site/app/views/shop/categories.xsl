<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../layout.xsl" />

<xsl:template match="data">
	<div class="shop">
		<div class="shop-categories">
			<xsl:if test="count(shop/node()[starts-with(name(),'submenu')]) &gt; 0">
				<div class="shop-categories-column"><xsl:apply-templates select="shop/node()[starts-with(name(),'submenu') and position()>0 and position()&lt;5]"/></div>
				<div class="shop-categories-column"><xsl:apply-templates select="shop/node()[starts-with(name(),'submenu') and position()>=5 and position()&lt;8]"/></div>
				<div class="shop-categories-column"><xsl:apply-templates select="shop/node()[starts-with(name(),'submenu') and position()>=8]"/></div>
			</xsl:if>
			<div class="clear-both"></div>
			<div class="bottom-barside" style="margin-left: 1em;">
				<xsl:variable name="share_url">http%3A%2F%2Fwww.kollage.com.ua%2Fshop%2Fcategories</xsl:variable>
				<xsl:variable name="share_title">%D0%9C%D0%B0%D0%B3%D0%B0%D0%B7%D0%B8%D0%BD%2C%20%D0%BA%D0%B0%D1%82%D0%B5%D0%B3%D0%BE%D1%80%D0%B8%D0%B8</xsl:variable>
				<div class="left-barside">
					<div class="left-barside-sn">
						<div class="header-left-barside-sn">����������:</div>
						<a href="http://vkontakte.ru/share.php?url={$share_url}&amp;title={$share_title}&amp;description=&amp;noparse=true" target="_blank" class="sn-vk vtip" title="������������ ������ ���������">&#160;</a>
						<a href="http://www.facebook.com/sharer.php?s=100&amp;p[title]={$share_title}&amp;p[url]={$share_url}&amp;p[summary]=" target="_blank" class="sn-fb vtip" title="������������ ������ � Facebook">&#160;</a>
						<a href="https://plus.google.com/share?url={$share_url}" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=700,width=600');return false;" class="sn-gl vtip" title="������������ ������ Google+">&#160;</a>
					</div>
				</div>
			</div>	
			<div class="clear-both"></div>	
		</div>
	</div>
	
		
	
</xsl:template>
<xsl:template match="data/shop/node()[starts-with(name(),'submenu')]">
	<ul class="shop-categories-block">
		<xsl:choose>
			<xsl:when test="@url != ''">
				<li><a href="{@url}"><xsl:value-of select="@title"/></a></li>
			</xsl:when>
			<xsl:otherwise>
				<li><a href="#" onclick="return(false)"><xsl:value-of select="@title"/></a></li>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:if test="count(./item) &gt; 0">
		<ul>
			<xsl:apply-templates select="./item"/>
		</ul>
		</xsl:if>
	</ul>
</xsl:template>

<xsl:template match="data/shop/*/item">
	<xsl:if test="@title != ''">
		<li><a href="{@url}"><xsl:value-of select="@title" disable-output-escaping="yes"/></a></li>
	</xsl:if>
</xsl:template>

<xsl:template match="data/shop/*/*/item">
	<xsl:if test="@title != ''">
		<li><a href="{@url}"><xsl:value-of select="@title" disable-output-escaping="yes"/></a></li>
	</xsl:if>

</xsl:template>

</xsl:stylesheet>

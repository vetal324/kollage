<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
                
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout_shop.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:template match="data">
    <h3 class="products_title"><xsl:value-of select="$locale/my/purse/title/text()"/></h3>
    
    <xsl:if test="/content/global/client/id = 0">
        <xsl:call-template name="login_form2"/>
    </xsl:if>
    
    <xsl:if test="/content/global/client/id > 0">
        
            <!-- shop -->
            <xsl:if test="count(list/*)">
                <table cellpadding="3" cellspacing="1" class="content_table">
                
                <tr>
                    <th width="20%"><xsl:value-of select="$locale/my/purse/created_at/text()"/></th>
                    <th width="20%"><xsl:value-of select="$locale/my/purse/amount/text()"/></th>
                    <th width="*"><xsl:value-of select="$locale/my/purse/description/text()"/></th>
                </tr>
                
                <xsl:apply-templates select="list"/>
                
                </table>
            </xsl:if>
            <!-- shop - end-->
            
    </xsl:if>
    
    <!-- <xsl:call-template name="new_products"/> -->
    <xsl:call-template name="best_products"/>
    
</xsl:template>

<xsl:template match="list/purse_operation">
<tr>
    <td><xsl:value-of select="created_at"/></td>
    <td><xsl:value-of select="amount"/></td>
    <td><xsl:value-of select="description"/></td>
</tr>
</xsl:template>

</xsl:stylesheet>

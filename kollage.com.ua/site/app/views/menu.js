var nMaxItem = 4;
var NameIndex = 0;
var DefaultState = 1;
var MouseOverState = 2;
var MouseDownState = 3;
var imgCounter = 0;
var ImageList = new Array();

function AddImageToImageList(name, Default, MouseOver, MouseDown)
{
    ImageList[imgCounter] = new Array(nMaxItem);
    ImageList[imgCounter][NameIndex] = name;
    ImageList[imgCounter][DefaultState] = new Image();
    ImageList[imgCounter][DefaultState].src = Default;
    if (MouseOver != "") {
        ImageList[imgCounter][MouseOverState] = new Image();
        ImageList[imgCounter][MouseOverState].src = MouseOver;
    }
    if (MouseDown != "") {
        ImageList[imgCounter][MouseDownState] = new Image();
        ImageList[imgCounter][MouseDownState].src = MouseDown;
    }

    imgCounter++;
}

function ReplaceImage(name, state)
{
    for (i = 0; i < imgCounter; i++) {
        if (document.images[ImageList[i][NameIndex]] != null) {
            if ((name == ImageList[i][NameIndex]) && (ImageList[i][state] != null))
                document.images[name].src = ImageList[i][state].src;
        }
    }
}
<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:msxsl="urn:schemas-microsoft-com:xslt"
				xmlns:exsl="http://exslt.org/common"
				extension-element-prefixes="exsl"
				version="1.0">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:template name="left_panel">
	<table cellpadding="2">
		<tr><td><xsl:value-of select="$locale/shop/left_panel/add/text()"/></td></tr>
		
			<form action="?shop.category_add" method="post">
			<tr><td><input type="text" class="middle" name="name"/></td></tr>
			<tr><td align="right"><input type="submit" value="{$locale/common/buttons/form_add/text()}"/></td></tr>
			</form>
		
		<tr><td><xsl:value-of select="$locale/shop/left_panel/del/text()"/></td></tr>
		
			<form action="?shop.category_del" method="post" onsubmit="return(confirm('{$locale/shop/left_panel/question/text()}'))">
			<tr><td><select name="category_id" id="category_id" class="middle"><xsl:apply-templates select="content/documents/left_panel/categories/category"/></select></td></tr>
			<tr><td align="right"><input type="submit" value="{$locale/common/buttons/form_del/text()}"/></td></tr>
			</form>
	</table>
</xsl:template>

<xsl:template match="category">
<option value="{id}"><xsl:value-of select="name"/></option>
</xsl:template>

</xsl:stylesheet>

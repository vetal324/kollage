
function GetWindowSize()
{
	var width = 0, height = 0;
	
	if( typeof( window.innerWidth ) == 'number' )
	{
		width = window.innerWidth;
		height = window.innerHeight;
	}
	else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) )
	{
		width = document.documentElement.clientWidth;
		height = document.documentElement.clientHeight;
	}
	else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) )
	{
		width = document.body.clientWidth;
		height = document.body.clientHeight;
	}
	
	return( [width,height] );
}

function GetWindowScroll()
{
	var scrOfX = 0, scrOfY = 0;
	if( typeof( window.pageYOffset ) == 'number' )
	{
		scrOfY = window.pageYOffset;
		scrOfX = window.pageXOffset;
	}
	else if( document.body && ( document.body.scrollLeft || document.body.scrollTop ) )
	{
		scrOfY = document.body.scrollTop;
		scrOfX = document.body.scrollLeft;
	}
	else if( document.documentElement && ( document.documentElement.scrollLeft || document.documentElement.scrollTop ) )
	{
		scrOfY = document.documentElement.scrollTop;
		scrOfX = document.documentElement.scrollLeft;
	}
	
	return [ scrOfX, scrOfY ];
}

function GetObjectVisibleSize( id )
{
	var w = 0, h = 0;
	if( typeof(id) == 'object' ) obj = id;
	else obj = document.getElementById(id);
	if( obj!=null )
	{
		w = ( obj.offsetWidth>0 )? obj.offsetWidth : obj.clientWidth;
		h = ( obj.offsetHeight>0 )? obj.offsetHeight : obj.clientHeight;
	}
	
	return( [w,h] );
}

function GetObjectSize( id )
{
	var w = 0, h = 0;
	if( typeof(id) == 'object' ) obj = id;
	else obj = document.getElementById(id);
	if( obj!=null )
	{
		w = obj.scrollWidth;
		h = obj.scrollHeight;
	}
	
	return( [w,h] );
}

function add_to_url(url,add)
{
	url2go = ""+ url;
	url2go = url2go.replace(/#/,"");
	params = add.match(/([^=]+)=(.*)/);
	if( params[1].length>0 )
	{
		if( url2go.indexOf(params[1]) != -1 )
		{
			//re = eval("/&*"+params[1]+"=[^&]*/");
			re = eval("/[&?]+"+params[1]+"=[^&]*/");
			url2go = url2go.replace(re,"");
		}
	}
	
	if( url2go.indexOf("?") == -1 )
	{
		url2go += "?" + add;
	}
	else
	{
		url2go += "&" + add;
	}
	
	return(url2go);
}

function del_image( url )
{
	if( confirm('Delete this image?') )
	{
		document.location = url;
	}
}

function del_item( url, msg )
{
	if( msg == '' ) msg = "Do you want to delete this item?";
	if( confirm(msg) )
	{
		document.location = url;
	}
}

function popup_image_preview( width, height, image_url )
{
	p = window.open(image_url,'image_preview','scrollbars=yes,status=no,width='+ width +',height='+ height +',resizable=yes')
	p.document.body.style.margin = 0;
	return( false );
}

function PreviewImage( event, image, width, height, close_logo, close_text )
{
	while( $('preview_image') ) $('preview_image').remove();
	var i = new Element( 'div', {'id':"preview_image",'title':close_logo,'class':"preview_image"}).update( '<img src="'+ image +'" width="'+ width +'" height="'+ height +'" border="0"/><div style="position:absolute;left:4;top:2;color:#eeeeee">'+ close_text +'</div>' );
	i.observe( 'click', function()
	{
		while($('preview_image')) $('preview_image').remove()
		if( BrowserDetect.browser == 'Explorer' && BrowserDetect.version>=5.5 && BrowserDetect.version<7 )
		{
			var sb = $A(document.getElementsByTagName('select'));
			sb.each(Element.show);
		}
	} );
	Element.insert( $('first_table'), i );
	
	if( BrowserDetect.browser == 'Explorer' && BrowserDetect.version>=5.5 && BrowserDetect.version<7 )
	{
		var sb = $A(document.getElementsByTagName('select'));
		sb.each(Element.hide);
	}
	
	var x, y;
	width = Number( width ); height = Number( height );
	x = ( (width+10) <= event.clientX )? event.clientX - (width+10) : 0;
	y = ( (height)/2 <= event.clientY )? GetWindowScroll()[1] + event.clientY - (height/2) : GetWindowScroll()[1];
	i.setStyle( "top:"+ y +";left:"+ x +";" );
}

function ScrollToObject( name )
{
	var obj = document.getElementById(name);
	if( obj )
	{
		var x = obj.x ? obj.x : obj.offsetLeft;
		var y = obj.y ? obj.y : obj.offsetTop;
		window.scrollTo( x, y );
	}
}

function element_is_null( obj_id )
{
	retval = false;
	obj = document.getElementById( obj_id );
	if( obj!=null )
	{
		if( obj.value== "" ) retval = true;
	}
	
	return(retval);
}

function element_is_email( obj_id )
{
	retval = false;
	obj = document.getElementById( obj_id );
	if( obj!=null )
	{
		if( obj.value.indexOf("@")!=-1 && obj.value.indexOf(".")!=-1 ) retval = true;
	}
	
	return(retval);
}

function element_is_number( obj_id )
{
	retval = false;
	obj = document.getElementById( obj_id );
	if( obj!=null )
	{
		obj.value = obj.value.replace( / /g, "" );
		re = /[\d\.\,-]/gi;
		if( obj.value.replace( re,"")=="" ) retval = true;
	}
	
	return(retval);
}

/* 
	form - form object
	elements - array of elements ['element id','{null|email|number}','alert message']
*/
function checkform( form, elements )
{
	msg = "";
	for( i in elements )
	{
		switch( elements[i][1] )
		{
			case 'email':
				if( !element_is_email( elements[i][0] ) ) msg += (elements[i][2]!="")? elements[i][2] +"\n" : "Element ["+ elements[i][0] +"] is not an email!\n";
				break;
			case 'number':
				if( !element_is_number( elements[i][0] ) ) msg += (elements[i][2]!="")? elements[i][2] +"\n" : "Element ["+ elements[i][0] +"] is not a number!\n";;
				break;
			case 'null':
			default:
				if( element_is_null( elements[i][0] ) ) msg += (elements[i][2]!="")? elements[i][2] +"\n" : "Element ["+ elements[i][0] +"] is null!\n";;
				break;
		}
	}
	if( msg!="" )
	{
		alert( msg ); return( false );
	}
	else return( true );
}

function select_select( s, value )
{
	var i;
	for( i=0; i<s.options.length; i++ )
	{
		if( s.options[i].value == value )
		{
			s.selectedIndex = i;
			break;
		}
	}
}

function createCookie(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}
function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function RemoveLeftPanel() {
	var panel = $$('.admin-panel-left-panel').each(function(el){
		el.remove();
	});
}
function RemoveSmallTabs() {
	var panel = $$('.admin-panel-small-tabs').each(function(el){
		el.remove();
	});
}

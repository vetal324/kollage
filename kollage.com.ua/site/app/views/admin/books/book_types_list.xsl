<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt"
				version="1.0" xmlns:csl="http://www.w3.org/1999/XSL/Transform">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout.xsl" />
<xsl:include href="../menu.xsl" />
<xsl:include href="../paginator.xsl" />

<xsl:template match="data">
	<xsl:choose>
		<xsl:when test="/content/global/user/roles/role[code='sop']/@selected = 'yes'">
			<xsl:apply-templates select="list"/>
		</xsl:when>
		<xsl:otherwise>
			<div class="error"><xsl:value-of select="$locale/common/msgs/access_denied/text()"/></div>
			<script type="text/javascript">RemoveSmallTabs();RemoveLeftPanel();</script>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="list">

	<div id="books-wrapper">

		<div class="books-breadcrumb">
			<ul>
				<li><a href="?{$controller_name}.book_types"><xsl:value-of select="$locale/books/breadcrumb/book_types/text()"/></a></li>
			</ul>

		</div>

		<div class="books-container list-container">

			<table class="list" id="list">
				<tr>
					<th width="*"><xsl:value-of select="$locale/books/list/name/text()"/></th>
					<th width="*"><xsl:value-of select="$locale/books/list/pages_count/text()"/></th>
					<th width="*"><xsl:value-of select="$locale/books/list/print_count/text()"/></th>
					<th width="15%"><xsl:value-of select="$locale/books/list/discounts/text()"/></th>
					<th width="*"><xsl:value-of select="$locale/books/list/formats/text()"/></th>
					<th width="*"><xsl:value-of select="$locale/books/list/options/text()"/></th>
					<th width="10%"><xsl:value-of select="$locale/books/list/logo/text()"/></th>
					<th width="2%" style="text-align:center">&#160;</th>
					<th width="2%" style="text-align:center">&#160;</th>
				</tr>

				<xsl:choose>
					<xsl:when test="book_type">
						<xsl:apply-templates select="book_type">
						</xsl:apply-templates>
					</xsl:when>
					<xsl:otherwise>
						<tr><td colspan="20" align="center"><xsl:value-of select="$locale/books/list/insert_msg/text()"/></td></tr>
					</xsl:otherwise>
				</xsl:choose>

				<xsl:variable name="btn_add_new" select="$locale/books/buttons/add_new/text()"/>

				<tr>
					<th class="actions" colspan="20">
						<table align="right"><tr>
							<td><input type="button" value="{$btn_add_new}" onclick="document.location='?{$controller_name}.book_type_edit&amp;last_position={last_position}'"/></td>
						</tr></table>
					</th>
				</tr>
			</table>

			<p><xsl:value-of select="$locale/books/list/help/text()" disable-output-escaping="yes"/></p>
		</div>
	</div>

	<script language="JavaScript" src="{$views_path}/article.js"></script>
</xsl:template>

<xsl:template match="book_type">
	<xsl:variable name="id" select="id"/>
	<xsl:variable name="img_path" select="image" />
	<xsl:variable name="book_type_name" select="name" />
	<tr id="row{position()}">
		<td class="row"><div class="cell" id="c{position()}"><xsl:value-of select="name"/><input type="hidden" name="cid{position()}" id="cid{position()}" value="{id}"/></div></td>
		<td class="row">
			<xsl:value-of select="min_page_count"/> - <xsl:value-of select="max_page_count"/>
		</td>
		<td class="row">
			<xsl:value-of select="min_print_count"/> - <xsl:value-of select="max_print_count"/>
		</td>
		<td class="row">
			<table class="">
				<tr>
					<td>�� <xsl:value-of select="threshold1"/>:</td>
					<td><xsl:value-of select="discount1"/> %</td>
				</tr>
				<tr>
					<td>�� <xsl:value-of select="threshold2"/>:</td>
					<td><xsl:value-of select="discount2"/> %</td>
				</tr>
				<tr>
					<td>�� <xsl:value-of select="threshold3"/>:</td>
					<td><xsl:value-of select="discount3"/> %</td>
				</tr>
				<tr>
					<td>�� <xsl:value-of select="threshold4"/>:</td>
					<td><xsl:value-of select="discount4"/> %</td>
				</tr>
			</table>
		</td>
		<td class="row">
			<ul>
				<xsl:for-each select="book_formats/book_format">
					<li><xsl:value-of select="name"/></li>
				</xsl:for-each>
			</ul>
			<a href="?{$controller_name}.book_formats&amp;type_id={$id}" title="{$locale/books/list/formats_edit/text()}" >
				������������� <img src="{$views_path}/images/edit.gif" border="0"/>
			</a>
		</td>
		<td class="row">
			<ul>
				<xsl:for-each select="book_options/book_option">
					<li><xsl:value-of select="name"/></li>
				</xsl:for-each>
			</ul>
			<a href="?{$controller_name}.book_options&amp;type_id={$id}" title="{$locale/books/list/options_edit/text()}" >
				������������� <img src="{$views_path}/images/edit.gif" border="0"/>
			</a>
		</td>
		<td class="row"><img src="{$data_path}/{$img_path}" alt="{$book_type_name}" width="150" style="border: 2px solid #ccc;" /> </td>
		<td class="row" align="center"><a title="{$locale/common/edit/text()}" href="?{$controller_name}.book_type_edit&amp;id={$id}"><img  src="{$views_path}/images/edit.gif" border="0"/></a></td>
		<td class="row" align="center"><a title="{$locale/common/del/text()}" href="#" onclick="article_delete('?{$controller_name}.book_type_del&amp;id={id}','{$locale/common/msgs/delete_item/text()}')"><img  src="{$views_path}/images/del.gif" border="0"/></a></td>
	</tr>
</xsl:template>

</xsl:stylesheet>
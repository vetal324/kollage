<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:exsl="http://exslt.org/common"
                extension-element-prefixes="exsl"
                version="1.0">

    <xsl:output method="html" encoding="utf-8" indent="no"/>

    <xsl:include href="../common.xsl" />
    <xsl:include href="../locale.xsl" />
    <xsl:include href="../layout.xsl" />
    <xsl:include href="../menu.xsl" />

    <xsl:template match="data">
        <xsl:choose>
            <xsl:when test="/content/global/user/roles/role[code='sop']/@selected = 'yes'">
                <xsl:apply-templates/>
            </xsl:when>
            <xsl:otherwise>
                <div class="error"><xsl:value-of select="$locale/common/msgs/access_denied/text()"/></div>
                <script type="text/javascript">RemoveSmallTabs();RemoveLeftPanel();</script>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="edit">
        <xsl:variable name="book_type_id" select="book_type/id"/>

        <div id="books-wrapper">

            <div class="books-breadcrumb">
                <ul>
                    <li><a href="?{$controller_name}.book_types"><xsl:value-of select="$locale/books/breadcrumb/book_types/text()"/></a></li>
                    <li> > </li>
                    <li><a href="?{$controller_name}.book_type_edit&amp;id={$book_type_id}"><xsl:value-of select="book_type/name"></xsl:value-of></a></li>
                    <li> > </li>
                    <li><a href="?{$controller_name}.book_options&amp;type_id={$book_type_id}"><xsl:value-of select="$locale/books/breadcrumb/options/text()"/></a></li>
                    <li> > </li>
                    <li>
                        <a href="#">
                            <xsl:choose>
                                <xsl:when test="book_option/id &gt; 0">
                                    <xsl:value-of select="book_option/name"/>
                                </xsl:when>
                                <xsl:otherwise><xsl:value-of select="$locale/books/breadcrumb/new_option/text()"/></xsl:otherwise>
                            </xsl:choose>
                        </a>
                    </li>
                </ul>

            </div>

            <div class="books-container edit-container">
                <xsl:call-template name="tiny_mce"/>
                <xsl:variable name="title">
                    <xsl:choose>
                        <xsl:when test="book_option/id &gt; 0"><xsl:value-of select="$locale/books/edit/title1/text()"/></xsl:when>
                        <xsl:otherwise><xsl:value-of select="$locale/books/edit/title2/text()"/></xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>

                <xsl:variable name="id" select="book_option/id"/>
                <xsl:variable name="position"><xsl:value-of select="book_option/position"/></xsl:variable>

                <form name="book_option_edit" id="book_option_edit" action="?{$controller_name}.book_option_save" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="id" value="{$id}"/>
                    <input type="hidden" name="last_position" value="{last_position}"/>
                    <input type="hidden" name="object[id]" value="{$id}"/>
                    <input type="hidden" name="object[lang]" value="{$lang}"/>
                    <input type="hidden" name="object[position]" value="{$position}"/>
                    <input type="hidden" name="object[type_id]" value="{$book_type_id}"/>
                    <table class="form" cellspacing="0" cellpadding="0">

                        <!-- Show message -->
                        <xsl:if test="save_result = 'true'">
                            <tr><td colspan="2"><div class="message"><xsl:value-of select="$locale/books/edit/save_msg_success/text()"/></div></td></tr>
                        </xsl:if>
                        <xsl:if test="save_result = 'false'">
                            <tr><td colspan="2"><div class="error"><xsl:value-of select="$locale/books/edit/save_msg_error/text()"/></div></td></tr>
                        </xsl:if>
                        <!-- /Show message -->

                        <tr><th colspan="2"><xsl:value-of select="$title"/></th></tr>

                        <tr>
                            <td class="left_label2"><label for="name"><xsl:value-of select="$locale/books/options/edit/name/text()"/></label></td>
                            <td><input type="text" class="medium" name="object[name]" id="name" value="{book_option/name}"/></td>
                        </tr>

                        <tr>
                            <td class="left_label2"><label for="price_base"><xsl:value-of select="$locale/books/options/edit/description/text()"/></label></td>
                            <td><input type="text" class="medium" name="object[description]" id="description" value="{book_option/description}"/></td>
                        </tr>

                        <tr>
                            <td class="left_label2"><label for="width"><xsl:value-of select="$locale/books/options/edit/price_page/text()"/></label></td>
                            <td><input type="text" class="medium" name="object[price_page]" id="price_page" value="{book_option/price_page}"/></td>
                        </tr>

                        <tr>
                            <td class="left_label2"><label for="width"><xsl:value-of select="$locale/books/options/edit/price_item/text()"/></label></td>
                            <td><input type="text" class="medium" name="object[price_item]" id="price_item" value="{book_option/price_item}"/></td>
                        </tr>

                        <tr>
                            <td class="left_label2"><label for="width"><xsl:value-of select="$locale/books/options/edit/price_order/text()"/></label></td>
                            <td><input type="text" class="medium" name="object[price_order]" id="price_order" value="{book_option/price_order}"/></td>
                        </tr>

                        <tr>
                            <th colspan="2" class="actions">
                                <a href="?{$controller_name}.book_options&amp;type_id={$book_type_id}">
                                    <xsl:value-of select="$locale/common/buttons/form_cancel/text()" />
                                </a>
                                <input type="submit" value="{$locale/common/buttons/form_save/text()}"/>
                            </th>
                        </tr>
                    </table>

                </form>

            </div>
        </div>
        <script language="JavaScript" src="{$views_path}/article.js"></script>

    </xsl:template>
</xsl:stylesheet>

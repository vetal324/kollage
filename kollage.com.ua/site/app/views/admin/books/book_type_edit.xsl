<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:exsl="http://exslt.org/common"
                extension-element-prefixes="exsl"
                version="1.0">

    <xsl:output method="html" encoding="utf-8" indent="no"/>

    <xsl:include href="../common.xsl" />
    <xsl:include href="../locale.xsl" />
    <xsl:include href="../layout.xsl" />
    <xsl:include href="../menu.xsl" />

    <xsl:template match="data">
        <xsl:choose>
            <xsl:when test="/content/global/user/roles/role[code='sop']/@selected = 'yes'">
                <xsl:apply-templates/>
            </xsl:when>
            <xsl:otherwise>
                <div class="error"><xsl:value-of select="$locale/common/msgs/access_denied/text()"/></div>
                <script type="text/javascript">RemoveSmallTabs();RemoveLeftPanel();</script>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="edit">

        <div id="books-wrapper">

            <div class="books-breadcrumb">
                <ul>
                    <li><a href="?{$controller_name}.book_types"><xsl:value-of select="$locale/books/breadcrumb/book_types/text()"/></a></li>
                    <li> > </li>
                    <li>
                        <a href="#">
                            <xsl:choose>
                                <xsl:when test="book_type/id &gt; 0">
                                    <xsl:value-of select="book_type/name"/>
                                </xsl:when>
                                <xsl:otherwise><xsl:value-of select="$locale/books/breadcrumb/new_book_type/text()"/></xsl:otherwise>
                            </xsl:choose>
                        </a>
                    </li>
                </ul>

            </div>

            <div class="books-container edit-container">
                <xsl:call-template name="tiny_mce"/>
                <xsl:variable name="title">
                    <xsl:choose>
                        <xsl:when test="book_type/id &gt; 0"><xsl:value-of select="$locale/books/edit/title1/text()"/></xsl:when>
                        <xsl:otherwise><xsl:value-of select="$locale/books/edit/title2/text()"/></xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>

                <xsl:variable name="id" select="book_type/id"/>
                <xsl:variable name="position"><xsl:value-of select="book_type/position"/></xsl:variable>

                <xsl:variable name="min_page_count">
                    <xsl:choose>
                        <xsl:when test="book_type/min_page_count > 0">
                            <xsl:value-of select="book_type/min_page_count"/>
                        </xsl:when>
                        <xsl:otherwise>
                            0
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:variable name="max_page_count">
                    <xsl:choose>
                        <xsl:when test="book_type/max_page_count > 0">
                            <xsl:value-of select="book_type/max_page_count"/>
                        </xsl:when>
                        <xsl:otherwise>
                            1000
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:variable name="min_print_count">
                    <xsl:choose>
                        <xsl:when test="book_type/min_print_count > 0">
                            <xsl:value-of select="book_type/min_print_count"/>
                        </xsl:when>
                        <xsl:otherwise>
                            0
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:variable name="max_print_count">
                    <xsl:choose>
                        <xsl:when test="book_type/max_print_count > 0">
                            <xsl:value-of select="book_type/max_print_count"/>
                        </xsl:when>
                        <xsl:otherwise>
                            1000
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>

                <form name="book_type_edit" id="book_type_edit" action="?{$controller_name}.book_type_save" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="id" value="{$id}"/>
                    <input type="hidden" name="last_position" value="{last_position}"/>
                    <input type="hidden" name="object[id]" value="{$id}"/>
                    <input type="hidden" name="object[lang]" value="{$lang}"/>
                    <input type="hidden" name="object[position]" value="{$position}"/>

                    <input type="hidden" name="object[min_page_count]" value="{$min_page_count}"/>
                    <input type="hidden" name="object[max_page_count]" value="{$max_page_count}"/>
                    <input type="hidden" name="object[min_print_count]" value="{$min_print_count}"/>
                    <input type="hidden" name="object[max_print_count]" value="{$max_print_count}"/>

                    <table class="form" cellspacing="0" cellpadding="0">
                        <!-- Show message -->
                        <xsl:if test="save_result = 'true'">
                            <tr><td colspan="2"><div class="message"><xsl:value-of select="$locale/books/edit/save_msg_success/text()"/></div></td></tr>
                        </xsl:if>
                        <xsl:if test="save_result = 'false'">
                            <tr><td colspan="2"><div class="error"><xsl:value-of select="$locale/books/edit/save_msg_error/text()"/></div></td></tr>
                        </xsl:if>
                        <!-- /Show message -->

                        <tr><th colspan="2"><xsl:value-of select="$title"/></th></tr>

                        <tr>
                            <td class="left_label2"><label for="name"><xsl:value-of select="$locale/books/edit/name/text()"/></label></td>
                            <td><input type="text" class="medium" name="object[name]" id="name" value="{book_type/name}"/></td>
                        </tr>

                        <tr>
                            <td class="left_label2"><label for="description"><xsl:value-of select="$locale/books/edit/description/text()"/></label></td>
                            <td><input type="text" class="medium" name="object[description]" id="description" value="{book_type/description}"/></td>
                        </tr>

                        <tr>
                            <td><xsl:value-of select="$locale/books/edit/page_count/text()"/></td>
                            <td>
                                <div id="pageCountRange" class="kollage-range"> </div>
                                <div class="kollage-range-info">
                                    <span id="pageCountMin">0</span> - <span id="pageCountMax">0</span>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td><xsl:value-of select="$locale/books/edit/print_count/text()"/></td>
                            <td>
                                <div id="printCountRange" class="kollage-range"> </div>
                                <div class="kollage-range-info">
                                    <span id="printCountMin">0</span> - <span id="printCountMax">0</span>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td rowspan="4"><xsl:value-of select="$locale/books/edit/discounts/text()"/></td>
                            <td>
                                <label class="discount-label"><xsl:value-of select="$locale/books/edit/discount/text()"/></label>
                                <input type="text" class="medium discount" name="object[discount1]" id="discount1" value="{book_type/discount1}"/>
                                <label class="discount-label"><xsl:value-of select="$locale/books/edit/discount_percent/text()"/></label>
                                <label class="discount-label"><xsl:value-of select="$locale/books/edit/discount_threshold/text()"/></label>
                                <input type="text" class="medium discount" name="object[threshold1]" id="threshold1" value="{book_type/threshold1}"/>
                                <label class="discount-label"><xsl:value-of select="$locale/books/edit/item_count/text()"/></label>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <label class="discount-label"><xsl:value-of select="$locale/books/edit/discount/text()"/></label>
                                <input type="text" class="medium discount" name="object[discount2]" id="discount2" value="{book_type/discount2}"/>
                                <label class="discount-label"><xsl:value-of select="$locale/books/edit/discount_percent/text()"/></label>
                                <label class="discount-label"><xsl:value-of select="$locale/books/edit/discount_threshold/text()"/></label>
                                <input type="text" class="medium discount" name="object[threshold2]" id="threshold2" value="{book_type/threshold2}"/>
                                <label class="discount-label"><xsl:value-of select="$locale/books/edit/item_count/text()"/></label>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <label class="discount-label"><xsl:value-of select="$locale/books/edit/discount/text()"/></label>
                                <input type="text" class="medium discount" name="object[discount3]" id="discount3" value="{book_type/discount3}"/>
                                <label class="discount-label"><xsl:value-of select="$locale/books/edit/discount_percent/text()"/></label>
                                <label class="discount-label"><xsl:value-of select="$locale/books/edit/discount_threshold/text()"/></label>
                                <input type="text" class="medium discount" name="object[threshold3]" id="threshold3" value="{book_type/threshold3}"/>
                                <label class="discount-label"><xsl:value-of select="$locale/books/edit/item_count/text()"/></label>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <label class="discount-label"><xsl:value-of select="$locale/books/edit/discount/text()"/></label>
                                <input type="text" class="medium discount" name="object[discount4]" id="discount4" value="{book_type/discount4}"/>
                                <label class="discount-label"><xsl:value-of select="$locale/books/edit/discount_percent/text()"/></label>
                                <label class="discount-label"><xsl:value-of select="$locale/books/edit/discount_threshold/text()"/></label>
                                <input type="text" class="medium discount" name="object[threshold4]" id="threshold4" value="{book_type/threshold4}"/>
                                <label class="discount-label"><xsl:value-of select="$locale/books/edit/item_count/text()"/></label>
                            </td>
                        </tr>

                        <tr>
                            <td class="left_label"><label for="image"><xsl:value-of select="$locale/books/edit/image/text()"/></label></td>
                            <td>
                                <table cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <input type="file" size="30" name="image" id="image"/>
                                        </td>
                                        <td>
                                            <xsl:if test="book_type/image != ''">&#160;&#160;
                                                <a href="#"
                                                   onclick="del_image('?{$controller_name}.book_type_del_img&amp;id={$id}&amp;type_id={book_type/id}')">
                                                    <img src="{$views_path}/images/del.gif" border="0"/>
                                                </a>
                                            </xsl:if>
                                        </td>
                                        <td>
                                            <xsl:if test="book_type/image != ''">
                                                <xsl:variable name="image" select="book_type/image"/>
                                                &#160;&#160;
                                                <a href="#"
                                                   onclick="return(popup_image_preview('640','480','{$data_path}/{$image}'))">
                                                    <img src="{$views_path}/images/preview.gif" border="0"/>
                                                </a>
                                            </xsl:if>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr>
                            <th colspan="2" class="actions">
                                <a href="?books.book_types">
                                    <xsl:value-of select="$locale/common/buttons/form_cancel/text()" />
                                </a>
                                <input type="submit" value="{$locale/common/buttons/form_save/text()}"/>
                            </th>
                        </tr>
                    </table>
                </form>

                <script language="JavaScript" src="{$views_path}/article.js"></script>
                <script type="text/javascript" language="javascript" charset="utf-8" src="{$views_path}/books/js/range.js?version=1"></script>
                <script type="text/javascript" language="javascript" charset="utf-8" >
                    $j(document).ready(function(){
                        var pageCountRangeConfig = {
                            id: "pageCountRange",
                            range: true,
                            min: 0,
                            max: 1000,
                            step: 2,
                            formId: "book_type_edit",
                            values: [<xsl:value-of select="$min_page_count"/>,<xsl:value-of select="$max_page_count"/>],
                            minRangeName: "object[min_page_count]",
                            maxRangeName: "object[max_page_count]",
                            minDisplayId: "pageCountMin",
                            maxDisplayId: "pageCountMax"
                        };
                        var pageCountRange = new RangeComponent(pageCountRangeConfig);
                        pageCountRange.init();

                        var printCountRangeConfig = {
                            id: "printCountRange",
                            range: true,
                            min: 0,
                            max: 1000,
                            step: 1,
                            formId: "book_type_edit",
                            values: [<xsl:value-of select="$min_print_count"/>,<xsl:value-of select="$max_print_count"/>],
                            minRangeName: "object[min_print_count]",
                            maxRangeName: "object[max_print_count]",
                            minDisplayId: "printCountMin",
                            maxDisplayId: "printCountMax"
                        };
                        var printCountRange = new RangeComponent(printCountRangeConfig);
                        printCountRange.init();
                    });
                </script>

            </div>

        </div>

    </xsl:template>
</xsl:stylesheet>
var RangeComponent = function(config) {

    console.log('RangeComponent, config:');
    console.log(config);

    this.selector   = "#" + config.id;
    this.min        = config.min;
    this.max        = config.max;
    this.values     = config.values;
    this.step       = config.step ? config.step : 1;

    this.minRangeName = config.minRangeName;
    this.maxRangeName = config.maxRangeName;

    this.minDisplayId = config.minDisplayId;
    this.maxDisplayId = config.maxDisplayId;

    this.minRangeInput = $j("input[name='" + this.minRangeName + "']");
    this.maxRangeInput = $j("input[name='" + this.maxRangeName + "']");

    this.minDisplayLabel = $j("#" + this.minDisplayId);
    this.maxDisplayLabel = $j("#" + this.maxDisplayId);
};

$j.extend(true, RangeComponent, {

    prototype: {

        init: function() {

            this.initSlider();

            this.initUI();
        },

        initSlider: function () {
            var scope = this;
            $j(this.selector).slider({
                range: true,
                min: this.min,
                max: this.max,
                values: this.values,
                step: scope.step,
                slide: function (event, ui) {
                    var minValue = ui.values[0];
                    var maxValue = ui.values[1];
                    scope.minRangeInput.val(minValue);
                    scope.maxRangeInput.val(maxValue);
                    scope.minDisplayLabel.text(minValue);
                    scope.maxDisplayLabel.text(maxValue);
                }
            })
        },

        initUI: function () {
            console.log('initUI invoked.');
            var minValue = this.values[0];
            var maxValue = this.values[1];
            this.minRangeInput.val(minValue);
            this.maxRangeInput.val(maxValue);
            this.minDisplayLabel.text(minValue);
            this.maxDisplayLabel.text(maxValue);
        }

    }

});
window.RangeComponent = RangeComponent;
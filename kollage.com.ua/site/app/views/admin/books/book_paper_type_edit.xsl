<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:exsl="http://exslt.org/common"
                extension-element-prefixes="exsl"
                version="1.0">

    <xsl:output method="html" encoding="utf-8" indent="no"/>

    <xsl:include href="../common.xsl" />
    <xsl:include href="../locale.xsl" />
    <xsl:include href="../layout.xsl" />
    <xsl:include href="../menu.xsl" />

    <xsl:template match="data">
        <xsl:choose>
            <xsl:when test="/content/global/user/roles/role[code='sop']/@selected = 'yes'">
                <xsl:apply-templates/>
            </xsl:when>
            <xsl:otherwise>
                <div class="error"><xsl:value-of select="$locale/common/msgs/access_denied/text()"/></div>
                <script type="text/javascript">RemoveSmallTabs();RemoveLeftPanel();</script>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="edit">
        <xsl:variable name="book_type_id" select="book_type/id"/>
        <xsl:variable name="book_format_id" select="book_format/id" />

        <div id="books-wrapper">

            <div class="books-breadcrumb">
                <ul>
                    <ul>
                        <li><a href="?{$controller_name}.book_types"><xsl:value-of select="$locale/books/breadcrumb/book_types/text()"/></a></li>
                        <li> > </li>
                        <li><a href="?{$controller_name}.book_type_edit&amp;id={$book_type_id}"><xsl:value-of select="book_type/name"></xsl:value-of></a></li>
                        <li> > </li>
                        <li><a href="?{$controller_name}.book_formats&amp;type_id={$book_type_id}"><xsl:value-of select="$locale/books/breadcrumb/formats/text()"/></a></li>
                        <li> > </li>
                        <li><a href="?{$controller_name}.book_format_edit&amp;type_id={$book_type_id}&amp;id={$book_format_id}"><xsl:value-of select="book_format/name"></xsl:value-of></a></li>
                        <li> > </li>
                        <li><a href="?{$controller_name}.book_paper_types&amp;format_id={$book_format_id}"><xsl:value-of select="$locale/books/breadcrumb/paper_types/text()"/></a></li>
                        <li> > </li>
                        <li>
                            <a href="#">
                                <xsl:choose>
                                    <xsl:when test="book_paper_type/id &gt; 0">
                                        <xsl:value-of select="book_paper_type/name"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="$locale/books/breadcrumb/new_paper_type/text()"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </a>
                        </li>
                    </ul>
                </ul>

            </div>

            <div class="books-container edit-container">
                <xsl:call-template name="tiny_mce"/>

                <xsl:variable name="title">
                    <xsl:choose>
                        <xsl:when test="book_paper_type/id &gt; 0"><xsl:value-of select="$locale/books/edit/title1/text()"/></xsl:when>
                        <xsl:otherwise><xsl:value-of select="$locale/books/edit/title2/text()"/></xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>

                <xsl:variable name="id" select="book_paper_type/id"/>
                <xsl:variable name="position"><xsl:value-of select="book_paper_type/position"/></xsl:variable>

                <form name="book_paper_type_edit" id="book_paper_type_edit" action="?{$controller_name}.book_paper_type_save" method="post">

                    <input type="hidden" name="id" value="{$id}"/>
                    <input type="hidden" name="last_position" value="{last_position}"/>
                    <input type="hidden" name="object[id]" value="{$id}"/>
                    <input type="hidden" name="object[lang]" value="{$lang}"/>
                    <input type="hidden" name="object[position]" value="{$position}"/>

                    <input type="hidden" name="object[type_id]" value="{$book_type_id}"/>
                    <input type="hidden" name="object[format_id]" value="{$book_format_id}"/>

                    <table class="form" cellspacing="0" cellpadding="0">

                        <!-- Show message -->
                        <xsl:if test="save_result = 'true'">
                            <tr><td colspan="2"><div class="message"><xsl:value-of select="$locale/books/edit/save_msg_success/text()"/></div></td></tr>
                        </xsl:if>
                        <xsl:if test="save_result = 'false'">
                            <tr><td colspan="2"><div class="error"><xsl:value-of select="$locale/books/edit/save_msg_error/text()"/></div></td></tr>
                        </xsl:if>
                        <!-- /Show message -->

                        <tr><th colspan="2"><xsl:value-of select="$title"/></th></tr>


                        <tr>
                            <td class="left_label2"><label for="name"><xsl:value-of select="$locale/books/paper_types/edit/name/text()"/></label></td>
                            <td><input type="text" class="medium" name="object[name]" id="name" value="{book_paper_type/name}"/></td>
                        </tr>

                        <tr>
                            <td class="left_label2"><label for="description"><xsl:value-of select="$locale/books/paper_types/edit/description/text()"/></label></td>
                            <td><input type="text" class="medium" name="object[description]" id="description" value="{book_paper_type/description}"/></td>
                        </tr>

                        <tr>
                            <td class="left_label2"><label for="price"><xsl:value-of select="$locale/books/paper_types/edit/price/text()"/></label></td>
                            <td><input type="text" class="medium" name="object[price]" id="price" value="{book_paper_type/price}"/></td>
                        </tr>

                        <tr>
                            <td class="left_label2"><label for="density"><xsl:value-of select="$locale/books/paper_types/edit/density/text()"/></label></td>
                            <td><input type="text" class="medium" name="object[density]" id="density" value="{book_paper_type/density}"/></td>
                        </tr>

                        <tr>
                            <th colspan="2" class="actions">
                                <a href="?{$controller_name}.book_paper_types&amp;format_id={$book_format_id}">
                                    <xsl:value-of select="$locale/common/buttons/form_cancel/text()" />
                                </a>
                                <input type="submit" value="{$locale/common/buttons/form_save/text()}"/>
                            </th>
                        </tr>

                    </table>

                </form>
            </div>

        </div>

        <script language="JavaScript" src="{$views_path}/article.js"></script>
    </xsl:template>

</xsl:stylesheet>
<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                version="1.0" xmlns:csl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="html" encoding="utf-8" indent="no"/>

    <xsl:include href="../common.xsl" />
    <xsl:include href="../locale.xsl" />
    <xsl:include href="../layout.xsl" />
    <xsl:include href="../menu.xsl" />
    <xsl:include href="../paginator.xsl" />

    <xsl:template match="data">
        <xsl:choose>
            <xsl:when test="/content/global/user/roles/role[code='sop']/@selected = 'yes'">
                <xsl:apply-templates select="list"/>
            </xsl:when>
            <xsl:otherwise>
                <div class="error"><xsl:value-of select="$locale/common/msgs/access_denied/text()"/></div>
                <script type="text/javascript">RemoveSmallTabs();RemoveLeftPanel();</script>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="list">
        <xsl:variable name="book_format_id" select="book_format/id" />
        <xsl:variable name="book_type_id" select="book_type/id" />
        <xsl:variable name="book_cover_type_id" select="book_cover_type/id" />

        <div id="books-wrapper">

            <div class="books-breadcrumb">
                <ul>
                    <li><a href="?{$controller_name}.book_types"><xsl:value-of select="$locale/books/breadcrumb/book_types/text()"/></a></li>
                    <li> > </li>
                    <li><a href="?{$controller_name}.book_type_edit&amp;id={$book_type_id}"><xsl:value-of select="book_type/name"></xsl:value-of></a></li>
                    <li> > </li>
                    <li><a href="?{$controller_name}.book_formats&amp;type_id={$book_type_id}"><xsl:value-of select="$locale/books/breadcrumb/formats/text()"/></a></li>
                    <li> > </li>
                    <li><a href="?{$controller_name}.book_format_edit&amp;type_id={$book_type_id}&amp;id={$book_format_id}"><xsl:value-of select="book_format/name"></xsl:value-of></a></li>
                    <li> > </li>
                    <li><a href="?{$controller_name}.book_covers&amp;format_id={$book_format_id}"><xsl:value-of select="$locale/books/breadcrumb/covers/text()"/></a></li>
                    <li> > </li>
                    <li><a href="?{$controller_name}.book_cover_edit&amp;format_id={$book_format_id}&amp;id={$book_cover_type_id}"><xsl:value-of select="book_cover_type/name"></xsl:value-of></a></li>
                    <li> > </li>
                    <li><a href="#"><xsl:value-of select="$locale/books/breadcrumb/cover_paper_types/text()"/></a></li>
                </ul>
            </div>

            <div class="books-container list-container">
                <table class="list" id="list">
                    <tr>
                        <th width="*"><xsl:value-of select="$locale/books/covers_paper_types/list/name/text()"/></th>
                        <th width="*"><xsl:value-of select="$locale/books/covers_paper_types/list/description/text()"/></th>
                        <th width="*"><xsl:value-of select="$locale/books/covers_paper_types/list/density/text()"/></th>
                        <th width="*"><xsl:value-of select="$locale/books/covers_paper_types/list/price/text()"/></th>
                        <th width="*"><xsl:value-of select="$locale/books/covers_paper_types/list/lamination/text()"/></th>
                        <th width="2%" style="text-align:center">&#160;</th>
                        <th width="2%" style="text-align:center">&#160;</th>
                    </tr>

                    <xsl:choose>
                        <xsl:when test="book_cover_paper_type">
                            <xsl:apply-templates select="book_cover_paper_type">
                            </xsl:apply-templates>
                        </xsl:when>
                        <xsl:otherwise>
                            <tr><td colspan="20" align="center"><xsl:value-of select="$locale/books/list/insert_msg/text()"/></td></tr>
                        </xsl:otherwise>
                    </xsl:choose>

                    <xsl:variable name="btn_add_new" select="$locale/books/buttons/add_new/text()"/>

                    <tr>
                        <th class="actions" colspan="20">
                            <table align="right"><tr>
                                <td><input type="button" value="{$btn_add_new}" onclick="document.location='?{$controller_name}.book_cover_paper_type_edit&amp;last_position={last_position}&amp;cover_id={$book_cover_type_id}'"/></td>
                            </tr></table>
                        </th>
                    </tr>
                </table>

                <p><xsl:value-of select="$locale/books/list/help/text()" disable-output-escaping="yes"/></p>
            </div>
        </div>

        <script language="JavaScript" src="{$views_path}/article.js"></script>
    </xsl:template>

    <xsl:template match="book_cover_paper_type">
        <xsl:variable name="id" select="id"/>
        <xsl:variable name="cover_id" select="cover_id"/>

        <tr id="row{position()}">
            <td class="row"><div class="cell" id="c{position()}"><xsl:value-of select="name"/><input type="hidden" name="cid{position()}" id="cid{position()}" value="{id}"/></div></td>
            <td class="row">
                <xsl:value-of select="description"/>
            </td>
            <td class="row"><xsl:value-of select="density"/></td>
            <td class="row"><xsl:value-of select="price"/></td>
            <td class="row">
                <xsl:choose>
                    <xsl:when test="is_laminated != 0">
                        ����
                    </xsl:when>
                    <xsl:otherwise>
                        ���
                    </xsl:otherwise>
                </xsl:choose>
            </td>
            <td class="row" align="center"><a title="{$locale/common/edit/text()}" href="?{$controller_name}.book_cover_paper_type_edit&amp;id={$id}&amp;cover_id={$cover_id}"><img  src="{$views_path}/images/edit.gif" border="0"/></a></td>
            <td class="row" align="center"><a title="{$locale/common/del/text()}" href="#" onclick="article_delete('?{$controller_name}.book_cover_paper_type_del&amp;id={id}&amp;cover_id={$cover_id}','{$locale/common/msgs/delete_item/text()}')"><img  src="{$views_path}/images/del.gif" border="0"/></a></td>
        </tr>
    </xsl:template>

</xsl:stylesheet>
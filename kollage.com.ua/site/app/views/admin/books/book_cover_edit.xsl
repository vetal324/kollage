<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:exsl="http://exslt.org/common"
                extension-element-prefixes="exsl"
                version="1.0">

    <xsl:output method="html" encoding="utf-8" indent="no"/>

    <xsl:include href="../common.xsl" />
    <xsl:include href="../locale.xsl" />
    <xsl:include href="../layout.xsl" />
    <xsl:include href="../menu.xsl" />

    <xsl:template match="data">
        <xsl:choose>
            <xsl:when test="/content/global/user/roles/role[code='sop']/@selected = 'yes'">
                <xsl:apply-templates/>
            </xsl:when>
            <xsl:otherwise>
                <div class="error"><xsl:value-of select="$locale/common/msgs/access_denied/text()"/></div>
                <script type="text/javascript">RemoveSmallTabs();RemoveLeftPanel();</script>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="edit">
        <xsl:variable name="book_type_id" select="book_type/id"/>
        <xsl:variable name="book_format_id" select="book_format/id" />

        <xsl:variable name="min_limit" select="book_type/min_page_count"/>
        <xsl:variable name="max_limit" select="book_type/max_page_count"/>

        <xsl:variable name="min_page_count">
            <xsl:choose>
                <xsl:when test="book_cover_type/min_page_count > 0">
                    <xsl:value-of select="book_cover_type/min_page_count"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$min_limit"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="max_page_count">
            <xsl:choose>
                <xsl:when test="book_cover_type/max_page_count > 0">
                    <xsl:value-of select="book_cover_type/max_page_count"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$max_limit"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <div id="books-wrapper">

            <div class="books-breadcrumb">
                <ul>
                    <ul>
                        <li><a href="?{$controller_name}.book_types"><xsl:value-of select="$locale/books/breadcrumb/book_types/text()"/></a></li>
                        <li> > </li>
                        <li><a href="?{$controller_name}.book_type_edit&amp;id={$book_type_id}"><xsl:value-of select="book_type/name"></xsl:value-of></a></li>
                        <li> > </li>
                        <li><a href="?{$controller_name}.book_formats&amp;type_id={$book_type_id}"><xsl:value-of select="$locale/books/breadcrumb/formats/text()"/></a></li>
                        <li> > </li>
                        <li><a href="?{$controller_name}.book_format_edit&amp;type_id={$book_type_id}&amp;id={$book_format_id}"><xsl:value-of select="book_format/name"></xsl:value-of></a></li>
                        <li> > </li>
                        <li><a href="?{$controller_name}.book_covers&amp;format_id={$book_format_id}"><xsl:value-of select="$locale/books/breadcrumb/covers/text()"/></a></li>
                        <li> > </li>
                        <li>
                            <a href="#">
                                <xsl:choose>
                                    <xsl:when test="book_cover_type/id &gt; 0">
                                        <xsl:value-of select="book_cover_type/name"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="$locale/books/breadcrumb/new_cover/text()"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </a>
                        </li>
                    </ul>
                </ul>

            </div>

            <div class="books-container edit-container">
                <xsl:call-template name="tiny_mce"/>

                <xsl:variable name="title">
                    <xsl:choose>
                        <xsl:when test="book_cover_type/id &gt; 0"><xsl:value-of select="$locale/books/edit/title1/text()"/></xsl:when>
                        <xsl:otherwise><xsl:value-of select="$locale/books/edit/title2/text()"/></xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>

                <xsl:variable name="id" select="book_cover_type/id"/>
                <xsl:variable name="position"><xsl:value-of select="book_cover_type/position"/></xsl:variable>

                <form name="book_cover_edit" id="book_cover_edit" action="?{$controller_name}.book_cover_save" method="post" enctype="multipart/form-data">

                    <input type="hidden" name="id" value="{$id}"/>
                    <input type="hidden" name="last_position" value="{last_position}"/>
                    <input type="hidden" name="object[id]" value="{$id}"/>
                    <input type="hidden" name="object[lang]" value="{$lang}"/>
                    <input type="hidden" name="object[position]" value="{$position}"/>

                    <input type="hidden" name="object[min_page_count]" value="{$min_page_count}"/>
                    <input type="hidden" name="object[max_page_count]" value="{$max_page_count}"/>

                    <input type="hidden" name="object[type_id]" value="{$book_type_id}"/>
                    <input type="hidden" name="object[format_id]" value="{$book_format_id}"/>

                    <table class="form" cellspacing="0" cellpadding="0">

                        <!-- Show message -->
                        <xsl:if test="save_result = 'true'">
                            <tr><td colspan="2"><div class="message"><xsl:value-of select="$locale/books/edit/save_msg_success/text()"/></div></td></tr>
                        </xsl:if>
                        <xsl:if test="save_result = 'false'">
                            <tr><td colspan="2"><div class="error"><xsl:value-of select="$locale/books/edit/save_msg_error/text()"/></div></td></tr>
                        </xsl:if>
                        <!-- /Show message -->

                        <tr><th colspan="2"><xsl:value-of select="$title"/></th></tr>


                        <tr>
                            <td class="left_label2"><label for="name"><xsl:value-of select="$locale/books/covers/edit/name/text()"/></label></td>
                            <td><input type="text" class="medium" name="object[name]" id="name" value="{book_cover_type/name}"/></td>
                        </tr>

                        <tr>
                            <td class="left_label2"><label for="description"><xsl:value-of select="$locale/books/covers/edit/description/text()"/></label></td>
                            <td><input type="text" class="medium" name="object[description]" id="description" value="{book_cover_type/description}"/></td>
                        </tr>

                        <tr>
                            <td><xsl:value-of select="$locale/books/covers/edit/pages_count/text()"/></td>
                            <td>
                                <div id="pageCountRange" class="kollage-range"> </div>
                                <div class="kollage-range-info">
                                    <span id="pageCountMin">0</span> - <span id="pageCountMax">0</span>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td class="left_label2"><label for="description"><xsl:value-of select="$locale/books/covers/edit/price_base/text()"/></label></td>
                            <td><input type="text" class="medium" name="object[price]" id="price" value="{book_cover_type/price}"/></td>
                        </tr>

                        <tr>
                            <td class="left_label"><label for="image"><xsl:value-of select="$locale/books/covers/edit/logo/text()"/></label></td>
                            <td>
                                <table cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <input type="file" size="30" name="image" id="image"/>
                                        </td>
                                        <td>
                                            <xsl:if test="book_cover_type/image != ''">&#160;&#160;
                                                <a href="#"
                                                   onclick="del_image('?{$controller_name}.book_cover_type_del_img&amp;id={$id}&amp;format_id={$book_format_id}')">
                                                    <img src="{$views_path}/images/del.gif" border="0"/>
                                                </a>
                                            </xsl:if>
                                        </td>
                                        <td>
                                            <xsl:if test="book_cover_type/image != ''">
                                                <xsl:variable name="image" select="book_cover_type/image"/>
                                                &#160;&#160;
                                                <a href="#"
                                                   onclick="return(popup_image_preview('640','480','{$data_path}/{$image}'))">
                                                    <img src="{$views_path}/images/preview.gif" border="0"/>
                                                </a>
                                            </xsl:if>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr>
                            <th colspan="2" class="actions">
                                <a href="?{$controller_name}.book_covers&amp;format_id={$book_format_id}">
                                    <xsl:value-of select="$locale/common/buttons/form_cancel/text()" />
                                </a>
                                <input type="submit" value="{$locale/common/buttons/form_save/text()}"/>
                            </th>
                        </tr>

                    </table>

                </form>
            </div>

        </div>

        <script language="JavaScript" src="{$views_path}/article.js"></script>
        <script type="text/javascript" language="javascript" charset="utf-8" src="{$views_path}/books/js/range.js?version=1"></script>
        <script language="JavaScript" charset="utf-8" >
            $j(document).ready(function(){
                var pageCountRangeConfig = {
                    id: "pageCountRange",
                    range: true,
                    min: <xsl:value-of select="$min_limit"/>,
                    max: <xsl:value-of select="$max_limit"/>,
                    step: 4,
                    formId: "book_cover_edit",
                    values: [<xsl:value-of select="$min_page_count"/>,<xsl:value-of select="$max_page_count"/>],
                    minRangeName: "object[min_page_count]",
                    maxRangeName: "object[max_page_count]",
                    minDisplayId: "pageCountMin",
                    maxDisplayId: "pageCountMax"
                };
                var pageCountRange = new RangeComponent(pageCountRangeConfig);
                pageCountRange.init();
            });
        </script>

    </xsl:template>

</xsl:stylesheet>
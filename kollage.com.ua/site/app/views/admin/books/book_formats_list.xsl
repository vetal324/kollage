<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt"
				version="1.0" xmlns:csl="http://www.w3.org/1999/XSL/Transform">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout.xsl" />
<xsl:include href="../menu.xsl" />
<xsl:include href="../paginator.xsl" />

<xsl:template match="data">
	<xsl:choose>
		<xsl:when test="/content/global/user/roles/role[code='sop']/@selected = 'yes'">
			<xsl:apply-templates select="list"/>
		</xsl:when>
		<xsl:otherwise>
			<div class="error"><xsl:value-of select="$locale/common/msgs/access_denied/text()"/></div>
			<script type="text/javascript">RemoveSmallTabs();RemoveLeftPanel();</script>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="list">
	<xsl:variable name="book_type_id" select="book_type/id"/>

	<div id="books-wrapper">

		<div class="books-breadcrumb">
			<ul>
				<li><a href="?{$controller_name}.book_types"><xsl:value-of select="$locale/books/breadcrumb/book_types/text()"/></a></li>
				<li> > </li>
				<li><a href="?{$controller_name}.book_type_edit&amp;id={$book_type_id}"><xsl:value-of select="book_type/name"></xsl:value-of></a></li>
				<li> > </li>
				<li><a href="#"><xsl:value-of select="$locale/books/breadcrumb/formats/text()"/></a></li>
			</ul>
		</div>

		<div class="books-container list-container">
			<table class="list" id="list">
				<tr>
					<th width="*"><xsl:value-of select="$locale/books/formats/list/name/text()"/></th>
					<th width="*"><xsl:value-of select="$locale/books/formats/list/description/text()"/></th>
					<th width="*"><xsl:value-of select="$locale/books/formats/list/size/text()"/></th>
					<th width="*"><xsl:value-of select="$locale/books/formats/list/page_count_step/text()"/></th>
					<th width="*"><xsl:value-of select="$locale/books/formats/list/cover/text()"/></th>
					<th width="*"><xsl:value-of select="$locale/books/formats/list/inner/text()"/></th>
					<th width="*"><xsl:value-of select="$locale/books/formats/list/logo/text()"/></th>
					<th width="2%" style="text-align:center">&#160;</th>
					<th width="2%" style="text-align:center">&#160;</th>
				</tr>

				<xsl:choose>
					<xsl:when test="book_format">
						<xsl:apply-templates select="book_format">
						</xsl:apply-templates>
					</xsl:when>
					<xsl:otherwise>
						<tr><td colspan="20" align="center"><xsl:value-of select="$locale/books/list/insert_msg/text()"/></td></tr>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:variable name="btn_add_new" select="$locale/books/buttons/add_new/text()"/>
				<tr>
					<th class="actions" colspan="20">
						<table align="right">
							<tr>
								<td>
									<input type="button" value="{$btn_add_new}" onclick="document.location='?{$controller_name}.book_format_edit&amp;last_position={last_position}&amp;type_id={$book_type_id}'"/>
								</td>
							</tr>
						</table>
					</th>
				</tr>
			</table>

			<p><xsl:value-of select="$locale/books/list/help/text()" disable-output-escaping="yes"/></p>
		</div>
	</div>
	<script language="JavaScript" src="{$views_path}/article.js"></script>
</xsl:template>

<xsl:template match="book_format">
	<xsl:variable name="id" select="id"/>
	<xsl:variable name="img_path" select="image" />
	<xsl:variable name="book_type_name" select="name" />
	<xsl:variable name="book_type_id" select="type_id"/>
	<tr id="row{position()}">
		<td class="row"><div class="cell" id="c{position()}"><xsl:value-of select="name"/><input type="hidden" name="cid{position()}" id="cid{position()}" value="{id}"/></div></td>
		<td class="row"><xsl:value-of select="description"/></td>
		<td class="row"><xsl:value-of select="width"/> x <xsl:value-of select="height"/></td>
		<td class="row"><xsl:value-of select="page_count_step"/></td>

		<td class="row">
			<u><xsl:value-of select="$locale/books/formats/list/cover_types/text()"/>:</u>
			<ul>
				<xsl:for-each select="cover_types/book_cover_type">
					<li><xsl:value-of select="name"/></li>
				</xsl:for-each>
			</ul>
			<a href="?{$controller_name}.book_covers&amp;format_id={$id}" title="{$locale/books/formats/list/cover_types/text()}" >
				������������� <img src="{$views_path}/images/edit.gif" border="0"/>
			</a>
		</td>
		<td class="row">
			<u><xsl:value-of select="$locale/books/formats/list/paper_types/text()"/>:</u>
			<ul>
				<xsl:for-each select="paper_types/book_paper_type">
					<li><xsl:value-of select="name"/></li>
				</xsl:for-each>
			</ul>
			<a href="?{$controller_name}.book_paper_types&amp;format_id={$id}" title="{$locale/books/formats/list/paper_types/text()}" >
				������������� <img src="{$views_path}/images/edit.gif" border="0"/>
			</a>
		</td>

		<td class="row">
			<xsl:choose>
				<xsl:when test="image != ''">
					<img src="{$data_path}/{$img_path}" alt="{$book_type_name}" width="100" height="150" />
				</xsl:when>
				<xsl:otherwise>
					[No image]
				</xsl:otherwise>
			</xsl:choose>
		</td>
		<td class="row" align="center"><a title="{$locale/common/edit/text()}" href="?{$controller_name}.book_format_edit&amp;id={$id}&amp;type_id={$book_type_id}"><img  src="{$views_path}/images/edit.gif" border="0"/></a></td>
		<td class="row" align="center"><a title="{$locale/common/del/text()}" href="#" onclick="article_delete('?{$controller_name}.book_format_del&amp;id={id}&amp;type_id={$book_type_id}','{$locale/common/msgs/delete_item/text()}')"><img  src="{$views_path}/images/del.gif" border="0"/></a></td>
	</tr>
</xsl:template>

</xsl:stylesheet>
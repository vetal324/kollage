<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:exsl="http://exslt.org/common"
                extension-element-prefixes="exsl"
                version="1.0">
                
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />

<xsl:template match="/">
  <html>
    <head>
      <title><xsl:value-of select="$locale/common/title/text()"/></title>
      <link rel="stylesheet" href="{$views_path}/styles.css" />
      <script language="JavaScript" src="{$views_path}/common.js"></script>
    </head>
    <body>
        <div style="padding:12px"><xsl:apply-templates select="content/documents/data"/></div>
    </body>
  </html>
</xsl:template>

<xsl:template match="data">
    <xsl:apply-templates/>
</xsl:template>

<xsl:template match="reloader">
    <script language="JavaScript">
        var op = window.opener;
        if(op)
        {
            var form = op.document.getElementById('reload');
            form.submit();
        }
        <xsl:if test="@close = 'yes'">
        window.close();
        </xsl:if>
    </script>
</xsl:template>

<xsl:template match="image">

    <xsl:call-template name="tiny_mce"/>
    
    <xsl:variable name="id" select="id"/>
    <xsl:variable name="parent_id" select="parent_id"/>
    <xsl:variable name="folder" select="folder"/>
    <xsl:variable name="name" select="name"/>
    <xsl:variable name="author" select="author"/>
    
    <xsl:variable name="header">
        <xsl:choose>
            <xsl:when test="id &gt; 0"><xsl:value-of select="$locale/mediabox_product/edit/title1/text()"/></xsl:when>
            <xsl:otherwise><xsl:value-of select="$locale/mediabox_product/edit/title2/text()"/></xsl:otherwise>
        </xsl:choose>
    </xsl:variable>

    <form action="?{$controller_name}.save" name="gallery_edit" id="gallery_edit" method="post" enctype="multipart/form-data">
        <input type="hidden" name="id" value="{$id}"/>
        <input type="hidden" name="parent_id" value="{$parent_id}"/>
        <input type="hidden" name="parent_folder" value="{$folder}"/>
        <input type="hidden" name="image[id]" value="{$id}"/>
        <input type="hidden" name="image[parent_id]" value="{$parent_id}"/>
        <input type="hidden" name="image[folder]" value="{$folder}"/>
    <table class="form" cellspacing="0" cellpadding="0">
        
        <tr><th colspan="2"><xsl:value-of select="$header"/></th></tr>
        
        <!--
        <tr>
            <td class="left_label"><label for="name"><xsl:value-of select="$locale/mediabox_product/edit/title/text()"/></label></td>
            <td><input type="text" class="large" name="image[name]" id="title" value="{$name}"/></td>
        </tr>
        
        <tr>
            <td class="left_label"><label for="author"><xsl:value-of select="$locale/mediabox_product/edit/author/text()"/></label></td>
            <td><input type="text" class="large" name="image[author]" id="author" value="{$author}"/></td>
        </tr>
        -->
        
        <tr><td colspan="2" class="label"><label for="description"><xsl:value-of select="$locale/mediabox_product/edit/text/text()"/></label></td></tr>
        <tr><td colspan="2"><textarea name="image[description]" id="text" class="large" rows="10" cols="80"><xsl:value-of select="description"/></textarea></td></tr>
        
        <!--
        <tr>
            <td class="left_label"><label for="tag"><xsl:value-of select="$locale/mediabox_product/edit/tag/text()"/></label></td>
            <td><input type="text" class="small" name="image[tag]" id="tag" value="{tag}"/>&#160;<xsl:value-of select="$locale/mediabox_product/edit/tag_help/text()"/></td>
        </tr>
        
        <tr><td colspan="2" class="label"><label for="filename"><xsl:value-of select="$locale/mediabox_product/edit/image/text()" disable-output-escaping="yes"/></label></td></tr>
        <tr><td colspan="2">
            <table cellspacing="0" cellpadding="0"><tr>
                <td><input type="file" size="70" name="filename" id="filename"/></td>
                <td>&#160;<xsl:if test="filename != ''"><a href="#" onclick="media_delimage('?{$controller_name}.delfilename&amp;parent_id={$parent_id}&amp;parent_folder={$folder}&amp;id={$id}')"><img  src="{$views_path}/images/del.gif" border="0"/></a></xsl:if></td>
                <td><xsl:if test="filename != ''"><a href="#" onclick="popup_image_preview(640,480,'{filename}');return(false);"><img  src="{$views_path}/images/preview.gif" border="0"/></a></xsl:if></td>
            </tr></table>
        </td></tr>
        
        <tr><td colspan="2" class="label"><label for="small"><xsl:value-of select="$locale/mediabox_product/edit/small/text()"/></label></td></tr>
        <tr><td colspan="2">
            <table cellspacing="0" cellpadding="0"><tr>
                <td><input type="file" size="70" name="small" id="small"/></td>
                <td>&#160;<xsl:if test="small != ''"><a href="#" onclick="media_delimage('?{$controller_name}.delsmall&amp;parent_id={$parent_id}&amp;parent_folder={$folder}&amp;id={$id}')"><img  src="{$views_path}/images/del.gif" border="0"/></a></xsl:if></td>
                <td><xsl:if test="small != ''"><a href="#" onclick="popup_image_preview(640,480,'{small}');return(false);"><img  src="{$views_path}/images/preview.gif" border="0"/></a></xsl:if></td>
            </tr></table>
        </td></tr>
        -->
        
        <tr><td colspan="2" class="label"><label for="tiny"><xsl:value-of select="$locale/mediabox_sanounce/edit/thumbnail/text()"/></label></td></tr>
        <tr><td colspan="2">
            <table cellspacing="0" cellpadding="0"><tr>
                <td><input type="file" size="70" name="tiny" id="tiny"/></td>
                <td>&#160;<xsl:if test="tiny != ''"><a href="#" onclick="media_delimage('?{$controller_name}.deltiny&amp;parent_id={$parent_id}&amp;parent_folder={$folder}&amp;id={$id}')"><img  src="{$views_path}/images/del.gif" border="0"/></a></xsl:if></td>
                <td><xsl:if test="tiny != ''"><a href="#" onclick="popup_image_preview(640,480,'{tiny}');return(false);"><img  src="{$views_path}/images/preview.gif" border="0"/></a></xsl:if></td>
            </tr></table>
        </td></tr>
        
        <tr><th colspan="2" class="actions"><input type="button" value="{$locale/mediabox_product/edit/close/text()}" onclick="self.close()"/>&#160;&#160;<input type="submit" value="{$locale/mediabox_product/edit/save/text()}"/></th></tr>
    </table>
    </form>
    
    <script language="JavaScript">
    function media_delimage( url )
    {
        if( confirm('Delete this image?') )
        {
            document.location = url;
        }
    }
    </script>
    
</xsl:template>

</xsl:stylesheet>

/*
    Using External Tools: Prototype and script.aculo.us
*/

function DefineDnDPosition( count_rows )
{
    for( i=1; i<=count_rows; i++ )
    {
        new Draggable( 'c'+i, { revert:true,constraint:'vertical',
            onEnd: function(obj,m){ 
                //$$('td.row').each( function(v){ v.style.backgroundColor = '#ffffff'; } );
            }
        } );
        Droppables.add( 'c'+i, { hoverclass: 'dnd',
            onDrop: function(s,d){
                //$$('td.row').each( function(v){ v.style.backgroundColor = '#ffffff'; } );
                s_id = s.id.substr(1); d_id = d.id.substr(1);
                ChangePosition( s_id, d_id ); // it's an separate function for each page
            },
            onHover: function(s,d){
                id = d.id.substr(1);
                //$$('td.row').each( function(v){ v.style.backgroundColor = '#ffffff'; } );
                //elms = $('row'+id).getElementsByClassName('row');
                //elms.each( function(v) { v.style.backgroundColor = '#c3d0ff'; } );
            }
        } );
    }
}

function ChangePosition( src, dst )
{
}

<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:msxsl="urn:schemas-microsoft-com:xslt"
				xmlns:exsl="http://exslt.org/common"
				extension-element-prefixes="exsl"
				version="1.0">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:include href="left_panel.xsl" />

<xsl:template match="data">
	<xsl:choose>
		<xsl:when test="/content/global/user/roles/role[code='shop_products']/@selected = 'yes'">
			<xsl:apply-templates/>
		</xsl:when>
		<xsl:otherwise>
			<div class="error"><xsl:value-of select="$locale/common/msgs/access_denied/text()"/></div>
			<script type="text/javascript">RemoveSmallTabs();RemoveLeftPanel();</script>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="edit">
	
	<xsl:variable name="category_id" select="feature/category_id"/>
	
	<table class="form" cellspacing="0" cellpadding="0">
		<tr>
			<th><xsl:value-of select="$locale/shop/category/features/values/title/text()"/> (<xsl:value-of select="feature/name"/>)</th>
			<th><xsl:value-of select="$locale/shop/category/features/position/text()"/></th>
			<th></th>
		</tr>

		<xsl:apply-templates select="feature/values/*">
			<xsl:with-param name="category_id" select="$category_id"/>
		</xsl:apply-templates>
		
		<tr><th colspan="3" class="actions">
			<form name="features" id="features" action="?{$controller_name}.feature_value_add" method="post">
				<input type="hidden" name="category_id" value="{$category_id}"/>
				<input type="hidden" name="object[lang]" value="{$lang}"/>
				<input type="hidden" name="object[feature_id]" value="{feature/id}"/>
			<table border="0" width="100%"><tr>
				<td width="20%"><xsl:value-of select="$locale/shop/category/features/values/add_title/text()"/></td>
				<td><input type="text" name="object[position]" class="small" style="width:60px;"/></td>
				<td width="*" align="left"><input type="text" name="object[name]" class="middle"/></td>
				<td width="10%" align="right"><input type="submit" value="{$locale/common/buttons/form_add/text()}"/></td>
			</tr></table>
			</form>
		</th></tr>
	</table>
	
	<script language="JavaScript" src="{$views_path}/article.js"></script>
	
</xsl:template>

<xsl:template match="value">
	<xsl:param name="category_id"/>
	
	<tr>
		<td width="*" class="row"><xsl:value-of select="name"/></td>
		<td width="30%" class="row">
			<form name="features" id="features" action="?{$controller_name}.feature_value_save" method="post" style="margin:0;padding:0">
				<input type="hidden" name="category_id" value="{$category_id}"/>
				<input type="hidden" name="object[id]" value="{id}"/>
				<input type="hidden" name="object[lang]" value="{$lang}"/>
				<table cellspacing="0" cellpadding="0" style="margin-right:3px;"><tr>
					<td class="row"><input type="text" name="object[position]" value="{position}" class="small" style="width:60px;margin-right:3px"/></td>
					<td><input type="text" name="object[name]" value="{name}" class="middle"/></td>
					<td><input type="submit" value="{$locale/common/buttons/form_save/text()}"/></td>
				</tr></table>
			</form>
		</td>
		<td width="2%" class="row"><a href="#" onclick="del_item('?{$controller_name}.feature_value_del&amp;id={id}&amp;category_id={$category_id}','{$locale/common/msgs/delete_item/text()}')"><img  src="{$views_path}/images/del.gif" border="0"/></a></td>
	</tr>
			
</xsl:template>

</xsl:stylesheet>

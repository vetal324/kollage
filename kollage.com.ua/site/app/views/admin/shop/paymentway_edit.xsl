<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:msxsl="urn:schemas-microsoft-com:xslt"
				xmlns:exsl="http://exslt.org/common"
				extension-element-prefixes="exsl"
				version="1.0">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:include href="left_panel.xsl" />

<xsl:template match="data">
	<xsl:choose>
		<xsl:when test="/content/global/user/roles/role[code='shop_products']/@selected = 'yes'">
			<xsl:apply-templates/>
		</xsl:when>
		<xsl:otherwise>
			<div class="error"><xsl:value-of select="$locale/common/msgs/access_denied/text()"/></div>
			<script type="text/javascript">RemoveSmallTabs();RemoveLeftPanel();</script>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="edit">

	<xsl:call-template name="tiny_mce"/>

	<xsl:variable name="title">
		<xsl:choose>
			<xsl:when test="paymentway/id &gt; 0"><xsl:value-of select="$locale/sop/paymentways/edit/title1/text()"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="$locale/sop/paymentways/edit/title2/text()"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	
	<xsl:variable name="id" select="paymentway/id"/>
	<xsl:variable name="name" select="paymentway/name"/>
	<xsl:variable name="position"><xsl:value-of select="paymentway/position"/></xsl:variable>

	<form name="paymentway_edit" id="paymentway_edit" action="?{$controller_name}.paymentway_save" method="post">
		<input type="hidden" name="id" value="{$id}"/>
		<input type="hidden" name="last_position" value="{last_position}"/>
		<input type="hidden" name="object[id]" value="{$id}"/>
		<input type="hidden" name="object[lang]" value="{$lang}"/>
		<input type="hidden" name="object[position]" value="{$position}"/>
	<table class="form" cellspacing="0" cellpadding="0">
	
		<!-- Show message -->
		<xsl:if test="save_result = 'true'">
		<tr><td colspan="2"><div class="message"><xsl:value-of select="$locale/sop/paymentways/edit/save_msg_success/text()"/></div></td></tr>
		</xsl:if>
		<xsl:if test="save_result = 'false'">
		<tr><td colspan="2"><div class="error"><xsl:value-of select="$locale/sop/paymentways/edit/save_msg_error/text()"/></div></td></tr>
		</xsl:if>
		<!-- /Show message -->
		
		<tr><th colspan="2"><xsl:value-of select="$title"/></th></tr>
		
		<tr>
			<td class="left_label"><label for="name"><xsl:value-of select="$locale/sop/paymentways/edit/name/text()"/></label></td>
			<td><input type="text" class="large" name="object[name]" id="name" value="{$name}"/></td>
		</tr>
		
		<tr><td class="label"><label for="code"><xsl:value-of select="$locale/sop/paymentways/edit/code/text()"/></label></td>
		<td>
			<xsl:variable name="selected" select="paymentway/code"/>
			<select name="object[code]" id="code">
				<xsl:for-each select="$locale/sop/paymentways/codes/*">
					<xsl:call-template name="gen-option">
						<xsl:with-param name="value" select="position()"/>
						<xsl:with-param name="title" select="text()"/>
						<xsl:with-param name="selected" select="$selected"/>
					</xsl:call-template>
				</xsl:for-each>
			</select>
		</td></tr>
		
		<tr><td class="left_label" colspan="2"><label for="description"><xsl:value-of select="$locale/sop/paymentways/edit/description/text()"/></label></td></tr>
		<tr><td colspan="2"><textarea name="object[description]" id="description" class="tinymce large" rows="15" cols="80"><xsl:value-of select="paymentway/description"/></textarea></td></tr>
		
		<tr><th colspan="2" class="actions"><input type="submit" value="{$locale/common/buttons/form_save/text()}"/></th></tr>
	</table>
		
	</form>
	
	<script language="JavaScript" src="{$views_path}/article.js"></script>
	
</xsl:template>

</xsl:stylesheet>

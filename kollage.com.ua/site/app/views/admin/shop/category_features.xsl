<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:msxsl="urn:schemas-microsoft-com:xslt"
				xmlns:exsl="http://exslt.org/common"
				extension-element-prefixes="exsl"
				version="1.0">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:include href="left_panel.xsl" />

<xsl:template match="data">
	<xsl:choose>
		<xsl:when test="/content/global/user/roles/role[code='shop_products']/@selected = 'yes'">
			<xsl:apply-templates/>
		</xsl:when>
		<xsl:otherwise>
			<div class="error"><xsl:value-of select="$locale/common/msgs/access_denied/text()"/></div>
			<script type="text/javascript">RemoveSmallTabs();RemoveLeftPanel();</script>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="edit">
	
	<xsl:variable name="category_id" select="category/id"/>
	
	<table class="form" cellspacing="0" cellpadding="0">
		<tr><th colspan="9"><xsl:value-of select="$locale/shop/category/features/title/text()"/></th></tr>
		
		<tr>
			<td><b><xsl:value-of select="$locale/shop/category/features/name/text()"/></b></td>
			<td><b><xsl:value-of select="$locale/shop/category/features/show_in_filter/text()"/></b></td>
			<td><b><xsl:value-of select="$locale/shop/category/features/in_short_description/text()"/></b></td>
			<td><b><xsl:value-of select="$locale/shop/category/features/match_analog/text()"/></b></td>
			<td><b><xsl:value-of select="$locale/shop/category/features/is_numeric/text()"/></b></td>
			<td><b><xsl:value-of select="$locale/shop/category/features/position/text()"/></b></td>
			<td><b><xsl:value-of select="$locale/shop/category/features/new_name/text()"/></b></td>
			<td></td>
			<td width="2%"></td>
			<td width="2%"></td>
		</tr>

		<xsl:apply-templates select="features/*">
			<xsl:with-param name="category_id" select="category/id"/>
		</xsl:apply-templates>
		
		<form name="features" id="features" action="?{$controller_name}.feature_add" method="post">
			<input type="hidden" name="category_id" value="{category/id}"/>
			<input type="hidden" name="object[lang]" value="{$lang}"/>
			<input type="hidden" name="object[category_id]" value="{category/id}"/>
		<tr>
			<th class="actions"></th>
			<th class="actions" style="text-align:left;padding:0"><input type="checkbox" name="object[show_in_filter]" value="1"/></th>
			<th class="actions" style="text-align:left;padding:0"><input type="checkbox" name="object[show_in_short_description]" value="1"/></th>
			<th class="actions" style="text-align:left;padding:0"><input type="checkbox" name="object[match_analog]" value="1"/></th>
			<th class="actions" style="text-align:left;padding:0"><input type="checkbox" name="object[is_numeric]" value="1"/></th>
			<th class="actions" style="text-align:left;padding:0"><input type="text" name="object[position]" class="small" style="width:60px;"/></th>
			<th class="actions" style="text-align:left;padding:0"><input type="text" name="object[name]" class="middle"/></th>
			<th class="actions" style="text-align:left;padding:0"><input type="submit" value="{$locale/common/buttons/form_add/text()}"/></th>
			<th class="actions"></th>
			<th class="actions"></th>
		</tr>
		</form>
	</table>
	
	<script language="JavaScript" src="{$views_path}/article.js"></script>
	
</xsl:template>

<xsl:template match="feature">
	<xsl:param name="category_id"/>
	
	<form name="features" id="features" action="?{$controller_name}.feature_save" method="post" style="margin:0;padding:0">
		<input type="hidden" name="category_id" value="{$category_id}"/>
		<input type="hidden" name="object[id]" value="{id}"/>
		<input type="hidden" name="object[lang]" value="{$lang}"/>
	<tr>
		<td class="row"><xsl:value-of select="name"/></td>
		<td class="row">
			<xsl:choose>
				<xsl:when test="show_in_filter = 1">
					<input type="checkbox" name="object[show_in_filter]" value="1" checked="yes"/>
				</xsl:when>
				<xsl:otherwise>
					<input type="checkbox" name="object[show_in_filter]" value="1"/>
				</xsl:otherwise>
			</xsl:choose>
		</td>
		<td class="row">
			<xsl:choose>
				<xsl:when test="show_in_short_description = 1">
					<input type="checkbox" name="object[show_in_short_description]" value="1" checked="yes"/>
				</xsl:when>
				<xsl:otherwise>
					<input type="checkbox" name="object[show_in_short_description]" value="1"/>
				</xsl:otherwise>
			</xsl:choose>
		</td>
		<td class="row">
			<xsl:choose>
				<xsl:when test="match_analog = 1">
					<input type="checkbox" name="object[match_analog]" value="1" checked="yes"/>
				</xsl:when>
				<xsl:otherwise>
					<input type="checkbox" name="object[match_analog]" value="1"/>
				</xsl:otherwise>
			</xsl:choose>
		</td>
		<td class="row">
			<xsl:choose>
				<xsl:when test="is_numeric = 1">
					<input type="checkbox" name="object[is_numeric]" value="1" checked="yes"/>
				</xsl:when>
				<xsl:otherwise>
					<input type="checkbox" name="object[is_numeric]" value="1"/>
				</xsl:otherwise>
			</xsl:choose>
		</td>
		<td class="row"><input type="text" name="object[position]" value="{position}" class="small" style="width:60px"/></td>
		<td class="row"><input type="text" name="object[name]" value="{name}" class="middle"/></td>
		<td class="row"><input type="submit" value="{$locale/common/buttons/form_save/text()}"/></td>
		<td width="2%" class="row"><a href="?{$controller_name}.feature_values&amp;id={id}&amp;category_id={$category_id}"><img  src="{$views_path}/images/edit.gif" border="0"/></a></td>
		<td width="2%" class="row"><a href="#" onclick="del_item('?{$controller_name}.feature_del&amp;id={id}&amp;category_id={$category_id}','{$locale/common/msgs/delete_item/text()}')"><img  src="{$views_path}/images/del.gif" border="0"/></a></td>
	</tr>
	</form>
</xsl:template>

</xsl:stylesheet>

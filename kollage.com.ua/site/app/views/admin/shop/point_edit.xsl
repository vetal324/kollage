<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:msxsl="urn:schemas-microsoft-com:xslt"
				xmlns:exsl="http://exslt.org/common"
				extension-element-prefixes="exsl"
				version="1.0">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:include href="left_panel.xsl" />

<xsl:template match="data">
	<xsl:choose>
		<xsl:when test="/content/global/user/roles/role[code='shop_products']/@selected = 'yes'">
			<xsl:apply-templates/>
		</xsl:when>
		<xsl:otherwise>
			<div class="error"><xsl:value-of select="$locale/common/msgs/access_denied/text()"/></div>
			<script type="text/javascript">RemoveSmallTabs();RemoveLeftPanel();</script>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="edit">

	<xsl:variable name="title">
		<xsl:choose>
			<xsl:when test="point/id &gt; 0"><xsl:value-of select="$locale/sop/points/edit/title1/text()"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="$locale/sop/points/edit/title2/text()"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	
	<xsl:variable name="id" select="point/id"/>
	<xsl:variable name="name" select="point/name"/>
	<xsl:variable name="address" select="point/address"/>
	<xsl:variable name="phone" select="point/phone"/>
	<xsl:variable name="email" select="point/email"/>
	<xsl:variable name="position"><xsl:value-of select="point/position"/></xsl:variable>

	<form name="point_edit" id="point_edit" action="?{$controller_name}.point_save" method="post">
		<input type="hidden" name="id" value="{$id}"/>
		<input type="hidden" name="last_position" value="{last_position}"/>
		<input type="hidden" name="object[id]" value="{$id}"/>
		<input type="hidden" name="object[lang]" value="{$lang}"/>
		<input type="hidden" name="object[position]" value="{$position}"/>
	<table class="form" cellspacing="0" cellpadding="0">
	
		<!-- Show message -->
		<xsl:if test="save_result = 'true'">
		<tr><td colspan="2"><div class="message"><xsl:value-of select="$locale/sop/points/edit/save_msg_success/text()"/></div></td></tr>
		</xsl:if>
		<xsl:if test="save_result = 'false'">
		<tr><td colspan="2"><div class="error"><xsl:value-of select="$locale/sop/points/edit/save_msg_error/text()"/></div></td></tr>
		</xsl:if>
		<!-- /Show message -->
		
		<tr><th colspan="2">
			<table width="100%"><tr>
				<th><xsl:value-of select="$title"/></th>
				<td align="right"><input type="button" value="{$locale/sop/points/buttons/clear/text()}" onclick="document.location='?{$controller_name}.point_clear&amp;id={point/id}'"/></td>
			</tr></table>
		</th></tr>
		
		<tr>
			<td class="left_label"><label for="name"><xsl:value-of select="$locale/sop/points/edit/name/text()"/></label></td>
			<td><input type="text" class="large" name="object[name]" id="name" value="{$name}"/></td>
		</tr>
		
		<tr>
			<td class="left_label"><label for="group_id"><xsl:value-of select="$locale/sop/points/edit/group/text()"/></label></td>
			<td>
				<xsl:variable name="selected" select="point/group_id"/>
				<select name="object[group_id]" id="group_id">
					<xsl:call-template name="gen-option">
						<xsl:with-param name="value" select="1"/>
						<xsl:with-param name="title" select="$locale/sop/points/edit/groups/sumy/text()"/>
						<xsl:with-param name="selected" select="$selected"/>
					</xsl:call-template>
					<xsl:call-template name="gen-option">
						<xsl:with-param name="value" select="2"/>
						<xsl:with-param name="title" select="$locale/sop/points/edit/groups/external/text()"/>
						<xsl:with-param name="selected" select="$selected"/>
					</xsl:call-template>
				</select>
			</td>
		</tr>
		
		<tr>
			<td class="left_label"><label for="address"><xsl:value-of select="$locale/sop/points/edit/address/text()"/></label></td>
			<td><input type="text" class="large" name="object[address]" id="address" value="{$address}"/></td>
		</tr>
		
		<tr>
			<td class="left_label"><label for="phone"><xsl:value-of select="$locale/sop/points/edit/phone/text()"/></label></td>
			<td><input type="text" class="middle" name="object[phone]" id="phone" value="{$phone}"/></td>
		</tr>
		
		<tr>
			<td class="left_label"><label for="email"><xsl:value-of select="$locale/sop/points/edit/email/text()"/></label></td>
			<td><input type="text" class="large" name="object[email]" id="email" value="{$email}"/></td>
		</tr>
		
		<!--
		<tr>
			<td class="left_label"><label for="order_prefix"><xsl:value-of select="$locale/sop/points/edit/order_prefix/text()"/></label></td>
			<td><input type="text" class="small" name="object[order_prefix]" id="order_prefix" value="{point/order_prefix}"/></td>
		</tr>
		-->
		
		<tr><th colspan="2" class="actions"><input type="submit" value="{$locale/common/buttons/form_save/text()}"/></th></tr>
	</table>
		
	</form>
	
	<script language="JavaScript" src="{$views_path}/article.js"></script>
	
</xsl:template>

</xsl:stylesheet>

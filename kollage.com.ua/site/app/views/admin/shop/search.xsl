<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" version="1.0">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout.xsl" />
<xsl:include href="../menu.xsl" />
<xsl:include href="../paginator.xsl" />

<xsl:include href="left_panel.xsl" />

<xsl:variable name="redirect" select="$http_request_uri"/>

<xsl:template match="data">
	<xsl:choose>
		<xsl:when test="/content/global/user/roles/role[code='shop_products']/@selected = 'yes'">
			<xsl:apply-templates select="products"/>
		</xsl:when>
		<xsl:otherwise>
			<div class="error"><xsl:value-of select="$locale/common/msgs/access_denied/text()"/></div>
			<script type="text/javascript">RemoveSmallTabs();RemoveLeftPanel();</script>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="products">
<xsl:variable name="folder" select="@folder"/>
<xsl:variable name="category_id" select="@category_id"/>
<xsl:variable name="rows" select="count(./product)"/>

	<table class="paginator"><tr><th>
		<xsl:call-template name="paginator">
			<xsl:with-param name="prefix">top</xsl:with-param>
		</xsl:call-template>
	</th></tr></table>
	
	<table class="list" id="list">
		<tr>
			<th width="2%" style="text-align:center"><a href="#" style="color:blue" onclick="article_select_all();return(false)">#</a></th>
			<th width="1%"></th>
			<th width="*"><xsl:value-of select="$locale/shop/product/list/header/text()"/></th>
			<th width="10%"><xsl:value-of select="$locale/shop/product/list/number/text()"/></th>
			<th width="10%" style="text-align:right"><xsl:value-of select="$locale/shop/product/list/price/text()"/></th>
			<th width="10%" style="text-align:right"><xsl:value-of select="$locale/shop/product/list/price_opt/text()"/></th>
			<th width="10%" style="text-align:right"><xsl:value-of select="$locale/shop/product/list/price_opt2/text()"/></th>
			<th width="4%" style="text-align:center"><xsl:value-of select="$locale/shop/product/list/show_in_shop_news/text()"/></th>
			<th width="4%" style="text-align:center"><xsl:value-of select="$locale/shop/product/list/show_in_best/text()"/></th>
			<th width="4%" style="text-align:center"><xsl:value-of select="$locale/shop/product/list/is_new/text()"/></th>
			<th width="4%" style="text-align:center"><xsl:value-of select="$locale/shop/product/list/is_action/text()"/></th>
			<!--<th width="15%"><xsl:value-of select="$locale/shop/product/list/created_at/text()"/></th>-->
			<th width="2%" style="text-align:center">&#160;</th>
			<th width="2%" style="text-align:center">&#160;</th>
		</tr>
		
		<xsl:choose>
			<xsl:when test="product">
				<form method="post" name="save" id="save">
					<input type="hidden" name="action" value="save"/>
					<input type="hidden" name="redirect" value="{$redirect}"/>
				<xsl:apply-templates select="product">
					<xsl:with-param name="folder" select="@folder"/>
				</xsl:apply-templates>
				</form>
			</xsl:when>
			<xsl:otherwise>
				<tr><td colspan="20" align="center"><xsl:value-of select="$locale/shop/product/list/insert_msg/text()"/></td></tr>
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:variable name="btn_save" select="$locale/shop/product/buttons/save/text()"/>
		<xsl:variable name="btn_delete_marked" select="$locale/shop/product/buttons/delete_marked/text()"/>
		<xsl:variable name="btn_add_new" select="$locale/shop/product/buttons/add_new/text()"/>
		
		<tr>
			<th class="actions" colspan="20">
				<table align="right"><tr>
					<td><input type="button" value="{$btn_save}" onclick="$('save').submit()"/></td>
					<td><input type="button" value="{$btn_delete_marked}" onclick="article_delete_marked('?{$controller_name}.del_marked&amp;folder={$folder}&amp;category_id={$category_id}&amp;page={$page}&amp;redirect={$redirect}','{$locale/common/msgs/delete_items/text()}')"/></td>
					<td><input type="button" value="{$btn_add_new}" onclick="document.location='?{$controller_name}.edit&amp;folder={$folder}&amp;category_id={$category_id}&amp;last_position={last_position}&amp;redirect={$redirect}'"/></td>
				</tr></table>
			</th>
		</tr>
	</table>
	
	<table class="paginator"><tr><th>
		<xsl:call-template name="paginator">
			<xsl:with-param name="prefix">bottom</xsl:with-param>
		</xsl:call-template>
	</th></tr></table>
	
	<p><xsl:value-of select="$locale/shop/product/list/help/text()"/></p>
	
	<xsl:if test="../parameters = 'scroll_to_list'">
		<script>ScrollToObject('list')</script>
	</xsl:if>
	
	<script language="JavaScript" src="{$views_path}/paginator.js"></script>
	<script language="JavaScript" src="{$views_path}/article.js"></script>
	
	<script type="text/javascript" language="javascript" charset="utf-8" src="{$views_path}/../shared/prototype.js"></script>
	<script type="text/javascript" language="javascript" charset="utf-8" src="{$views_path}/../shared/scriptaculos/scriptaculous.js"></script>
	<script type="text/javascript" language="javascript" charset="utf-8" src="{$views_path}/../shared/scriptaculos/effects.js"></script>
	<script type="text/javascript" language="javascript" charset="utf-8" src="{$views_path}/../shared/scriptaculos/dragdrop.js"></script>
	<script type="text/javascript" language="javascript" charset="utf-8" src="{$views_path}/dnd.js"></script>
	<script type="text/javascript" language="javascript">
		DefineDnDPosition( <xsl:value-of select="$rows"/> );

		function ChangePosition( src, dst )
		{
			src_id = $F( 'cid'+src ); dst_id = $F( 'cid'+dst );
			document.location = '?<xsl:value-of select="$controller_name"/>.swap_position&amp;src='+ src_id +'&amp;dst='+ dst_id +'&amp;page=<xsl:value-of select="$page"/>&amp;pages=<xsl:value-of select="$pages"/>';
		}
	</script>
	
</xsl:template>

<xsl:template match="product">
  <xsl:param name="folder"/>
  
	<xsl:variable name="id" select="id"/>
	
	<tr id="row{position()}">
		<td class="row"><input type="checkbox" name="del_item{position()}" id="del_item{position()}" value="{$id}"/></td>
		<td>
			<xsl:choose>
				<xsl:when test="status = 1">
					<img src="{$views_path}/images/a.jpg" border="0" width="7" height="7" onclick="document.location='?shop.set_status&amp;page={$page}&amp;id={id}&amp;v=0&amp;redirect={$redirect}'" style="cursor:pointer"/>
				</xsl:when>
				<xsl:otherwise>
					<img src="{$views_path}/images/i.jpg" border="0" width="7" height="7" onclick="document.location='?shop.set_status&amp;page={$page}&amp;id={id}&amp;v=1&amp;redirect={$redirect}'" style="cursor:pointer"/>
				</xsl:otherwise>
			</xsl:choose>
		</td>
		<td class="row"><div class="cell" id="c{position()}"><xsl:value-of select="name" disable-output-escaping="yes"/><input type="hidden" name="cid{position()}" id="cid{position()}" value="{id}"/></div></td>
		<td class="row"><input type="text" class="small2" name="number{position()}" value="{number}"/></td>
		<td class="row" align="right"><input type="text" class="small" name="price{position()}" value="{price}"/></td>
		<td class="row" align="right"><input type="text" class="small" name="price_opt{position()}" value="{price_opt}"/></td>
		<td class="row" align="right"><input type="text" class="small" name="price_opta{position()}" value="{price_opt2}"/></td>
		
		<td class="row" align="center">
			<xsl:choose>
				<xsl:when test="show_in_shop_news = 1">
					<img src="{$views_path}/images/a.jpg" border="0" width="7" height="7" onclick="document.location='?shop.set_show_in_shop_news&amp;page={$page}&amp;id={id}&amp;v=0&amp;redirect={$redirect}'" style="cursor:pointer"/>
				</xsl:when>
				<xsl:otherwise>
					<img src="{$views_path}/images/d.jpg" border="0" width="7" height="7" onclick="document.location='?shop.set_show_in_shop_news&amp;page={$page}&amp;id={id}&amp;v=1&amp;redirect={$redirect}'" style="cursor:pointer"/>
				</xsl:otherwise>
			</xsl:choose>
		</td>
		
		<td class="row" align="center">
			<xsl:choose>
				<xsl:when test="show_in_best = 1">
					<img src="{$views_path}/images/a.jpg" border="0" width="7" height="7" onclick="document.location='?shop.set_show_in_best&amp;page={$page}&amp;id={id}&amp;v=0&amp;redirect={$redirect}'" style="cursor:pointer"/>
				</xsl:when>
				<xsl:otherwise>
					<img src="{$views_path}/images/d.jpg" border="0" width="7" height="7" onclick="document.location='?shop.set_show_in_best&amp;page={$page}&amp;id={id}&amp;v=1&amp;redirect={$redirect}'" style="cursor:pointer"/>
				</xsl:otherwise>
			</xsl:choose>
		</td>
		
		<td class="row" align="center">
			<xsl:choose>
				<xsl:when test="is_new = 1">
					<img src="{$views_path}/images/a.jpg" border="0" width="7" height="7" onclick="document.location='?shop.set_is_new&amp;page={$page}&amp;id={id}&amp;v=0&amp;redirect={$redirect}'" style="cursor:pointer"/>
				</xsl:when>
				<xsl:otherwise>
					<img src="{$views_path}/images/d.jpg" border="0" width="7" height="7" onclick="document.location='?shop.set_is_new&amp;page={$page}&amp;id={id}&amp;v=1&amp;redirect={$redirect}'" style="cursor:pointer"/>
				</xsl:otherwise>
			</xsl:choose>
		</td>
		
		<td class="row" align="center">
			<xsl:choose>
				<xsl:when test="is_action = 1">
					<img src="{$views_path}/images/a.jpg" border="0" width="7" height="7" onclick="document.location='?shop.set_is_action&amp;page={$page}&amp;id={id}&amp;v=0&amp;redirect={$redirect}'" style="cursor:pointer"/>
				</xsl:when>
				<xsl:otherwise>
					<img src="{$views_path}/images/d.jpg" border="0" width="7" height="7" onclick="document.location='?shop.set_is_action&amp;page={$page}&amp;id={id}&amp;v=1&amp;redirect={$redirect}'" style="cursor:pointer"/>
				</xsl:otherwise>
			</xsl:choose>
		</td>
		
		<!--<td class="row"><xsl:value-of select="created_at"/></td>-->
		
		<td class="row" align="center"><a title="{$locale/common/edit/text()}" href="?shop.edit&amp;id={$id}&amp;folder={$folder}&amp;page={$page}&amp;redirect={$redirect}"><img  src="{$views_path}/images/edit.gif" border="0"/></a></td>
		<td class="row" align="center"><a title="{$locale/common/del/text()}" href="#" onclick="article_delete('?shop.del&amp;id={id}&amp;category_id={category_id}&amp;folder={$folder}&amp;page={$page}&amp;redirect={$redirect}','{$locale/common/msgs/delete_item/text()}')"><img  src="{$views_path}/images/del.gif" border="0"/></a></td>
	</tr>
</xsl:template>

</xsl:stylesheet>

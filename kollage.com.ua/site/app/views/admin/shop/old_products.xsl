<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" version="1.0">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout.xsl" />
<xsl:include href="../menu.xsl" />
<xsl:include href="../paginator.xsl" />

<xsl:include href="left_panel.xsl" />

<xsl:template match="data">
	<xsl:choose>
		<xsl:when test="/content/global/user/roles/role[code='shop_products']/@selected = 'yes'">
			<xsl:apply-templates select="products"/>
		</xsl:when>
		<xsl:otherwise>
			<div class="error"><xsl:value-of select="$locale/common/msgs/access_denied/text()"/></div>
			<script type="text/javascript">RemoveSmallTabs();RemoveLeftPanel();</script>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="products">
<xsl:variable name="folder" select="@folder"/>
<xsl:variable name="category_id" select="@category_id"/>
<xsl:variable name="rows" select="count(./product)"/>

	<form method="get" action="">
	<input type="hidden" name="{$controller_name}.old_products" value=""/>
	<table><tr>
		<td><xsl:value-of select="$locale/shop/product/list/monthes/text()"/></td>
		<td><input type="text" class="small" name="monthes" value="{monthes}"/></td>
		<td>
			<select name="category_id" id="category_id">
				<option value="0"><xsl:value-of select="$locale/common/all"/></option>
				<xsl:apply-templates select="categories/*">
					<xsl:with-param name="category_id" select="@category_id"/>
				</xsl:apply-templates>
			</select>
		</td>
		<td><input type="submit" value="{$locale/common/buttons/form_select/text()}"/></td>
	</tr></table>
	</form>

	<table class="paginator"><tr><th>
		<xsl:call-template name="paginator">
			<xsl:with-param name="prefix">top</xsl:with-param>
		</xsl:call-template>
	</th></tr></table>
	
	<table class="list" id="list">
		<tr>
			<th width="2%">#</th>
			<th width="*"><xsl:value-of select="$locale/shop/product/list/header/text()"/></th>
			<th width="7%"><xsl:value-of select="$locale/shop/product/list/number/text()"/></th>
			<th width="10%" style="text-align:right"><xsl:value-of select="$locale/shop/product/list/price/text()"/></th>
			<th width="10%" style="text-align:right"><xsl:value-of select="$locale/shop/product/list/price_opt/text()"/></th>
			<th width="10%" style="text-align:right"><xsl:value-of select="$locale/shop/product/list/price_opt2/text()"/></th>
			<th width="15%"><xsl:value-of select="$locale/shop/product/list/created_at/text()"/></th>
			<th width="15%"><xsl:value-of select="$locale/shop/product/list/updated_at/text()"/></th>
			<th width="2%" style="text-align:center">&#160;</th>
			<th width="2%" style="text-align:center">&#160;</th>
		</tr>
		
		<xsl:choose>
			<xsl:when test="product">
				<form method="post" name="save" id="save">
					<input type="hidden" name="action" value="save"/>
				<xsl:apply-templates select="product">
					<xsl:with-param name="folder" select="@folder"/>
				</xsl:apply-templates>
				</form>
			</xsl:when>
			<xsl:otherwise>
				<tr><td colspan="20" align="center"><xsl:value-of select="$locale/shop/product/list/insert_msg/text()"/></td></tr>
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:variable name="btn_save" select="$locale/shop/product/buttons/save/text()"/>
		<xsl:variable name="btn_delete_marked" select="$locale/shop/product/buttons/delete_marked/text()"/>
		<xsl:variable name="btn_add_new" select="$locale/shop/product/buttons/add_new/text()"/>
		
		<tr>
			<th class="actions" colspan="20">
				<table align="right"><tr>
					<td><input type="button" value="{$btn_delete_marked}" onclick="article_delete_marked('?{$controller_name}.del_marked&amp;folder={$folder}&amp;category_id={$category_id}&amp;page={$page}&amp;monthes={monthes}&amp;ret=old_product','{$locale/common/msgs/delete_items/text()}')"/></td>
				</tr></table>
			</th>
		</tr>
	</table>
	
	<table class="paginator"><tr><th>
		<xsl:call-template name="paginator">
			<xsl:with-param name="prefix">bottom</xsl:with-param>
		</xsl:call-template>
	</th></tr></table>
	
	<p><xsl:value-of select="$locale/shop/product/list/help/text()"/></p>
	
	<xsl:if test="../parameters = 'scroll_to_list'">
		<script>ScrollToObject('list')</script>
	</xsl:if>
	
	<script language="JavaScript" src="{$views_path}/paginator.js"></script>
	<script language="JavaScript" src="{$views_path}/article.js"></script>
	
</xsl:template>

<xsl:template match="product">
  <xsl:param name="folder"/>
  
	<xsl:variable name="id" select="id"/>
	
	<tr id="row{position()}">
		<td class="row"><input type="checkbox" name="del_item{position()}" id="del_item{position()}" value="{$id}"/></td>
		<td class="row"><div class="cell" id="c{position()}"><xsl:value-of select="name"/><input type="hidden" name="cid{position()}" id="cid{position()}" value="{id}"/></div></td>
		<td class="row"><xsl:value-of select="number"/></td>
		<td class="row" align="right"><xsl:value-of select="price"/></td>
		<td class="row" align="right"><xsl:value-of select="price_opt"/></td>
		<td class="row" align="right"><xsl:value-of select="price_opt2"/></td>
		<td class="row"><xsl:value-of select="created_at"/></td>
		<td class="row"><xsl:value-of select="updated_at"/></td>
		
		<td class="row" align="center"><a title="{$locale/common/edit/text()}" href="?shop.edit&amp;id={$id}&amp;folder={$folder}&amp;page={$page}&amp;ret=old_product"><img  src="{$views_path}/images/edit.gif" border="0"/></a></td>
		<td class="row" align="center"><a title="{$locale/common/del/text()}" href="#" onclick="article_delete('?shop.del&amp;id={id}&amp;category_id={category_id}&amp;folder={$folder}&amp;page={$page}&amp;monthes={../monthes}&amp;ret=old_product','{$locale/common/msgs/delete_item/text()}')"><img  src="{$views_path}/images/del.gif" border="0"/></a></td>
	</tr>
</xsl:template>

<xsl:template match="category">
	<xsl:param name="category_id"/>
	<xsl:call-template name="gen-option">
		<xsl:with-param name="value" select="@id"/>
		<xsl:with-param name="title" select="concat(@prefix,name)"/>
		<xsl:with-param name="selected" select="$category_id"/>
	</xsl:call-template>
	<xsl:apply-templates select="category"/>
</xsl:template>

</xsl:stylesheet>

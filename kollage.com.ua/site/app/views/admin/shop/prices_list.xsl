<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:msxsl="urn:schemas-microsoft-com:xslt"
				xmlns:exsl="http://exslt.org/common"
				extension-element-prefixes="exsl"
				version="1.0">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:include href="left_panel.xsl" />

<xsl:template match="data">
	<xsl:choose>
		<xsl:when test="/content/global/user/roles/role[code='shop_products']/@selected = 'yes'">
			<xsl:apply-templates/>
		</xsl:when>
		<xsl:otherwise>
			<div class="error"><xsl:value-of select="$locale/common/msgs/access_denied/text()"/></div>
			<script type="text/javascript">RemoveSmallTabs();RemoveLeftPanel();</script>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="edit">

	<xsl:call-template name="tiny_mce"/>

	<xsl:variable name="title">
		<xsl:choose>
			<xsl:when test="settings/id &gt; 0"><xsl:value-of select="$locale/shop/settings/edit/title1/text()"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="$locale/shop/settings/edit/title2/text()"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<form name="prices_save" id="prices_save" action="?{$controller_name}.prices_save" method="post" enctype="multipart/form-data">
		<input type="hidden" name="id" value="1"/>
		<input type="hidden" name="object[id]" value="1"/>
		<input type="hidden" name="object[lang]" value="{$lang}"/>
	<table class="form" cellspacing="0" cellpadding="0">
	
		<!-- Show message -->
		<xsl:if test="save_result = 'true'">
		<tr><td colspan="2"><div class="message"><xsl:value-of select="$locale/shop/settings/edit/save_msg_success/text()"/></div></td></tr>
		</xsl:if>
		<xsl:if test="save_result = 'false'">
		<tr><td colspan="2"><div class="error"><xsl:value-of select="$locale/shop/settings/edit/save_msg_error/text()"/></div></td></tr>
		</xsl:if>
		<!-- /Show message -->
		
		<tr><th colspan="2"><xsl:value-of select="$title"/></th></tr>
		
		<tr>
			<td class="left_label"><label for="file_price"><xsl:value-of select="$locale/shop/settings/prices/file_price/text()"/></label></td>
			<td>
				<table cellspacing="0" cellpadding="0"><tr>
					<td><input type="file" name="file_price"/></td>
					<td><xsl:if test="settings/file_price != ''">&#160;&#160;<a href="#" onclick="del_item('?{$controller_name}.price_del_file_price&amp;id={settings/id}','{$locale/common/msgs/delete_item/text()}')"><img  src="{$views_path}/images/del.gif" border="0"/></a></xsl:if></td>
				</tr></table>
			</td>
		</tr>
		
		<tr>
			<td class="left_label"><label for="file_price_opt"><xsl:value-of select="$locale/shop/settings/prices/file_price_opt/text()"/></label></td>
			<td>
				<table cellspacing="0" cellpadding="0"><tr>
					<td><input type="file" name="file_price_opt"/></td>
					<td><xsl:if test="settings/file_price_opt != ''">&#160;&#160;<a href="#" onclick="del_item('?{$controller_name}.price_del_file_price_opt&amp;id={settings/id}','{$locale/common/msgs/delete_item/text()}')"><img  src="{$views_path}/images/del.gif" border="0"/></a></xsl:if></td>
				</tr></table>
			</td>
		</tr>
		
		<tr>
			<td class="left_label"><label for="file_price_opt2"><xsl:value-of select="$locale/shop/settings/prices/file_price_opt2/text()"/></label></td>
			<td>
				<table cellspacing="0" cellpadding="0"><tr>
					<td><input type="file" name="file_price_opt2"/></td>
					<td><xsl:if test="settings/file_price_opt2 != ''">&#160;&#160;<a href="#" onclick="del_item('?{$controller_name}.price_del_file_price_opt&amp;id={settings/id}','{$locale/common/msgs/delete_item/text()}')"><img  src="{$views_path}/images/del.gif" border="0"/></a></xsl:if></td>
				</tr></table>
			</td>
		</tr>
		
		<tr><th colspan="2" class="actions"><input type="submit" value="{$locale/common/buttons/form_save/text()}"/></th></tr>
	</table>
		
	</form>
	
	<script language="JavaScript" src="{$views_path}/article.js"></script>
	
</xsl:template>

</xsl:stylesheet>

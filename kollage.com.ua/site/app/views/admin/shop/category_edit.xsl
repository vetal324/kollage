<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:msxsl="urn:schemas-microsoft-com:xslt"
				xmlns:exsl="http://exslt.org/common"
				extension-element-prefixes="exsl"
				version="1.0">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:include href="left_panel.xsl" />

<xsl:template match="data">
	<xsl:choose>
		<xsl:when test="/content/global/user/roles/role[code='shop_products']/@selected = 'yes'">
			<xsl:apply-templates/>
		</xsl:when>
		<xsl:otherwise>
			<div class="error"><xsl:value-of select="$locale/common/msgs/access_denied/text()"/></div>
			<script type="text/javascript">RemoveSmallTabs();RemoveLeftPanel();</script>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="edit">

	<xsl:variable name="title">
		<xsl:choose>
			<xsl:when test="category/id &gt; 0"><xsl:value-of select="$locale/shop/category/edit/title1/text()"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="$locale/shop/category/edit/title2/text()"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	
	<xsl:variable name="category_id" select="category/id"/>

	<form name="category_edit" id="category_edit" action="?{$controller_name}.category_save" method="post">
		<input type="hidden" name="id" value="1"/>
		<input type="hidden" name="object[id]" value="{category/id}"/>
		<input type="hidden" name="category_id" value="{category/id}"/>
		<input type="hidden" name="object[lang]" value="{$lang}"/>
	<table class="form" cellspacing="0" cellpadding="0">
	
		<!-- Show message -->
		<xsl:if test="save_result = 'true'">
		<tr><td colspan="2"><div class="message"><xsl:value-of select="$locale/shop/category/edit/save_msg_success/text()"/></div></td></tr>
		</xsl:if>
		<xsl:if test="save_result = 'false'">
		<tr><td colspan="2"><div class="error"><xsl:value-of select="$locale/shop/category/edit/save_msg_error/text()"/></div></td></tr>
		</xsl:if>
		<!-- /Show message -->
		
		<tr><th colspan="2"><xsl:value-of select="$title"/></th></tr>
		
		<tr>
			<td class="left_label"><label for="name"><xsl:value-of select="$locale/shop/category/edit/name/text()"/></label></td>
			<td><input type="text" class="large" name="object[name]" id="name" value="{category/name}"/></td>
		</tr>
		
		<tr>
			<td class="left_label"><label for="position"><xsl:value-of select="$locale/shop/category/edit/position/text()"/></label></td>
			<td><input type="text" class="small" name="object[position]" id="position" value="{category/position}"/></td>
		</tr>
		
		<tr><td class="label"><label for="link_to_category_id"><xsl:value-of select="$locale/shop/category/edit/link_to_category/text()"/></label></td>
		<td>
			<select name="object[link_to_category_id]" class="middle" id="link_to_category_id">
				<option value=""><xsl:value-of select="$locale/shop/category/edit/link_to_category_empty/text()"/></option>
				<xsl:apply-templates select="categories/category">
					<xsl:with-param name="mode" select="1"/>
					<xsl:with-param name="category_id" select="category/link_to_category_id"/>
				</xsl:apply-templates>
			</select>
		</td></tr>
		
		<tr>
			<td class="left_label"><label><xsl:value-of select="$locale/services/article/edit/show_in_menu/text()"/></label></td>
			<td>
			<xsl:choose>
				<xsl:when test="category/show_in_menu = 1"><input type="radio" name="object[show_in_menu]" id="show_in_menu_active" value="1" checked="yes" /></xsl:when>
				<xsl:otherwise><input type="radio" name="object[show_in_menu]" id="show_in_menu_active" value="1" /></xsl:otherwise>
			</xsl:choose>
			<label for="show_in_menu_active" class="lower"><xsl:value-of select="$locale/common/yes/text()"/></label>
			<xsl:choose>
				<xsl:when test="category/show_in_menu = 0"><input type="radio" name="object[show_in_menu]" id="show_in_menu_inactive" value="0" checked="yes"/></xsl:when>
				<xsl:otherwise><input type="radio" name="object[show_in_menu]" id="show_in_menu_inactive" value="0"/></xsl:otherwise>
			</xsl:choose>
			<label for="show_in_menu_inactive" class="lower"><xsl:value-of select="$locale/common/no/text()"/></label></td>
		</tr>
		
		<tr>
			<td class="left_label"><label for="page_title"><xsl:value-of select="$locale/shop/category/edit/page_title/text()"/></label></td>
			<td><input type="text" class="large" name="object[page_title]" id="page_title" value="{category/page_title}"/></td>
		</tr>
		
		<tr>
			<td class="left_label"><label for="page_keywords"><xsl:value-of select="$locale/shop/category/edit/page_keywords/text()"/></label></td>
			<td><input type="text" class="large" name="object[page_keywords]" id="page_keywords" value="{category/page_keywords}"/></td>
		</tr>
		
		<tr><th colspan="2" class="actions"><input type="submit" value="{$locale/common/buttons/form_save/text()}"/></th></tr>
		
		<tr><td colspan="2">&#160;</td></tr>
	</table>
	</form>
	
	<table class="form" cellspacing="0" cellpadding="0">
		<tr><th colspan="3" class="subheader"><xsl:value-of select="$locale/shop/category/edit/subcategories/title/text()"/></th></tr>

						<xsl:apply-templates select="subcategories/category">
							<xsl:with-param name="mode" select="2"/>
							<xsl:with-param name="category_id" select="category/id"/>
						</xsl:apply-templates>
		
		<tr><th colspan="3" class="actions">
			<form name="subcategory" id="subcategory" action="?{$controller_name}.subcategory_add" method="post">
				<input type="hidden" name="category_id" value="{category/id}"/>
				<input type="hidden" name="object[lang]" value="{$lang}"/>
			<table border="0" width="100%"><tr>
				<td width="20%"><xsl:value-of select="$locale/shop/category/edit/subcategories/parent/text()"/></td>
				<td width="10%">
					<select name="object[parent_id]">
						<xsl:apply-templates select="subcategories/category">
							<xsl:with-param name="mode" select="1"/>
							<xsl:with-param name="category_id" select="category/id"/>
						</xsl:apply-templates>
					</select>
				</td>
				<td width="*" align="left"><input type="text" name="object[name]" class="middle"/></td>
				<td width="10%" align="right"><input type="submit" value="{$locale/common/buttons/form_add/text()}"/></td>
			</tr></table>
			</form>
		</th></tr>
	</table>
	
	<script language="JavaScript" src="{$views_path}/article.js"></script>
	
</xsl:template>

<xsl:template match="category">
	<xsl:param name="mode"/>
	<xsl:param name="category_id"/>
	
	<xsl:choose>
		<xsl:when test="$mode = 1">
			<xsl:call-template name="gen-option">
				<xsl:with-param name="value" select="@id"/>
				<xsl:with-param name="title" select="concat(@prefix,name)"/>
				<xsl:with-param name="selected" select="$category_id"/>
			</xsl:call-template>
		</xsl:when>
		<xsl:otherwise>
			<tr>
				<td width="*" class="row"><xsl:value-of select="concat(@prefix,@prefix,@prefix,name)"/></td>
				<td width="30%" class="row">
					<form name="subcategory" id="subcategory" action="?{$controller_name}.subcategory_save" method="post" style="margin:0;padding:0">
						<input type="hidden" name="category_id" value="{$category_id}"/>
						<input type="hidden" name="object[id]" value="{@id}"/>
						<input type="hidden" name="object[lang]" value="{$lang}"/>
						<table cellspacing="0" cellpadding="0"><tr>
							<td><input type="text" name="object[name]" value="{name}" class="middle"/></td>
							<td><input type="submit" value="{$locale/common/buttons/form_save/text()}"/></td>
						</tr></table>
					</form>
				</td>
				<td width="2%" class="row"><a href="#" onclick="del_item('?{$controller_name}.subcategory_del&amp;id={@id}&amp;category_id={$category_id}','{$locale/common/msgs/delete_item/text()}')"><img  src="{$views_path}/images/del.gif" border="0"/></a></td>
			</tr>
		</xsl:otherwise>
	</xsl:choose>
	
	<xsl:apply-templates select="category">
		<xsl:with-param name="mode" select="$mode"/>
		<xsl:with-param name="category_id" select="$category_id"/>
	</xsl:apply-templates>
</xsl:template>

</xsl:stylesheet>


var productEdit = {
	options: {
		'pointsTitleId': 'points-title',
		'pointsListId': 'points-list'
	},
	
	init: function() {
		var self = this, o = this.options;
		
		$(o.pointsTitleId).observe( 'click', function() {
			self.pointsToggle();
		} );
	},
	
	pointsToggle: function() {
		var self = this, o = this.options;
		$(o.pointsListId).toggle();
		if( $(o.pointsTitleId).hasClassName('points-closed') ) {
			$(o.pointsTitleId).removeClassName('points-closed');
			$(o.pointsTitleId).addClassName('points-opened');
		}
		else {
			$(o.pointsTitleId).removeClassName('points-opened');
			$(o.pointsTitleId).addClassName('points-closed');
		}
	}
};

Event.observe(document, 'dom:loaded', function(){
	productEdit.init();
});

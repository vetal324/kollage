<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:msxsl="urn:schemas-microsoft-com:xslt"
				xmlns:exsl="http://exslt.org/common"
				extension-element-prefixes="exsl"
				version="1.0">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="left_panel.xsl" />

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:template match="data">
	<xsl:choose>
		<xsl:when test="/content/global/user/roles/role[code='shop_products']/@selected = 'yes'">
			<xsl:apply-templates/>
		</xsl:when>
		<xsl:otherwise>
			<div class="error"><xsl:value-of select="$locale/common/msgs/access_denied/text()"/></div>
			<script type="text/javascript">RemoveSmallTabs();RemoveLeftPanel();</script>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="product_edit">

	<xsl:call-template name="tiny_mce"/>
	<script type="text/javascript" language="javascript" charset="utf-8" src="{$views_path}/../shared/prototype.js"></script>
	<script type="text/javascript" language="javascript" charset="utf-8" src="{$views_path}/{$controller_name}/functions.js"></script>

	<xsl:variable name="title">
		<xsl:choose>
			<xsl:when test="product/id &gt; 0"><xsl:value-of select="$locale/shop/product/edit/title1/text()"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="$locale/shop/product/edit/title2/text()"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	
	<xsl:variable name="id" select="product/id"/>
	<xsl:variable name="category_id" select="product/category_id"/>
	<xsl:variable name="folder_for_child" select="product/folder_for_child"/>
	<xsl:variable name="position"><xsl:value-of select="product/position"/></xsl:variable>
	<xsl:variable name="name" select="product/name"/>
	<xsl:variable name="status" select="product/status"/>
	<xsl:variable name="design_type" select="product/design_type"/>
	
	<!--
	<form name="reload" id="reload" action="?{$controller_name}.edit" method="post">
		<input type="hidden" name="id" value="{$id}"/>
		<input type="hidden" name="aticle[category_id]" value="{$category_id}"/>
	</form>
	-->

	<form name="product_edit" id="reload" action="?{$controller_name}.save&amp;category_id={$category_id}" method="post" enctype="multipart/form-data">
		<input type="hidden" name="id" value="{$id}"/>
		<input type="hidden" name="last_position" value="{last_position}"/>
		<input type="hidden" name="product[id]" value="{$id}"/>
		<input type="hidden" name="product[category_id]" value="{$category_id}"/>
		<input type="hidden" name="product[position]" value="{$position}"/>
		<input type="hidden" name="product[lang]" value="{$lang}"/>
	<table class="form" cellspacing="0" cellpadding="0">
	
		<!-- Show message -->
		<xsl:if test="save_result = 'true'">
		<tr><td colspan="2"><div class="message"><xsl:value-of select="$locale/shop/product/edit/save_msg_success/text()"/></div></td></tr>
		</xsl:if>
		<xsl:if test="save_result = 'false'">
		<tr><td colspan="2"><div class="error"><xsl:value-of select="$locale/shop/product/edit/save_msg_error/text()"/></div></td></tr>
		</xsl:if>
		<!-- /Show message -->
		
		<tr><th colspan="2">
			<table width="100%" cellspacing="0" cellpadding="0"><tr>
				<th width="*"><xsl:value-of select="$title"/></th>
				<td width="30%">
					<table width="100%" cellspacing="0" cellpadding="2"><tr>
						<td><xsl:value-of select="$locale/shop/product/edit/move2category/text()"/></td>
						<td>
							<select name="change_category_id" id="change_category_id">
								<xsl:apply-templates select="categories/category"/>
							</select>
						</td>
						<td><input type="button" value="{$locale/common/buttons/form_move/text()}" onclick="document.location='?{$controller_name}.change_category&amp;id={product/id}&amp;category_id='+$('change_category_id').options[$('change_category_id').selectedIndex].value"/></td>
					</tr></table>
				</td>
			</tr></table>
		</th></tr>
		
		<tr>
			<td class="left_label"><label for="name"><xsl:value-of select="$locale/shop/product/edit/name/text()"/></label></td>
			<td><input type="text" class="large" name="product[name]" id="name" value="{$name}"/></td>
		</tr>
		
		<!--
		<tr><td class="label"><label for="brand_id"><xsl:value-of select="$locale/shop/product/edit/brand/text()"/></label></td>
		<td>
			<xsl:variable name="selected" select="product/brand_id"/>
			<select name="product[brand_id]" class="middle" id="brand_id">
				<xsl:for-each select="brands/*">
					<xsl:call-template name="gen-option">
						<xsl:with-param name="value" select="id"/>
						<xsl:with-param name="title" select="name"/>
						<xsl:with-param name="selected" select="$selected"/>
					</xsl:call-template>
				</xsl:for-each>
			</select>
		</td></tr>
		-->
		
		<tr>
			<td class="left_label"><label for="number"><xsl:value-of select="$locale/shop/product/edit/number/text()"/></label></td>
			<td><input type="text" class="large" name="product[number]" id="number" value="{product/number}"/></td>
		</tr>
		
		<tr>
			<td class="left_label"><label for="price"><xsl:value-of select="$locale/shop/product/edit/price/text()"/></label></td>
			<td><input type="text" class="small" name="product[price]" id="price" value="{product/price}"/></td>
		</tr>
		
		<tr>
			<td class="left_label"><label for="price_opt"><xsl:value-of select="$locale/shop/product/edit/price_opt/text()"/></label></td>
			<td><input type="text" class="small" name="product[price_opt]" id="price_opt" value="{product/price_opt}"/></td>
		</tr>
		
		<tr>
			<td class="left_label"><label for="price_opt2"><xsl:value-of select="$locale/shop/product/edit/price_opt2/text()"/></label></td>
			<td><input type="text" class="small" name="product[price_opt2]" id="price_opt2" value="{product/price_opt2}"/></td>
		</tr>
		
		<!--
		<tr><td colspan="2" class="label"><label for="ingress"><xsl:value-of select="$locale/shop/product/edit/ingress/text()"/></label></td></tr>
		<tr><td colspan="2"><textarea name="product[ingress]" id="ingress" class="large" rows="8" cols="80"><xsl:value-of select="product/ingress"/></textarea></td></tr>
		-->
		
		<!--
		<tr><td colspan="2" class="label"><label for="description"><xsl:value-of select="$locale/shop/product/edit/description/text()"/></label></td></tr>
		<tr><td colspan="2"><textarea name="product[description]" id="description" class="large" rows="15" cols="80"><xsl:value-of select="product/description"/></textarea></td></tr>
		-->
		
		<tr>
			<td class="left_label"><label for="nice_price"><xsl:value-of select="$locale/shop/product/edit/nice_price/text()"/></label></td>
			<td><input type="text" class="small" name="product[nice_price]" id="nice_price" value="{product/nice_price}"/></td>
		</tr>
		
		<tr>
			<td class="left_label"><label for="nice_price_start_at"><xsl:value-of select="$locale/shop/product/edit/nice_price_start_at/text()"/></label></td>
			<td><input type="text" class="middle" name="product[nice_price_start_at]" id="nice_price_start_at" value="{product/nice_price_start_at}"/></td>
		</tr>
		
		<tr>
			<td class="left_label"><label for="nice_price_end_at"><xsl:value-of select="$locale/shop/product/edit/nice_price_end_at/text()"/></label></td>
			<td><input type="text" class="middle" name="product[nice_price_end_at]" id="nice_price_end_at" value="{product/nice_price_end_at}"/></td>
		</tr>
		
		<!--
		<tr><td class="label"><label for="design_type"><xsl:value-of select="$locale/shop/product/edit/design_type/text()"/></label></td>
		<td>
			<xsl:variable name="options">
				<item id="1"><xsl:value-of select="$locale/shop/product/edit/design_types/simple/text()"/></item>
				<item id="2"><xsl:value-of select="$locale/shop/product/edit/design_types/extended/text()"/></item>
			</xsl:variable>
			<xsl:variable name="selected" select="product/design_type"/>
			<select name="product[design_type]" class="middle" id="design_type">
				<xsl:for-each select="exsl:node-set($options)/*">
					<xsl:call-template name="gen-option">
						<xsl:with-param name="value" select="@id"/>
						<xsl:with-param name="title" select="."/>
						<xsl:with-param name="selected" select="$selected"/>
					</xsl:call-template>
				</xsl:for-each>
			</select>
		</td></tr>
		-->
		
		<tr>
			<td class="left_label"><label><xsl:value-of select="$locale/shop/product/edit/status/text()"/></label></td>
			<td>
			<xsl:choose>
				<xsl:when test="$status = 1"><input type="radio" name="product[status]" id="status_active" value="1" checked="yes" /></xsl:when>
				<xsl:otherwise><input type="radio" name="product[status]" id="status_active" value="1" /></xsl:otherwise>
			</xsl:choose>
			<label for="status_active" class="lower"><xsl:value-of select="$locale/shop/product/edit/statuses/active/text()"/></label>
			<xsl:choose>
				<xsl:when test="$status = 0"><input type="radio" name="product[status]" id="status_inactive" value="0" checked="yes"/></xsl:when>
				<xsl:otherwise><input type="radio" name="product[status]" id="status_inactive" value="0"/></xsl:otherwise>
			</xsl:choose>
			<label for="status_inactive" class="lower"><xsl:value-of select="$locale/shop/product/edit/statuses/inactive/text()"/></label></td>
		</tr>
		
		<tr>
			<td class="left_label"><label><xsl:value-of select="$locale/shop/product/edit/show_in_shop_news/text()"/></label></td>
			<td>
			<xsl:choose>
				<xsl:when test="product/show_in_shop_news = 1"><input type="radio" name="product[show_in_shop_news]" id="show_in_shop_news_active" value="1" checked="yes" /></xsl:when>
				<xsl:otherwise><input type="radio" name="product[show_in_shop_news]" id="show_in_shop_news_active" value="1" /></xsl:otherwise>
			</xsl:choose>
			<label for="show_in_shop_news_active" class="lower"><xsl:value-of select="$locale/common/yes/text()"/></label>
			<xsl:choose>
				<xsl:when test="product/show_in_shop_news = 0"><input type="radio" name="product[show_in_shop_news]" id="show_in_shop_news_inactive" value="0" checked="yes"/></xsl:when>
				<xsl:otherwise><input type="radio" name="product[show_in_shop_news]" id="show_in_shop_news_inactive" value="0"/></xsl:otherwise>
			</xsl:choose>
			<label for="show_in_shop_news_inactive" class="lower"><xsl:value-of select="$locale/common/no/text()"/></label></td>
		</tr>
		
		<tr>
			<td class="left_label"><label><xsl:value-of select="$locale/shop/product/edit/show_in_best/text()"/></label></td>
			<td>
			<xsl:choose>
				<xsl:when test="product/show_in_best = 1"><input type="radio" name="product[show_in_best]" id="show_in_best_active" value="1" checked="yes" /></xsl:when>
				<xsl:otherwise><input type="radio" name="product[show_in_best]" id="show_in_best_active" value="1" /></xsl:otherwise>
			</xsl:choose>
			<label for="show_in_best_active" class="lower"><xsl:value-of select="$locale/common/yes/text()"/></label>
			<xsl:choose>
				<xsl:when test="product/show_in_best = 0"><input type="radio" name="product[show_in_best]" id="show_in_best_inactive" value="0" checked="yes"/></xsl:when>
				<xsl:otherwise><input type="radio" name="product[show_in_best]" id="show_in_best_inactive" value="0"/></xsl:otherwise>
			</xsl:choose>
			<label for="show_in_best_inactive" class="lower"><xsl:value-of select="$locale/common/no/text()"/></label></td>
		</tr>
		
		<tr>
			<td class="left_label"><label><xsl:value-of select="$locale/shop/product/edit/is_new/text()"/></label></td>
			<td>
			<xsl:choose>
				<xsl:when test="product/is_new = 1"><input type="radio" name="product[is_new]" id="is_new_active" value="1" checked="yes" /></xsl:when>
				<xsl:otherwise><input type="radio" name="product[is_new]" id="is_new_active" value="1" /></xsl:otherwise>
			</xsl:choose>
			<label for="is_new_active" class="lower"><xsl:value-of select="$locale/common/yes/text()"/></label>
			<xsl:choose>
				<xsl:when test="product/is_new = 0"><input type="radio" name="product[is_new]" id="is_new_inactive" value="0" checked="yes"/></xsl:when>
				<xsl:otherwise><input type="radio" name="product[is_new]" id="is_new_inactive" value="0"/></xsl:otherwise>
			</xsl:choose>
			<label for="is_new_inactive" class="lower"><xsl:value-of select="$locale/common/no/text()"/></label></td>
		</tr>
		
		<tr>
			<td class="left_label"><label><xsl:value-of select="$locale/shop/product/edit/is_action/text()"/></label></td>
			<td>
			<xsl:choose>
				<xsl:when test="product/is_action = 1"><input type="radio" name="product[is_action]" id="is_action_active" value="1" checked="yes" /></xsl:when>
				<xsl:otherwise><input type="radio" name="product[is_action]" id="is_action_active" value="1" /></xsl:otherwise>
			</xsl:choose>
			<label for="is_action_active" class="lower"><xsl:value-of select="$locale/common/yes/text()"/></label>
			<xsl:choose>
				<xsl:when test="product/is_action = 0"><input type="radio" name="product[is_action]" id="is_action_inactive" value="0" checked="yes"/></xsl:when>
				<xsl:otherwise><input type="radio" name="product[is_action]" id="is_action_inactive" value="0"/></xsl:otherwise>
			</xsl:choose>
			<label for="is_action_inactive" class="lower"><xsl:value-of select="$locale/common/no/text()"/></label></td>
		</tr>
		
		<tr><th colspan="2" id="points-title" class="actions points-closed" style="text-align:left;font-weight:normal"><label for="points"><xsl:value-of select="$locale/shop/product/edit/points/text()"/></label></th></tr>
		<tr id="points-list" style="display:none"><td colspan="2">
			<table width="100%">
				<xsl:apply-templates select="points/*"/>
			</table>
		</td></tr>
		
		<tr><td colspan="2">&#160;</td></tr>
		
		<tr><th colspan="2" class="actions"><input type="button" onclick="document.location='?{$controller_name}.index&amp;category_id={product/category_id}&amp;page={$page}'" value="{$locale/common/buttons/form_back/text()}"/>&#160;<input type="submit" value="{$locale/common/buttons/form_save/text()}"/></th></tr>
	</table>
		
	</form>
	
	<table class="form" cellspacing="0" cellpadding="0" id="features">
		<!-- Features box -->
		<xsl:if test="product/id &gt; 0">
			<form action="?{$controller_name}.save_features&amp;id={product/id}&amp;category_id={$category_id}" method="post">
			<tr><td colspan="2">&#160;</td></tr>
			<tr><th colspan="2"><xsl:value-of select="$locale/shop/product/edit/features_title/text()"/></th></tr>
			<tr><td colspan="2">
				<table width="100%">
				<xsl:apply-templates select="features/*"/>
				<xsl:apply-templates select="product/individual_features/*"/>
				
				<tr>
					<td><input type="text" class="middle" name="individual_name1"/></td>
					<td colspan="2"><input type="text" class="middle" name="individual_value1"/> <xsl:value-of select="$locale/shop/product/edit/individual_features_note"/></td>
				</tr>
				<tr>
					<td><input type="text" class="middle" name="individual_name2"/></td>
					<td colspan="2"><input type="text" class="middle" name="individual_value2"/></td>
				</tr>
				<tr>
					<td><input type="text" class="middle" name="individual_name3"/></td>
					<td colspan="2"><input type="text" class="middle" name="individual_value3"/></td>
				</tr>
				
				</table>
				<div class="feature_ma"><xsl:value-of select="$locale/shop/product/edit/features_help/text()"/></div>
				<div>&#160;</div>
			</td></tr>
			<tr><th colspan="2" class="actions">
				<input type="submit" value="{$locale/common/buttons/form_save/text()}"/>
			</th></tr>
			</form>
		</xsl:if>
		<!-- /Features box -->
	</table>
		
	<table class="form" cellspacing="0" cellpadding="0" id="mediabox">
		<!-- Media box -->
		<xsl:if test="product/id &gt; 0">
		<tr><td colspan="2">&#160;</td></tr>
		<tr><th colspan="2">MEDIA</th></tr>
		<tr><td colspan="2">
			<xsl:choose>
				<xsl:when test="count(product/images/image) > 0">
					<xsl:apply-templates select="product/images">
					</xsl:apply-templates>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="empty_media_box">
						<xsl:with-param name="parent_id" select="$id"/>
						<xsl:with-param name="parent_folder" select="$folder_for_child"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</td></tr>
		</xsl:if>
		<!-- /Media box -->
		<div id="media"></div>
		
		<!-- Files box -->
		<!--
		<xsl:if test="product/id &gt; 0">
		<tr><td colspan="2">&#160;</td></tr>
		<tr><th colspan="2">UPLOADED FILES</th></tr>
		<tr><td colspan="2">
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tr>
				<th class="subheader" width="5%">#</th>
				<th class="subheader">Title</th>
				<th class="subheader" style="text-align:center" width="10%">Type</th>
				<th class="subheader" style="text-align:center" width="10%">Position</th>
				<th class="subheader" width="5%">Del</th>
			</tr>
			
			<xsl:choose>
				<xsl:when test="count(product/files/*) > 0">
					<xsl:apply-templates select="product/files">
						<xsl:with-param name="id1" select="$id1"/>
						<xsl:with-param name="id2" select="$id2"/>
						<xsl:with-param name="id3" select="$id3"/>
					</xsl:apply-templates>
				</xsl:when>
				<xsl:otherwise>
					<tr><td colspan="20" style="text-align:center">Upload new file</td></tr>
				</xsl:otherwise>
			</xsl:choose>

				<tr><td colspan="20" style="border-top:1px dotted;border-bottom:1px dotted">
		
				<form name="product_files" id="product_files" action="{$script}" method="post" enctype="multipart/form-data">
					<input type="hidden" name="obj" value="content"/>
					<input type="hidden" name="cmd" value="upload_file"/>
					<input type="hidden" name="id1" value="{$id1}"/>
					<input type="hidden" name="id2" value="{$id2}"/>
					<input type="hidden" name="id3" value="{$id3}"/>
					<input type="hidden" name="file[parent_id]" value="{$id}"/>
					
					<table cellspacing="0" cellpadding="0" width="100%"><tr>
						<td>Title:</td><td><input type="text" class="middle" name="file[title]" id="file_title" value=""/></td>
						<td>File:</td><td><input type="file" name="file" id="file_file" value=""/></td>
						<td>Type:</td><td><select name="file[type]" class="small" id="file_type">
						<xsl:for-each select="file_types/type">
							<xsl:call-template name="gen-option">
								<xsl:with-param name="value" select="@id"/>
								<xsl:with-param name="title" select="@name"/>
							</xsl:call-template>
						</xsl:for-each>
						</select></td>
						<td style="text-align:right"><input type="submit" value="Add"/></td>
					</tr></table>
		
				</form>
				
				</td></tr>
			</table>
			
		</td></tr>
		</xsl:if>
		-->
		<!-- /Files box -->
		
	</table>
	
	<script language="JavaScript" src="{$views_path}/media_box.js"></script>
	<script language="JavaScript" src="{$views_path}/article.js"></script>
	
</xsl:template>

<xsl:template match="point">
	<tr>
		<td><xsl:value-of select="name"/><input type="hidden" name="point_id{position()}" value="{id}"/></td>
		<td><xsl:value-of select="address"/></td>
		<td><input type="text" name="quantity{position()}" value="{quantity}" class="small"/></td>
	</tr>
</xsl:template>

<xsl:template match="images">
	<table class="media_box" width="100%" border="0">
		<xsl:apply-templates select="image">
		</xsl:apply-templates>
	</table>
</xsl:template>

<xsl:template match="features/feature">
	<xsl:variable name="class">
		<xsl:choose>
			<xsl:when test="@match_analog = 1">feature_ma</xsl:when>
			<xsl:otherwise>feature_wma</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	
	<xsl:variable name="fid" select="id"/>
	
	<xsl:variable name="pid" select="../../product/features/feature[@id=$fid]/@value_id"/>
	
	<tr>
		<td class="{$class}"><xsl:value-of select="name"/></td>
		<td class="{$class}" colspan="2">
			<select class="feature-select" name="feature_id_{position()}">
				<option value="-1"></option>
				<xsl:for-each select="values/value">
					<xsl:call-template name="gen-option">
						<xsl:with-param name="value" select="id"/>
						<xsl:with-param name="title" select="name"/>
						<xsl:with-param name="selected" select="$pid"/>
					</xsl:call-template>
				</xsl:for-each>
			</select>
		</td>
	</tr>
</xsl:template>

<xsl:template match="individual_features/feature">
	<tr>
		<td><xsl:value-of select="individual_name"/></td>
		<td><xsl:value-of select="individual_value"/></td>
		<td><a title="{$locale/common/del/text()}" href="#" onclick="article_delete('?shop.del_individual_feature&amp;id={../../id}&amp;feature_id={@id}','{$locale/common/msgs/delete_item/text()}')"><img  src="{$views_path}/images/del.gif" border="0"/></a></td>
	</tr>
</xsl:template>


<xsl:template match="image">
	<xsl:variable name="id" select="id"/>
	<xsl:variable name="parent_id" select="parent_id"/>
	<xsl:variable name="folder" select="folder"/>
	<xsl:variable name="parent_folder" select="../../folder_for_child"/>
	<xsl:variable name="thumb">
		<xsl:choose>
			<xsl:when test="thumbnail">
				<xsl:value-of select="thumbnail"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$views_path"/>/images/gallery/media_empty_thumbnail.gif
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<xsl:if test="position() = 1 or position() = 6 or position() = 11 or position() = 16 or position() = 21"><xsl:value-of disable-output-escaping = "yes" select = "string('&lt;tr&gt;')"/></xsl:if>
	<td><table class="media_box_item" width="100" height="100" cellspacing="0" cellpadding="0">
		<tr><td colspan="4"><a href="#" onclick="popup_media_product({$parent_id},'{$parent_folder}',{$id})"><img src="{$thumb}" width="100" height="90" border="0"/></a></td></tr>
		<tr>
			<th align="left" width="10%"><a href="?{$controller_name}.move_up_gallery&amp;id={$parent_id}&amp;image_id={$id}"><img src="{$views_path}/images/gallery/media_sortleft.gif" hspace="4" vspace="4" border="0"/></a></th>
			<th align="left" width="10%"><a href="?{$controller_name}.move_down_gallery&amp;id={$parent_id}&amp;image_id={$id}"><img src="{$views_path}/images/gallery/media_sortright.gif" hspace="4" vspace="4" border="0"/></a></th>
			<th style="text-align:center"><span class="tag"><xsl:value-of select="tag"/>&#160;</span></th>
			<th style="text-align:right" width="10%"><a href="#" onclick="del_gallery('?{$controller_name}.del_media&amp;id={$parent_id}&amp;image_id={$id}');return(false)"><img src="{$views_path}/images/gallery/media_del.gif" hspace="4" vspace="4" border="0"/></a></th>
		</tr>
	</table></td>
	
	<xsl:choose>
		<xsl:when test="count(../image) = position()">
			<td width="80%"><a href="#" onclick="popup_media_product({$parent_id},'{$parent_folder}',0)"><img  src="{$views_path}/images/plus.gif" border="0"/></a></td><xsl:value-of disable-output-escaping = "yes" select = "string('&lt;/tr&gt;')"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:if test="position() = 5 or position() = 10 or position() = 15 or position() = 20 or position() = 25"><td width="30%">&#160;</td><xsl:value-of disable-output-escaping = "yes" select = "string('&lt;/tr&gt;')"/></xsl:if>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="empty_media_box">
	<xsl:param name="parent_id"/>
	<xsl:param name="parent_folder"/>
	
	<table class="media_box" width="100%" height="100px"><tr><td><a href="#" onclick="popup_media_product({$parent_id},'{$parent_folder}',0)"><img  src="{$views_path}/images/plus.gif" border="0"/></a></td></tr></table>
</xsl:template>

<xsl:template match="category">
	<xsl:call-template name="gen-option">
		<xsl:with-param name="value" select="@id"/>
		<xsl:with-param name="title" select="concat(@prefix,name)"/>
		<xsl:with-param name="selected" select="0"/>
	</xsl:call-template>
	<xsl:apply-templates select="category"/>
</xsl:template>

</xsl:stylesheet>

<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:msxsl="urn:schemas-microsoft-com:xslt"
				xmlns:exsl="http://exslt.org/common"
				extension-element-prefixes="exsl"
				version="1.0">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:include href="left_panel.xsl" />

<xsl:template match="data">
	<xsl:choose>
		<xsl:when test="/content/global/user/roles/role[code='sop']/@selected = 'yes'">
			<xsl:apply-templates/>
		</xsl:when>
		<xsl:otherwise>
			<div class="error"><xsl:value-of select="$locale/common/msgs/access_denied/text()"/></div>
			<script type="text/javascript">RemoveSmallTabs();RemoveLeftPanel();</script>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="order">

<table class="form" cellspacing="0" cellpadding="0">
	<tr><th><xsl:value-of select="$locale/sop/orders/view/title/text()"/></th></tr>
	
	<tr><td>
		<table width="100%">
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/sop/orders/view/number/text()"/></b></td>
				<td class="row"><xsl:value-of select="order_number"/></td>
			</tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/sop/orders/view/product_details/text()"/></b></td>
				<td class="row"><xsl:value-of select="product_details"/>&#160;</td>
			</tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/sop/orders/view/created_at/text()"/></b></td>
				<td class="row"><xsl:value-of select="created_at"/></td>
			</tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/sop/orders/view/updated_at/text()"/></b></td>
				<td class="row"><xsl:value-of select="updated_at"/>&#160;</td>
			</tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/sop/orders/view/status/text()"/></b></td>
				<td class="row">
					<xsl:choose>
						<xsl:when test="status = 1">
							<xsl:value-of select="$locale/sop/orders/statuses/s1/text()"/>
						</xsl:when>
						<xsl:when test="status = 2">
							<xsl:value-of select="$locale/sop/orders/statuses/s2/text()"/>
						</xsl:when>
						<xsl:when test="status = 3">
							<xsl:value-of select="$locale/sop/orders/statuses/s3/text()"/>
						</xsl:when>
						<xsl:when test="status = 4">
							<xsl:value-of select="$locale/sop/orders/statuses/s4/text()"/>
						</xsl:when>
						<xsl:when test="status = 5">
							<xsl:value-of select="$locale/sop/orders/statuses/s5/text()"/>
						</xsl:when>
					</xsl:choose>
				</td>
			</tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/sop/orders/list/comments/text()"/></b></td>
				<td class="row"><xsl:value-of select="comment"/>&#160;</td>
			</tr>
			
			<tr><td class="row" colspan="2">&#160;</td></tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/printbook/orders/pages"/></b></td>
				<td class="row"><xsl:value-of select="page_count"/>&#160;</td>
			</tr>
			
			<tr><td class="row" colspan="2">&#160;</td></tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/sop/orders/view/client/text()"/></b></td>
				<td class="row"><xsl:value-of select="first_name"/>&#160;<xsl:value-of select="last_name"/></td>
			</tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/sop/orders/view/phone/text()"/></b></td>
				<td class="row"><xsl:value-of select="phone"/>&#160;</td>
			</tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/sop/orders/view/email/text()"/></b></td>
				<td class="row"><a href="mailto:{email}"><xsl:value-of select="email"/></a>&#160;</td>
			</tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/sop/orders/view/address2/text()"/></b></td>
				<td class="row"><xsl:value-of select="zip"/>,&#160;<xsl:value-of select="city"/>,&#160;<xsl:value-of select="street"/></td>
			</tr>
			
			<tr><td class="row" colspan="2">&#160;</td></tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/printbook/orders/quantity"/></b></td>
				<td class="row"><xsl:value-of select="copies"/>&#160;</td>
			</tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/sop/orders/view/total_price/text()"/></b></td>
				<td class="row"><xsl:value-of select="total_price"/>&#160;</td>
			</tr>
			
		</table>
	</td></tr>
	
	<tr><td>&#160;</td></tr>
	
	<tr><th><xsl:value-of select="$locale/sop/orders/view/title2/text()"/></th></tr>
	<tr><td>
	
	<xsl:call-template name="tiny_mce"/>

	<form name="send" id="send" action="?{$controller_name}.order_send" method="post">
		<input type="hidden" name="id" value="{id}"/>
		
		<table class="form" cellspacing="0" cellpadding="0">
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/sop/orders/view/subject/text()"/></b></td>
				<td class="row"><input type="text" class="large" name="subject" value="N{order_number}"/></td>
			</tr>
		
			<tr>
				<td class="row" colspan="2"><b><xsl:value-of select="$locale/sop/orders/view/body/text()"/></b></td>
			</tr>
			<tr>
				<td class="row" colspan="2"><textarea name="body" class="tinymce large" rows="10"></textarea></td>
			</tr>
			
			<tr><th colspan="2" class="actions"><input type="submit" value="{$locale/common/buttons/form_send/text()}"/></th></tr>
		</table>
	</form>
	
	</td></tr>
</table>

</xsl:template>

</xsl:stylesheet>

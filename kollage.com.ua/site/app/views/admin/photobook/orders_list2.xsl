<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" version="1.0">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout.xsl" />
<xsl:include href="../menu.xsl" />
<xsl:include href="../paginator.xsl" />

<xsl:include href="left_panel.xsl" />

<xsl:template match="data">
	<xsl:choose>
		<xsl:when test="/content/global/user/roles/role[code='sop']/@selected = 'yes'">
			<xsl:apply-templates select="list"/>
		</xsl:when>
		<xsl:otherwise>
			<div class="error"><xsl:value-of select="$locale/common/msgs/access_denied/text()"/></div>
			<script type="text/javascript">RemoveSmallTabs();RemoveLeftPanel();</script>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="list">
	<xsl:variable name="rows" select="count(./order)"/>

	<table class="paginator"><tr><th>
		<xsl:call-template name="paginator">
			<xsl:with-param name="prefix">top</xsl:with-param>
		</xsl:call-template>
	</th></tr></table>
	
	<table class="list" id="list">
		<tr>
			<th width="2%" style="text-align:center"><a href="#" style="color:blue" onclick="article_select_all();return(false)">#</a></th>
			<th width="*"><xsl:value-of select="$locale/sop/orders/list/number/text()"/></th>
			<th width="10%"><xsl:value-of select="$locale/sop/orders/list/status/text()"/></th>
			<th width="15%"><xsl:value-of select="$locale/sop/orders/list/client/text()"/></th>
			<th width="10%"><xsl:value-of select="$locale/sop/orders/list/phone/text()"/></th>
			<th width="10%"><xsl:value-of select="$locale/sop/orders/list/email/text()"/></th>
			<th width="8%"><xsl:value-of select="$locale/sop/orders/list/photos_count/text()"/></th>
			<th width="8%"><xsl:value-of select="$locale/sop/orders/list/total_price/text()"/></th>
			<th width="10%"><xsl:value-of select="$locale/sop/orders/list/created_at/text()"/></th>
			<th width="2%" style="text-align:center">&#160;</th>
			<th width="2%" style="text-align:center">&#160;</th>
		</tr>
		
		<xsl:choose>
			<xsl:when test="order">
				<xsl:apply-templates select="order">
				</xsl:apply-templates>
			</xsl:when>
			<xsl:otherwise>
				<tr><td colspan="20" align="center"><xsl:value-of select="$locale/sop/orders/list/insert_msg/text()"/></td></tr>
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:variable name="btn_save" select="$locale/sop/orders/buttons/save/text()"/>
		<xsl:variable name="btn_delete_marked" select="$locale/sop/orders/buttons/delete_marked/text()"/>
		<xsl:variable name="btn_change_status_all" select="$locale/sop/orders/buttons/change_status2_all/text()"/>
		<xsl:variable name="btn_change_status_marked" select="$locale/sop/orders/buttons/change_status2_marked/text()"/>
		<xsl:variable name="btn_make_document" select="$locale/sop/orders/buttons/make_document/text()"/>
		
		<tr>
			<th class="actions" colspan="20">
				<table align="right"><tr>
					<td><input type="button" value="{$btn_change_status_all}" onclick="document.location='?{$controller_name}.orders_change_status_all&amp;status=3'"/></td>
					<td><input type="button" value="{$btn_change_status_marked}" onclick="article_delete_marked('?{$controller_name}.orders_change_status_marked&amp;status=3','{$locale/common/msgs/change_status/text()}')"/></td>
					<td><input type="button" value="{$btn_delete_marked}" onclick="article_delete_marked('?{$controller_name}.orders_del_marked','{$locale/common/msgs/delete_items/text()}')"/></td>
				</tr></table>
			</th>
		</tr>
	</table>
	
	<table class="paginator"><tr><th>
		<xsl:call-template name="paginator">
			<xsl:with-param name="prefix">bottom</xsl:with-param>
		</xsl:call-template>
	</th></tr></table>
	
	<p><xsl:value-of select="$locale/sop/orders/list/help/text()" disable-output-escaping="yes"/></p>
	
	<xsl:if test="../parameters = 'scroll_to_list'">
		<script>ScrollToObject('list')</script>
	</xsl:if>
	
	<script language="JavaScript" src="{$views_path}/paginator.js"></script>
	<script language="JavaScript" src="{$views_path}/article.js"></script>
	
</xsl:template>

<xsl:template match="order">
	<xsl:variable name="id" select="id"/>
	<tr id="row{position()}">
		<td class="row"><input type="checkbox" name="del_item{position()}" id="del_item{position()}" value="{$id}"/></td>
		<td class="row">
			<xsl:choose>
				<xsl:when test="total_price > 0 and total_price &lt;= payed">
					<span style="color:#00cc00"><b><xsl:value-of select="order_number"/></b></span>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="order_number"/>
				</xsl:otherwise>
			</xsl:choose>
		</td>
		<td class="row">
			<xsl:choose>
				<xsl:when test="status = 1">
					<xsl:value-of select="$locale/sop/orders/statuses/s1/text()"/>
				</xsl:when>
				<xsl:when test="status = 2">
					<xsl:value-of select="$locale/sop/orders/statuses/s2/text()"/>
				</xsl:when>
				<xsl:when test="status = 3">
					<xsl:value-of select="$locale/sop/orders/statuses/s3/text()"/>
				</xsl:when>
				<xsl:when test="status = 4">
					<xsl:value-of select="$locale/sop/orders/statuses/s4/text()"/>
				</xsl:when>
				<xsl:when test="status = 5">
					<xsl:value-of select="$locale/sop/orders/statuses/s5/text()"/>
				</xsl:when>
			</xsl:choose>
		</td>
		
		<td class="row"><xsl:value-of select="first_name"/>&#160;<xsl:value-of select="last_name"/></td>
		<td class="row"><xsl:value-of select="phone"/></td>
		<td class="row"><a href="mailto:{email}"><xsl:value-of select="email"/></a></td>
		
		<td class="row"><xsl:value-of select="copies"/></td>
		<td class="row"><xsl:value-of select="total_price"/></td>
		<td class="row"><xsl:value-of select="created_at"/></td>
		
		<td class="row" align="center"><a title="{$locale/common/view/text()}" href="?{$controller_name}.order_view&amp;id={$id}"><img src="{$views_path}/images/edit.gif" border="0"/></a></td>
		<td class="row" align="center"><a title="{$locale/common/del/text()}" href="#" onclick="article_delete('?{$controller_name}.order_del&amp;id={id}&amp;list={status}','{$locale/common/msgs/delete_item/text()}')"><img  src="{$views_path}/images/del.gif" border="0"/></a></td>
	</tr>
</xsl:template>

</xsl:stylesheet>

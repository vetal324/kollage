<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:msxsl="urn:schemas-microsoft-com:xslt"
				xmlns:exsl="http://exslt.org/common"
				extension-element-prefixes="exsl"
				version="1.0">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:variable name="controller_name" select="content/global/controller_name"/>
<xsl:variable name="method_name" select="content/global/method_name"/>
<xsl:variable name="views_path" select="content/global/views_path"/>
<xsl:variable name="data_path" select="content/global/data_path"/>
<xsl:variable name="fair_path" select="content/global/fair_path"/>
<xsl:variable name="fair_documents_path" select="content/global/fair_documents_path"/>
<xsl:variable name="lang" select="content/global/lang"/>
<xsl:variable name="http_query_string" select="content/global/http_query_string"/>
<xsl:variable name="http_request_uri" select="content/global/http_request_uri"/>

<xsl:variable name="rows_per_page"><xsl:value-of select="/content/global/rows_per_page"/></xsl:variable>
<xsl:variable name="pages"><xsl:value-of select="/content/global/pages"/></xsl:variable>
<xsl:variable name="page"><xsl:value-of select="/content/global/page"/></xsl:variable>
<xsl:variable name="page_prev" select="$page - 1"/>
<xsl:variable name="page_next" select="$page + 1"/>

<xsl:variable name="exchange_rate" select="/content/global/currencies/currency[code='840']/exchange_rate"/>

<xsl:template name="gen-option">
  <xsl:param name="value"/>
  <xsl:param name="title"/>
  <xsl:param name="selected"/>
  <xsl:choose>
	<xsl:when test="$value = $selected">
	  <option value="{$value}" s="{$selected}" selected="yes"><xsl:value-of select="$title"/></option>
	</xsl:when>
	<xsl:otherwise>
	  <option value="{$value}" s="{$selected}"><xsl:value-of select="$title"/></option>
	</xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template name="sorting">
  <xsl:param name="sort_title"/>
  <xsl:param name="sort_url"/>
  <xsl:param name="sort_number"/>
  <xsl:param name="sort_number_current"/>
  
  <table class="sort" cellspacing="0" cellpadding="0"><tr>
  <xsl:choose>
	  <xsl:when test="$sort_number_current = $sort_number">
		  <td><a href="{$sort_url}&amp;sort=-{$sort_number}&amp;page={$page}"><xsl:value-of select="$sort_title"/></a></td>
		  <td><img src="{$views_path}/images/sort_asc.gif"/></td>
	  </xsl:when>
	  <xsl:when test="$sort_number_current = number($sort_number * -1)">
		  <td><a href="{$sort_url}&amp;sort={$sort_number}&amp;page={$page}"><xsl:value-of select="$sort_title"/></a></td>
		  <td><img src="{$views_path}/images/sort_desc.gif"/></td>
	  </xsl:when>
	  <xsl:otherwise>
		  <td><a href="{$sort_url}&amp;sort={$sort_number}&amp;page={$page}"><xsl:value-of select="$sort_title"/></a></td>
	  </xsl:otherwise>
  </xsl:choose>
  </tr></table>
</xsl:template>

<xsl:template name="position">
  <xsl:param name="up_url"/>
  <xsl:param name="down_url"/>
  
	<table cellspacing="0" cellpadding="0" class="nostyle" align="center">
		<tr>
			<td><a href="{$up_url}"><img src="{$views_path}/images/move_up.gif" border="0"/></a></td>
			<td><a href="{$down_url}"><img src="{$views_path}/images/move_down.gif" border="0"/></a></td>
		</tr>
	</table>
</xsl:template>

<xsl:template name="tiny_mce">
	<xsl:call-template name="tiny_mce_4"/>
</xsl:template>

<xsl:template name="tiny_mce_2">

	<!-- tinyMCE -->
	<script language="javascript" type="text/javascript" src="{$views_path}/../shared/tiny_mce/tiny_mce.js"></script>
	<script language="javascript" type="text/javascript">
		tinyMCE.init({
			mode : "textareas",
			width : 650,
			plugins : "table",
			theme_advanced_buttons1 : "fontselect,fontsizeselect,forecolor,backcolor,separator,bold,italic,separator,justifyleft,justifycenter,justifyright,justifyfull,separator,link,unlink,separator,bullist,numlist,separator,outdent,indent,separator,undo,redo",
			theme_advanced_buttons2 : "tablecontrols,separator,code",
			theme_advanced_buttons3 : "",
			theme_advanced_buttons4 : "",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "right",
			convert_urls : "false",
			table_cell_styles : "Border1px=border1px;Border2px=border2px",
			language : "ru_utf8"
		});
	</script>
	<!-- /tinyMCE -->
	
</xsl:template>

<xsl:template name="tiny_mce_3">

	<!-- tinyMCE -->
	<script language="javascript" type="text/javascript" src="{$views_path}/../shared/tiny_mce-3/tiny_mce.js"></script>
	<script language="javascript" type="text/javascript">
		tinyMCE.init({
			mode : "textareas",
			width : 650,
			plugins : "table",
			theme_advanced_buttons1 : "fontselect,fontsizeselect,forecolor,backcolor,separator,bold,italic,separator,justifyleft,justifycenter,justifyright,justifyfull,separator,link,unlink,separator,bullist,numlist,separator,outdent,indent,separator,undo,redo",
			theme_advanced_buttons2 : "tablecontrols,separator,code",
			theme_advanced_buttons3 : "",
			theme_advanced_buttons4 : "",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "right",
			convert_urls : false,
			relative_urls : false,
			fix_list_elements : false,
			remove_script_host : false,
			table_cell_styles : "Border1px=border1px;Border2px=border2px",
			language : "ru",
			valid_children: "+body[style]"
		});
	</script>
	<!-- /tinyMCE -->
	
</xsl:template>

<xsl:template name="tiny_mce_4">
	<script language="javascript" type="text/javascript" src="/js/tinymce/tinymce.min.js"></script>
	<script language="javascript" type="text/javascript">
		tinymce.init({
			selector: "textarea.tinymce",
			width : 650,
			plugins: [
				"link image lists charmap",
				"searchreplace wordcount media nonbreaking paste",
				"table textcolor code colorpicker hr"
			],
			toolbar: 'undo redo | styleselect | fontselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor backcolor emoticons',
			language: 'ru',
			resize: 'both',
			convert_urls: false
 		});
	</script>
</xsl:template>

</xsl:stylesheet>

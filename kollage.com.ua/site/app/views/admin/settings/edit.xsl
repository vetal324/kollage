<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:msxsl="urn:schemas-microsoft-com:xslt"
				xmlns:exsl="http://exslt.org/common"
				extension-element-prefixes="exsl"
				version="1.0">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:template match="data">
	<xsl:choose>
		<xsl:when test="/content/global/user/roles/role[code='settings']/@selected = 'yes'">
			<xsl:apply-templates/>
		</xsl:when>
		<xsl:otherwise>
			<div class="error"><xsl:value-of select="$locale/common/msgs/access_denied/text()"/></div>
			<script type="text/javascript">RemoveSmallTabs();RemoveLeftPanel();</script>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="edit">

	<xsl:call-template name="tiny_mce"/>

	<form name="edit" id="edit" action="?{$controller_name}.save" method="post" enctype="multipart/form-data">
		<input type="hidden" name="id" value="{settings/id}"/>
		<input type="hidden" name="object[id]" value="{settings/id}"/>
		<input type="hidden" name="object[lang]" value="{$lang}"/>
	<table class="form" cellspacing="0" cellpadding="0">
	
		<!-- Show message -->
		<xsl:if test="save_result = 'true'">
		<tr><td colspan="2"><div class="message"><xsl:value-of select="$locale/settings/edit/save_msg_success/text()"/></div></td></tr>
		</xsl:if>
		<xsl:if test="save_result = 'false'">
		<tr><td colspan="2"><div class="error"><xsl:value-of select="$locale/settings/edit/save_msg_error/text()"/></div></td></tr>
		</xsl:if>
		<!-- /Show message -->
		
		<tr><th colspan="2"><xsl:value-of select="$locale/settings/edit/title/text()"/></th></tr>
		
		<tr>
			<td class="left_label"><label for="from_name"><xsl:value-of select="$locale/settings/edit/from_name/text()"/></label></td>
			<td><input type="text" class="middle" name="object[from_name]" id="from_name" value="{settings/from_name}"/></td>
		</tr>
		
		<tr>
			<td class="left_label"><label for="from_email"><xsl:value-of select="$locale/settings/edit/from_email/text()"/></label></td>
			<td><input type="text" class="middle" name="object[from_email]" id="from_email" value="{settings/from_email}"/></td>
		</tr>
		
		<tr><td colspan="2"><label for="contacts_at_top"><xsl:value-of select="$locale/settings/edit/contacts_at_top/text()"/></label></td></tr>
		<tr><td colspan="2">
			<textarea name="object[contacts_at_top]" id="contacts_at_top" class="tinymce large" rows="7" cols="80"><xsl:value-of select="settings/contacts_at_top"/></textarea>
		</td></tr>
		
		<!-- activation -->
		<tr><td colspan="2"><br/><br/></td></tr>
		<tr><th colspan="2"><xsl:value-of select="$locale/settings/edit/activation_mail/text()"/></th></tr>
		
		<tr>
			<td class="left_label"><label for="activation_mail_subject"><xsl:value-of select="$locale/settings/edit/mail_subject/text()"/></label></td>
			<td><input type="text" class="large" name="object[activation_mail_subject]" id="activation_mail_subject" value="{settings/activation_mail_subject}"/></td>
		</tr>
		
		<tr><td class="left_label" colspan="2"><label for="activation_mail_body"><xsl:value-of select="$locale/settings/edit/mail_body/text()"/></label></td></tr>
		<tr><td colspan="2">
			<textarea name="object[activation_mail_body]" id="activation_mail_body" class="tinymce large" rows="15" cols="80"><xsl:value-of select="settings/activation_mail_body"/></textarea>
			<p class="help"><i><xsl:value-of select="$locale/settings/edit/activation_help/text()" disable-output-escaping="yes"/></i></p>
		</td></tr>
		<!-- activation - end -->
		
		<!-- register -->
		<tr><td colspan="2"><br/><br/></td></tr>
		<tr><th colspan="2"><xsl:value-of select="$locale/settings/edit/register_mail/text()"/></th></tr>
		
		<tr>
			<td class="left_label"><label for="register_mail_subject"><xsl:value-of select="$locale/settings/edit/mail_subject/text()"/></label></td>
			<td><input type="text" class="large" name="object[register_mail_subject]" id="register_mail_subject" value="{settings/register_mail_subject}"/></td>
		</tr>
		
		<tr><td class="left_label" colspan="2"><label for="register_mail_body"><xsl:value-of select="$locale/settings/edit/mail_body/text()"/></label></td></tr>
		<tr><td colspan="2">
			<textarea name="object[register_mail_body]" id="register_mail_body" class="tinymce large" rows="15" cols="80"><xsl:value-of select="settings/register_mail_body"/></textarea>
			<p class="help"><i><xsl:value-of select="$locale/settings/edit/register_help/text()" disable-output-escaping="yes"/></i></p>
		</td></tr>
		<!-- register - end -->
		
		<!-- password_reminder -->
		<tr><td colspan="2"><br/><br/></td></tr>
		<tr><th colspan="2"><xsl:value-of select="$locale/settings/edit/password_reminder_mail/text()"/></th></tr>
		
		<tr>
			<td class="left_label"><label for="password_reminder_mail_subject"><xsl:value-of select="$locale/settings/edit/mail_subject/text()"/></label></td>
			<td><input type="text" class="large" name="object[password_reminder_mail_subject]" id="password_reminder_mail_subject" value="{settings/password_reminder_mail_subject}"/></td>
		</tr>
		
		<tr><td class="left_label" colspan="2"><label for="password_reminder_mail_body"><xsl:value-of select="$locale/settings/edit/mail_body/text()"/></label></td></tr>
		<tr><td colspan="2">
			<textarea name="object[password_reminder_mail_body]" id="password_reminder_mail_body" class="tinymce large" rows="15" cols="80"><xsl:value-of select="settings/password_reminder_mail_body"/></textarea>
			<p class="help"><i><xsl:value-of select="$locale/settings/edit/password_reminder_help/text()" disable-output-escaping="yes"/></i></p>
		</td></tr>
		<!-- password_reminder - end -->
		
		<!-- register2admin -->
		<tr><td colspan="2"><br/><br/></td></tr>
		<tr><th colspan="2"><xsl:value-of select="$locale/settings/edit/register2admin_mail/text()"/></th></tr>
		
		<tr>
			<td class="left_label"><label for="register_mail2admin_subject"><xsl:value-of select="$locale/settings/edit/mail_subject/text()"/></label></td>
			<td><input type="text" class="large" name="object[register_mail2admin_subject]" id="register_mail2admin_subject" value="{settings/register_mail2admin_subject}"/></td>
		</tr>
		
		<tr><td class="left_label" colspan="2"><label for="register_mail2admin_body"><xsl:value-of select="$locale/settings/edit/mail_body/text()"/></label></td></tr>
		<tr><td colspan="2">
			<textarea name="object[register_mail2admin_body]" id="register_mail_body" class="tinymce large" rows="15" cols="80"><xsl:value-of select="settings/register_mail2admin_body"/></textarea>
			<p class="help"><i><xsl:value-of select="$locale/settings/edit/register2admin_mail_help/text()" disable-output-escaping="yes"/></i></p>
		</td></tr>
		<!-- register2admin - end -->
		
		<!-- confirm_prof -->
		<tr><td colspan="2"><br/><br/></td></tr>
		<tr><th colspan="2"><xsl:value-of select="$locale/settings/edit/confirm_prof_mail/text()"/></th></tr>

		<tr>
			<td class="left_label"><label for="confirm_prof_subject"><xsl:value-of select="$locale/settings/edit/mail_subject/text()"/></label></td>
			<td><input type="text" class="large" name="object[confirm_prof_subject]" id="confirm_prof_subject" value="{settings/confirm_prof_subject}"/></td>
		</tr>

		<tr>
			<td class="left_label" colspan="2"><label for="confirm_prof_body"><xsl:value-of select="$locale/settings/edit/mail_body/text()"/></label></td>
		</tr>
		<tr>
			<td colspan="2">
				<textarea name="object[confirm_prof_body]" id="confirm_prof_body" class="tinymce large" rows="15" cols="80"><xsl:value-of select="settings/confirm_prof_body"/></textarea>
				<p class="help"><i><xsl:value-of select="$locale/settings/edit/confirm_prof_mail_help/text()" disable-output-escaping="yes"/></i></p>
			</td>
		</tr>
		<!-- /confirm_prof -->

		<!-- prof_confirmed -->
		<tr><td colspan="2"><br/><br/></td></tr>
		<tr><th colspan="2"><xsl:value-of select="$locale/settings/edit/prof_confirmed_email/text()"/></th></tr>

		<tr>
			<td class="left_label"><label for="prof_confirmed_subject"><xsl:value-of select="$locale/settings/edit/mail_subject/text()"/></label></td>
			<td><input type="text" class="large" name="object[prof_confirmed_subject]" id="prof_confirmed_subject" value="{settings/prof_confirmed_subject}"/></td>
		</tr>

		<tr>
			<td class="left_label" colspan="2"><label for="confirm_prof_body"><xsl:value-of select="$locale/settings/edit/mail_body/text()"/></label></td>
		</tr>
		<tr>
			<td colspan="2">
				<textarea name="object[prof_confirmed_body]" id="prof_confirmed_body" class="tinymce large" rows="15" cols="80"><xsl:value-of select="settings/prof_confirmed_body"/></textarea>
				<p class="help"><i><xsl:value-of select="$locale/settings/edit/confirm_prof_mail_help/text()" disable-output-escaping="yes"/></i></p>
			</td>
		</tr>
		<!-- /prof_confirmed -->
		
		<!-- confirm_ads -->
		<tr><td colspan="2"><br/><br/></td></tr>
		<tr><th colspan="2"><xsl:value-of select="$locale/settings/edit/confirm_ads_mail/text()"/></th></tr>

		<tr>
			<td class="left_label"><label for="confirm_ads_subject"><xsl:value-of select="$locale/settings/edit/mail_subject/text()"/></label></td>
			<td><input type="text" class="large" name="object[confirm_ads_subject]" id="confirm_ads_subject" value="{settings/confirm_ads_subject}"/></td>
		</tr>

		<tr>
			<td class="left_label" colspan="2"><label for="confirm_ads_body"><xsl:value-of select="$locale/settings/edit/mail_body/text()"/></label></td>
		</tr>
		<tr>
			<td colspan="2">
				<textarea name="object[confirm_ads_body]" id="confirm_ads_body" class="tinymce large" rows="15" cols="80"><xsl:value-of select="settings/confirm_ads_body"/></textarea>
				<p class="help"><i><xsl:value-of select="$locale/settings/edit/confirm_ads_mail_help/text()" disable-output-escaping="yes"/></i></p>
			</td>
		</tr>
		<!-- /confirm_ads -->

		<!-- ads_confirmed -->
		<tr><td colspan="2"><br/><br/></td></tr>
		<tr><th colspan="2"><xsl:value-of select="$locale/settings/edit/ads_confirmed_email/text()"/></th></tr>

		<tr>
			<td class="left_label"><label for="ads_confirmed_subject"><xsl:value-of select="$locale/settings/edit/mail_subject/text()"/></label></td>
			<td><input type="text" class="large" name="object[ads_confirmed_subject]" id="ads_confirmed_subject" value="{settings/ads_confirmed_subject}"/></td>
		</tr>

		<tr>
			<td class="left_label" colspan="2"><label for="confirm_ads_body"><xsl:value-of select="$locale/settings/edit/mail_body/text()"/></label></td>
		</tr>
		<tr>
			<td colspan="2">
				<textarea name="object[ads_confirmed_body]" id="ads_confirmed_body" class="tinymce large" rows="15" cols="80"><xsl:value-of select="settings/ads_confirmed_body"/></textarea>
				<p class="help"><i><xsl:value-of select="$locale/settings/edit/confirm_ads_mail_help/text()" disable-output-escaping="yes"/></i></p>
			</td>
		</tr>
		<!-- /ads_confirmed -->

		<tr><th colspan="2" class="actions"><input type="submit" value="{$locale/common/buttons/form_save/text()}"/></th></tr>
	</table>
		
	</form>
	
</xsl:template>

</xsl:stylesheet>

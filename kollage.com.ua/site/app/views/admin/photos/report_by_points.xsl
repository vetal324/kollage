<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:msxsl="urn:schemas-microsoft-com:xslt"
				xmlns:exsl="http://exslt.org/common"
				extension-element-prefixes="exsl"
				version="1.0">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:include href="left_panel.xsl" />

<xsl:template match="data">
	<xsl:choose>
		<xsl:when test="/content/global/user/roles/role[code='sop']/@selected = 'yes'">
			<xsl:apply-templates/>
		</xsl:when>
		<xsl:otherwise>
			<div class="error"><xsl:value-of select="$locale/common/msgs/access_denied/text()"/></div>
			<script type="text/javascript">RemoveSmallTabs();RemoveLeftPanel();</script>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="report">
	<xsl:variable name="title" select="$locale/sop/report_by_points/title"></xsl:variable>

	<table class="form" cellspacing="0" cellpadding="0" style="width:800px">
		
		<tr><th colspan="4"><xsl:value-of select="$title"/></th></tr>
		
		<tr>
			<td colspan="4" style="background-color:#dddddd;padding:4px">
				<table cellspacing="0" cellpadding="0"><tr>
				<form method="post">
					<td><xsl:value-of select="$locale/clients/activity/period/text()"/></td>
					<td>&#160;</td>
					<td>� <input type="text" name="from" value="{from}" class="small"/></td>
					<td>&#160;</td>
					<td>�� <input type="text" name="to" value="{to}" class="small"/></td>
					<td>&#160;</td>
					<td><input type="submit" value="{$locale/clients/activity/buttons/show/text()}"/></td>
				</form>
				</tr></table>
			</td>
		</tr>
		
		<xsl:apply-templates select="items"/>
		
	</table>
   
</xsl:template>

<xsl:template match="items/item">
	<tr><td colspan="4"><b><xsl:value-of select="point/name"/></b></td></tr>
		
	<tr>
		<td>#</td>
		<td>������</td>
		<td>���. ����������</td>
		<td>�����</td>
	</tr>
		
	<xsl:apply-templates select="size"/>
</xsl:template>

<xsl:template match="items/item/size">
	<tr>
		<td><xsl:value-of select="position()"/>.</td>
		<td><xsl:value-of select="name"/></td>
		<td><xsl:value-of select="quantity"/></td>
		<td><xsl:value-of select="format-number(price,'#0.00')"/></td>
	</tr>
</xsl:template>

</xsl:stylesheet>

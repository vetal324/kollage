<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:msxsl="urn:schemas-microsoft-com:xslt"
				xmlns:exsl="http://exslt.org/common"
				extension-element-prefixes="exsl"
				version="1.0">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:include href="left_panel.xsl" />

<xsl:template match="data">
	<xsl:choose>
		<xsl:when test="/content/global/user/roles/role[code='sop']/@selected = 'yes'">
			<xsl:apply-templates/>
		</xsl:when>
		<xsl:otherwise>
			<div class="error"><xsl:value-of select="$locale/common/msgs/access_denied/text()"/></div>
			<script type="text/javascript">RemoveSmallTabs();RemoveLeftPanel();</script>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="report">
	<xsl:variable name="title" select="$locale/sop/report_by_dates/title"></xsl:variable>

	<table class="form" cellspacing="0" cellpadding="0" style="width:800px">
		
		<tr><th colspan="5"><xsl:value-of select="$title"/></th></tr>
		
		<tr>
			<td colspan="5" style="background-color:#dddddd;padding:4px">
				<form method="post">
				
				<table cellspacing="0" cellpadding="0">
				<tr>
					<td><xsl:value-of select="$locale/clients/activity/period/text()"/></td>
					<td>&#160;</td>
					<td>� <input type="text" name="from" value="{from}" class="small"/></td>
					<td>&#160;</td>
					<td>�� <input type="text" name="to" value="{to}" class="small"/></td>
					<td>&#160;&#160;&#160;&#160;</td>
					<td><input type="submit" value="{$locale/clients/activity/buttons/show/text()}"/></td>
				</tr>
				</table>
				
				<table cellspacing="0" cellpadding="0">
				<tr>
					<td><xsl:value-of select="$locale/sop/report_by_dates/points_select/text()"/></td>
					<td>
						<select name="point_id">
							<xsl:variable name="selected" select="point_id"/>
              <xsl:for-each select="points/point">
                  <xsl:call-template name="gen-option">
                      <xsl:with-param name="value" select="id"/>
                      <xsl:with-param name="title" select="name"/>
                      <xsl:with-param name="selected" select="$selected"/>
                  </xsl:call-template>
              </xsl:for-each>
						</select>
					</td>
				</tr>
				</table>
				
				</form>
			</td>
		</tr>
		
		<xsl:if test="count(items/*) > 0">
			<tr><td colspan="5">&#160;</td></tr>
			<tr><td colspan="5"><b><xsl:value-of select="items/point/name"/></b></td></tr>
			<tr>
				<td><b>#</b></td>
				<td><b>����</b></td>
				<td><b>������</b></td>
				<td><b>����������</b></td>
				<td><b>����</b></td>
			</tr>
			<xsl:apply-templates select="items"/>
		</xsl:if>
		
	</table>
   
</xsl:template>

<xsl:template match="point">
</xsl:template>

<xsl:template match="items/item">
		
	<tr>
		<td><xsl:value-of select="position()-1"/>.</td>
		<td><xsl:value-of select="date"/></td>
		<td><xsl:value-of select="name"/></td>
		<td><xsl:value-of select="quantity"/></td>
		<td><xsl:value-of select="format-number(price,'#0.00')"/></td>
	</tr>
		
</xsl:template>

</xsl:stylesheet>

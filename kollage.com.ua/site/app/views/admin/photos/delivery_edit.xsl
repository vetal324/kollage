<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:msxsl="urn:schemas-microsoft-com:xslt"
				xmlns:exsl="http://exslt.org/common"
				extension-element-prefixes="exsl"
				version="1.0">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:include href="left_panel.xsl" />

<xsl:template match="data">
	<xsl:choose>
		<xsl:when test="/content/global/user/roles/role[code='sop']/@selected = 'yes'">
			<xsl:apply-templates select="edit"/>
		</xsl:when>
		<xsl:otherwise>
			<div class="error"><xsl:value-of select="$locale/common/msgs/access_denied/text()"/></div>
			<script type="text/javascript">RemoveSmallTabs();RemoveLeftPanel();</script>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="edit">

	<xsl:variable name="title">
		<xsl:choose>
			<xsl:when test="delivery/id &gt; 0"><xsl:value-of select="$locale/sop/deliveries/edit/title1/text()"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="$locale/sop/deliveries/edit/title2/text()"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	
	<xsl:variable name="id" select="delivery/id"/>
	<xsl:variable name="name" select="delivery/name"/>
	<xsl:variable name="price" select="delivery/price"/>
	<xsl:variable name="position"><xsl:value-of select="delivery/position"/></xsl:variable>

	<form name="delivery_edit" id="delivery_edit" action="?{$controller_name}.delivery_save" method="post">
		<input type="hidden" name="id" value="{$id}"/>
		<input type="hidden" name="last_position" value="{last_position}"/>
		<input type="hidden" name="object[id]" value="{$id}"/>
		<input type="hidden" name="object[lang]" value="{$lang}"/>
		<input type="hidden" name="object[position]" value="{$position}"/>
	<table class="form" cellspacing="0" cellpadding="0">
	
		<!-- Show message -->
		<xsl:if test="save_result = 'true'">
		<tr><td colspan="2"><div class="message"><xsl:value-of select="$locale/sop/deliveries/edit/save_msg_success/text()"/></div></td></tr>
		</xsl:if>
		<xsl:if test="save_result = 'false'">
		<tr><td colspan="2"><div class="error"><xsl:value-of select="$locale/sop/deliveries/edit/save_msg_error/text()"/></div></td></tr>
		</xsl:if>
		<!-- /Show message -->
		
		<tr><th colspan="2"><xsl:value-of select="$title"/></th></tr>
		
		<tr>
			<td class="left_label"><label for="name"><xsl:value-of select="$locale/sop/deliveries/edit/name/text()"/></label></td>
			<td><input type="text" class="large" name="object[name]" id="name" value="{$name}"/></td>
		</tr>
		
		<tr>
			<td class="left_label"><label for="price"><xsl:value-of select="$locale/sop/deliveries/edit/price/text()"/></label></td>
			<td><input type="text" class="small" name="object[price]" id="price" value="{$price}"/></td>
		</tr>
		
		<tr>
			<td class="left_label"><label for="order_prefix"><xsl:value-of select="$locale/sop/deliveries/edit/order_prefix/text()"/></label></td>
			<td><input type="text" class="small" name="object[order_prefix]" id="order_prefix" value="{delivery/order_prefix}"/></td>
		</tr>
		
		<tr>
			<td class="left_label"><label for="paymentways"><xsl:value-of select="$locale/sop/deliveries/edit/paymentways/text()"/></label></td>
			<td>
				<xsl:apply-templates select="delivery/paymentways/paymentway"/>
			</td>
		</tr>
		
		<tr><th colspan="2" class="actions"><input type="submit" value="{$locale/common/buttons/form_save/text()}"/></th></tr>
	</table>
		
	</form>
	
	<script language="JavaScript" src="{$views_path}/article.js"></script>
	
</xsl:template>

<xsl:template match="paymentway">
	<div style="float:left;clear:none;padding-right:15px">
		<table cellspacing="0" cellpadding="0" border="0" width="200"><tr>
			<td>
				<xsl:choose>
					<xsl:when test="@selected = 'yes'">
						<input type="checkbox" name="paymentway[]" value="{id}" id="paymentway{id}" checked="yes"/>
					</xsl:when>
					<xsl:otherwise>
						<input type="checkbox" name="paymentway[]" value="{id}" id="paymentway{id}"/>
					</xsl:otherwise>
				</xsl:choose>
			</td>
			<td><label for="paymentway{id}" class="lower"><xsl:value-of select="name"/></label></td>
		</tr></table>
	</div>
</xsl:template>

</xsl:stylesheet>

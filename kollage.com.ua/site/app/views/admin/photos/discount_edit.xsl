<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:msxsl="urn:schemas-microsoft-com:xslt"
				xmlns:exsl="http://exslt.org/common"
				extension-element-prefixes="exsl"
				version="1.0">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:include href="left_panel.xsl" />

<xsl:template match="data">
	<xsl:choose>
		<xsl:when test="/content/global/user/roles/role[code='sop']/@selected = 'yes'">
			<xsl:apply-templates/>
		</xsl:when>
		<xsl:otherwise>
			<div class="error"><xsl:value-of select="$locale/common/msgs/access_denied/text()"/></div>
			<script type="text/javascript">RemoveSmallTabs();RemoveLeftPanel();</script>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="edit">

	<xsl:variable name="title">
		<xsl:choose>
			<xsl:when test="discount/id &gt; 0"><xsl:value-of select="$locale/sop/discounts/edit/title1/text()"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="$locale/sop/discounts/edit/title2/text()"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	
	<xsl:variable name="id" select="discount/id"/>
	<xsl:variable name="client_type_id" select="discount/client_type_id"/>
	<xsl:variable name="name" select="discount/name"/>
	<xsl:variable name="discount" select="discount/discount"/>
	<xsl:variable name="position"><xsl:value-of select="discount/position"/></xsl:variable>

	<form name="discount_edit" id="discount_edit" action="?{$controller_name}.discount_save" method="post">
		<input type="hidden" name="id" value="{$id}"/>
		<input type="hidden" name="last_position" value="{last_position}"/>
		<input type="hidden" name="discount[id]" value="{$id}"/>
		<input type="hidden" name="discount[lang]" value="{$lang}"/>
		<input type="hidden" name="discount[position]" value="{$position}"/>
	<table class="form" cellspacing="0" cellpadding="0">
	
		<!-- Show message -->
		<xsl:if test="save_result = 'true'">
		<tr><td colspan="2"><div class="message"><xsl:value-of select="$locale/sop/discounts/edit/save_msg_success/text()"/></div></td></tr>
		</xsl:if>
		<xsl:if test="save_result = 'false'">
		<tr><td colspan="2"><div class="error"><xsl:value-of select="$locale/sop/discounts/edit/save_msg_error/text()"/></div></td></tr>
		</xsl:if>
		<!-- /Show message -->
		
		<tr><th colspan="2"><xsl:value-of select="$title"/></th></tr>
		
		<tr>
			<td class="left_label"><label for="name"><xsl:value-of select="$locale/sop/discounts/edit/name/text()"/></label></td>
			<td><input type="text" class="large" name="discount[name]" id="name" value="{$name}"/></td>
		</tr>
		
		<tr>
			<td class="left_label"><label for="client_type_id"><xsl:value-of select="$locale/sop/discounts/edit/client_type/text()"/></label></td>
			<td>
			<select name="discount[client_type_id]" class="middle" id="client_type_id">
				<xsl:for-each select="discount/client_types/*">
					<xsl:call-template name="gen-option">
						<xsl:with-param name="value" select="id"/>
						<xsl:with-param name="title" select="name"/>
						<xsl:with-param name="selected" select="$client_type_id"/>
					</xsl:call-template>
				</xsl:for-each>
			</select>
			</td>
		</tr>
		
		<tr>
			<td class="left_label"><label for="discount"><xsl:value-of select="$locale/sop/discounts/edit/discount/text()"/></label></td>
			<td><input type="text" class="small" name="discount[discount]" id="discount" value="{$discount}"/></td>
		</tr>
		
		<tr><th colspan="2" class="actions"><input type="submit" value="{$locale/common/buttons/form_save/text()}"/></th></tr>
	</table>
		
	</form>
	
	<script language="JavaScript" src="{$views_path}/article.js"></script>
	
</xsl:template>

</xsl:stylesheet>

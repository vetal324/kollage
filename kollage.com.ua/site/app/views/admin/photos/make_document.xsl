<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" version="1.0">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout.xsl" />
<xsl:include href="../menu.xsl" />
<xsl:include href="../paginator.xsl" />

<xsl:include href="left_panel.xsl" />

<xsl:template match="/">
  <html>
	<head>
	  <title><xsl:value-of select="$locale/common/title/text()"/></title>
	  <link rel="stylesheet" href="{$views_path}/styles.css" />
	  <script language="JavaScript" src="{$views_path}/common.js"></script>
	</head>
	<body>
	
	<div style="padding:6px"><xsl:value-of select="$locale/sop/orders/list/print_at/text()"/><xsl:value-of select="/content/global/current_datetime"/></div>
	
	<xsl:apply-templates select="content/documents/data"/>
	
	<input type="button" value="{$locale/sop/orders/buttons/print/text()}" onclick="window.print()"/>&#160;
	<input type="button" value="{$locale/sop/orders/buttons/export_xls/text()}" onclick="document.location='?{$controller_name}.export_md&amp;status=2'"/>
	
	</body>
  </html>
</xsl:template>

<xsl:template match="data">
	<xsl:apply-templates select="delivery"/>
</xsl:template>

<xsl:template match="delivery">
	<table width="1100" border="1" style="margin-bottom:10px">
		<tr><th colspan="20"><b><xsl:value-of select="name"/></b></th></tr>
		<xsl:if test="count(./orders/order)>0">
			<tr>
				<th>&#160;</th>
				<th><b><xsl:value-of select="$locale/sop/orders/list/created_at/text()"/></b></th>
				<th><b><xsl:value-of select="$locale/sop/orders/list/number/text()"/></b></th>
				<th><b><xsl:value-of select="$locale/sop/orders/list/fio/text()"/></b></th>
				<th><b><xsl:value-of select="$locale/sop/orders/list/phone/text()"/></b></th>
				<th><b><xsl:value-of select="$locale/sop/orders/list/address/text()"/></b></th>
				<th><b><xsl:value-of select="$locale/sop/orders/list/comments/text()"/></b></th>
				<th><b><xsl:value-of select="$locale/sop/orders/list/photos/text()"/></b></th>
				<th><b><xsl:value-of select="$locale/sop/orders/list/photos_price_amount/text()"/></b></th>
				<th><b><xsl:value-of select="$locale/sop/orders/list/fio2/text()"/></b></th>
				<th><b><xsl:value-of select="$locale/sop/orders/list/signature/text()"/></b></th>
			</tr>
		</xsl:if>
		<!--<xsl:apply-templates select="orders/order"/>-->
		<xsl:apply-templates/>
	</table>
</xsl:template>

<xsl:template match="point">
<xsl:if test="count(./orders/order)>0">
		<tr><th colspan="20"><b><xsl:value-of select="name"/></b></th></tr>
		<tr>
			<th>&#160;</th>
			<th><b><xsl:value-of select="$locale/sop/orders/list/created_at/text()"/></b></th>
			<th><b><xsl:value-of select="$locale/sop/orders/list/number/text()"/></b></th>
			<th><b><xsl:value-of select="$locale/sop/orders/list/fio/text()"/></b></th>
			<th><b><xsl:value-of select="$locale/sop/orders/list/phone/text()"/></b></th>
			<th><b><xsl:value-of select="$locale/sop/orders/list/address/text()"/></b></th>
			<th><b><xsl:value-of select="$locale/sop/orders/list/comments/text()"/></b></th>
			<th><b><xsl:value-of select="$locale/sop/orders/list/photos/text()"/></b></th>
			<th><b><xsl:value-of select="$locale/sop/orders/list/photos_price_amount/text()"/></b></th>
			<th><b><xsl:value-of select="$locale/sop/orders/list/fio2/text()"/></b></th>
			<th><b><xsl:value-of select="$locale/sop/orders/list/signature/text()"/></b></th>
		</tr>
		<xsl:apply-templates select="orders/order"/>
</xsl:if>
</xsl:template>

<xsl:template match="delivery/name">
</xsl:template>

<xsl:template match="order">
	<tr>
		<td width="15"><xsl:value-of select="position()"/>.</td>
		<td width="80"><xsl:value-of select="created_at"/></td>
		<td width="165">
			<xsl:value-of select="order_number"/>
			<xsl:choose>
				<xsl:when test="total_price > 0 and total_price &lt;= payed"><b>(�������)</b></xsl:when>
				<xsl:otherwise><br/><b>(�� �������)</b></xsl:otherwise>
			</xsl:choose>
		</td>
		<td width="100"><xsl:value-of select="client/firstname"/><br/><xsl:value-of select="client/lastname"/></td>
		<td width="100"><xsl:value-of select="client/phone"/></td>
		
		<xsl:choose>
			<xsl:when test="address != ''">
				<td width="175"><xsl:value-of select="address"/></td>
			</xsl:when>
			<xsl:otherwise>
				<td width="175"><xsl:value-of select="client/city"/>, <xsl:value-of select="client/street/name"/>, <xsl:value-of select="client/address"/></td>
			</xsl:otherwise>
		</xsl:choose>
		
		<td width="120"><xsl:value-of select="comments"/>&#160;</td>
		
		<td width="120">
			<!--<xsl:apply-templates select="photos/photo"/>-->
			<xsl:if test="count( photos/photo[size/name = '10x15'] ) > 0">
				10x15 - <xsl:value-of select="sum( photos/photo[size/name = '10x15']/quantity )"/><xsl:value-of select="$locale/common/items_amount/text()"/><br/>
			</xsl:if>
			<xsl:if test="count( photos/photo[size/name = '15x21'] ) > 0">
				15x21 - <xsl:value-of select="sum( photos/photo[size/name = '15x21']/quantity )"/><xsl:value-of select="$locale/common/items_amount/text()"/><br/>
			</xsl:if>
			<xsl:if test="count( photos/photo[size/name = '20x30'] ) > 0">
				20x30 - <xsl:value-of select="sum( photos/photo[size/name = '20x30']/quantity )"/><xsl:value-of select="$locale/common/items_amount/text()"/><br/>
			</xsl:if>
			<xsl:if test="count( photos/photo[size/name = '30x40'] ) > 0">
				30x40 - <xsl:value-of select="sum( photos/photo[size/name = '30x40']/quantity )"/><xsl:value-of select="$locale/common/items_amount/text()"/><br/>
			</xsl:if>
			<xsl:if test="count( photos/photo[size/name = '40x60'] ) > 0">
				40x60 - <xsl:value-of select="sum( photos/photo[size/name = '40x60']/quantity )"/><xsl:value-of select="$locale/common/items_amount/text()"/><br/>
			</xsl:if>
			<xsl:if test="count( photos/photo[size/name = '60x80'] ) > 0">
				60x80 - <xsl:value-of select="sum( photos/photo[size/name = '60x80']/quantity )"/><xsl:value-of select="$locale/common/items_amount/text()"/><br/>
			</xsl:if>
			<xsl:if test="count( photos/photo[size/name = '15x35'] ) > 0">
				15x35 - <xsl:value-of select="sum( photos/photo[size/name = '15x35']/quantity )"/><xsl:value-of select="$locale/common/items_amount/text()"/><br/>
			</xsl:if>
			<xsl:if test="count( photos/photo[size/name = '������'] ) > 0">
				������ - <xsl:value-of select="sum( photos/photo[size/name = '������']/quantity )"/><xsl:value-of select="$locale/common/items_amount/text()"/><br/>
			</xsl:if>
		</td>
		<td align="right" width="*"><xsl:value-of select="total_price"/></td>
		<td width="80">&#160;</td>
		<td width="80">&#160;</td>
		
	</tr>
</xsl:template>

<xsl:template match="photo">
	<xsl:value-of select="size/name"/> - <xsl:value-of select="quantity"/><xsl:value-of select="$locale/common/items_amount/text()"/><br/>
</xsl:template>

</xsl:stylesheet>

<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:msxsl="urn:schemas-microsoft-com:xslt"
				xmlns:exsl="http://exslt.org/common"
				extension-element-prefixes="exsl"
				version="1.0">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:include href="left_panel.xsl" />

<xsl:template match="data">
	<xsl:choose>
		<xsl:when test="/content/global/user/roles/role[code='sop']/@selected = 'yes'">
			<xsl:apply-templates/>
		</xsl:when>
		<xsl:otherwise>
			<div class="error"><xsl:value-of select="$locale/common/msgs/access_denied/text()"/></div>
			<script type="text/javascript">RemoveSmallTabs();RemoveLeftPanel();</script>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="size_edit">

	<xsl:call-template name="tiny_mce"/>

	<xsl:variable name="title">
		<xsl:choose>
			<xsl:when test="size/id &gt; 0"><xsl:value-of select="$locale/sop/sizes/edit/title1/text()"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="$locale/sop/sizes/edit/title2/text()"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	
	<xsl:variable name="id" select="size/id"/>
	<xsl:variable name="name" select="size/name"/>
	<xsl:variable name="price" select="size/price"/>
	<xsl:variable name="weight" select="size/weight"/>
	<xsl:variable name="position"><xsl:value-of select="size/position"/></xsl:variable>

	<form name="size_edit" id="size_edit" action="?{$controller_name}.size_save" method="post">
		<input type="hidden" name="id" value="{$id}"/>
		<input type="hidden" name="last_position" value="{last_position}"/>
		<input type="hidden" name="size[id]" value="{$id}"/>
		<input type="hidden" name="size[position]" value="{$position}"/>
		<input type="hidden" name="size[lang]" value="{$lang}"/>
	<table class="form" cellspacing="0" cellpadding="0">
	
		<!-- Show message -->
		<xsl:if test="save_result = 'true'">
		<tr><td colspan="2"><div class="message"><xsl:value-of select="$locale/sop/sizes/edit/save_msg_success/text()"/></div></td></tr>
		</xsl:if>
		<xsl:if test="save_result = 'false'">
		<tr><td colspan="2"><div class="error"><xsl:value-of select="$locale/sop/sizes/edit/save_msg_error/text()"/></div></td></tr>
		</xsl:if>
		<!-- /Show message -->
		
		<tr><th colspan="2"><xsl:value-of select="$title"/></th></tr>
		
		<tr>
			<td class="left_label"><label for="name"><xsl:value-of select="$locale/sop/sizes/edit/name/text()"/></label></td>
			<td><input type="text" class="large" name="size[name]" id="name" value="{$name}"/></td>
		</tr>
		
		<!--<tr>
			<td class="left_label"><label for="price"><xsl:value-of select="$locale/sop/sizes/edit/price/text()"/></label></td>
			<td><input type="text" class="small" name="size[price]" id="price" value="{size/price}"/></td>
		</tr>-->
		
		<tr>
			<td class="left_label"><label for="price"><xsl:value-of select="$locale/sop/sizes/edit/weight/text()"/></label></td>
			<td><input type="text" class="small" name="size[weight]" id="weight" value="{$weight}"/></td>
		</tr>
		
		<tr>
			<td class="left_label" colspan="2"><label for="price"><xsl:value-of select="$locale/sop/sizes/edit/price/text()"/></label></td>
		</tr>
		<tr>
			<td class="left_label"></td>
			<td>
				<table width="50%">
					<tr>
						<td><xsl:value-of select="$locale/sop/sizes/edit/price/retail/text()"/></td>
						<td><input type="text" class="small" name="size[price1]" id="price" value="{size/price1}"/></td>
					</tr>
					<tr>
						<td><xsl:value-of select="$locale/sop/sizes/edit/price/wholesale/text()"/></td>
						<td><input type="text" class="small" name="size[price2]" id="price" value="{size/price2}"/></td>
					</tr>
					<tr>
						<td><xsl:value-of select="$locale/sop/sizes/edit/price/big_wholesale/text()"/></td>
						<td><input type="text" class="small" name="size[price3]" id="price" value="{size/price3}"/></td>
					</tr>
					<tr>
						<td><xsl:value-of select="$locale/sop/sizes/edit/price/adman/text()"/></td>
						<td><input type="text" class="small" name="size[price4]" id="price" value="{size/price4}"/></td>
					</tr>
					<tr>
						<td><xsl:value-of select="$locale/sop/sizes/edit/price/professional/text()"/></td>
						<td><input type="text" class="small" name="size[price5]" id="price" value="{size/price5}"/></td>
					</tr>
				</table>
			</td>
		</tr>
		
		<tr><td colspan="2" class="label"><label for="description"><xsl:value-of select="$locale/sop/sizes/edit/description/text()"/></label></td></tr>
		<tr><td colspan="2">
			<textarea name="size[description]" id="description" class="tinymce large" rows="16" cols="80"><xsl:value-of select="size/description" disable-output-escaping="yes"/></textarea>
		</td></tr>
		
		<tr><th colspan="2" class="actions"><input type="submit" value="{$locale/common/buttons/form_save/text()}"/></th></tr>
	</table>
		
	</form>
	
	<script language="JavaScript" src="{$views_path}/article.js"></script>
	
</xsl:template>

</xsl:stylesheet>

<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" version="1.0">

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:template match="data">
	<table width="100%">
		<tr><td colspan="2"><div><b><xsl:value-of select="$locale/welcome/title/text()"/></b></div><br/></td></tr>
		<tr>
			<td width="30%"><xsl:value-of select="$locale/welcome/login_as/text()"/></td>
			<td>www.kollage.com.ua</td>
		</tr>
		<tr>
			<td><xsl:value-of select="$locale/welcome/date/text()"/></td>
			<td><xsl:value-of select="/content/global/current_datetime"/></td>
		</tr>
		<tr><td colspan="2">&#160;</td></tr>
		<tr><td colspan="2"><xsl:value-of select="$locale/welcome/msg1/text()"/><br/><br/></td></tr>
		<tr><td colspan="2"><xsl:value-of select="$locale/welcome/msg2/text()"/><br/><br/></td></tr>
		<tr><td colspan="2"><xsl:value-of select="$locale/welcome/msg3/text()"/><br/><xsl:value-of select="$locale/welcome/msg4/text()"/></td></tr>
	</table>
</xsl:template>

</xsl:stylesheet>

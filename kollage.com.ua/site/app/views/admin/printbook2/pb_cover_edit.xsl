<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:msxsl="urn:schemas-microsoft-com:xslt"
				xmlns:exsl="http://exslt.org/common"
				extension-element-prefixes="exsl"
				version="1.0">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:template match="data">
	<xsl:choose>
		<xsl:when test="/content/global/user/roles/role[code='sop']/@selected = 'yes'">
			<xsl:apply-templates/>
		</xsl:when>
		<xsl:otherwise>
			<div class="error"><xsl:value-of select="$locale/common/msgs/access_denied/text()"/></div>
			<script type="text/javascript">RemoveSmallTabs();RemoveLeftPanel();</script>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="edit">

	<xsl:call-template name="tiny_mce"/>
	
	<a href="?{$controller_name}.cover_categories_list&amp;pb_size_id={pb_size/id}"><h2><xsl:value-of select="pb_size/name"/>: �������, ���������</h2></a>
	<br/>
	<a href="?{$controller_name}.pb_covers_list&amp;pb_category_id={pb_category/id}&amp;pb_size_id={pb_size/id}"><h3>�������� ������� � ��������� "<xsl:value-of select="pb_category/name"/>":</h3></a>

	<xsl:variable name="title">
		<xsl:choose>
			<xsl:when test="pb_cover/id &gt; 0"><xsl:value-of select="$locale/printbook/edit/title1/text()"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="$locale/printbook/edit/title2/text()"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	
	<xsl:variable name="id" select="pb_cover/id"/>
	<xsl:variable name="position"><xsl:value-of select="pb_cover/position"/></xsl:variable>

	<form name="edit" id="edit" action="?{$controller_name}.pb_cover_save" method="post" enctype="multipart/form-data">
		<input type="hidden" name="id" value="{$id}"/>
		<input type="hidden" name="last_position" value="{last_position}"/>
		<input type="hidden" name="pb_size_id" value="{pb_size/id}"/>
		<input type="hidden" name="pb_category_id" value="{pb_category/id}"/>
		<input type="hidden" name="object[id]" value="{$id}"/>
		<input type="hidden" name="object[pb_size_id]" value="{pb_size/id}"/>
		<input type="hidden" name="object[category_id]" value="{pb_category/id}"/>
		<input type="hidden" name="object[lang]" value="{$lang}"/>
		<input type="hidden" name="object[position]" value="{$position}"/>
	<table class="form" cellspacing="0" cellpadding="0">
	
		<!-- Show message -->
		<xsl:if test="save_result = 'true'">
		<tr><td colspan="2"><div class="message"><xsl:value-of select="$locale/printbook/edit/save_msg_success/text()"/></div></td></tr>
		</xsl:if>
		<xsl:if test="save_result = 'false'">
		<tr><td colspan="2"><div class="error"><xsl:value-of select="$locale/printbook/edit/save_msg_error/text()"/></div></td></tr>
		</xsl:if>
		<!-- /Show message -->
		
		<tr><th colspan="2"><xsl:value-of select="$title"/></th></tr>
		
		<tr>
			<td class="left_label"><label for="name"><xsl:value-of select="$locale/printbook/edit/name/text()"/></label></td>
			<td><input type="text" class="large" name="object[name]" id="name" value="{pb_cover/name}"/></td>
		</tr>
		
		<tr>
			<td class="left_label"><label for="description"><xsl:value-of select="$locale/printbook/boxes/edit/description/text()"/></label></td>
			<td><input type="text" class="large" name="object[description]" maxlength="255" id="description" value="{pb_cover/description}"/></td>
		</tr>
		
		<tr>
			<td class="left_label"><label for="price"><xsl:value-of select="$locale/printbook/edit/price/text()"/></label></td>
			<td><input type="text" class="medium" name="object[price]" id="price" value="{pb_cover/price}"/></td>
		</tr>

        <tr>
            <td class="left_label"><label for="allow_min_pages"><xsl:value-of select="$locale/printbook/edit/allow_min_pages/text()"/></label></td>
            <td><input type="text" class="medium" name="object[allow_min_pages]" id="allow_min_pages" value="{pb_cover/allow_min_pages}"/></td>
        </tr>
		
		<tr>
			<td class="left_label"><label><xsl:value-of select="$locale/printbook/edit/is_individual/text()"/></label></td>
			<td>
			<xsl:choose>
				<xsl:when test="pb_cover/is_individual = 1"><input type="radio" name="object[is_individual]" id="is_individual_active" value="1" checked="yes" /></xsl:when>
				<xsl:otherwise><input type="radio" name="object[is_individual]" id="is_individual_active" value="1" /></xsl:otherwise>
			</xsl:choose>
			<label for="is_individual_active" class="lower"><xsl:value-of select="$locale/common/yes/text()"/></label>
			<xsl:choose>
				<xsl:when test="pb_cover/is_individual = 0"><input type="radio" name="object[is_individual]" id="is_individual_inactive" value="0" checked="yes"/></xsl:when>
				<xsl:otherwise><input type="radio" name="object[is_individual]" id="is_individual_inactive" value="0"/></xsl:otherwise>
			</xsl:choose>
			<label for="is_individual_inactive" class="lower"><xsl:value-of select="$locale/common/no/text()"/></label></td>
		</tr>
		
		<tr>
			<td class="left_label"><label><xsl:value-of select="$locale/printbook/edit/has_supercover/text()"/></label></td>
			<td>
			<xsl:choose>
				<xsl:when test="pb_cover/has_supercover = 1"><input type="radio" name="object[has_supercover]" id="has_supercover_active" value="1" checked="yes" /></xsl:when>
				<xsl:otherwise><input type="radio" name="object[has_supercover]" id="has_supercover_active" value="1" /></xsl:otherwise>
			</xsl:choose>
			<label for="has_supercover_active" class="lower"><xsl:value-of select="$locale/common/exist/text()"/></label>
			<xsl:choose>
				<xsl:when test="pb_cover/has_supercover = 0 or pb_cover/has_supercover = ''"><input type="radio" name="object[has_supercover]" id="has_supercover_inactive" value="0" checked="yes"/></xsl:when>
				<xsl:otherwise><input type="radio" name="object[has_supercover]" id="has_supercover_inactive" value="0"/></xsl:otherwise>
			</xsl:choose>
			<label for="has_supercover_inactive" class="lower"><xsl:value-of select="$locale/common/no/text()"/></label></td>
		</tr>
		
		<tr>
			<td class="left_label"><label for="supercover_price"><xsl:value-of select="$locale/printbook/edit/supercover_price/text()"/></label></td>
			<td><input type="text" class="medium" name="object[supercover_price]" id="supercover_price" value="{pb_cover/supercover_price}"/></td>
		</tr>
		
		<tr>
			<td class="left_label"><label><xsl:value-of select="$locale/printbook/edit/check_size"/></label></td>
			<td>
			<xsl:choose>
				<xsl:when test="pb_cover/check_size = 1"><input type="radio" name="object[check_size]" id="check_size_active" value="1" checked="yes" /></xsl:when>
				<xsl:otherwise><input type="radio" name="object[check_size]" id="is_check_size" value="1" /></xsl:otherwise>
			</xsl:choose>
			<label for="check_size_active" class="lower"><xsl:value-of select="$locale/common/yes/text()"/></label>
			<xsl:choose>
				<xsl:when test="pb_cover/check_size = 0"><input type="radio" name="object[check_size]" id="check_size_inactive" value="0" checked="yes"/></xsl:when>
				<xsl:otherwise><input type="radio" name="object[check_size]" id="check_size_inactive" value="0"/></xsl:otherwise>
			</xsl:choose>
			<label for="check_size_inactive" class="lower"><xsl:value-of select="$locale/common/no/text()"/></label></td>
		</tr>
		
		<tr>
			<td class="left_label"><label for="image"><xsl:value-of select="$locale/printbook/boxes/edit/image/text()"/></label></td>
			<td>
				<table cellspacing="0" cellpadding="0"><tr>
				<td><input type="file" size="30" name="image" id="image"/></td>
				<td><xsl:if test="pb_cover/image != ''">&#160;&#160;<a href="#" onclick="del_image('?{$controller_name}.pb_cover_del_image&amp;id={$id}&amp;pb_size_id={pb_size/id}')"><img  src="{$views_path}/images/del.gif" border="0"/></a></xsl:if></td>
				<td><xsl:if test="pb_cover/image != ''"><xsl:variable name="image" select="pb_cover/image"/>
				&#160;&#160;<a href="#" onclick="return(popup_image_preview('640','480','{$data_path}/{$image}'))"><img src="{$views_path}/images/preview.gif" border="0"/></a></xsl:if></td>
			</tr></table>
			</td>
		</tr>
		
		<tr><th colspan="2" class="actions"><input type="submit" value="{$locale/common/buttons/form_save/text()}"/></th></tr>
	</table>
		
	</form>
	
	<script language="JavaScript" src="{$views_path}/article.js"></script>
	
</xsl:template>

</xsl:stylesheet>

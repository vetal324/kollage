<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:msxsl="urn:schemas-microsoft-com:xslt"
				xmlns:exsl="http://exslt.org/common"
				extension-element-prefixes="exsl"
				version="1.0">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:include href="left_panel.xsl" />

<xsl:template match="data">
	<xsl:choose>
		<xsl:when test="/content/global/user/roles/role[code='clients']/@selected = 'yes' and /content/global/user/roles/role[code='users']/@selected = 'yes'">
			<xsl:apply-templates/>
		</xsl:when>
		<xsl:otherwise>
			<div class="error"><xsl:value-of select="$locale/common/msgs/access_denied/text()"/></div>
			<script type="text/javascript">RemoveSmallTabs();RemoveLeftPanel();</script>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="stat">

	<xsl:variable name="title" select="$locale/clients/stat/title/text()"></xsl:variable>

	<table class="form" cellspacing="0" cellpadding="0">
		
		<tr><th colspan="2"><xsl:value-of select="$title"/></th></tr>
		
		<tr>
			<td><b><xsl:value-of select="$locale/clients/stat/purse/text()"/></b></td>
			<td><xsl:value-of select="purse_amount"/></td>
		</tr>
		
		<tr>
			<td><b><xsl:value-of select="$locale/clients/stat/posterminal_operations/sop_total/text()"/></b></td>
			<td><xsl:value-of select="posterminal_operations/sop_total"/></td>
		</tr>
		
		<tr>
			<td><b><xsl:value-of select="$locale/clients/stat/posterminal_operations/sop_total_payed/text()"/></b></td>
			<td><xsl:value-of select="posterminal_operations/sop_total_payed"/></td>
		</tr>
		
		<tr>
			<td colspan="2"><b><xsl:value-of select="$locale/clients/stat/client_types/text()"/></b></td>
		</tr>
		
		<xsl:apply-templates select="types"/>
		
		<tr>
			<td colspan="2" style="background-color:#dddddd;padding:4px">
				<table cellspacing="0" cellpadding="0" width="100%"><tr>
				<form method="post">
					<td width="30%"><b><xsl:value-of select="$locale/clients/stat/sop/text()"/></b></td>
					<td>&#160;&#160;&#160;&#160;&#160;&#160;</td>
					<td><xsl:value-of select="$locale/clients/stat/period/text()"/></td>
					<td>&#160;</td>
					<td>� <input type="text" name="from" value="{from}" class="small"/></td>
					<td>&#160;</td>
					<td>�� <input type="text" name="to" value="{to}" class="small"/></td>
					<td>&#160;</td>
					<td><input type="submit" value="{$locale/clients/stat/buttons/show/text()}"/></td>
				</form>
				</tr></table>
			</td>
		</tr>
		
		<tr>
			<td valign="top" colspan="2"><b><xsl:value-of select="$locale/clients/stat/sop_sum3/text()"/></b></td>
		</tr>
		<tr>
			<td colspan="2">
				<table cellspacing="0" cellpadding="0" width="100%" border="1">
					<tr>
						<td rowspan="2" align="center"><b>������</b></td>
						<td colspan="2" align="center"><b>�������</b></td>
						<td colspan="2" align="center"><b>���</b></td>
						<td colspan="2" align="center"><b>������� ���</b></td>
						<td colspan="2" align="center"><b>��������� �����</b></td>
						<td colspan="2" align="center"><b>������������</b></td>
						<td colspan="2" align="center"><b>�����</b></td>
					</tr>
					<tr>
						<td align="center">���-��</td>
						<td align="center">�����</td>
						<td align="center">���-��</td>
						<td align="center">�����</td>
						<td align="center">���-��</td>
						<td align="center">�����</td>
						<td align="center">���-��</td>
						<td align="center">�����</td>
						<td align="center">���-��</td>
						<td align="center">�����</td>
						<td align="center">���-��</td>
						<td align="center">�����</td>
					</tr>
					
					<xsl:for-each select="sop_sum4/item">
						<tr>
							<td><b><xsl:value-of select="name"/></b></td>
							<xsl:for-each select="items/item">
								<td align="right"><xsl:value-of select="@quantity"/></td>
								<td align="right"><xsl:value-of select="@sum"/></td>
							</xsl:for-each>
							<td align="right"><xsl:value-of select="@quantity"/></td>
							<td align="right"><xsl:value-of select="@sum"/></td>
						</tr>
					</xsl:for-each>
					
					<tr>
						<td><b>�����</b></td>
						
						<td align="right"><b><xsl:value-of select="sum(sop_sum4/item/items/item[position()=1]/@quantity)"/></b></td>
						<td align="right"><b><xsl:value-of select="sum(sop_sum4/item/items/item[position()=1]/@sum)"/></b></td>
						
						<td align="right"><b><xsl:value-of select="sum(sop_sum4/item/items/item[position()=2]/@quantity)"/></b></td>
						<td align="right"><b><xsl:value-of select="sum(sop_sum4/item/items/item[position()=2]/@sum)"/></b></td>

						<td align="right"><b><xsl:value-of select="sum(sop_sum4/item/items/item[position()=3]/@quantity)"/></b></td>
						<td align="right"><b><xsl:value-of select="sum(sop_sum4/item/items/item[position()=3]/@sum)"/></b></td>

						<td align="right"><b><xsl:value-of select="sum(sop_sum4/item/items/item[position()=4]/@quantity)"/></b></td>
						<td align="right"><b><xsl:value-of select="sum(sop_sum4/item/items/item[position()=4]/@sum)"/></b></td>

						<td align="right"><b><xsl:value-of select="sum(sop_sum4/item/items/item[position()=5]/@quantity)"/></b></td>
						<td align="right"><b><xsl:value-of select="sum(sop_sum4/item/items/item[position()=5]/@sum)"/></b></td>
																		
						<td align="right"><b><xsl:value-of select="sum(sop_sum4/item/items/item/@quantity)"/></b></td>
						<td align="right"><b><xsl:value-of select="sum(sop_sum4/item/items/item/@sum)"/></b></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr><td colspan="2" class="row">&#160;</td></tr>
		
		<tr>
			<td colspan="2">
				<b><xsl:value-of select="$locale/clients/stat/sop_top10/text()"/></b>
			</td>
		</tr>
		<xsl:apply-templates select="sop_top10"/>
		
	</table>
   
</xsl:template>

<xsl:template match="types/type">
	<tr>
		<td><xsl:value-of select="name"/></td>
		<td><xsl:value-of select="items"/></td>
	</tr>
</xsl:template>

<xsl:template match="sop_top10/type">
	<tr>
		<td><xsl:value-of select="name"/></td>
		<td>
			<xsl:apply-templates select="clients"/>
		</td>
	</tr>
</xsl:template>

<xsl:template match="client">
	<xsl:value-of select="position()"/>. <a href="?clients.edit&amp;id={client/id}" style="color:#336699"><xsl:value-of select="client/firstname"/>&#160;<xsl:value-of select="client/lastname"/></a>&#160;-&#160;<xsl:value-of select="format-number(sop_amount,'#0.00')"/><br/>
</xsl:template>

</xsl:stylesheet>

<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" version="1.0">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout.xsl" />
<xsl:include href="../menu.xsl" />
<xsl:include href="../paginator.xsl" />

<xsl:include href="left_panel.xsl" />

<xsl:template match="data">
	<xsl:choose>
		<xsl:when test="/content/global/user/roles/role[code='clients']/@selected = 'yes' and /content/global/user/roles/role[code='users']/@selected = 'yes'">
			<xsl:apply-templates select="list"/>
		</xsl:when>
		<xsl:otherwise>
			<div class="error"><xsl:value-of select="$locale/common/msgs/access_denied/text()"/></div>
			<script type="text/javascript">RemoveSmallTabs();RemoveLeftPanel();</script>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="list">
	<xsl:variable name="client_id" select="client/id"/>

	<table><tr client_id="{client/id}">
		<td><xsl:value-of select="$locale/clients/purse_operations/list/client/text()"/>&#160;<b><xsl:value-of select="client/firstname"/>&#160;<xsl:value-of select="client/lastname"/></b></td>
		<td width="50px">&#160;</td>
		<td><xsl:value-of select="$locale/clients/purse_operations/list/purse/text()"/>&#160;<b><xsl:value-of select="client/purse"/></b></td>
	</tr></table>

	<table class="paginator"><tr><th>
		<xsl:call-template name="paginator">
			<xsl:with-param name="prefix">top</xsl:with-param>
		</xsl:call-template>
	</th></tr></table>
	
	<table class="list" id="list">
		<tr>
			<th width="15%"><xsl:value-of select="$locale/clients/purse_operations/list/created_at/text()"/></th>
			<th width="18%"><xsl:value-of select="$locale/clients/purse_operations/list/amount/text()"/></th>
			<th><xsl:value-of select="$locale/clients/purse_operations/list/description/text()"/></th>
		</tr>
		
		<xsl:choose>
			<xsl:when test="purse_operation">
				<xsl:apply-templates select="purse_operation">
				</xsl:apply-templates>
			</xsl:when>
			<xsl:otherwise>
				<tr><td colspan="20" align="center"><xsl:value-of select="$locale/clients/purse_operations/list/insert_msg/text()"/></td></tr>
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:variable name="btn_add_new" select="$locale/clients/purse_operations/buttons/add_new/text()"/>
		
		<tr>
			<th class="actions" colspan="20">
				<table align="right"><tr>
					<td><input type="button" value="{$btn_add_new}" onclick="popup_purse({$client_id})"/></td>
				</tr></table>
			</th>
		</tr>
	</table>
	
	<table class="paginator"><tr><th>
		<xsl:call-template name="paginator">
			<xsl:with-param name="prefix">bottom</xsl:with-param>
		</xsl:call-template>
	</th></tr></table>
	
	<xsl:if test="../parameters = 'scroll_to_list'">
		<script>ScrollToObject('list')</script>
	</xsl:if>
	
	<form name="reload" id="reload" action="?{$controller_name}.list_purse_operations" method="post">
		<input type="hidden" name="id" value="{client/id}"/>
		<input type="hidden" name="page" value="{$page}"/>
		<input type="hidden" name="rows_per_page" value="{$rows_per_page}"/>
	</form>
	
	<script language="JavaScript" src="{$views_path}/paginator.js"></script>
	<script language="JavaScript" src="{$views_path}/article.js"></script>
	<script language="JavaScript" src="{$views_path}/purse.js"></script>
</xsl:template>

<xsl:template match="purse_operation">
	<xsl:variable name="id" select="id"/>
	<tr>
		<td class="row"><xsl:value-of select="created_at"/></td>
		<td class="row" align="right" style="padding-right:10px;"><xsl:value-of select="amount"/></td>
		<td class="row"><xsl:value-of select="description"/></td>
	</tr>
</xsl:template>

</xsl:stylesheet>

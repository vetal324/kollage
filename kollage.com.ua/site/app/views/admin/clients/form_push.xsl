<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:msxsl="urn:schemas-microsoft-com:xslt"
				xmlns:exsl="http://exslt.org/common"
				extension-element-prefixes="exsl"
				version="1.0">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:include href="left_panel.xsl" />

<xsl:template match="data">
	<xsl:choose>
		<xsl:when test="/content/global/user/roles/role[code='clients']/@selected = 'yes' and /content/global/user/roles/role[code='users']/@selected = 'yes'">
			<xsl:apply-templates/>
		</xsl:when>
		<xsl:otherwise>
			<div class="error"><xsl:value-of select="$locale/common/msgs/access_denied/text()"/></div>
			<script type="text/javascript">RemoveSmallTabs();RemoveLeftPanel();</script>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="clients">

	<xsl:call-template name="tiny_mce"/>

	<xsl:variable name="title" select="$locale/clients/send_push/title/text()"></xsl:variable>

	<form name="client" id="client" action="?{$controller_name}.send_push" method="post">
	<table class="form" cellspacing="0" cellpadding="0">
	
		<!-- Show message -->
		<xsl:choose>
			<xsl:when test="save_result = 'false'">
				<tr><td colspan="2"><div class="error"><xsl:value-of select="$locale/clients/send_push/save_msg_error/text()"/></div></td></tr>
			</xsl:when>
			<xsl:otherwise>
				<tr><td colspan="2"><div class="message"><xsl:value-of select="save_result"/></div></td></tr>
			</xsl:otherwise>
		</xsl:choose>
		<!-- /Show message -->
		
		<tr><th colspan="2"><xsl:value-of select="$title"/></th></tr>
		
		<tr>
			<td class="left_label"><label for="type_id"><xsl:value-of select="$locale/clients/send_push/type_id/text()"/></label></td>
			<td>
			<select name="type_id" class="middle" id="type_id">
				<option value="0"><xsl:value-of select="$locale/clients/send_push/to_all/text()"/></option>
				<xsl:for-each select="types/*">
					<xsl:call-template name="gen-option">
						<xsl:with-param name="value" select="id"/>
						<xsl:with-param name="title" select="name"/>
						<xsl:with-param name="selected" select="0"/>
					</xsl:call-template>
				</xsl:for-each>
			</select>
			</td>
		</tr>
		
		<tr>
			<td class="left_label"><label><xsl:value-of select="$locale/clients/send_push/subject/text()"/></label></td>
			<td><input type="text" class="large" name="subject" value="" maxlength="400"/></td>
		</tr>
		
		<tr>
			<td class="left_label"><label><xsl:value-of select="$locale/clients/send_push/body/text()"/></label></td>
			<td><input type="text" class="large" name="body" value="" maxlength="400"/></td>
		</tr>
		
		<tr><th colspan="2" class="actions">
			<input type="submit" value="{$locale/common/buttons/form_send/text()}"/>
		</th></tr>
	</table>
	</form>
   
</xsl:template>

</xsl:stylesheet>

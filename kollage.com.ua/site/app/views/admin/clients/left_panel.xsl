<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:msxsl="urn:schemas-microsoft-com:xslt"
				xmlns:exsl="http://exslt.org/common"
				extension-element-prefixes="exsl"
				version="1.0">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:template name="left_panel">
	
	<table cellpadding="2">
		<tr><td><xsl:value-of select="$locale/clients/left_panel/search/text()"/></td></tr>
		
		<form method="get">
		<input type="hidden" name="clients.search"/>
		<tr><td><input type="text" class="middle" name="name"/></td></tr>
		<tr><td align="right"><input type="submit" value="{$locale/common/buttons/form_search/text()}"/></td></tr>
		</form>
		
	</table>
	
</xsl:template>

</xsl:stylesheet>

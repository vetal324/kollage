<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:msxsl="urn:schemas-microsoft-com:xslt"
				xmlns:exsl="http://exslt.org/common"
				extension-element-prefixes="exsl"
				version="1.0">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:include href="left_panel.xsl" />

<xsl:template match="data">
	<xsl:choose>
		<xsl:when test="/content/global/user/roles/role[code='clients']/@selected = 'yes'">
			<xsl:apply-templates/>
		</xsl:when>
		<xsl:otherwise>
			<div class="error"><xsl:value-of select="$locale/common/msgs/access_denied/text()"/></div>
			<script type="text/javascript">RemoveSmallTabs();RemoveLeftPanel();</script>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="stat">

	<xsl:variable name="title" select="$locale/clients/activity/title"></xsl:variable>

	<table class="form" cellspacing="0" cellpadding="0" style="width:800px">
		
		<tr><th colspan="6"><xsl:value-of select="$title"/></th></tr>
		
		<tr>
			<td colspan="6" style="background-color:#dddddd;padding:4px">
				<table cellspacing="0" cellpadding="0" width="100%"><tr>
				<form method="post">
					<td>
						<select name="show">
							<xsl:variable name="show_selected" select="show"/>
							<xsl:call-template name="gen-option">
								<xsl:with-param name="value">1</xsl:with-param>
								<xsl:with-param name="title"><xsl:value-of select="$locale/clients/activity/shows/show1"/></xsl:with-param>
								<xsl:with-param name="selected" select="$show_selected"/>
							</xsl:call-template>
							<xsl:call-template name="gen-option">
								<xsl:with-param name="value">2</xsl:with-param>
								<xsl:with-param name="title"><xsl:value-of select="$locale/clients/activity/shows/show2"/></xsl:with-param>
								<xsl:with-param name="selected" select="$show_selected"/>
							</xsl:call-template>
						</select>
					</td>
					<td>&#160;</td>
					<td>
						<select name="limit">
							<xsl:variable name="limit_selected" select="limit"/>
							<xsl:call-template name="gen-option">
								<xsl:with-param name="value">10</xsl:with-param>
								<xsl:with-param name="title">10</xsl:with-param>
								<xsl:with-param name="selected" select="$limit_selected"/>
							</xsl:call-template>
							<xsl:call-template name="gen-option">
								<xsl:with-param name="value">20</xsl:with-param>
								<xsl:with-param name="title">20</xsl:with-param>
								<xsl:with-param name="selected" select="$limit_selected"/>
							</xsl:call-template>
							<xsl:call-template name="gen-option">
								<xsl:with-param name="value">50</xsl:with-param>
								<xsl:with-param name="title">50</xsl:with-param>
								<xsl:with-param name="selected" select="$limit_selected"/>
							</xsl:call-template>
							<xsl:call-template name="gen-option">
								<xsl:with-param name="value">100</xsl:with-param>
								<xsl:with-param name="title">100</xsl:with-param>
								<xsl:with-param name="selected" select="$limit_selected"/>
							</xsl:call-template>
							<xsl:call-template name="gen-option">
								<xsl:with-param name="value">200</xsl:with-param>
								<xsl:with-param name="title">200</xsl:with-param>
								<xsl:with-param name="selected" select="$limit_selected"/>
							</xsl:call-template>
							<xsl:call-template name="gen-option">
								<xsl:with-param name="value">500</xsl:with-param>
								<xsl:with-param name="title">500</xsl:with-param>
								<xsl:with-param name="selected" select="$limit_selected"/>
							</xsl:call-template>
						</select>
					</td>
					<td>&#160;</td>
					<td align="right"><xsl:value-of select="$locale/clients/activity/min/text()"/></td>
					<td>&#160;</td>
					<td><input type="text" name="min" value="{min}" class="small"/></td>
					<td>&#160;</td>
					<td><xsl:value-of select="$locale/clients/activity/period/text()"/></td>
					<td>&#160;</td>
					<td>� <input type="text" name="from" value="{from}" class="small"/></td>
					<td>&#160;</td>
					<td>�� <input type="text" name="to" value="{to}" class="small"/></td>
					<td>&#160;</td>
					<td><input type="submit" value="{$locale/clients/activity/buttons/show/text()}"/></td>
				</form>
				</tr></table>
			</td>
		</tr>
		
		<xsl:apply-templates select="sop_top"/>
		
	</table>
   
</xsl:template>

<xsl:template match="sop_top/type">
	<tr><td colspan="6"><b><xsl:value-of select="name"/></b></td></tr>
		
	<tr>
		<td>#</td>
		<td>���</td>
		<td>���. �������</td>
		<td>�����</td>
		<td>��������� �����</td>
		<td>�������</td>
	</tr>
		
	<xsl:apply-templates select="clients"/>
</xsl:template>

<xsl:template match="client">
	<tr>
		<td><xsl:value-of select="position()"/>.</td>
		<td><a href="?clients.edit&amp;id={client/id}" style="color:#336699"><xsl:value-of select="client/firstname"/>&#160;<xsl:value-of select="client/lastname"/></a></td>
		<td><xsl:value-of select="sop_count"/></td>
		<td><xsl:value-of select="format-number(sop_amount,'#0.00')"/></td>
		<td><xsl:value-of select="sop_date"/></td>
		<td><xsl:value-of select="client/phone"/></td>
	</tr>
</xsl:template>

</xsl:stylesheet>

<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" version="1.0">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout.xsl" />
<xsl:include href="../menu.xsl" />
<xsl:include href="../paginator.xsl" />

<xsl:include href="left_panel.xsl" />

<xsl:template match="data">
	<xsl:choose>
		<xsl:when test="/content/global/user/roles/role[code='clients']/@selected = 'yes'">
			<xsl:apply-templates select="clients"/>
		</xsl:when>
		<xsl:otherwise>
			<div class="error"><xsl:value-of select="$locale/common/msgs/access_denied/text()"/></div>
			<script type="text/javascript">RemoveSmallTabs();RemoveLeftPanel();</script>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="clients">
	<xsl:variable name="rows" select="count(./client)"/>
	
	<table><tr>
		<xsl:choose>
		<xsl:when test="type_id = 0">
			<td><input type="radio" checked="yes" name="type_id" id="type_id0" value="0" onclick="document.location='?{$controller_name}.index&amp;type_id=0'"/></td>
		</xsl:when>
		<xsl:otherwise>
			<td><input type="radio" name="type_id" id="type_id0" value="0" onclick="document.location='?{$controller_name}.index&amp;type_id=0'"/></td>
		</xsl:otherwise>
		</xsl:choose>
		<td><label for="type_id0">���</label></td>
		<td>&#160;&#160;</td>
		
		<xsl:choose>
		<xsl:when test="type_id = 1">
			<td><input type="radio" checked="yes" name="type_id" id="type_id1" value="1" onclick="document.location='?{$controller_name}.index&amp;type_id=1'"/></td>
		</xsl:when>
		<xsl:otherwise>
			<td><input type="radio" name="type_id" id="type_id1" value="1" onclick="document.location='?{$controller_name}.index&amp;type_id=1'"/></td>
		</xsl:otherwise>
		</xsl:choose>
		<td><label for="type_id1">�������</label></td>
		<td>&#160;&#160;</td>
		
		<xsl:choose>
		<xsl:when test="type_id = 2">
			<td><input type="radio" checked="yes" name="type_id" id="type_id2" value="2" onclick="document.location='?{$controller_name}.index&amp;type_id=2'"/></td>
		</xsl:when>
		<xsl:otherwise>
			<td><input type="radio" name="type_id" id="type_id2" value="2" onclick="document.location='?{$controller_name}.index&amp;type_id=2'"/></td>
		</xsl:otherwise>
		</xsl:choose>
		<td><label for="type_id2">���</label></td>
		<td>&#160;&#160;</td>
		
		<xsl:choose>
		<xsl:when test="type_id = 3">
			<td><input type="radio" checked="yes" name="type_id" id="type_id3" value="3" onclick="document.location='?{$controller_name}.index&amp;type_id=3'"/></td>
		</xsl:when>
		<xsl:otherwise>
			<td><input type="radio" name="type_id" id="type_id3" value="3" onclick="document.location='?{$controller_name}.index&amp;type_id=3'"/></td>
		</xsl:otherwise>
		</xsl:choose>
		<td><label for="type_id3">������� ���</label></td>
		<td>&#160;&#160;</td>
		
		<xsl:choose>
		<xsl:when test="type_id = 4">
			<td><input type="radio" checked="yes" name="type_id" id="type_id4" value="4" onclick="document.location='?{$controller_name}.index&amp;type_id=4'"/></td>
		</xsl:when>
		<xsl:otherwise>
			<td><input type="radio" name="type_id" id="type_id4" value="4" onclick="document.location='?{$controller_name}.index&amp;type_id=4'"/></td>
		</xsl:otherwise>
		</xsl:choose>
		<td><label for="type_id4">��������� �����</label></td>
		<td>&#160;&#160;</td>
		
		<xsl:choose>
		<xsl:when test="type_id = -1">
			<td><input type="radio" checked="yes" name="type_id" id="type_id4" value="-1" onclick="document.location='?{$controller_name}.index&amp;type_id=-1'"/></td>
		</xsl:when>
		<xsl:otherwise>
			<td><input type="radio" name="type_id" id="type_id4" value="3" onclick="document.location='?{$controller_name}.index&amp;type_id=-1'"/></td>
		</xsl:otherwise>
		</xsl:choose>
		<td><label for="type_id4">������������</label></td>
	</tr></table>

	<table class="paginator"><tr><th>
		<xsl:call-template name="paginator">
			<xsl:with-param name="prefix">top</xsl:with-param>
		</xsl:call-template>
	</th></tr></table>
	
	<table class="list" id="list">
		<tr>
			<th width="2%">#</th>
			<th width="2%"><xsl:value-of select="$locale/clients/client/list/id/text()"/></th>
			<th width="*">
				<xsl:call-template name="sorting">
					<xsl:with-param name="sort_title" select="$locale/clients/client/list/name/text()"/>
					<xsl:with-param name="sort_url">?<xsl:value-of select="$controller_name"/>.<xsl:value-of select="$method_name"/>&amp;type_id=<xsl:value-of select="type_id"/></xsl:with-param>
					<xsl:with-param name="sort_number">2</xsl:with-param>
					<xsl:with-param name="sort_number_current" select="sort_number"/>
				</xsl:call-template>
			</th>
			<th width="5%" align="center"><xsl:value-of select="$locale/clients/client/list/status/text()"/></th>
			<th width="10%"><xsl:value-of select="$locale/clients/client/list/email/text()"/></th>
			<th width="5%"><xsl:value-of select="$locale/clients/client/list/city/text()"/></th>
			<th width="17%"><xsl:value-of select="$locale/clients/client/list/address/text()"/></th>
			<th width="9%"><xsl:value-of select="$locale/clients/client/list/phone/text()"/></th>
			<th width="6%"><xsl:value-of select="$locale/clients/client/list/photo_discount/text()"/></th>
			<th width="6%" style="text-align:center"><xsl:value-of select="$locale/clients/client/list/purse/text()"/></th>
			<th width="10%">
				<xsl:call-template name="sorting">
					<xsl:with-param name="sort_title" select="$locale/clients/client/list/created_at/text()"/>
					<xsl:with-param name="sort_url">?<xsl:value-of select="$controller_name"/>.<xsl:value-of select="$method_name"/>&amp;type_id=<xsl:value-of select="type_id"/></xsl:with-param>
					<xsl:with-param name="sort_number">10</xsl:with-param>
					<xsl:with-param name="sort_number_current" select="sort_number"/>
				</xsl:call-template>
			</th>
			<th width="2%" style="text-align:center">&#160;</th>
			<th width="2%" style="text-align:center">&#160;</th>
			<th width="2%" style="text-align:center">&#160;</th>
		</tr>
		
		<xsl:choose>
			<xsl:when test="client">
				<xsl:apply-templates select="client">
				</xsl:apply-templates>
			</xsl:when>
			<xsl:otherwise>
				<tr><td colspan="20" align="center"><xsl:value-of select="$locale/clients/client/list/insert_msg/text()"/></td></tr>
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:variable name="btn_save" select="$locale/clients/client/buttons/save/text()"/>
		<xsl:variable name="btn_delete_marked" select="$locale/clients/client/buttons/delete_marked/text()"/>
		<xsl:variable name="btn_add_new" select="$locale/clients/client/buttons/add_new/text()"/>
		
		<tr>
			<th class="actions" colspan="20">
				<table align="right"><tr>
					<!--<td><input type="button" value="{$btn_save}" onclick="article_save_changes('?obj=content&amp;cmd=save_changes&amp;folder={$folder}')"/></td>-->
					<td><input type="button" value="{$btn_delete_marked}" onclick="article_delete_marked('?{$controller_name}.del_marked&amp;page={$page}&amp;type_id={type_id}','{$locale/common/msgs/delete_items/text()}')"/></td>
					<td><input type="button" value="{$btn_add_new}" onclick="document.location='?{$controller_name}.edit&amp;last_position={last_position}&amp;page={$page}&amp;type_id={type_id}'"/></td>
				</tr></table>
			</th>
		</tr>
	</table>
	
	<table class="paginator"><tr><th>
		<xsl:call-template name="paginator">
			<xsl:with-param name="prefix">bottom</xsl:with-param>
		</xsl:call-template>
	</th></tr></table>

	<div class="help">
		���������:<br/>
		<img src="{$views_path}/images/auth.jpg" border="0" width="7" height="7" style="cursor:pointer"/> - ������ ������� ���������<br/>
		<img src="{$views_path}/images/a.jpg" border="0" width="7" height="7" style="cursor:pointer"/> - ������ ��������, �.�. ����� ������ ������<br/>
		<img src="{$views_path}/images/i.jpg" border="0" width="7" height="7" style="cursor:pointer"/> - ������ ���������������. �� ����� ������ ����� ������, ������������� ������.<br/>
	</div>
	
	<script language="JavaScript" src="{$views_path}/paginator.js"></script>
	<script language="JavaScript" src="{$views_path}/article.js"></script>
	
	<script type="text/javascript" language="javascript" charset="utf-8" src="{$views_path}/../shared/prototype.js"></script>
	<script type="text/javascript" language="javascript" charset="utf-8" src="{$views_path}/../shared/scriptaculos/scriptaculous.js"></script>
	<script type="text/javascript" language="javascript" charset="utf-8" src="{$views_path}/../shared/scriptaculos/effects.js"></script>
	<script type="text/javascript" language="javascript" charset="utf-8" src="{$views_path}/../shared/scriptaculos/dragdrop.js"></script>
	<script type="text/javascript" language="javascript" charset="utf-8" src="{$views_path}/dnd.js"></script>
	<script type="text/javascript" language="javascript">
		DefineDnDPosition( <xsl:value-of select="$rows"/> );

		function ChangePosition( src, dst )
		{
			src_id = $F( 'cid'+src ); dst_id = $F( 'cid'+dst );
			document.location = '?<xsl:value-of select="$controller_name"/>.swap_position&amp;src='+ src_id +'&amp;dst='+ dst_id +'&amp;page=<xsl:value-of select="$page"/>&amp;pages=<xsl:value-of select="$pages"/>';
		}
	</script>
	
</xsl:template>

<xsl:template match="client">
	<xsl:variable name="id" select="id"/>
	<tr id="row{position()}">
		<td class="row"><input type="checkbox" name="del_item{position()}" id="del_item{position()}" value="{$id}"/></td>
		<td class="row"><xsl:value-of select="id"/></td>
		<td class="row"><div class="cell" id="c{position()}"><xsl:value-of select="lastname"/>&#160;<xsl:value-of select="firstname"/>&#160;<xsl:value-of select="lastname2"/><input type="hidden" name="cid{position()}" id="cid{position()}" value="{id}"/></div></td>
		<td class="row" align="center">
			<xsl:choose>
				<xsl:when test="status = 1">
					<img src="{$views_path}/images/auth.jpg" border="0" width="7" height="7"/>
				</xsl:when>
				<xsl:when test="status = 2">
					<img src="{$views_path}/images/a.jpg" border="0" width="7" height="7" onclick="document.location='?{$controller_name}.set_status&amp;id={id}&amp;v=0'" style="cursor:pointer"/>
				</xsl:when>
				<xsl:otherwise>
					<img src="{$views_path}/images/i.jpg" border="0" width="7" height="7" onclick="document.location='?{$controller_name}.set_status&amp;id={id}&amp;v=2'" style="cursor:pointer"/>
				</xsl:otherwise>
			</xsl:choose>
		</td>
		<td class="row"><xsl:value-of select="email"/></td>
		<td class="row"><xsl:value-of select="city"/></td>
		<td class="row"><xsl:if test="street/name != ''"><xsl:value-of select="street/name"/>, </xsl:if><xsl:value-of select="address"/></td>
		<td class="row"><strong> +<xsl:value-of select="country/phone_code"/>&#160;</strong><xsl:value-of select="phone"/></td>
		<td class="row" align="center"><xsl:value-of select="photo_discount"/></td>
		<td class="row" style="text-align:center"><xsl:value-of select="purse"/></td>
		<td class="row"><xsl:value-of select="created_at"/></td>
		
		<td class="row" align="center"><a title="{$locale/common/purse/text()}" href="?{$controller_name}.list_purse_operations&amp;id={$id}&amp;page={$page}&amp;type_id={../type_id}"><img  src="{$views_path}/images/purse.gif" border="0"/></a></td>
		<td class="row" align="center"><a title="{$locale/common/edit/text()}" href="?{$controller_name}.edit&amp;id={$id}&amp;page={$page}&amp;type_id={../type_id}"><img  src="{$views_path}/images/edit.gif" border="0"/></a></td>
		<td class="row" align="center"><a title="{$locale/common/del/text()}" href="#" onclick="article_delete('?{$controller_name}.del&amp;id={id}&amp;page={$page}&amp;type_id={../type_id}','{$locale/common/msgs/delete_item/text()}')"><img  src="{$views_path}/images/del.gif" border="0"/></a></td>
	</tr>
</xsl:template>

</xsl:stylesheet>

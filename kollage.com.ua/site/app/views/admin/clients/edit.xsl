<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:msxsl="urn:schemas-microsoft-com:xslt"
				xmlns:exsl="http://exslt.org/common"
				extension-element-prefixes="exsl"
				version="1.0">

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:include href="left_panel.xsl" />

<xsl:output method="html" encoding="utf-8" indent="yes"/>

<xsl:template match="data">
	<xsl:choose>
		<xsl:when test="/content/global/user/roles/role[code='clients']/@selected = 'yes'">
			<xsl:apply-templates/>
		</xsl:when>
		<xsl:otherwise>
			<div class="error"><xsl:value-of select="$locale/common/msgs/access_denied/text()"/></div>
			<script type="text/javascript">RemoveSmallTabs();RemoveLeftPanel();</script>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="edit">

	  <script language="JavaScript" src="{$views_path}/../shared/prototype.js"></script>
	  <script language="JavaScript" src="{$views_path}/../shared/JsHttpRequest.js"></script>

	<xsl:variable name="title">
		<xsl:choose>
			<xsl:when test="client/id &gt; 0"><xsl:value-of select="$locale/clients/client/edit/title1/text()"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="$locale/clients/client/edit/title2/text()"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	
	<xsl:variable name="id" select="client/id"/>
	<xsl:variable name="type_id" select="client/type_id"/>
	<xsl:variable name="firstname" select="client/firstname"/>
	<xsl:variable name="lastname" select="client/lastname"/>
	<xsl:variable name="email" select="client/email"/>
	<xsl:variable name="city" select="client/city"/>
	<xsl:variable name="zip" select="client/zip"/>
	<xsl:variable name="address" select="client/address"/>
	<xsl:variable name="phone" select="client/phone"/>
	<xsl:variable name="bdate" select="client/bdate"/>
	<xsl:variable name="status" select="client/status"/>
	<xsl:variable name="send_news" select="client/send_news"/>
	<xsl:variable name="purse" select="client/purse"/>
	<xsl:variable name="position"><xsl:value-of select="client/position"/></xsl:variable>

	<form name="client" id="client" action="?{$controller_name}.save" method="post">
		<input type="hidden" name="id" value="{$id}"/>
		<input type="hidden" name="last_position" value="{last_position}"/>
		<input type="hidden" name="page" value="{$page}"/>
		<input type="hidden" name="client[id]" value="{$id}"/>
		<input type="hidden" name="client[position]" value="{$position}"/>
		<input type="hidden" name="client[lang]" value="{$lang}"/>
	<table class="form" cellspacing="0" cellpadding="0">
	
		<!-- Show message -->
		<xsl:if test="save_result = 'true'">
		<tr><td colspan="2"><div class="message"><xsl:value-of select="$locale/clients/client/edit/save_msg_success/text()"/></div></td></tr>
		</xsl:if>
		<xsl:if test="save_result = 'false'">
		<tr><td colspan="2"><div class="error"><xsl:value-of select="$locale/clients/client/edit/save_msg_error/text()"/></div></td></tr>
		</xsl:if>
		<!-- /Show message -->
		
		<tr><th colspan="2"><xsl:value-of select="$title"/></th></tr>
		
		<tr>
			<td class="left_label"><label for="type_id"><xsl:value-of select="$locale/clients/client/edit/type_id/text()"/></label></td>
			<td>
			<xsl:variable name="selected" select="client/type_id"/>
			<select name="client[type_id]" class="middle" id="type_id">
				<xsl:for-each select="client/types/*">
					<xsl:call-template name="gen-option">
						<xsl:with-param name="value" select="id"/>
						<xsl:with-param name="title" select="name"/>
						<xsl:with-param name="selected" select="$selected"/>
					</xsl:call-template>
				</xsl:for-each>
			</select>
			</td>
		</tr>
		
		<tr>
			<td class="left_label"><label for="firstname"><xsl:value-of select="$locale/clients/client/edit/firstname/text()"/></label></td>
			<td><input type="text" class="medium" name="client[firstname]" id="firstname" value="{$firstname}"/></td>
		</tr>
		
		<tr>
			<td class="left_label"><label for="lastname2"><xsl:value-of select="$locale/clients/client/edit/lastname2/text()"/></label></td>
			<td><input type="text" class="medium" name="client[lastname2]" id="lastname2" value="{client/lastname2}"/></td>
		</tr>
		
		<tr>
			<td class="left_label"><label for="lastname"><xsl:value-of select="$locale/clients/client/edit/lastname/text()"/></label></td>
			<td><input type="text" class="medium" name="client[lastname]" id="lastname" value="{$lastname}"/></td>
		</tr>
		
		<tr>
			<td class="left_label"><label for="email"><xsl:value-of select="$locale/clients/client/edit/email/text()"/></label></td>
			<td><input type="text" class="large" name="client[email]" id="email" value="{$email}"/></td>
		</tr>
		<!--
		<tr>
			<td class="left_label">������</label></td>
			<td><xsl:value-of select="client/password"/></td>
		</tr>
		-->
		<tr>
			<td class="left_label"><label for="city"><xsl:value-of select="$locale/clients/client/edit/city/text()"/></label></td>
			<td><input type="text" class="medium" name="client[city]" id="city" value="{$city}"/></td>
		</tr>
		
		<!--
		<tr>
			<td class="left_label"><label for="zip"><xsl:value-of select="$locale/clients/client/edit/zip/text()"/></label></td>
			<td><input type="text" class="small" name="client[zip]" id="zip" value="{$zip}"/></td>
		</tr>
		-->
			
		<!--
		<tr>
		<td class="ftitle"><xsl:value-of select="$locale/clients/client/edit/region/text()" disable-output-escaping="yes"/></td>
		<td>
			<xsl:variable name="selected" value=""/>
			<select name="region_id" id="region_id" onchange="FillStreets()">
				<xsl:for-each select="regions/*">
					<xsl:call-template name="gen-option">
						<xsl:with-param name="value" select="id"/>
						<xsl:with-param name="title" select="name"/>
						<xsl:with-param name="selected" select="$selected"/>
					</xsl:call-template>
				</xsl:for-each>
			</select>
		</td></tr>
		-->
		
		<tr>
		<td class="ftitle"><xsl:value-of select="$locale/clients/client/edit/street/text()" disable-output-escaping="yes"/></td>
		<td>
			<xsl:variable name="selected" value=""/>
			<select name="client[street_id]" id="street_id">
			</select>
		</td></tr>
		
		<tr>
			<td class="left_label"><label for="address"><xsl:value-of select="$locale/clients/client/edit/address/text()"/></label></td>
			<td><input type="text" class="large" name="client[address]" id="address" value="{$address}"/></td>
		</tr>
		
		<tr>
			<td class="left_label"><label for="phone"><xsl:value-of select="$locale/clients/client/edit/phone/text()"/></label></td>
			<td><strong><xsl:value-of select="client/country/name"/> +<xsl:value-of select="client/country/phone_code"/>&#160;</strong> <input type="text" class="medium" name="client[phone]" id="phone" value="{$phone}"/></td>
		</tr>
						
		<tr><td colspan="2"><label for="description"><xsl:value-of select="$locale/clients/client/edit/description/text()" disable-output-escaping="yes"/></label></td></tr>
		<tr>
			<td colspan="2"><textarea class="tinymce" cols="30" rows="3" name="client[description]" id="description" style="height:40px"><xsl:value-of select="client/description"/></textarea></td>
		</tr>
		
		<!--
		<tr>
			<td class="left_label"><label for="bdate"><xsl:value-of select="$locale/clients/client/edit/bdate/text()"/></label></td>
			<td><input type="text" class="small" name="client[bdate]" id="bdate" value="{$bdate}"/></td>
		</tr>
		-->
		
		<xsl:if test="$status != 1">
		<tr>
			<td class="left_label"><label for="status"><xsl:value-of select="$locale/clients/client/edit/status/text()"/></label></td>
			<td>
				<xsl:choose>
					<xsl:when test="$status = 2"><input type="radio" name="client[status]" id="status_active" value="2" checked="yes" /></xsl:when>
					<xsl:otherwise><input type="radio" name="client[status]" id="status_active" value="2" /></xsl:otherwise>
				</xsl:choose>
				<label for="status_active" class="lower"><xsl:value-of select="$locale/clients/client/edit/statuses/active/text()"/></label>
				<xsl:choose>
					<xsl:when test="$status = 0"><input type="radio" name="client[status]" id="status_inactive" value="0" checked="yes"/></xsl:when>
					<xsl:otherwise><input type="radio" name="client[status]" id="status_inactive" value="0"/></xsl:otherwise>
				</xsl:choose>
				<label for="status_inactive" class="lower"><xsl:value-of select="$locale/clients/client/edit/statuses/inactive/text()"/></label>
			</td>
		</tr>
		</xsl:if>
		
		<tr>
			<td class="left_label"><label for="send_news"><xsl:value-of select="$locale/clients/client/edit/send_news/text()"/></label></td>
			<td>
				<xsl:choose>
					<xsl:when test="$send_news = 1"><input type="radio" name="client[send_news]" id="send_news_active" value="1" checked="yes" /></xsl:when>
					<xsl:otherwise><input type="radio" name="client[send_news]" id="send_news_active" value="1" /></xsl:otherwise>
				</xsl:choose>
				<label for="send_news_active" class="lower"><xsl:value-of select="$locale/common/yes/text()"/></label>
				<xsl:choose>
					<xsl:when test="$send_news = 0"><input type="radio" name="client[send_news]" id="send_news_inactive" value="0" checked="yes"/></xsl:when>
					<xsl:otherwise><input type="radio" name="client[send_news]" id="send_news_inactive" value="0"/></xsl:otherwise>
				</xsl:choose>
				<label for="send_news_inactive" class="lower"><xsl:value-of select="$locale/common/no/text()"/></label>
			</td>
		</tr>
		
		<tr>
			<td class="left_label"><label for="allow_fair"><xsl:value-of select="$locale/clients/client/edit/allow_fair/text()"/></label></td>
			<td>
				<xsl:choose>
					<xsl:when test="client/allow_fair = 1"><input type="radio" name="client[allow_fair]" id="allow_fair_active" value="1" checked="yes" /></xsl:when>
					<xsl:otherwise><input type="radio" name="client[allow_fair]" id="allow_fair_active" value="1" /></xsl:otherwise>
				</xsl:choose>
				<label for="allow_fair_active" class="lower"><xsl:value-of select="$locale/common/yes/text()"/></label>
				<xsl:choose>
					<xsl:when test="client/allow_fair = 0"><input type="radio" name="client[allow_fair]" id="allow_fair_inactive" value="0" checked="yes"/></xsl:when>
					<xsl:otherwise><input type="radio" name="client[allow_fair]" id="allow_fair_inactive" value="0"/></xsl:otherwise>
				</xsl:choose>
				<label for="allow_fair_inactive" class="lower"><xsl:value-of select="$locale/common/no/text()"/></label>
			</td>
		</tr>
		
		<tr>
			<td class="left_label"><label for="allow_userfile"><xsl:value-of select="$locale/clients/client/edit/allow_userfile/text()"/></label></td>
			<td>
				<xsl:choose>
					<xsl:when test="client/allow_userfile = 1"><input type="radio" name="client[allow_userfile]" id="allow_userfile_active" value="1" checked="yes" /></xsl:when>
					<xsl:otherwise><input type="radio" name="client[allow_userfile]" id="allow_userfile_active" value="1" /></xsl:otherwise>
				</xsl:choose>
				<label for="allow_userfile_active" class="lower"><xsl:value-of select="$locale/common/yes/text()"/></label>
				<xsl:choose>
					<xsl:when test="client/allow_userfile = 0"><input type="radio" name="client[allow_userfile]" id="allow_userfile_inactive" value="0" checked="yes"/></xsl:when>
					<xsl:otherwise><input type="radio" name="client[allow_userfile]" id="allow_userfile_inactive" value="0"/></xsl:otherwise>
				</xsl:choose>
				<label for="allow_userfile_inactive" class="lower"><xsl:value-of select="$locale/common/no/text()"/></label>
			</td>
		</tr>
		
		<!--<tr>
			<td class="left_label"><label for="allow_fair"><xsl:value-of select="$locale/clients/client/edit/professional/text()"/></label></td>
			<td>
				<xsl:choose>
					<xsl:when test="client/professional = 1"><input type="radio" name="client[professional]" id="professional_active" value="1" checked="yes" /></xsl:when>
					<xsl:otherwise><input type="radio" name="client[professional]" id="professional_active" value="1" /></xsl:otherwise>
				</xsl:choose>
				<label for="professional_active" class="lower"><xsl:value-of select="$locale/common/yes/text()"/></label>
				<xsl:choose>
					<xsl:when test="client/professional = 0"><input type="radio" name="client[professional]" id="professional_inactive" value="0" checked="yes"/></xsl:when>
					<xsl:otherwise><input type="radio" name="client[professional]" id="professional_inactive" value="0"/></xsl:otherwise>
				</xsl:choose>
				<label for="professional_inactive" class="lower"><xsl:value-of select="$locale/common/no/text()"/></label>
			</td>
		</tr>-->
		
		<tr>
			<td class="left_label"><label for="photo_discount"><xsl:value-of select="$locale/clients/client/edit/photo_discount/text()"/></label></td>
			<td><input type="text" class="medium" name="client[photo_discount]" id="photo_discount" value="{client/photo_discount}"/></td>
		</tr>
		
		<tr>
			<td class="left_label"><label for="printbook_discount"><xsl:value-of select="$locale/clients/client/edit/printbook_discount/text()"/></label></td>
			<td><input type="text" class="medium" name="client[printbook_discount]" id="printbook_discount" value="{client/printbook_discount}"/></td>
		</tr>
		
		<tr><th colspan="2" class="actions">
			<xsl:if test="client/status = 1"><input type="button" id="ba" style="visibility:hidden" onclick="document.location='?{$controller_name}.activate&amp;id={client/id}&amp;page={$page}'" value="{$locale/clients/client/buttons/activate/text()}"/>&#160;</xsl:if>
			<xsl:if test="client/order_limit = 1"><input type="button" id="bc" style="visibility:hidden" onclick="document.location='?{$controller_name}.clear_order_limit&amp;id={client/id}&amp;page={$page}'" value="{$locale/clients/client/buttons/clear_order_limit/text()}"/>&#160;</xsl:if>
			<input type="submit" id="bs" style="visibility:hidden" value="{$locale/common/buttons/form_save/text()}"/>
		</th></tr>
	</table>
	
	<script>
	FillStreets();
	function FillStreets()
	{
		var r, s, region_id, d, i;
		
		//r = $('region_id');
		s = $('street_id');
		
		s.options.length = 0;
		s.options[0] = new Option( '��������....................', 0 );
		d = new Date();
		//region_id = r.options[r.selectedIndex].value;
		region_id = 0;
		JsHttpRequest.query(
			'?base.get_streets&amp;region_id='+region_id+'&amp;date='+ d.getTime(),
			{
			},
			function( result, txt )
			{
				s.options[0] = new Option( '- ����������� -', 0 );
				for( i=1; i&lt;result['streets'].length; i++ )
				{
					s.options[i] = new Option( result['streets'][i][1], result['streets'][i][0] );
				}
				select_select( s, <xsl:value-of select="client/street_id"/> );
				if( $('ba') ) $('ba').style.visibility = 'visible';
				if( $('bc') ) $('bc').style.visibility = 'visible';
				if( $('bs') ) $('bs').style.visibility = 'visible';
			}
		);
	}
	</script>
		
	</form>
	
	<script language="JavaScript" src="{$views_path}/article.js"></script>
	
	<br/><br/>
	<xsl:call-template name="tiny_mce"/>

	<form name="client" id="client" action="?{$controller_name}.send2client" method="post" enctype="multipart/form-data">
		<input type="hidden" name="id" value="{client/id}"/>
		<input type="hidden" name="page" value="{$page}"/>
	<table class="form" cellspacing="0" cellpadding="0">
		
		<tr><th colspan="2"><xsl:value-of select="$locale/clients/send/title2/text()"/></th></tr>
		
		<tr>
			<td class="left_label"><label><xsl:value-of select="$locale/clients/send/subject/text()"/></label></td>
			<td><input type="text" class="large" name="subject" value=""/></td>
		</tr>
		
		<tr>
			<td colspan="2" class="left_label"><label><xsl:value-of select="$locale/clients/send/body/text()"/></label></td>
		</tr>
		<tr>
			<td colspan="2"><textarea name="body" class="tinymce large" rows="20"></textarea></td>
		</tr>
		
		<tr>
			<td class="left_label"><label><xsl:value-of select="$locale/clients/send/file/text()"/></label></td>
			<td><input type="file" name="file" value=""/></td>
		</tr>
		
		<tr><th colspan="2" class="actions">
			<input type="submit" value="{$locale/common/buttons/form_send/text()}"/>
		</th></tr>
	</table>
	</form>

	<br/><br/>

	<form name="push_notification" id="push_notification" action="?{$controller_name}.push2client" method="post" enctype="multipart/form-data">
		<input type="hidden" name="id" value="{client/id}"/>
		<input type="hidden" name="page" value="{$page}"/>
	<table class="form" cellspacing="0" cellpadding="0">
		
		<tr><th colspan="2"><xsl:value-of select="$locale/clients/push/title/text()"/></th></tr>
		
		<tr>
			<td class="left_label"><label><xsl:value-of select="$locale/clients/push/subject/text()"/></label></td>
			<td><input type="text" class="large" name="subject" value="" maxlength="400"/></td>
		</tr>
		
		<tr>
			<td class="left_label"><label><xsl:value-of select="$locale/clients/push/body/text()"/></label></td>
			<td><input type="text" class="large" name="body" value="" maxlength="400"/></td>
		</tr>
		
		<tr><th colspan="2" class="actions">
			<input type="submit" value="{$locale/common/buttons/form_send/text()}"/>
		</th></tr>
	</table>
	</form>
	   
</xsl:template>

</xsl:stylesheet>

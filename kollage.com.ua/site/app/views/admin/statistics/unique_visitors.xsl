<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" version="1.0">

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:template match="data">
Unique visitors: <b><xsl:value-of select="uv"/></b> (From: <xsl:value-of select="from_date"/> To: <xsl:value-of select="to_date"/>)
</xsl:template>

</xsl:stylesheet>

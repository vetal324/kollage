<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" version="1.0">
                
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:template name="paginator">
  <xsl:param name="prefix"/>
    <table cellspacing="0" cellpadding="0" style="width:100%;"><tr>
        <td width="2%">&#160;<a href="#" onclick="goto_prev('{$prefix}','{$page_prev}');return(false)"><img src="{$views_path}/images/paginator/go_prev.gif" border="0"/></a></td>
        <td width="5%" style="text-align:center"><xsl:value-of select="$page"/>/<xsl:value-of select="$pages"/></td>
        <td width="2%"><a href="#" onclick="goto_next('{$prefix}','{$page_next}');return(false)"><img src="{$views_path}/images/paginator/go_next.gif" border="0"/></a></td>

        <td style="width:50px">&#160;</td>
        <td width="10%"><xsl:value-of select="$locale/paginator/goto_page/text()"/></td>
        <td width="5%"><input type="text" name="page{$prefix}" id="page{$prefix}" value="1" style="width:40px"/></td>
        <td width="2%"><a href="#" onclick="goto_page('{$prefix}');return(false)">&#160;<img src="{$views_path}/images/paginator/go.gif" border="0"/></a></td>
        <td style="width:50px">&#160;</td>

        <td width="10%"><xsl:value-of select="$locale/paginator/rows_per_page/text()"/></td>
        <td width="5%"><select name="rows{$prefix}" id="rows{$prefix}">
        <xsl:variable name="options">
            <item id="10"/>
            <item id="15"/>
            <item id="20"/>
            <item id="25"/>
            <item id="30"/>
            <item id="40"/>
            <item id="50"/>
        </xsl:variable>
        <xsl:variable name="selected" select="$rows_per_page"/>
            <xsl:call-template name="gen-option">
                <xsl:with-param name="value">10</xsl:with-param>
                <xsl:with-param name="title">10</xsl:with-param>
                <xsl:with-param name="selected" select="$selected"/>
            </xsl:call-template>
            <xsl:call-template name="gen-option">
                <xsl:with-param name="value">20</xsl:with-param>
                <xsl:with-param name="title">20</xsl:with-param>
                <xsl:with-param name="selected" select="$selected"/>
            </xsl:call-template>
            <xsl:call-template name="gen-option">
                <xsl:with-param name="value">25</xsl:with-param>
                <xsl:with-param name="title">25</xsl:with-param>
                <xsl:with-param name="selected" select="$selected"/>
            </xsl:call-template>
            <xsl:call-template name="gen-option">
                <xsl:with-param name="value">30</xsl:with-param>
                <xsl:with-param name="title">30</xsl:with-param>
                <xsl:with-param name="selected" select="$selected"/>
            </xsl:call-template>
            <xsl:call-template name="gen-option">
                <xsl:with-param name="value">50</xsl:with-param>
                <xsl:with-param name="title">50</xsl:with-param>
                <xsl:with-param name="selected" select="$selected"/>
            </xsl:call-template>
        </select></td>
        <td width="2%"><a href="#" onclick="set_rows('{$prefix}');return(false)">&#160;<img src="{$views_path}/images/paginator/go.gif" border="0"/></a>&#160;</td>
    </tr></table>
</xsl:template>

</xsl:stylesheet>

<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:asp="remove" version="1.0">
                
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:variable name="path"><xsl:value-of select="/documents/options/option[@name='templates_folder']/@value"/></xsl:variable>

<xsl:template match="/">
  <xsl:variable name="return_url" select="/documents/content/return_url"/>
  <html>
    <head>
      <title><xsl:value-of select="/documents/dictionary/item[@id='1']"/></title>
      <link rel="stylesheet" href="{$path}styles/styles.css" />
    </head>
    <body>
         <table width="100%" height="80%" border="0" cellspacing="0" cellpadding="0">
            <tr><td valign="middle" align="center">
            
            <table width="384" height="212" border="0" cellspacing="0" cellpadding="0" background="{$path}elements/logon/images/login_form_bg.gif"><tr><td>
            
                <table width="384" height="212" border="0" cellspacing="0" cellpadding="0" background="">
                    <tr>
                        <td height="25" valign="top" align="left">
                            <table border="0" cellspacing="0" cellpadding="0"><tr>
                                <td><img src="{$path}elements/bigtabs/images/bigtab_left_active.gif"/></td>
                                <td align="center" background="{$path}elements/bigtabs/images/bigtab_body_active.gif" class="bigtab_active"><a href="default.aspx" class="bigtab_active">RESTRICTED AREA. Du ma logge inn</a></td>
                                <td><img src="{$path}elements/bigtabs/images/bigtab_right_active.gif"/></td>
                            </tr></table>
                        </td>
                    </tr>
                    <tr>
                        <td height="187" valign="middle" align="center">
                            <label class="error" name="Msg" id="Msg"><xsl:value-of select="/documents/content/item[@name='message']"/></label>
                            <table border="0" cellspacing="2" cellpadding="4" background="">
                                <form name="frmlogin" method="POST" action="logon.aspx">
                                <input type="hidden" name="cmd" value="login"/>
                                <input type="hidden" name="ReturnUrl" value="{$return_url}"/>
                                <tr>
                                    <td class="lbllogin">Brukernavn:</td>
                                    <td><input type="text" name="UserName" id="UserName"/></td>
                                </tr>
                                <tr>
                                    <td class="lbllogin">Passord:</td>
                                    <td><input type="password" name="UserPass" id="UserPass"/></td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="right"><input type="submit" value="LOGIN" class="btn"/></td>
                                </tr>
                                </form>
                            </table>
                        </td>
                    </tr>
                </table>
            
            </td></tr></table>
            
            </td></tr>
         </table>
    </body>
  </html>
</xsl:template>

</xsl:stylesheet>

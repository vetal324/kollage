<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:msxsl="urn:schemas-microsoft-com:xslt"
				xmlns:exsl="http://exslt.org/common"
				extension-element-prefixes="exsl"
				version="1.0">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:template match="data">
	<xsl:choose>
		<xsl:when test="/content/global/user/roles/role[code='streets']/@selected = 'yes'">
			<xsl:apply-templates/>
		</xsl:when>
		<xsl:otherwise>
			<div class="error"><xsl:value-of select="$locale/common/msgs/access_denied/text()"/></div>
			<script type="text/javascript">RemoveSmallTabs();RemoveLeftPanel();</script>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="street_edit">

	<xsl:call-template name="tiny_mce"/>

	<xsl:variable name="title">
		<xsl:choose>
			<xsl:when test="street/id &gt; 0"><xsl:value-of select="$locale/streets/edit/title1/text()"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="$locale/streets/edit/title2/text()"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	
	<xsl:variable name="id" select="street/id"/>
	<xsl:variable name="folder" select="street/folder"/>
	<xsl:variable name="position"><xsl:value-of select="street/position"/></xsl:variable>

	<form name="street_edit" id="street_edit" action="?{$controller_name}.save" method="post" enctype="multipart/form-data">
		<input type="hidden" name="folder" value="{$folder}"/>
		<input type="hidden" name="id" value="{$id}"/>
		<input type="hidden" name="last_position" value="{last_position}"/>
		<input type="hidden" name="object[id]" value="{$id}"/>
		<input type="hidden" name="object[folder]" value="{$folder}"/>
		<input type="hidden" name="object[position]" value="{$position}"/>
		<input type="hidden" name="object[lang]" value="{$lang}"/>
	<table class="form" cellspacing="0" cellpadding="0">
	
		<!-- Show message -->
		<xsl:if test="save_result = 'true'">
		<tr><td colspan="2"><div class="message"><xsl:value-of select="$locale/streets/edit/save_msg_success/text()"/></div></td></tr>
		</xsl:if>
		<xsl:if test="save_result = 'false'">
		<tr><td colspan="2"><div class="error"><xsl:value-of select="$locale/streets/edit/save_msg_error/text()"/></div></td></tr>
		</xsl:if>
		<!-- /Show message -->
		
		<tr><th colspan="2"><xsl:value-of select="$title"/></th></tr>
		
		<tr>
			<td class="left_label"><label for="name"><xsl:value-of select="$locale/streets/edit/name/text()"/></label></td>
			<td><input type="text" class="large" name="object[name]" id="name" value="{street/name}"/></td>
		</tr>
		
		<tr><td class="label"><label for="region_id"><xsl:value-of select="$locale/streets/edit/region/text()"/></label></td>
		<td>
			<xsl:variable name="selected" select="street/region_id"/>
			<select name="object[region_id]" class="middle" id="region_id">
				<xsl:for-each select="regions/*">
					<xsl:call-template name="gen-option">
						<xsl:with-param name="value" select="id"/>
						<xsl:with-param name="title" select="name"/>
						<xsl:with-param name="selected" select="$selected"/>
					</xsl:call-template>
				</xsl:for-each>
			</select>
		</td></tr>
		
		<tr>
			<td class="left_label"><label for="square"><xsl:value-of select="$locale/streets/edit/square/text()"/></label></td>
			<td><input type="text" class="small" name="object[square]" id="square" value="{street/square}"/></td>
		</tr>
		
		<tr><td colspan="2" class="label"><label for="description"><xsl:value-of select="$locale/streets/edit/description/text()"/></label></td></tr>
		<tr><td colspan="2"><textarea name="object[description]" id="description" class="tinymce large" rows="8" cols="80"><xsl:value-of select="street/description"/></textarea></td></tr>
		
		<tr>
			<td class="left_label"><label for="image"><xsl:value-of select="$locale/streets/edit/image/text()"/></label></td>
			<td>
				<table cellspacing="0" cellpadding="0"><tr>
				<td><input type="file" size="60" name="image" id="image"/></td>
				<td><xsl:if test="street/image != ''">&#160;&#160;<a href="#" onclick="del_image('?{$controller_name}.del_image&amp;id={$id}')"><img  src="{$views_path}/images/del.gif" border="0"/></a></xsl:if></td>
				<td><xsl:if test="street/image != ''"><xsl:variable name="image" select="street/image"/>
				&#160;&#160;<a href="#" onclick="return(popup_image_preview('{street/image/@width}','{street/image/@height}','{$data_path}/{$image}'))"><img src="{$views_path}/images/preview.gif" border="0"/></a></xsl:if></td>
			</tr></table>
			</td>
		</tr>
		
		<tr><th colspan="2" class="actions"><input type="submit" value="{$locale/common/buttons/form_save/text()}"/></th></tr>
	</table>
		
	</form>
		
	<script language="JavaScript" src="{$views_path}/article.js"></script>
	
</xsl:template>

</xsl:stylesheet>

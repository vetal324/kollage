<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:template match="/">
  <html>
	  <head>
		  <title>
			  <xsl:value-of select="$locale/common/title/text()"/>
		  </title>
		  <link rel="stylesheet" href="{$views_path}/styles.css"/>
		  <link rel="stylesheet" href="/fonts/awesome/css/font-awesome.min.css"/>
		  <link rel="stylesheet" href="{$views_path}/books/css/kollage-books.css?version=28"/>
		  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"/>
		  <script language="JavaScript" src="{$views_path}/../shared/bd.js"></script>
		  <script language="JavaScript" src="{$views_path}/../shared/prototype.js"></script>
		  <script language="JavaScript" src="{$views_path}/common.js"></script>
		  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		  <script language="JavaScript" >
			  window.$j = jQuery.noConflict();
		  </script>
	  </head>
	<body>
		<div id="first_table"><xsl:call-template name="logo"/></div>
		
		<table cellspacing="0" cellpadding="0" width="100%" border="0">
		<tr><td>
				<xsl:apply-templates select="content/documents/bigtabs"/>
		</td></tr>
		</table>

		<table cellspacing="0" cellpadding="0" width="100%">
		<tr>
			<td width="180"><img border="0" src="{$views_path}/images/menuleft/menu_left_header.gif"/></td>
			<td background="{$views_path}/images/smalltabs/after_bar_bg.gif" valign="bottom">
				<table cellspacing="0" cellpadding="0" width="100%" class="admin-panel-small-tabs">
				<tr>
					<td><xsl:apply-templates select="content/documents/smalltabs"/></td>
				</tr>
				</table>
			</td>
		</tr>
		</table>

		<table cellspacing="0" cellpadding="0" width="100%" height="90%">
		<tr>
			<td width="180" valign="top" class="admin-panel-left-panel" background="{$views_path}/images/menuleft/menu_left_bg.gif">
				<xsl:apply-templates select="content/documents/menuleft"/>
				<xsl:if test="content/documents/left_panel/@show = 'yes'">
					<xsl:call-template name="left_panel"/>
				</xsl:if>
			</td>
			<td valign="top">
				<div style="padding:12px"><xsl:apply-templates select="content/documents/data"/></div>
			</td>
		</tr>
		</table>
		
	</body>
  </html>
</xsl:template>

<xsl:template name="logo">
	<table cellspacing="2" cellpadding="4" width="100%"><tr>
		<td width="2%"><a href="/admin/"><img src="{$views_path}/images/logo.gif" border="0" height="50"/></a></td>
		<td><xsl:value-of select="$locale/common/logged_as/text()"/><b><xsl:value-of select="/content/global/user/login"/></b></td>
	</tr></table>
</xsl:template>

</xsl:stylesheet>

<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:msxsl="urn:schemas-microsoft-com:xslt"
				xmlns:exsl="http://exslt.org/common"
				extension-element-prefixes="exsl"
				version="1.0">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:include href="left_panel.xsl" />

<xsl:template match="data">
	<xsl:choose>
		<xsl:when test="/content/global/user/roles/role[code='fair']/@selected = 'yes'">
			<xsl:apply-templates/>
		</xsl:when>
		<xsl:otherwise>
			<div class="error"><xsl:value-of select="$locale/common/msgs/access_denied/text()"/></div>
			<script type="text/javascript">RemoveSmallTabs();RemoveLeftPanel();</script>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="edit">

	<xsl:call-template name="tiny_mce"/>

	<xsl:variable name="title">
		<xsl:choose>
			<xsl:when test="settings/id &gt; 0"><xsl:value-of select="$locale/shop/settings/edit/title1/text()"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="$locale/shop/settings/edit/title2/text()"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<form name="settings_edit" id="settings_edit" action="?{$controller_name}.settings_save" method="post">
		<input type="hidden" name="id" value="1"/>
		<input type="hidden" name="object[id]" value="1"/>
		<input type="hidden" name="object[lang]" value="{$lang}"/>
	<table class="form" cellspacing="0" cellpadding="0" border="0">
	
		<!-- Show message -->
		<xsl:if test="save_result = 'true'">
		<tr><td colspan="2"><div class="message"><xsl:value-of select="$locale/shop/settings/edit/save_msg_success/text()"/></div></td></tr>
		</xsl:if>
		<xsl:if test="save_result = 'false'">
		<tr><td colspan="2"><div class="error"><xsl:value-of select="$locale/shop/settings/edit/save_msg_error/text()"/></div></td></tr>
		</xsl:if>
		<!-- /Show message -->
		
		<tr><th colspan="2"><xsl:value-of select="$title"/></th></tr>
		
		<tr><th colspan="2" class="actions" style="text-align:left"><xsl:value-of select="$locale/sop/settings/edit/mail_to_client/text()"/></th></tr>
		<tr>
			<td class="left_label"><label for="order_mail_subject"><xsl:value-of select="$locale/shop/settings/edit/order_mail_subject/text()"/></label></td>
			<td><input type="text" class="large" name="object[order_mail_subject]" id="order_mail_subject" value="{settings/order_mail_subject}"/></td>
		</tr>
		
		<tr><td class="left_label" colspan="2"><label for="order_mail_body"><xsl:value-of select="$locale/shop/settings/edit/order_mail_body/text()"/></label></td></tr>
		<tr><td colspan="2"><textarea name="object[order_mail_body]" id="order_mail_body" class="large" rows="15" cols="80"><xsl:value-of select="settings/order_mail_body"/></textarea></td></tr>
		
		<tr><td colspan="2">
			{CLIENT} - <xsl:value-of select="$locale/sop/settings/edit/client/text()"/>;
			{ORDER_NR} - <xsl:value-of select="$locale/shop/settings/edit/order_nr/text()"/>;
			{ORDER_STATUS} - <xsl:value-of select="$locale/shop/settings/edit/order_status/text()"/>;
			{ORDER} - <xsl:value-of select="$locale/shop/settings/edit/order/text()"/>;
		</td></tr>
		
		<tr><th colspan="2" class="actions" style="text-align:left"><xsl:value-of select="$locale/sop/settings/edit/mail_to_admin/text()"/></th></tr>
		<tr>
			<td class="left_label"><label for="admin_emails"><xsl:value-of select="$locale/sop/settings/edit/admin_emails/text()"/></label></td>
			<td><input type="text" class="large" name="object[admin_emails]" id="admin_emails" value="{settings/admin_emails}"/></td>
		</tr>
		<tr>
			<td class="left_label"><label for="admin_order_mail_subject"><xsl:value-of select="$locale/sop/settings/edit/order_mail_subject/text()"/></label></td>
			<td><input type="text" class="large" name="object[admin_order_mail_subject]" id="admin_order_mail_subject" value="{settings/admin_order_mail_subject}"/></td>
		</tr>
		
		<tr><td class="left_label" colspan="2"><label for="admin_order_mail_body"><xsl:value-of select="$locale/sop/settings/edit/order_mail_body/text()"/></label></td></tr>
		<tr><td colspan="2"><textarea name="object[admin_order_mail_body]" id="admin_order_mail_body" class="large" rows="15" cols="80"><xsl:value-of select="settings/admin_order_mail_body"/></textarea></td></tr>
		
		<tr><td colspan="2">
			{CLIENT} - <xsl:value-of select="$locale/sop/settings/edit/client/text()"/>;
			{ORDER_NR} - <xsl:value-of select="$locale/sop/settings/edit/order_nr/text()"/>;
			{ORDER_STATUS} - <xsl:value-of select="$locale/sop/settings/edit/order_status/text()"/>;
			{ORDER} - <xsl:value-of select="$locale/sop/settings/edit/order/text()"/>;
		</td></tr>
		
		<tr><th colspan="2" class="actions"><input type="submit" value="{$locale/common/buttons/form_save/text()}"/></th></tr>
	</table>
		
	</form>
	
	<script language="JavaScript" src="{$views_path}/article.js"></script>
	
</xsl:template>

</xsl:stylesheet>

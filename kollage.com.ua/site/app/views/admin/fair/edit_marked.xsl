<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout.xsl" />
<xsl:include href="../menu.xsl" />
<xsl:include href="../paginator.xsl" />

<xsl:include href="left_panel.xsl" />

<xsl:template match="data">
	<xsl:choose>
		<xsl:when test="/content/global/user/roles/role[code='fair']/@selected = 'yes'">
			<xsl:apply-templates select="fair_edit"/>
		</xsl:when>
		<xsl:otherwise>
			<div class="error"><xsl:value-of select="$locale/common/msgs/access_denied/text()"/></div>
			<script type="text/javascript">RemoveSmallTabs();RemoveLeftPanel();</script>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="fair_edit">
		
		<form method="post" action="?{$controller_name}.save_marked">
		<input type="hidden" name="id" value="{object/id}"/>
		<input type="hidden" name="category_id" value="{object/category_id1}"/>
		<input type="hidden" name="object[status]" value="1"/>
		<input type="hidden" name="object[lang]" value="{$lang}"/>
		<input type="hidden" name="ids" value="{ids}"/>
		<table class="form" cellpadding="3" cellspacing="1">
			
		<!-- Show message -->
		<xsl:if test="save_result = 'true'">
		<tr><td colspan="2"><div class="message"><xsl:value-of select="$locale/shop/product/edit/save_msg_success/text()"/></div></td></tr>
		</xsl:if>
		<xsl:if test="save_result = 'false'">
		<tr><td colspan="2"><div class="error"><xsl:value-of select="$locale/shop/product/edit/save_msg_error/text()"/></div></td></tr>
		</xsl:if>
		<!-- /Show message -->
			
			<tr>
				<th colspan="2"><xsl:value-of select="$locale/fair/edit/title_edit_marked/text()"/>&#160;(<xsl:value-of select="ids/@count"/>&#160;��.)</th>
			</tr>
			
			<tr>
				<td align="center" valign="top" style="vertical-align:top;text-align:center">
					<xsl:if test="object[position()=1]/id"><div style="margin-bottom:5px"><img src="?{$controller_name}.display_photo&amp;id={object[position()=1]/id}" border="0" width="{object[position()=1]/thumbnail_w}" height="{object[position()=1]/thumbnail_h}"/></div></xsl:if>
					<xsl:if test="object[position()=2]/id"><div style="margin-bottom:5px"><img src="?{$controller_name}.display_photo&amp;id={object[position()=2]/id}" border="0" width="{object[position()=2]/thumbnail_w}" height="{object[position()=2]/thumbnail_h}"/></div></xsl:if>
					<xsl:if test="object[position()=3]/id"><div style="margin-bottom:5px"><img src="?{$controller_name}.display_photo&amp;id={object[position()=3]/id}" border="0" width="{object[position()=3]/thumbnail_w}" height="{object[position()=3]/thumbnail_h}"/></div></xsl:if>
					<xsl:if test="object[position()=4]/id"><div style="margin-bottom:5px"><img src="?{$controller_name}.display_photo&amp;id={object[position()=4]/id}" border="0" width="{object[position()=4]/thumbnail_w}" height="{object[position()=4]/thumbnail_h}"/></div></xsl:if>
					<xsl:if test="object[position()=5]/id"><div style="margin-bottom:5px"><img src="?{$controller_name}.display_photo&amp;id={object[position()=5]/id}" border="0" width="{object[position()=5]/thumbnail_w}" height="{object[position()=5]/thumbnail_h}"/></div></xsl:if>
				</td>
				<td valign="top">
					<table width="350px">
						<tr>
							<td><xsl:value-of select="$locale/fair/edit/title/text()" disable-output-escaping="yes"/>*</td>
							<td><input type="text" name="object[title]" id="title" class="middle" value="{object/title}"/></td>
						</tr>
						
						<tr>
							<td><xsl:value-of select="$locale/fair/edit/category1/text()" disable-output-escaping="yes"/>*</td>
							<td>
								<select name="category_id1" id="category_id1" onchange="FillSubCategoriesBox()"></select><br/>
								<select name="category_id2" id="category_id2" onchange="FillSubSubCategoriesBox()"></select><br/>
								<select name="category_id3" id="category_id3"></select>
							</td>
						</tr>
						
						<tr><td colspan="2"><xsl:value-of select="$locale/fair/edit/description/text()" disable-output-escaping="yes"/>*</td></tr>
						<tr>
							<td colspan="2"><textarea cols="30" rows="3" name="object[description]" id="description" style="width:335px;height:80px"><xsl:value-of select="object/description"/></textarea></td>
						</tr>
						
						<tr><td colspan="2"><xsl:value-of select="$locale/fair/edit/keywords/text()" disable-output-escaping="yes"/>&#160;*</td></tr>
						<tr><td colspan="2"><input type="text" name="object[keywords]" id="keywords" class="middle" style="width:335px" value="{object/keywords}"/></td></tr>
						
						<!--
						<tr>
							<td colspan="2"><xsl:value-of select="$locale/fair/edit/with_processing/text()" disable-output-escaping="yes"/>&#160;&#160;&#160;
								<xsl:choose>
									<xsl:when test="object/with_processing = 1"><input type="radio" name="object[with_processing]" id="with_processing_active" value="1" checked="yes"/></xsl:when>
									<xsl:otherwise><input type="radio" name="object[with_processing]" id="with_processing_active" value="1" /></xsl:otherwise>
								</xsl:choose>
								<label for="with_processing_active" class="lower"><xsl:value-of select="$locale/common/yes/text()"/></label>
								<xsl:choose>
									<xsl:when test="object/with_processing = 0 or object/with_processing = ''"><input type="radio" name="object[with_processing]" id="with_processing_inactive" value="0" checked="yes"/></xsl:when>
									<xsl:otherwise><input type="radio" name="object[with_processing]" id="with_processing_inactive" value="0"/></xsl:otherwise>
								</xsl:choose>
								<label for="with_processing_inactive" class="lower"><xsl:value-of select="$locale/common/no/text()"/></label>
							</td>
						</tr>
						-->
						
						<tr>
							<td colspan="2"><xsl:value-of select="$locale/fair/edit/license/text()" disable-output-escaping="yes"/>&#160;&#160;&#160;
								<xsl:choose>
									<xsl:when test="object/license = 1 or object/license = ''"><input type="radio" name="object[license]" id="license_active" value="1" checked="yes"/></xsl:when>
									<xsl:otherwise><input type="radio" name="object[license]" id="license_active" value="1" /></xsl:otherwise>
								</xsl:choose>
								<label for="license_active" class="lower"><xsl:value-of select="$locale/common/yes/text()"/></label>
								<xsl:choose>
									<xsl:when test="object/license = 0"><input type="radio" name="object[license]" id="license_inactive" value="0" checked="yes"/></xsl:when>
									<xsl:otherwise><input type="radio" name="object[license]" id="license_inactive" value="0"/></xsl:otherwise>
								</xsl:choose>
								<label for="license_inactive" class="lower"><xsl:value-of select="$locale/common/no/text()"/></label>
							</td>
						</tr>
						
						<tr>
							<td colspan="2"><xsl:value-of select="$locale/fair/edit/license_price/text()" disable-output-escaping="yes"/>&#160;&#160;&#160;
							<input type="text" name="object[license_price]" id="license_price" class="small" value="{object/license_price}"/></td>
						</tr>
			
						<tr><td colspan="2">
							<table class="trash_list" width="335px">
								<tr>
									<th><xsl:value-of select="$locale/fair/edit/size/text()" disable-output-escaping="yes"/></th>
									<th><xsl:value-of select="$locale/fair/edit/price/text()" disable-output-escaping="yes"/></th>
								</tr>
								
								<xsl:if test="object/xs !=''">
								<tr>
									<td>XS (<xsl:value-of select="object/xs_w"/>x<xsl:value-of select="object/xs_h"/>px)</td>
									<td align="center"><input type="text" name="object[xs_price]" id="xs_price" class="small" value="{object/xs_price}"/></td>
								</tr>
								</xsl:if>
								
								<xsl:if test="object/s !=''">
								<tr>
									<td>S (<xsl:value-of select="object/s_w"/>x<xsl:value-of select="object/s_h"/>px)</td>
									<td align="center"><input type="text" name="object[s_price]" id="s_price" class="small" value="{object/s_price}"/></td>
								</tr>
								</xsl:if>
								
								<xsl:if test="object/m !=''">
								<tr>
									<td>M (<xsl:value-of select="object/m_w"/>x<xsl:value-of select="object/m_h"/>px)</td>
									<td align="center"><input type="text" name="object[m_price]" id="m_price" class="small" value="{object/m_price}"/></td>
								</tr>
								</xsl:if>
								
								<xsl:if test="object/l !=''">
								<tr>
									<td>L (<xsl:value-of select="object/l_w"/>x<xsl:value-of select="object/l_h"/>px)</td>
									<td align="center"><input type="text" name="object[l_price]" id="l_price" class="small" value="{object/l_price}"/></td>
								</tr>
								</xsl:if>
								
								<xsl:if test="object/xl !=''">
								<tr>
									<td>XL (<xsl:value-of select="object/xl_w"/>x<xsl:value-of select="object/xl_h"/>px)</td>
									<td align="center"><input type="text" name="object[xl_price]" id="xl_price" class="small" value="{object/xl_price}"/></td>
								</tr>
								</xsl:if>
								
								<xsl:if test="object/xxl !=''">
								<tr>
									<td>XXL (<xsl:value-of select="object/xxl_w"/>x<xsl:value-of select="object/xxl_h"/>px)</td>
									<td align="center"><input type="text" name="object[xxl_price]" id="xxl_price" class="small" value="{object/xxl_price}"/></td>
								</tr>
								</xsl:if>
								
							</table>
						</td></tr>
			
						<tr><td colspan="2">
							<input type="submit" value="{$locale/fair/edit/submit/text()}" class="button"/>
						</td></tr>
					</table>
				</td>
			</tr>
			
		</table>
		</form>
		<script>
		var categories = <xsl:value-of select="categories_js"/>;
		function FillCategoriesBox()
		{
			var i=0, category_id1;
			category_id1 = $('category_id1');
			category_id1.options.length = 0;
			for( i=0; i &lt; categories.length; i++ )
			{
				category_id1.options[i] = new Option( categories[i][1], categories[i][0] );
			}
		}
		function FillSubCategoriesBox()
		{
			var parent_id, parent, i=0, category_id1, category_id2, category_id3;
			category_id1 = $('category_id1'); category_id2 = $('category_id2'); category_id3 = $('category_id3');
			category_id2.options.length = 0;
			parent_id = category_id1.options[category_id1.selectedIndex].value;
			parent = FindIndexByValue( categories, parent_id );
			for( i=0; i &lt; categories[parent][2].length; i++ )
			{
				category_id2.options[i] = new Option( categories[parent][2][i][1], categories[parent][2][i][0] );
			}
			FillSubSubCategoriesBox();
		}
		function FillSubSubCategoriesBox()
		{
			var parent_id, parent, parent_id2, parent2, i=0, category_id1, category_id2, category_id3;
			category_id1 = $('category_id1'); category_id2 = $('category_id2'); category_id3 = $('category_id3');
			category_id3.options.length = 0;
			parent_id = category_id1.options[category_id1.selectedIndex].value;
			parent = FindIndexByValue( categories, parent_id );
			parent_id2 = category_id2.options[category_id2.selectedIndex].value;
			parent2 = FindIndexByValue( categories[parent][2], parent_id2 );
			if( categories[parent][2][parent2][2].length > 0 )
			{
				category_id3.style.visibility = '';
				for( i=0; i &lt; categories[parent][2][parent2][2].length; i++ )
				{
					category_id3.options[i] = new Option( categories[parent][2][parent2][2][i][1], categories[parent][2][parent2][2][i][0] );
				}
			}
			else
			{
				category_id3.style.visibility = 'hidden';
			}
		}
		function FindIndexByValue( arr, val )
		{
			var i = 0, retval = 0;
			for( i; i &lt; arr.length; i++ )
			{
				if( arr[i][0] == val )
				{
					retval = i;
					break;
				}
			}
			return( retval );
		}
		FillCategoriesBox();
		FillSubCategoriesBox();
		FillSubSubCategoriesBox();
		<xsl:if test="object/category_id1 > 0">
		var categories_line = '<xsl:value-of select="categories_line"/>';
		var cl_arr = categories_line.split( ',');
		if( cl_arr.length > 0 )
		{
			var i = cl_arr.length - 1;
			var category_id1, category_id2, category_id3;
			category_id1 = $('category_id1'); category_id2 = $('category_id2'); category_id3 = $('category_id3');
			var ob = 1;
			for( i; i>=0; i-- )
			{
				if( ob == 1 )
				{
					select_select( category_id1, cl_arr[i] );
					FillSubCategoriesBox();
				}
				else if( ob == 2 )
				{
					select_select( category_id2, cl_arr[i] );
					FillSubSubCategoriesBox();
				}
				else if( ob == 3 ) select_select( category_id3, cl_arr[i] );
				ob++;
			}
		}
		</xsl:if>
		</script>
	
</xsl:template>

</xsl:stylesheet>

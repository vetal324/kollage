<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" version="1.0">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout.xsl" />
<xsl:include href="../menu.xsl" />
<xsl:include href="../paginator.xsl" />

<xsl:include href="left_panel.xsl" />

<xsl:template match="data">
	<xsl:choose>
		<xsl:when test="/content/global/user/roles/role[code='fair']/@selected = 'yes'">
			<xsl:apply-templates select="fair_objects"/>
		</xsl:when>
		<xsl:otherwise>
			<div class="error"><xsl:value-of select="$locale/common/msgs/access_denied/text()"/></div>
			<script type="text/javascript">RemoveSmallTabs();RemoveLeftPanel();</script>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="fair_objects">
<xsl:variable name="folder" select="@folder"/>
<xsl:variable name="category_id" select="@category_id"/>
<xsl:variable name="rows" select="count(./object)"/>

	<table class="paginator"><tr><th>
		<xsl:call-template name="paginator">
			<xsl:with-param name="prefix">top</xsl:with-param>
		</xsl:call-template>
	</th></tr></table>
	
	<table class="list" id="list">
		<tr>
			<th width="2%" style="text-align:center"><a href="#" style="color:blue" onclick="article_select_all();return(false)">#</a></th>
			<th width="1%"></th>
			<th width="10%"><xsl:value-of select="$locale/fair/list/image/text()"/></th>
			<th width="20%"><xsl:value-of select="$locale/fair/list/title/text()"/></th>
			<th width="*"><xsl:value-of select="$locale/fair/list/description/text()"/></th>
			<th width="20%"><xsl:value-of select="$locale/fair/list/owner/text()"/></th>
			<th width="15%"><xsl:value-of select="$locale/shop/product/list/created_at/text()"/></th>
			<th width="2%" style="text-align:center">&#160;</th>
			<th width="2%" style="text-align:center">&#160;</th>
		</tr>
		
		<xsl:choose>
			<xsl:when test="object">
				<xsl:apply-templates select="object">
					<xsl:with-param name="folder" select="@folder"/>
				</xsl:apply-templates>
			</xsl:when>
			<xsl:otherwise>
				<tr><td colspan="20" align="center"><xsl:value-of select="$locale/fair/list/insert_msg/text()"/></td></tr>
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:variable name="btn_save" select="$locale/shop/product/buttons/save/text()"/>
		<xsl:variable name="btn_delete_marked" select="$locale/shop/product/buttons/delete_marked/text()"/>
		<xsl:variable name="btn_accept_marked" select="$locale/shop/product/buttons/accept_marked/text()"/>
		<xsl:variable name="btn_add_new" select="$locale/shop/product/buttons/add_new/text()"/>
		
		<tr>
			<th class="actions" colspan="20">
				<table align="right"><tr>
					<td><input type="button" value="{$btn_accept_marked}" onclick="article_delete_marked('?{$controller_name}.accept_marked&amp;folder={$folder}&amp;category_id={$category_id}&amp;page={$page}','{$locale/common/msgs/accept_items/text()}')"/></td>
					<td><input type="button" value="{$btn_delete_marked}" onclick="article_delete_marked('?{$controller_name}.del_marked&amp;folder={$folder}&amp;category_id={$category_id}&amp;page={$page}','{$locale/common/msgs/delete_items/text()}')"/></td>
				</tr></table>
			</th>
		</tr>
	</table>
	
	<table class="paginator"><tr><th>
		<xsl:call-template name="paginator">
			<xsl:with-param name="prefix">bottom</xsl:with-param>
		</xsl:call-template>
	</th></tr></table>
	
	<p><xsl:value-of select="$locale/fair/list/help/text()"/></p>
	
	<script language="JavaScript" src="{$views_path}/paginator.js"></script>
	<script language="JavaScript" src="{$views_path}/article.js"></script>
	
	<script type="text/javascript" language="javascript" charset="utf-8" src="{$views_path}/../shared/prototype.js"></script>
	
</xsl:template>

<xsl:template match="object">
  <xsl:param name="folder"/>
  
	<xsl:variable name="id" select="id"/>
	
	<tr id="row{position()}">
		<td class="row"><input type="checkbox" name="del_item{position()}" id="del_item{position()}" value="{$id}"/></td>
		<td>
			<xsl:choose>
				<xsl:when test="status = 1">
					<img src="{$views_path}/images/a.jpg" border="0" width="7" height="7" onclick="document.location='?fair.set_status&amp;page={$page}&amp;id={id}&amp;v=0'" style="cursor:pointer"/>
				</xsl:when>
				<xsl:otherwise>
					<img src="{$views_path}/images/i.jpg" border="0" width="7" height="7" onclick="document.location='?fair.set_status&amp;page={$page}&amp;id={id}&amp;v=1'" style="cursor:pointer"/>
				</xsl:otherwise>
			</xsl:choose>
		</td>
		<td class="row"><a href="#" onclick="PreviewImage(event,'?{$controller_name}.display_photo&amp;id={id}&amp;type=1','{preview_w}','{preview_h}','������� �� �������� ����� �������','�������');return(false)"><img  src="?{$controller_name}.display_photo&amp;id={id}" border="0" width="{thumbnail_w}" height="{thumbnail_h}"/></a></td>
		<td class="row"><xsl:value-of select="title"/></td>
		<td class="row"><xsl:value-of select="description"/></td>
		<td class="row"><xsl:value-of select="client/firstname"/>&#160;<xsl:value-of select="client/lastname"/></td>
		<td class="row"><xsl:value-of select="created_at"/></td>
		
		<td class="row" align="center"><a title="{$locale/common/edit/text()}" href="?fair.edit&amp;id={$id}&amp;folder={$folder}&amp;page={$page}&amp;category_id={category_id1}"><img  src="{$views_path}/images/edit.gif" border="0"/></a></td>
		<td class="row" align="center"><a title="{$locale/common/del/text()}" href="#" onclick="article_delete('?fair.del&amp;id={id}&amp;category_id={category_id1}&amp;folder={$folder}&amp;page={$page}','{$locale/common/msgs/delete_item/text()}')"><img  src="{$views_path}/images/del.gif" border="0"/></a></td>
	</tr>
</xsl:template>

</xsl:stylesheet>

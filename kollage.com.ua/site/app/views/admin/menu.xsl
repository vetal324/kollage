<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" version="1.0">
                
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:template match="bigtabs">
    <div class="bigtabs">
        <ul><xsl:apply-templates/></ul>
    </div>
</xsl:template>

<xsl:template match="bigtabs/item">
        <xsl:choose>
            <xsl:when test="@chosen='true'">
                <xsl:choose>
                    <xsl:when test="@enabled='true'"><li class="current"><a href="{@url}"><span><xsl:value-of select="@title"/></span></a></li></xsl:when>
                    <xsl:otherwise><li><span class="disabled"><xsl:value-of select="@title"/></span></li></xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
                <li><a href="{@url}"><span><xsl:value-of select="@title"/></span></a></li>
            </xsl:otherwise>
        </xsl:choose>
</xsl:template>

<xsl:template match="smalltabs">
    <table cellspacing="0" cellpadding="0"><tr>
    <xsl:apply-templates/>
    <td>&#160;</td>
    </tr></table>
</xsl:template>

<xsl:template match="smalltabs/item">
    <xsl:variable name="sufix">
        <xsl:choose>
            <xsl:when test="@chosen='true'">active</xsl:when>
            <xsl:otherwise>usual</xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    
    <td>
        <table border="0" cellspacing="0" cellpadding="0"><tr>
            <td><img src="{$views_path}/images/smalltabs/smalltab_side.gif"/></td>
            <td align="center" background="{$views_path}/images/smalltabs/smalltab_body_{$sufix}.gif">
            <xsl:text>&#160;</xsl:text>
            <xsl:choose>
                <xsl:when test="@enabled='true'"><a href="{@url}"><xsl:value-of select="@title"/></a></xsl:when>
                <xsl:otherwise><span class="disabled"><xsl:value-of select="@title"/></span></xsl:otherwise>
            </xsl:choose>
            <xsl:text>&#160;</xsl:text>
            </td>
            <td><img src="{$views_path}/images/smalltabs/smalltab_side.gif"/></td>
        </tr></table>
    </td>
    <td><img src="{$views_path}/images/smalltabs/smalltab_delim.gif"/></td>
</xsl:template>

<xsl:template match="menuleft">
    <table width="100%" cellspacing="0" cellpadding="0">
    <xsl:apply-templates/>
    <tr><td><img src="{$views_path}/images/menuleft/menu_left_delim.gif"/></td></tr>
    </table>
</xsl:template>

<xsl:template match="menuleft/separator">
    <tr><td><img src="{$views_path}/images/menuleft/menu_left_delim.gif"/></td></tr>
</xsl:template>

<xsl:template match="menuleft/item">
    <xsl:variable name="sufix">
        <xsl:choose>
            <xsl:when test="@chosen='true'">active</xsl:when>
            <xsl:otherwise>usual</xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    
    <tr><td>
        <table border="0" cellspacing="0" cellpadding="0">
            <tr><td><img src="{$views_path}/images/menuleft/menu_left_delim_top.gif"/></td></tr>
            <tr><td height="24" align="left" onclick="document.location='{@url}'" style="cursor:pointer" background="{$views_path}/images/menuleft/menu_left_body_{$sufix}.gif">
            <xsl:text>&#160;</xsl:text>
            <xsl:choose>
                <xsl:when test="@enabled='true'"><a href="{@url}"><span class="menuleft_{$sufix}"><xsl:value-of select="@title" disable-output-escaping="yes"/></span></a></xsl:when>
                <xsl:otherwise><span class="disabled"><xsl:value-of select="@title" disable-output-escaping="yes"/></span></xsl:otherwise>
            </xsl:choose>
            <xsl:text>&#160;</xsl:text>
            </td></tr>
            <tr><td><img src="{$views_path}/images/menuleft/menu_left_delim_bottom.gif"/></td></tr>
        </table>
    </td></tr>
</xsl:template>

<xsl:template match="menuleft/*[starts-with(local-name(),'submenu')]">
    <xsl:variable name="sufix">
        <xsl:choose>
            <xsl:when test="@chosen='true'">active</xsl:when>
            <xsl:otherwise>usual</xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    
    <tr><td>
        <table border="0" cellspacing="0" cellpadding="0">
            <tr><td><img src="{$views_path}/images/menuleft/menu_left_delim_top.gif"/></td></tr>
            <tr><td height="24" align="left" onclick="document.location='{@url}'" style="cursor:pointer" background="{$views_path}/images/menuleft/menu_left_body_{$sufix}.gif">
            <xsl:text>&#160;</xsl:text>
            <xsl:choose>
                <xsl:when test="@enabled='true'"><a href="{@url}"><span class="menuleft_{$sufix}"><xsl:value-of select="@title" disable-output-escaping="yes"/></span></a></xsl:when>
                <xsl:otherwise><span class="disabled"><xsl:value-of select="@title" disable-output-escaping="yes"/></span></xsl:otherwise>
            </xsl:choose>
            <xsl:text>&#160;</xsl:text>
            </td></tr>
            <tr><td><img src="{$views_path}/images/menuleft/menu_left_delim_bottom.gif"/></td></tr>
        </table>
    </td></tr>
    
    <xsl:if test="@chosen='true' or ./*[@chosen='true'] or ./*/*[@chosen='true']">
        <xsl:apply-templates/>
    </xsl:if>
</xsl:template>

<xsl:template match="menuleft/*[starts-with(local-name(),'submenu')]/*[starts-with(local-name(),'item')]">
    <xsl:variable name="sufix">
        <xsl:choose>
            <xsl:when test="@chosen='true'">active</xsl:when>
            <xsl:otherwise>usual</xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    
    <tr><td>
        <table border="0" cellspacing="0" cellpadding="0">
            <tr><td><img src="{$views_path}/images/menuleft/menu_left_delim_top.gif"/></td></tr>
            <tr><td height="18" align="left" onclick="document.location='{@url}'" style="cursor:pointer" background="{$views_path}/images/menuleft/menu_left_body_{$sufix}1.gif">
            <xsl:text>&#160;&#160;&#160;</xsl:text>
            <xsl:choose>
                <xsl:when test="@enabled='true'"><a href="{@url}"><xsl:value-of select="@title" disable-output-escaping="yes"/></a></xsl:when>
                <xsl:otherwise><span class="disabled"><xsl:value-of select="@title" disable-output-escaping="yes"/></span></xsl:otherwise>
            </xsl:choose>
            <xsl:text>&#160;</xsl:text>
            </td></tr>
            <tr><td><img src="{$views_path}/images/menuleft/menu_left_delim_bottom.gif"/></td></tr>
        </table>
    </td></tr>
</xsl:template>

<xsl:template match="menuleft/*[starts-with(local-name(),'submenu')]/*[starts-with(local-name(),'submenu')]/item">
    <xsl:variable name="sufix">
        <xsl:choose>
            <xsl:when test="@chosen='true'">active</xsl:when>
            <xsl:otherwise>usual</xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    
    <tr><td>
        <table border="0" cellspacing="0" cellpadding="0">
            <tr><td><img src="{$views_path}/images/menuleft/menu_left_delim_top.gif"/></td></tr>
            <tr><td height="18" align="left" onclick="document.location='{@url}'" style="cursor:pointer" background="{$views_path}/images/menuleft/menu_left_body_{$sufix}1.gif">
            <xsl:text>&#160;&#160;&#160;&#160;&#160;&#160;</xsl:text>
            <xsl:choose>
                <xsl:when test="@enabled='true'"><a href="{@url}"><xsl:value-of select="@title" disable-output-escaping="yes"/></a></xsl:when>
                <xsl:otherwise><span class="disabled"><xsl:value-of select="@title" disable-output-escaping="yes"/></span></xsl:otherwise>
            </xsl:choose>
            <xsl:text>&#160;</xsl:text>
            </td></tr>
            <tr><td><img src="{$views_path}/images/menuleft/menu_left_delim_bottom.gif"/></td></tr>
        </table>
    </td></tr>
</xsl:template>

<xsl:template match="menuleft/*[starts-with(local-name(),'submenu')]/*[starts-with(local-name(),'submenu')]">
    <xsl:variable name="sufix">
        <xsl:choose>
            <xsl:when test="@chosen='true'">active</xsl:when>
            <xsl:otherwise>usual</xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    
    <tr><td>
        <table border="0" cellspacing="0" cellpadding="0">
            <tr><td><img src="{$views_path}/images/menuleft/menu_left_delim_top.gif"/></td></tr>
            <tr><td height="18" align="left" onclick="document.location='{@url}'" style="cursor:pointer" background="{$views_path}/images/menuleft/menu_left_body_{$sufix}1.gif">
            <xsl:text>&#160;&#160;&#160;</xsl:text>
            <xsl:choose>
                <xsl:when test="@enabled='true'"><a href="{@url}"><xsl:value-of select="@title" disable-output-escaping="yes"/></a></xsl:when>
                <xsl:otherwise><span class="disabled"><xsl:value-of select="@title" disable-output-escaping="yes"/></span></xsl:otherwise>
            </xsl:choose>
            <xsl:text>&#160;</xsl:text>
            </td></tr>
            <tr><td><img src="{$views_path}/images/menuleft/menu_left_delim_bottom.gif"/></td></tr>
        </table>
    </td></tr>
    
    <xsl:if test="@chosen='true' or ./*[@chosen='true']">
        <xsl:apply-templates/>
    </xsl:if>
</xsl:template>

<xsl:template match="menuleft/*[starts-with(local-name(),'submenu')]/*[starts-with(local-name(),'submenu')]/*[starts-with(local-name(),'submenu')]">
    <xsl:variable name="sufix">
        <xsl:choose>
            <xsl:when test="@chosen='true'">active</xsl:when>
            <xsl:otherwise>usual</xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    
    <tr><td>
        <table border="0" cellspacing="0" cellpadding="0">
            <tr><td><img src="{$views_path}/images/menuleft/menu_left_delim_top.gif"/></td></tr>
            <tr><td height="18" align="left" onclick="document.location='{@url}'" style="cursor:pointer" background="{$views_path}/images/menuleft/menu_left_body_{$sufix}1.gif">
            <xsl:text>&#160;&#160;&#160;&#160;&#160;</xsl:text>
            <xsl:choose>
                <xsl:when test="@enabled='true'"><a href="{@url}"><xsl:value-of select="@title" disable-output-escaping="yes"/></a></xsl:when>
                <xsl:otherwise><span class="disabled"><xsl:value-of select="@title" disable-output-escaping="yes"/></span></xsl:otherwise>
            </xsl:choose>
            <xsl:text>&#160;</xsl:text>
            </td></tr>
            <tr><td><img src="{$views_path}/images/menuleft/menu_left_delim_bottom.gif"/></td></tr>
        </table>
    </td></tr>
    
    <xsl:if test="@chosen='true' or ./*[@chosen='true']">
        <xsl:apply-templates/>
    </xsl:if>
</xsl:template>

</xsl:stylesheet>

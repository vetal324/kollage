
function set_rows(prefix)
{
    obj = document.getElementById('rows'+prefix);
    url2go = add_to_url( document.location, "rows_per_page=" + obj.options[obj.selectedIndex].text );
    document.location = url2go;
}

function goto_page(prefix)
{
    obj = document.getElementById('page'+prefix);
    url2go = add_to_url( document.location, "page=" + obj.value );
    document.location = url2go;
}

function goto_prev(prefix,page)
{
    url2go = add_to_url( document.location, "page=" + page );
    document.location = url2go;
}

function goto_next(prefix,page)
{
    url2go = add_to_url( document.location, "page=" + page );
    document.location = url2go;
}

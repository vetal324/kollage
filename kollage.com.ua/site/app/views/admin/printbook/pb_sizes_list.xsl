<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" version="1.0">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout.xsl" />
<xsl:include href="../menu.xsl" />
<xsl:include href="../paginator.xsl" />

<xsl:template match="data">
	<xsl:choose>
		<xsl:when test="/content/global/user/roles/role[code='sop']/@selected = 'yes'">
			<xsl:apply-templates select="list"/>
		</xsl:when>
		<xsl:otherwise>
			<div class="error"><xsl:value-of select="$locale/common/msgs/access_denied/text()"/></div>
			<script type="text/javascript">RemoveSmallTabs();RemoveLeftPanel();</script>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="list">
	<xsl:variable name="rows" select="count(./pb_size)"/>

	<table class="paginator"><tr><th>
		<xsl:call-template name="paginator">
			<xsl:with-param name="prefix">top</xsl:with-param>
		</xsl:call-template>
	</th></tr></table>
	
	<table class="list" id="list">
		<tr>
			<th width="2%">#</th>
			<th width="*"><xsl:value-of select="$locale/printbook/list/name/text()"/></th>
			<th width="7%"><xsl:value-of select="$locale/printbook/list/price_base/text()"/></th>
			<th width="10%"><xsl:value-of select="$locale/printbook/list/price_per_page/text()"/></th>
			<th width="10%"><xsl:value-of select="$locale/printbook/list/start_page/text()"/></th>
			<th width="10%"><xsl:value-of select="$locale/printbook/list/page_block/text()"/></th>
			<th width="20%">�������� � ��������</th>
			<th width="2%" style="text-align:center">&#160;</th>
			<th width="2%" style="text-align:center">&#160;</th>
		</tr>
		
		<xsl:choose>
			<xsl:when test="pb_size">
				<xsl:apply-templates select="pb_size">
				</xsl:apply-templates>
			</xsl:when>
			<xsl:otherwise>
				<tr><td colspan="20" align="center"><xsl:value-of select="$locale/printbook/list/insert_msg/text()"/></td></tr>
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:variable name="btn_save" select="$locale/printbook/buttons/save/text()"/>
		<xsl:variable name="btn_delete_marked" select="$locale/printbook/buttons/delete_marked/text()"/>
		<xsl:variable name="btn_add_new" select="$locale/printbook/buttons/add_new/text()"/>
		
		<tr>
			<th class="actions" colspan="20">
				<table align="right"><tr>
					<td><input type="button" value="{$btn_delete_marked}" onclick="article_delete_marked('?{$controller_name}.pb_sizes_del_marked','{$locale/common/msgs/delete_items/text()}')"/></td>
					<td><input type="button" value="{$btn_add_new}" onclick="document.location='?{$controller_name}.pb_size_edit&amp;last_position={last_position}'"/></td>
				</tr></table>
			</th>
		</tr>
	</table>
	
	<table class="paginator"><tr><th>
		<xsl:call-template name="paginator">
			<xsl:with-param name="prefix">bottom</xsl:with-param>
		</xsl:call-template>
	</th></tr></table>
	
	<p><xsl:value-of select="$locale/printbook/list/help/text()" disable-output-escaping="yes"/></p>
	
	<xsl:if test="../parameters = 'scroll_to_list'">
		<script>ScrollToObject('list')</script>
	</xsl:if>
	
	<script language="JavaScript" src="{$views_path}/paginator.js"></script>
	<script language="JavaScript" src="{$views_path}/article.js"></script>
	
	<script type="text/javascript" language="javascript" charset="utf-8" src="{$views_path}/../shared/prototype.js"></script>
	<script type="text/javascript" language="javascript" charset="utf-8" src="{$views_path}/../shared/scriptaculos/scriptaculous.js"></script>
	<script type="text/javascript" language="javascript" charset="utf-8" src="{$views_path}/../shared/scriptaculos/effects.js"></script>
	<script type="text/javascript" language="javascript" charset="utf-8" src="{$views_path}/../shared/scriptaculos/dragdrop.js"></script>
	<script type="text/javascript" language="javascript" charset="utf-8" src="{$views_path}/dnd.js"></script>
	<script type="text/javascript" language="javascript">
		DefineDnDPosition( <xsl:value-of select="$rows"/> );

		function ChangePosition( src, dst )
		{
			src_id = $F( 'cid'+src ); dst_id = $F( 'cid'+dst );
			document.location = '?<xsl:value-of select="$controller_name"/>.pb_sizes_swap_position&amp;src='+ src_id +'&amp;dst='+ dst_id +'&amp;page=<xsl:value-of select="$page"/>&amp;pages=<xsl:value-of select="$pages"/>';
		}
	</script>
	
</xsl:template>

<xsl:template match="pb_size">
	<xsl:variable name="id" select="id"/>
	<tr id="row{position()}">
		<td class="row"><input type="checkbox" name="del_item{position()}" id="del_item{position()}" value="{$id}"/></td>
		<td class="row"><div class="cell" id="c{position()}"><xsl:value-of select="name"/><input type="hidden" name="cid{position()}" id="cid{position()}" value="{id}"/></div></td>
		
		<td class="row"><xsl:value-of select="price_base"/></td>
		<td class="row"><xsl:value-of select="price_per_page"/></td>
		<td class="row"><xsl:value-of select="start_page"/></td>
		<td class="row"><xsl:value-of select="page_block"/></td>
		
		<td class="row">
			<select onchange="document.location=this.value">
				<option>- �������� -</option>
				<option value="?{$controller_name}.pb_laminations_list&amp;pb_size_id={$id}">�������������</option>
				<option value="?{$controller_name}.pb_papers_list&amp;pb_size_id={$id}" onclick="document.location=this.value">������</option>
				<option value="?{$controller_name}.cover_categories_list&amp;pb_size_id={$id}" onclick="document.location=this.value">�������</option>
				<option value="?{$controller_name}.box_categories_list&amp;pb_size_id={$id}" onclick="document.location=this.value">������� � �����</option>
				<option value="?{$controller_name}.design_work_categories_list&amp;pb_size_id={$id}" onclick="document.location=this.value">�������������� �������</option>
				<option value="?{$controller_name}.pb_paper_bases_list&amp;pb_size_id={$id}" onclick="document.location=this.value">������ �����</option>
                
                <xsl:if test="type=2">
                    <option value="?{$controller_name}.pb_forzaces_list&amp;pb_size_id={$id}" onclick="document.location=this.value">�������</option>
                </xsl:if>
                
			</select>
		</td>
		<td class="row" align="center"><a title="{$locale/common/edit/text()}" href="?{$controller_name}.pb_size_edit&amp;id={$id}"><img  src="{$views_path}/images/edit.gif" border="0"/></a></td>
		<td class="row" align="center"><a title="{$locale/common/del/text()}" href="#" onclick="article_delete('?{$controller_name}.pb_size_del&amp;id={id}','{$locale/common/msgs/delete_item/text()}')"><img  src="{$views_path}/images/del.gif" border="0"/></a></td>
	</tr>
</xsl:template>

</xsl:stylesheet>

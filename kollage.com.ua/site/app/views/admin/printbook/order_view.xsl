<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:msxsl="urn:schemas-microsoft-com:xslt"
				xmlns:exsl="http://exslt.org/common"
				extension-element-prefixes="exsl"
				version="1.0">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:include href="left_panel.xsl" />

<xsl:template match="data">
	<xsl:choose>
		<xsl:when test="/content/global/user/roles/role[code='sop']/@selected = 'yes'">
			<xsl:apply-templates/>
		</xsl:when>
		<xsl:otherwise>
			<div class="error"><xsl:value-of select="$locale/common/msgs/access_denied/text()"/></div>
			<script type="text/javascript">RemoveSmallTabs();RemoveLeftPanel();</script>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="order">

<table class="form" cellspacing="0" cellpadding="0">
	<tr>
		<th>
			<xsl:value-of select="$locale/sop/orders/view/title/text()"/>
			<button id="print-report" style="margin-left: 10px"><xsl:value-of select="$locale/printbook/orders/report_button/text()" /></button>
		</th>
	</tr>
	
	<tr><td>
		<table width="100%" class="report">
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/sop/orders/view/number/text()"/></b></td>
				<td class="row"><xsl:value-of select="order_number"/></td>
			</tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/sop/orders/view/created_at/text()"/></b></td>
				<td class="row"><xsl:value-of select="created_at"/></td>
			</tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/sop/orders/view/updated_at/text()"/></b></td>
				<td class="row"><xsl:value-of select="updated_at"/></td>
			</tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/sop/orders/view/paymentway_name/text()"/></b></td>
				<td class="row"><xsl:value-of select="paymentway/name"/></td>
			</tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/sop/orders/view/delivery/text()"/></b></td>
				<td class="row"><xsl:value-of select="delivery/name"/></td>
			</tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/sop/orders/view/status/text()"/></b></td>
				<td class="row">
					<xsl:choose>
						<xsl:when test="status = 1">
							<xsl:value-of select="$locale/sop/orders/statuses/s1/text()"/>
						</xsl:when>
						<xsl:when test="status = 2">
							<xsl:value-of select="$locale/sop/orders/statuses/s2/text()"/>
						</xsl:when>
						<xsl:when test="status = 3">
							<xsl:value-of select="$locale/sop/orders/statuses/s3/text()"/>
						</xsl:when>
						<xsl:when test="status = 4">
							<xsl:value-of select="$locale/sop/orders/statuses/s4/text()"/>
						</xsl:when>
						<xsl:when test="status = 5">
							<xsl:value-of select="$locale/sop/orders/statuses/s5/text()"/>
						</xsl:when>
					</xsl:choose>
				</td>
			</tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/sop/orders/list/comments/text()"/></b></td>
				<td class="row"><xsl:value-of select="comments"/>&#160;</td>
			</tr>
			
			<tr><td class="row" colspan="2">&#160;</td></tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/printbook/orders/format"/></b></td>
				<td class="row"><xsl:value-of select="pb_size/name"/></td>
			</tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/printbook/orders/pages"/></b></td>
				<td class="row"><xsl:value-of select="pages"/></td>
			</tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/printbook/orders/paper"/></b></td>
				<td class="row"><xsl:value-of select="pb_paper/name"/></td>
			</tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/printbook/orders/lamination"/></b></td>
				<td class="row"><xsl:value-of select="pb_lamination/name"/></td>
			</tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/printbook/orders/forzaces"/></b></td>
				<td class="row">
                    <xsl:choose>
                        <xsl:when test="pb_size/type = 1">
                            <xsl:choose>
                                <xsl:when test="forzaces = 1">
                                    <xsl:value-of select="$locale/printbook/orders/forzaces_yes"/>
                                </xsl:when>
                                <xsl:when test="forzaces = 2">
                                    <xsl:value-of select="$locale/printbook/orders/forzaces_no"/>
                                </xsl:when>
                            </xsl:choose>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="pb_forzace/name"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </td>
			</tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/printbook/orders/paper_base"/></b></td>
				<td class="row"><xsl:value-of select="pb_paper_base/name"/></td>
			</tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/printbook/orders/personal_pages"/></b></td>
				<td class="row">
					<xsl:choose>
						<xsl:when test="personal_pages = 1">
							<xsl:value-of select="$locale/common/yes"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$locale/common/no"/>
						</xsl:otherwise>
					</xsl:choose>
                </td>
			</tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/printbook/orders/personal_cover"/></b></td>
				<td class="row">
					<xsl:choose>
						<xsl:when test="personal_cover_id > 1">
							<xsl:value-of select="$locale/common/yes"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$locale/common/no"/>
						</xsl:otherwise>
					</xsl:choose>
								</td>
			</tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/printbook/orders/cover"/></b></td>
				<td class="row"><xsl:value-of select="pb_cover/name"/>&#160;</td>
			</tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/printbook/orders/box"/></b></td>
				<td class="row"><xsl:value-of select="pb_box/name"/>&#160;</td>
			</tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/printbook/orders/design_work"/></b></td>
				<td class="row"><xsl:value-of select="pb_design_work/name"/>&#160;</td>
			</tr>
			
			<tr><td class="row" colspan="2">&#160;</td></tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/sop/orders/view/client/text()"/></b></td>
				<td class="row"><xsl:value-of select="client/firstname"/>&#160;<xsl:value-of select="client/lastname"/></td>
			</tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/sop/orders/view/client_type/text()"/></b></td>
				<td class="row"><xsl:value-of select="client/type"/></td>
			</tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/sop/orders/view/email/text()"/></b></td>
				<td class="row"><a href="mailto:{client/email}"><xsl:value-of select="client/email"/></a></td>
			</tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/sop/orders/view/address/text()"/></b></td>
				<td class="row"><xsl:value-of select="client/city"/>, <xsl:value-of select="client/street/name"/>, <xsl:value-of select="client/address"/></td>
			</tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/sop/orders/view/address2/text()"/></b></td>
				<td class="row"><xsl:if test="address = ''">&#160;</xsl:if><xsl:value-of select="address"/></td>
			</tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/sop/orders/view/phone/text()"/></b></td>
				<td class="row"><xsl:value-of select="client/phone"/></td>
			</tr>
			
			<tr><td class="row" colspan="2">&#160;</td></tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/printbook/orders/quantity"/></b></td>
				<td class="row"><xsl:value-of select="quantity"/></td>
			</tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/printbook/orders/price_one"/></b></td>
				<td class="row"><xsl:value-of select="calculated_price_one"/></td>
			</tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/printbook/orders/price_design"/></b></td>
				<td class="row"><xsl:value-of select="calculated_price_design_work"/></td>
			</tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/sop/orders/view/discount_price/text()"/></b></td>
				<td class="row"><xsl:value-of select="discount_price"/></td>
			</tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/sop/orders/view/delivery_price/text()"/></b></td>
				<td class="row"><xsl:value-of select="delivery_price"/></td>
			</tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/sop/orders/view/total_price/text()"/></b></td>
				<td class="row"><xsl:value-of select="total_price"/></td>
			</tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/sop/orders/view/payed_price/text()"/></b></td>
				<td class="row"><xsl:value-of select="payed"/></td>
			</tr>
			
		</table>
	</td></tr>
	
	<tr><td>&#160;</td></tr>
	
	<tr><th><xsl:value-of select="$locale/sop/orders/view/title2/text()"/></th></tr>
	<tr><td>
	
	<xsl:call-template name="tiny_mce"/>

	<form name="send" id="send" action="?{$controller_name}.order_send" method="post">
		<input type="hidden" name="id" value="{id}"/>
		
		<table class="form" cellspacing="0" cellpadding="0">
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/sop/orders/view/subject/text()"/></b></td>
				<td class="row"><input type="text" class="large" name="subject" value="N{order_number}"/></td>
			</tr>
		
			<tr>
				<td class="row" colspan="2"><b><xsl:value-of select="$locale/sop/orders/view/body/text()"/></b></td>
			</tr>
			<tr>
				<td class="row" colspan="2"><textarea name="body" class="tinymce large" rows="10"></textarea></td>
			</tr>
			
			<tr><th colspan="2" class="actions"><input type="submit" value="{$locale/common/buttons/form_send/text()}"/></th></tr>
		</table>
	</form>
	
	</td></tr>
</table>

<script>
(function($){
	$('#print-report').click(function(){
		var report = $('.report').clone().get().first();
		var style = $('<style>table tr:nth-child(2n+1) {background-color: #f0f0f0}</style>').get().first();
		var w = window.open('', 'Report');
		var d = w.document;
		d.body.append(report);
		d.body.append(style)
		w.print();
	});
})(jQuery);
</script>

</xsl:template>

</xsl:stylesheet>

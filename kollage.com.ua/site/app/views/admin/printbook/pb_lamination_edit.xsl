<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:msxsl="urn:schemas-microsoft-com:xslt"
				xmlns:exsl="http://exslt.org/common"
				extension-element-prefixes="exsl"
				version="1.0">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:template match="data">
	<xsl:choose>
		<xsl:when test="/content/global/user/roles/role[code='sop']/@selected = 'yes'">
			<xsl:apply-templates/>
		</xsl:when>
		<xsl:otherwise>
			<div class="error"><xsl:value-of select="$locale/common/msgs/access_denied/text()"/></div>
			<script type="text/javascript">RemoveSmallTabs();RemoveLeftPanel();</script>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="edit">

	<xsl:call-template name="tiny_mce"/>
	
	<a href="?{$controller_name}.pb_laminations_list&amp;pb_size_id={pb_size/id}"><h2><xsl:value-of select="pb_size/name"/>: �������������</h2></a>

	<xsl:variable name="title">
		<xsl:choose>
			<xsl:when test="pb_lamination/id &gt; 0"><xsl:value-of select="$locale/printbook/edit/title1/text()"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="$locale/printbook/edit/title2/text()"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	
	<xsl:variable name="id" select="pb_lamination/id"/>
	<xsl:variable name="position"><xsl:value-of select="pb_lamination/position"/></xsl:variable>

	<form name="edit" id="edit" action="?{$controller_name}.pb_lamination_save" method="post">
		<input type="hidden" name="id" value="{$id}"/>
		<input type="hidden" name="last_position" value="{last_position}"/>
		<input type="hidden" name="pb_size_id" value="{pb_size/id}"/>
		<input type="hidden" name="object[id]" value="{$id}"/>
		<input type="hidden" name="object[pb_size_id]" value="{pb_size/id}"/>
		<input type="hidden" name="object[lang]" value="{$lang}"/>
		<input type="hidden" name="object[position]" value="{$position}"/>
	<table class="form" cellspacing="0" cellpadding="0">
	
		<!-- Show message -->
		<xsl:if test="save_result = 'true'">
		<tr><td colspan="2"><div class="message"><xsl:value-of select="$locale/printbook/edit/save_msg_success/text()"/></div></td></tr>
		</xsl:if>
		<xsl:if test="save_result = 'false'">
		<tr><td colspan="2"><div class="error"><xsl:value-of select="$locale/printbook/edit/save_msg_error/text()"/></div></td></tr>
		</xsl:if>
		<!-- /Show message -->
		
		<tr><th colspan="2"><xsl:value-of select="$title"/></th></tr>
		
		<tr>
			<td class="left_label"><label for="name"><xsl:value-of select="$locale/printbook/edit/name/text()"/></label></td>
			<td><input type="text" class="large" name="object[name]" id="name" value="{pb_lamination/name}"/></td>
		</tr>
		
		<tr>
			<td class="left_label">
				<label for="price" class="when_type1 displayinline"><xsl:value-of select="$locale/printbook/edit/price1/text()"/></label>
				<label for="price" class="when_type2 dontdisplay"><xsl:value-of select="$locale/printbook/edit/price2/text()"/></label>
			</td>
			<td><input type="text" class="medium" name="object[price]" id="price" value="{pb_lamination/price}"/></td>
		</tr>
		
		<tr><th colspan="2" class="actions"><input type="submit" value="{$locale/common/buttons/form_save/text()}"/></th></tr>
	</table>
		
	</form>
	
	<script language="JavaScript" src="{$views_path}/article.js"></script>
	<script type="text/javascript">
	var apb = {
		options: {
			'type': '<xsl:value-of select="pb_size/type"/>'
		},
		
		init: function() {
		},
		
		onChangeType: function() {
			var self = this; var o = this.options;
			
			var type = o.type;
			 
			if( type == 2 ) {
				$$('.when_type1').each( function(el) {
					el.removeClassName('displayinline');
					el.addClassName('dontdisplay');
				} );
				$$('.when_type2').each( function(el) {
					el.removeClassName('dontdisplay');
					el.addClassName('displayinline');
				} );
			}
			else {
				$$('.when_type1').each( function(el) {
					el.removeClassName('dontdisplay');
					el.addClassName('displayinline');
				} );
				$$('.when_type2').each( function(el) {
					el.removeClassName('displayinline');
					el.addClassName('dontdisplay');
				} );
			}
			
		}
	};
	apb.onChangeType();
	</script>
	
</xsl:template>

</xsl:stylesheet>

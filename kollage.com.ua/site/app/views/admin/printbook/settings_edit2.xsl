<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:msxsl="urn:schemas-microsoft-com:xslt"
				xmlns:exsl="http://exslt.org/common"
				extension-element-prefixes="exsl"
				version="1.0">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:template match="data">
	<xsl:choose>
		<xsl:when test="/content/global/user/roles/role[code='sop']/@selected = 'yes'">
			<xsl:apply-templates/>
		</xsl:when>
		<xsl:otherwise>
			<div class="error"><xsl:value-of select="$locale/common/msgs/access_denied/text()"/></div>
			<script type="text/javascript">RemoveSmallTabs();RemoveLeftPanel();</script>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="edit">

	<xsl:call-template name="tiny_mce"/>

	<xsl:variable name="title">
		<xsl:choose>
			<xsl:when test="settings/id &gt; 0"><xsl:value-of select="$locale/sop/settings/edit/title1/text()"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="$locale/sop/settings/edit/title2/text()"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<form name="settings_edit" id="settings_edit" action="?{$controller_name}.settings_save2" method="post">
		<input type="hidden" name="id" value="1"/>
		<input type="hidden" name="object[id]" value="1"/>
		<input type="hidden" name="object[lang]" value="{$lang}"/>
	<table class="form" cellspacing="0" cellpadding="0">
	
		<!-- Show message -->
		<xsl:if test="save_result = 'true'">
		<tr><td colspan="2"><div class="message"><xsl:value-of select="$locale/sop/settings/edit/save_msg_success/text()"/></div></td></tr>
		</xsl:if>
		<xsl:if test="save_result = 'false'">
		<tr><td colspan="2"><div class="error"><xsl:value-of select="$locale/sop/settings/edit/save_msg_error/text()"/></div></td></tr>
		</xsl:if>
		<!-- /Show message -->
		
		<tr><th colspan="2"><xsl:value-of select="$title"/></th></tr>
		
		<tr>
			<td class="left_label"><label for="dont_use_discount"><xsl:value-of select="$locale/sop/settings/edit/dont_use_discount/text()"/></label></td>
			<td>
				<xsl:choose>
					<xsl:when test="settings/dont_use_discount = 1"><input type="radio" name="object[dont_use_discount]" id="status_active" value="1" checked="yes" /></xsl:when>
					<xsl:otherwise><input type="radio" name="object[dont_use_discount]" id="status_active" value="1" /></xsl:otherwise>
				</xsl:choose>
				<label for="status_active" class="lower"><xsl:value-of select="$locale/common/yes"/></label>&#160;&#160;&#160;
				<xsl:choose>
					<xsl:when test="settings/dont_use_discount = 0"><input type="radio" name="object[dont_use_discount]" id="status_inactive" value="0" checked="yes"/></xsl:when>
					<xsl:otherwise><input type="radio" name="object[dont_use_discount]" id="status_inactive" value="0"/></xsl:otherwise>
				</xsl:choose>
				<label for="status_inactive" class="lower"><xsl:value-of select="$locale/common/no"/></label>
			</td>
		</tr>
		
		<tr><th colspan="2" class="actions"><input type="submit" value="{$locale/common/buttons/form_save/text()}"/></th></tr>
	</table>
		
	</form>
	
	<!--
	<form name="settings_sql" id="settings_sql" action="?{$controller_name}.settings_sql" method="post">
	<table class="form" cellspacing="0" cellpadding="0">
		<tr>
			<td class="left_label">sql</td>
			<td><input type="text" class="large" name="sql" id="sql" value=""/></td>
		</tr>
		
		<tr><th colspan="2" class="actions"><input type="submit"/></th></tr>
	</table>
	</form>
	
	<div><xsl:value-of select="save_result"/></div>
	-->
	
	<script language="JavaScript" src="{$views_path}/article.js"></script>
	
</xsl:template>

</xsl:stylesheet>

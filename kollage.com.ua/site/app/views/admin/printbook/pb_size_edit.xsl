<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:msxsl="urn:schemas-microsoft-com:xslt"
				xmlns:exsl="http://exslt.org/common"
				extension-element-prefixes="exsl"
				version="1.0">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:template match="data">
	<xsl:choose>
		<xsl:when test="/content/global/user/roles/role[code='sop']/@selected = 'yes'">
			<xsl:apply-templates/>
		</xsl:when>
		<xsl:otherwise>
			<div class="error"><xsl:value-of select="$locale/common/msgs/access_denied/text()"/></div>
			<script type="text/javascript">RemoveSmallTabs();RemoveLeftPanel();</script>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="edit">

	<xsl:call-template name="tiny_mce"/>

	<xsl:variable name="title">
		<xsl:choose>
			<xsl:when test="pb_size/id &gt; 0"><xsl:value-of select="$locale/printbook/edit/title1/text()"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="$locale/printbook/edit/title2/text()"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	
	<xsl:variable name="id" select="pb_size/id"/>
	<xsl:variable name="position"><xsl:value-of select="pb_size/position"/></xsl:variable>

	<form name="paymentway_edit" id="paymentway_edit" action="?{$controller_name}.pb_size_save" method="post" enctype="multipart/form-data">
		<input type="hidden" name="id" value="{$id}"/>
		<input type="hidden" name="last_position" value="{last_position}"/>
		<input type="hidden" name="object[id]" value="{$id}"/>
		<input type="hidden" name="object[lang]" value="{$lang}"/>
		<input type="hidden" name="object[position]" value="{$position}"/>
	<table class="form" cellspacing="0" cellpadding="0">
	
		<!-- Show message -->
		<xsl:if test="save_result = 'true'">
		<tr><td colspan="2"><div class="message"><xsl:value-of select="$locale/printbook/edit/save_msg_success/text()"/></div></td></tr>
		</xsl:if>
		<xsl:if test="save_result = 'false'">
		<tr><td colspan="2"><div class="error"><xsl:value-of select="$locale/printbook/edit/save_msg_error/text()"/></div></td></tr>
		</xsl:if>
		<!-- /Show message -->
		
		<tr><th colspan="2"><xsl:value-of select="$title"/></th></tr>
		
	<tr>
		<td class="left_label"><label for="type">���</label></td>
		<td>
			<xsl:choose>
				<xsl:when test="pb_size/type = 1"><input type="radio" name="object[type]" id="type1" value="1" checked="yes" onclick="apb.onChangeType()"/></xsl:when>
				<xsl:otherwise><input type="radio" name="object[type]" id="type1" value="1"  onclick="apb.onChangeType()"/></xsl:otherwise>
			</xsl:choose>
			<label for="type1" class="lower">Printbook</label>&#160;&#160;&#160;
			<xsl:choose>
				<xsl:when test="pb_size/type = 2"><input type="radio" name="object[type]" id="type2" value="2" checked="yes"  onclick="apb.onChangeType()"/></xsl:when>
				<xsl:otherwise><input type="radio" name="object[type]" id="type2" value="2"  onclick="apb.onChangeType()"/></xsl:otherwise>
			</xsl:choose>
			<label for="type2" class="lower">Photobook</label>
			<xsl:choose>
				<xsl:when test="pb_size/type = 3"><input type="radio" name="object[type]" id="type2" value="3" checked="yes"  onclick="apb.onChangeType()"/></xsl:when>
				<xsl:otherwise><input type="radio" name="object[type]" id="type3" value="3"  onclick="apb.onChangeType()"/></xsl:otherwise>
			</xsl:choose>
			<label for="type3" class="lower">Polibook</label>
		</td>
	</tr>
		
		<tr>
			<td class="left_label2"><label for="name"><xsl:value-of select="$locale/printbook/edit/name/text()"/></label></td>
			<td><input type="text" class="medium" name="object[name]" id="name" value="{pb_size/name}"/></td>
		</tr>
		
		<tr>
			<td class="left_label2"><label for="price_base"><xsl:value-of select="$locale/printbook/edit/price_base/text()"/></label></td>
			<td><input type="text" class="medium" name="object[price_base]" id="price_base" value="{pb_size/price_base}"/></td>
		</tr>
		
		<tr>
			<td class="left_label2">
				<label for="price_per_page" class="when_type1 displayinline"><xsl:value-of select="$locale/printbook/edit/price_per_page/text()"/></label>
				<label for="price_per_page" class="when_type2 dontdisplay"><xsl:value-of select="$locale/printbook/edit/price_per_page2/text()"/></label>
			</td>
			<td><input type="text" class="medium" name="object[price_per_page]" id="price_per_page" value="{pb_size/price_per_page}"/></td>
		</tr>
		
		<tr>
			<td class="left_label2">
				<label for="price_personal_page" class="when_type1 displayinline"><xsl:value-of select="$locale/printbook/edit/price_personal_page/text()"/></label>
				<label for="price_personal_page" class="when_type2 dontdisplay"><xsl:value-of select="$locale/printbook/edit/price_personal_page2/text()"/></label>
			</td>
			<td><input type="text" class="medium" name="object[price_personal_page]" id="price_personal_page" value="{pb_size/price_personal_page}"/></td>
		</tr>
		
		<tr>
			<td class="left_label2"><label for="price_personal_cover"><xsl:value-of select="$locale/printbook/edit/price_personal_cover/text()"/></label></td>
			<td><input type="text" class="medium" name="object[price_personal_cover]" id="price_personal_cover" value="{pb_size/price_personal_cover}"/></td>
		</tr>
		
		<tr>
			<td class="left_label2">
				<label for="start_page" class="when_type1 displayinline"><xsl:value-of select="$locale/printbook/edit/start_page/text()"/></label>
				<label for="start_page" class="when_type2 dontdisplay"><xsl:value-of select="$locale/printbook/edit/start_page2/text()"/></label>
			</td>
			<td><input type="text" class="medium" name="object[start_page]" id="start_page" value="{pb_size/start_page}"/></td>
		</tr>
		
		<tr>
			<td class="left_label2">
				<label for="stop_page" class="when_type1 displayinline"><xsl:value-of select="$locale/printbook/edit/stop_page/text()"/></label>
				<label for="stop_page" class="when_type2 dontdisplay"><xsl:value-of select="$locale/printbook/edit/stop_page2/text()"/></label>
			</td>
			<td><input type="text" class="medium" name="object[stop_page]" id="stop_page" value="{pb_size/stop_page}"/></td>
		</tr>
		
		<tr>
			<td class="left_label2">
				<label for="page_block" class="when_type1 displayinline"><xsl:value-of select="$locale/printbook/edit/page_block/text()"/></label>
				<label for="page_block" class="when_type2 dontdisplay"><xsl:value-of select="$locale/printbook/edit/page_block2/text()"/></label>
			</td>
			<td><input type="text" class="medium" name="object[page_block]" id="page_block" value="{pb_size/page_block}"/></td>
		</tr>
		
		<tr>
			<td class="left_label2"><label><xsl:value-of select="$locale/printbook/edit/cover_min_size/text()"/></label></td>
			<td>
				<input type="text" class="small" name="object[cover_min_width]" id="cover_min_width" value="{pb_size/cover_min_width}"/>&#160;X&#160;
				<input type="text" class="small" name="object[cover_min_height]" id="cover_min_height" value="{pb_size/cover_min_height}"/>
			</td>
		</tr>
		
		<tr>
			<td class="left_label2">
				<label class="when_type1 displayinline"><xsl:value-of select="$locale/printbook/edit/page_min_size/text()"/></label>
				<label class="when_type2 dontdisplay"><xsl:value-of select="$locale/printbook/edit/page_min_size2/text()"/></label>
			</td>
			<td>
				<input type="text" class="small" name="object[page_min_width]" id="page_min_width" value="{pb_size/page_min_width}"/>&#160;X&#160;
				<input type="text" class="small" name="object[page_min_height]" id="page_min_height" value="{pb_size/page_min_height}"/>
			</td>
		</tr>
		
		<tr>
			<td class="left_label2"><label for="discount1"><xsl:value-of select="$locale/printbook/edit/discount1/text()"/></label></td>
			<td><input type="text" class="medium" name="object[discount1]" id="discount1" value="{pb_size/discount1}"/></td>
		</tr>
		
		<tr>
			<td class="left_label2"><label for="discount2"><xsl:value-of select="$locale/printbook/edit/discount2/text()"/></label></td>
			<td><input type="text" class="medium" name="object[discount2]" id="discount2" value="{pb_size/discount2}"/></td>
		</tr>
		
		<tr>
			<td class="left_label2"><label for="discount3"><xsl:value-of select="$locale/printbook/edit/discount3/text()"/></label></td>
			<td><input type="text" class="medium" name="object[discount3]" id="discount3" value="{pb_size/discount3}"/></td>
		</tr>
		
		<tr>
			<td class="left_label"><label for="image"><xsl:value-of select="$locale/printbook/boxes/edit/image/text()"/></label></td>
			<td>
				<table cellspacing="0" cellpadding="0"><tr>
				<td><input type="file" size="30" name="image" id="image"/></td>
				<td><xsl:if test="pb_size/image != ''">&#160;&#160;<a href="#" onclick="del_image('?{$controller_name}.pb_size_del_image&amp;id={$id}&amp;pb_size_id={pb_size/id}')"><img  src="{$views_path}/images/del.gif" border="0"/></a></xsl:if></td>
				<td><xsl:if test="pb_size/image != ''"><xsl:variable name="image" select="pb_size/image"/>
				&#160;&#160;<a href="#" onclick="return(popup_image_preview('640','480','{$data_path}/{$image}'))"><img src="{$views_path}/images/preview.gif" border="0"/></a></xsl:if></td>
			</tr></table>
			</td>
		</tr>
		
		<tr><th colspan="2" class="actions"><input type="submit" value="{$locale/common/buttons/form_save/text()}"/></th></tr>
	</table>
		
	</form>
	
	<script language="JavaScript" src="{$views_path}/article.js"></script>
	<script type="text/javascript">
	var apb = {
		options: {
			type1ID: 'type1',
			type2ID: 'type2',
			type3ID: 'type3'
		},
		
		init: function() {
		},
		
		onChangeType: function() {
			var self = this; var o = this.options;
			
			var type = 1;
			if( $(o.type2ID).checked ) {
				type = 2;
			}
			else if( $(o.type3ID).checked ) {
				type = 3;
			}
			 
			if( type == 2 || type == 3 ) {
				$$('.when_type1').each( function(el) {
					el.removeClassName('displayinline');
					el.addClassName('dontdisplay');
				} );
				$$('.when_type2').each( function(el) {
					el.removeClassName('dontdisplay');
					el.addClassName('displayinline');
				} );
			}
			else {
				$$('.when_type1').each( function(el) {
					el.removeClassName('dontdisplay');
					el.addClassName('displayinline');
				} );
				$$('.when_type2').each( function(el) {
					el.removeClassName('displayinline');
					el.addClassName('dontdisplay');
				} );
			}
			
		}
	};
	apb.onChangeType();
	</script>
	
</xsl:template>

</xsl:stylesheet>

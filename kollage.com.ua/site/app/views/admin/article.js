
function ask_and_go( url, msg ) {
	if( url && msg && confirm(msg) ) {
		document.location = url;
	}
}

function article_delete( url, msg )
{
    msg = (msg)? msg : 'Are you sure to delete this object?';
    if( confirm(msg) )
    {
        document.location = url;
    }
}

function article_delete_marked( url, msg )
{
    msg = (msg)? msg : 'Are you sure to delete marked objects?';
    if( confirm(msg) )
    {
        var i = 1, item = null;
        var ids = ''; delim = '';
        while( item = document.getElementById('del_item'+i) )
        {
            if( item.checked )
            {
                ids += delim + item.value;
                delim = ',';
            }
            i++;
        }
        if(ids!='')
        {
            document.location = add_to_url(url,'&ids='+ids);
        }
    }
}

function article_send2admin_marked( url, msg )
{
    msg = (msg)? msg : 'Send selected orders to admin?';
    if( confirm(msg) )
    {
        var i = 1, item = null;
        var ids = ''; delim = '';
        while( item = document.getElementById('del_item'+i) )
        {
            if( item.checked )
            {
                ids += delim + item.value;
                delim = ',';
            }
            i++;
        }
        if(ids!='')
        {
            document.location = add_to_url(url,'&ids='+ids);
        }
    }
}

function article_select_all()
{
    var item;
    var i = 1;
    var checked = 0, unchecked = 0, flag = true;
    while( item = document.getElementById('del_item'+i) )
    {
        if( item.checked ) checked ++;
        else unchecked ++;
        i++;
    }
    if( checked < (checked+unchecked) ) flag = true;
    else flag = false;
    i = 1;
    while( item = document.getElementById('del_item'+i) )
    {
        item.checked = flag;
        i++;
    }
}

function article_save_changes( url )
{
    var use_at_startpage = check_checkboxes( 'use_at_startpage' );
    if( use_at_startpage != '' )
    {
        document.location = add_to_url(url,'&use_at_startpage1='+use_at_startpage[0] + '&use_at_startpage0='+use_at_startpage[1]);
    }
}

function check_checkboxes( tag )
{
    var i = 1;
    var ids_checked = '', ids_unchecked = '', delim1 = '', delim2 = '';
    while( item = document.getElementById(tag+i) )
    {
        if( item.checked )
        {
            ids_checked += delim1 + item.value;
            delim1 = ',';
        }
        else
        {
            ids_unchecked += delim2 + item.value;
            delim2 = ',';
        }
        i++;
    }
    var retval = Array(2);
    retval[0] = ids_checked;
    retval[1] = ids_unchecked;
    return( retval );
}

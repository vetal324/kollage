<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:msxsl="urn:schemas-microsoft-com:xslt"
				xmlns:exsl="http://exslt.org/common"
				extension-element-prefixes="exsl"
				version="1.0">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="left_panel.xsl" />

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:template match="data">
	<xsl:choose>
		<xsl:when test="/content/global/user/roles/role[code='articles']/@selected = 'yes'">
			<xsl:apply-templates/>
		</xsl:when>
		<xsl:otherwise>
			<div class="error"><xsl:value-of select="$locale/common/msgs/access_denied/text()"/></div>
			<script type="text/javascript">RemoveSmallTabs();RemoveLeftPanel();</script>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="article_edit">

	<xsl:call-template name="tiny_mce"/>

	<xsl:variable name="title">
		<xsl:choose>
			<xsl:when test="article/id &gt; 0"><xsl:value-of select="$locale/services/article/edit/title1/text()"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="$locale/services/article/edit/title2/text()"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	
	<xsl:variable name="id" select="article/id"/>
	<xsl:variable name="category_id" select="article/category_id"/>
	<xsl:variable name="folder" select="article/folder"/>
	<xsl:variable name="folder_for_child" select="article/folder_for_child"/>
	<xsl:variable name="position"><xsl:value-of select="article/position"/></xsl:variable>
	<xsl:variable name="header" select="article/header"/>
	<xsl:variable name="publish_date" select="article/publish_date"/>
	<xsl:variable name="status" select="article/status"/>
	<xsl:variable name="design_type" select="article/design_type"/>
	
	<form name="reload" id="reload" action="?{$controller_name}.edit" method="post">
		<input type="hidden" name="id" value="{$id}"/>
		<input type="hidden" name="category_id" value="{$category_id}"/>
		<input type="hidden" name="aticle[category_id]" value="{$category_id}"/>
		<input type="hidden" name="folder" value="{$folder}"/>
	</form>

	<form name="article_edit" id="article_edit" action="?{$controller_name}.save&amp;category_id={$category_id}" method="post" enctype="multipart/form-data">
		<input type="hidden" name="folder" value="{$folder}"/>
		<input type="hidden" name="id" value="{$id}"/>
		<input type="hidden" name="last_position" value="{last_position}"/>
		<input type="hidden" name="article[id]" value="{$id}"/>
		<input type="hidden" name="article[category_id]" value="{$category_id}"/>
		<input type="hidden" name="article[folder]" value="{$folder}"/>
		<input type="hidden" name="article[position]" value="{$position}"/>
		<input type="hidden" name="article[lang]" value="{$lang}"/>
	<table class="form" cellspacing="0" cellpadding="0">
	
		<!-- Show message -->
		<xsl:if test="save_result = 'true'">
		<tr><td colspan="2"><div class="message"><xsl:value-of select="$locale/services/article/edit/save_msg_success/text()"/></div></td></tr>
		</xsl:if>
		<xsl:if test="save_result = 'false'">
		<tr><td colspan="2"><div class="error"><xsl:value-of select="$locale/services/article/edit/save_msg_error/text()"/></div></td></tr>
		</xsl:if>
		<!-- /Show message -->
		
		<tr><th colspan="2"><xsl:value-of select="$title"/></th></tr>
		
		<tr>
			<td class="left_label"><label for="header"><xsl:value-of select="$locale/services/article/edit/header/text()"/></label></td>
			<td><input type="text" class="large" name="article[header]" id="header" value="{$header}"/></td>
		</tr>
		
		<tr>
			<td class="left_label"><label for="page_title"><xsl:value-of select="$locale/services/article/edit/page_title/text()"/></label></td>
			<td><input type="text" class="large" name="article[page_title]" id="page_title" value="{article/page_title}"/></td>
		</tr>
		
		<tr>
			<td class="left_label"><label for="page_keywords"><xsl:value-of select="$locale/services/article/edit/page_keywords/text()"/></label></td>
			<td><input type="text" class="large" name="article[page_keywords]" id="page_keywords" value="{article/page_keywords}"/></td>
		</tr>
		
		<tr>
			<td class="left_label"><label for="relative_url"><xsl:value-of select="$locale/services/article/edit/relative_url/text()"/></label></td>
			<td><input type="text" disabled="true" class="large" id="relative_url" value="{article/relative_url}"/></td>
		</tr>
		
		<!--
		<tr><td colspan="2" class="label"><label for="ingress"><xsl:value-of select="$locale/services/article/edit/ingress/text()"/></label></td></tr>
		<tr><td colspan="2"><textarea name="article[ingress]" id="ingress" class="large" rows="11" cols="80"><xsl:value-of select="article/ingress" disable-output-escaping="yes"/></textarea></td></tr>
		-->
		
		<tr><td colspan="2" class="label"><label for="text"><xsl:value-of select="$locale/services/article/edit/text/text()"/></label></td></tr>
		<tr><td colspan="2">
			<textarea name="article[text]" id="text" class="tinymce large" rows="26" cols="80"><xsl:value-of select="article/text" disable-output-escaping="yes"/></textarea>
			<p class="help"><i><xsl:value-of select="$locale/services/article/edit/help/text()" disable-output-escaping="yes"/></i></p>
		</td></tr>
		
		<tr>
			<td class="left_label"><label for="publish_date"><xsl:value-of select="$locale/services/article/edit/publish_date/text()"/></label></td>
			<td><input type="text" class="small" name="article[publish_date]" id="publish_date" value="{$publish_date}"/></td>
		</tr>
		
		<!--
		<tr>
			<td class="left_label"><label for="image"><xsl:value-of select="$locale/services/article/edit/image/text()"/></label></td>
			<td>
				<table cellspacing="0" cellpadding="0"><tr>
				<td><input type="file" size="60" name="image" id="image"/></td>
				<td><xsl:if test="article/image != ''">&#160;&#160;<a href="#" onclick="del_image('?{$controller_name}.del_image&amp;id={$id}')"><img  src="{$views_path}/images/del.gif" border="0"/></a></xsl:if></td>
				<td><xsl:if test="article/image != ''"><xsl:variable name="image" select="article/image"/>
				&#160;&#160;<a href="#" onclick="return(popup_image_preview({article/image/@width},{article/image/@height},'{$data_path}/{$image}'))"><img src="{$views_path}/images/preview.gif" border="0"/></a></xsl:if></td>
			</tr></table>
			</td>
		</tr>
		
		<tr><td class="label"><label for="design_type"><xsl:value-of select="$locale/services/article/edit/design_type/text()"/></label></td>
		<td>
			<xsl:variable name="options">
				<item id="1"><xsl:value-of select="$locale/services/article/edit/design_types/simple/text()"/></item>
				<item id="2"><xsl:value-of select="$locale/services/article/edit/design_types/extended/text()"/></item>
			</xsl:variable>
			<xsl:variable name="selected" select="article/design_type"/>
			<select name="article[design_type]" class="middle" id="design_type">
				<xsl:for-each select="exsl:node-set($options)/*">
					<xsl:call-template name="gen-option">
						<xsl:with-param name="value" select="@id"/>
						<xsl:with-param name="title" select="."/>
						<xsl:with-param name="selected" select="$selected"/>
					</xsl:call-template>
				</xsl:for-each>
			</select>
		</td></tr>
		-->
		
		<tr>
			<td class="left_label"><label><xsl:value-of select="$locale/services/article/edit/status/text()"/></label></td>
			<td>
			<xsl:choose>
				<xsl:when test="$status = 1"><input type="radio" name="article[status]" id="status_active" value="1" checked="yes" /></xsl:when>
				<xsl:otherwise><input type="radio" name="article[status]" id="status_active" value="1" /></xsl:otherwise>
			</xsl:choose>
			<label for="status_active" class="lower"><xsl:value-of select="$locale/services/article/edit/statuses/active/text()"/></label>
			<xsl:choose>
				<xsl:when test="$status = 0"><input type="radio" name="article[status]" id="status_inactive" value="0" checked="yes"/></xsl:when>
				<xsl:otherwise><input type="radio" name="article[status]" id="status_inactive" value="0"/></xsl:otherwise>
			</xsl:choose>
			<label for="status_inactive" class="lower"><xsl:value-of select="$locale/services/article/edit/statuses/inactive/text()"/></label></td>
		</tr>
		
		<tr>
			<td class="left_label"><label><xsl:value-of select="$locale/services/article/edit/show_in_menu/text()"/></label></td>
			<td>
			<xsl:choose>
				<xsl:when test="article/show_in_menu = 1"><input type="radio" name="article[show_in_menu]" id="show_in_menu_active" value="1" checked="yes" /></xsl:when>
				<xsl:otherwise><input type="radio" name="article[show_in_menu]" id="show_in_menu_active" value="1" /></xsl:otherwise>
			</xsl:choose>
			<label for="show_in_menu_active" class="lower"><xsl:value-of select="$locale/common/yes/text()"/></label>
			<xsl:choose>
				<xsl:when test="article/show_in_menu = 0"><input type="radio" name="article[show_in_menu]" id="show_in_menu_inactive" value="0" checked="yes"/></xsl:when>
				<xsl:otherwise><input type="radio" name="article[show_in_menu]" id="show_in_menu_inactive" value="0"/></xsl:otherwise>
			</xsl:choose>
			<label for="show_in_menu_inactive" class="lower"><xsl:value-of select="$locale/common/no/text()"/></label></td>
		</tr>
		
		<tr><td colspan="2"><hr/></td></tr>
		<tr>
			<td class="left_label"><label><xsl:value-of select="$locale/services/article/edit/client_types/text()"/></label></td>
			<td>
				<xsl:apply-templates select="/content/global/client_types/type">
					<xsl:with-param name="client_types" select="article/client_types" />
				</xsl:apply-templates>
			</td>
		</tr>
		
		<tr><th colspan="2" class="actions"><input type="submit" value="{$locale/common/buttons/form_save/text()}"/></th></tr>
	</table>
		
	</form>
		
	<table class="form" cellspacing="0" cellpadding="0">
		<!-- Media box -->
		<xsl:if test="article/id &gt; 0">
		<tr><td colspan="2">&#160;</td></tr>
		<tr><th colspan="2">MEDIA</th></tr>
		<tr><td colspan="2">
			<xsl:choose>
				<xsl:when test="count(article/images/image) > 0">
					<xsl:apply-templates select="article/images">
					</xsl:apply-templates>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="empty_media_box">
						<xsl:with-param name="parent_id" select="$id"/>
						<xsl:with-param name="parent_folder" select="$folder_for_child"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</td></tr>
		</xsl:if>
		<!-- /Media box -->
		<div id="media"></div>
		
		<!-- Files box -->
		<!--
		<xsl:if test="article/id &gt; 0">
		<tr><td colspan="2">&#160;</td></tr>
		<tr><th colspan="2">UPLOADED FILES</th></tr>
		<tr><td colspan="2">
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tr>
				<th class="subheader" width="5%">#</th>
				<th class="subheader">Title</th>
				<th class="subheader" style="text-align:center" width="10%">Type</th>
				<th class="subheader" style="text-align:center" width="10%">Position</th>
				<th class="subheader" width="5%">Del</th>
			</tr>
			
			<xsl:choose>
				<xsl:when test="count(article/files/*) > 0">
					<xsl:apply-templates select="article/files">
						<xsl:with-param name="id1" select="$id1"/>
						<xsl:with-param name="id2" select="$id2"/>
						<xsl:with-param name="id3" select="$id3"/>
					</xsl:apply-templates>
				</xsl:when>
				<xsl:otherwise>
					<tr><td colspan="20" style="text-align:center">Upload new file</td></tr>
				</xsl:otherwise>
			</xsl:choose>

				<tr><td colspan="20" style="border-top:1px dotted;border-bottom:1px dotted">
		
				<form name="article_files" id="article_files" action="{$script}" method="post" enctype="multipart/form-data">
					<input type="hidden" name="obj" value="content"/>
					<input type="hidden" name="cmd" value="upload_file"/>
					<input type="hidden" name="id1" value="{$id1}"/>
					<input type="hidden" name="id2" value="{$id2}"/>
					<input type="hidden" name="id3" value="{$id3}"/>
					<input type="hidden" name="file[parent_id]" value="{$id}"/>
					
					<table cellspacing="0" cellpadding="0" width="100%"><tr>
						<td>Title:</td><td><input type="text" class="middle" name="file[title]" id="file_title" value=""/></td>
						<td>File:</td><td><input type="file" name="file" id="file_file" value=""/></td>
						<td>Type:</td><td><select name="file[type]" class="small" id="file_type">
						<xsl:for-each select="file_types/type">
							<xsl:call-template name="gen-option">
								<xsl:with-param name="value" select="@id"/>
								<xsl:with-param name="title" select="@name"/>
							</xsl:call-template>
						</xsl:for-each>
						</select></td>
						<td style="text-align:right"><input type="submit" value="Add"/></td>
					</tr></table>
		
				</form>
				
				</td></tr>
			</table>
			
		</td></tr>
		</xsl:if>
		-->
		<!-- /Files box -->
		
	</table>
	
	<xsl:if test="save_result = 'scroll_to_media'">
		<script>ScrollToObject('media')</script>
	</xsl:if>
	
	<script language="JavaScript" src="{$views_path}/media_box.js"></script>
	<script language="JavaScript" src="{$views_path}/article.js"></script>
	
</xsl:template>

<xsl:template match="images">
	<table class="media_box" width="100%" border="0">
		<xsl:apply-templates select="image">
		</xsl:apply-templates>
	</table>
</xsl:template>

<xsl:template match="image">
	<xsl:variable name="id" select="id"/>
	<xsl:variable name="parent_id" select="parent_id"/>
	<xsl:variable name="folder" select="folder"/>
	<xsl:variable name="parent_folder" select="../../folder_for_child"/>
	<xsl:variable name="thumb">
		<xsl:choose>
			<xsl:when test="thumbnail">
				<xsl:value-of select="thumbnail"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$views_path"/>/images/gallery/media_empty_thumbnail.gif
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<xsl:if test="position() = 1 or position() = 6 or position() = 11 or position() = 16 or position() = 21"><xsl:value-of disable-output-escaping = "yes" select = "string('&lt;tr&gt;')"/></xsl:if>
	<td><table class="media_box_item" width="100" height="100" cellspacing="0" cellpadding="0">
		<tr><td colspan="4"><a href="#" onclick="popup_media({$parent_id},'{$parent_folder}',{$id})"><img src="{$thumb}" width="100" height="90" border="0"/></a></td></tr>
		<tr>
			<th align="left" width="10%"><a href="?{$controller_name}.move_up_gallery&amp;id={$parent_id}&amp;image_id={$id}"><img src="{$views_path}/images/gallery/media_sortleft.gif" hspace="4" vspace="4" border="0"/></a></th>
			<th align="left" width="10%"><a href="?{$controller_name}.move_down_gallery&amp;id={$parent_id}&amp;image_id={$id}"><img src="{$views_path}/images/gallery/media_sortright.gif" hspace="4" vspace="4" border="0"/></a></th>
			<th style="text-align:center"><span class="tag"><xsl:value-of select="tag"/>&#160;</span></th>
			<th style="text-align:right" width="10%"><a href="#" onclick="del_gallery('?{$controller_name}.del_media&amp;id={$parent_id}&amp;image_id={$id}');return(false)"><img src="{$views_path}/images/gallery/media_del.gif" hspace="4" vspace="4" border="0"/></a></th>
		</tr>
	</table></td>
	
	<xsl:choose>
		<xsl:when test="count(../image) = position()">
			<td width="80%"><a href="#" onclick="popup_media({$parent_id},'{$parent_folder}',0)"><img  src="{$views_path}/images/plus.gif" border="0"/></a></td><xsl:value-of disable-output-escaping = "yes" select = "string('&lt;/tr&gt;')"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:if test="position() = 5 or position() = 10 or position() = 15 or position() = 20 or position() = 25"><td width="30%">&#160;</td><xsl:value-of disable-output-escaping = "yes" select = "string('&lt;/tr&gt;')"/></xsl:if>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="empty_media_box">
	<xsl:param name="parent_id"/>
	<xsl:param name="parent_folder"/>
	
	<table class="media_box" width="100%" height="100px"><tr><td><a href="#" onclick="popup_media({$parent_id},'{$parent_folder}',0)"><img  src="{$views_path}/images/plus.gif" border="0"/></a></td></tr></table>
</xsl:template>

<xsl:template match="type">
	<xsl:param name="client_types" />
	<table cellspacing="0" cellpadding="0"><tr>
		<xsl:choose>
			<xsl:when test="contains($client_types, id)">
				<td><input type="checkbox" id="client-type-{id}" value="{id}" name="article[client_types][]" checked="checked" /></td>
				<td><label for="client-type-{id}" class="lower"><xsl:value-of select="name"/></label></td>
			</xsl:when>
			<xsl:otherwise>
				<td><input type="checkbox" id="client-type-{id}" value="{id}" name="article[client_types][]" /></td>
				<td><label for="client-type-{id}" class="lower"><xsl:value-of select="name"/></label></td>
			</xsl:otherwise>
		</xsl:choose>
	</tr></table>
</xsl:template>

</xsl:stylesheet>

<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:msxsl="urn:schemas-microsoft-com:xslt"
				xmlns:exsl="http://exslt.org/common"
				extension-element-prefixes="exsl"
				version="1.0">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="left_panel.xsl" />

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:template match="data">
	<xsl:choose>
		<xsl:when test="/content/global/user/roles/role[code='articles']/@selected = 'yes'">
			<xsl:apply-templates/>
		</xsl:when>
		<xsl:otherwise>
			<div class="error"><xsl:value-of select="$locale/common/msgs/access_denied/text()"/></div>
			<script type="text/javascript">RemoveSmallTabs();RemoveLeftPanel();</script>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="article_edit">
	<xsl:variable name="id" select="article/id"/>
	<xsl:variable name="category_id" select="article/category_id"/>
	<xsl:variable name="folder" select="article/folder"/>
	<xsl:variable name="folder_for_child" select="article/folder_for_child"/>
	<xsl:variable name="position"><xsl:value-of select="article/position"/></xsl:variable>
	<xsl:variable name="header" select="article/header"/>
	<xsl:variable name="publish_date" select="article/publish_date"/>
	<xsl:variable name="status" select="article/status"/>
	<xsl:variable name="design_type" select="article/design_type"/>
	
	<form name="reload" id="reload" action="?{$controller_name}.service_anounces_right_edit" method="post">
		<input type="hidden" name="id" value="{$id}"/>
		<input type="hidden" name="folder" value="{$folder}"/>
	</form>

	<form name="article_edit" id="article_edit" action="?{$controller_name}.service_anounces_right_save&amp;category_id={$category_id}" method="post" enctype="multipart/form-data">
		<input type="hidden" name="folder" value="{$folder}"/>
		<input type="hidden" name="id" value="{$id}"/>
		<input type="hidden" name="last_position" value="{last_position}"/>
		<input type="hidden" name="article[id]" value="{$id}"/>
		<input type="hidden" name="article[folder]" value="{$folder}"/>
		<input type="hidden" name="article[position]" value="{$position}"/>
		<input type="hidden" name="article[lang]" value="{$lang}"/>
	<table class="form" cellspacing="0" cellpadding="0" border="0">
	
		<!-- Show message -->
		<xsl:if test="save_result = 'true'">
		<tr><td colspan="2"><div class="message"><xsl:value-of select="$locale/services/article/edit/save_msg_success/text()"/></div></td></tr>
		</xsl:if>
		<xsl:if test="save_result = 'false'">
		<tr><td colspan="2"><div class="error"><xsl:value-of select="$locale/services/article/edit/save_msg_error/text()"/></div></td></tr>
		</xsl:if>
		<!-- /Show message -->
	</table>
		
	</form>
		
	<table class="form" cellspacing="0" cellpadding="0">
		<!-- Media box -->
		<xsl:if test="article/id &gt; 0">
		<tr><th colspan="2"><xsl:value-of select="$locale/services/service_anounces_right/title/text()"/></th></tr>
		<tr><td colspan="2">
			<xsl:choose>
				<xsl:when test="count(article/images/image) > 0">
					<xsl:apply-templates select="article/images">
					</xsl:apply-templates>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="empty_media_box">
						<xsl:with-param name="parent_id" select="$id"/>
						<xsl:with-param name="parent_folder" select="$folder_for_child"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</td></tr>
		</xsl:if>
		<!-- /Media box -->
		<div id="media"></div>
		
	</table>
	
	<script language="JavaScript" src="{$views_path}/media_box.js"></script>
	<script language="JavaScript" src="{$views_path}/article.js"></script>
	
</xsl:template>

<xsl:template match="images">
	<table class="media_box" width="100%" border="0">
		<xsl:apply-templates select="image">
		</xsl:apply-templates>
	</table>
</xsl:template>

<xsl:template match="image">
	<xsl:variable name="id" select="id"/>
	<xsl:variable name="parent_id" select="parent_id"/>
	<xsl:variable name="folder" select="folder"/>
	<xsl:variable name="parent_folder" select="../../folder_for_child"/>
	<xsl:variable name="thumb">
		<xsl:choose>
			<xsl:when test="thumbnail">
				<xsl:value-of select="thumbnail"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$views_path"/>/images/gallery/media_empty_thumbnail.gif
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<xsl:if test="position() = 1 or position() = 6 or position() = 11 or position() = 16 or position() = 21"><xsl:value-of disable-output-escaping = "yes" select = "string('&lt;tr&gt;')"/></xsl:if>
	<td><table class="media_box_item" width="100" height="100" cellspacing="0" cellpadding="0">
		<tr><td colspan="4"><a href="#" onclick="popup_media_sanounce({$parent_id},'{$parent_folder}',{$id})"><img src="{$thumb}" width="100" height="90" border="0"/></a></td></tr>
		<tr>
			<th align="left" width="10%"><a href="?{$controller_name}.service_anounces_right_move_up_gallery&amp;id={$parent_id}&amp;image_id={$id}"><img src="{$views_path}/images/gallery/media_sortleft.gif" hspace="4" vspace="4" border="0"/></a></th>
			<th align="left" width="10%"><a href="?{$controller_name}.service_anounces_right_move_down_gallery&amp;id={$parent_id}&amp;image_id={$id}"><img src="{$views_path}/images/gallery/media_sortright.gif" hspace="4" vspace="4" border="0"/></a></th>
			<th style="text-align:center"><span class="tag"><xsl:value-of select="tag"/>&#160;</span></th>
			<th style="text-align:right" width="10%"><a href="#" onclick="del_gallery('?{$controller_name}.service_anounces_right_del_media&amp;id={$parent_id}&amp;image_id={$id}');return(false)"><img src="{$views_path}/images/gallery/media_del.gif" hspace="4" vspace="4" border="0"/></a></th>
		</tr>
	</table></td>
	
	<xsl:choose>
		<xsl:when test="count(../image) = position()">
			<td width="80%"><a href="#" onclick="popup_media_sanounce({$parent_id},'{$parent_folder}',0)"><img  src="{$views_path}/images/plus.gif" border="0"/></a></td><xsl:value-of disable-output-escaping = "yes" select = "string('&lt;/tr&gt;')"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:if test="position() = 5 or position() = 10 or position() = 15 or position() = 20 or position() = 25"><td width="30%">&#160;</td><xsl:value-of disable-output-escaping = "yes" select = "string('&lt;/tr&gt;')"/></xsl:if>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="empty_media_box">
	<xsl:param name="parent_id"/>
	<xsl:param name="parent_folder"/>
	
	<table class="media_box" width="100%" height="100px"><tr><td><a href="#" onclick="popup_media_sanounce({$parent_id},'{$parent_folder}',0)"><img  src="{$views_path}/images/plus.gif" border="0"/></a></td></tr></table>
</xsl:template>

</xsl:stylesheet>

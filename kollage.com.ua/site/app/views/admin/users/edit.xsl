<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:msxsl="urn:schemas-microsoft-com:xslt"
				xmlns:exsl="http://exslt.org/common"
				extension-element-prefixes="exsl"
				version="1.0">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:template match="data">
	<xsl:choose>
		<xsl:when test="/content/global/user/roles/role[code='users']/@selected = 'yes'">
			<xsl:apply-templates/>
		</xsl:when>
		<xsl:otherwise>
			<div class="error"><xsl:value-of select="$locale/common/msgs/access_denied/text()"/></div>
			<script type="text/javascript">RemoveSmallTabs();RemoveLeftPanel();</script>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="edit">

	<xsl:variable name="title">
		<xsl:choose>
			<xsl:when test="user/id &gt; 0"><xsl:value-of select="$locale/users/edit/title1/text()"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="$locale/users/edit/title2/text()"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	
	<xsl:variable name="id" select="user/id"/>
	<xsl:variable name="folder" select="user/folder"/>
	<xsl:variable name="position"><xsl:value-of select="user/position"/></xsl:variable>

	<form name="user_edit" id="user_edit" action="?{$controller_name}.save" method="post" enctype="multipart/form-data">
		<input type="hidden" name="folder" value="{$folder}"/>
		<input type="hidden" name="id" value="{$id}"/>
		<input type="hidden" name="last_position" value="{last_position}"/>
		<input type="hidden" name="object[id]" value="{$id}"/>
		<input type="hidden" name="object[folder]" value="{$folder}"/>
		<input type="hidden" name="object[position]" value="{$position}"/>
		<input type="hidden" name="object[lang]" value="{$lang}"/>
	<table class="form" cellspacing="0" cellpadding="0">
	
		<!-- Show message -->
		<xsl:if test="save_result = 'true'">
		<tr><td colspan="2"><div class="message"><xsl:value-of select="$locale/users/edit/save_msg_success/text()"/></div></td></tr>
		</xsl:if>
		<xsl:if test="save_result = 'false'">
		<tr><td colspan="2"><div class="error"><xsl:value-of select="$locale/users/edit/save_msg_error/text()"/></div></td></tr>
		</xsl:if>
		<!-- /Show message -->
		
		<tr><th colspan="2"><xsl:value-of select="$title"/></th></tr>
		
		<tr>
			<td class="left_label"><label for="name"><xsl:value-of select="$locale/users/edit/name/text()"/></label></td>
			<td><input type="text" class="large" name="object[name]" id="name" value="{user/name}"/></td>
		</tr>
		
		<tr>
			<td class="left_label"><label for="email"><xsl:value-of select="$locale/users/edit/email/text()"/></label></td>
			<td><input type="text" class="large" name="object[email]" id="email" value="{user/email}"/></td>
		</tr>
		
		<tr>
			<td class="left_label"><label for="login"><xsl:value-of select="$locale/users/edit/login/text()"/></label></td>
			<td><input type="text" class="large" name="object[login]" id="login" value="{user/login}"/></td>
		</tr>
		
		<tr>
			<td class="left_label"><label for="pwd"><xsl:value-of select="$locale/users/edit/pwd/text()"/></label></td>
			<td><input type="password" class="large" name="pwd" id="pwd" value=""/></td>
		</tr>
		
		<tr><th colspan="2"><xsl:value-of select="$locale/users/edit/roles/text()"/></th></tr>
		<tr><td colspan="2">
			<table cellspacing="0" cellpadding="0" border="0">
				<xsl:apply-templates select="user/roles"/>
			</table>
		</td></tr>
		
		<tr><th colspan="2" class="actions"><input type="submit" value="{$locale/common/buttons/form_save/text()}"/></th></tr>
	</table>
		
	</form>
		
	<script language="JavaScript" src="{$views_path}/article.js"></script>
	
</xsl:template>

<xsl:template match="role">
	<tr>
		<td class="simple" style="padding-right:15px">
			<table cellspacing="0" cellpadding="0" border="0"><tr>
				<td>
					<xsl:choose>
						<xsl:when test="@selected = 'yes'">
							<input type="checkbox" name="role_id[]" value="{id}" id="role_id{id}" checked="yes"/>
						</xsl:when>
						<xsl:otherwise>
							<input type="checkbox" name="role_id[]" value="{id}" id="role_id{id}"/>
						</xsl:otherwise>
					</xsl:choose>
				</td>
				<td><label for="role_id{id}"><xsl:value-of select="code"/></label></td>
			</tr></table>
		</td>
	</tr>
</xsl:template>

</xsl:stylesheet>

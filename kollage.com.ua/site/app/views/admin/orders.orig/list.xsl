<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" version="1.0">

    <xsl:output method="html" encoding="utf-8" indent="no"/>

    <xsl:include href="../common.xsl" />
    <xsl:include href="../locale.xsl" />
    <xsl:include href="../layout.xsl" />
    <xsl:include href="../menu.xsl" />
    <xsl:include href="../paginator.xsl" />


    <xsl:template match="data">
        <xsl:choose>
            <xsl:when test="/content/global/user/roles/role[code='clients']/@selected = 'yes'">
                <xsl:apply-templates select="orders"/>
            </xsl:when>
            <xsl:otherwise>
                <div class="error"><xsl:value-of select="$locale/common/msgs/access_denied/text()"/></div>
                <script type="text/javascript">RemoveSmallTabs();RemoveLeftPanel();</script>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="orders">
        <xsl:variable name="rows" select="count(./order)"/>
        <table class="paginator"><tr><th>
            <xsl:call-template name="paginator">
                <xsl:with-param name="prefix">top</xsl:with-param>
            </xsl:call-template>
        </th></tr></table>
        <table class="list" id="list">
            <tr>
                <th width="5%">id</th>
                <th width="10%">���</th>
                <th width="10%">�������</th>
                <th width="10%">E=mail</th>
                <th width="50%">��������</th>
                <th width="10%">���� ������</th>
                <th width="5%">��������</th>
            </tr>
            <xsl:choose>
                <xsl:when test="order">
                    <xsl:apply-templates select="order">
                    </xsl:apply-templates>
                </xsl:when>
                <xsl:otherwise>
                    <tr><td colspan="20" align="center"><xsl:value-of select="$locale/orders/order/list/insert_msg/text()"/></td></tr>
                </xsl:otherwise>
            </xsl:choose>
        </table>

        <table class="paginator"><tr><th>
            <xsl:call-template name="paginator">
                <xsl:with-param name="prefix">bottom</xsl:with-param>
            </xsl:call-template>
        </th></tr></table>

        <script language="JavaScript" src="{$views_path}/paginator.js"></script>
        <script language="JavaScript" src="{$views_path}/article.js"></script>
        <script type="text/javascript" language="javascript" charset="utf-8" src="{$views_path}/../shared/prototype.js"></script>
        <script type="text/javascript" language="javascript" charset="utf-8" src="{$views_path}/../shared/scriptaculos/scriptaculous.js"></script>
        <script type="text/javascript" language="javascript" charset="utf-8" src="{$views_path}/../shared/scriptaculos/effects.js"></script>
        <script type="text/javascript" language="javascript" charset="utf-8" src="{$views_path}/../shared/scriptaculos/dragdrop.js"></script>
        <script type="text/javascript" language="javascript" charset="utf-8" src="{$views_path}/dnd.js"></script>
        <script type="text/javascript" language="javascript">
            DefineDnDPosition( <xsl:value-of select="$rows"/> );

            function ChangePosition( src, dst )
            {
            src_id = $F( 'cid'+src ); dst_id = $F( 'cid'+dst );
            document.location = '?<xsl:value-of select="$controller_name"/>.swap_position&amp;src='+ src_id +'&amp;dst='+ dst_id +'&amp;page=<xsl:value-of select="$page"/>&amp;pages=<xsl:value-of select="$pages"/>';
            }
        </script>

    </xsl:template>

    <xsl:template match="order">
        <xsl:variable name="id" select="id"/>
        <tr id="row{position()}">
            <td class="row"><xsl:value-of select="id"/></td>
            <td class="row"><xsl:value-of select="name"/></td>
            <td class="row"><xsl:value-of select="phone"/></td>
            <td class="row"><xsl:value-of select="email"/></td>
            <td class="row">
                <IMG>
                    <xsl:attribute name="src">
                        <xsl:value-of select="photo"/>
                    </xsl:attribute>
                </IMG>
            </td>
            <td class="row"><xsl:value-of select="date_create"/></td>
            <td class="row" align="center"><a title="{$locale/common/del/text()}" href="#" onclick="article_delete('?{$controller_name}.del&amp;id={id}&amp;page={$page}','{$locale/common/msgs/delete_item/text()}')"><img  src="{$views_path}/images/del.gif" border="0"/></a></td>
        </tr>
    </xsl:template>

</xsl:stylesheet>

<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />

<xsl:template match="/">
  <html>
	<head>
	  <title><xsl:value-of select="$locale/common/title/text()"/></title>
	  <link rel="stylesheet" href="{$views_path}/styles.css" />
	  <script language="JavaScript" src="{$views_path}/common.js"></script>
	</head>
	<body>
		<div style="padding:12px">

			<form name="purse_operation" id="purse_operation" action="?{$controller_name}.save" method="post" 
				onsubmit="return(checkform(this,[['amount','number','{$locale/clients/purse_operations/edit/messages/m2/text()}'],['amount','null','{$locale/clients/purse_operations/edit/messages/m1/text()}'],['description','null','{$locale/clients/purse_operations/edit/messages/m3/text()}']]))">
				<input type="hidden" name="object[client_id]" value="0"/>
			<table class="form" cellspacing="0" cellpadding="0">
				
				<tr><th colspan="2" align="left"><xsl:value-of select="$locale/clients/purse_operations/edit/title/text()"/></th></tr>
				
				<tr>
					<td class="left_label"><label for="amount"><xsl:value-of select="$locale/clients/purse_operations/edit/amount/text()"/></label></td>
					<td><input type="text" class="small" name="object[amount]" id="amount" value="0.0"/></td>
				</tr>
				
				<tr>
					<td class="left_label"><label for="description"><xsl:value-of select="$locale/clients/purse_operations/edit/description/text()"/></label></td>
					<td><input type="text" class="large" name="object[description]" id="description" value=""/></td>
				</tr>
				
				<tr><th colspan="2" class="actions"><input type="submit" value="{$locale/common/buttons/form_save/text()}"/></th></tr>
			</table>
			</form>
	
			<xsl:apply-templates select="content/documents/data/reloader"/>
		</div>
	</body>
  </html>
</xsl:template>

<xsl:template match="reloader">
	<script language="JavaScript">
		var op = window.opener;
		if(op)
		{
			var form = op.document.getElementById('reload');
			form.submit();
		}
		<xsl:if test="@close = 'yes'">
		window.close();
		</xsl:if>
	</script>
</xsl:template>

</xsl:stylesheet>

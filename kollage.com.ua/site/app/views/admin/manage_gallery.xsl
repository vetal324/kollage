<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
                
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:variable name="script"></xsl:variable>

<xsl:variable name="site_root"><xsl:value-of select="/documents/options/option[@name='site_root']/@value"/></xsl:variable>
<xsl:variable name="path"><xsl:value-of select="/documents/options/option[@name='templates_folder']/@value"/></xsl:variable>
<xsl:variable name="request_url"><xsl:value-of select="/documents/options/option[@name='request_url']/@value"/></xsl:variable>
<xsl:variable name="uploaded_url"><xsl:value-of select="/documents/options/option[@name='upload_path']/@value"/></xsl:variable>

<xsl:template match="/">
  <html>
    <head>
      <title><xsl:value-of select="/documents/dictionary/item[@id='1']"/></title>
      <link rel="stylesheet" href="{$path}styles/styles.css" />
    </head>
    <body>
        <div style="padding:12px"><xsl:apply-templates select="documents/content"/></div>
    </body>
  </html>
</xsl:template>

<xsl:template match="content">
    <xsl:apply-templates />
</xsl:template>

<xsl:template match="reloader">
    <script language="JavaScript">
        var op = window.opener;
        if(op)
        {
            var form = op.document.getElementById('reload');
            form.submit();
        }
        <xsl:if test="@close = 'yes'">
        window.close();
        </xsl:if>
    </script>
</xsl:template>


<xsl:template match="gallery">
    <xsl:variable name="id" select="id"/>
    <xsl:variable name="parent_id" select="parent_id"/>
    <xsl:variable name="folder" select="folder"/>
    <xsl:variable name="title" select="title"/>
    <xsl:variable name="author" select="author"/>
    
    <xsl:variable name="header">
        <xsl:choose>
            <xsl:when test="id &gt; 0">EDIT EXISTENT MEDIA</xsl:when>
            <xsl:otherwise>ADD NEW MEDIA</xsl:otherwise>
        </xsl:choose>
    </xsl:variable>

    <form name="gallery_edit" id="gallery_edit" action="manage_gallery.aspx" method="post" enctype="multipart/form-data">
        <input type="hidden" name="cmd" value="save"/>
        <input type="hidden" name="gallery[id]" value="{$id}"/>
        <input type="hidden" name="gallery[parent_id]" value="{$parent_id}"/>
        <input type="hidden" name="gallery[folder]" value="{$folder}"/>
    <table class="form" cellspacing="0" cellpadding="0">
        
        <tr><th colspan="2"><xsl:value-of select="$header"/></th></tr>
        
        <tr>
            <td class="left_label"><label for="title">title</label></td>
            <td><input type="text" class="large" name="gallery[title]" id="title" value="{$title}"/></td>
        </tr>
        
        <tr>
            <td class="left_label"><label for="author">author</label></td>
            <td><input type="text" class="large" name="gallery[author]" id="author" value="{$author}"/></td>
        </tr>
        
        <tr><td colspan="2" class="label"><label for="text">text</label></td></tr>
        <tr><td colspan="2"><textarea name="gallery[text]" id="text" class="large" rows="5" cols="80"><xsl:value-of select="text"/></textarea></td></tr>
        
        <tr><td colspan="2" class="label"><label for="thumbnail">thumbnail image<xsl:if test="../image_sizes/size[@tag='thumbnail']"> (<xsl:value-of select="../image_sizes/size[@tag='thumbnail']/@width"/> x <xsl:value-of select="../image_sizes/size[@tag='thumbnail']/@height"/> pixels)</xsl:if></label></td></tr>
        <tr><td colspan="2">
            <table cellspacing="0" cellpadding="0"><tr>
                <td><input type="file" size="70" name="thumbnail" id="thumbnail"/></td>
                <td>&#160;<xsl:if test="images/image[@tag='thumbnail']/filename != ''"><a href="#" onclick="media_delimage('{$script}?cmd=delimage&amp;parent_id={$parent_id}&amp;parent_folder={$folder}&amp;id={$id}&amp;image_id={images/image[@tag='thumbnail']/id}')"><img  src="{$path}images/del.gif" border="0"/></a></xsl:if></td>
                <td><xsl:if test="images/image[@tag='thumbnail']/filename != ''"><a href="#" onclick="popup_image_preview({images/image[@tag='thumbnail']/width},{images/image[@tag='thumbnail']/height},'{$uploaded_url}{images/image[@tag='thumbnail']/filename}');return(false);"><img  src="{$path}images/preview.gif" border="0"/></a></xsl:if></td>
            </tr></table>
        </td></tr>
        
        <tr><td colspan="2" class="label"><label for="normal">normal image<xsl:if test="../image_sizes/size[@tag='normal']"> (<xsl:value-of select="../image_sizes/size[@tag='normal']/@width"/> x <xsl:value-of select="../image_sizes/size[@tag='normal']/@height"/> pixels)</xsl:if></label></td></tr>
        <tr><td colspan="2">
            <table cellspacing="0" cellpadding="0"><tr>
                <td><input type="file" size="70" name="normal" id="normal"/></td>
                <td>&#160;<xsl:if test="images/image[@tag='normal']/filename != ''"><a href="#" onclick="media_delimage('{$script}?cmd=delimage&amp;parent_id={$parent_id}&amp;parent_folder={$folder}&amp;id={$id}&amp;image_id={images/image[@tag='normal']/id}')"><img  src="{$path}images/del.gif" border="0"/></a></xsl:if></td>
                <td><xsl:if test="images/image[@tag='normal']/filename != ''"><a href="#" onclick="popup_image_preview({images/image[@tag='normal']/width},{images/image[@tag='normal']/height},'{$uploaded_url}{images/image[@tag='normal']/filename}');return(false);"><img  src="{$path}images/preview.gif" border="0"/></a></xsl:if></td>
            </tr></table>
        </td></tr>
        
        <tr><td colspan="2" class="label"><label for="normal">big image<xsl:if test="../image_sizes/size[@tag='big']"> (<xsl:value-of select="../image_sizes/size[@tag='big']/@width"/> x <xsl:value-of select="../image_sizes/size[@tag='big']/@height"/> pixels)</xsl:if></label></td></tr>
        <tr><td colspan="2">
            <table cellspacing="0" cellpadding="0"><tr>
                <td><input type="file" size="70" name="big" id="big"/></td>
                <td>&#160;<xsl:if test="images/image[@tag='big']/filename != ''"><a href="#" onclick="media_delimage('{$script}?cmd=delimage&amp;parent_id={$parent_id}&amp;parent_folder={$folder}&amp;id={$id}&amp;image_id={images/image[@tag='big']/id}')"><img  src="{$path}images/del.gif" border="0"/></a></xsl:if></td>
                <td><xsl:if test="images/image[@tag='big']/filename != ''"><a href="#" onclick="popup_image_preview({images/image[@tag='big']/width},{images/image[@tag='big']/height},'{$uploaded_url}{images/image[@tag='big']/filename}');return(false);"><img  src="{$path}images/preview.gif" border="0"/></a></xsl:if></td>
            </tr></table>
        </td></tr>
        
        <tr><th colspan="2" class="actions"><input type="button" value="Close" onclick="self.close()"/>&#160;&#160;<input type="submit" value="Save"/></th></tr>
    </table>
    </form>
    
    <script language="JavaScript">
    function media_delimage( url )
    {
        if( confirm('Delete this image?') )
        {
            document.location = url;
        }
    }
    </script>
    
</xsl:template>

</xsl:stylesheet>

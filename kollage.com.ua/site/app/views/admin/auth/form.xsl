<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:asp="remove" version="1.0">

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />

<xsl:template match="/">
  <xsl:variable name="return_url" select="/documents/content/return_url"/>
  <html>
	<head>
	  <title><xsl:value-of select="$locale/common/title/text()"/></title>
	  <link rel="stylesheet" href="{$views_path}/styles.css" />
	</head>
	<body>
		 <table width="100%" height="80%" border="0" cellspacing="0" cellpadding="0">
			<tr><td valign="middle" align="center">
			
			<table width="384" height="212" border="0" cellspacing="0" cellpadding="0" background="{$views_path}/images/logon/login_form_bg.gif"><tr><td>
			
				<table width="384" height="212" border="0" cellspacing="0" cellpadding="0" background="">
					<tr>
						<td height="25" valign="top" align="left">
							<table border="0" cellspacing="0" cellpadding="0"><tr>
								<td><img src="{$views_path}/images/bigtabs/bigtab_left_active.gif"/></td>
								<td align="center" background="{$views_path}/images/bigtabs/bigtab_body_active.gif" class="bigtab_active"><a href="" class="bigtab_active"><xsl:value-of select="$locale/login_form/form_title/text()"/></a></td>
								<td><img src="{$views_path}/images/bigtabs/bigtab_right_active.gif"/></td>
							</tr></table>
						</td>
					</tr>
					<tr>
						<td height="187" valign="middle" align="center">
							<xsl:if test="content/documents/error_login">
								<label class="error" name="Msg" id="Msg"><xsl:value-of select="$locale/login_form/error_login/text()"/></label>
							</xsl:if>
							
							<table border="0" cellspacing="2" cellpadding="4" background="">
								<form method="POST">
								<input type="hidden" name="ReturnUrl" value="{$return_url}"/>
								<tr>
									<td class="lbllogin"><xsl:value-of select="$locale/login_form/username/text()"/></td>
									<td><input type="text" name="username" id="username"/></td>
								</tr>
								<tr>
									<td class="lbllogin"><xsl:value-of select="$locale/login_form/password/text()"/></td>
									<td><input type="password" name="password" id="password"/></td>
								</tr>
								<tr>
									<td colspan="2" align="right"><input type="submit" value="{$locale/login_form/form_submit/text()}" class="btn"/></td>
								</tr>
								</form>
							</table>
						</td>
					</tr>
				</table>
			
			</td></tr></table>
			
			</td></tr>
		 </table>
	</body>
  </html>
</xsl:template>

</xsl:stylesheet>

<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:msxsl="urn:schemas-microsoft-com:xslt"
				xmlns:exsl="http://exslt.org/common"
				extension-element-prefixes="exsl"
				version="1.0">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:template match="data">
	<xsl:choose>
		<xsl:when test="/content/global/user/roles/role[code='shop_orders']/@selected = 'yes'">
			<xsl:apply-templates/>
		</xsl:when>
		<xsl:otherwise>
			<div class="error"><xsl:value-of select="$locale/common/msgs/access_denied/text()"/></div>
			<script type="text/javascript">RemoveSmallTabs();RemoveLeftPanel();</script>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="order">

<script language="JavaScript" src="{$views_path}/article.js"></script>

<table class="form" cellspacing="0" cellpadding="0">
	<tr><th><xsl:value-of select="$locale/shop/orders/view/title/text()"/></th></tr>
	
	<tr><td>
		<table width="100%">
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/shop/orders/view/number/text()"/></b></td>
				<td class="row"><xsl:value-of select="order_number"/></td>
			</tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/shop/orders/view/created_at/text()"/></b></td>
				<td class="row"><xsl:value-of select="created_at"/></td>
			</tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/shop/orders/view/paymentway_name/text()"/></b></td>
				<td class="row">
					<xsl:choose>
						<xsl:when test="paymentway/name != ''"><xsl:value-of select="paymentway/name"/></xsl:when>
						<xsl:otherwise>-</xsl:otherwise>
					</xsl:choose>
				</td>
			</tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/shop/orders/view/delivery/text()"/></b></td>
				<td class="row">
					<xsl:choose>
						<xsl:when test="delivery/name != ''"><xsl:value-of select="delivery/name"/></xsl:when>
						<xsl:otherwise>-</xsl:otherwise>
					</xsl:choose>
				
				</td>
			</tr>
			
			<xsl:if test="delivery_id = 1">
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/shop/orders/view/point_name/text()"/></b></td>
				<td class="row"><xsl:value-of select="point_name"/></td>
			</tr>
			</xsl:if>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/shop/orders/view/status/text()"/></b></td>
				<td class="row">
					<xsl:choose>
						<xsl:when test="status = 1">
							<xsl:value-of select="$locale/shop/orders/statuses/s1/text()"/>
						</xsl:when>
						<xsl:when test="status = 2">
							<xsl:value-of select="$locale/shop/orders/statuses/s2/text()"/>
						</xsl:when>
						<xsl:when test="status = 3">
							<xsl:value-of select="$locale/shop/orders/statuses/s3/text()"/>
						</xsl:when>
						<xsl:when test="status = 4">
							<xsl:value-of select="$locale/shop/orders/statuses/s4/text()"/>
						</xsl:when>
						<xsl:when test="status = 99">
							<xsl:value-of select="$locale/shop/orders/statuses/s99/text()"/>
						</xsl:when>
					</xsl:choose>
				</td>
			</tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/sop/orders/list/comments/text()"/></b></td>
				<td class="row" style="color:#336699"><xsl:value-of select="comments"/>&#160;</td>
			</tr>
			
			<tr><td class="row" colspan="2">&#160;</td></tr>
			
			<xsl:variable name="order_name">
				<xsl:choose>
					<xsl:when test="name != ''"><xsl:value-of select="name"/></xsl:when>
					<xsl:otherwise><xsl:value-of select="client/firstname"/>&#160;<xsl:value-of select="client/lastname"/></xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			
			<xsl:variable name="order_phone">
				<xsl:choose>
					<xsl:when test="phone != ''"><xsl:value-of select="phone"/></xsl:when>
					<xsl:otherwise><xsl:value-of select="client/phone"/></xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			
			<xsl:variable name="order_email">
				<xsl:choose>
					<xsl:when test="email != ''"><xsl:value-of select="email"/></xsl:when>
					<xsl:otherwise><xsl:value-of select="client/email"/></xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/shop/orders/view/client/text()"/></b></td>
				<td class="row"><xsl:value-of select="$order_name"/></td>
			</tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/shop/orders/view/client_type/text()"/></b></td>
				<td class="row">
					<xsl:choose>
						<xsl:when test="client/type != ''"><xsl:value-of select="client/type"/></xsl:when>
						<xsl:otherwise>-</xsl:otherwise>
					</xsl:choose>
				</td>
			</tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/shop/orders/view/email/text()"/></b></td>
				<td class="row"><a href="mailto:{$order_email}"><xsl:value-of select="$order_email"/></a></td>
			</tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/shop/orders/view/address/text()"/></b></td>
				<td class="row"><xsl:value-of select="client/city"/>, <xsl:value-of select="client/street/name"/>, <xsl:value-of select="client/address"/></td>
			</tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/shop/orders/view/address2/text()"/></b></td>
				<td class="row"><xsl:if test="address = ''">&#160;</xsl:if><xsl:value-of select="address"/></td>
			</tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/shop/orders/view/phone/text()"/></b></td>
				<td class="row"><xsl:value-of select="$order_phone"/></td>
			</tr>
			
			<tr><td colspan="2">&#160;</td></tr>
		
			<tr>
				<td colspan="2">
					<table cellspacing="1" cellpadding="3" width="100%" border="0">
						<tr><th colspan="7" style="text-align:center"><xsl:value-of select="$locale/shop/orders/view/products/text()"/></th></tr>
						<tr>
							<td width="2%" align="center"><b>#</b></td>
							<td width="2%" align="center">&#160;</td>
							<td width="*" align="center"><b><xsl:value-of select="$locale/shop/orders/view/name/text()"/></b></td>
							<td width="15%" align="center"><b><xsl:value-of select="$locale/shop/orders/view/price/text()"/></b></td>
							<td width="10%" align="center"><b><xsl:value-of select="$locale/shop/orders/view/quantity/text()"/></b></td>
							<td width="15%" align="center"><b><xsl:value-of select="$locale/shop/orders/view/total/text()"/></b></td>
							<td width="2%" align="center">&#160;</td>
						</tr>
						
						<xsl:apply-templates select="products"/>
						
					</table>
					<p>
						<img src="{$views_path}/images/a.jpg" border="0" width="7" height="7"/> - ���� � �������<br/>
						<img src="{$views_path}/images/i.jpg" border="0" width="7" height="7"/> - ��� � �������
					</p>
				</td>
			</tr>
			
			<tr><td colspan="2">&#160;</td></tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/shop/orders/view/delivery_price/text()"/></b></td>
				<td class="row"><xsl:value-of select="delivery/price"/></td>
			</tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/shop/orders/view/total_price/text()"/></b></td>
				<td class="row"><xsl:value-of select="format-number( number(amount) * $exchange_rate, '0.00')"/></td>
			</tr>
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/shop/orders/view/payed_price/text()"/></b></td>
				<td class="row">
					<xsl:choose>
						<xsl:when test="payed != ''"><xsl:value-of select="payed"/></xsl:when>
						<xsl:otherwise>-</xsl:otherwise>
					</xsl:choose>
				</td>
			</tr>
			
		</table>
	</td></tr>
	
	<tr><th colspan="2" class="actions">
	
	<xsl:if test="status = 1">
		<input type="button" value="{$locale/common/buttons/form_process/text()}" onclick="ProcessOrder({id});return(false)"/>
		<script>
			function ProcessOrder( id )
			{
				if( confirm('<xsl:value-of select="$locale/shop/orders/view/process_msg/text()"/>') )
				{
					document.location = '?<xsl:value-of select="$controller_name"/>.process&amp;id='+ id;
				}
			}
		</script>
	</xsl:if>
	
	<xsl:if test="status = 2">
		<input type="button" value="{$locale/common/buttons/form_order_finish/text()}" onclick="ProcessOrder({id});return(false)"/>
		<script>
			function ProcessOrder( id )
			{
				if( confirm('<xsl:value-of select="$locale/shop/orders/view/process_msg2/text()"/>') )
				{
					document.location = '?<xsl:value-of select="$controller_name"/>.process&amp;status=3&amp;id='+ id;
				}
			}
		</script>
	</xsl:if>
	
	</th></tr>
	
	<tr><td>&#160;</td></tr>
	
	<tr><th><xsl:value-of select="$locale/shop/orders/view/title2/text()"/></th></tr>
	<tr><td>
	
	<xsl:call-template name="tiny_mce"/>

	<form name="send" id="send" action="?{$controller_name}.order_send" method="post">
		<input type="hidden" name="id" value="{id}"/>
		
		<table class="form" cellspacing="0" cellpadding="0">
		
			<tr>
				<td class="row"><b><xsl:value-of select="$locale/shop/orders/view/subject/text()"/></b></td>
				<td class="row"><input type="text" class="large" name="subject" value="N{order_number}"/></td>
			</tr>
		
			<tr>
				<td class="row" colspan="2"><b><xsl:value-of select="$locale/shop/orders/view/body/text()"/></b></td>
			</tr>
			<tr>
				<td class="row" colspan="2"><textarea name="body" class="tinymce large" rows="10"></textarea></td>
			</tr>
			
			<tr><th colspan="2" class="actions"><input type="submit" value="{$locale/common/buttons/form_send/text()}"/></th></tr>
		</table>
	</form>
	
	</td></tr>
</table>

</xsl:template>

<xsl:template match="order_product">
	<xsl:variable name="product_name">
		<xsl:choose>
			<xsl:when test="product/id > 0"><xsl:value-of select="product/name"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="product_name"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<tr>
		<xsl:choose>
			<xsl:when test="pexists = 1">
				<td class="green"><xsl:value-of select="position()"/>.</td>
				<td class="green" align="center"><img src="{$views_path}/images/a.jpg" border="0" width="7" height="7" onclick="document.location='?{$controller_name}.set_exists&amp;id={id}&amp;v=0&amp;category_id={category_id}';return(false)" style="cursor:pointer"/></td>
				<td class="green"><xsl:value-of select="$product_name"/></td>
				<td class="green" align="right"><xsl:value-of select="format-number( number(price) * $exchange_rate, '0.00')"/></td>
				<td class="green" align="center"><xsl:value-of select="quantity"/></td>
				<td class="green" align="right"><xsl:value-of select="format-number( number(price) * $exchange_rate * number(quantity), '0.00')"/></td>
			</xsl:when>
			
			<xsl:otherwise>
				<td class="red"><xsl:value-of select="position()"/>.</td>
				<td class="red" align="center"><img src="{$views_path}/images/i.jpg" border="0" width="7" height="7" onclick="document.location='?{$controller_name}.set_exists&amp;id={id}&amp;v=1&amp;category_id={category_id}';return(false)" style="cursor:pointer"/></td>
				<td class="red"><xsl:value-of select="$product_name"/></td>
				<td class="red" align="right"><xsl:value-of select="format-number( number(price) * $exchange_rate, '0.00')"/></td>
				<td class="red" align="center"><xsl:value-of select="quantity"/></td>
				<td class="red" align="right"><xsl:value-of select="format-number( number(price) * $exchange_rate * number(quantity), '0.00')"/></td>
			</xsl:otherwise>
			
		</xsl:choose>
		<td align="right"><a title="{$locale/common/del/text()}" href="#" onclick="article_delete('?{$controller_name}.del_product&amp;id={id}&amp;page={$page}','{$locale/common/msgs/delete_item/text()}');return(false)"><img  src="{$views_path}/images/del.gif" border="0"/></a></td>
	</tr>
</xsl:template>

</xsl:stylesheet>

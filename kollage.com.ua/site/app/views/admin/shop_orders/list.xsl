<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" version="1.0">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout.xsl" />
<xsl:include href="../menu.xsl" />
<xsl:include href="../paginator.xsl" />

<xsl:template match="data">
	<xsl:choose>
		<xsl:when test="/content/global/user/roles/role[code='shop_orders']/@selected = 'yes'">
			<xsl:apply-templates select="list"/>
		</xsl:when>
		<xsl:otherwise>
			<div class="error"><xsl:value-of select="$locale/common/msgs/access_denied/text()"/></div>
			<script type="text/javascript">RemoveSmallTabs();RemoveLeftPanel();</script>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="list">
	<xsl:variable name="rows" select="count(./order)"/>

	<table class="paginator"><tr><th>
		<xsl:call-template name="paginator">
			<xsl:with-param name="prefix">top</xsl:with-param>
		</xsl:call-template>
	</th></tr></table>
	
	<table class="list" id="list">
		<tr>
			<th width="2%" style="text-align:center"><a href="#" style="color:blue" onclick="article_select_all();return(false)">#</a></th>
			<th width="10%"><xsl:value-of select="$locale/shop/orders/list/number/text()"/></th>
			<th width="10%"><xsl:value-of select="$locale/shop/orders/list/status/text()"/></th>
			<th width="*"><xsl:value-of select="$locale/shop/orders/list/client/text()"/></th>
			<th width="10%"><xsl:value-of select="$locale/shop/orders/list/phone/text()"/></th>
			<th width="15%"><xsl:value-of select="$locale/shop/orders/list/email/text()"/></th>
			<th width="10%"><xsl:value-of select="$locale/shop/orders/list/delivery/text()"/></th>
			<th width="10%"><xsl:value-of select="$locale/shop/orders/list/paymentway/text()"/></th>
			<th width="8%"><xsl:value-of select="$locale/shop/orders/list/total_price/text()"/></th>
			<th width="8%"><xsl:value-of select="$locale/shop/orders/list/payed_price/text()"/></th>
			<th width="10%"><xsl:value-of select="$locale/shop/orders/list/created_at/text()"/></th>
			<th width="2%" style="text-align:center">&#160;</th>
			<th width="2%" style="text-align:center">&#160;</th>
		</tr>
		
		<xsl:choose>
			<xsl:when test="order">
				<xsl:apply-templates select="order">
				</xsl:apply-templates>
			</xsl:when>
			<xsl:otherwise>
				<tr><td colspan="20" align="center"><xsl:value-of select="$locale/shop/orders/list/insert_msg/text()"/></td></tr>
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:variable name="btn_save" select="$locale/shop/orders/buttons/save/text()"/>
		<xsl:variable name="btn_delete_marked" select="$locale/shop/orders/buttons/delete_marked/text()"/>
		<xsl:variable name="btn_add_new" select="$locale/shop/orders/buttons/add_new/text()"/>
		
		<tr>
			<th class="actions" colspan="20">
				<table align="right"><tr>
					<td><input type="button" value="{$btn_delete_marked}" onclick="article_delete_marked('?{$controller_name}.orders_del_marked','{$locale/common/msgs/delete_items/text()}')"/></td>
				</tr></table>
			</th>
		</tr>
	</table>
	
	<table class="paginator"><tr><th>
		<xsl:call-template name="paginator">
			<xsl:with-param name="prefix">bottom</xsl:with-param>
		</xsl:call-template>
	</th></tr></table>
	
	<p><xsl:value-of select="$locale/shop/orders/list/help/text()" disable-output-escaping="yes"/></p>
	
	<xsl:if test="../parameters = 'scroll_to_list'">
		<script>ScrollToObject('list')</script>
	</xsl:if>
	
	<script language="JavaScript" src="{$views_path}/paginator.js"></script>
	<script language="JavaScript" src="{$views_path}/article.js"></script>
	
</xsl:template>

<xsl:template match="order">
	<xsl:variable name="id" select="id"/>
	<tr id="row{position()}">
		<td class="row"><input type="checkbox" name="del_item{position()}" id="del_item{position()}" value="{$id}"/></td>
		<td class="row">
			<xsl:choose>
				<xsl:when test="amount &lt;= payed and payed != 0">
					<span style="color:#00cc00"><b><xsl:value-of select="order_number"/></b></span>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="order_number"/>
				</xsl:otherwise>
			</xsl:choose>
		</td>
		<td class="row">
			<xsl:choose>
				<xsl:when test="status = 1">
					<xsl:value-of select="$locale/shop/orders/statuses/s1/text()"/>
				</xsl:when>
				<xsl:when test="status = 2">
					<xsl:value-of select="$locale/shop/orders/statuses/s2/text()"/>
				</xsl:when>
				<xsl:when test="status = 3">
					<xsl:value-of select="$locale/shop/orders/statuses/s3/text()"/>
				</xsl:when>
				<xsl:when test="status = 4">
					<xsl:value-of select="$locale/shop/orders/statuses/s4/text()"/>
				</xsl:when>
				<xsl:when test="status = 99">
					<xsl:value-of select="$locale/shop/orders/statuses/s99/text()"/>
				</xsl:when>
			</xsl:choose>
		</td>
			
		<xsl:variable name="order_name">
			<xsl:choose>
				<xsl:when test="name != ''"><xsl:value-of select="name"/></xsl:when>
				<xsl:otherwise><xsl:value-of select="client/firstname"/>&#160;<xsl:value-of select="client/lastname"/></xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		
		<xsl:variable name="order_phone">
			<xsl:choose>
				<xsl:when test="phone != ''"><xsl:value-of select="phone"/></xsl:when>
				<xsl:otherwise><xsl:value-of select="client/phone"/></xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		
		<xsl:variable name="order_email">
			<xsl:choose>
				<xsl:when test="email != ''"><xsl:value-of select="email"/></xsl:when>
				<xsl:otherwise><xsl:value-of select="client/email"/></xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		
		<td class="row"><xsl:value-of select="$order_name"/></td>
		<td class="row"><xsl:value-of select="$order_phone"/></td>
		<td class="row"><a href="mailto:{$order_email}"><xsl:value-of select="$order_email"/></a></td>
		<td class="row"><xsl:value-of select="delivery/name"/></td>
		<td class="row"><xsl:value-of select="paymentway/name"/></td>
		<td class="row"><xsl:value-of select="format-number( number(amount) * $exchange_rate, '0.00')"/></td>
		<td class="row">
			<xsl:choose>
				<xsl:when test="number(amount) * $exchange_rate &lt;= payed and payed != 0">
					<span style="color:#00cc00"><b><xsl:value-of select="payed"/></b></span>
				</xsl:when>
				<xsl:otherwise>
					<b><xsl:value-of select="payed"/></b>
				</xsl:otherwise>
			</xsl:choose>
		</td>
		<td class="row"><xsl:value-of select="created_at"/></td>
		
		<td class="row" align="center"><a title="{$locale/common/view/text()}" href="?{$controller_name}.order_view&amp;id={$id}"><img src="{$views_path}/images/edit.gif" border="0"/></a></td>
		<td class="row" align="center"><a title="{$locale/common/del/text()}" href="#" onclick="article_delete('?{$controller_name}.order_del&amp;id={id}&amp;list={status}','{$locale/common/msgs/delete_item/text()}')"><img  src="{$views_path}/images/del.gif" border="0"/></a></td>
	</tr>
</xsl:template>

</xsl:stylesheet>

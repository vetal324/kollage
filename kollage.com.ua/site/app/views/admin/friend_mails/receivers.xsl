<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" version="1.0">

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../layout.xsl" />
<xsl:include href="../menu.xsl" />
<xsl:include href="../paginator.xsl" />

<xsl:template match="data">

    <table class="paginator"><tr><th>
        <xsl:call-template name="paginator">
            <xsl:with-param name="prefix">top</xsl:with-param>
        </xsl:call-template>
    </th></tr></table>
    
    <table class="list">
        <tr>
            <th width="2%">#</th>
            <th width="40%">Email</th>
            <th>Created at</th>
        </tr>
        
        <xsl:apply-templates/>
    </table>
    
    <table class="paginator"><tr><th>
        <xsl:call-template name="paginator">
            <xsl:with-param name="prefix">bottom</xsl:with-param>
        </xsl:call-template>
    </th></tr></table>
    <script language="JavaScript" src="{$views_path}/paginator.js"></script>

</xsl:template>

<xsl:template match="items">
    <xsl:apply-templates/>
</xsl:template>

<xsl:template match="item">
    <tr>
        <td class="row"></td>
        <td class="row"><xsl:value-of select="sender_email"/></td>
        <td class="row"><xsl:value-of select="created_at"/></td>
    </tr>
</xsl:template>

</xsl:stylesheet>

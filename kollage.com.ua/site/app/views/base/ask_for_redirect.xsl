<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
                
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />

<xsl:template match="/">
  <html>
    <head>
      <title><xsl:value-of select="$locale/common/title/text()" disable-output-escaping="yes"/></title>
      <link rel="stylesheet" href="{$views_path}/styles.css" />
      <script language="JavaScript" src="{$views_path}/common.js"></script>
    </head>
    <body>

    <table width="100%" height="100%" cellpadding="0" cellspacing="0"><tr><td align="center" valign="middle">
    
    <table width="640" align="center" class="page" cellpadding="0" cellspacing="0">
        <tr><td style="height:98px;background-image:url({$views_path}/images/site_detect_logo.jpg);background-repeat:no-repeat">&#160;</td></tr>
        <tr>
            <td valign="top" style="padding:10px;">
            
                <xsl:variable name="network" select="/content/documents/data/network"/>
            
                <div style="font-weight: bold;text-align:center;color:#ff9000"><xsl:value-of select="$locale/redirect/title/text()" disable-output-escaping="yes"/></div>
                <div style="font-weight: bold;margin-top:5px;margin-bottom:5px"><xsl:value-of select="$locale/redirect/name/text()" disable-output-escaping="yes"/>&#160;<xsl:value-of select="$locale/redirect/networks/*[name()=$network]" disable-output-escaping="yes"/></div>
                <div><xsl:value-of select="$locale/redirect/description/text()" disable-output-escaping="yes"/></div>
                <div style="margin-top:5px"><a href="http://{/content/documents/data/redirect}?base.startpage"><xsl:value-of select="$locale/redirect/goto_local/text()" disable-output-escaping="yes"/></a></div>
                <div><a href="?base.startpage"><xsl:value-of select="$locale/redirect/goto_inet/text()" disable-output-escaping="yes"/></a></div>
            </td>
        </tr>
    </table>
    
    </td></tr></table>

    </body>
  </html>
</xsl:template>

</xsl:stylesheet>

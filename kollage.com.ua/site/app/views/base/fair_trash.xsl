<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:exsl="http://exslt.org/common"
                extension-element-prefixes="exsl"
                version="1.0">
                
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:template match="/">
    <!--<xsl:apply-templates select="/content/global/fair_trash"/>-->
</xsl:template>

<xsl:template match="fair_trash">

    <xsl:variable name="tmpTotal">
        <xsl:for-each select="./*">
            <xsl:choose>
                <xsl:when test="product/nice_price_active = 1">
                    <item>
                        <xsl:value-of select="number(product/nice_price)* /content/global/currency/exchange_rate * @quantity"/>
                    </item>
                </xsl:when>
                <xsl:otherwise>
                    <item>
                        <xsl:choose>
                            <xsl:when test="/content/global/client/type_id = 2">
                                <xsl:value-of select="number(product/price_opt)* /content/global/currency/exchange_rate * @quantity"/>
                            </xsl:when>
                            <xsl:when test="/content/global/client/type_id = 3">
                                <xsl:value-of select="number(product/price_opt2)* /content/global/currency/exchange_rate * @quantity"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="number(product/price)* /content/global/currency/exchange_rate * @quantity"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </item>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:variable>
    <xsl:variable name="myTotal" select="exsl:node-set($tmpTotal)/*"/>
    <xsl:if test="$myTotal > 0">
        <b><xsl:value-of select="format-number( sum($myTotal), '#,##0.00' )"/>&#160;<xsl:value-of select="/content/global/currency/symbol" disable-output-escaping="yes"/></b>
    </xsl:if>
</xsl:template>

</xsl:stylesheet>

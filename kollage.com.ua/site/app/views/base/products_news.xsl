<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
                
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:template match="/">
    <xsl:apply-templates select="/content/documents/product"/>
</xsl:template>

<xsl:template match="product">
        <table width="100%" height="100%" cellspacing="0" cellpadding="0">
            <tr>
                <td class="product_title"><xsl:value-of select="name" disable-output-escaping="yes"/></td>
            </tr>
            <tr>
                <td class="products_news" valign="top">
                    <table cellspacing="0" cellpadding="0" width="100%" height="100%">
                        <tr>
                            <td height="10%" align="center">
                                <xsl:choose>
                                    <xsl:when test="images/image/tiny != ''">
                                        <img src="{images/image/tiny}" border="0"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <img src="{$views_path}/images/empty.gif" width="180" height="124" border="0"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </td>
                        </tr>
                        <tr>
                            <td height="*" valign="top">
                                <table width="100%" cellpadding="0" cellspacing="0" class="product_info" border="0">
                                    <tr>
                                	<th>
                                        <xsl:choose>
                                            <xsl:when test="nice_price_active = 1">
                                                <xsl:value-of select="format-number( nice_price*/content/global/currency/exchange_rate,'###,##0.00')"/>&#160;<xsl:value-of select="/content/global/currency/symbol" disable-output-escaping="yes"/>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:choose>
                                                    <xsl:when test="/content/global/client/type_id = 2">
                                                        <xsl:value-of select="format-number( price_opt*/content/global/currency/exchange_rate,'###,##0.00')"/>&#160;<xsl:value-of select="/content/global/currency/symbol" disable-output-escaping="yes"/>
                                                    </xsl:when>
                                                    <xsl:when test="/content/global/client/type_id = 3">
                                                        <xsl:value-of select="format-number( price_opt2*/content/global/currency/exchange_rate,'###,##0.00')"/>&#160;<xsl:value-of select="/content/global/currency/symbol" disable-output-escaping="yes"/>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <xsl:value-of select="format-number( price*/content/global/currency/exchange_rate,'###,##0.00')"/>&#160;<xsl:value-of select="/content/global/currency/symbol" disable-output-escaping="yes"/>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </th>
                                	<td align="right"><a href="?shop.product&amp;id={id}"><xsl:value-of select="$locale/shop/list/details" disable-output-escaping="yes"/></a></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="left"><xsl:value-of select="ingress" disable-output-escaping="yes"/></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
</xsl:template>

</xsl:stylesheet>

<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../layout.xsl" />

<xsl:template match="data">
	<link rel="stylesheet" href="/js/jquery/plugins/fotorama/fotorama.css" />
	<style>
		.fotorama__wrap {background: none;}
		.startpage-fotorama-wrapper {position: relative;}
		.startpage-right-barside-sn {<!-- position: absolute; --> top: -18px; right: 20px; z-index: 7;}
	</style>
	<script type="text/javascript" src="/js/jquery/plugins/fotorama/fotorama.js"></script>
	
	<script type="text/javascript" src="{$views_path}/{$controller_name}/functions.js"></script>
	
	
	<div class="startpage-fotorama-wrapper">
		<div class="startpage-fotorama">
			<img src="{$views_path}/images/startpage/14.jpg" class="hidden" alt="��������� ���������� ��� �������: �����"/>
			<img src="{$views_path}/images/startpage/01.jpg" class="hidden" alt="�����: ��������� / �����������"/>
			<img src="{$views_path}/images/startpage/02.jpg" class="hidden" alt="��������-�������: ��� ��� ���� - �����, ����������, HD �����������"/>
			<img src="{$views_path}/images/startpage/03.jpg" class="hidden" alt="����������: �� ���� �� ����������!"/>
			<img src="{$views_path}/images/startpage/04.jpg" class="hidden" alt="��������: � ������ ���� / ��������-���������� ���������"/>
			<img src="{$views_path}/images/startpage/05.jpg" class="hidden" alt="���������: ��� �����, ���������, ���������, ���������������"/>
			<img src="{$views_path}/images/startpage/06.jpg" class="hidden" alt="������ ���� �N-LINE: �� ������ �� ����!"/>
			<div data-img="{$views_path}/images/startpage/07.jpg">
				<img width="100%" src="{$views_path}/images/startpage/07.jpg" alt="���������: ����� ������������� � �����������!"/>
			</div>
		</div>           
	</div>
	<div class="startpage-fotorama-bottom2">
		<div class="startpage-fotorama-bottom-item"><a href="https://play.google.com/store/apps/details?id=com.kollageorderphoto">��������� ���������� ��� �������: <span class="startpage-fotorama-bottom-font-white">�����</span></a></div>
		<div class="startpage-fotorama-bottom-item" style="display:none"><a href="/services/article/6/16">�����: <span class="startpage-fotorama-bottom-font-white">��������� / �����������</span></a></div>
		<div class="startpage-fotorama-bottom-item" style="display:none"><a href="/shop/categories">��������-�������: <span class="startpage-fotorama-bottom-font-white">��� ��� ���� - �����, ����������, HD �����������</span></a></div>
		<div class="startpage-fotorama-bottom-item" style="display:none"><a href="/services/article/24/57">����������: <span class="startpage-fotorama-bottom-font-white">�� ���� �� ����������!</span></a></div>
		<div class="startpage-fotorama-bottom-item" style="display:none"><a href="/services/article/23/374">��������: <span class="startpage-fotorama-bottom-font-white">� ������ ���� / ��������-���������� ���������</span></a></div>
		<div class="startpage-fotorama-bottom-item" style="display:none"><a href="/services/article/18/283">���������: <span class="startpage-fotorama-bottom-font-white">��� �����, ���������, ���������, ���������������</span></a></div>
		<div class="startpage-fotorama-bottom-item" style="display:none"><a href="/photos/order">������ ���� �N-LINE: <span class="startpage-fotorama-bottom-font-white">�� ������ �� ����!</span></a></div>
		<div class="startpage-fotorama-bottom-item" style="display:none">���������: <span class="startpage-fotorama-bottom-font-white">����� ������������� � �����������!</span></div>
	</div>
	
	<div class="startpage-blocks">
			<!-- new products -->
			<div class="four-blocks-in-line">
				<h4>�������</h4>
				<div class="startpage-new-products"></div>
			</div>
			<!-- /new products -->
			
			<div class="space-between-blocks"></div>
			
			<!--  products -->
			<div class="four-blocks-in-line">
				<h4>������� �����������</h4>
				<div class="startpage-action-products"></div>
			</div>
			<!-- /new products -->
			
			<div class="space-between-blocks"></div>
			
			<!--  services -->
			<div class="four-blocks-in-line">
				<h4>����������� �����</h4>
				<div class="startpage-banners-services"></div>
			</div>
			<!-- /services -->
			
			<div class="space-between-blocks"></div>
			
			<!--  actions -->
			<div class="four-blocks-in-line">
				<h4>�����</h4>
				<div class="startpage-banners-actions"></div>
			</div>
			<!-- /actions -->
		
			<div class="clear-both"></div>
			<script type="text/javascript">
				$j('.startpage-new-products').kNewProductsBox();
				$j('.startpage-action-products').kActionProductsBox();
				$j('.startpage-banners-services').kBannerBox({filter:1});
				$j('.startpage-banners-actions').kBannerBox({filter:2});
				
				Startpage.init();
			</script>
	</div>
	
	<table width="100%"><tr>
		<td align="left" valign="top" width="50%"><xsl:value-of select="substring-before(homepage/article/text_transformed,'{{BREAK}}')" disable-output-escaping="yes"/></td>
		<td align="left" valign="top" width="50%"><xsl:value-of select="substring-after(homepage/article/text_transformed,'{{BREAK}}')" disable-output-escaping="yes"/></td>
	</tr></table>
		<div class="startpage-right-barside-sn">
			<div class="left-barside-sn">
				<xsl:variable name="share_title" select="article/header"/>
				<xsl:variable name="share_url">http://www.kollage.com.ua/<xsl:value-of select="article/id"/></xsl:variable>

				<div class="header-left-barside-sn">����������:</div>
				<a href="http://vkontakte.ru/share.php?url={$share_url}&amp;title={$share_title}&amp;description=&amp;noparse=true" target="_blank" class="sn-vk vtip" title="������������ ������ ���������">&#160;</a>
				<a href="http://www.facebook.com/sharer.php?s=100&amp;p[title]={$share_title}&amp;p[url]={$share_url}&amp;p[summary]=" target="_blank" class="sn-fb vtip" title="������������ ������ � Facebook">&#160;</a>
				<a href="https://plus.google.com/share?url={$share_url}" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=700,width=600');return false;" class="sn-gl vtip" title="������������ ������ Google+">&#160;</a>
				
			</div>
			<div class="page-clear-both"></div>
		</div>
	
</xsl:template>

</xsl:stylesheet>

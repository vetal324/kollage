<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
                
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:template match="/">
  <html>
    <head>
      <title><xsl:value-of select="$locale/common/title/text()" disable-output-escaping="yes"/></title>
      <link rel="stylesheet" href="{$views_path}/styles.css" />
      <script language="JavaScript" src="{$views_path}/menu.js"></script>
      <script language="JavaScript" src="{$views_path}/shared/swfobject/swfobject.js"></script>
    </head>
    <body>

    <table width="100%" height="100%" cellpadding="0" cellspacing="0"><tr><td align="center" valign="middle">
    
    <table width="640" height="480" align="center" class="page" cellpadding="0" cellspacing="0">
        <tr>
            <td valign="top">
                <script type="text/javascript">
                    swfobject.embedSWF('mediaplayer.swf', 
                        'website', 
                        '640', 
                        '480', 
                        '9.0.45', 
                        '', 
                        { file:"intro.flv", image:"", autostart: "true" }, 
                        {bgcolor: '#ffffff', menu: 'false', wmode: 'opaque'}, 
                        {id: 'website'});
                </script>
                <div id="website">
                    <p>����� ����������� ������ �������� ��� ������� ������ ����� ������������� Flash Player 9+!</p>
                    <p>
                        <a href="http://www.adobe.com/go/getflashplayer">
                            <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
                        </a>
                    </p>
                </div>
            </td>
        </tr>
    </table>
    
    </td></tr></table>

    </body>
  </html>
</xsl:template>

</xsl:stylesheet>

<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../layout.xsl" />
<xsl:include href="../fine-uploader.xsl" />

<xsl:template match="data">
	<xsl:call-template name="fine_uploader_layouts"/>

	<xsl:if test="/content/global/client/id = 0">
		<script type="text/javascript">
		$j(function(){
			Events.publish('/Login/Show');
		});
		</script>
	</xsl:if>
	
	<xsl:if test="/content/global/client/id > 0">
		<script type="text/javascript" src="{$views_path}/{$controller_name}/functions.js?20200322"></script>
		
		<link rel="stylesheet" href="/js/swfupload/styles.css" />
		<script src="/js/swfupload/swfupload.js"></script>
		<script src="/js/swfupload/swfupload.queue.js"></script>
		<script src="/js/swfupload/swfupload.speed.js"></script>
		<script src="/js/swfupload/fileprogress.js"></script>
		<script src="/js/swfupload/handlers.js"></script>  
		
		<div class="article-wrapper">
			<div class="article-content">
				<div id="account-welcome" class="panel">
					<div class="page_title"><h1>Персональный кабинет</h1></div>
					<p>Приветствуем Вас в персональном кабинете. Выбирете раздел в меню слева.</p>
				</div>
				
				<div id="account-my-properties" class="panel hidden">
					<div class="page_title"><h1>Настройки</h1></div>
					
					<form method="post" action="/base/my_properties_save">
						<p class="message red"><xsl:value-of select="change_properties/message"/></p>
					<table cellpadding="0" cellspacing="3">
						<tr><td><label for="firstname"><xsl:value-of select="$locale/my/properties/firstname/text()" disable-output-escaping="yes"/></label></td></tr>
						<tr><td><input type="text" id="firstname" name="firstname" class="inputbox_middle" value="{/content/global/client/firstname}"/></td></tr>
						
						<tr><td><label for="lastname"><xsl:value-of select="$locale/my/properties/lastname/text()" disable-output-escaping="yes"/></label></td></tr>
						<tr><td><input type="text" id="lastname" name="lastname" class="inputbox_middle" value="{/content/global/client/lastname}"/></td></tr>
						
						<tr><td><label for="email"><xsl:value-of select="$locale/my/properties/email/text()" disable-output-escaping="yes"/></label></td></tr>
						<tr><td><input type="text" id="email" name="email" class="inputbox_middle" value="{/content/global/client/email}"/></td></tr>
						
						<tr><td><label for="country"><xsl:value-of select="$locale/my/properties/country/text()" disable-output-escaping="yes"/></label></td></tr>
						<tr><td>
							<select name="country" class="middle" id="type_id">
								<xsl:variable name="selected" select="/content/global/client/country/id"/>
								<xsl:for-each select="/content/global/countries/*">
									<xsl:call-template name="gen-option">
										<xsl:with-param name="value" select="id"/>
										<xsl:with-param name="title" select="name"/>
										<xsl:with-param name="selected" select="$selected"/>
									</xsl:call-template>
								</xsl:for-each>
							</select>
						</td></tr>
						
						<tr><td><label for="phone"><xsl:value-of select="$locale/my/properties/phone/text()" disable-output-escaping="yes"/></label></td></tr>
						<tr><td><input type="text" id="phone" name="phone" class="inputbox_middle" value="{/content/global/client/phone}"/></td></tr>
						
						<tr><td><label><xsl:value-of select="$locale/my/properties/send_news/text()" disable-output-escaping="yes"/></label></td></tr>
						<tr><td>
							<xsl:choose>
								<xsl:when test="/content/global/client/send_news = 1"><input type="radio" name="send_news" id="send_news_active" value="1" checked="yes" /></xsl:when>
								<xsl:otherwise><input type="radio" name="send_news" id="send_news_active" value="1" /></xsl:otherwise>
							</xsl:choose>
							<label for="send_news_active" class="lower"><xsl:value-of select="$locale/common/yes/text()"/></label>
							<xsl:choose>
								<xsl:when test="/content/global/client/send_news = 0"><input type="radio" name="send_news" id="send_news_inactive" value="0" checked="yes" /></xsl:when>
								<xsl:otherwise><input type="radio" name="send_news" id="send_news_inactive" value="0"/></xsl:otherwise>
							</xsl:choose>
							<label for="send_news_inactive" class="lower"><xsl:value-of select="$locale/common/no/text()"/></label>
						</td></tr>
						
						<tr>
							<td colspan="2" style="padding: 5px 0 5px 0;"><input type="submit" value="{$locale/my/properties/form_submit/text()}" class="button"/></td>
						</tr>
					</table>
					</form>
					
					<p></p>
					<div class="page_title"><h1>Смена пароля</h1></div>
					
					<form method="post" action="/base/my_properties_change_password">
					<p class="message red"><xsl:value-of select="change_password/message"/></p>
					<table cellpadding="0" cellspacing="3">
						<tr><td><label for="old_password"><xsl:value-of select="$locale/my/properties/change_password/old_password/text()" disable-output-escaping="yes"/></label></td></tr>
						<tr><td><input type="password" id="old_password" name="old_password" class="inputbox_middle" /></td></tr>
						
						<tr><td><label for="new_password"><xsl:value-of select="$locale/my/properties/change_password/new_password/text()" disable-output-escaping="yes"/></label></td></tr>
						<tr><td><input type="password" id="new_password" name="new_password" class="inputbox_middle" /></td></tr>
						
						<tr><td><label for="confirm_new_password"><xsl:value-of select="$locale/my/properties/change_password/confirm_new_password/text()" disable-output-escaping="yes"/></label></td></tr>
						<tr><td><input type="password" id="confirm_new_password" name="confirm_new_password" class="inputbox_middle" onchange="CheckPswd(this, 'new_password', 'info')"/></td></tr>
						
						<tr>
							<td colspan="2" style="padding: 5px 0 5px 0;"><input type="submit" value="{$locale/my/properties/form_submit/text()}" class="button"/></td>
						</tr>
					</table>
					</form>
				</div>
				
				<div id="account-my-purse" class="panel hidden">
					<div class="page_title"><h1>Операции по электронному кошельку</h1></div>
					<xsl:choose>
						<xsl:when test="count(purse/*)">
							<table cellpadding="3" cellspacing="1" class="account_table">
							
							<tr>
								<th width="20%"><xsl:value-of select="$locale/my/purse/created_at/text()"/></th>
								<th width="20%"><xsl:value-of select="$locale/my/purse/amount/text()"/></th>
								<th width="*"><xsl:value-of select="$locale/my/purse/description/text()"/></th>
							</tr>
							
							<xsl:apply-templates select="purse"/>
							
							</table>
						</xsl:when>
						<xsl:otherwise>
							У Вас нет операция на данный момент
						</xsl:otherwise>
					</xsl:choose>
				</div>
				
				<xsl:if test="/content/global/client/allow_userfile = 1">
				<div id="account-my-files" class="panel hidden">
					<div class="page_title"><h1><xsl:value-of select="$locale/my/files/title/text()"/></h1></div>
					<div><input type="button" class="button" onclick="ShowMyFilesUploadForm()" id="my-files-add" value="{$locale/my/files/list/upload}"/></div>
					<table class="account_table" cellspacing="0" cellpadding="0">
						<tr>
							<th width="2%">#</th>
							<th width="20%"><xsl:value-of select="$locale/my/files/list/name"/></th>
							<th width="*"><xsl:value-of select="$locale/my/files/list/description"/></th>
							<th width="20%"><xsl:value-of select="$locale/my/files/list/created_at"/></th>
							<th width="2%">&#160;</th>
						</tr>
						
						<xsl:choose>
							<xsl:when test="count(userfiles/userfile) > 0">
								<xsl:apply-templates select="userfiles/userfile"/>
							</xsl:when>
							<xsl:otherwise>
								<tr><td colspan="5" class="nofiles"><xsl:value-of select="$locale/my/files/list/nofiles"/></td></tr>
							</xsl:otherwise>
						</xsl:choose>
						
						<tfoot>
							<tr>
								<th colspan="5" align="left"><input type="button" class="button" onclick="ShowMyFilesUploadForm()" id="my-files-add" value="{$locale/my/files/list/upload}"/></th>
							</tr>
						</tfoot>
					</table>
				</div>
				</xsl:if>
				
				<div id="account-my-orders-photo" class="panel hidden">
					<div class="page_title"><h1>Заказы фотографий</h1></div>
					<xsl:choose>
						<xsl:when test="count(orders_photo/*)">
							<table cellpadding="3" cellspacing="1" class="account_table">
							
							<tr>
								<th width="20"><xsl:value-of select="$locale/my/orders/photo/n/text()"/></th>
								<th width="128"><xsl:value-of select="$locale/my/orders/photo/number/text()"/></th>
								<th width="90"><xsl:value-of select="$locale/my/orders/photo/status/text()"/></th>
								<th width="*"><xsl:value-of select="$locale/my/orders/photo/comments/text()"/></th>
								<th width="80"><xsl:value-of select="$locale/my/orders/photo/total_price/text()"/></th>
							</tr>
							
							<xsl:apply-templates select="orders_photo"/>
							
							</table>
						</xsl:when>
						<xsl:otherwise>
							У Вас нет заказов на данный момент
						</xsl:otherwise>
					</xsl:choose>
				</div>
				
				<div id="account-my-orders-shop" class="panel hidden">
					<div class="page_title"><h1>Заказы товаров</h1></div>
					<xsl:choose>
						<xsl:when test="count(orders_shop/*)">
							<table cellpadding="3" cellspacing="1" class="account_table">
							
							<tr>
								<th width="20"><xsl:value-of select="$locale/my/orders/photo/n/text()"/></th>
								<th width="128"><xsl:value-of select="$locale/my/orders/photo/number/text()"/></th>
								<th width="90"><xsl:value-of select="$locale/my/orders/photo/status/text()"/></th>
								<th width="*"><xsl:value-of select="$locale/my/orders/photo/comments/text()"/></th>
								<th width="80"><xsl:value-of select="$locale/my/orders/shop/total_price/text()"/>, грн</th>
							</tr>
							
							<xsl:apply-templates select="orders_shop"/>
							
							</table>
						</xsl:when>
						<xsl:otherwise>
							У Вас нет заказов на данный момент
						</xsl:otherwise>
					</xsl:choose>
				</div>
				
				<div id="account-my-orders-book" class="panel hidden">
					<div class="page_title"><h1>Заказы Фотокниг</h1></div>
					<xsl:choose>
						<xsl:when test="count(printbook/*)">
							<table cellpadding="3" cellspacing="1" class="account_table">
							
							<tr>
								<th width="20"><xsl:value-of select="$locale/my/orders/photo/n/text()"/></th>
								<th width="128"><xsl:value-of select="$locale/my/orders/photo/number/text()"/></th>
								<th width="90"><xsl:value-of select="$locale/my/orders/photo/status/text()"/></th>
								<th width="*"><xsl:value-of select="$locale/my/orders/photo/comments/text()"/></th>
								<th width="80"><xsl:value-of select="$locale/my/orders/shop/total_price/text()"/>, грн</th>
							</tr>
							
							<xsl:apply-templates select="printbook"/>
							
							</table>
						</xsl:when>
						<xsl:otherwise>
							У Вас нет заказов на данный момент
						</xsl:otherwise>
					</xsl:choose>
				</div>
				
			</div>
		</div>
		<div class="left-barside">
			<ul class="leftmenu">
				<li class="leftmenu-submenu"><a href="#my-properties" id="account-my-properties"><span>Настройки</span></a></li>
				
				<li class="leftmenu-submenu"><span>Заказы</span>
					<ul>
						<li><a href="#orders-photo" id="account-my-orders-photo">Заказы фотографий</a></li>
						<li><a href="#orders-book" id="account-my-orders-book">Заказы фотокниг</a></li>
						<li><a href="#orders-shop" id="account-my-orders-shop">Заказы товаров</a></li>
					</ul>
				</li>
				
				<xsl:if test="/content/global/client/allow_userfile = 1">
					<li class="leftmenu-submenu"><a href="#my-files" id="account-my-files"><span>Мои файлы</span></a></li>
				</xsl:if>
				
				<li class="leftmenu-submenu"><a href="#my-purse" id="account-my-purse"><span>Электронный кошелек</span></a></li>
			</ul>
		</div>
		<div class="page-clear-both"></div>

		<div id="dlg-add-my-files-container" class="hidden" title="Загрузка файлов">
			<div id="dlg-content">
				<table cellpadding="10" cellspacing="0" class="content_table" width="100%" height="100%">
					<tr>
						<td width="*" valign="top">
							<xsl:value-of select="$locale/my/files/upload/help/text()" disable-output-escaping="yes"/>
							<div>
								<div><xsl:value-of select="$locale/my/files/upload/apply_parameters/text()" disable-output-escaping="yes"/></div>
								<table cellspacing="0" cellpadding="0" style="margin-top:5px">
								<tr>
									<td>
										<textarea rows="5" cols="30" id="userfiles_description"></textarea>
									</td>
								</tr>
								</table>
							</div>
						</td>
						<td width="420" valign="top">
							<div id="fine-uploader-manual-trigger"></div>
						</td>
					</tr>
				</table>
			</div>
		</div>

		<script type="text/javascript">
		Account.init();
		
		$j(function(){
			$j('#dlg-add-my-files-container').dialog({
				autoOpen: false,
				resizable: false,
				minHeight: 400,
				minWidth: 820,
				modal: true,
				close: function(event, ui){
					document.location.reload();
				}
			});
		});
		
		function ShowMyFilesUploadForm()
		{
			$j('#dlg-add-my-files-container').dialog('open');
			var uploader = FineUploaderWrapper.init({
				request: {
					endpoint: '/base/add_userfiles_fine_uploader',
				},
				validation: {
					allowedExtensions: ['jpeg', 'jpg', 'zip', 'rar', 'cdr', 'ai'],
					sizeLimit: 900 * 1024 * 1024
				},
			});
			if (uploader) {
				$j('#fine-uploader-trigger-upload').click(function() {
					uploader.setParams({
						description: document.getElementById("userfiles_description").value
					});
					uploader.uploadStoredFiles();
				});
			}
		}
		</script>
		
		<xsl:if test="active_panel">
			<script type="text/javascript">
				<xsl:if test="active_panel != ''">
					Account.hidePanels();
					Account.showPanel('<xsl:value-of select="active_panel"/>');
				</xsl:if>
				<xsl:if test="active_panel = ''">
					Account.hidePanels();
					Account.showPanel('div#account-my-properties');
				</xsl:if>
			</script>
		</xsl:if>
		
	</xsl:if>
	
</xsl:template>

<xsl:template match="userfile">
	<tr>
		<td><input type="hidden" name="file_id{position()}" id="file_id{position()}" value="{id}"/><input type="checkbox" name="file_cb{position()}" id="file_cb{position()}"/></td>
		<td><a href="/base/download_userfile?id={id}"><xsl:value-of select="name"/></a></td>
		<td><xsl:value-of select="description"/></td>
		<td><xsl:value-of select="created_at"/></td>
		<td><input type="button" onclick="document.location='/base/del_userfile?id={id}'" class="button" value="{$locale/common/buttons/form_del}"/></td>
	</tr>
</xsl:template>

<xsl:template match="purse/purse_operation">
	<tr>
		<td><xsl:value-of select="created_at"/></td>
		<td><xsl:value-of select="amount"/></td>
		<td><xsl:value-of select="description"/></td>
	</tr>
</xsl:template>

<xsl:template match="orders_photo/order">
<tr>
	<td><xsl:value-of select="position()"/>.</td>
	<td><b><xsl:value-of select="substring(order_number,1,10)"/></b><br/><xsl:value-of select="created_at"/></td>
	<td>
		<xsl:choose>
			<xsl:when test="status = 1">
				<xsl:value-of select="$locale/my/orders/photo/statuses/s1/text()"/>
			</xsl:when>
			<xsl:when test="status = 2">
				<xsl:value-of select="$locale/my/orders/photo/statuses/s2/text()"/>
			</xsl:when>
			<xsl:when test="status = 3">
				<xsl:value-of select="$locale/my/orders/photo/statuses/s3/text()"/>
			</xsl:when>
			<xsl:when test="status = 4">
				<xsl:value-of select="$locale/my/orders/photo/statuses/s4/text()"/>
			</xsl:when>
			<xsl:when test="status = 5">
				<xsl:value-of select="$locale/my/orders/photo/statuses/s5/text()"/>
			</xsl:when>
		</xsl:choose>
	</td>
	<td>
		<xsl:value-of select="$locale/my/orders/photo/delivery/text()"/>: <xsl:value-of select="delivery_name"/>;
		
		<xsl:if test="delivery != '0.00' or photos_count > 100">
		<xsl:value-of select="$locale/my/orders/photo/address/text()"/>: 
		<xsl:choose>
			<xsl:when test="address != ''">
				<xsl:value-of select="address"/>;
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="/content/global/client/address"/>;
			</xsl:otherwise>
		</xsl:choose>
		</xsl:if>
		
		<xsl:if test="delivery = '0.00' and photos_count &lt; 100">
		<xsl:value-of select="$locale/my/orders/photo/point/text()"/>: <xsl:value-of select="point_address"/>;
		</xsl:if>
		
	</td>
	<td align="right"><xsl:value-of select="total_price"/></td>
</tr>
</xsl:template>

<xsl:template match="orders_shop/order">
<tr>
	<td><xsl:value-of select="position()"/>.</td>
	<td><b><xsl:value-of select="substring(order_number,1,10)"/></b><br/><xsl:value-of select="created_at"/></td>
	<td>
		<xsl:choose>
			<xsl:when test="status = 1">
				<xsl:value-of select="$locale/my/orders/shop/statuses/s1/text()"/>
			</xsl:when>
			<xsl:when test="status = 2">
				<xsl:value-of select="$locale/my/orders/shop/statuses/s2/text()"/>
			</xsl:when>
			<xsl:when test="status = 3">
				<xsl:value-of select="$locale/my/orders/shop/statuses/s3/text()"/>
			</xsl:when>
			<xsl:when test="status = 4">
				<xsl:value-of select="$locale/my/orders/shop/statuses/s4/text()"/>
			</xsl:when>
			<xsl:when test="status = 5">
				<xsl:value-of select="$locale/my/orders/shop/statuses/s5/text()"/>
			</xsl:when>
			<xsl:when test="status = 99">
				<xsl:value-of select="$locale/my/orders/shop/statuses/s99/text()"/>
			</xsl:when>
		</xsl:choose>
	</td>
	<td>
		<xsl:value-of select="$locale/my/orders/photo/delivery/text()"/>: <xsl:value-of select="delivery/name"/>;
	</td>
	<td align="right"><xsl:value-of select="format-number(amount * $exchange_rate, '0.00')"/></td>
</tr>
</xsl:template>

<xsl:template match="printbook/order">
<tr>
	<td><xsl:value-of select="position()"/>.</td>
	<td><b><xsl:value-of select="substring(order_number,1,10)"/></b><br/><xsl:value-of select="created_at"/></td>
	<td>
		<xsl:choose>
			<xsl:when test="status = 1">
				<xsl:value-of select="$locale/my/orders/shop/statuses/s1/text()"/>
			</xsl:when>
			<xsl:when test="status = 2">
				<xsl:value-of select="$locale/my/orders/shop/statuses/s2/text()"/>
			</xsl:when>
			<xsl:when test="status = 3">
				<xsl:value-of select="$locale/my/orders/shop/statuses/s3/text()"/>
			</xsl:when>
			<xsl:when test="status = 4">
				<xsl:value-of select="$locale/my/orders/shop/statuses/s4/text()"/>
			</xsl:when>
			<xsl:when test="status = 5">
				<xsl:value-of select="$locale/my/orders/shop/statuses/s5/text()"/>
			</xsl:when>
			<xsl:when test="status = 99">
				<xsl:value-of select="$locale/my/orders/shop/statuses/s99/text()"/>
			</xsl:when>
		</xsl:choose>
	</td>
	<td>
		<xsl:value-of select="$locale/my/orders/photo/delivery/text()"/>: <xsl:value-of select="delivery/name"/>;
	</td>
	<td align="right"><xsl:value-of select="format-number(total_price, '0.00')"/></td>
</tr>
</xsl:template>

</xsl:stylesheet>

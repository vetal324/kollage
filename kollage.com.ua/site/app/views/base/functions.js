
var Startpage = {
	options: {
		'fotorama': '.startpage-fotorama'
	},
	
	slider: null,
	sliderShowImg: function(idx) {
		if( this.slider ) {
			this.slider.trigger('showimg', [idx,333]);
		}
	},
	
	init: function() {
		var self=this, o=this.options;
		
		$j(this.options.fotorama).on('fotorama:show', function (e, fotorama, extra) {
			$j('.startpage-fotorama-bottom2').children().hide();
		});
		$j(this.options.fotorama).on('fotorama:showend', function (e, fotorama, extra) {
			var obj = $j('.startpage-fotorama-bottom2').children()[fotorama.activeIndex];
			$j(obj).fadeIn();
		});
		this.slider = $j(this.options.fotorama).fotorama({
			'captions': false,
			'autoplay': true,
			'navBackground': '#0a0b0c',
			'alwaysPadding': false,
			'nav': 'none',
			'width': '100%',
			'transition': 'slide',
			'click': false
		});
		
		//this.Thumbnails.init();
	},
	
	Thumbnails: {
		options: {
			'boxSelector': '.startpage-thumbnails',
			'sliderSelector': '.startpage-fotorama .fotorama__wrap',
			'coverOpacity': 0.5
		},
		
		oW: null,
		oH: null,
		
		init: function() {
			var self=this, o=this.options;
			$j(window).bind( 'resize', function(){
				self.hideThumbnails();
			});
			this.applyCovers();
		},
		
		sliderOnShowImg: function() {
			var self=this, o=this.options;
			
			var sizes = self.getSize();
			if( sizes['fH'] != sizes['h'] ) {
				$j(o.boxSelector).children().height( sizes['h'] );
				$j(o.boxSelector).children().width( sizes['w'] );
				$j(o.boxSelector).children().hide();
				$j(o.boxSelector).children().fadeIn();
			}
		},
		
		getSize: function() {
			var retval = {
				'w': null,
				'h': null,
				'fW': null,
				'fH': null
			};
			
			var self=this, o=this.options;
			var w = $j(o.boxSelector).width() -1;
			var h = $j(o.boxSelector).parent().find(o.sliderSelector).height();
			
			retval['w'] = Math.floor(w/2) - 3;
			retval['h'] = Math.floor(h/3) - 2;
			retval['fW'] = $j(o.boxSelector).children().first().width();
			retval['fH'] = $j(o.boxSelector).children().first().height();
			
			return( retval );
		},
		
		hideThumbnails: function() {
			var self=this, o=this.options;
			var sizes = self.getSize();
			if( sizes['fH'] != sizes['h'] ) {
				console.log('hide');
				$j(o.boxSelector).children().hide();
			}
			else {
			}
		},
		
		applyCovers: function() {
			var self=this, o=this.options;
			
			$j(o.boxSelector).children().each( function(idx) {
				var cover = $j( '<div class="img-cover" rel="'+ idx +'"></div>' ).css('opacity', o.coverOpacity);
				cover.click( function(){
					var idx = parseInt( $j(this).attr('rel') );
					if( idx >= 0 ) {
						Startpage.sliderShowImg(idx);
					}
				} );
				cover.hover(function( e ) {
					$j(this).css( 'opacity', 0 );
				}, function( e ) {
					$j(this).css( 'opacity', o.coverOpacity );
				});
				$j(this).children().first().before( cover );
			});
		}
	}
};

var Account = {
	options: {
		panel: 'div.panel',
		
		tabs: [
			{
				panel: '#account-welcome',
				link: null,
				suffix: 'welcome'
			},
			{
				panel: 'div#account-my-files',
				link: 'a#account-my-files',
				suffix: 'my-files'
			},
			{
				panel: 'div#account-my-purse',
				link: 'a#account-my-purse',
				suffix: 'my-purse'
			},
			{
				panel: 'div#account-my-properties',
				link: 'a#account-my-properties',
				suffix: 'my-properties'
			},
			{
				panel: 'div#account-my-orders-photo',
				link: 'a#account-my-orders-photo',
				suffix: 'orders-photo'
			},
			{
				panel: 'div#account-my-orders-shop',
				link: 'a#account-my-orders-shop',
				suffix: 'orders-shop'
			},
			{
				panel: 'div#account-my-orders-book',
				link: 'a#account-my-orders-book',
				suffix: 'orders-shop'
			}
		]
	},
	
	init: function() {
		var self=this, o=this.options;
		
		for( var i=0; i<o.tabs.length; i++ ) {
			var t = o.tabs[i];
			$j(t.link).data( 'o', t );
			$j(t.link).click(function(){
				self.hidePanels();
				self.showPanel($j(this).data('o').panel);
			});
		}
		
		var id = window.location.hash || '';
		if( id ) {
			this.hidePanels();
			id = id.replace( '#', '' ).toLowerCase();
			for( var i=0; i<o.tabs.length; i++ ) {
				var t = o.tabs[i];
				if( id == t.suffix ) {
					this.showPanel( t.panel );
					break;
				}
			}
		}
		else {
			this.showPanel( o.tabs[0].panel );
		}
	},
	
	hidePanels: function() {
		var self=this, o=this.options;
		
		$j(o.panel).hide();
	},
	
	showPanel: function( id ) {
		var self=this, o=this.options;
		
		$j(id).show();
	}
};


var AnyRequest = {
	$: jQuery,
	
	options: {
		dialog: '#dlg-any-request-container',
		dialogContent: '#dlg-content',
		dialogLoader: '.dlg-footer-loader',
		
		protectImage: 'img.protect_image',
		reload: '.btn-reload',
		form: {
			name: 'input[name="name"]',
			phone: 'input[name="phone"]',
			email: 'input[name="email"]',
			comment: 'textarea[name="comment"]',
			protect_code: 'input[name="protect_code"]'
		},
		
		dialogFooter: '#dlg-footer',
		dialogBtnSend: '.dlg-btn-send'
	},
		
	init: function() {
		var self = this, o = this.options;
		
        self._applyDialog()
        
		/*
        this.$(o.dialog).dialog({
			autoOpen: false,
			resizable: true,
			minHeight: 160,
			minWidth: 380,
			modal: true,
			open: function(event, ui){
				self._applyDialog();
			}
		});
		
		this.$(o.dialogBtnClose, o.dialog).click(function(){
			self._closeDlg();
		});
		
		Events.subscribe( '/RequestProduct/Show', function(info) {
			self.info = info;
			self._showDlg();
		});
        */
	},
	
	/*_showDlg: function(){
		var o=this.options;
		this.$(o.dialog).dialog('open');
	},
	
	_closeDlg: function(){
		var o=this.options;
		this.$(o.dialog).dialog("close");
	},*/
	
	_applyDialog: function() {
		var self=this, o=this.options;
		
		this._resetDialog();
		
		var img = this.$(o.protectImage,o.dialog).first();
		img.attr( 'src', '/base/get_any_request_protect_image_new?'+ Tools.getTimestamp() );
		var reload = this.$(o.reload,o.dialog).first();
		reload.click(function(){
			img.attr( 'src', '/app/views/images/loader1.gif' );
			img.attr( 'src', '/base/get_any_request_protect_image_new?'+ Tools.getTimestamp() );
		});
		
		this.$.mask.definitions['9'] = '';
		this.$.mask.definitions['d'] = '[0-9]';
		this.$(o.form.phone,o.dialog).mask(' ddddddddd?d');
		this.$(o.form.email,o.dialog).keyup(function(evnt){
			var e = self.$(this);
			e.val( Tools.formatEmail(e.val()) );
		});
		
		this.$(o.dialogBtnSend, o.dialog).click(function(){
			self.send();
		});
	},
	
	_resetDialog: function() {
		var self=this, o=this.options;
		
		/*for( var f in o.form ) {
			this.$(o.form[f], o.dialog).val('');
		}*/
		this.$(o.form.protect_code, o.dialog).val('');
	},
	
	send: function() {
		var self=this, o=this.options;
		
		if( this._checkForm() ) {
			this._showDialogLoader();
			var fv = this._getFormItems();
			this.$.ajax({
				url: '/base/json_search_send_message',
				data: {
					name: fv['name'].val,
					phone: fv['phone'].val,
					email: fv['email'].val,
					comment: fv['comment'].val,
					protect_code: fv['protect_code'].val
				},
				dataType: 'json',
				type: 'get',
				success: function(data) {
                    var r = data.result;
					if( r == 1 ) {
                        Alert.message('Сообщение отправлено.');
                        $j(o.form.comment,o.dialog).val('');
                        $j(o.form.protect_code,o.dialog).val('');
                        var img = $j(o.protectImage,o.dialog).first();
                        img.attr( 'src', '/app/views/images/loader1.gif' );
                        img.attr( 'src', '/base/get_any_request_protect_image_new?'+ Tools.getTimestamp() );
					}
					else if( r == -1 ) Alert.error('Ошибка параметров.<br/>Внимательно проверьте поля формы.');
					else if( r == -2 ) Alert.error('Ошибка проверочного кода.<br/>Вы можете обновить код, нажав кнопку справа от картинки с кодом.');
					self._hideDialogLoader();
				}
			});
		}
	},
	
	_checkForm: function() {
		var self=this, o=this.options;
		var retval = true;
		
		var fv = this._getFormItems();
		
		if( !fv['name'].val ) {
			fv['name'].obj.addClass('inp-error');
			retval = false;
		}
        else fv['name'].obj.removeClass('inp-error');
		
		if( !fv['phone'].val ) {
			fv['phone'].obj.addClass('inp-error');
			retval = false;
		}
		else fv['phone'].obj.removeClass('inp-error');
		
		/*if( !fv['email'].val ) {
			fv['email'].obj.addClass('inp-error');
			retval = false;
		}
		else fv['email'].obj.removeClass('inp-error');*/
		
		if( !fv['comment'].val ) {
			fv['comment'].obj.addClass('inp-error');
			retval = false;
		}
		else fv['comment'].obj.removeClass('inp-error');
		
		if( !fv['protect_code'].val ) {
			fv['protect_code'].obj.addClass('inp-error');
			retval = false;
		}
		else fv['protect_code'].obj.removeClass('inp-error');
		
		return( retval );
	},
	
	_getFormItems: function() {
		var self=this, o=this.options;
		var retval = [];
		
		var name = this.$(o.form.name,o.dialog);
		var nameVal = this.$.trim( name.val() );
		retval['name'] = {
			obj: name,
			val: nameVal
		};
		
		var email = this.$(o.form.email,o.dialog);
		var emailVal = this.$.trim( email.val() );
		retval['email'] = {
			obj: email,
			val: Tools.formatEmail(emailVal)
		};
		
		var phone = this.$(o.form.phone,o.dialog);
		var phoneVal = this.$.trim( phone.val() );
		retval['phone'] = {
			obj: phone,
			val: Tools.formatPhone(phoneVal)
		};
		
		var comment = this.$(o.form.comment,o.dialog);
		var commentVal = this.$.trim( comment.val() );
		retval['comment'] = {
			obj: comment,
			val: commentVal
		};
		
		var protect_code = this.$(o.form.protect_code,o.dialog);
		var protect_codeVal = this.$.trim( protect_code.val() );
		retval['protect_code'] = {
			obj: protect_code,
			val: protect_codeVal
		};
		
		return( retval );
	},
	
	_showDialogLoader: function() {
		var self = this, o = this.options;
		this.$(o.dialogLoader, o.dialog).show();
	},
	_hideDialogLoader: function() {
		var self = this, o = this.options;
		this.$(o.dialogLoader, o.dialog).hide();
	}
};

$j(document).ready(function(){
	AnyRequest.init();
});
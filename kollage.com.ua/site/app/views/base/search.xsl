<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../layout.xsl" />

<xsl:template match="data">
    <script type="text/javascript" src="{$views_path}/{$controller_name}/functions.js"></script>
	<div class="article-wrapper">
		<div class="article-content">
			<div class="page_title"><h1><span class="red"><xsl:value-of select="$locale/common/search_results_title/text()" disable-output-escaping="yes"/></span></h1></div>
				
				<xsl:choose>
					<xsl:when test="ord=2">
						<xsl:if test="count(articles/*) > 0">
							<h2 class="blue"><xsl:value-of select="$locale/common/search_results_title_articles/text()" disable-output-escaping="yes"/></h2>
							<ol class="search-results"><xsl:apply-templates select="articles/*"/></ol>
						</xsl:if>
						<xsl:if test="count(products/*) > 0">
							<h2 class="blue"><xsl:value-of select="$locale/common/search_results_title_products/text()" disable-output-escaping="yes"/></h2>
							<ol class="search-results"><xsl:apply-templates select="products/*"/></ol>
						</xsl:if>
					</xsl:when>
					<xsl:otherwise>
						<xsl:if test="count(products/*) > 0">
							<h2 class="blue"><xsl:value-of select="$locale/common/search_results_title_products/text()" disable-output-escaping="yes"/></h2>
							<ol class="search-results"><xsl:apply-templates select="products/*"/></ol>
						</xsl:if>
						<xsl:if test="count(articles/*) > 0">
							<h2 class="blue"><xsl:value-of select="$locale/common/search_results_title_articles/text()" disable-output-escaping="yes"/></h2>
							<ol class="search-results"><xsl:apply-templates select="articles/*"/></ol>
						</xsl:if>
					</xsl:otherwise>
				</xsl:choose>
				
				<xsl:if test="count(products/*)=0 and count(articles/*)=0">
                    <div class="search-results"><xsl:value-of select="$locale/common/search_no_results/text()" disable-output-escaping="yes"/></div>
                    <br />
					<!-- Any Request Container -->
                    <div id="dlg-any-request-container" class="search-results" title="������ ������" style="width:60%">
                        <div id="dlg-content">
                            <h2>��������� � ��������� ������ ����� � ��� �������� �������� � ����.</h2>
                            <div class="form">
                                <div class="row">
                                    <label>���</label>
                                    <input type="text" name="name"/>
                                </div>
                                <div class="row">
                                    <label>��������� �������</label>
                                    <input type="text" name="phone"/>
                                </div>
                                <div class="row">
                                    <label>E-mail</label>
                                    <input type="text" name="email"/>
                                </div>
                                <div class="row">
                                    <label>����� ���������</label>
                                    <textarea rows="6" name="comment"/>
                                </div>
                                <div class="row">
                                    <label>������� ������� � ��������</label>
                                    <img src="/app/views/images/loader1.gif" class="protect_image"/><div class="btn-reload"></div>
                                </div>
                                <div class="row">
                                    <input type="text" name="protect_code"/>
                                </div>
                            </div>
                        </div>
                        <div id="dlg-footer">
                            <div class="dlg-footer-loader hidden">
                                <div class="loader1"></div>
                            </div>
                            <div class="dlg-footer-buttons">
                                <a class="dlg-btn-send"></a>
                            </div>
                            <div class="clear-both"></div>
                        </div>
                    </div>
                    <!-- Any Request Container -->
				</xsl:if>
			
		</div>
	</div>
    <div class="page-clear-both"></div>
	
</xsl:template>

<xsl:template match="product">
	<li><a href="/shop/product/{id}" target="_blank"><xsl:value-of select="name" disable-output-escaping="yes"/></a></li>
</xsl:template>

<xsl:template match="article">
	<xsl:choose>
		<xsl:when test="category_id=12">
			<li><a href="/photos/article/{id}" target="_blank"><xsl:value-of select="header" disable-output-escaping="yes"/></a></li>
		</xsl:when>
		<xsl:otherwise>
			<li><a href="/services/article/{category_id}/{id}" target="_blank"><xsl:value-of select="header" disable-output-escaping="yes"/></a></li>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

</xsl:stylesheet>

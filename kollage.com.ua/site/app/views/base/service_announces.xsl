<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
                
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="../common.xsl" />
<xsl:include href="../locale.xsl" />
<xsl:include href="../menu.xsl" />

<xsl:template match="/">
    <xsl:apply-templates select="/content/documents/service_announces"/>
</xsl:template>

<xsl:template match="/content/documents/service_announces/image">
        <table width="100%" height="100%" cellspacing="0" cellpadding="0" class="service_announces">
            <!--
            <tr>
                <td class="product_title"><xsl:value-of select="name" disable-output-escaping="yes"/></td>
            </tr>
            -->
            <!--
            <tr>
                <td class="product_title"><xsl:value-of select="$locale/services/service_announces/title/text()" disable-output-escaping="yes"/></td>
            </tr>
            -->
            <tr>
                <td class="service_announces" valign="top">
                    <table cellspacing="0" cellpadding="0" width="100%" height="100%">
                        <tr>
                            <td height="10%" align="center">
                                <xsl:choose>
                                    <xsl:when test="tiny != ''">
                                        <img src="{tiny}" border="0" vspace="0" hspace="0" style="border:0;margin:0;padding:0"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <img src="{$views_path}/images/empty.gif" width="180" height="124" border="0"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </td>
                        </tr>
                        <tr>
                            <td height="*" valign="top">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
</xsl:template>

</xsl:stylesheet>

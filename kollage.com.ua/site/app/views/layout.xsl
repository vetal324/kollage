<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output 
	media-type="text/html"
	method="html"
	encoding="utf-8"
	doctype-system=""
	doctype-public=""
	indent="no"/>

<xsl:include href="login_form.xsl" />
<xsl:include href="login_form2.xsl" />
<xsl:include href="client_form.xsl" />

<xsl:include href="common.xsl" />
<xsl:include href="locale.xsl" />
<xsl:include href="menu.xsl" />

<xsl:template match="/">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru-ru" lang="ru-ru">
	<head>
		<meta name="robots" content="index, follow" />
		<xsl:variable name="page_keywords">
		<xsl:choose>
			<xsl:when test="/content/documents/data/article/page_keywords != ''"><xsl:value-of select="/content/documents/data/article/page_keywords" disable-output-escaping="yes"/></xsl:when>
			<xsl:when test="/content/documents/data/product/page_keywords != ''"><xsl:value-of select="/content/documents/data/product/page_keywords" disable-output-escaping="yes"/></xsl:when>
			<xsl:when test="/content/documents/data/category/page_keywords != ''"><xsl:value-of select="/content/documents/data/category/page_keywords" disable-output-escaping="yes"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="$locale/common/keywords/text()" disable-output-escaping="yes"/></xsl:otherwise>
		</xsl:choose>
		</xsl:variable>
		<meta name="keywords" content="{$page_keywords}"/>
		
		<title>
		<xsl:choose>
			<xsl:when test="/content/documents/data/article/page_title != ''"><xsl:value-of select="/content/documents/data/article/page_title" disable-output-escaping="yes"/></xsl:when>
			<xsl:when test="/content/documents/data/product/page_title != ''"><xsl:value-of select="/content/documents/data/product/page_title" disable-output-escaping="yes"/></xsl:when>
			<xsl:when test="/content/documents/data/category/page_title != ''"><xsl:value-of select="/content/documents/data/category/page_title" disable-output-escaping="yes"/></xsl:when>
			<xsl:when test="/content/documents/data/article/header != ''"><xsl:value-of select="/content/documents/data/category/name"/><xsl:if test="/content/documents/data/category/name"> / </xsl:if><xsl:value-of select="/content/documents/data/article/header"/>, <xsl:value-of select="$locale/common/title" disable-output-escaping="yes"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="$locale/common/title/text()" disable-output-escaping="yes"/></xsl:otherwise>
		</xsl:choose>
		</title>
		
		<xsl:variable name="page_description">
		<xsl:choose>
			<xsl:when test="/content/documents/data/article/page_title != ''"><xsl:value-of select="/content/documents/data/article/page_title" disable-output-escaping="yes"/></xsl:when>
			<xsl:when test="/content/documents/data/product/page_title != ''"><xsl:value-of select="/content/documents/data/product/page_title" disable-output-escaping="yes"/></xsl:when>
			<xsl:when test="/content/documents/data/category/page_title != ''"><xsl:value-of select="/content/documents/data/category/page_title" disable-output-escaping="yes"/></xsl:when>
			<xsl:when test="/content/documents/data/article/header != ''"><xsl:value-of select="/content/documents/data/category/name"/><xsl:if test="/content/documents/data/category/name"> / </xsl:if><xsl:value-of select="/content/documents/data/article/header"/>, <xsl:value-of select="$locale/common/title" disable-output-escaping="yes"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="$locale/common/description/text()" disable-output-escaping="yes"/></xsl:otherwise>
		</xsl:choose>
		</xsl:variable>
		<meta name="description" content="{$page_description}"/>
		
		<script type="text/javascript" src="{$views_path}/shared/JsHttpRequest.js"></script>
		<script type="text/javascript" src="{$views_path}/shared/swfobject/swfobject.js"></script>
		
		<link rel="stylesheet" href="/js/highslide/highslide.css" type="text/css" charset="utf-8"/>
		<!--<script type="text/javascript" src="/js/highslide/highslide-with-gallery.js"></script>-->
		<script type="text/javascript" src="/js/highslide/highslide-with-gallery.packed.js"></script>
		
		<link rel="stylesheet" href="/js/jquery/css/theme2/jquery-ui-1.10.2.custom.css" type="text/css" charset="utf-8"/>
		<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/file-uploader/5.16.2/fine-uploader-new.min.css"></link>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/file-uploader/5.16.2/fine-uploader.min.js"></script>
		
		<script type="text/javascript">
		var $j = jQuery.noConflict();
		</script>
		
		<script type="text/javascript" src="/js/json-template/json-template.js"></script>
		<script type="text/javascript" src="/js/requirejs/requirejs.js"></script>
		<script type="text/javascript">
			require.config({
				baseUrl: '/js/requirejs'
			});
		</script> 
		
		<link rel="stylesheet" href="/js/jquery/plugins/alerts/jquery.alerts.css" type="text/css" charset="utf-8"/>
		<script type="text/javascript" src="/js/jquery/plugins/alerts/jquery.alerts.js"></script>
		
		<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/prototype/1.7.1.0/prototype.js"></script>
		<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/scriptaculous/1.8.3/scriptaculous.js?load=effects,dragdrop"></script>
	
		<script type="text/javascript" src="/js/pubsub.js"></script>
		<link rel="stylesheet" type="text/css" href="/js/jquery/plugins/jquery-mega-drop-down-menu/css/dcmegamenu.css"/>
		<link rel="stylesheet" type="text/css" href="/js/jquery/plugins/jquery-mega-drop-down-menu/css/skins/custom.css"/>
		<script type="text/javascript" src="/js/jquery/plugins/jquery-mega-drop-down-menu/js/jquery.hoverIntent.minified.js"></script>
		<script type="text/javascript" src="/js/jquery/plugins/jquery-mega-drop-down-menu/js/jquery.dcmegamenu.1.3.3-updated.js"></script>
		<script type="text/javascript" src="/js/jquery/plugins/masked/jquery.maskedinput.min.js"></script>
		<script type="text/javascript" src="/js/jquery/plugins/cycle/jquery.cycle.js"></script>        
		<script type="text/javascript" src="/js/jquery/plugins/jquery.cookie.js"></script>        
		<script type="text/javascript" src="/js/jquery/plugins/sticky/jquery.sticky.js"></script>

		<link rel="stylesheet" href="/fonts/zrnic/stylesheet.css" type="text/css" charset="utf-8"/>
		<link rel="stylesheet" href="/fonts/droidsans-bold/stylesheet.css" type="text/css" charset="utf-8"/>
		<link rel="stylesheet" href="/fonts/micra/stylesheet.css" type="text/css" charset="utf-8"/>
		<link href='//fonts.googleapis.com/css?family=Roboto+Condensed&amp;subset=latin,cyrillic' rel='stylesheet' type='text/css'/>
		<link href='//fonts.googleapis.com/css?family=Istok+Web:400,700&amp;subset=latin,cyrillic' rel='stylesheet' type='text/css'/>
		<link rel="stylesheet" href="{$views_path}/styles.css" type="text/css" charset="utf-8"/>
		
		<link rel="stylesheet" href="/js/vtip/css/vtip.css" type="text/css" charset="utf-8"/>
		<script type="text/javascript" src="/js/vtip/vtip.js"></script>

		<a href="https://plus.google.com/105063464548667534713" rel="publisher"/>
		
		<script type="text/javascript">
		var Site = {
			ViewsPath: '<xsl:value-of select="$views_path"/>',
			ControllerName: '<xsl:value-of select="$controller_name"/>',
			Currency: {
				Symbol: '<xsl:value-of select="/content/global/currency/symbol" disable-output-escaping="yes"/>',
				Rate: parseFloat('<xsl:value-of select="/content/global/currency/exchange_rate" disable-output-escaping="yes"/>')
			},
            CurrencyUSD: {
                    Symbol: '<xsl:value-of select="/content/global/currencies/currency[code='840']/symbol" disable-output-escaping="yes"/>',
                    Rate: parseFloat('<xsl:value-of select="/content/global/currencies/currency[code='840']/exchange_rate" disable-output-escaping="yes"/>')
                },
			BannersServices: {
				items: [
				<xsl:for-each select="/content/documents/banners_services/*">
					<xsl:if test="position()>1">,</xsl:if>{name: '<xsl:value-of select="name" disable-output-escaping="yes"/>',description: '<xsl:value-of select="description" disable-output-escaping="yes"/>',url: '<xsl:value-of select="url" disable-output-escaping="yes"/>',image: '<xsl:value-of select="$data_path"/>/<xsl:value-of select="image" disable-output-escaping="yes"/>',image_type: '<xsl:value-of select="image_type" disable-output-escaping="yes"/>',created_at: '<xsl:value-of select="created_at" disable-output-escaping="yes"/>'}
				</xsl:for-each>]
			},
			BannersActions: {
				items: [
				<xsl:for-each select="/content/documents/banners_actions/*">
					<xsl:if test="position()>1">,</xsl:if>{name: '<xsl:value-of select="name" disable-output-escaping="yes"/>',description: '<xsl:value-of select="description" disable-output-escaping="yes"/>',url: '<xsl:value-of select="url" disable-output-escaping="yes"/>',image: '<xsl:value-of select="$data_path"/>/<xsl:value-of select="image" disable-output-escaping="yes"/>',image_type: '<xsl:value-of select="image_type" disable-output-escaping="yes"/>',created_at: '<xsl:value-of select="created_at" disable-output-escaping="yes"/>'}
				</xsl:for-each>]
			},
			ProductsNew: {
				items: [
				<xsl:for-each select="/content/global/new_products/*">
					<xsl:variable name="product_price">
						<xsl:call-template name="get_product_price">
							<xsl:with-param name="product" select="."/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:if test="position()>1">,</xsl:if>{id: '<xsl:value-of select="id" disable-output-escaping="yes"/>',name: '<xsl:value-of select="name" disable-output-escaping="yes"/>',price: '<xsl:value-of select="$product_price" disable-output-escaping="yes"/>',image: {id:'<xsl:value-of select="./images[1]/image/id" disable-output-escaping="yes"/>',path:'<xsl:value-of select="$data_path"/>/',filename:'<xsl:value-of select="./images[1]/image/filename" disable-output-escaping="yes"/>',small:'<xsl:value-of select="./images[1]/image/small" disable-output-escaping="yes"/>',tiny:'<xsl:value-of select="./images[1]/image/tiny" disable-output-escaping="yes"/>',thumbnail:'<xsl:value-of select="./images[1]/image/thumbnail" disable-output-escaping="yes"/>',type:'<xsl:value-of select="./images[1]/image/type" disable-output-escaping="yes"/>'},created_at: '<xsl:value-of select="created_at" disable-output-escaping="yes"/>'}
				</xsl:for-each>]
			},
			ProductsAction: {
				items: [
				<xsl:for-each select="/content/global/action_products/*">
					<xsl:variable name="product_price">
						<xsl:call-template name="get_product_price">
							<xsl:with-param name="product" select="."/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:if test="position()>1">,</xsl:if>{id: '<xsl:value-of select="id" disable-output-escaping="yes"/>',name: '<xsl:value-of select="name" disable-output-escaping="yes"/>',price: '<xsl:value-of select="$product_price" disable-output-escaping="yes"/>',image: {id:'<xsl:value-of select="./images[1]/image/id" disable-output-escaping="yes"/>',path:'<xsl:value-of select="$data_path"/>/',filename:'<xsl:value-of select="./images[1]/image/filename" disable-output-escaping="yes"/>',small:'<xsl:value-of select="./images[1]/image/small" disable-output-escaping="yes"/>',tiny:'<xsl:value-of select="./images[1]/image/tiny" disable-output-escaping="yes"/>',thumbnail:'<xsl:value-of select="./images[1]/image/thumbnail" disable-output-escaping="yes"/>',type:'<xsl:value-of select="./images[1]/image/type" disable-output-escaping="yes"/>'},created_at: '<xsl:value-of select="created_at" disable-output-escaping="yes"/>'}
				</xsl:for-each>]
			},
			Countries: {
				items: [<xsl:for-each select="/content/global/countries/*"><xsl:if test="position()>1">,</xsl:if>{id: '<xsl:value-of select="id" disable-output-escaping="yes"/>',name: '<xsl:value-of select="name" disable-output-escaping="yes"/>',phone_code: '<xsl:value-of select="phone_code" disable-output-escaping="yes"/>'}</xsl:for-each>]
			}
		};
		var Client = {
			id: parseInt('<xsl:value-of select="/content/global/client/id"/>'),
			type: parseInt('<xsl:value-of select="/content/global/client/type_id"/>'),
			firstname: '<xsl:value-of select="/content/global/client/firstname"/>',
			lastname: '<xsl:value-of select="/content/global/client/lastname"/>',
			email: '<xsl:value-of select="/content/global/client/email"/>',
			phone: '<xsl:value-of select="/content/global/client/phone"/>',
			
			isLogged: function() {
				return( this.id>0 );
			}
		};
		</script>
		
		<script type="text/javascript" src="{$views_path}/common.js?timestamp=201909051256"></script>
	
	</head>
<body>
	<div class="header-wrapper">
		<div class="header">
			<a class="header-logo-link" href="/" title="������� �� ������� �������� �����">&#160;</a>
			<div class="header-slogan"><xsl:value-of select="$locale/common/header_slogan" disable-output-escaping="yes"/></div>
			<div class="header-main-block-wrapper">
				<div class="header-main-block">
					<div class="login-register">
					<xsl:choose>
						<xsl:when test="/content/global/client/id > 0">
							<a href="/base/account" class="link-account">
								�������
								<!--<xsl:value-of select="/content/global/client/firstname" disable-output-escaping="yes"/><xsl:if test="/content/global/client/lastname">&#160;</xsl:if><xsl:value-of select="/content/global/client/lastname" disable-output-escaping="yes"/>-->
							</a> / 
						</xsl:when>
						<xsl:otherwise><a href="#" class="link-register" onclick="return(false)">�����������</a> / </xsl:otherwise>
					</xsl:choose>
					
					<xsl:choose>
						<xsl:when test="/content/global/client/id > 0"><a href="/base/logout" class="link-logout">�����</a></xsl:when>
						<xsl:otherwise><a href="#" class="link-login" onclick="return(false)">����</a></xsl:otherwise>
					</xsl:choose>
					</div>
					<div class="header-phones">
						<div class="slideshow">
							<div class="header-phone"><div class="header-phone-where">������</div>(0542) 61-04-82</div>
							<div class="header-phone" style="display:none"><div class="header-phone-where">������</div>(066) 336-61-23</div>
							<div class="header-phone" style="display:none"><div class="header-phone-where">������</div>(097) 639-97-87</div>
							<div class="header-phone" style="display:none"><div class="header-phone-where">��������-�������</div>(0542) 61-20-23</div>
							<div class="header-phone" style="display:none"><div class="header-phone-where">��������-�������</div>(095) 128-15-51</div>
							<div class="header-phone" style="display:none"><div class="header-phone-where">��������-�������</div>(096) 005-90-33</div>                            
						</div>                    
					</div>
					<div class="header-search">
						<form action="/base/search">
						<xsl:variable name="search_ord">
							<xsl:choose>
								<xsl:when test="$controller_name = 'shop'">1</xsl:when>
								<xsl:otherwise>2</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<input type="hidden" name="ord" value="{$search_ord}"/>
						<div class="header-search-bg">
							<input type="text" name="str" value="" placeholder="������..."/>
							<input type="image" src="/app/views/images/search-btn.png" title="search form"/>
						</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="top-menu-wrapper">
		<div class="top-menu black">
			<ul class="mega-menu">
				<xsl:apply-templates select="content/documents/topmenu"/>
			</ul>
		</div>
	</div>

	<div class="page-wrapper">
		<div class="page">
			<xsl:apply-templates select="content/documents/data"/>
			<div class="page-cover hidden"></div>
			
			<!-- Line of four blocks -->
			<xsl:if test="$controller_name!='base' or ($controller_name='base' and $method_name!='startpage')">
			
				<div class="startpage-blocks top-border-startpage-blocks">
					<!-- new products -->
					<div class="four-blocks-in-line">
						<h4>�������</h4>
						<div class="startpage-new-products"></div>
					</div>
					<!-- /new products -->
					
					<div class="space-between-blocks"></div>
					
					<!--  products -->
					<div class="four-blocks-in-line">
						<h4>������� �����������</h4>
						<div class="startpage-action-products"></div>
					</div>
					<!-- /new products -->
					
					<div class="space-between-blocks"></div>
					
					<!--  services -->
					<div class="four-blocks-in-line">
						<h4>����������� �����</h4>
						<div class="startpage-banners-services"></div>
					</div>
					<!-- /services -->
					
					<div class="space-between-blocks"></div>
					
					<!--  actions -->
					<div class="four-blocks-in-line">
						<h4>�����</h4>
						<div class="startpage-banners-actions"></div>
					</div>
					<!-- /actions -->
					
					<div class="clear-both"></div>
				</div>
				<script type="text/javascript">
					$j('.startpage-new-products').kNewProductsBox();
					$j('.startpage-action-products').kActionProductsBox();
					$j('.startpage-banners-services').kBannerBox({filter:1});
					$j('.startpage-banners-actions').kBannerBox({filter:2});
				</script>
			</xsl:if>
			<!-- /Line of four blocks -->
		</div>
	</div>
	
	<div class="page-clear-both"></div>
	
	<div class="footer-wrapper">
		<div class="footer">
			<div class="social-links">
				<div class="footer-ss-text">���������������:</div>
				<a class="footer-ss ss-f" href="https://www.facebook.com/digitalstudiokollage" target="_blank"></a>
				<a class="footer-ss ss-g" href="https://plus.google.com/116472227237436839401/posts?hl=ru" target="_blank"></a>
				<a class="footer-ss ss-vk" href="https://vk.com/studiokollage" target="_blank"></a>
			</div>
			<a class="google-app" target="_blank" href='https://play.google.com/store/apps/details?id=com.kollageorderphoto'><img alt='�������� � Google Play' src='https://play.google.com/intl/ru_ru/badges/images/generic/ru_badge_web_generic.png'/></a>
			<a class="bonus-plus" href=" /photos/article/401"></a>
			<div class="copyr"><xsl:value-of select="$locale/common/footer_text" disable-output-escaping="yes"/><br/>
         <a href="/privacy_policy">�������� ������������������</a></div>
			<div class="phones"><xsl:value-of select="$locale/common/footer_phones" disable-output-escaping="yes"/></div>
		</div>
	</div>
	
	<!-- Registration Container -->
	<div id="dlg-registration-container" class="hidden" title="����������� ������ ������������">
		<div id="dlg-content"></div>
		<div id="dlg-footer">
			<div class="dlg-footer-loader hidden">
				<div class="loader1"></div>
			</div>
			<div class="dlg-footer-buttons">
				<a class="dlg-btn-register"></a>
				<a class="dlg-btn-close"></a>
			</div>
			<div class="clear-both"></div>
		</div>
	</div>
	<!-- /Registration Container -->
	
	<!-- Register activation Container -->
	<div id="dlg-registeractivation-container" class="hidden" title="����� ��������� ������������">
		<div id="dlg-content"></div>
		<div id="dlg-footer">
			<div class="dlg-footer-loader hidden">
				<div class="loader1"></div>
			</div>
			<div class="dlg-footer-buttons">
				<a class="dlg-btn-send"></a>
				<a class="dlg-btn-close"></a>
			</div>
			<div class="clear-both"></div>
		</div>
	</div>
	<!-- /Register activation  Container -->
	
	<!-- Login Container -->
	<div id="dlg-login-container" class="hidden" title="����������� / ����">
		<div id="dlg-content"></div>
		<div id="dlg-footer">
			<div class="dlg-footer-loader hidden">
				<div class="loader1"></div>
			</div>
			<div class="dlg-footer-buttons">
				<a class="dlg-btn-login"></a>
				<a class="dlg-btn-registration"></a>
			</div>
			<div class="clear-both"></div>
		</div>
	</div>
	<!-- /Login Container -->
	
	<!-- Reminder Container -->
	<div id="dlg-reminder-container" class="hidden" title="����� ����������� ������">
		<div id="dlg-content"></div>
		<div id="dlg-footer">
			<div class="dlg-footer-loader hidden">
				<div class="loader1"></div>
			</div>
			<div class="dlg-footer-buttons">
				<a class="dlg-btn-send"></a>
				<a class="dlg-btn-close"></a>
			</div>
			<div class="clear-both"></div>
		</div>
	</div>
	<!-- /Reminder Container -->
	
	<!-- Shop Trash Container -->
	<div id="dlg-shop-trash-container" class="hidden" title="�������">
		<div id="dlg-content">
			<table class="trash-products">
				<tr>
					<th width="30">#</th>
					<th width="*">������������</th>
					<th width="100">����, ���</th>
					<th width="50">���.</th>
					<th width="100">�����, ���</th>
					<th width="30"></th>
				</tr>
			</table>
		</div>
		<div id="dlg-content2" class="shop-order-block" style="height:0px;overflow:hidden;">
			<hr/>
			<h2>���������� ������</h2>
			<table>
				<tr>
					<td width="50%">
						<label>���, �������<span class="red">*</span></label>
						<input type="text" name="name" class="small"/>
					</td>
					<td width="50%">
						<div class="shop-order-code-panel">
							<label>������� ������� � ��������<span class="red">*</span></label>
							<table style="width:97%"><tr>
								<td width="110"><img src="/app/views/images/loader1.gif" class="protect_image"/></td>
								<td width="37"><div class="btn-reload"></div></td>
								<td width="*"><input type="text" name="protect_code"/></td>
							</tr></table>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<label>���. �������<span class="red">*</span></label>
						<table style="width: 100%" cellspacing="0" cellpadding="0"><tr>
							<td><select name="country"><option>�������� ������</option></select></td>
							<td><input type="text" name="phone" class="small"/></td>
						</tr></table>
					</td>
					<td>
						<label>E-mail</label>
						<input type="text" name="email" class="small"/>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<label>���������� � ������</label>
						<input type="text" name="comments"/>
					</td>
				</tr>
			</table>
		</div>
		<div id="dlg-footer">
			<div class="dlg-footer-loader hidden">
				<div class="loader1"></div>
			</div>
			<div class="dlg-footer-buttons">
				<a class="dlg-btn-order-save" style="display:none"></a>
				<a class="dlg-btn-order"></a>
				<a class="dlg-btn-close"></a>
			</div>
			<div class="clear-both"></div>
		</div>
	</div>
	<!-- Shop Trash Container -->
	
	<!-- Comparision Container -->
	<div id="dlg-comparision-container" class="hidden" title="������� ��������� �������">
		<div id="dlg-content"></div>
		<div id="dlg-footer">
			<div class="dlg-footer-loader hidden">
				<div class="loader1"></div>
			</div>
			<div class="dlg-footer-buttons">
				<a class="dlg-btn-delete-all"></a>
				<a class="dlg-btn-close"></a>
			</div>
			<div class="clear-both"></div>
		</div>
	</div>
	<!-- /Comparision Container -->
	
	<!-- Action Container -->
	<div id="dlg-action-container" class="hidden" title="���������">
		<div id="dlg-content"></div>
		<div id="dlg-footer">
			<div class="dlg-footer-loader hidden">
				<div class="loader1"></div>
			</div>
			<div class="dlg-footer-buttons">
				<a class="dlg-btn-send"></a>
				<a class="dlg-btn-close"></a>
			</div>
			<div class="clear-both"></div>
		</div>
	</div>
	<!-- Action Container -->
	
	<!-- Anounce Container -->
	<div id="dlg-anounce-container" class="hidden" title="�����">
		<div id="dlg-content"></div>
		<div id="dlg-footer">
			<div class="dlg-footer-loader hidden">
				<div class="loader1"></div>
			</div>
			<div class="dlg-footer-buttons">
				<a class="dlg-btn-close"></a>
			</div>
			<div class="clear-both"></div>
		</div>
	</div>
	<!-- Anounce Container -->
	
	<!-- google -->
	<script type="text/javascript">
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-28053790-2']);
	  _gaq.push(['_trackPageview']);
	
	  (function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
	</script>
	<!-- google -->
	
	<!-- Start SiteHeart code -->
	<xsl:choose>
		<xsl:when test="$controller_name = 'shop'">
			<script>(function(){var widget_id = 701667;_shcp =[{widget_id : widget_id}];var lang =(navigator.language || navigator.systemLanguage || navigator.userLanguage ||"en").substr(0,2).toLowerCase();var url ="widget.siteheart.com/widget/sh/"+ widget_id +"/"+ lang +"/widget.js";var hcc = document.createElement("script");hcc.type ="text/javascript";hcc.async =true;hcc.src =("https:"== document.location.protocol ?"https":"http")+"://"+ url;var s = document.getElementsByTagName("script")[0];s.parentNode.insertBefore(hcc, s.nextSibling);})();</script>
		</xsl:when>
		<xsl:otherwise>
			<script id="{$controller_name}">(function(){var widget_id = 674222;_shcp =[{widget_id : widget_id}];var lang =(navigator.language || navigator.systemLanguage || navigator.userLanguage ||"en").substr(0,2).toLowerCase();var url ="widget.siteheart.com/widget/sh/"+ widget_id +"/"+ lang +"/widget.js";var hcc = document.createElement("script");hcc.type ="text/javascript";hcc.async =true;hcc.src =("https:"== document.location.protocol ?"https":"http")+"://"+ url;var s = document.getElementsByTagName("script")[0];s.parentNode.insertBefore(hcc, s.nextSibling);})();</script>
		</xsl:otherwise>
	</xsl:choose>
	<!-- End SiteHeart code -->
	
</body>
</html>
</xsl:template>

</xsl:stylesheet>

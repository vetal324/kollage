<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
                
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:template name="client_form">
    <!-- LOGIN BOX -->
    <table width="185" height="25" cellpadding="0" cellspacing="0" class="login_title" align="center">
        <tr>
            <td align="left"><xsl:value-of select="$locale/client_form/form_title/text()" disable-output-escaping="yes"/></td>
        </tr>
    </table>
    <table width="185" cellpadding="15" cellspacing="0" class="login" align="center">
        <tr>
            <td align="left">
                <table cellpadding="0" cellspacing="1" align="left">
                    <form method="post" action="?{$controller_name}.logout&amp;url=&#63;{$controller_name}.{$method_name}">
                    <tr>
                	<td class="ftitle"><xsl:value-of select="/content/global/client/firstname" disable-output-escaping="yes"/>&#160;<xsl:value-of select="/content/global/client/lastname" disable-output-escaping="yes"/></td>
                    </tr>
                    <tr>
                	<td class="ftitle"><xsl:value-of select="/content/global/client/email" disable-output-escaping="yes"/></td>
                    </tr>
                    <tr>
                	<td class="ftitle">ID: <xsl:value-of select="/content/global/client/id" disable-output-escaping="yes"/></td>
                    </tr>
                    <!--
                    <tr>
                	<td class="ftitle"><xsl:value-of select="$locale/client_form/type/text()" disable-output-escaping="yes"/><xsl:value-of select="/content/global/client/type" disable-output-escaping="yes"/></td>
                    </tr>
                    -->
                    <tr>
                	<td class="ftitle"><xsl:value-of select="$locale/client_form/purse/text()" disable-output-escaping="yes"/><xsl:value-of select="/content/global/client/purse" disable-output-escaping="yes"/>&#160;<xsl:value-of select="$locale/common/currency/text()" disable-output-escaping="yes"/></td>
                    </tr>
                    <tr>
                	<td style="padding: 5px 0 5px 0;"><input type="submit" value="{$locale/client_form/form_submit/text()}" class="button"/></td>
                    </tr>
                    <tr>
                        <td><small><a href="?{$controller_name}.my_properties"><xsl:value-of select="$locale/photos/leftmenu/my_settings/text()" disable-output-escaping="yes"/></a></small></td>
                    </tr>
                    <tr>
                        <td><small><a href="?{$controller_name}.my_orders"><xsl:value-of select="$locale/photos/leftmenu/my_orders/text()" disable-output-escaping="yes"/></a></small></td>
                    </tr>
                    <tr>
                        <td><small><a href="?{$controller_name}.my_purse"><xsl:value-of select="$locale/photos/leftmenu/my_purse/text()" disable-output-escaping="yes"/></a></small></td>
                    </tr>
                    
                    <xsl:if test="/content/global/client/allow_userfile = 1">
                    <tr>
                        <td><small><a href="?services.my_files"><xsl:value-of select="$locale/photos/leftmenu/my_files/text()" disable-output-escaping="yes"/></a></small></td>
                    </tr>
                    </xsl:if>
                    
                    <xsl:if test="/content/global/client/allow_fair = 1">
                    <tr>
                        <td><small><a href="?fair.my_fair"><xsl:value-of select="$locale/photos/leftmenu/my_fair/text()" disable-output-escaping="yes"/></a></small></td>
                    </tr>
                    </xsl:if>
                    
                    </form>
                </table>
            </td>
        </tr>
    </table>
    <!-- LOGIN BOX - END -->
</xsl:template>

</xsl:stylesheet>

<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:msxsl="urn:schemas-microsoft-com:xslt"
				xmlns:exsl="http://exslt.org/common"
				extension-element-prefixes="exsl"
				version="1.0">
				
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="banners.xsl" />

<xsl:variable name="session_id" select="content/global/session_id"/>
<xsl:variable name="controller_name" select="content/global/controller_name"/>
<xsl:variable name="method_name" select="content/global/method_name"/>
<xsl:variable name="site_prefix" select="content/global/site_prefix"/>
<xsl:variable name="views_path" select="content/global/views_path"/>
<xsl:variable name="data_path" select="content/global/data_path"/>
<xsl:variable name="sop_path" select="content/global/sop_path"/>
<xsl:variable name="fair_path" select="content/global/fair_path"/>
<xsl:variable name="fair_documents_path" select="content/global/fair_documents_path"/>
<xsl:variable name="lang" select="content/global/lang"/>

<xsl:variable name="rows_per_page"><xsl:value-of select="/content/global/rows_per_page"/></xsl:variable>
<xsl:variable name="pages"><xsl:value-of select="/content/global/pages"/></xsl:variable>
<xsl:variable name="page"><xsl:value-of select="/content/global/page"/></xsl:variable>
<xsl:variable name="page_prev" select="$page - 1"/>
<xsl:variable name="page_next" select="$page + 1"/>

<!--<xsl:variable name="exchange_rate" select="/content/global/currency/exchange_rate"/>-->
<xsl:variable name="exchange_rate" select="/content/global/currencies/currency[code='840']/exchange_rate"/>

<xsl:template name="gen-option">
  <xsl:param name="value"/>
  <xsl:param name="title"/>
  <xsl:param name="selected"/>
  <xsl:choose>
	<xsl:when test="$value = $selected">
	  <option value="{$value}" s="{$selected}" selected="yes"><xsl:value-of select="$title"/></option>
	</xsl:when>
	<xsl:otherwise>
	  <option value="{$value}" s="{$selected}"><xsl:value-of select="$title"/></option>
	</xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template name="sorting">
  <xsl:param name="sort_asc_url"/>
  <xsl:param name="sort_desc_url"/>
  
	<table cellspacing="0" cellpadding="0" class="nostyle">
		<tr>
			<td><a href="{$sort_asc_url}"><img src="{$views_path}images/sort_asc.gif" border="0"/></a></td>
			<td><a href="{$sort_desc_url}"><img src="{$views_path}images/sort_desc.gif" border="0"/></a></td>
		</tr>
	</table>
</xsl:template>

<xsl:template name="position">
  <xsl:param name="up_url"/>
  <xsl:param name="down_url"/>
  
	<table cellspacing="0" cellpadding="0" class="nostyle" align="center">
		<tr>
			<td><a href="{$up_url}"><img src="{$views_path}/images/move_up.gif" border="0"/></a></td>
			<td><a href="{$down_url}"><img src="{$views_path}/images/move_down.gif" border="0"/></a></td>
		</tr>
	</table>
</xsl:template>

<xsl:template name="tiny_mce">

	<!-- tinyMCE -->
	<script language="javascript" type="text/javascript" src="{$views_path}/../shared/tiny_mce/tiny_mce.js"></script>
	<script language="javascript" type="text/javascript">
		tinyMCE.init({
			mode : "textareas",
			width : 650,
			theme_advanced_buttons1 : "separator,bold,italic,separator,link,unlink,separator,undo,redo",
			theme_advanced_buttons2 : "",
			theme_advanced_buttons3 : "",
			theme_advanced_buttons4 : "",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "right"
		});
	</script>
	<!-- /tinyMCE -->
	
</xsl:template>

<xsl:template name="pagination">
	<xsl:param name="page"></xsl:param>
	<xsl:param name="start">1</xsl:param>
	<xsl:param name="stop">1</xsl:param>
	<xsl:param name="step">1</xsl:param>
	<xsl:param name="url"></xsl:param>
	
	<xsl:variable name="i">
		<xsl:choose>
			<xsl:when test="$page = 1">1</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="($page - 7) > 1"><xsl:value-of select="$page - 7"/></xsl:when>
					<xsl:otherwise>1</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	
	<xsl:variable name="j">
		<xsl:choose>
			<xsl:when test="$stop > 15">
				<xsl:choose>
					<xsl:when test="($i + 14) > $stop">
						<xsl:value-of select="$stop"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$i + 14"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise><xsl:value-of select="$stop"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	
	<xsl:if test="$i > 1">...&#160;</xsl:if>
	
	<xsl:call-template name="pagination_loop">
		<xsl:with-param name="page"><xsl:value-of select="$page"/></xsl:with-param>
		<xsl:with-param name="url"><xsl:value-of select="$url"/></xsl:with-param>
		<xsl:with-param name="stop"><xsl:value-of select="$j"/></xsl:with-param>
		<xsl:with-param name="start"><xsl:value-of select="$i"/></xsl:with-param>
	</xsl:call-template>
	
	<xsl:if test="$j &lt; $stop">...</xsl:if>
</xsl:template>

<xsl:template name="pagination_loop">
	<xsl:param name="page"></xsl:param>
	<xsl:param name="start">1</xsl:param>
	<xsl:param name="stop">1</xsl:param>
	<xsl:param name="step">1</xsl:param>
	<xsl:param name="url"></xsl:param>
	
	<xsl:choose>
		<xsl:when test="$page = $start"><xsl:value-of select="$start"/>&#160;</xsl:when>
		<xsl:otherwise><a href="{$url}&amp;page={$start}"><xsl:value-of select="$start"/></a>&#160;</xsl:otherwise>
	</xsl:choose>
	
	<xsl:if test="$start &lt; $stop">
		<xsl:call-template name="pagination_loop">
			<xsl:with-param name="page"><xsl:value-of select="$page"/></xsl:with-param>
			<xsl:with-param name="url"><xsl:value-of select="$url"/></xsl:with-param>
			<xsl:with-param name="stop"><xsl:value-of select="$stop"/></xsl:with-param>
			<xsl:with-param name="start"><xsl:value-of select="$start + $step"/></xsl:with-param>
		</xsl:call-template>
	</xsl:if>
</xsl:template>

<xsl:template match="currency">
	<table cellpadding="0" cellspacing="0"><tr>
		<td>
			<xsl:choose>
				<xsl:when test="/content/global/currency/id = id">
					<input type="radio" checked="yes" name="currency" id="currency{id}"/>
				</xsl:when>
				<xsl:otherwise>
					<input type="radio" name="currency" id="currency{id}" onclick="ChangeCurrency({id})"/>
				</xsl:otherwise>
			</xsl:choose>
		</td>
		<td><label for="currency{id}"><xsl:value-of select="symbol"/>&#160;&#160;</label></td></tr>
	</table>
</xsl:template>

<xsl:template name="get_product_price">
	<xsl:param name="product"></xsl:param>
    <xsl:variable name="exchange_rate" select="/content/global/currencies/currency[code='840']/exchange_rate"/>
	<xsl:choose>
		<xsl:when test="nice_price_active = 1">
			<xsl:value-of select="format-number( nice_price*$exchange_rate,'###,##0.00')"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:choose>
				<xsl:when test="/content/global/client/type_id = 2">
					<xsl:value-of select="format-number( price_opt*$exchange_rate,'###,##0.00')"/>
				</xsl:when>
				<xsl:when test="/content/global/client/type_id = 3">
					<xsl:value-of select="format-number( price_opt2*$exchange_rate,'###,##0.00')"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="format-number( price*$exchange_rate,'###,##0.00')"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

</xsl:stylesheet>

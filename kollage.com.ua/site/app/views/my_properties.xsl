<?xml version="1.0" encoding="windows-1251"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
                
<xsl:output method="html" encoding="utf-8" indent="no"/>

<xsl:include href="common.xsl" />
<xsl:include href="locale.xsl" />
<xsl:include href="layout_photos.xsl" />
<xsl:include href="menu.xsl" />

<xsl:template match="data">
    <h3 class="page_title"><xsl:value-of select="$locale/my/properties/title/text()"/> (<xsl:value-of select="/content/global/client/email"/>)</h3>
    
    <xsl:if test="/content/global/client/id = 0">
        <xsl:call-template name="login_form2"/>
    </xsl:if>
    
    <xsl:if test="/content/global/client/id > 0">
        <!-- CLIENT FORM -->
        <table cellpadding="0" cellspacing="0" border="0">
            
            <form method="post" name="order_params" id="order_params" action="?{$controller_name}.my_properties_save" style="margin:0;padding:0" 
            onsubmit="return(checkform(this,[['firstname','null','{$locale/register_form/m/firstname/text()}'],['password','null','{$locale/register_form/m/password/text()}'],['city','null','{$locale/register_form/m/city/text()}'],['zip','null','{$locale/register_form/m/zip/text()}'],['address','null','{$locale/register_form/m/address/text()}'],['phone','null','{$locale/register_form/m/phone/text()}']]))">
            <tr>
            <td width="47%" valign="top">
            
                <table cellpadding="0" cellspacing="3">
                <tr>
                <td class="ftitle"><xsl:value-of select="$locale/my/properties/firstname/text()" disable-output-escaping="yes"/>*</td>
                </tr>
                <tr>
                <td><input type="text" name="object[firstname]" id="firstname" class="inputbox_middle" value="{/content/global/client/firstname}"/></td>
                </tr>
                
                <tr>
                <td class="ftitle"><xsl:value-of select="$locale/my/properties/lastname/text()" disable-output-escaping="yes"/></td>
                </tr>
                <tr>
                <td><input type="text" name="object[lastname]" id="lastname" class="inputbox_middle" value="{/content/global/client/lastname}"/></td>
                </tr>
                
                <tr>
                <td class="ftitle"><xsl:value-of select="$locale/my/properties/password/text()" disable-output-escaping="yes"/>*</td>
                </tr>
                <tr>
                <td><input type="password" name="object[password]" id="password" class="inputbox_middle" value="{/content/global/client/password}"/></td>
                </tr>
                
                <tr>
                <td class="ftitle"><xsl:value-of select="$locale/my/properties/city/text()" disable-output-escaping="yes"/>*</td>
                </tr>
                <tr>
                <td><input type="text" name="object[city]" id="city" class="inputbox_middle" value="{/content/global/client/city}"/></td>
                </tr>
                
                <tr>
                <td colspan="2" style="padding: 5px 0 5px 0;"><input type="submit" value="{$locale/my/properties/form_submit/text()}" class="button"/></td>
                </tr>
            
            </table>
            </td>
            
            <td width="6%" valign="top">&#160;</td>
            
            <td width="47%" valign="top">
                <table cellpadding="0" cellspacing="3">
                
                <tr>
                <td class="ftitle"><xsl:value-of select="$locale/my/properties/zip/text()" disable-output-escaping="yes"/>*</td>
                </tr>
                <tr>
                <td><input type="text" name="object[zip]" id="zip" class="inputbox_middle" value="{/content/global/client/zip}"/></td>
                </tr>
                
                <tr>
                <td class="ftitle"><xsl:value-of select="$locale/my/properties/address/text()" disable-output-escaping="yes"/>*</td>
                </tr>
                <tr>
                <td><input type="text" name="object[address]" id="address" class="inputbox_middle" value="{/content/global/client/address}"/></td>
                </tr>
                
                <tr>
                <td class="ftitle"><xsl:value-of select="$locale/my/properties/phone/text()" disable-output-escaping="yes"/>*</td>
                </tr>
                <tr>
                <td><input type="text" name="object[phone]" id="phone" class="inputbox_middle" value="{/content/global/client/phone}"/></td>
                </tr>
                
                <tr>
                <td class="ftitle"><xsl:value-of select="$locale/my/properties/bdate/text()" disable-output-escaping="yes"/></td>
                </tr>
                <tr>
                <td><input type="text" name="object[bdate]" id="bdate" class="inputbox_middle" value="{/content/global/client/bdate}"/></td>
                </tr>
                
                <tr>
                <td class="ftitle"><xsl:value-of select="$locale/my/properties/send_news/text()" disable-output-escaping="yes"/></td>
                </tr>
                <tr><td>
                    <xsl:choose>
                        <xsl:when test="/content/global/client/send_news = 1"><input type="radio" name="object[send_news]" id="send_news_active" value="1" checked="yes" /></xsl:when>
                        <xsl:otherwise><input type="radio" name="object[send_news]" id="send_news_active" value="1" /></xsl:otherwise>
                    </xsl:choose>
                    <label for="send_news_active" class="lower"><xsl:value-of select="$locale/common/yes/text()"/></label>
                    <xsl:choose>
                        <xsl:when test="/content/global/client/send_news = 0"><input type="radio" name="object[send_news]" id="send_news_inactive" value="0" checked="yes" /></xsl:when>
                        <xsl:otherwise><input type="radio" name="object[send_news]" id="send_news_inactive" value="0"/></xsl:otherwise>
                    </xsl:choose>
                    <label for="send_news_inactive" class="lower"><xsl:value-of select="$locale/common/no/text()"/></label>
                </td></tr>
            
                </table>
            
            </td></tr>
            
            </form>
                
        </table>
        <!-- CLIENT FORM - END -->
    </xsl:if>
    
</xsl:template>

</xsl:stylesheet>

var PBCalculator = function(config) {

    this.sourceUrl = config.sourceUrl;

    this.templates = config.templates;

    this.container = $("#pb-wrapper");

    this.pbCurrencyContainer = this.container.find("#pb-currencies");

    this.pbTypesContainer = this.container.find("#pb-types");
    this.pbFormatsContainer = this.container.find("#pb-formats");
    this.pbCoverTypesContainer = this.container.find("#pb-cover-types");
    this.pbCoversContainer = this.container.find("#pb-covers");

    //currencies, loaded from server
    this.currencyId = 'UAH';
    this.currencies = {};
    this.currency = {};

    //pbTypes, loaded from server
    this.pbTypes = {};

    //selected pbType
    this.pbTypeId = 0;
    this.pbType = {};

    //pbFormats (sizes)
    this.pbFormatId = 0;
    this.pbFormats = {};
    this.pbFormat = {};

    //pbCoverTypes
    this.pbCoverTypeId = 0;
    this.pbCoverTypes = {};
    this.pbCoverType = {};

    //pbCovers
    this.pbCoverId = 0;
    this.pbCovers = {};
    this.pbCover = {};

};

$.extend(true, PBCalculator, {

    prototype: {

        /*============================================================================================================*/
        /*========================================== INITIALIZATION ==================================================*/
        /*============================================================================================================*/

        init: function () {
            var scope = this;
            scope.initializeTemplates();
            this.loadData().done(function (response) {
                if (response.pbTypes) {
                    scope.initializeCalculator(response);
                } else {
                    console.log('Failed to initialize BookCalculator.');
                }
            });
        },

        loadData: function () {
            var scope = this;
            return $.ajax({
                url: scope.sourceUrl,
                type: 'GET',
                dataType: "json",
                contentType:"application/json; charset=UTF-8",
                cache: false,
                data: {}
            });
        },

        initializeTemplates: function () {

            this.pbCurrencyTemplate     = Handlebars.compile($("#" + this.templates.pbCurrencyTemplateId).html());

            this.pbTypeTemplate         = Handlebars.compile($("#" + this.templates.pbTypeTemplateId).html());
            this.pbFormatTemplate       = Handlebars.compile($("#" + this.templates.pbFormatTemplateId).html());
            this.pbCoverTypeTemplate    = Handlebars.compile($("#" + this.templates.pbCoverTypeTemplateId).html());
            this.pbCoverTemplate        = Handlebars.compile($("#" + this.templates.pbCoverTemplateId).html());

        },

        initializeCalculator: function (response) {

            this.pbTypes = response.pbTypes;
            this.currencies = response.currencies;

            console.log('response:');
            console.log(response);

            this.render();

            this.bindListeners();
        },


        /*============================================================================================================*/
        /*=============================================== RENDERING ==================================================*/
        /*============================================================================================================*/

        render: function () {

            this.renderPbTypes();

            this.renderCurrencies();
        },

        /*==========================================   PB currencies    ==============================================*/
        renderCurrencies: function () {
            var currencies = this.currencies;
            this.pbCurrencyContainer.html("");
            if (currencies && Object.keys(currencies).length > 0) {
                for (var key in currencies) {
                    var currency = currencies[key];
                    var currencyHtml = this.pbCurrencyTemplate(currency);
                    this.pbCurrencyContainer.append(currencyHtml);
                }
                this.selectDefaultPbCurrency();
            }
        },

        selectDefaultPbCurrency: function () {
            if (this.currencies && Object.keys(this.currencies).length > 0) {
                var currencyId = Object.keys(this.currencies)[0];
                this.selectPbCurrency(currencyId);
            }
        },

        selectPbCurrency: function (currencyId) {
            this.currencyId = currencyId;
            this.currency = this.currencies[this.currencyId];
            $("#pb-currencies .product-item").removeClass('active');
            $("#pb-currency-" + currencyId).addClass('active');
            this.calculatePrice(this);
        },

        /*============================================ PB types ======================================================*/
        renderPbTypes: function () {
            var pbTypes = this.pbTypes;
            this.pbTypesContainer.html("");
            if (pbTypes) {
                for (var key in pbTypes) {
                    var pbType = pbTypes[key];
                    var pbTypesHtml = this.pbTypeTemplate(pbType);
                    this.pbTypesContainer.append(pbTypesHtml);
                }
                this.selectDefaultPbType();
            }
        },

        selectDefaultPbType: function () {
            if (this.pbTypes  && Object.keys(this.pbTypes).length > 0) {
                var pbTypeId = Object.keys(this.pbTypes)[0];
                this.selectPbType(pbTypeId);
            }
        },

        selectPbType: function (pbTypeId) {
            this.pbTypeId = pbTypeId;
            this.pbType = this.pbTypes[this.pbTypeId];
            $("#pb-types .product-item").removeClass('active');
            $("#pb-type-" + pbTypeId).addClass('active');
            var imgSrc = $("#pb-type-" + pbTypeId).find("img").attr("src");
            $("#img-preview-section .preview-container").css('background-image', 'url("' + imgSrc + '")');
            this.renderPbFormats();
        },

        /*========================================== PB formats ======================================================*/
        renderPbFormats: function () {
            this.pbFormatsContainer.html("");
            this.pbFormats = this.pbType.formats;
            if (this.pbFormats && Object.keys(this.pbFormats).length > 0) {
                for (var key in this.pbFormats) {
                    var format = this.pbFormats[key];
                    var pbFormatHtml = this.pbFormatTemplate(format);
                    this.pbFormatsContainer.append(pbFormatHtml);
                }
                this.selectDefaultPbFormat();
            }

        },

        selectDefaultPbFormat: function () {
            if (this.pbFormats  && Object.keys(this.pbFormats).length > 0) {
                var pbFormatId = Object.keys(this.pbFormats)[0];
                this.selectPbFormat(pbFormatId);
            }
        },

        selectPbFormat: function (pbFormatId) {
            this.pbFormatId = pbFormatId;
            this.pbFormat = this.pbFormats[this.pbFormatId];
            $("#pb-formats .product-item").removeClass('active');
            $("#pb-format-" + pbFormatId).addClass('active');
            this.renderPbCoverTypes();
        },


        /*====================================== PB cover types ======================================================*/
        renderPbCoverTypes: function () {
            this.pbCoverTypesContainer.html("");
            this.pbCoverTypes = this.pbFormat.cover_types;
            if (this.pbCoverTypes && Object.keys(this.pbCoverTypes).length > 0) {
                for (var key in this.pbCoverTypes) {
                    var pbCoverType = this.pbCoverTypes[key];
                    var pbCoverTypeHtml = this.pbCoverTypeTemplate(pbCoverType);
                    this.pbCoverTypesContainer.append(pbCoverTypeHtml);
                }
                this.selectDefaultPbCoverType();
            }
        },

        selectDefaultPbCoverType: function () {
            if (this.pbCoverTypes && Object.keys(this.pbCoverTypes).length > 0) {
                var pbCoverTypeId = Object.keys(this.pbCoverTypes)[0];
                this.selectPbCoverType(pbCoverTypeId);
            }
        },

        selectPbCoverType: function (pbCoverTypeId) {
            this.pbCoverTypeId = pbCoverTypeId;
            this.pbCoverType = this.pbCoverTypes[this.pbCoverTypeId];
            $("#pb-cover-types .product-item").removeClass('active');
            $("#pb-cover-type-" + pbCoverTypeId).addClass('active');
            this.renderPbCovers();
        },

        /*============================================ PB coves ======================================================*/
        renderPbCovers: function () {
            this.pbCoversContainer.html("");
            this.pbCovers = this.pbCoverType.covers;
            if (this.pbCovers && Object.keys(this.pbCovers).length > 0) {
                for (var key in this.pbCovers) {
                    var pbCover = this.pbCovers[key];
                    var pbCoverHtml = this.pbCoverTemplate(pbCover);
                    this.pbCoversContainer.append(pbCoverHtml);
                }
                this.selectDefaultPbCover();
            }
        },

        selectDefaultPbCover: function () {
            if (this.pbCovers && Object.keys(this.pbCovers).length > 0) {
                var pbCoverId = Object.keys(this.pbCovers)[0];
                this.selectPbCover(pbCoverId);
            }
        },

        selectPbCover: function (pbCoverId) {
            this.pbCoverId = pbCoverId;
            this.pbCover = this.pbCovers[this.pbCoverId];
            $("#pb-covers .product-item").removeClass('active');
            $("#pb-cover-" + pbCoverId).addClass('active');
        },

        /*============================================================================================================*/
        /*======================================= BINDING LISTENERS ==================================================*/
        /*============================================================================================================*/

        bindListeners: function () {

            this.container.on('click', '#pb-currencies .product-item', {scope: this}, this.pbCurrencyClickHandler);

            this.container.on('click', '#pb-types .product-item', {scope: this}, this.pbTypeClickHandler);
            this.container.on('click', '#pb-formats .product-item', {scope: this}, this.pbFormatClickHandler);

            this.container.on('click', '#pb-cover-types .product-item', {scope: this}, this.pbCoverTypeClickHandler);
            this.container.on('click', '#pb-covers .product-item', {scope: this}, this.pbCoverClickHandler);
        },


        /*============================================================================================================*/
        /*========================================== EVENT HANDLERS ==================================================*/
        /*============================================================================================================*/
        pbCurrencyClickHandler: function (event) {
            var scope = event.data.scope;
            var currencyId = $(this).data("id");
            scope.selectPbCurrency(currencyId);
        },

        pbTypeClickHandler: function (event) {
            var scope = event.data.scope;
            var pbTypeId = $(this).data("id");
            scope.selectPbType(pbTypeId);
            scope.calculatePrice(scope);
        },

        pbFormatClickHandler: function (event) {
            var scope = event.data.scope;
            var pbFormatId = $(this).data("id");
            scope.selectPbFormat(pbFormatId);
            scope.calculatePrice(scope);
        },

        pbCoverTypeClickHandler: function (event) {
            var scope = event.data.scope;
            var pbCoverTypeId = $(this).data("id");
            scope.selectPbCoverType(pbCoverTypeId);
            scope.calculatePrice(scope);
        },

        pbCoverClickHandler: function (event) {
            var scope = event.data.scope;
            var pbCoverId = $(this).data("id");
            scope.selectPbCover(pbCoverId);
            scope.calculatePrice(scope);
        },

        /*============================================================================================================*/
        /*============================================= HELPERS ======================================================*/
        /*============================================================================================================*/


        /*============================================================================================================*/
        /*============================================= CALCULATION ==================================================*/
        /*============================================================================================================*/
        calculatePrice: function (scope) {


        }

    }

});
window.PBCalculator = PBCalculator;
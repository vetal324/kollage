<?php
/*****************************************************
 Class v.1.0, 2009
 Get IPTC, XMP information from EXIF format
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

class AExif
{
    var $filename;
    var $data;
    var $parsed;
    var $error;
    var $str_delim = "\n";
    
    function __construct( $filename )
    {
        $this->filename = $filename;
        $this->data = Array();
        $this->parsed = Array();
        $this->error = "";
        
        if( $this->filename )
        {
            $this->GetInformation();
        }
    }
    
    function SetFilename( $filename )
    {
        $this->filename = $filename;
        $this->data = Array();
        $this->parsed = Array();
        $this->error = "";
        
        if( $this->filename )
        {
            $this->GetInformation();
        }
        else
        {
            $this->error .= $this->str_delim ."Parameter [filename] is empty";
        }
    }
    
    function GetInformation()
    {
        if( $this->filename && file_exists($this->filename) )
        {
            if( function_exists("exiv2_get") )
            {
                $this->data = Array();
                $this->data = exiv2_get( $this->filename );
                $this->ParseExiv2();
            }
            else
            {
                $this->error .= $this->str_delim ."Function [exiv2_get()] is not exist. Please install exiv2 module for PHP";
            }
        }
        else
        {
            $this->error .= $this->str_delim ."File is not exist";
        }
    }
    
    function ParseExiv2()
    {
        if( count($this->data) > 0 )
        {
            $this->parsed = Array();
            $this->parsed["make"] = null;
            $this->parsed["model"] = null;
            $this->parsed["software"] = null;
            $this->parsed["resolution_x"] = null;
            $this->parsed["resolution_y"] = null;
            $this->parsed["exposure_time"] = null;
            $this->parsed["iso"] = null;
            $this->parsed["datetime_original"] = null;
            $this->parsed["datetime_digitized"] = null;
            
            if( array_key_exists( "Exif.Image.Make", $this->data ) )
            {
                $exif_make = $this->data["Exif.Image.Make"]["value"];
            }
            
            if( array_key_exists( "Xmp.tiff.Make", $this->data ) )
            {
                $xmp_make = $this->data["Xmp.tiff.Make"]["value"];
            }
            
            if( $exif_make ) $this->parsed["make"] = $exif_make;
            if( $xmp_make ) $this->parsed["make"] = $xmp_make;
            
            if( array_key_exists( "Exif.Image.Model", $this->data ) )
            {
                $exif_model = $this->data["Exif.Image.Model"]["value"];
            }
            
            if( array_key_exists( "Xmp.tiff.Model", $this->data ) )
            {
                $xmp_model = $this->data["Xmp.tiff.Model"]["value"];
            }
            
            if( $xmp_model ) $this->parsed["model"] = $xmp_model;
            if( $exif_model ) $this->parsed["model"] = $exif_model;
            
            if( array_key_exists( "Exif.Image.Software", $this->data ) )
            {
                $exif_software = $this->data["Exif.Image.Software"]["value"];
            }
            
            if( array_key_exists( "Xmp.xmp.CreatorTool", $this->data ) )
            {
                $xmp_software = $this->data["Xmp.xmp.CreatorTool"]["value"];
            }
            
            if( $xmp_software ) $this->parsed["software"] = $xmp_software;
            if( $exif_software ) $this->parsed["software"] = $exif_software;
            
            if( array_key_exists( "Exif.Image.XResolution", $this->data ) )
            {
                $exif_xresolution = $this->data["Exif.Image.XResolution"]["value"];
                list($exif_xresolution1,$exif_xresolution2) = split( "/", $exif_xresolution );
                $this->parsed["resolution_x"] = intval( $exif_xresolution1 / $exif_xresolution2 );
            }
            
            if( array_key_exists( "Exif.Image.YResolution", $this->data ) )
            {
                $exif_yresolution = $this->data["Exif.Image.YResolution"]["value"];
                list($exif_yresolution1,$exif_yresolution2) = split( "/", $exif_yresolution );
                $this->parsed["resolution_y"] = intval( $exif_yresolution1 / $exif_yresolution2 );
            }
            
            if( array_key_exists( "Exif.Photo.ExposureTime", $this->data ) )
            {
                $exposure_time = $this->data["Exif.Photo.ExposureTime"]["value"];
                $this->parsed["exposure_time"] = $exposure_time;
            }
            
            if( array_key_exists( "Exif.Photo.ISOSpeedRatings", $this->data ) )
            {
                $iso = $this->data["Exif.Photo.ISOSpeedRatings"]["value"];
                $this->parsed["iso"] = $iso;
            }
            
            if( array_key_exists( "Exif.Photo.DateTimeOriginal", $this->data ) )
            {
                $datetime_original = $this->data["Exif.Photo.DateTimeOriginal"]["value"];
                list($d,$t) = split( " ", $datetime_original );
                list($y,$m,$d) = split( ":", $d );
                list($hh,$mm,$ss) = split( ":", $t );
                $this->parsed["datetime_original"] = sprintf( "%02d.%02d.%d %02d:%02d:%02d", intval($d), intval($m), intval($y), intval($hh), intval($mm), intval($ss) );
            }
            
            if( array_key_exists( "Exif.Photo.DateTimeDigitized", $this->data ) )
            {
                $datetime_digitized = $this->data["Exif.Photo.DateTimeDigitized"]["value"];
                list($d,$t) = split( " ", $datetime_digitized );
                list($y,$m,$d) = split( ":", $d );
                list($hh,$mm,$ss) = split( ":", $t );
                $this->parsed["datetime_digitized"] = sprintf( "%02d.%02d.%d %02d:%02d:%02d", intval($d), intval($m), intval($y), intval($hh), intval($mm), intval($ss) );
            }
            
            $this->FixingParameters();
        }
        else
        {
            $this->error .= $this->str_delim ."Data array is empty";
        }
    }
    
    function FixingParameters()
    {
        if( count($this->data) > 0 )
        {
            $expr = Array( "/^v757.*/i", "/^Ver.*/i", "/^QSS-.*/i", "/^Digital Camera FinePix.*/i" );
            foreach( $expr as $e )
            {
                if( preg_match( $e, $this->parsed["software"] ) )
                {
                    $this->parsed["software"] = "";
                    break;
                }
            }
        }
    }
}

?>
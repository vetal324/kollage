<?php
/*****************************************************
 Class v.1.2.1, (2006-2008)
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

if( !defined('SITEROOT') ) define( 'SITEROOT', realpath( dirname(__FILE__) . '/../' ) );

class Attachment
{
    var $name, $filename, $content_type;
    var $path, $uri;
    
    function Attachment( $prefix='data' )
    {
        $this->path = SITEROOT;
        if( $prefix )
        {
            $this->path .= "/$prefix/";
            $this->uri = "/$prefix/";
        }
    }
    
    function rm()
    {
        $retval = false;
        
        if( $this->filename && file_exists( $this->path . $this->filename ) )
        {
            $retval = unlink( $this->path . $this->filename );
        }
        
        return( $retval );
    }
    
    function uri()
    {
        return( $this->uri . $this->filename );
    }
    
    function get_id() { return( $this->id ); }
    function get_parent_id() { return( $this->parent_id ); }
    function get_name() { return( $this->name ); }
    function get_filename() { return( $this->filename ); }
    function get_path() { return( $this->path ); }
    
    function upload( $form_element_name='file' )
    {
        $retval = false;
        
        if( is_uploaded_file($_FILES[$form_element_name]['tmp_name']) )
        {
            $suffix = str_replace( '.', '', microtime(true) );
            $this->name = trim( basename( $_FILES[$form_element_name]['name'] ) );
            $this->filename = $suffix .'.'. $this->get_file_ext( $this->name );
            $uploadfile = $this->path . $this->filename;

            if( move_uploaded_file($_FILES[$form_element_name]['tmp_name'], $uploadfile ) ) 
            {
                chmod( $uploadfile, 0664 );
                $this->content_type = basename($_FILES[$form_element_name]['type']);
                $retval = true;
            }
        }
        
        return( $retval );
    }
    
    function upload_blob( $name, $blob )
    {
        $retval = false;
        
        if( $name && $blob )
        {
            $suffix = str_replace( '.', '', microtime(true) );
            $this->name = $name;
            $this->filename = $suffix .'.'. $this->get_file_ext( $this->name );
            $uploadfile = $this->path . $this->filename;

            if( file_put_contents($uploadfile, $blob ) )
            {
                chmod( $uploadfile, 0664 );
                // $this->content_type = basename($_FILES[$form_element_name]['type']);
                $retval = true;
            }
        }
        
        return( $retval );
    }
    
    function get_file_ext( $file )
    {
        $retval = explode( ".", $file );
        
        return( $retval[ count($retval)-1 ] );
    }
}

?>
<?php
/***********************************************************************
 Class v.1.0, 2008, PHP5
    Log into existed file
 Copyright Andrey Nebogin anebogin@gmail.com
*************************************************************************/

class Logging
{
    var $filename;
    var $fp;
    var $isOpened;
    
    function __construct( $filename='' )
    {
        if( $filename && file_exists($filename) )
        {
            $this->Open( $filename );
        }
    }
    
    function __destruct()
    {
        $this->Close();
    }
    
    function Open( $filename )
    {
        $retavl = false;
        $this->isOpened = false;
        
        if( file_exists($filename) )
        {
            $this->filename = $filename;
            $this->fp = fopen( $this->filename, "a+" );
            if( $this->fp )
            {
                $retval = true;
                $this->isOpened = true;
            }
        }
        
        return( $retval );
    }
    
    function Close()
    {
        if( $this->isOpened )
        {
            fclose( $this->fp );
            $this->isOpened = false;
        }
    }
    
    function Write( $text )
    {
        $retval = false;
        
        if( $text && $this->isOpened )
        {
            $header = date( "d.m.Y H:i:s" );
            $host = $_SERVER['HTTP_HOST'];
            $ipaddress = $_SERVER['REMOTE_ADDR'];
            $delim = " ( ";
            if( $host )
            {
                $header .= $delim ."Host=". $host;
                $delim = " ";
            }
            if( $ipaddress )
            {
                $header .= $delim ."IpAddress=". $ipaddress;
                $delim = " )";
                $header .= $delim;
            }
            if( fwrite( $this->fp, $header ."\n". $text ."\n\n" ) )
            {
                $retval = true;
            }
        }
        
        return( $retval );
    }
    
    function IsOpened()
    {
        return( $this->isOpened );
    }
}

?>
<?php
/*****************************************************
 Dictionary v.1.2, 10.2007
 Copyright Andrey Nebogin anebogin@gmail.com
 PHP 5.x
******************************************************/

class Dictionary
{
    var $filename, $doc, $xml, $encoding;
    var $loaded;
    var $default_encoding = 'utf-8';
    
    function Dictionary( $filename, $encoding="windows-1251" )
    {
        $this->filename = $filename;
        $this->xml = "";
        $this->loaded = false;
        $this->encoding = $encoding;
        $this->load();
    }
    
    function load()
    {
        $this->loaded = false;
        if( file_exists($this->filename) )
        {
            $this->doc = new DOMDocument( '1.0', $this->default_encoding );
            $this->doc->load( $this->filename );
            if( $this->doc )
            {
                $this->loaded = true;
            }
        }
    }
    
    function is_loaded()
    {
        return( $this->loaded );
    }
    
    function get( $path )
    {
        $retval = "";
        
        if( $this->is_loaded() )
        {
            if( substr($path,0,1) == '/' ) $path = substr( $path, 1 );
            if( substr($path,strlen($path)-1,1) == '/' ) $path = substr( $path, 0, strlen($path)-1 );
            
            $path_elements = explode( '/', $path ); //print_r( $path_elements );
            if( count($path_elements)>0 )
            {
                $name = $path_elements[ count($path_elements)-1 ];
                
                $node = $path_elements[0];
                if( $node == $name )
                {
                    $items = $this->doc->getElementsByTagName( $node );
                    $retval = $this->decode( $items->item(0)->nodeValue );
                }
                else
                {
                    $i = 0; $j = 0;
                    $items = $this->doc->getElementsByTagName( $path_elements[$i] );
                    $i++;
                    while( $i<count($path_elements) && $items = $items->item($j)->getElementsByTagName($path_elements[$i]) )
                    {
                        if( $items->length==0 )
                        {
                            $j++;
                            $items = $this->doc->getElementsByTagName( $path_elements[$i-1] );
                            if( $items->length == $j ) break;
                        }
                        else
                        {
                            $i++; $j = 0;
                        }
                    }
                    $retval = $this->decode( $items->item($j)->nodeValue );
                }
            }
        }
        
        return( $retval );
    }
    
    function decode( $str )
    {
        $retval = convert_encoding( $this->default_encoding, $this->encoding, $str );
        
        return( $retval );
    }
}

?>
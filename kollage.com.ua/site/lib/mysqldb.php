<?php
/*****************************************************
 MySQL layout v.2, 2017
 	Moving from php 5.2 to 5.6
******************************************************/

define( 'HAS_ONE', 1 );
define( 'HAS_MANY', 2 );

/* Class implement interface to MySQL database */
class MysqlDB
{
	var $persistent;
	var $dbConnection;
	var $dsn;
	var $charset;
	var $version;
	var $last_query;

	function MysqlDB( $charset='latin1' )
	{
		$this->charset = $charset; #'cp1251','koi8r','win1251ukr', 'utf8'
	}

	############################ /Public methods #########################
	function connect( $dsn, $persistent=true )
	{
		$retval = true;

		if( !is_array($dsn) ) $retval = false;

		if( $retval )
		{
			$this->persistent = $persistent;
			$this->dbConnection = new mysqli( $dsn['host'], $dsn['user'], $dsn['password'], $dsn['dbname'] );

			if( $this->dbConnection->connect_errno ) $retval = false;
			if( $retval )
			{
				$this->dbConnection->query( "set character set $this->charset" );
				$this->dbConnection->query( "set names $this->charset" );
			}
		}

		$this->dsn = $dsn;

		return( $retval );
	}

	function disconnect()
	{
		if( $this->persistent ) return( true );
		else mysqli::close();
	}

	function create( $sql )
	{
		$retval = false;

		if( $this->dbConnection )
		{
			$query = "create " . $sql;
			$retval = $this->dbConnection->query( $query );
		}

		return( $retval );
	}

	function drop( $sql )
	{
		$retval = false;

		if( $this->dbConnection )
		{
			$query = "drop " . $sql;
			$retval = $this->dbConnection->query( $query );
		}

		return( $retval );
	}

	function getone( $sql )
	{
		$retval = null;

		if( $this->dbConnection )
		{
			if( $rs = $this->dbConnection->query( $sql ) )
			{
				$a = $rs->fetch_row();
				$retval = $a[0];
			}
		}

		return( $retval );
	}
	
	function query( $sql )
	{
		$this->last_query = $sql;
		$retval = $this->dbConnection->query( $sql );
		return( $retval );
	}
	
	function getrow( $rs )
	{
		if( $rs ) return( $rs->fetch_row() );
		else {
			trigger_error( "Query [{$this->last_query}] {$this->dbConnection->error}", E_USER_ERROR );
			return( null );
		}
	}

	function fetch_row( $rs ) {
		return( $this->getrow($rs) );
	}

	function get_error_txt()
	{
		return( $this->dbConnection->error );
	}

	function get_error_no()
	{
		return( $this->dbConnection->errno );
	}
}
/* /Class implement interface to MySQL database */

/* Class implement interface to MySQL table */
class MysqlTable
{
	var $dbConnection;
	var $name;
	var $result;

	var $numFields = 0;
	var $fields;    // fields["field_name"][{position|type|flags}], [flags]="not_null", "primary_key", "unique_key", "multiple_key", "blob", "unsigned", "zerofill", "binary", "enum", "auto_increment", "timestamp"

	var $rowcount = 0;
	var $data; // data[row_number]["field_name"]

	var $conditions = "";
	var $order = "";
	var $limit = "";
	var $group = "";
	var $distinct = false;

	var $isInitialized = false;
	
	var $associations;
	
	var $last_insert_id;
	
	var $debug, $debug_messages = "";

	function MysqlTable( $table_name, $debug=false )
	{
		global $db;
		$this->dbConnection = $db->dbConnection;
		$this->name = $table_name;
		$this->debug = $debug;
		$this->init();
	}

	##################### Public methods ###########################
	# conditions - WHERE, order - ORDER, limit - LIMIT station, group - GROUP BY station
	function find_all( $conditions="", $order="", $limit="", $group="", $distinct=false )
	{
		if( $this->debug ) $this->debug_messages .= "Call find_all('$conditions', '$order', '$limit', '$group', $distinct)\n";
	
		$this->conditions = $conditions;
		$this->order = $order;
		$this->limit = $limit;
		$this->group = $group;
		$this->distinct = $distinct;
		$this->query();
		$this->get_rows();
		$this->get_associations();

		if( count($this->data)>0 ) return( $this->data );
		else return( null );
	}

	# conditions - WHERE, order - ORDER, limit - LIMIT station
	function find_first( $conditions="", $order="", $limit="1", $group="" )
	{
		if( $this->debug ) $this->debug_messages .= "Call find_first('$conditions', '$order', '$limit', '$group')\n";
	
		$this->conditions = $conditions;
		$this->order = $order;
		$this->limit = $limit;
		$this->group = $group;
		$this->query();
		$this->get_rows();
		$this->get_associations();
		
		if( count($this->data)!=1 ) return( null );
		else return( $this->data[0] );
	}
	
	function is_initialized() { return($this->isInitialized); }
	
	# add association to general table
	# $name - name of this association
	# $table - table name for this association
	# $field - foreign key (field of the table $table )
	# $where - addition where clause
	# After get rows you can read values: $mytable->data[row][$name][<fieldname>]
	function has_one( $name, $table, $field, $where='' )
	{
		if( $this->debug ) $this->debug_messages .= "Call has_one('$name', '$table', '$field', '$where')\n";
	
		$this->associations[$name] = array($table,$field,HAS_ONE,$where);
	}
	
	# add association to general table
	function has_many( $name, $table, $field, $where='' )
	{
		if( $this->debug ) $this->debug_messages .= "Call has_many('$name', '$table', '$field', '$where')\n";
	
		$this->associations[$name] = array($table,$field,HAS_MANY,$where);
	}
	
	function noerrors()
	{
		$retval = false;
		if( $this->isInitialized ) $retval = true;
		return( $retval );
	}
	
	function field_array( $field_name )
	{
		$retval = Array();
		foreach( $this->data as $row )
		{
			array_push( $retval, $row[$field_name] );
		}
		
		return( $retval );
	}
	
	function field_aarray( $field_name1, $field_name2 )
	{
		$retval = Array();
		foreach( $this->data as $row )
		{
			$retval[ $row[ $field_name1 ] ] = $row[ $field_name2 ];
		}
		
		return( $retval );
	}

	# row_data - cache {"fieldname"=>fieldvalue,.......}
	function save( $row_data )
	{
		if( $this->debug ) $this->debug_messages .= "Call save(..)\n";
	
		$retval = false;
		$update_flag = false;
		
		$primary_key_field = $this->find_primary_key_field();
		if( $primary_key_field )
		{
			foreach( $row_data as $k=>$v )
			{
				if( stristr($primary_key_field,$k) )
				{
					# primary key exist in $row_data then call update
					if($v>0) $update_flag = true;
				}
			}
			if( $update_flag )
			{
				if( $this->update( $row_data, $primary_key_field ) ) $retval = true;
			}
			else
			{
				if( $this->insert( $row_data ) ) $retval = true;
			}
		}
		
		return( $retval );
	}
	
	function get_last_insert_id()
	{
		if( $this->debug ) $this->debug_messages .= "Call get_last_insert_id(); Returns: '{$this->last_insert_id}'\n";
	
		return( $this->last_insert_id );
	}
	
	function get_count( $where='' )
	{
		$retval = 0;
		
		$where_str = '';
		if( $where !='' ) $where_str = " where $where";
		
		if( $this->is_initialized() )
		{
			$query_str = "select count(*) as count from $this->name" . $where_str;
			$result = $this->dbConnection->query( $query_str );
			$result->data_seek(0);
			if( $row = $result->fetch_assoc() )
			{
				$retval = $row['count'];
			}
		}
		
		if( $this->debug ) $this->debug_messages .= "Call get_count($where); Returns: '$retval'\n";
		
		return( $retval );
	}
	
	function delete( $id )
	{
		$this->del( $id );
	}
	
	function del( $id )
	{
		if( $this->debug ) $this->debug_messages .= "Call del('$id')\n";
	
		$retval = false;
		
		if( $id>0 )
		{
			$query_str = "delete from $this->name where id=$id";
			
			if( $this->debug ) $this->debug_messages .= "\t$query_str\n";
			
			$this->result = $this->dbConnection->query( $query_str );
			if( $this->result ) $retval = true;
			
			foreach( $this->associations as $k=>$v )
			{
				if( $v[2]==HAS_MANY )
				{
					//$query_str = "update $v[0] set $v[1]=null where $v[1]=$id";
					$query_str = "delete from $v[0] where $v[1]=$id";
					
					if( $this->debug ) $this->debug_messages .= "\t$query_str\n";
					
					$this->result = $this->dbConnection->query( $query_str );
				}
			}
			
			//Delete records from dictionary
			$sql = "delete from dictionary where folder='{$this->name}' and parent_id={$id}";
			$this->dbConnection->query( $sql );
		}
		
		return( $retval );
	}
	
	/*
		$extended - it's extended clause for where
	*/
	function move_position_up( $id, $folder='', $extended='' )
	{
		if( $this->debug ) $this->debug_messages .= "Call move_position_up($id, '$folder', '$extended')\n";
		
		if( $this->field_exist('position') )
		{
			$delim = '';
			if( $folder )
			{
				$condition = "folder='$folder'";
				$delim = ' and ';
			}
			if( $extended ) $condition .= $delim . $extended;
			
			$this->find_all( $condition, 'position,id' );
			$field_i = null;
			for( $i=0; $i<$this->rowcount; $i++)
			{
				if( $this->data[$i]['id'] == $id ) $field_i = $i;
				$this->data[$i]['position'] = $i+1;
			}
			if( $field_i && $field_i>0 )
			{
				$this->data[$field_i]['position'] = $this->data[$field_i]['position'] - 1;
				$this->data[$field_i-1]['position'] = $this->data[$field_i-1]['position'] + 1;
			}
			for( $i=0; $i<$this->rowcount; $i++)
			{
				$row = array( 'id'=>$this->data[$i]['id'], 'position'=>$this->data[$i]['position'] );
				$this->save( $row );
			}
		}
	}
	
	function move_position_down( $id, $folder='', $extended='' )
	{
		if( $this->debug ) $this->debug_messages .= "Call move_position_down($id, '$folder', '$extended')\n";
		
		if( $this->field_exist('position') )
		{
			$delim = '';
			if( $folder )
			{
				$condition = "folder='$folder'";
				$delim = ' and ';
			}
			if( $extended ) $condition .= $delim . $extended;
			
			$this->find_all( $condition, 'position,id' );
			$field_i = null;
			for( $i=0; $i<$this->rowcount; $i++)
			{
				if( $this->data[$i]['id'] == $id ) $field_i = $i;
				$this->data[$i]['position'] = $i+1;
			}
			if( $field_i>=0 && $field_i<($this->rowcount-1)  )
			{
				$this->data[$field_i]['position'] = $this->data[$field_i]['position'] + 1;
				$this->data[$field_i+1]['position'] = $this->data[$field_i+1]['position'] - 1;
			}
			for( $i=0; $i<$this->rowcount; $i++)
			{
				$row = array( 'id'=>$this->data[$i]['id'], 'position'=>$this->data[$i]['position'] );
				$this->save( $row );
			}
		}
	}
	
	function fix_positions( $folder='', $extended='' )
	{
		if( $this->debug ) $this->debug_messages .= "Call fix_positions('$folder', '$extended')\n";
		
		if( $this->field_exist('position') )
		{
			$delim = ' where ';
			if( $folder )
			{
				$condition = $delim ."folder='$folder'";
				$delim = ' and ';
			}
			if( $extended ) $condition .= $delim . $extended;

			$sql = "update {$this->name} set position=(@rownum:=@rownum+1)". $condition ." order by position,id";
			$this->dbConnection->query( "set @rownum:=0" );
			$this->dbConnection->query( $sql );
			
			if( $this->debug ) $this->debug_messages .= "\tContinue fix_positions(): sql={$sql}\n";
		}
	}
	
	/* 
		Change position in the table. The all fields between src and dst are changed.
		src = ID
		dst = ID
		extended = where additional condition
	*/
	function change_position( $src, $dst, $extended='' )
	{
		$table = $this->name;
		$delim = ' and '; $condition = '';
		if( $extended ) $condition .= $delim . $extended;
		
		if( $this->debug ) $this->debug_messages .= "Call change_position('{$src}','$dst','{$extended}')\n";
		
		if( $src>0 && $dst>0 )
		{
			$rs = $this->dbConnection->query( "select position from {$table} where id={$src}" ); $row = $rs->fetch_row();
			$src_position = $row[0];
			$rs = $this->dbConnection->query( "select position from {$table} where id={$dst}" ); $row = $rs->fetch_row();
			$dst_position = $row[0];
			
			if( $dst_position > $src_position )
			{
				$sql = "select id,position from {$table} where position>{$src_position} and position<={$dst_position} {$condition} order by position,id";
				if( $this->debug ) $this->debug_messages .= "\tsql={$sql}\n";
				$rs = $this->dbConnection->query( $sql );
				while( $row = $rs->fetch_row() )
				{
					$sql = "update {$table} set position={$row[1]}-1 where id={$row[0]}";
					if( $this->debug ) $this->debug_messages .= "\tsql={$sql}\n";
					$this->dbConnection->query( $sql );
				}
			}
			else
			{
				$sql = "select id,position from {$table} where position<{$src_position} and position>={$dst_position} {$condition} order by position,id";
				if( $this->debug ) $this->debug_messages .= "\tsql={$sql}\n";
				$rs = $this->dbConnection->query( $sql );
				while( $row = $rs->fetch_row() )
				{
					$sql = "update {$table} set position={$row[1]}+1 where id={$row[0]}";
					if( $this->debug ) $this->debug_messages .= "\tsql={$sql}\n";
					$this->dbConnection->query( $sql );
				}
			}
			$this->dbConnection->query( "update {$table} set position={$dst_position} where id={$src}" );
		}
		else
		{
			if( $this->debug ) $this->debug_messages .= "\tError in parameters\n";
		}
	}
	
	// sort_way = {0|1} 0==asc; 1==desc
	function MakeSQLForSorting( $sort_field, $sort_way=0 )
	{
		$retval = "select id from {$this->name}";
		
		$order_way = ($sort_way)? ' desc':'asc';
		
		if( $this->field_exist($sort_field) )
		{
			$order = "{$sort_field} {$order_way}";
			$retval .= ' order by '. $order;
		}
		else
		{
			$td = new MysqlTable('dictionary');
			if( $td->find_first("folder='{$this->name}' and name='{$sort_field}'") )
			{
				$order = "d.value {$order_way}";
				$retval = "select t.id from {$this->name} t left join dictionary d on t.id=d.parent_id where d.folder='{$this->name}' and name='{$sort_field}' order by {$order}";
			}
		}
		
		return( $retval );
	}

	function escape_string( $string ) {
		return( $this->dbConnection->real_escape_string($string) );
	}
	##################### /Public methods ###########################

	##################### Private methods ###########################
	function init()
	{
		if( $this->debug ) $this->debug_messages .= "Call init()\n";
		
		$this->associations = array();
		$this->find_first();
		$this->data = array();
	}
	
	function insert( $row_data )
	{
		if( $this->debug ) $this->debug_messages .= "Call insert(..)\n";
		
		$retval = false;
		$names = ''; $values = '';
		$primary_key_field = $this->find_primary_key_field();
		$dictionary = array();

		$delim1 = ' ';
		foreach( $row_data as $k=>$v )
		{
			if( $this->field_exist($k) && $k!=$primary_key_field )
			{
				$names .= $delim1 . $k;
				$values .= $delim1 . $this->make_value( $k, $v );
				$delim1 = ',';
			}
			else
			{
				$dictionary[$k] = $v;
			}
		}
		
		if( $this->field_exist("created_at") )
		{
			$names .= $delim1 . "created_at";
			$now = date( "Y-m-d H:i:s" );
			$values .= $delim1 . "'$now'";
			$delim1 = ',';
		}
		
		$query_str = "insert into $this->name ($names) values ($values)";
		
		if( $this->debug ) $this->debug_messages .= "\t$query_str\n";
		
		$this->result = $this->dbConnection->query( $query_str );
		if( $this->result ) $retval = true;
		$this->last_insert_id = $this->dbConnection->insert_id;
		
		if( $this->last_insert_id>0 && count($dictionary)>0 )
		{
			$lang = 1; //default lang
			if( array_key_exists( 'lang', $dictionary ) )
			{
				$lang = $dictionary['lang'];
			}
			
			$dict_table = new MysqlTable('dictionary');
			foreach( $dictionary as $k=>$v )
			{
				$dict_params = array();
				if( $k!='lang' && $k!='id' )
				{
					$dict_params['parent_id'] = $this->last_insert_id;
					$dict_params['folder'] = $this->name;
					$dict_params['name'] = $k;
					$dict_params['value'] = $dictionary[$k];
					$dict_params['lang'] = $lang;
					$dict_table->save( $dict_params );
				}
			}
		}
		
		return( $retval );
	}
	
	function update( $row_data, $primary_key_field="id" )
	{
		if( $this->debug ) $this->debug_messages .= "Call update(row_data, '$primary_key_field')\n";
		
		$retval = false;
		$dictionary = array();

		$delim1 = ' ';
		if( $primary_key_field )
		{
			$query_str = "update $this->name set ";
			foreach( $row_data as $k=>$v )
			{
				if( $this->field_exist($k) )
				{
					$query_str .= $delim1 . "$k=" . $this->make_value( $k, $v );
					$delim1 = ',';
				}
				else
				{
					if( $k!=$primary_key_field ) $dictionary[$k] = $v;
				}
			}
		
			if( $this->field_exist("updated_at") )
			{
				$now = date( "Y-m-d H:i:s" );
				$query_str .= $delim1 . "updated_at='$now'";
			}
		
			$query_str .= " where $primary_key_field=" . $row_data[$primary_key_field];
			
			if( $this->debug ) $this->debug_messages .= "\t$query_str\n";
			
			$this->result = $this->dbConnection->query( $query_str );
			if( $this->result )
			{
				$retval = true;
				$this->last_insert_id = $row_data[$primary_key_field];
			}
		
			if( $retval && count($dictionary)>0 )
			{
				if( $this->debug ) $this->debug_messages .= "\tBegin updating dictionary\n";
				
				$lang = 1; //default lang
				if( array_key_exists( 'lang', $dictionary ) )
				{
					$lang = $dictionary['lang'];
				}
				
				$dict_table = new MysqlTable('dictionary');
				$parent_id = $row_data[$primary_key_field];
				$folder = $this->name;
				
				foreach( $dictionary as $k=>$v )
				{
					$dict_condition = "parent_id=$parent_id and folder='$folder' and lang='$lang' and name='$k'";
				
					if( $this->debug ) $this->debug_messages .= "\tProcess dictionary parameter: $k=$v\n";
					if( $this->debug ) $this->debug_messages .= "\tDictionary condition is ($dict_condition)\n";
					
					$row = $dict_table->find_first( $dict_condition );
					if( $row!=null )
					{
						if( $this->debug ) $this->debug_messages .= "\tUpdate dictionary intry\n";
					
						$dict_id = $row['id'];
						$dict_params = array();
						if( $k!='lang' )
						{
							$dict_params['id'] = $dict_id;
							$dict_params['parent_id'] = $parent_id;
							$dict_params['folder'] = $folder;
							$dict_params['name'] = $k;
							$dict_params['value'] = $dictionary[$k];
							$dict_params['lang'] = $lang;
							$dict_table->save( $dict_params );
						}
					}
					else
					{
						$dict_params = array();
						if( $k!='lang' && $k!='id' )
						{
							if( $this->debug ) $this->debug_messages .= "\tInsert dictionary entry\n";
							
							$dict_params['parent_id'] = $this->last_insert_id;
							$dict_params['folder'] = $this->name;
							$dict_params['name'] = $k;
							$dict_params['value'] = $dictionary[$k];
							$dict_params['lang'] = $lang;
							$dict_table->save( $dict_params );
						}
					}
				}
				
				if( $this->debug ) $this->debug_messages .= "\tEnd updating dictionary\n";
			}
		}
		return( $retval );
	}
	
	function make_value( $field_name, $field_value )
	{
		$retval = '';

		if( $this->field_is_text($field_name) )
		{
			if( $field_value ) {
				//$val = $this->addslashes($field_value);
				$val = $this->escape_string($field_value);
				$retval = "'$val'";
			}
			else $retval = 'null';
		}
		elseif( $this->field_is_date($field_name) )
		{
			$val = $this->make_date_value($field_value);
			$retval = ($val)? "'$val'" : 'null';
		}
		else
		{
			if( strval($field_value)=='' ) $val = 'null';
			else $val = str_replace(',','.',$field_value);
			$retval = "$val";
		}
		
		if( $this->debug ) $this->debug_messages .= "Call make_value('$field_name', '$field_value'); Returns: '$retval'\n";
		
		return( $retval );
	}

	function make_date_value( $field_value )
	{
		$retval = '';
		$dt = new MysqlDateTime( $field_value );
		if( $dt->IsParsed() ) $retval = $dt->GetMysqlValue();
		
		if( $this->debug ) $this->debug_messages .= "Call make_date_value('$field_value'); Returns: '$retval'\n";
		
		return( $retval );
	}
		
	function field_is_text( $field_name )
	{
		$retval = false;
		$k = $field_name;
		if( $this->fields[$k]['type']=="string" || $this->fields[$k]['type']=="char" || $this->fields[$k]['type']=="varchar" || $this->fields[$k]['type']=="text" || $this->fields[$k]['type']=="blob" )
		{
			$retval = true;
		}
		
		if( $this->debug ) $this->debug_messages .= "Call field_is_text('$field_name'); Returns: '$retval'\n";
		
		return( $retval );
	}
	
	function field_is_date( $field_name )
	{
		$retval = false;
		
		$k = $field_name;
		if( $this->fields[$k]['type']=="datetime" || $this->fields[$k]['type']=="date" || $this->fields[$k]['type']=="timestamp" || $this->fields[$k]['type']=="year" )
		{
			$retval = true;
		}
		
		if( $this->debug ) $this->debug_messages .= "Call field_is_date('$field_name'); Returns: '$retval'\n";
		
		return( $retval );
	}
	
	function find_primary_key_field()
	{
		$retval = false;
		if( $this->isInitialized )
		{
			foreach( $this->fields as $k=>$v )
			{
				if( stristr($v["flags"],"primary_key") )
				{
					$retval = $k;
				}
			}
		}
		
		if( $this->debug ) $this->debug_messages .= "Call find_primary_key_field(); Returns: '$retval'\n";
		
		return( $retval );
	}
	
	function query()
	{
		if( $this->debug ) $this->debug_messages .= "Call query()\n";
		
		if( $this->name )
		{
			if( $this->conditions ) $where = " where $this->conditions"; else $where = "";
			if( $this->order ) $order = " order by $this->order"; else $order = "";
			if( $this->limit ) $limit = "  limit $this->limit"; else $limit = "";
			if( $this->group ) $group = "  group by $this->group"; else $group = "";
			if( $this->distinct ) $distinct = "  distinct "; else $distinct = "";

			$query_str = "select $distinct * from $this->name" . $where . $group . $order . $limit;
			
			if( $this->debug ) $this->debug_messages .= "\t$query_str\n";
			
			$this->result = $this->dbConnection->query( $query_str );
			if( $this->result )
			{
				$this->isInitialized = true;
			}
			else
			{
				$this->isInitialized = false;
			}
			$this->get_fileds_names();

			$this->conditions = "";
			$this->order = "";
			$this->limit = "";
		}
	}

	function get_rows()
	{
		if( $this->debug ) $this->debug_messages .= "Call get_rows()\n";
		
		if( $this->isInitialized )
		{
			unset( $this->data );
			$this->data = array();
			$row_num = 0;
			while( $row = $this->result->fetch_assoc() )
			{
				foreach( $this->fields as $k=>$v )
				{
					$this->data[$row_num][$k] = $this->stripslashes( $row[$k] );
				}

				$row_num++;
			}

			$this->rowcount = $row_num;
		}
	}

	function mysqli_field_name($result, $field_offset)
	{
	   $properties = mysqli_fetch_field_direct($result, $field_offset);
	   return is_object($properties)? $properties->name : null;
	}

	function mysqli_field_type($result, $field_offset)
	{
		$retval = null;
	   $properties = mysqli_fetch_field_direct($result, $field_offset);
	   if( is_object($properties) ) {
	   	switch( $properties->type ) {
	   		case 1:
	   			$retval = 'tinyint'; break;
	   		case 2:
	   			$retval = 'smallint'; break;
	   		case 3:
	   			$retval = 'int'; break;
	   		case 4:
	   			$retval = 'float'; break;
	   		case 5:
	   			$retval = 'double'; break;
	   		case 7:
	   			$retval = 'timestamp'; break;
	   		case 8:
	   			$retval = 'bigint'; break;
	   		case 9:
	   			$retval = 'mediumint'; break;
	   		case 10:
	   			$retval = 'date'; break;
	   		case 11:
	   			$retval = 'time'; break;
	   		case 12:
	   			$retval = 'datetime'; break;
	   		case 13:
	   			$retval = 'year'; break;
	   		case 16:
	   			$retval = 'bit'; break;
	   		case 252:
	   			$retval = 'text'; break;
	   		case 253:
	   			$retval = 'varchar'; break;
	   		case 254:
	   			$retval = 'char'; break;
	   		case 246:
	   			$retval = 'decimal'; break;
	   	}
	   }

	   return( $retval );
	}

	function mysqli_field_flags($result, $field_offset)
	{
		$retval = '';

	   $properties = mysqli_fetch_field_direct($result, $field_offset);
	   if( isset($properties->flags) ) {
	   	$flags = intval( $properties->flags );
	   	if( ($flags&1) == 1 ) $retval .= 'not_null ';
	   	if( ($flags&2) == 2 ) $retval .= 'primary_key ';
	   	if( ($flags&4) == 4 ) $retval .= 'unique_key ';
	   	if( ($flags&8) == 8 ) $retval .= 'multiple_key ';
	   	if( ($flags&16) == 16 ) $retval .= 'blob ';
	   	if( ($flags&32) == 32 ) $retval .= 'unsigned ';
	   	if( ($flags&64) == 64 ) $retval .= 'zerofill ';
	   	if( ($flags&128) == 128 ) $retval .= 'binary ';
	   	if( ($flags&256) == 256 ) $retval .= 'enum ';
	   	if( ($flags&512) == 512 ) $retval .= 'auto_increment ';
	   	if( ($flags&1024) == 1024 ) $retval .= 'timestamp ';
	   	if( ($flags&2048) == 2048 ) $retval .= 'set ';
	   	if( ($flags&16384) == 16384 ) $retval .= 'part_key ';
	   	if( ($flags&32768) == 32768 ) $retval .= 'group ';
	   	if( ($flags&65536) == 65536 ) $retval .= 'unique ';
	   }

	   return( $retval );
	}

	function get_fileds_names()
	{
		if( $this->debug ) $this->debug_messages .= "Call get_fileds_names()\n";
		
		if( $this->isInitialized )
		{
			$this->numFields = $this->result->field_count;
			for( $i=0; $i<$this->numFields; $i++ )
			{
				$this->fields[ strtolower( $this->mysqli_field_name( $this->result, $i ) ) ] = Array( "position"=>$i, "type"=>strtolower( $this->mysqli_field_type($this->result, $i) ), "flags"=>$this->mysqli_field_flags($this->result, $i ) );
				$this->fields[ $this->mysqli_field_name( $this->result, $i ) ] = Array( "position"=>$i, "type"=>strtolower( $this->mysqli_field_type($this->result, $i) ), "flags"=>$this->mysqli_field_flags($this->result, $i ) );
			}
		}
	}
	
	function get_fields()
	{
		$retval = Array();
		
		if( $this->is_initialized() )
		{
			$retval = $this->fields;
		}
		
		if( $this->debug ) $this->debug_messages .= "Call get_fields(); Returns: '$retval'\n";
		
		return( $retval );
	}
	
	function field_exist( $fieldname )
	{
		$retval = false;
		
		foreach( $this->fields as $kf=>$vf )
		{
			if( $kf == $fieldname )
			{
				$retval = true;
				break;
			}
		}
		
		if( $this->debug ) $this->debug_messages .= "Call field_exist($fieldname); Returns: '$retval'\n";
		
		return( $retval );
	}
	
	function get_associations()
	{
		if( $this->debug ) $this->debug_messages .= "Call get_associations()\n";
		
		foreach( $this->associations as $k=>$v )
		{
			$atable = new MysqlTable( $v[0] );
			$where = '';
			if( $v[3]!='' ) $where = ' and '. $v[3];
			if( $v[2]==HAS_ONE )
			{
				for( $i=0; $i<$this->rowcount; $i++ )
				{
					$row = $atable->find_first( "id=" . $this->data[$i][$v[1]] . $where );
					if( $row!=null )
					{
						foreach( $row as $k2=>$v2 )
						{
							$this->data[$i][$k][$k2] = $v2;
						}
					}
				}
			}
			elseif( $v[2]==HAS_MANY )
			{
				for( $i=0; $i<$this->rowcount; $i++ )
				{
					$atable->find_all( $v[1] . "=" . $this->data[$i]['id'] . $where );
					if( $atable->rowcount>0 )
					{
						for( $j=0; $j<$atable->rowcount; $j++ )
						{
							foreach( $atable->data[$j] as $k2=>$v2 )
							{
								$this->data[$i][$k][$j][$k2] = $v2;
							}
						}
					}
				}
			}
		}
	}
	
	function addslashes( $txt )
	{
		$retval = str_replace("'","\\'", $this->stripslashes($txt));
		return( $retval );
	}
	
	function stripslashes( $txt )
	{
		$retval = preg_replace("/[\\\]+'/","'",$txt);
		//$retval = preg_replace("/[\\\]+\"/",'"',$retval);
		return( $retval );
	}
	
	##################### /Private methods ###########################
}
/* /Class implement interface to MySQL table */

/* Class implement interface to MySQL tables */
class MysqlTables
{
	var $views = Array();
	var $relation = Array();
	var $where, $order, $limit;
	var $data = Array();
	var $isInitialized = false;
	var $rowcount = 0;
	var $result;
	
	function MysqlTables()
	{
	}
	
	function add_view( $table_name, $table_synonym, $table_fkey )
	{
		$t = new MysqlTable( $table_name );
		if( $t->is_initialized() )
		{
			array_push( $this->views, Array( 'table_name'=>$table_name, 'table_synonym'=>$table_synonym, 'table_fkey'=>$table_fkey, 'table'=>$t ) );
		}
		$this->try_init();
	}
	
	function add_relation( $table_name, $table_synonym )
	{
		$t = new MysqlTable( $table_name );
		if( $t->is_initialized() )
		{
			$this->relation = Array( 'table_name'=>$table_name, 'table_synonym'=>$table_synonym, 'table'=>$t );
		}
		$this->try_init();
	}
	
	function is_initialized() { return($this->isInitialized); }
	
	function try_init()
	{
		if( count($this->views)>=2 && $this->relation['table_name']!='' )
		{
			$this->isInitialized = true;
		}
	}
	
	function find( $where='', $order='', $limit='' )
	{
		$this->where = $where;
		$this->order = $order;
		$this->limit = $limit;
		
		$this->query();
		$this->get_rows();
		
		return( $this->rowcount );
	}
	
	function query()
	{
		if( $this->is_initialized() )
		{
			$where = ' where ';
			
			$fields = '';
			$tables = '';
			$delim1 = ''; $delim2 = ''; $delim3 = '';
			foreach( $this->views as $view )
			{
				$fs = $view['table']->get_fields();
				foreach( array_keys($fs) as $k )
				{
					$fields .= $delim1 . $view['table_synonym'] .'.'. $k .' as '. $view['table_synonym'] .'_'. $k;
					$delim1 = ',';
				}
				$tables .= $delim2 . $view['table_name'] . ' ' . $view['table_synonym'];
				$delim2 = ',';
				
				$where .= $delim3 . $view['table_synonym'] .'.id='. $this->relation['table_synonym'] .'.'. $view['table_fkey'];
				$delim3 = ' and ';
			}
			
			$tables .= $delim2 . $this->relation['table_name'] . ' ' . $this->relation['table_synonym'];
			
			if( $this->where ) $where .= " and $this->where"; else $where .= "";
			if( $this->order ) $order = " order by $this->order"; else $order = "";
			if( $this->limit ) $limit = "  limit $this->limit"; else $limit = "";

			$query_str = "select $fields from $tables" . $where . $order . $limit;
			//echo "<br>$query_str";
			$this->result = $this->dbConnection->query( $query_str );

			$this->where = "";
			$this->order = "";
			$this->limit = "";
		}
	}
	
	function get_rows()
	{
		if( $this->is_initialized() )
		{
			$this->data = array();
			$row_num = 0;
			while( $row = $this->result->fetch_assoc() )
			{
				foreach( $this->views as $view )
				{
					$fs = $view['table']->get_fields();
					foreach( array_keys($fs) as $k )
					{
						$this->data[$row_num][$view['table_synonym']][$k] = $this->stripslashes( $row[$view['table_synonym'].'_'.$k] );
					}
				}

				$row_num++;
			}

			$this->rowcount = $row_num;
		}
	}
	
	function get_count( $where='' )
	{
		$retval = 0;
	  
		if( $this->is_initialized() )
		{
			$where_str = ' where ';
		
			$tables = '';
			$delim2 = ''; $delim3 = '';
			foreach( $this->views as $view )
			{
				$tables .= $delim2 . $view['table_name'] . ' ' . $view['table_synonym'];
				$delim2 = ',';
				
				$where_str .= $delim3 . $view['table_synonym'] .'.id='. $this->relation['table_synonym'] .'.'. $view['table_fkey'];
				$delim3 = ' and ';
			}
			
			$tables .= $delim2 . $this->relation['table_name'] . ' ' . $this->relation['table_synonym'];
			if( $where !='' ) $where_str .= " and $where";
			
			$query_str = "select count(*) as count from $tables" . $where_str;
			$result = $this->dbConnection->query( $query_str );
			//echo "<br>$query_str";
			if( $row = $result->fetch_assoc() )
			{
				$retval = $row['count'];
			}
		}
		
		return( $retval );
	}
	
	function get_values_as_string( $table_synonym, $table_field, $delim=',' )
	{
		$retval = '';
		
		if( $this->is_initialized() )
		{
			$delim1 = '';
			for( $i=0; $i<$this->rowcount; $i++ )
			{
				$retval .= $delim1 . $this->data[$i][$table_synonym][$table_field];
				$delim1 = $delim;
			}
		}
		
		return( $retval );
	}
	
	function get_min_value( $table_synonym, $table_field )
	{
		$retval = '';
		
		if( $this->is_initialized() && $this->rowcount>0 )
		{
			$min = $this->data[0][$table_synonym][$table_field];
			for( $i=1; $i<$this->rowcount; $i++ )
			{
				if( $this->data[$i][$table_synonym][$table_field] < $min ) $min = $this->data[$i][$table_synonym][$table_field];
			}
			$retval = $min;
		}
		
		return( $retval );
	}
	
	function get_max_value( $table_synonym, $table_field )
	{
		$retval = '';
		
		if( $this->is_initialized() && $this->rowcount>0 )
		{
			$max = $this->data[0][$table_synonym][$table_field];
			for( $i=1; $i<$this->rowcount; $i++ )
			{
				if( $this->data[$i][$table_synonym][$table_field] > $max ) $max = $this->data[$i][$table_synonym][$table_field];
			}
			$retval = $max;
		}
		
		return( $retval );
	}
	
	function addslashes( $txt )
	{
		$retval = str_replace("'","\\'", $this->stripslashes($txt));
		return( $retval );
	}
	
	function stripslashes( $txt )
	{
		$retval = preg_replace("/[\\\]+'/","'",$txt);
		//$retval = preg_replace("/[\\\]+\"/",'"',$retval);
		return( $retval );
	}
}
/* /Class implement interface to MySQL tables */

class MysqlDateTime
{
	var $value;
	var $y, $m, $d, $hh, $mm, $ss;
	var $is_parsed = false;
	
	function MysqlDateTime( $value=null )
	{
		$this->value = $value;
		
		if( $this->IsMysqlDateTime( $this->value ) )
		{
			$this->Parse( $this->value );
		}
		else
		{
			$this->ParseFrontEnd( $this->value );
		}
	}
	
	function IsMysqlDateTime( $value )
	{
		$retval = false;
		if( preg_match( "/([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})/", $value, $regs ) )
		{
			$retval = true;
		}
		
		return( $retval );
	}
	
	/*
		string format: dd.mm.yyyy hh:mm:ss
	*/
	function ParseFrontEnd( $value )
	{
		$this->is_parsed = false;
		
		if( $value )
		{
			$s = '.';
			if( strpos($value,'.') ) $s = '.';
			if( strpos($value,'-') ) $s = '-';
			if( strpos($value,'/') ) $s = '/';
			
			$a = explode( $s, $value );
			if( is_array($a) )
			{
				if( count($a)>0 ) $this->d = $a[0];
				if( count($a)>1 ) $this->m = $a[1];
				if( strpos($value,' ') )
				{
					$part2 = explode( ' ', $a[2] );
					
					if( is_array($part2) )
					{
						$this->y = $part2[0];
						$part3 = explode( ':', $part2[1] );
						if( is_array($part3) )
						{
							if( count($part3)>0 ) $this->hh = $part3[0];
							if( count($part3)>1 ) $this->mm = $part3[1];
							if( count($part3)>2 ) $this->ss = $part3[2];
						}
					}
				
				}
				else
				{
					if( count($a)>2 ) $this->y = $a[2];
				}
			
				if( $this->y || $this->y != '0' ) $this->is_parsed = true;
			}
		}
		
		return( $this->is_parsed );
	}
	
	/*
		string format: yyyy-mm-dd hh:mm:ss
	*/
	function Parse( $value )
	{
		$this->is_parsed = false;
		
		if( $value )
		{
			$a = explode( '-', $value );
			$this->y = $a[0];
			$this->m = $a[1];
			if( strpos($value,' ') )
			{
				$part2 = explode( ' ', $a[2] );
				if( is_array($part2) )
				{
					$this->d = $part2[0];
					$part3 = explode( ':', $part2[1] );
					if( is_array($part3) )
					{
						if( count($part3)>0 ) $this->hh = $part3[0];
						if( count($part3)>1 ) $this->mm = $part3[1];
						if( count($part3)>2 ) $this->ss = $part3[2];
					}
				}
			}
			else
			{
				$this->d = $a[2];
			}
			
			if( $this->y || $this->y != '0' ) $this->is_parsed = true;
		}
		
		return( $this->is_parsed );
	}
	
	function ParseCurrentDateTime()
	{
		$this->Parse( $this->GetCurrentDateTime() );
	}
	
	function GetCurrentDateTime()
	{
		return( date("Y-m-d H:i") );
	}
	
	function IsParsed()
	{
		return( $this->is_parsed );
	}
	
	function GetMysqlValue( $format='y.m.d.hh.mm.ss', $delim='-' )
	{
		return( $this->GetValue( $format, $delim ) );
	}
	
	function GetFrontEndValue( $format='d.m.y.hh.mm.ss', $delim='.' )
	{
		return( $this->GetValue( $format, $delim ) );
	}
	
	function GetValue( $format, $delim='.' )
	{
		$retval = ''; $delim1 = ''; $delim2 = ''; $delim3 = ' ';
		$f = explode( '.', $format );
		
		if( $this->IsParsed() )
		{
			foreach( $f as $fi )
			{
				switch( $fi )
				{
					case 'd':
						if( $this->d )
						{
							$retval .= $delim1 . $this->AddZero($this->d);
							$delim1 = $delim;
						}
						break;
					case 'm':
						if( $this->m )
						{
							$retval .= $delim1 . $this->AddZero($this->m);
							$delim1 = $delim;
						}
						break;
					case 'y':
						if( $this->y )
						{
							$retval .= $delim1 . $this->y;
							$delim1 = $delim;
						}
						break;
					case 'hh':
						if( $this->hh )
						{
							$retval .= $delim3 . $delim2 . $this->AddZero($this->hh);
							$delim2 = ':';
							$delim3 = '';
						}
						break;
					case 'mm':
						if( $this->mm )
						{
							$retval .= $delim3 . $delim2 . $this->AddZero($this->mm);
							$delim2 = ':';
							$delim3 = '';
						}
						break;
					case 'ss':
						if( $this->ss )
						{
							$retval .= $delim3 . $delim2 . $this->AddZero($this->ss);
							$delim2 = ':';
							$delim3 = '';
						}
						break;
				}
			}
		}
		
		return( $retval );
	}
	
	function AddZero( $value, $len=2 )
	{
		$retval = strval( $value );
		$strlen = strlen( $retval );
		
		if( $strlen<$len )
		{
			while( $strlen<$len )
			{
				$retval = '0'. $retval;
				$strlen++;
			}
		}
		
		return( $retval );
	}
}

?>
<?php
/*****************************************************
 Class v.1.0, 2008
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

/* Encodings: windows-1251, utf-8, cp866, koi8-r, ... */
class CyrLetters
{
    var $encoding = 'windows-1251';
    var $letters_win1251 = Array(
        'a'=>'�',
        'b'=>'�',
        'v'=>'�',
        'g'=>'�',
        'd'=>'�',
        'e'=>'�',
        'yo'=>'�',
        'ge'=>'�',
        'z'=>'�',
        'i'=>'�',
        'iy'=>'�',
        'k'=>'�',
        'l'=>'�',
        'm'=>'�',
        'n'=>'�',
        'o'=>'�',
        'p'=>'�',
        'r'=>'�',
        's'=>'�',
        't'=>'�',
        'u'=>'�',
        'f'=>'�',
        'x'=>'�',
        'c'=>'�',
        'ch'=>'�',
        'sh'=>'�',
        'shya'=>'�',
        'tz'=>'�',
        'ii'=>'�',
        'mz'=>'�',
        'ee'=>'�',
        'yu'=>'�',
        'ya'=>'�'
    );
    
    function __construct( $encoding='windows-1251' )
    {
        $this->encoding = $encoding;
    }
    
    function Get( $key )
    {
        $retval = "";
        
        if( array_key_exists( $key, $this->letters_win1251 ) )
        {
            if( $this->encoding == 'windows-1251' )
            {
                $retval = $this->letters_win1251[ $key ];
            }
            else
            {
                $retval = iconv( 'windows-1251', $this->encoding, $this->letters_win1251[ $key ] );
            }
        }
        
        return( $retval );
    }
}

class Stemmer
{
    var $encoding;
    var $letters;
    var $perfective_gerund1, $perfective_gerund2, $reflexive, $adjective, $participle, $verb, $noun, $derivational, $superlative;
    var $debug = false, $debug_str = "", $debug_delim = ", ";
    
    function __construct( $encoding='windows-1251' )
    {
        $this->encoding = $encoding;
        $this->letters = new CyrLetters( $encoding );
        $this->perfective_gerund1 = Array(
            $this->letters->Get('v'),
            $this->letters->Get('v') . $this->letters->Get('sh') . $this->letters->Get('i'),
            $this->letters->Get('v') . $this->letters->Get('sh') . $this->letters->Get('i') . $this->letters->Get('s') . $this->letters->Get('mz')
        );
        $this->perfective_gerund2 = Array(
            $this->letters->Get('i') . $this->letters->Get('v'),
            $this->letters->Get('i') . $this->letters->Get('v') . $this->letters->Get('sh') . $this->letters->Get('i'),
            $this->letters->Get('i') . $this->letters->Get('v') . $this->letters->Get('sh') . $this->letters->Get('i') . $this->letters->Get('s') . $this->letters->Get('mz'),
            $this->letters->Get('ii') . $this->letters->Get('v'),
            $this->letters->Get('ii') . $this->letters->Get('v') . $this->letters->Get('sh') . $this->letters->Get('i'),
            $this->letters->Get('ii') . $this->letters->Get('v') . $this->letters->Get('sh') . $this->letters->Get('i') . $this->letters->Get('s') . $this->letters->Get('mz')
        );
        $this->reflexive = Array(
            $this->letters->Get('s') . $this->letters->Get('ya'),
            $this->letters->Get('s') . $this->letters->Get('mz')
        );
        $this->adjective = Array(
            $this->letters->Get('e') . $this->letters->Get('e'),
            $this->letters->Get('i') . $this->letters->Get('e'),
            $this->letters->Get('ii') . $this->letters->Get('e'),
            $this->letters->Get('o') . $this->letters->Get('e'),
            $this->letters->Get('i') . $this->letters->Get('m') . $this->letters->Get('i'),
            $this->letters->Get('ii') . $this->letters->Get('m') . $this->letters->Get('i'),
            $this->letters->Get('e') . $this->letters->Get('iy'),
            $this->letters->Get('i') . $this->letters->Get('iy'),
            $this->letters->Get('ii') . $this->letters->Get('iy'),
            $this->letters->Get('o') . $this->letters->Get('iy'),
            $this->letters->Get('e') . $this->letters->Get('m'),
            $this->letters->Get('i') . $this->letters->Get('m'),
            $this->letters->Get('ii') . $this->letters->Get('m'),
            $this->letters->Get('o') . $this->letters->Get('m'),
            $this->letters->Get('e') . $this->letters->Get('g') . $this->letters->Get('o'),
            $this->letters->Get('o') . $this->letters->Get('g') . $this->letters->Get('o'),
            $this->letters->Get('e') . $this->letters->Get('m') . $this->letters->Get('u'),
            $this->letters->Get('o') . $this->letters->Get('m') . $this->letters->Get('u'),
            $this->letters->Get('i') . $this->letters->Get('x'),
            $this->letters->Get('ii') . $this->letters->Get('x'),
            $this->letters->Get('u') . $this->letters->Get('yu'),
            $this->letters->Get('yu') . $this->letters->Get('yu'),
            $this->letters->Get('a') . $this->letters->Get('ya'),
            $this->letters->Get('ya') . $this->letters->Get('ya'),
            $this->letters->Get('o') . $this->letters->Get('yu'),
            $this->letters->Get('e') . $this->letters->Get('yu')
        );
        $this->participle = Array(
            $this->letters->Get('i') . $this->letters->Get('v') . $this->letters->Get('sh'),
            $this->letters->Get('ii') . $this->letters->Get('v') . $this->letters->Get('sh'),
            $this->letters->Get('u') . $this->letters->Get('yu') . $this->letters->Get('shya'),
            $this->letters->Get('e') . $this->letters->Get('m'),
            $this->letters->Get('n') . $this->letters->Get('n'),
            $this->letters->Get('v') . $this->letters->Get('sh'),
            $this->letters->Get('yu') . $this->letters->Get('shya'),
            $this->letters->Get('shya')
        );
        $this->verb = Array(
            $this->letters->Get('i') . $this->letters->Get('l') . $this->letters->Get('a'),
            $this->letters->Get('ii') . $this->letters->Get('l') . $this->letters->Get('a'),
            $this->letters->Get('e') . $this->letters->Get('n') . $this->letters->Get('a'),
            $this->letters->Get('e') . $this->letters->Get('iy') . $this->letters->Get('t') . $this->letters->Get('e'),
            $this->letters->Get('u') . $this->letters->Get('iy') . $this->letters->Get('t') . $this->letters->Get('e'),
            $this->letters->Get('i') . $this->letters->Get('t') . $this->letters->Get('e'),
            $this->letters->Get('i') . $this->letters->Get('l') . $this->letters->Get('l'),
            $this->letters->Get('ii') . $this->letters->Get('l') . $this->letters->Get('l'),
            $this->letters->Get('e') . $this->letters->Get('iy'),
            $this->letters->Get('u') . $this->letters->Get('iy'),
            $this->letters->Get('i') . $this->letters->Get('l'),
            $this->letters->Get('ii') . $this->letters->Get('l'),
            $this->letters->Get('i') . $this->letters->Get('m'),
            $this->letters->Get('ii') . $this->letters->Get('m'),
            $this->letters->Get('e') . $this->letters->Get('n'),
            $this->letters->Get('i') . $this->letters->Get('l') . $this->letters->Get('o'),
            $this->letters->Get('ii') . $this->letters->Get('l') . $this->letters->Get('o'),
            $this->letters->Get('e') . $this->letters->Get('n') . $this->letters->Get('o'),
            $this->letters->Get('ya') . $this->letters->Get('t'),
            $this->letters->Get('u') . $this->letters->Get('e') . $this->letters->Get('t'),
            $this->letters->Get('u') . $this->letters->Get('yu') . $this->letters->Get('t'),
            $this->letters->Get('i') . $this->letters->Get('t'),
            $this->letters->Get('ii') . $this->letters->Get('t'),
            $this->letters->Get('e') . $this->letters->Get('n') . $this->letters->Get('ii'),
            $this->letters->Get('i') . $this->letters->Get('t') . $this->letters->Get('mz'),
            $this->letters->Get('ii') . $this->letters->Get('t') . $this->letters->Get('mz'),
            $this->letters->Get('i') . $this->letters->Get('sh') . $this->letters->Get('mz'),
            $this->letters->Get('u') . $this->letters->Get('yu'),
            $this->letters->Get('yu'),
            
            $this->letters->Get('l') . $this->letters->Get('a'),
            $this->letters->Get('n') . $this->letters->Get('a'),
            $this->letters->Get('e') . $this->letters->Get('t') . $this->letters->Get('e'),
            $this->letters->Get('iy') . $this->letters->Get('t') . $this->letters->Get('e'),
            $this->letters->Get('l') . $this->letters->Get('i'),
            $this->letters->Get('iy'),
            $this->letters->Get('l'),
            $this->letters->Get('e') . $this->letters->Get('m'),
            $this->letters->Get('n'),
            $this->letters->Get('l') . $this->letters->Get('o'),
            $this->letters->Get('n') . $this->letters->Get('o'),
            $this->letters->Get('e') . $this->letters->Get('t'),
            $this->letters->Get('yu') . $this->letters->Get('t'),
            $this->letters->Get('n') . $this->letters->Get('ii'),
            $this->letters->Get('t') . $this->letters->Get('mz'),
            $this->letters->Get('e') . $this->letters->Get('sh'),
            $this->letters->Get('n') . $this->letters->Get('n') . $this->letters->Get('o'),
        );
        $this->noun = Array(
            $this->letters->Get('a'),
            $this->letters->Get('e') . $this->letters->Get('v'),
            $this->letters->Get('o') . $this->letters->Get('v'),
            $this->letters->Get('i') . $this->letters->Get('e'),
            $this->letters->Get('ii') . $this->letters->Get('e'),
            $this->letters->Get('mz') . $this->letters->Get('e'),
            $this->letters->Get('e'),
            $this->letters->Get('i') . $this->letters->Get('ya') . $this->letters->Get('m') . $this->letters->Get('i'),
            $this->letters->Get('ya') . $this->letters->Get('m') . $this->letters->Get('i'),
            $this->letters->Get('a') . $this->letters->Get('m') . $this->letters->Get('i'),
            $this->letters->Get('e') . $this->letters->Get('i'),
            $this->letters->Get('i') . $this->letters->Get('i'),
            $this->letters->Get('i'),
            $this->letters->Get('i') . $this->letters->Get('e') . $this->letters->Get('iy'),
            $this->letters->Get('e') . $this->letters->Get('iy'),
            $this->letters->Get('o') . $this->letters->Get('iy'),
            $this->letters->Get('i') . $this->letters->Get('iy'),
            $this->letters->Get('iy'),
            $this->letters->Get('i') . $this->letters->Get('ya') . $this->letters->Get('m'),
            $this->letters->Get('ya') . $this->letters->Get('m'),
            $this->letters->Get('i') . $this->letters->Get('e') . $this->letters->Get('m'),
            $this->letters->Get('e') . $this->letters->Get('m'),
            $this->letters->Get('a') . $this->letters->Get('m'),
            $this->letters->Get('o') . $this->letters->Get('m'),
            $this->letters->Get('o'),
            $this->letters->Get('u'),
            $this->letters->Get('a') . $this->letters->Get('x'),
            $this->letters->Get('i') . $this->letters->Get('ya') . $this->letters->Get('x'),
            $this->letters->Get('ya') . $this->letters->Get('x'),
            $this->letters->Get('ii'),
            $this->letters->Get('mz'),
            $this->letters->Get('i') . $this->letters->Get('yu'),
            $this->letters->Get('mz') . $this->letters->Get('yu'),
            $this->letters->Get('yu'),
            $this->letters->Get('i') . $this->letters->Get('ya'),
            $this->letters->Get('mz') . $this->letters->Get('ya'),
            $this->letters->Get('ya'),
        );
        $this->derivational = Array(
            $this->letters->Get('o') . $this->letters->Get('s') . $this->letters->Get('t'),
            $this->letters->Get('o') . $this->letters->Get('s') . $this->letters->Get('t') . $this->letters->Get('mz'),
        );
        $this->superlative = Array(
            $this->letters->Get('e') . $this->letters->Get('iy') . $this->letters->Get('sh') . $this->letters->Get('e'),
            $this->letters->Get('e') . $this->letters->Get('iy') . $this->letters->Get('sh')
        );
    }
    
    function Get( $word )
    {
        $retval = $word;
        $step1 = true;
        
        if( $word )
        {
            mb_internal_encoding( $this->encoding );
            $retval = mb_strtolower( $word );
            
            if( $step1 )
            {
                if( $this->debug ) $this->debug_str .= "perfective_gerund2: ";
                foreach( $this->perfective_gerund2 as $v )
                {
                    $len = mb_strlen($v);
                    $vv = mb_substr( $retval, -1*$len, $len );
                    if( $this->debug ) $this->debug_str .= "v={$v} vv={$vv}" . $this->debug_delim;
                    if( strcmp($vv,$v) == 0 )
                    {
                        $retval = mb_substr( $retval, 0, mb_strlen($retval)-$len );
                        $step1 = false;
                        break;
                    }
                }
                if( $this->debug ) $this->debug_str .= "result={$retval}" . $this->debug_delim;
            }
            
            if( $step1 )
            {
                if( $this->debug ) $this->debug_str .= "perfective_gerund1: ";
                foreach( $this->perfective_gerund1 as $v )
                {
                    $len = mb_strlen($v);
                    $vv = mb_substr( $retval, -1*$len, $len );
                    if( $this->debug ) $this->debug_str .= "v={$v} vv={$vv}" . $this->debug_delim;
                    if( strcmp($vv,$v) == 0 )
                    {
                        $retval = mb_substr( $retval, 0, mb_strlen($retval)-$len );
                        $step1 = false;
                        break;
                    }
                }
                if( $this->debug ) $this->debug_str .= "result={$retval}" . $this->debug_delim;
            }
            
            if( $step1 )
            {
                if( $this->debug ) $this->debug_str .= "reflexive: ";
                foreach( $this->reflexive as $v )
                {
                    $len = mb_strlen($v);
                    $vv = mb_substr( $retval, -1*$len, $len );
                    if( $this->debug ) $this->debug_str .= "v={$v} vv={$vv}" . $this->debug_delim;
                    if( strcmp($vv,$v) == 0 )
                    {
                        $retval = mb_substr( $retval, 0, mb_strlen($retval)-$len );
                        //$step1 = false;
                        break;
                    }
                }
                if( $this->debug ) $this->debug_str .= "result={$retval}" . $this->debug_delim;
            }
            
            if( $step1 )
            {
                if( $this->debug ) $this->debug_str .= "adjective: ";
                foreach( $this->adjective as $v )
                {
                    $len = mb_strlen($v);
                    $vv = mb_substr( $retval, -1*$len, $len );
                    if( $this->debug ) $this->debug_str .= "v={$v} vv={$vv}" . $this->debug_delim;
                    if( strcmp($vv,$v) == 0 )
                    {
                        $retval = mb_substr( $retval, 0, mb_strlen($retval)-$len );
                        //$step1 = false;
                        break;
                    }
                }
                if( $this->debug ) $this->debug_str .= "result={$retval}" . $this->debug_delim;
                
                if( $this->debug ) $this->debug_str .= "participle: ";
                foreach( $this->participle as $v )
                {
                    $len = mb_strlen($v);
                    $vv = mb_substr( $retval, -1*$len, $len );
                    if( $this->debug ) $this->debug_str .= "v={$v} vv={$vv}" . $this->debug_delim;
                    if( strcmp($vv,$v) == 0 )
                    {
                        $retval = mb_substr( $retval, 0, mb_strlen($retval)-$len );
                        //$step1 = false;
                        break;
                    }
                }
                if( $this->debug ) $this->debug_str .= "result={$retval}" . $this->debug_delim;
            }
            
            if( $step1 )
            {
                if( $this->debug ) $this->debug_str .= "verb: ";
                foreach( $this->verb as $v )
                {
                    $len = mb_strlen($v);
                    $vv = mb_substr( $retval, -1*$len, $len );
                    if( $this->debug ) $this->debug_str .= "v={$v} vv={$vv}" . $this->debug_delim;
                    if( strcmp($vv,$v) == 0 )
                    {
                        $retval = mb_substr( $retval, 0, mb_strlen($retval)-$len );
                        break;
                    }
                }
                if( $this->debug ) $this->debug_str .= "result={$retval}" . $this->debug_delim;
            }
            
            if( $step1 )
            {
                if( $this->debug ) $this->debug_str .= "noun: ";
                foreach( $this->noun as $v )
                {
                    $len = mb_strlen($v);
                    $vv = mb_substr( $retval, -1*$len, $len );
                    if( $this->debug ) $this->debug_str .= "v={$v} vv={$vv}" . $this->debug_delim;
                    if( strcmp($vv,$v) == 0 )
                    {
                        $retval = mb_substr( $retval, 0, mb_strlen($retval)-$len );
                        break;
                    }
                }
                if( $this->debug ) $this->debug_str .= "result={$retval}" . $this->debug_delim;
            }
            
            //Remove i
            $vv = mb_substr( $retval, -1, 1 );
            if( strcmp( $this->letters->Get('i'), $vv ) == 0 )
            {
                $retval = mb_substr( $retval, 0, mb_strlen($retval)-1 );
                if( $this->debug ) $this->debug_str .= "remove(i)={$retval}" . $this->debug_delim;
            }
            
            if( $this->debug ) $this->debug_str .= "derivational: ";
            foreach( $this->derivational as $v )
            {
                $len = mb_strlen($v);
                $vv = mb_substr( $retval, -1*$len, $len );
                if( $this->debug ) $this->debug_str .= "v={$v} vv={$vv}" . $this->debug_delim;
                if( strcmp($vv,$v) == 0 )
                {
                    $retval = mb_substr( $retval, 0, mb_strlen($retval)-$len );
                    break;
                }
            }
            if( $this->debug ) $this->debug_str .= "result={$retval}" . $this->debug_delim;
            
            if( $this->debug ) $this->debug_str .= "superlative: ";
            foreach( $this->superlative as $v )
            {
                $len = mb_strlen($v);
                $vv = mb_substr( $retval, -1*$len, $len );
                if( $this->debug ) $this->debug_str .= "v={$v} vv={$vv}" . $this->debug_delim;
                if( strcmp($vv,$v) == 0 )
                {
                    $retval = mb_substr( $retval, 0, mb_strlen($retval)-$len );
                    break;
                }
            }
            if( $this->debug ) $this->debug_str .= "result={$retval}" . $this->debug_delim;
            
            //Remove mz
            $vv = mb_substr( $retval, -1, 1 );
            if( strcmp( $this->letters->Get('mz'), $vv ) == 0 )
            {
                $retval = mb_substr( $retval, 0, mb_strlen($retval)-1 );
                if( $this->debug ) $this->debug_str .= "remove(mz)={$retval}" . $this->debug_delim;
            }
        }
        
        return( $retval );
    }
}

?>
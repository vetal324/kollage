<?php
/***********************************************************************
 Class v.1.0.1, 2008, PHP5
    Detect a type of the browser
    type
        0 - unknown
        1 - opera
        2 - IE
        3 - Safari
        4 - Firefox
        5 - Chrome
 Copyright Andrey Nebogin anebogin@gmail.com
*************************************************************************/

class ABrowserDetect
{
    var $userAgentStr;
    var $_isOpera = false, $_isIE = false, $_isIE7 = false, $_isSafari = false, $_isSafari3 = false, $_isGecko = false, $_isChrome = false, $_isFirefox = false;
    var $_isWindows = false, $_isMac = false, $_isLinux = false;
    var $_type = 0;
    
    function __construct()
    {
        $this->userAgentStr = strtolower( $_SERVER['HTTP_USER_AGENT'] );
        $this->Init();
    }
    
    function Init()
    {
        if( preg_match('/opera/',$this->userAgentStr) )
        {
            $this->_isOpera = true;
            $this->_type = 1;
        }
        
        if( preg_match('/msie/',$this->userAgentStr) )
        {
            $this->_isIE = true;
            $this->_type = 2;
        }
        
        if( preg_match('/msie 7/',$this->userAgentStr) ) $this->_isIE7 = true;
        
        if( preg_match('/webkit/',$this->userAgentStr) || preg_match('/khtml/',$this->userAgentStr) )
        {
            $this->_isSafari = true;
            $this->_type = 3;
        }
        
        if( $this->_isSafari && preg_match('/version\/3\./',$this->userAgentStr) ) $this->_isSafari3 = true;
        
        if( preg_match('/gecko/',$this->userAgentStr) ) $this->_isGecko = true;
        
        if( preg_match('/firefox/',$this->userAgentStr) )
        {
            $this->_isFirefox = true;
            $this->_type = 4;
        }
        
        if( preg_match('/chrome/',$this->userAgentStr) )
        {
            $this->_isChrome = true;
            $this->_type = 5;
        }
        
        if( preg_match('/windows/',$this->userAgentStr) ) $this->_isWindows = true;
        
        if( preg_match('/macintosh/',$this->userAgentStr) ) $this->_isMac = true;
        
        if( preg_match('/linux/',$this->userAgentStr) ) $this->_isLinux = true;
    }
    
    function GetType() { return( $this->_type ); }
    function IsOpera() { return( $this->_isOpera ); }
    function IsIE() { return( $this->_isIE ); }
    function IsIE7() { return( $this->_isIE7 ); }
    function IsSafari() { return( $this->_isSafari ); }
    function IsSafari3() { return( $this->_isSafari3 ); }
    function IsGecko() { return( $this->_isGecko ); }
    function IsFirefox() { return( $this->_isFirefox ); }
    function IsChrome() { return( $this->_isChrome ); }
    function IsWindows() { return( $this->_isWindows ); }
    function IsMac() { return( $this->_isMac ); }
    function IsLinux() { return( $this->_isLinux ); }
}

$bd = new ABrowserDetect();

?>
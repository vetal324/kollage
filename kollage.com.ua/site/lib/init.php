<?php

ini_set( 'display_errors', 'Off' );
ini_set( 'log_errors', 'On' );
date_default_timezone_set('UTC');

//error_reporting(E_ALL);
error_reporting( E_ALL & ~E_NOTICE );
//error_reporting( E_ALL & ~E_NOTICE & ~E_WARNING );

#Use PEAR
include("Net/IPv4.php");

include( 'aimage.php' );
include( 'logging.php' );
include( 'amail2.php' );
include( 'amail3.php' );
include( 'afiletype.php' );
include( 'stemmer.php' );
include( 'attachment.php' );
include( 'mysqldb.php' );
include( 'welements.php' );
include( 'dictionary.php' );
include( 'model.php' );
include( 'abd.php' );
include( 'protect_image.php' );
include( 'JsHttpRequest.php' );
include( 'aexif.php' );

change_lang( $_SESSION['lang'] );
include_models();

$dsn = Array( "host"=>$_ENV['db_host'], "dbname"=>$_ENV['db_name'], "user"=>$_ENV['db_user'], "password"=>$_ENV['db_password'] );
$db = new MysqlDB( 'utf8' );
$dbresult = $db->connect( $dsn, false );

$log = new Logging( SITEROOT ."/site.log" );

change_currency( $_SESSION['currency_id'] );

?>
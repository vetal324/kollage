<?php
/*****************************************************
 Class v.1.0, 2007
 It's a class for emulating cron functions
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

define( 'SITEROOT', realpath( dirname(__FILE__) . '/../' ) );

class Cron
{
    var $items;
    
    var $loaded = false;
    var $tablename = 'cron';

    function __construct()
    {
        $this->items = Array();
        $this->Load();
    }
    
    function Load()
    {
        $this->loaded = false;
        
        $t = new MysqlTable( $this->tablename );
        if( $t->find_all("status=1") )
        {
            $this->_Load( $t->data );
        }
    }
    
    function _Load( &$rows )
    {
        foreach( $rows as $row )
        {
            $this->items[ intval($row['id']) ] = Array( 
                'id' => intval($row['id']),
                'last_start' => intval($row['last_start']),
                'period' => intval($row['period']),
                'status' => intval($row['status']),
                'script' => $row['script']
                );
        }
        
        $this->loaded = true;
    }

    function IsLoaded()
    {
        return( $this->loaded );
    }
    
    function Run( $script )
    {
        $retval = false;
        
        //$script = SITEROOT .'/'. $script;
        //if( file_exists( $script ) )
        if( file_exists( SITEROOT .'/'. $script ) )
        {
            exec( "php {$script} >/dev/null &" );
            $retval = true;
        }
        
        return( $retval );
    }
    
    function ProcessItems()
    {
        global $db;

        $now = time();
        
        foreach( $this->items as $item )
        {
            if( ($item['last_start']+$item['period']) <= $now )
            {
                if( $this->Run( $item['script'] ) )
                {
                    $db->query( "update {$this->tablename} set last_start='{$now}' where id={$item['id']}" );
                }
            }
        }
    }
}

?>
<?php
/***********************************************************************
 Class v.1.2.2, 2008, PHP5
    Send email
 Copyright Andrey Nebogin anebogin@gmail.com
*************************************************************************/

class AMail
{
    var $subject, $body;
    var $from_name, $from_email, $reply_to;
    var $to_email;
    var $cc, $bcc;
    var $attachments;
    var $boundary;
    var $charset, $in_charset;
    
    function __construct( $charset='ISO-8859-1', $in_charset='UTF-8' )
    {
        $this->charset = $charset;
        $this->in_charset = $in_charset;
        $this->MakeBoundary();
    }
    
    function __destruct()
    {
    }

    function AttachFile( $fname, $name )
    {
        $retval = false;
        
        $name = $this->ConvertString( $name );
        
        if( !empty( $fname ) )
        {
            if( $fp = fopen( $fname, 'r' ) )
            {
                $data = fread( $fp, filesize( $fname ) );
                fclose( $fp );
    
                $data = base64_encode( $data );
                $data = chunk_split( $data, 76, "\n" );
    
                $head = "--{$this->boundary}\n";
                $head .= "Content-Type: application/octet-stream; name=\"$name\"\n";
                $head .= "Content-Transfer-Encoding: base64\n";
                $head .= "Content-Disposition: attachment; filename=\"$name\"\n\n";
    
                $attach = $head . $data;
            
                $this->attachments .= $attach;
                $retval = true;
            }
        }
        
        return( $retval );
    }

    function AttachImage( $fname, $name )
    {
        $retval = false;
        
        $name = $this->ConvertString( $name );
        
        if( !empty( $fname ) )
        {
            if( $fp = fopen( $fname, 'r' ) )
            {
                if( !$fp ) { return( '' ); }
                $data = fread( $fp, filesize( $fname ) );
                fclose( $fp );
    
                $data = base64_encode( $data );
                $data = chunk_split( $data, 76, "\n" );
        
                $image_type = 'gif';
    
                $head = "\n--{$this->boundary}\n";
                $head .= "Content-Type: image/$image_type; name=\"$name\"\n";
                $head .= "Content-Transfer-Encoding: base64\n";
                $head .= "Content-ID: <". $name .">\n\n";
    
                $attach = $head . $data;
    
                $this->attachments .= $attach;
                $retval = true;
            }
        }
        
        return( $retval );
    }

    function AttachHtml( $txt )
    {
        $retval = false;
        
        if( $txt )
        {
            $this->attachments .= $this->PrepareHtml( $txt );
            $retval = true;
        }
        
        return( $retval );
    }

    function PrepareHtml( $txt )
    {
        $retval = "";
        
        $txt = $this->ConvertString( $txt );
        
        if( $txt )
        {
            $head = "\n--{$this->boundary}\n";
            $head .= "Content-Type: text/html; charset={$this->charset}\n";
            $head .= "Content-Transfer-Encoding: quoted-printable\n";
            $head .= "Content-Disposition: inline\n\n";
            $head .= quoted_printable_encode($txt) ."\n";
    
            $retval = $head;
        }
        
        return( $retval );
    }

    function MakeBoundary( $prefix = 'ST' )
    {
        $b = '----=' . $prefix . md5( uniqid( rand() ) );
        $this->boundary = $b;
        
        return( $b );
    }
    
    function Send( $body )
    {
        $retval = false;
        
        if( $this->from_email && $this->to_email && $this->subject && $body )
        {
            $this->body = "";
            $this->body .= "This is a multi-part message in MIME format.\n";
            $this->body .= $this->PrepareHtml( $body );
            $this->body .= $this->attachments;
            $this->body .= "--{$this->boundary}--";
            
            $headers = "MIME-Version: 1.0\n";
            $headers.= sprintf("Content-type: %s;\n", 'multipart/related');
            $headers.= sprintf("\tboundary=\"%s\"\n", $this->boundary);
            $headers.= sprintf("From: \"%s\" %s\n", $this->ConvertString($this->from_name), '<'.$this->from_email.'>');
            
            if( $this->reply_to ) $headers.= sprintf("Reply-To: %s\n", $this->reply_to );
            if( $this->cc ) $headers.= sprintf( "Cc: %s\n", $this->cc );
            if( $this->bcc ) $headers.= sprintf( "Bcc: %s\n", $this->bcc );
            
            $headers.= "X-Priority: 3";
            
            $to = $this->to_email;
            
            //$subject = "=?{$this->charset}?Q?". quoted_printable_encode( $this->subject ) ."?=";
            $subject = "=?{$this->charset}?B?". base64_encode( $this->ConvertString($this->subject) ) ."?=";
            
            $retval = mail( $to, $subject, $this->body, $headers );
            $this->body = "";
        }
        
        return( $retval );
    }
    
    function ConvertString( $str )
    {
        $retval = '';
        
        if( function_exists('iconv') )
        {
            $retval = iconv( $this->in_charset, $this->charset, $str );
        }
        else
        {
            $retval = utf8_decode( $str );
        }
        
        return( $retval );
    }
}

function quoted_printable_encode_character( $matches )
{
   $character = $matches[0];
   return sprintf( '=%02x', ord( $character ) );
}

function quoted_printable_encode( $string )
{
   $string = preg_replace_callback(
     '/[^\x21-\x3C\x3E-\x7E\x09\x20]/',
     'quoted_printable_encode_character',
     $string
   );
   $newline = "=\n";
   $string = preg_replace( '/(.{73}[^=]{0,3})/', '$1'.$newline, $string);
   
   return $string;
}

?>
<?php

define( "LANG_RU", 'ru' );
define( "LANG_UA", 'ua' );
define( "LANG_EN", 'en' );
define( "LANG_NO", 'no' );
define( "LANG_SE", 'se' );
define( "LANG_DEFAULT", LANG_RU );

define( 'SITEROOT', realpath( dirname(__FILE__) . '/../' ) );

$script = ($_SERVER['SCRIPT_NAME'])? $_SERVER['SCRIPT_NAME'] : $_SERVER['SCRIPT_FILENAME'];
define( 'SITEPREFIX', preg_replace( "/admin\/*[^\/]+.php/", "", $script) );

function load_configuration()
{
    $config = array();
    include( realpath(SITEROOT.'/config.php') );
    $config['env'] = $_ENV;
    $_ENV = $config;
}

function parse_html_query()
{
    $query = $_SERVER['QUERY_STRING'];

    $parts = explode('&', $query);
    if( is_array($parts) )
    {
        $classpath = $parts[0];
        $parameters = Array();
    }

    $parts = explode( '.', $classpath );
    $class = $parts[0];
    $method = isset($parts[1])? $parts[1] : '';
    $method = str_replace( '=', '', $method );

    $a_parameters = $parts;
    array_shift($a_parameters);

    foreach( $a_parameters as $p )
    {
        $parts = explode( '=', $p );
        $key = $parts[0];
        $value = ( isset($parts[1]) )? $parts[1] : '';
        $parameters[$key] = $value;
    }

    return Array( $class, $method, $parameters );
}

function include_models()
{
    $path = realpath( SITEROOT .'/'. $_ENV['app_path'] .'/'. $_ENV['models_path'] );
    $dir = opendir( $path );
    
    $classes = Array();

    while( $class = readdir($dir) )
    {
        if( substr($class, -4) == '.php')
        {
            include_class( $path, $class );
        }
    }
}

function include_class( $path, $class )
{
    $filepath = $path .'/'. $class;
    if( is_file($filepath) ) include_once( $filepath );
}

function &load_controller( $class )
{
    $retval = null;
    $class .= '_controller';
    $filepath = realpath( sprintf('%s/%s/%s.php', SITEROOT, $_ENV['app_path'] .'/'. $_ENV['controllers_path'], $class) );

    if( is_file($filepath) )
    {
        include_once $filepath;
        if( class_exists($class) ) {
            $retval = new $class;
            //$reflectionClass = new ReflectionClass($class);
            //$retval = $reflectionClass->newInstance();
            return( $retval );
        }
        else {
            user_error( sprintf('Class name \'%s\' doesnt exist', $class), E_USER_ERROR );
        }
    }
    else
    {
        user_error( sprintf('Undefined class name \'%s\'', $class), E_USER_ERROR );
    }
}

function run_action( &$document, $method, $parameters=array() )
{
    $retval = false;
    
    if( method_exists($document, $method) )
    {
        call_user_func_array( array($document, $method), $parameters );
        //call_user_method_array( $method, $document, $parameters );
        $retval = true;
    }
    
    return( $retval );
}

function run_action_old( &$document, $method, $parameters=array() )
{
    if( method_exists($document, $method) )
    {
        call_user_func_array( array($document, $method), $parameters );
        //call_user_method_array( $method, $document, $parameters );
    }
    else
    {
        user_error( sprintf('Call to undefined method %s()', $method), E_USER_ERROR );
    }
}

function initialize_session( $name )
{
    //session_set_cookie_params( 3600 * $_ENV['session_lifetime'] );
        
    if( isset($_REQUEST["PHPSESSID"]) )
    {
        session_id( $_REQUEST["PHPSESSID"] );
    }
    session_name( $name );
    session_start();
}

function xml_header()
{
    return '<?xml version="1.0" encoding="utf-8"?>';
}

function redirect( $url )
{
    if( !headers_sent() )
    {
        header( sprintf('Location: %s', $url) );
    }
    else
    {
        printf( '<script type="text/javascript">document.location.href=\'%s\'</script>', $url );
    }
    die();
}

function change_lang( $lang=LANG_DEFAULT )
{
    if( !$lang ) $lang = LANG_DEFAULT;
    $_SESSION['lang'] = $lang;
}

function change_currency( $currency=null )
{
    global $db;
    
    if( !$currency )
    {
        //$currency = $db->getone( "select id from currencies order by position limit 1" );
        $currency = $db->getone( "select id from currencies where code=980 order by created_at desc limit 1" );
    }
    $_SESSION['currency_id'] = $currency;
}

function parse_coordinates( $coordinates )
{
    $retval = Array();
    
    $arr = split( ';', $coordinates );
    $i = 0;
    foreach( $arr as $v )
    {
        $retval[$i] = split( 'x', $v );
        $i++;
    }
    
    return( $retval );
}

function CR2BR( &$text )
{
    $text = str_replace( "\r", "", $text );
    $text = str_replace( "\n", "<br>", $text );
}

function unlink_associations( $path, $name )
{
    $path = realpath( $path );
    $dir = opendir( $path );

    while( $file = readdir($dir) )
    {
        if( preg_match( "/.*$name/", $file ) )
        {
            unlink( $path .'/'. $file );
        }
    }
}

function xmlencode($var)
{
    $val = $var;
    $val = str_replace("\"", "&quot;", $val);
    $val = str_replace("<", "&lt;", $val);
    $val = str_replace(">", "&gt;", $val);
    $val = str_replace("&", "&amp;", $val);
    
    return $val;
}

function mkdir_r( $dirName, $rights=0775 )
{
    $delim = '/';
    $dirs = explode($delim, $dirName);
    $dir='';
    foreach ($dirs as $part)
    {
        $dir.=$part.$delim;
        if( !is_dir($dir) && strlen($dir)>0 )
        {
            mkdir( $dir, $rights );
            chmod( $dir, $rights );
        }
    }
}

function rmdir_rf($dirname)
{
    if( $dirHandle = opendir($dirname) )
    {
        chdir( $dirname );
        while( $file = readdir($dirHandle) )
        {
            if( $file == '.' || $file == '..') continue;
            if( is_dir($file) ) rmdir_rf($file);
            else unlink($file);
        }
        chdir('..');
        closedir($dirHandle);
        rmdir($dirname);
    }
}

function get_file_ext( $file )
{
    $retval = explode( ".", $file );
    
    return( $retval[ count($retval)-1 ] );
}

function get_folder_into_xml( $folder_prefix )
{
    $retval = "<folder>";
    $dir = SITEROOT .'/'. $folder_prefix;
    if( is_dir($dir) && $dirHandle = opendir($dir) )
    {
        chdir( $dir );
        while( $file = readdir($dirHandle) )
        {
            if( $file == '.' || $file == '..') continue;
            $type = ''; $ext = ''; $size='0';
            if( is_file($file) )
            {
                $type = 'file';
                $ext = get_file_ext( $file );
                $size = filesize( $file );
            }
            elseif( is_dir($file) )
            {
                $type = 'dir';
            }
            $owner = fileowner( $file );
            $group = filegroup( $file );
            $retval .= "<item type='{$type}' ext='$ext' size='{$size}' owner='{$owner}' group='{$group}'>{$file}</item>";
        }
        closedir($dirHandle);
    }
    $retval .= "</folder>";
    
    return( $retval );
}

function put_file_into_browser( $f, $name )
{
    if( file_exists( $f ) )
    {
        $name = str_replace(',', '', $name);
        $name = str_replace(' ', '', $name);
        header("Content-Type: document/export");
        header("Content-Disposition: attachment; filename=\"{$name}\"");
        $buf  ='';
        if( $fd = fopen( $f, 'r' ) )
        {
            while( !feof($fd) )
            {
                $buf = fread( $fd, 4096 );
                echo $buf;
            }
            fclose($fd);
        }
    }
}

function DeleteFontInformation( $str )
{
    $retval = $str;
    
    $retval = preg_replace( "<[/]*font[^<]*>", "", $retval );
    $retval = preg_replace( "<div[^<]*>", "<div>", $retval );
    $retval = preg_replace( "<[/]*span[^<]*>", "", $retval );
    $retval = preg_replace( "<p[^<]*>", "<p>", $retval );
    $retval = preg_replace( "<[/]*h[0-9]{1}[^<]*>", "", $retval );
    $retval = preg_replace( "(<[a-z0-9]+)[ ]+([^<]*)(>)", "\\1\\3", $retval );
    
    return( $retval );
}

function DeleteHTMLTags( $str )
{
    $retval = $str;
    
    $retval = preg_replace( "</p[^<]*>", "\n", $retval );
    $retval = preg_replace( "<br[^<]*[/]*>", "\n", $retval );
    $retval = preg_replace( "<[/]*[^<]*[/]*>", "", $retval );
    
    return( $retval );
}

function DeleteHTMLComments( $str )
{
    $retval = $str;
    
    $retval = preg_replace( "/<!--.+-->/", "", $retval );
    $retval = preg_replace( "/display:\s*none/", "", $retval );
    $retval = preg_replace( "/<font.*>({img[^{]+})<\/font>/", "\1", $retval );
    $retval = preg_replace( "/<span.*>({img[^{]+})<\/span>/", "\1", $retval );
    $retval = str_replace( chr(01), '', $retval );
    $retval = str_replace( chr(02), '', $retval );
    $retval = str_replace( chr(03), '', $retval );
    $retval = str_replace( chr(05), '', $retval );
    $retval = str_replace( chr(07), '', $retval );
    $retval = str_replace( chr(08), '', $retval );
    $retval = str_replace( chr(20), '', $retval );
    $retval = str_replace( chr(21), '', $retval );
    $retval = str_replace( chr(22), '', $retval );
    $retval = str_replace( chr(23), '', $retval );
    $retval = str_replace( chr(24), '', $retval );
    $retval = str_replace( chr(33), '', $retval );
    
    return( $retval );
}

function GetFileExtension( $filename )
{
    $retval = "";
    
    if( $filename != '' )
    {
        if( preg_match( "/.+\.([^\.]+)/", $filename, $r ) )
        {
            if( count($r)>1 ) $retval = $r[1];
        }
    }
    
    return( $retval );
}

if (!function_exists('mb_str_split')) {
    function mb_str_split($str, $len = 1)
    {
        $arr = array();
        $strLen = mb_strlen($str, 'UTF-8');
        for ($i = 0; $i < $strLen; $i++)
        {
            $arr[] = mb_substr($str, $i, $len, 'UTF-8');
        }
        return $arr;
    }
}

function url_cyrillic_translite( $text ) {
    $retval = "";

    $text = mb_strtolower( $text, 'UTF-8' );
    $text = preg_replace( "/[\s]{2}/u", " ", $text );

    $ta = Array(
        'а' => 'a',
        'б' => 'b',
        'в' => 'v',
        'г' => 'g',
        'ѓ' => 'g',
        'д' => 'd',
        'е' => 'e',
        'ё' => 'e',
        'ж' => 'zh',
        'з' => 'z',
        'и' => 'i',
        'й' => 'y',
        'к' => 'k',
        'л' => 'l',
        'м' => 'm',
        'н' => 'n',
        'о' => 'o',
        'п' => 'p',
        'р' => 'r',
        'с' => 's',
        'т' => 't',
        'у' => 'u',
        'ф' => 'f',
        'х' => 'h',
        'ц' => 'c',
        'ч' => 'ch',
        'ш' => 'sh',
        'щ' => 'sh',
        'ъ' => '',
        'ы' => 'y',
        'ь' => '',
        'э' => 'e',
        'ю' => 'yu',
        'я' => 'ya',
        'і' => 'i',
        'ї' => 'yi',
        'є' => 'e',
        ' ' => '_',
        ',' => '_',
        ';' => '_',
        '/' => '_',
        ':' => '',
        '.' => '',
        '–' => '-',
        '?' => '',
        '!' => '',
        '|' => '',
        '"' => '',
        '\'' => '',
        '~' => ''
    );

    $chars = mb_str_split( $text );
    foreach( $chars as $c ) {
        if( array_key_exists($c, $ta)) $retval .= $ta[$c];
        else $retval .= $c;
    }
    $retval = preg_replace( "/[_]{2,3}/u", "_", $retval );
    $retval = preg_replace( "/_?\-+_?/u", "-", $retval );

    return( $retval );
}

function format_phone( $code, $phone ) {
    if( $phone ) {
        $phone = trim( $phone );
        $phone = preg_replace( '@[\s\\\/=&\(\)\[\]\{\}%\^\+\-\._]+@', '', $phone );
        $pos = strpos($phone, $code);
        if( $pos===0 ) $phone = substr( $phone, strlen($code) );
        
        if( $code == '380' ) {
            $zero = substr( $phone, 0, 1);
            if( $zero == '0' ) $phone = substr( $phone, 1 );
        }
    }
    
    return( $phone );
}

function format_email( $email ) {
    if( $email ) {
        $email = trim( $email );
        $email = preg_replace( '@[\s\\\/=&\(\)\[\]\{\}%\+\^]+@', '', $email );
        $email = preg_replace( '/[\.]{2,}/', '.', $email );
    }
    
    return( $email );
}

function format_price( $price ) {
    return( sprintf( "%01.2f", $price ) );
}

function convert_encoding( $from, $to, $str ) {
    return mb_convert_encoding( $str, $to, $from );
}

?>
<?php
/***********************************************************************
 Class v.2, 02/2016, PHP5
	Send email
 Copyright Andrey Nebogin anebogin@gmail.com
*************************************************************************/

require_once 'Mail.php';
require_once 'Mail/mime.php' ;

class AMailSendGrid
{
	var $subject, $body;
	var $from_name, $from_email, $reply_to;
	var $to_email;
	var $cc, $bcc;
	var $charset, $in_charset;
	
	var $parameters = Array(
		'host' => 'ssl://smtp.sendgrid.net',
		'port' => '465',
		'auth' => true,
		'username' => 'apikey',
		'password' => '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'
	);
	
	var $crlf = "\n";
	
	var $mime;
	
	function __construct( $charset='ISO-8859-1', $in_charset='UTF-8' )
	{
		$charset = 'UTF-8';
		$this->charset = $charset;
		$this->in_charset = $in_charset;
		$this->initMime();
	}
	
	private function initMime() {
		$this->mime = new Mail_mime( array(
			'eol' => $this->crlf,
			'head_charset' => $this->charset,
			'text_charset' => $this->charset,
			'html_charset' => $this->charset
		));
	}
	
	function __destruct()
	{
	}

	function AttachFile( $fname, $name, $mimetype='application/octet-stream' )
	{
		return( $this->mime->addAttachment( $fname, $mimetype, $name ) );
	}

	function AttachImage( $fname, $name )
	{
		return( $this->AttachFile( $fname, $name, 'image/gif' ) );
	}
	
	// returns empty string if sent okay, otherwise returns error text
	function Send( $body )
	{
		$retval = '';
		
		if( $this->from_email && $this->to_email && $this->subject && $body )
		{
			$headers = array(
				'From' => "Kollage Info <{$this->from_email}>",
				'Subject' => $this->subject
			);
			if( $this->bcc ) $headers['Bcc'] = $this->bcc;
			if( $this->cc ) $headers['Cc'] = $this->cc;
			
			$this->mime->setHTMLBody( $body );
			$body = $this->mime->get();
			$headers = $this->mime->headers( $headers );
			
			$smtp = Mail::factory( 'smtp', $this->parameters );
			$mail = $smtp->send( $this->to_email, $headers, $body );
			
			if( PEAR::isError($mail) ) {
				$retval = $mail->getMessage();
			} else {
				$retval = $mail;
			}
			
			//reset
			$this->body = "";
			$this->initMime();
		}
		
		return( $retval );
	}
}


?>
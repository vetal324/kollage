<?php
/*****************************************************
 Class v.2.3, 2007-2011
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

define( 'FILE_ERROR', 99 );
define( 'FILE_UNKNOWN', 100 );
define( 'FILE_JPG', 101 );
define( 'FILE_GIF', 102 );
define( 'FILE_PNG', 103 );
define( 'FILE_SWF', 104 );
define( 'FILE_MOV', 105 );
define( 'FILE_PDF', 106 );
define( 'FILE_ZIP', 107 );
define( 'FILE_FLV', 108 );
define( 'FILE_RAR', 109 );
define( 'FILE_PSD', 110 );
define( 'FILE_BMP', 111 );
define( 'FILE_TIF', 112 );
define( 'FILE_JPC', 113 );
define( 'FILE_JP2', 114 );
define( 'FILE_JPX', 115 );
define( 'FILE_JB2', 116 );
define( 'FILE_IFF', 117 );
define( 'FILE_WBMP', 118 );
define( 'FILE_XBM', 119 );

class AFileType
{
	var $filename, $type;
	var $g_width, $g_height; //width and height of the graphics object
	var $g_mime, $g_iptc;
	
	function __construct( $filename='' )
	{
		$this->type = FILE_UNKNOWN;
		$this->filename = $filename;
		if( $this->filename ) $this->type = $this->DetermineType();
		$this->g_width = 0;
		$this->g_height = 0;
		$this->g_mime = "";
		$this->g_iptc = "";
	}
	
	function __destruct()
	{
		unset( $this->type );
		$this->type = null;
		settype( $this, 'null' );
	}
	
	function GetType()
	{
		return( $this->type );
	}
	
	function GetTypeStr()
	{
		switch( $this->type )
		{
			case AIMAGE_JPG:
				$retval = 'jpeg';
				break;
			case AIMAGE_GIF:
				$retval = 'gif';
				break;
			case AIMAGE_PNG:
				$retval = 'png';
				break;
			case AIMAGE_SWF:
				$retval = 'swf';
				break;
			case AIMAGE_MOV:
				$retval = 'mov';
				break;
			case FILE_PDF:
				$retval = 'pdf';
				break;
			case FILE_ZIP:
				$retval = 'zip';
				break;
			case FILE_FLV:
				$retval = 'flv';
				break;
			case FILE_RAR:
				$retval = 'rar';
				break;
			default:
				$retval = 'unknown';
				break;
		}
		
		return( $retval );
	}
	
	function GetContentType()
	{
		$retval = '';
		
		switch( $this->type )
		{
			case FILE_JPG: $retval = 'image/jpeg'; break;
			case FILE_GIF: $retval = 'image/gif'; break;
			case FILE_PNG: $retval = 'image/png'; break;
			case FILE_SWF: $retval = 'application/x-shockwave-flash'; break;
			case FILE_MOV: $retval = 'video/quicktime'; break;
			case FILE_PDF: $retval = 'application/pdf'; break;
			case FILE_ZIP: $retval = 'application/zip'; break;
			case FILE_RAR: $retval = 'application/rar'; break;
			default: $retval = 'unknown/unknown'; break;
		}
		
		return( $retval );
	}
	
	function SetFile( $filename )
	{
		$this->filename = $filename;
		if( $this->filename ) $this->type = $this->DetermineType();
	}
	
	function DetermineType()
	{
		$retval = FILE_UNKNOWN;
		
		if( file_exists( $this->filename ) )
		{
			$general = getimagesize( $this->filename, $info );
			//$general = exif_imagetype( $this->filename );
			if( count($general) >=3 )
			{
				$this->g_width = $general[0];
				$this->g_height = $general[1];
				$this->g_mime = $general['mime'];
				
				if( isset($info["APP13"]) )
				{
					$this->g_iptc = $info["APP13"];
				}
				
				switch( $general[2] )
				{
					case 1:
						$retval = FILE_GIF;
						break;
					case 2:
						$retval = FILE_JPG;
						break;
					case 3:
						$retval = FILE_PNG;
						break;
					case 4:
					case 13:
						$retval = FILE_SWF;
						break;
					case 5:
						$retval = FILE_PSD;
						break;
					case 6:
						$retval = FILE_BMP;
						break;
					case 7:
					case 8:
						$retval = FILE_TIF;
						break;
					case 9:
						$retval = FILE_JPC;
						break;
					case 10:
						$retval = FILE_JP2;
						break;
					case 11:
						$retval = FILE_JPX;
						break;
					case 12:
						$retval = FILE_JB2;
						break;
					case 14:
						$retval = FILE_IFF;
						break;
					case 15:
						$retval = FILE_WBMP;
						break;
					case 16:
						$retval = FILE_XBM;
						break;
				}
			}
		}
		else $retval = FILE_ERROR;
		
		if( $retval == FILE_UNKNOWN || $retval == FILE_ERROR )
		{
			$retval = $this->DetermineType1();
		}
		
		return( $retval );
	}
	
	function DetermineType1()
	{
		$retval = FILE_UNKNOWN;
		
		if( file_exists( $this->filename ) )
		{
			$general = exif_imagetype( $this->filename );
				
			switch( $general )
			{
				case 1:
					$retval = FILE_GIF;
					break;
				case 2:
					$retval = FILE_JPG;
					break;
				case 3:
					$retval = FILE_PNG;
					break;
				case 4:
				case 13:
					$retval = FILE_SWF;
					break;
				case 5:
					$retval = FILE_PSD;
					break;
				case 6:
					$retval = FILE_BMP;
					break;
				case 7:
				case 8:
					$retval = FILE_TIF;
					break;
				case 9:
					$retval = FILE_JPC;
					break;
				case 10:
					$retval = FILE_JP2;
					break;
				case 11:
					$retval = FILE_JPX;
					break;
				case 12:
					$retval = FILE_JB2;
					break;
				case 14:
					$retval = FILE_IFF;
					break;
				case 15:
					$retval = FILE_WBMP;
					break;
				case 16:
					$retval = FILE_XBM;
					break;
			}
		}
		else $retval = FILE_ERROR;
		
		if( $retval == FILE_UNKNOWN )
		{
			$retval = $this->DetermineType2();
		}
		
		return( $retval );
	}
	
	function DetermineType2()
	{
		$retval = FILE_UNKNOWN;
		
		if( file_exists( $this->filename ) )
		{
			$f = fopen( $this->filename, 'r' );
			if( $f!=false )
			{
				$data = fread( $f, 11 );
				fclose( $f );
				
				$test = substr( $data, 0, 8 );
				if( strcmp( $test, chr(0).chr(0).chr(07).chr(181).chr(109).chr(111).chr(111).chr(118) )==0 ) $retval = FILE_MOV;
				if( strcmp( $test, chr(0).chr(0).chr(0).chr(8).chr(119).chr(105).chr(100).chr(101) )==0 ) $retval = FILE_MOV;
				
				$test = substr( $data, 0, 10 );
				if( strcmp( $test, chr(0).chr(0).chr(0).chr(32).chr(102).chr(116).chr(121).chr(112).chr(113).chr(116) )==0 ) $retval = FILE_MOV;
				
				$test = substr( $data, 0, 4 );
				if( strcmp( $test, '%PDF' )==0 ) $retval = FILE_PDF;
				
				$test = substr( $data, 0, 2 );
				if( strcmp( $test, 'PK' )==0 ) $retval = FILE_ZIP;
				
				$test = substr( $data, 0, 3 );
				if( strcmp( $test, 'FLV' )==0 ) $retval = FILE_FLV;
				
				$test = substr( $data, 0, 4 );
				if( strcmp( $test, 'Rar!' )==0 ) $retval = FILE_RAR;
			}
			else $retval = FILE_ERROR;
		}
		else $retval = FILE_ERROR;
		
		if( $retval == FILE_UNKNOWN || $retval == FILE_ERROR )
		{
			$retval = $this->DetermineType3();
		}
		
		return( $retval );
	}
	
	function DetermineType3()
	{
		$retval = FILE_UNKNOWN;
		
		$ext = strtolower( GetFileExtension( $this->filename ) );
		
		switch( $ext )
		{
			case 'gif':
				$retval = FILE_GIF;
				break;
			case 'jpg':
			case 'jpeg':
				$retval = FILE_JPG;
				break;
			case 'png':
				$retval = FILE_PNG;
				break;
			case 'swf':
				$retval = FILE_SWF;
				break;
			case 'psd':
				$retval = FILE_PSD;
				break;
			case 'bmp':
				$retval = FILE_BMP;
				break;
			case 'tif':
			case 'tiff':
				$retval = FILE_TIF;
				break;
		}
		
		return( $retval );
	}
}

?>
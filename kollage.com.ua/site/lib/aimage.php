<?php
/*****************************************************
 Class v.2.2, 2006-2009
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

define( 'AIMAGE_FILE_NOT_EXISTS', 101 );
define( 'AIMAGE_ERROR_OPEN_FILE', 102 );

if( !defined('SITEROOT') ) define( 'SITEROOT', realpath( dirname(__FILE__) . '/../' ) );

class AImage
{
	var $filename;
	var $ft; // FileType
	var $error;
	var $debug, $debug_messages;
	var $font_path = "/lib/fonts/";
	var $fonts = Array("ambass.ttf","arial.ttf","euro.ttf","times.ttf");
	var $opened = false;
	var $img = null;
	
	function __construct( $filename='', $debug=false )
	{
		$this->ft = new AFileType();
		if( $filename!='' ) $this->SetFilename( $filename );
		$this->debug = $debug;
		$this->error = null;
		$this->font_path = SITEROOT . $this->font_path;
	}
	
	function __destruct()
	{
		$this->Close();
		settype( $this, 'null' );
	}
	
	function Close()
	{
		if( $this->debug ) $this->debug_messages .= "Call Close()\n";
		if( $this->opened )
		{
			if( $this->debug ) $this->debug_messages .= "\tContinue Close, destroying the image\n";
			imagedestroy( $this->img );
			$this->opened = false;
			unset( $this->img ); $this->img = null;
		}
	}
	
	function SetFilename( $filename )
	{
		$this->opened = false;
		$this->Close();
		if( $this->debug ) $this->debug_messages .= "Call SetFilename('$filename')\n";
		$this->filename = $filename;
		$this->ft->SetFile( $filename );
	}
	
	function GetError()
	{
		return( $this->error );
	}
	
	function GetTypeStr()
	{
		return( $this->ft->GetTypeStr() );
	}
	
	function GetType()
	{
		return( $this->ft->GetType() );
	}
	
	function CopyResizeOrFitin( $name, $width=0, $height=0, $bgcolor=Array(0,0,0) )
	{
		$retval = null;
		
		$img = $this->Open();
		if( $img )
		{
			$w  = imagesx( $img );
			$h = imagesy( $img );
			
			if( ($width >= $height && $w >= $h) || ($width < $height && $w < $h) ) //the same orientation
			{
				if( $w < $width || $h < $height )
				{
					$retval = $this->CopyFitIn( $name, $width, $height, $bgcolor );
				}
				else
				{
					$retval = $this->CopyResize( $name, $width, $height );
				}
			}
			else
			{
				$retval = $this->CopyFitIn( $name, $width, $height, $bgcolor );
			}
		}
		
		return( $retval );
	}
	
	function CopyResize( $name, $width=0, $height=0, $old_x=0, $old_y=0, $old_width=0, $old_height=0 )
	{
		$retval = null;
		$use_crop = false;
		
		if( $this->debug ) $this->debug_messages .= "Call CopyResize('$name',$width,$height,$old_x,$old_y,$old_width,$old_height)\n";
		
		$img = $this->Open();
		if( $img )
		{
			$type = $this->GetType();
			
			if( $old_width==0  ) $old_width  = imagesx( $img );
			if( $old_height==0 ) $old_height = imagesy( $img );
			
			$scale = 0;
			if( $width>0 && $height==0 ) //calculating height
			{
				$scale = $old_width / $width;
				$height = floor( $old_height / $scale );
			}
			elseif( $height>0 && $width==0 ) //calculating width
			{
				$scale = $old_height / $height;
				$width = floor( $old_width / $scale );
			}
			else //use resizing and croping
			{
				$use_crop = true;
				if( $this->debug ) $this->debug_messages .= "\tUse resizeing and croping\n";
			}
			if( $this->debug ) $this->debug_messages .= "\tContinue CopyResize, scale=$scale; width=$width; height=$height\n";
			
			if( $name && $old_width>$width && $old_height>$height && $width>0 && $height>0 )
			{
				$thumbnail = imagecreatetruecolor( $width, $height );
				
				imagesavealpha( $thumbnail, true );
				$trans_colour = imagecolorallocatealpha( $thumbnail, 0, 0, 0, 127 );
				imagefill( $thumbnail, 0, 0, $trans_colour );
				
				if( function_exists('imageantialias') ) imageantialias( $img, true );
				if( function_exists('imageantialias') ) imageantialias( $thumbnail, true );
				if( !$use_crop )
				{
					imagecopyresampled( $thumbnail, $img, 0, 0, $old_x, $old_y, $width, $height, $old_width, $old_height );
				}
				else
				{
					$leftCrop = 0; $topCrop = 0;
					$tmpWidth = $width; $tmpHeight = $height;
					$ratioWidth = $old_width / $width;
					$ratioHeight = $old_height / $height;
					if( $ratioWidth > $ratioHeight ) $tmpWidth = floor( $old_width / $ratioHeight );
					else $tmpHeight = floor( $old_height / $ratioWidth );
					
					if( $this->debug ) $this->debug_messages .= "\tratioWidth=$ratioWidth; ratioHeight=$ratioHeight; tmpWidth=$tmpWidth; tmpHeight=$tmpHeight\n";
					
					$tmp = imagecreatetruecolor( $tmpWidth, $tmpHeight );
					imagecopyresampled( $tmp, $img, 0, 0, 0, 0, $tmpWidth, $tmpHeight, $old_width, $old_height );
					if( $ratioWidth > $ratioHeight ) $leftCrop = floor( ($tmpWidth - $width)/2 );
					else $topCrop = floor( ($tmpHeight - $height)/2 );
					
					if( $this->debug ) $this->debug_messages .= "\tleftCrop=$leftCrop; topCrop=$topCrop\n";
					
					imagecopy( $thumbnail, $tmp, 0, 0, $leftCrop, $topCrop, $width, $height );
				}
				$retval = Array( 0, 0 );
				$retval[0] = $width;
				$retval[1] = $height;
				
				switch( $type )
				{
					case FILE_JPG:
						imagejpeg( $thumbnail, $name );
						if( $this->debug ) $this->debug_messages .= "\tContinue CopyResize, call imagejpeg( 'thumbnail', '$name' ), returns $retval\n";
						break;
					case FILE_GIF:
						imagegif( $thumbnail, $name );
						if( $this->debug ) $this->debug_messages .= "\tContinue CopyResize, call imagegif( 'thumbnail', '$name' ), returns $retval\n";
						break;
					case FILE_PNG:
						imagepng( $thumbnail, $name );
						if( $this->debug ) $this->debug_messages .= "\tContinue CopyResize, call imagepng( 'thumbnail', '$name' ), returns $retval\n";
						break;
					default:
						if( $this->debug ) $this->debug_messages .= "\tUNKNOWN TYPE. type={$type}\n";
				}
				
				unset( $thumbnail ); $thumbnail = null;
				unset( $tmp ); $tmp = null;
				
			}
			else
			{
				if( $this->debug ) $this->debug_messages .= "\tContinue CopyResize, image is too small\n";
			}
		}
		
		return( $retval );
	}
	
	function CopyScale( $name, $width=0, $height=0, $old_x=0, $old_y=0, $old_width=0, $old_height=0 )
	{
		$img = $this->Open();
		if( $img && $name && $width>0 && $height>0 )
		{
			$old_width  = imagesx( $img );
			$old_height = imagesy( $img );
			
			if( $old_width > 0 && $old_height > 0 )
			{
				$scale = 1;
				if( $old_width>$width || $old_height>$height )
				{
					if( $old_width > $old_height )
					{
						$scale = $old_width / $width;
						if( ($old_height/$scale) > $height )
						{
							$scale = $old_height / $height;
						}
					}
					else
					{
						$scale = $old_height / $height;
					}
				}
				$w = floor( $old_width / $scale );
				$h = floor( $old_height / $scale );
				
				return( $this->CopyResize( $name, $w, $h, $old_x, $old_y, $old_width, $old_height ) );
			}
		}
	}
	
	// bgcolor is array in rgb format= {r,g,b}
	function CopyFitIn( $name, $width=0, $height=0, $bgcolor=Array(0,0,0) )
	{
		$retval = null;
		
		if( $this->debug ) $this->debug_messages .= "Call CopyFitIn('$name',$width,$height,$bgcolor)\n";
		
		$img = $this->Open();
		if( $img && $name && $width>0 && $height>0 )
		{
			$type = $this->GetType();
			
			$old_width  = imagesx( $img );
			$old_height = imagesy( $img );
			
			if( $old_width > 0 && $old_height > 0 )
			{
				$thumbnail = imagecreatetruecolor( $width, $height );
				
				imagesavealpha( $thumbnail, true );
				$trans_colour = imagecolorallocatealpha( $thumbnail, 0, 0, 0, 127 );
				imagefill( $thumbnail, 0, 0, $trans_colour );
				
				if( function_exists('imageantialias') ) imageantialias( $img, true );
				if( function_exists('imageantialias') ) imageantialias( $thumbnail, true );
				
				$bg = imagecolorallocate( $thumbnail, $bgcolor[0], $bgcolor[1], $bgcolor[2] );
				imagefilledrectangle( $thumbnail, 0, 0, $width - 1, $height - 1, $bg );
		
				$scale = 1;
				if( $old_width>$width || $old_height>$height )
				{
					if( $old_width > $old_height )
					{
						$scale = $old_width / $width;
						if( ($old_height/$scale) > $height )
						{
							$scale = $old_height / $height;
						}
					}
					else
					{
						$scale = $old_height / $height;
					}
				}
				$w = floor( $old_width / $scale );
				$h = floor( $old_height / $scale );
				//echo "w=$w; h=$h";
				
				$x = ($width-$w)/2;
				$y = ($height-$h)/2;
				
				imagecopyresampled( $thumbnail, $img, $x, $y, 0, 0, $w, $h, $old_width, $old_height );
				$retval = Array( 0, 0 );
				$retval[0] = $w;
				$retval[1] = $h;
				
				switch( $type )
				{
					case FILE_JPG:
						imagejpeg( $thumbnail, $name );
						if( $this->debug ) $this->debug_messages .= "\tContinue CopyResize, call imagejpeg( 'thumbnail', '$name' ), returns $retval\n";
						break;
					case FILE_GIF:
						imagegif( $thumbnail, $name );
						if( $this->debug ) $this->debug_messages .= "\tContinue CopyResize, call imagegif( 'thumbnail', '$name' ), returns $retval\n";
						break;
					case FILE_PNG:
						imagepng( $thumbnail, $name );
						if( $this->debug ) $this->debug_messages .= "\tContinue CopyResize, call imagepng( 'thumbnail', '$name' ), returns $retval\n";
						break;
				}
				
				unset( $thumbnail ); $thumbnail = null;
				
			}
		}
		else if( $this->debug ) $this->debug_messages .= "CopyFitIn. Input parameters error.\n";
		
		return( $retval );
	}
	
	function Rotate( $name, $degree )
	{
		$retval = null;
		
		$img = $this->Open();
		if( $img && $name && $degree )
		{
			$type = $this->GetType();
			
			$rh = imagerotate( $img, $degree, 0 );
			
			switch( $type )
			{
				case FILE_JPG:
					imagejpeg( $rh, $name );
					break;
				case FILE_GIF:
					imagegif( $rh, $name );
					break;
				case FILE_PNG:
					imagepng( $rh, $name );
					break;
			}
			
			imagedestroy( $rh );
			unset( $rh ); $rh = null;
		}
		
		return( $retval );
	}
	
	function Watermark( $text, $dst='', $font='', $r = 128, $g = 128, $b = 128, $alpha_level = 85 )
	{
		$retval = false;
		
		$img = $this->Open();
		if( $img && $dst && $text )
		{
			if( !$font ) $font = $this->font_path . $this->fonts[0];
			$type = $this->GetType();
			$w = $this->GetWidth();
			$h = $this->GetHeight();
			$angle = -rad2deg( atan2( (-$h),($w) ) ); 
			$text = " ". $text ." ";
			
			$c = imagecolorallocatealpha( $img, $r, $g, $b, $alpha_level );
			$size = ( ($w+$h)/2 ) * 2/strlen( $text );  
			$box  = imagettfbbox( $size, $angle, $font, $text );
			$x = $w/2 - abs($box[4] - $box[0])/2;
			$y = $h/2 + abs($box[5] - $box[1])/2;
			if( imagettftext( $img, $size , $angle, $x, $y, $c, $font, $text ) )
			{
				switch( $type )
				{
					case FILE_JPG:
						$retval = imagejpeg( $img, $dst );
						break;
					case FILE_GIF:
						$retval = imagegif( $img, $dst );
						break;
					case FILE_PNG:
						$retval = imagepng( $img, $dst );
						break;
				}
			}
		}
	}

	function Open()
	{
		$retval = null;
		
		if( $this->debug ) $this->debug_messages .= "Call Open()\n";
		if( !$this->opened )
		{
			if( $this->filename && file_exists( $this->filename ) && is_file( $this->filename ) )
			{
				$type = $this->GetType();
				switch( $type )
				{
					case FILE_JPG:
						$retval = imagecreatefromjpeg( $this->filename );
						if( $this->debug ) $this->debug_messages .= "\tContinue Open, call imagecreatefromjpeg('{$this->filename}')\n";
						break;
					case FILE_GIF:
						$retval = imagecreatefromgif( $this->filename );
						if( $this->debug ) $this->debug_messages .= "\tContinue Open, call imagecreatefromgif('{$this->filename}')\n";
						break;
					case FILE_PNG:
						$retval = imagecreatefrompng( $this->filename );
						if( $this->debug ) $this->debug_messages .= "\tContinue Open, call imagecreatefrompng('{$this->filename}')\n";
						break;
				}
				if( $retval != null )
				{
					$this->opened = true;
					$this->img = $retval;
					if( $this->debug ) $this->debug_messages .= "\tContinue Open, save resource identifier into this->img ({$retval})\n";
				}
			}
			else
			{
				$this->error = AIMAGE_FILE_NOT_EXISTS;
				if( $this->debug ) $this->debug_messages .= "\tContinue Open, filename '{$this->filename}' not exists)\n";
			}
		}
		else
		{
			if( $this->debug ) $this->debug_messages .= "\tContinue Open, file is already opened, returns saved resource identifier this->img ({$this->img})\n";
			$retval = $this->img;
		}
		
		return( $retval );
	}
	
	function GetWidth()
	{
		return( $this->ft->g_width );
	}
	
	function GetHeight()
	{
		return( $this->ft->g_height );
	}
}

?>
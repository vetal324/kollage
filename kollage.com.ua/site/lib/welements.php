<?php
/*****************************************************
 Web elements v.1.4.1, 2008
 Copyright Andrey Nebohin anebogin@gmail.com
******************************************************/

class WEMenuItem
{
    var $name, $title, $url, $onclick, $parameters, $selected, $enabled;
    
    function WEMenuItem( $name="", $title="", $url="", $onclick="" )
    {
        $this->name = $name;
        $this->title = $title;
        $this->url = $url;
        $this->onclick = $onclick;
        $this->parameters = Array();
        $this->unselect();
        $this->enable();
    }
    
    function get_url()
    {
        $retval = $this->url;
        $delim = "";
        
        if( strpos( $this->url, '?' ) ) $delim = '&';
        else
        {
            if( count( $this->parameters )>0 ) $delim = '?';
        }
        
        foreach( $this->parameters as $k )
        {
            $retval .= $delim . $k .'='. $this->parameters[$k];
            $delim = '&';
        }
        
        return( $retval );
    }
    
    function select()
    {
        $this->selected = true;
    }
    
    function unselect()
    {
        $this->selected = false;
    }
    
    function unselect_all()
    {
    }
    
    function is_selected()
    {
        $retval = ($this->selected)? 'true' : 'false';
        
        return( $retval );
    }
    
    function enable()
    {
        $this->enabled = true;
    }
    
    function disable()
    {
        $this->enabled = false;
    }
    
    function is_enabled()
    {
        $retval = ($this->enabled)? 'true' : 'false';
        return( $retval );
    }
    
    function add_parameter( $name, $value )
    {
        $this->parameters[$name] = $value;
    }
    
    function del_parameter( $name )
    {
        $d = Array();
        
        foreach( $this->parameters as $k=>$v )
        {
            if( $name != $k )
            {
                $d[$k] = $v;
            }
        }
        
        unset( $this->parameters );
        $this->parameters = $d;
    }
    
    function xml()
    {
        $retval = '';
        
        if( $this->onclick == '' )
        {
            $this->onclick = "document.location='". $this->get_url() ."'";
        }
        
        $retval .= "<item name=\"". $this->name ."\" title=\"". xmlencode($this->title) ."\" url=\"". xmlencode($this->get_url()) ."\" onclick=\"". xmlencode($this->onclick) ."\" chosen=\"". $this->is_selected() ."\" enabled=\"". $this->is_enabled() ."\"></item>";
        
        return( $retval );
    }
}

class WEMenuSeparator
{
    var $selected, $enabled, $name;
    
    function WEMenuSeparator()
    {
        $this->name = md5( time() + mt_rand(1,100) );
    }

    function select()
    {
        $this->selected = true;
    }
    
    function unselect()
    {
        $this->selected = false;
    }
    
    function unselect_all()
    {
    }
    
    function is_selected()
    {
        $retval = ($this->selected)? 'true' : 'false';
        
        return( $retval );
    }
    
    function enable()
    {
        $this->enabled = true;
    }
    
    function disable()
    {
        $this->enabled = false;
    }
    
    function is_enabled()
    {
        $retval = ($this->enabled)? 'true' : 'false';
        return( $retval );
    }
    
    function xml()
    {
        $retal = '';
        $retval .= "<separator/>";
        
        return( $retval );
    }
}

class WEMenu extends WEMenuItem
{
    var $items;
    
    function WEMenu( $name="", $title="", $url="" )
    {
        parent::WEMenuItem( $name, $title, $url );
        $this->items = Array();
    }
    
    function add_item( $item )
    {
        if( is_object($item) ) $this->items[$item->name] = $item;
    }
    
    function del_item( $name )
    {
    }
    
    function get_item( $name )
    {
        $retval = null;
        if( $this->items[$name] )
        {
            $retval = $this->items[$name];
        }
        
        return( $retval );
    }
    
    function del_all()
    {
        $this->items = Array();
    }
    
    function select_child( $name )
    {
        if( array_key_exists($name,$this->items) && $this->items[$name] )
        {
            $this->unselect_all();
            $this->items[$name]->select();
        }
        else
        {
            foreach( $this->items as $i )
            {
                if( isset($i->items) )
                {
                    $i->select_child( $name );
                }
            }
        }
    }
    
    function unselect_child( $name )
    {
        if( $this->items[$name] )
        {
            $this->items[$name]->unselect();
        }
    }
    
    function unselect_all()
    {
        foreach( $this->items as $item )
        {
            $item->unselect();
        }
    }
    
    function enable_child( $name )
    {
        if( $this->items[$name] )
        {
            $this->items[$name]->enable();
        }
    }
    
    function disable_child( $name )
    {
        if( $this->items[$name] )
        {
            $this->items[$name]->disable();
        }
    }
    
    function xml()
    {
        $retval = "<". $this->name ." name='". $this->name ."' title='". xmlencode($this->title) ."' url='". xmlencode($this->get_url()) ."' chosen='". $this->is_selected() ."' enabled='". $this->is_enabled() ."'>";
        
        foreach( $this->items as $item ) $retval .= $item->xml();
        
        $retval .= "</". $this->name .">";
      
        return( $retval );
    }
}

class WETabs extends WEMenu
{
    function WETabs( $name )
    {
        parent::WEMenu( $name );
    }
}

class WETab extends WEMenuItem
{
    function WETab( $name="", $title="", $url="" )
    {
        parent::WEMenuItem( $name, $title, $url );
    }
}

?>
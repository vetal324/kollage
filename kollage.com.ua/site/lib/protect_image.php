<?php
/***********************************************************************
 Class v.1.0, 2007, PHP5
	1. Generate a protect code.
	2. Generate a protect image using a protect code.
 Copyright Andrey Nebogin anebogin@gmail.com, Oleg Golovko wodan@list.ru
*************************************************************************/

define( "PC_COMPLEX", 1 ); //0-9a-z
define( "PC_NUMERIC", 2 ); //0-9
define( "PC_CHAR",    3 ); //a-z

class ProtectCode
{
	var $code;
	#var $numbers = Array( "1","2","3","4","5","6","7","8","9","0" );
	var $numbers = Array( "1","2","3","4","5","6","7","8","9" );
	#var $chars = Array( "a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z" );
	var $chars = Array( "a","b","c","d","e","f","g","h","i","j","k","m","n","p","q","r","s","t","u","v","w","x","y","z" );
	
	function ProtectCode( $length=6, $format=PC_COMPLEX )
	{
		$this->Generate( $length, $format );
	}
	
	function Generate( $length=6, $format=PC_COMPLEX )
	{
		$retval = null;
		
		switch( $format )
		{
			case PC_CHAR:
				$a = $this->chars;
				break;
			case PC_NUMERIC:
				$a = $this->numbers;
				break;
			case PC_COMPLEX:
			default:
				$a = array_merge( $this->chars, $this->numbers );
				break;
		}
		
		for( $i=0; $i<$length; $i++ )
		{
			$n = mt_rand( 0, count($a)-1 );
			$ch = $a[ $n ];
			$retval .= $ch;
		}
		
		$this->code = $retval;
		
		return( $retval );
	}
	
	function GetCode()
	{
		return( $this->code );
	}
}

class ProtectImage
{
	var $code, $image;
	var $font_path = "./lib/fonts/";
	var $fonts = Array("ambass.ttf","arial.ttf","euro.ttf","times.ttf");
	var $font_width = 18;
	var $font_height = 29;
	
	function __construct( $code="" )
	{
		$this->Generate( $code );
	}
	
	function __destruct()
	{
		imagedestroy( $this->image );
	}
	
	function Generate( $code )
	{
		$retval = false;
		
		$this->code = $code;
		$length = strlen( $this->code );
		$this->image = null;
		$codes = preg_split('//', $this->code, -1, PREG_SPLIT_NO_EMPTY);
		$font = realpath( $this->font_path . $this->GetRandomFont() );
		$bg = realpath( $this->font_path . 'bg02.jpg' );
		$this->image = imagecreate( $length*$this->font_width, $this->font_height );
		if( file_exists($bg) ) {
			$bg_image = imagecreatefromjpeg( $bg );
			imagecopy( $this->image, $bg_image, 0, 0, mt_rand(0,900), mt_rand(0,900), $length*$this->font_width, $this->font_height );
		}
		if( $this->image && file_exists( $font ) )
		{
			$black = imagecolorallocate( $this->image, 0, 0, 0 );
			$white = imagecolorallocate( $this->image, 255, 255, 255 );
			$x = 8; $y = 16;
			
			for( $i=0; $i<$length; $i++ )
			{
				$angle = mt_rand( -30, 30 );
				imagettftext ($this->image, 18, $angle, $x, $y, $white, $font, $codes[$i] );
				if( round($i/2)==floor($i/2) ) $y += 3; else $y -= 3;
				$x += 15;
			}
			$retval = true;
		}
		
		return( $retval );
	}
	
	function SaveImage( $path )
	{
		if( $this->image )
		{
			imagegif( $this->image, $path );
		}
	}
	
	function PrintImage()
	{
		if( $this->image )
		{
			header( "Content-type: image/gif" );
			imagegif( $this->image );
		}
	}
	
	function GetCode()
	{
		return( $this->code );
	}
	
	function GetRandomFont()
	{
		//$i = mt_rand( 0, count($this->fonts)-1 );
		//$retval = $this->fonts[ $i ];
		$retval = $this->fonts[ 0 ];
		
		return( $retval );
	}
	
}

?>
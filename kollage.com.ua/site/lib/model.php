<?php
/*****************************************************
 Class v.1.0, 2007
 It's basical class for all models
 Copyright Andrey Nebogin anebogin@gmail.com
******************************************************/

class Model
{
    var $id;
    var $position, $created_at, $updated_at;
    
    var $loaded = false;
    var $tablename = '';

    function Model( $id=0 )
    {
        $this->id = $id;
        $this->position = 0;
        $this->Load($id);
    }
    
    function Load( $id )
    {
        $this->loaded = false;
        
        if( $id>0 )
        {
            $t = new MysqlTable( $this->tablename );
            if( $t->find_first( "id=$id" ) )
            {
                $this->_Load( $t->data[0] );
            }
        }
    }
    
    function _Load( &$row )
    {
    }

    function IsLoaded()
    {
        return( $this->loaded );
    }
    
    function Save( $data=null )
    {   
        $this->CheckupData();
        return( $this->id );
    }
    
    function Delete()
    {
        if( $this->IsLoaded() )
        {
            $t = new MysqlTable( $this->tablename );
            $t->del( $this->id );
        }
    }
    
    function Del()
    {
        $this->Delete();
    }
    
    function CheckupData()
    {
        return( true );
    }
    
    function GetLastPosition( $condition='' )
    {
        global $db;
        $where = ($condition)? "where {$condition}" : '';
        
        return( intval($db->getone( "select position from {$this->tablename} {$where} order by position desc,id desc limit 1" )) );
    }
    
    function Xml()
    {
    }
}

?>
<?php

require('lib/util.php');
load_configuration();
initialize_session( 'frontend' );

require('lib/init.php');

$controller = load_controller( 'designer_orders' );
$controller->checkup_orders();

?>
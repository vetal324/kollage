<?php
    require('lib/util.php');
    load_configuration();
    initialize_session( 'frontend' );
    
    require('lib/init.php');
    require('lib/cron.php');
    
    $cron = new Cron();
    $cron->ProcessItems();
    
    header( "Content-type: image/gif" );
    readfile( 'empty.gif' );

?>
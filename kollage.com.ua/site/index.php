<?php

require('lib/util.php');
load_configuration();
initialize_session( 'frontend' );

require('lib/init.php');

list($controller, $method, $parameters) = parse_html_query();
if( !$controller )
{
    $controller = 'base'; $method = 'welcome';
}

if( $controller && $method )
{
    $document = load_controller( $controller );
    $log->Write("Loaded controller {$controller}=". is_object($document) );
    if( is_object($document) )
    {
        //run_action( $document, $method, $parameters );
        if( !run_action( $document, $method ) )
        {
            $document = load_controller( "services" );
            run_action( $document, "show" );
        }
    }
    else
    {
        $document = load_controller( "services" );
        run_action( $document, "show" );
    }
}

/* run bbclone stats */
define("_BBC_PAGE_NAME", "Kollage");
define("_BBCLONE_DIR", "bbclone/");
define("COUNTER", _BBCLONE_DIR."mark_page.php");
if (is_readable(COUNTER)) include_once(COUNTER);
/* run bbclone stats */

?>
<?php

require('lib/util.php');
load_configuration();
initialize_session( 'frontend' );

require('lib/init.php');

set_time_limit(3600);
header("Expires: 0");
header("Cache-Control: private");
header("Pragma: no-cache");

header('Content-type: text/xml');
echo "<?xml version=\"1.0\" encoding=\"windows-1251\"?>";

$controller = 'billing_gate'; $method = 'main';

if( $controller && $method )
{
    $document = load_controller( $controller );
    run_action( $document, $method );
}

?>